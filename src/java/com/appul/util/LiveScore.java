/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.util.Date;

/**
 *
 * @author Akter
 */
public class LiveScore {

    private int tournamentId;
    private String tournamentName;
    private Date matchDate;
    private int teamId1;
    private int teamId2;
    private String team1Name;
    private String team2Name;
    private String team1ShortName;
    private String team2ShortName;
    private String logoLink1;
    private String logoLink2;
    private String venueName;
    private int venueId;
    private int matchId;
    private String matchWinner;
    private String tossWinner;
    private int team1Score;
    private int team2Score;
    private int team1Over;
    private int team2Over;
    private int team1Ball;
    private int team2Ball;
    private float team1CRR;
    private float team2CRR;
    private float team2RRR;
    private String team1Player1;
    private String team1Player2;
    private String team1Striker;
    private String team2Bowler1;
    private String team2Player1;
    private String team2Player2;
    private String team2Striker;
    private String team1Bowler1;
    private String team1Wicket;
    private String team2Wicket;
    private String team1Player1Url;
    private String team1Player2Url;
    private String team2Bowler1Url;
    private String team2Player1Url;
    private String team2Player2Url;
    private String team1Bowler1Url;
    private int inningsId;

    //getter and setter methods  

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public int getTeamId1() {
        return teamId1;
    }

    public void setTeamId1(int teamId1) {
        this.teamId1 = teamId1;
    }

    public int getTeamId2() {
        return teamId2;
    }

    public void setTeamId2(int teamId2) {
        this.teamId2 = teamId2;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getTeam1ShortName() {
        return team1ShortName;
    }

    public void setTeam1ShortName(String team1ShortName) {
        this.team1ShortName = team1ShortName;
    }

    public String getTeam2ShortName() {
        return team2ShortName;
    }

    public void setTeam2ShortName(String team2ShortName) {
        this.team2ShortName = team2ShortName;
    }

    public String getLogoLink1() {
        return logoLink1;
    }

    public void setLogoLink1(String logoLink1) {
        this.logoLink1 = logoLink1;
    }

    public String getLogoLink2() {
        return logoLink2;
    }

    public void setLogoLink2(String logoLink2) {
        this.logoLink2 = logoLink2;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }
    public int getInningsId() {
        return inningsId;
    }

    public void setInningsId(int inningsId) {
        this.inningsId = inningsId;
    }
    public String getMatchWinner() {
        return matchWinner;
    }

    public void setMatchWinner(String matchWinner) {
        this.matchWinner = matchWinner;
    }

    public String getTossWinner() {
        return tossWinner;
    }

    public void setTossWinner(String tossWinner) {
        this.tossWinner = tossWinner;
    }

    public int getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(int team1Score) {
        this.team1Score = team1Score;
    }

    public int getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(int team2Score) {
        this.team2Score = team2Score;
    }

    public int getTeam1Over() {
        return team1Over;
    }

    public void setTeam1Over(int team1Over) {
        this.team1Over = team1Over;
    }

    public int getTeam2Over() {
        return team2Over;
    }

    public void setTeam2Over(int team2Over) {
        this.team2Over = team2Over;
    }

    public int getTeam1Ball() {
        return team1Ball;
    }

    public void setTeam1Ball(int team1Ball) {
        this.team1Ball = team1Ball;
    }

    public int getTeam2Ball() {
        return team2Ball;
    }

    public void setTeam2Ball(int team2Ball) {
        this.team2Ball = team2Ball;
    }

    public float getTeam1CRR() {
        return team1CRR;
    }

    public void setTeam1CRR(float team1CRR) {
        this.team1CRR = team1CRR;
    }

    public double getTeam2CRR() {
        return team2CRR;
    }

    public void setTeam2CRR(float team2CRR) {
        this.team2CRR = team2CRR;
    }

    public float getTeam2RRR() {
        return team2RRR;
    }

    public void setTeam2RRR(float team2RRR) {
        this.team2RRR = team2RRR;
    }

    public String getTeam1Player1() {
        return team1Player1;
    }

    public void setTeam1Player1(String team1Player1) {
        this.team1Player1 = team1Player1;
    }

    public String getTeam1Player2() {
        return team1Player2;
    }

    public void setTeam1Player2(String team1Player2) {
        this.team1Player2 = team1Player2;
    }

    public String getTeam1Striker() {
        return team1Striker;
    }

    public void setTeam1Striker(String team1Striker) {
        this.team1Striker = team1Striker;
    }

    public String getTeam2Bowler1() {
        return team2Bowler1;
    }

    public void setTeam2Bowler1(String team2Bowler1) {
        this.team2Bowler1 = team2Bowler1;
    }

    public String getTeam2Player1() {
        return team2Player1;
    }

    public void setTeam2Player1(String team2Player1) {
        this.team2Player1 = team2Player1;
    }

    public String getTeam2Player2() {
        return team2Player2;
    }

    public void setTeam2Player2(String team2Player2) {
        this.team2Player2 = team2Player2;
    }

    public String getTeam2Striker() {
        return team2Striker;
    }

    public void setTeam2Striker(String team2Striker) {
        this.team2Striker = team2Striker;
    }

    public String getTeam1Bowler1() {
        return team1Bowler1;
    }

    public void setTeam1Bowler1(String team1Bowler1) {
        this.team1Bowler1 = team1Bowler1;
    }

    public String getTeam1Player1Url() {
        return team1Player1Url;
    }

    public void setTeam1Player1Url(String team1Player1Url) {
        this.team1Player1Url = team1Player1Url;
    }

    public String getTeam1Player2Url() {
        return team1Player2Url;
    }

    public void setTeam1Player2Url(String team1Player2Url) {
        this.team1Player2Url = team1Player2Url;
    }

    public String getTeam2Bowler1Url() {
        return team2Bowler1Url;
    }

    public void setTeam2Bowler1Url(String team2Bowler1Url) {
        this.team2Bowler1Url = team2Bowler1Url;
    }

    public String getTeam2Player1Url() {
        return team2Player1Url;
    }

    public void setTeam2Player1Url(String team2Player1Url) {
        this.team2Player1Url = team2Player1Url;
    }

    public String getTeam2Player2Url() {
        return team2Player2Url;
    }

    public void setTeam2Player2Url(String team2Player2Url) {
        this.team2Player2Url = team2Player2Url;
    }

    public String getTeam1Bowler1Url() {
        return team1Bowler1Url;
    }

    public void setTeam1Bowler1Url(String team1Bowler1Url) {
        this.team1Bowler1Url = team1Bowler1Url;
    }

    public String getTeam1Wicket() {
        return team1Wicket;
    }

    public void setTeam1Wicket(String team1Wicket) {
        this.team1Wicket = team1Wicket;
    }

    public String getTeam2Wicket() {
        return team2Wicket;
    }

    public void setTeam2Wicket(String team2Wicket) {
        this.team2Wicket = team2Wicket;
    }
}
