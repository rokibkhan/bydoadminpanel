package com.appul.util;


import java.io.*;
import java.net.*;
import java.util.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class dayDiff{

public static void main(String args[]){
	String datestart="";	
	String dateend="";
	double diff=0;
	
	if (args.length>0){
		datestart=args[0].trim();
		dateend=args[1].trim();
	} else {
		System.out.println("Required parameter not passed as arguments.");
		return;
	}

	try{
	
		//System.out.println("Adding Date");
		diff=getDateDiff(datestart,dateend);
		System.out.println(diff);
	} catch(Exception e){
		e.printStackTrace();
	}
}

public static double getDateDiff(String datestart, String dateend) throws Exception{


        //Total time for one day
        int one_day=1000*60*60*24;
        Date dtstart=getFormatedDate(datestart);
        Date dtend=getFormatedDate(dateend);
		double diff=0;

    	diff=Math.ceil((dtend.getTime()-dtstart.getTime())/(one_day)); 


		return diff;
	}
	
public	static Date getFormatedDate(String dateIn) throws Exception{

	DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
    Date dateOut = dateFormat.parse(dateIn);	   	    
    return dateOut;
}
}