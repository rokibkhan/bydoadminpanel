/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.util.Date;

/**
 *
 * @author Akter
 */
public class LiveMatchList {

    private int tournamentId;
    private String tournamentName;
    private Date matchDate;
    private int teamId1;
    private int teamId2;
    private String team1Name;
    private String team2Name;
    private String team1ShortName;
    private String team2ShortName;
    private String logoLink1;
    private String logoLink2;
    private String venueName;
    private int venueId;
    private int matchId;
    private int matchNumber;
    private String tossWinnerId;
    private String matchWinnerId;
    private String tossWinnerChoice;
    private int team1Run;
    private int team2Run;
    private int team1Wicket;
    private int team2Wicket;
    private String team1Over;
    private String team2Over;
    private float team1CRR;
    private float team2CRR;
    private float team1RRR;
    private float team2RRR;
    private int inningId;

    //getter and setter methods  
    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public int getTeamId1() {
        return teamId1;
    }

    public void setTeamId1(int teamId1) {
        this.teamId1 = teamId1;
    }

    public int getTeamId2() {
        return teamId2;
    }

    public void setTeamId2(int teamId2) {
        this.teamId2 = teamId2;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }
    
    
     public String getTeam1ShortName() {
        return team1ShortName;
    }

    public void setTeam1ShortName(String team1ShortName) {
        this.team1ShortName = team1ShortName;
    }

    public String getTeam2ShortName() {
        return team2ShortName;
    }

    public void setTeam2ShortName(String team2ShortName) {
        this.team2ShortName = team2ShortName;
    }
    
    public String getLogoLink1() {
        return logoLink1;
    }

    public void setLogoLink1(String logoLink1) {
        this.logoLink1 = logoLink1;
    }

    public String getLogoLink2() {
        return logoLink2;
    }

    public void setLogoLink2(String logoLink2) {
        this.logoLink2 = logoLink2;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getMatchNumber() {
        return matchNumber;
    }

    public void setMatchNumber(int matchNumber) {
        this.matchNumber = matchNumber;
    }

    public String getTossWinnerId() {
        return tossWinnerId;
    }

    public void setTossWinnerId(String tossWinnerId) {
        this.tossWinnerId = tossWinnerId;
    }

    public String getTossWinnerChoice() {
        return tossWinnerChoice;
    }

    public void setTossWinnerChoice(String tossWinnerChoice) {
        this.tossWinnerChoice = tossWinnerChoice;
    }
  public String getMatchWinnerId() {
        return matchWinnerId;
    }

    public void setMatchWinnerId(String matchWinnerId) {
        this.matchWinnerId = matchWinnerId;
    }
    public int getTeam1Run() {
        return team1Run;
    }

    public void setTeam1Run(int team1Run) {
        this.team1Run = team1Run;
    }

    public int getTeam2Run() {
        return team2Run;
    }

    public void setTeam2Run(int team2Run) {
        this.team2Run = team2Run;
    }

    public int getTeam1Wicket() {
        return team1Wicket;
    }

    public void setTeam1Wicket(int team1Wicket) {
        this.team1Wicket = team1Wicket;
    }

    public int getTeam2Wicket() {
        return team2Wicket;
    }

    public void setTeam2Wicket(int team2Wicket) {
        this.team2Wicket = team2Wicket;
    }

    public String getTeam1Over() {
        return team1Over;
    }

    public void setTeam1Over(String team1Over) {
        this.team1Over = team1Over;
    }

    public String getTeam2Over() {
        return team2Over;
    }

    public void setTeam2Over(String team2Over) {
        this.team2Over = team2Over;
    }

    public float getTeam1CRR() {
        return team1CRR;
    }

    public void setTeam1CRR(float team1CRR) {
        this.team1CRR = team1CRR;
    }

    public float getTeam2CRR() {
        return team2CRR;
    }

    public void setTeam2CRR(float team2CRR) {
        this.team2CRR = team2CRR;
    }

    public float getTeam1RRR() {
        return team1RRR;
    }

    public void setTeam1RRR(float team1RRR) {
        this.team1RRR = team1RRR;
    }

    public float getTeam2RRR() {
        return team2RRR;
    }

    public void setTeam2RRR(float team2RRR) {
        this.team2RRR = team2RRR;
    }
    
    
     public int getInningId() {
        return inningId;
    }

    public void setInningId(int inningId) {
        this.inningId = inningId;
    }

}
