/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Akter
 */
public class DateTime {

    public Long getDateTimetoSec(Date datetime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(datetime);

        String year = "";
        String month = "";
        String ddate = "";
        String hour = "";
        String min = "";
        String sec = "";
        long convertTime = 0;
        year = date.substring(0, 4);
        month = date.substring(5, 7);
        ddate = date.substring(8, 10);
        hour = date.substring(11, 13);
        min = date.substring(14, 16);
        sec = date.substring(17, date.length());
        convertTime = (Long.parseLong(year) * 365 * 24 * 3600) + (Long.parseLong(month) * 30 * 24 * 3600) + (Long.parseLong(ddate) * 24 * 3600)
                + (Long.parseLong(hour) * 3600) + (Long.parseLong(min) * 60) + (Long.parseLong(sec));
        return convertTime;

    }

    public Long getDatetoSec(Date dateonly) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(dateonly);

        String year = "";
        String month = "";
        String ddate = "";
        String hour = "";
        String min = "";
        String sec = "";
        long convertTime = 0;
        year = date.substring(0, 4);
        month = date.substring(5, 7);
        ddate = date.substring(8, 10);
        convertTime = (Long.parseLong(year) * 365 * 24 * 3600) + (Long.parseLong(month) * 30 * 24 * 3600) + (Long.parseLong(ddate) * 24 * 3600);

        return convertTime;

    }

    public static String getDateTime() {
        java.text.DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
