package com.appul.util;

import java.util.*;

public class dateAdd {

    public static void main(String args[]) {
        String inputdate = "";
        String retdate = "";
        int days = 0;

        if (args.length > 0) {
            inputdate = args[0].trim();
            days = Integer.parseInt(args[1].trim());
        } else {
            System.out.println("Required parameter not passed as arguments.");
            return;
        }

        //System.out.println("Adding Date");
        retdate = dateAdd.retDate(inputdate, days);
        System.out.println(retdate);

    }

    public static String retDate(String strDate, Integer daytoAdd) {

//int daytoAdd=0;
        String retdate = null;
//daytoAdd=Integer.parseInt(strDayToAdd);

        int date = Integer.parseInt(strDate);
        Calendar cal = dateToCalendar(date);
        cal.add(Calendar.DATE, daytoAdd);
        date = calendarToDate(cal);
//System.out.println(date);
        retdate = Integer.toString(date);
        return retdate;
    }

    public static Calendar dateToCalendar(int date) {
        int day = date % 100;
//System.out.println(day);
        int month = (date / 100) % 100 - 1;
//System.out.println(month);
        int year = date / 10000;
//System.out.println(year);
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);
//System.out.println(cal);
        return cal;
    }

    public static int calendarToDate(Calendar cal) {
        int day = cal.get(Calendar.DATE);
//System.out.println("day "+day);
        int month = cal.get(Calendar.MONTH) + 1;
//System.out.println("month "+month);
        int year = cal.get(Calendar.YEAR);
//System.out.println("year "+year);
//System.out.println(year * 10000 + month * 100 + day);
        return year * 10000 + month * 100 + day;
    }
}