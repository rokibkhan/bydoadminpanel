/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.util.HibernateUtil;
import com.appul.entity.SyRoles;
import com.appul.entity.SyUser;
import java.util.*;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class getRole {

    Session dbsession = null;
    org.hibernate.Transaction dbtrx = null;

    public String getRoleName(String roleID) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyRoles where roleId='" + roleID + "'");
        Iterator itr = q1.list().iterator();


        String roleName = "";

        if (itr.hasNext()) {
            SyRoles role = (SyRoles) itr.next();
            roleName = role.getRoleDesc();
        } else {
            roleName = "";
        }
        dbtrx.commit();
        dbsession.close();
        return roleName;
    }

    public String getRoleID(String roleName) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyRoles where roleDesc='" + roleName + "'");
        Iterator itr = q1.list().iterator();


        String roleID = "";

        if (itr.hasNext()) {
            SyRoles role = (SyRoles) itr.next();
            roleID = role.getRoleId();
        } else {
            roleID = "";
        }
        dbtrx.commit();
        dbsession.close();
        return roleID;
    }
}
