/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.sql.Blob;
import java.util.Date;

/**
 *
 * @author Akter
 */
public class FixtureObj {

    private int tournamentId;
    private String tournamentName;
    private Date matchDate;
    private int teamId1;
    private int teamId2;
    private String team1Name;
    private String team2Name;
    private String team1ShortName;
    private String team2ShortName;
    private byte[] team1Logo;
    private byte[] team2Logo;
    private String logoLink1;
    private String logoLink2;
    private String venueName;
    private int venueId;
    private int matchId;
    private int matchNumber;

    //getter and setter methods  
    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public int getTeamId1() {
        return teamId1;
    }

    public void setTeamId1(int teamId1) {
        this.teamId1 = teamId1;
    }

    public int getTeamId2() {
        return teamId2;
    }

    public void setTeamId2(int teamId2) {
        this.teamId2 = teamId2;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getTeam1ShortName() {
        return team1ShortName;
    }

    public void setTeam1ShortName(String team1ShortName) {
        this.team1ShortName = team1ShortName;
    }

    public String getTeam2ShortName() {
        return team2ShortName;
    }

    public void setTeam2ShortName(String team2ShortName) {
        this.team2ShortName = team2ShortName;
    }

    public byte[] getTeam1Logo() {
        return team1Logo;
    }

    public void setTeam1Logo(byte[] team1Logo) {
        this.team1Logo = team1Logo;
    }

    public byte[] getTeam2Logo() {
        return team2Logo;
    }

    public void setTeam2Logo(byte[] team2Logo) {
        this.team2Logo = team2Logo;
    }

    public String getLogoLink1() {
        return logoLink1;
    }

    public void setLogoLink1(String logoLink1) {
        this.logoLink1 = logoLink1;
    }

    public String getLogoLink2() {
        return logoLink2;
    }

    public void setLogoLink2(String logoLink2) {
        this.logoLink2 = logoLink2;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getMatchNumber() {
        return matchNumber;
    }

    public void setMatchNumber(int matchNumber) {
        this.matchNumber = matchNumber;
    }

}
