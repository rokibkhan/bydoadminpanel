package com.appul.util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.*;

/**
 *
 * @author Akter
 */
public class DBAccess {

    String myname = null;

    public static Connection dbsrc() {

        String ip = "192.168.22.26";
        String port = "3306";
        String dbname = "appuldb";
        String userid = "appul";
        String passwd = "appul";

        Connection dbcon = null;

        try {
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            String dbURL = "jdbc:mysql://dblapp:3306/appuldb";
            String dbURL = "jdbc:mysql://192.168.22.26:3306/appuldb";
            dbcon = DriverManager.getConnection(dbURL, userid, passwd);

            return dbcon;
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return null;
        }
    }

}
