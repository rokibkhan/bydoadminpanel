/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

@WebServlet(name = "VoucherEntry", urlPatterns = {"/VoucherEntry"})

public class VoucherEntry extends HttpServlet {

    public static Logger logger = Logger.getLogger("VoucherEntry.class");

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();
            String sessionid = request.getParameter("sessionid").trim();
            getRegistryID getId = new getRegistryID();
            String vId = getId.getID(5);
            String voucherType = request.getParameter("voucherType").trim();
            String description = request.getParameter("description") == null ? "" : request.getParameter("description").trim();
            String voucherDate = request.getParameter("voucherDate").trim();
            String userStoreId = request.getParameter("storeId").trim();

            String row = request.getParameter("hide").trim();
            int rowCount = Integer.parseInt(row);
            System.out.println("rowCount : " + rowCount);
            Double debitTotal = 0.00;
            Double creditTotal = 0.00;

            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();
            String vChildId = "";

            Query q1 = null;
            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            String accountHead = "";
            String payeeId = "";
            String chequeNo = "";
            String debit = "";
            String credit = "";
            String accountHead1 = "";
            String strMsg = "";

            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {
            }
            try {

                for (int i = 0; i < rowCount; i++) {

                    accountHead = request.getParameter("accountHead[" + i + "]").trim();
                    System.out.println("Value of i: " + i + "accountHead1" + accountHead1);
//                    accountHead = accountHead1.substring(0, accountHead1.lastIndexOf("|")).trim();
                    payeeId = request.getParameter("payeeId[" + i + "]") == null ? "" : request.getParameter("payeeId[" + i + "]");
                    chequeNo = request.getParameter("chequeNo[" + i + "]") == null ? "" : request.getParameter("chequeNo[" + i + "]");
                    debit = request.getParameter("debit[" + i + "]") == null ? "0" : request.getParameter("debit[" + i + "]");
                    credit = request.getParameter("credit[" + i + "]") == null ? "0" : request.getParameter("credit[" + i + "]");

                    debitTotal = debitTotal + Double.parseDouble(debit);
                    creditTotal = creditTotal + Double.parseDouble(credit);
                    vChildId = getId.getID(6);

                    Double drValue = Double.parseDouble(debit);
                    Double crValue = Double.parseDouble(credit);
//                    String ValueCr = "-" + crValue.toString();

                    if (drValue != 0.0D) {
                        Query qVoucherChild6 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP,STORE_ID) values(" + vChildId + "," + vId + ",'" + voucherType + "','" + voucherDate + "','" + accountHead + "','" + chequeNo + "','" + payeeId + "'," + drValue + ",now(),'" + userId + "','" + ipAddress + "','0','"+userStoreId+"') ");
//                        Query qVoucherChild6 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP) values(V_CHILD_ID.nextval," + vId + ",'" + voucherType + "',to_date('" + voucherDate + "','YYYY-MM-DD'),'" + accountHead + "','" + chequeNo + "','" + payeeId + "'," + drValue + ",systimestamp,'" + userId + "','" + ipAddress + "','0') ");
                        qVoucherChild6.executeUpdate();
                    }
                    if (crValue != 0.0D) {
                        Query qVoucherChild8 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP,STORE_ID) values(" + vChildId + "," + vId + ",'" + voucherType + "','" + voucherDate + "','" + accountHead + "','" + chequeNo + "','" + payeeId + "'," + crValue + "*-1,now(),'" + userId + "','" + ipAddress + "','0','"+userStoreId+"') ");
//                        Query qVoucherChild8 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP) values(V_CHILD_ID.nextval," + vId + ",'" + voucherType + "',to_date('" + voucherDate + "','YYYY-MM-DD'),'" + accountHead + "','" + chequeNo + "','" + payeeId + "'," + crValue + "*-1,systimestamp,'" + userId + "','" + ipAddress + "','0') ");
                        qVoucherChild8.executeUpdate();
                    }

                }
                Query qVoucher = dbsession.createSQLQuery(" insert into voucher (ID,VOUCHER_ID,VOUCHER_TYPE,PREPARED,CHECKEDBY,APPROVED,DESCRIPTION,VOUCHER_DATE,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP,STORE_ID) values(" + vId + "," + vId + ",'" + voucherType + "','','','','" + description + "','" + voucherDate + "',now(),'" + userId + "','" + ipAddress + "','0','"+userStoreId+"' ) ");
                qVoucher.executeUpdate();

            } catch (NumberFormatException e) {
                dbtrx.rollback();
                dbsession.close();
                strMsg = "Voucher psoting Failed";
                logger.info("Voucher psoting Failed");
                System.out.println("Voucher psoting Failed");

            } finally {

                dbsession.flush();
                dbtrx.commit();
                dbsession.close();
                 strMsg = "Voucher saved sucessfully.";
                logger.info("Voucher saved sucessfully.");
                System.out.println("Voucher saved sucessfully.");
                out.println("Voucher saved sucessfully.");
                response.sendRedirect(GlobalVariable.baseUrl + "/account/voucherAdd.jsp?sessionid="+sessionid+ "&strMsg=" + strMsg+ "&vId=" + vId);

            }

        } catch (HibernateException e) {
        } finally {
            out.close();
        }
    }
}
