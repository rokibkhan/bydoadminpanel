/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.MemberTemp;
import com.appul.util.DBAccess;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "MemberAddMyPictureUpload", urlPatterns = {"/MemberAddMyPictureUpload"})
@MultipartConfig
public class MemberAddMyPictureUpload extends HttpServlet {

    private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 5 * 1024 * 1024;  //5242880; // 5MB -> 5 * 1024 * 1024
    private int maxMemSize = 4 * 1024 * 1024;

    private File file;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

//        getRegistryID getId = new getRegistryID();
//        String idS = getId.getID(43);
//        int memberDocumentId = Integer.parseInt(idS);
        String strMsg = "";

        //    HttpSession session = request.getSession(false);
        //   String userNameH = session.getAttribute("username").toString();
        //   String memberIdH = session.getAttribute("memberId").toString();
        //    String sessionIdH = session.getId();
        String userNameH = "NAAA";
        String memberIdH = "1";
       // String sessionIdH = session.getId();
        String sessionIdH = "SSS";

        filePath = GlobalVariable.imageMemberUploadPath;

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        //  java.io.PrintWriter out = response.getWriter();

//        if (!isMultipart) {
//
//            strMsg = "ERROR!!! Form enctype type is not multipart/form-data";
//            response.sendRedirect(GlobalVariable.baseUrl + "/memberManagement/registration.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&strMsg=" + strMsg);
//            return;
//        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator item = fileItems.iterator();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());
            
            DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
            Date dateFileToday = new Date();
            String filePrefixToday = formatterFileToday.format(dateFileToday);

            // long uniqueNumber = System.currentTimeMillis();
            

            String fileNameNID = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

            String name = "";
            String value = "";

            //    String memberRegId = request.getParameter("memberRegId") == null ? "" : request.getParameter("memberRegId").trim();
            // String memberVerifyId = request.getParameter("memberVerifyId") == null ? "" : request.getParameter("memberVerifyId").trim();
            String memberRegId = "";
            String memberVerifyId = "";
            String documentTypeNID = "";
//            String eventVenue = "";
//            String eventDate = "";
//            String eventDesc = "";
//            String eventShortDesc = "";

            String fieldName = "";
            String fileName = "";
            String contentType = "";

            while (item.hasNext()) {
                FileItem fi = (FileItem) item.next();
                if (!fi.isFormField()) {

                    // Get the uploaded file parameters
                    fieldName = fi.getFieldName();
                    System.out.println("FileName:: " + fieldName);
                    fileName = fi.getName();
                    contentType = fi.getContentType();
                    System.out.println("FileName contentType:: " + contentType);
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    if (fileName.lastIndexOf("\\") >= 0) {
                        //  System.out.println("FileName0::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //   file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
                        file = new File(filePath + fileNameNID);
                    } else {
                        //  System.out.println("FileName::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        file = new File(filePath + fileNameNID);
                    }
                    fi.write(file);
                    System.out.println("Uploaded Filename: " + fileName + "<br>");
                    System.out.println("Uploaded Filename: " + fileNameNID + "<br>");
                } else {

                    name = fi.getFieldName();
                    value = fi.getString();
                    System.out.println("FieldName name:: " + name);
                    System.out.println("FieldName value:: " + value);
                }

                if (name.equalsIgnoreCase("memberRegId")) {
                    memberRegId = value;
                }

                if (name.equalsIgnoreCase("memberVerifyId")) {
                    memberVerifyId = value;
                }

            }

//            out.println("photoCaption: " + photoCaption + "<br>");
//            out.println("fileNameNID: " + fileNameNID + "<br>");
            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateX = new Date();
            String adddate = dateFormatX.format(dateX);

            Date adddate1 = dateFormatX.parse(adddate);

            System.out.println("adddate   adddate:: " + adddate);

            String pictureName = fileNameNID;
            
            Query myPictureSQL = dbsession.createSQLQuery("UPDATE  member_temp SET picture_name ='" + fileNameNID + "' WHERE id='" + memberVerifyId + "'");

            myPictureSQL.executeUpdate();

            dbtrx.commit();
            dbsession.flush();
            dbsession.close();

            if (dbtrx.wasCommitted()) {

                strMsg = "Picture Uploaded Successfully";

                response.sendRedirect(GlobalVariable.baseUrl + "/memberManagement/memberAddAddressInfo.jsp?sessionid=" + sessionIdH + "&regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=4&strMsg=" + strMsg);

                //   response.sendRedirect("registrationVerification.jsp?regTempId=" + member_temp_id + "&regMemId=" + regMemberId + "&regStep=2&strMsg=" + strMsg);
                //response.sendRedirect("registrationAddressInfo.jsp?regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=3&strMsg=" + strMsg);
                return;

            } else {
                dbtrx.rollback();

                strMsg = "Error!!! When Picture Upload";
                response.sendRedirect(GlobalVariable.baseUrl + "/memberManagement/memberAddMyPictureUpload.jsp?sessionid=" + sessionIdH + "&regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=3&strMsg=" + strMsg);

                return;
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }

    }
}
