/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * Author : Akter   
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.apache.log4j.Logger;

public class ClosedFinancialYear {

    public static Logger logger = Logger.getLogger("ClosedFinancialYear.class");

    public static String genTempBill(String billRef, String userId, String userStoreId) {

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        String result = "";

        String fromDate = null;
        String toDate = null;

        String sqlstr = "select FINANCIAL_YEAR,ED,TD from FINANCIAL_YEAR "
                + "where STATUS in('G')   ";
        Query qBillRef = dbsession.createSQLQuery(sqlstr);

        if (qBillRef.list().size() > 0) {
            logger.info("Another Financial Year Closing is Ongoing");
            return "AAnother Financial Year Closing is Ongoing";
        }


        /*check any other Posting ongoing*/
        sqlstr = "select FINANCIAL_YEAR,ED,TD from FINANCIAL_YEAR "
                + "where STATUS='P'  ";
        qBillRef = dbsession.createSQLQuery(sqlstr);

        if (qBillRef.list().size() > 0) {
            logger.info("Another Financial Year Closing is Ongoing");
            return "Another Financial Year Closing is Ongoing";

        }

        /* this code have to move to front end later */
        sqlstr = "select FINANCIAL_YEAR,ED,TD from FINANCIAL_YEAR "
                + "where STATUS in('O','H') and financial_year='" + billRef + "' ";
        qBillRef = dbsession.createSQLQuery(sqlstr);

        if (qBillRef.list().size() <= 0) {
            logger.info("No Financial Year Closing is due");
            return "No Financial Year Closing is due";
        }

        String feeYearId = "";
        try {
            sqlstr = "update FINANCIAL_YEAR set STATUS='G' where STATUS not in('G','P','C') and financial_year='" + billRef + "'  ";
            System.out.println(sqlstr);
            Query qUpdateBillRef = dbsession.createSQLQuery(sqlstr);
            int upcount = qUpdateBillRef.executeUpdate();

            System.out.println("upcount=======" + upcount);
            Query qFeeYear = dbsession.createSQLQuery("SELECT member_fees_year_id FROM member_fees_year where member_fees_year_name= '" + billRef + "' ");
            if (!qFeeYear.list().isEmpty()) {
                feeYearId = qFeeYear.uniqueResult().toString();
            }

            if (!(upcount == 0 || feeYearId.equalsIgnoreCase(""))) {

                sqlstr = "update FINANCIAL_YEAR set STATUS='H' where STATUS='G' and financial_year='" + billRef + "'  ";
                qUpdateBillRef = dbsession.createSQLQuery(sqlstr);
                upcount = qUpdateBillRef.executeUpdate();

                String memberId = "";
                String amount = "";
                String memberFeeId = "";
                String ed = "";
                String td = "";

                Query q1 = null;
                Query q2 = null;

                Object[] object1 = null;
                Object[] object2 = null;

                getRegistryID getId = new getRegistryID();

                InetAddress ip;
                String hostname = "";
                String ipAddress = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostName();
                    ipAddress = ip.getHostAddress();

                } catch (UnknownHostException e) {
                }

                q2 = dbsession.createSQLQuery("SELECT mt.member_id,mi.annaral_subscription_fee \n"
                        + "FROM member m,member_type mt,member_type_info mi\n"
                        + "where mt.member_type_id=mi.member_type_id\n"
                        + "and m.id=mt.member_id\n"
                        + "and now() between ed and td and mt.member_id  in (SELECT member_id FROM member_life_info where life_status=0);");
                if (!q2.list().isEmpty()) {
                    for (Iterator itr1 = q2.list().iterator(); itr1.hasNext();) {

                        object2 = (Object[]) itr1.next();
                        memberId = object2[0].toString();
                        amount = object2[1].toString();

                        Query qMemberExists = dbsession.createSQLQuery("select m.id from member m,financial_year y where m.receipt_date<td  and financial_year='" + billRef + "' and  m.id=" + memberId + "");
                        if (!qMemberExists.list().isEmpty()) {

                            Query qMemberFeeExists = dbsession.createSQLQuery("SELECT txn_id FROM member_fee  where fee_year>= '" + feeYearId + "' and member_id =" + memberId + " and bill_type=1 ");
                            if (qMemberFeeExists.list().isEmpty()) {
                                memberFeeId = getId.getID(23);
                                Query qVoucherChild6 = dbsession.createSQLQuery("insert into member_fee(txn_id,member_id,ref_no,ref_description,txn_date,fee_year,amount,status,due_date,bill_type,add_user,add_date )"
                                        + " values(" + memberFeeId + "," + memberId + ",'','',now(),'" + feeYearId + "','" + amount + "','0',date_add(now(),interval 365 day),1,'" + userId + "',now()) ");
//                        
                                qVoucherChild6.executeUpdate();
                            }
                        }

                    }

                }

                sqlstr = "update FINANCIAL_YEAR set STATUS='C' where FINANCIAL_YEAR='" + billRef + "'   ";
                qUpdateBillRef = dbsession.createSQLQuery(sqlstr);
                qUpdateBillRef.executeUpdate();

                result = "Successfully Generated";
                logger.info("Update record into FINANCIAL_YEAR set STATUS");
                dbtrx.commit();

            }

            dbsession.close();
        } catch (Exception e) {
            dbtrx.rollback();
            dbsession.close();
            e.printStackTrace();
            dbsession.close();
             result = "Bill generation failed!";
        }
        return result;

    }

}
