/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.DBAccess;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet(name = "RefereeUpload", urlPatterns = {"/RefereeUpload"})
@MultipartConfig
public class RefereeUpload extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

//            Part playerId = request.getPart("playerId");
            Part firstName = request.getPart("firstName");
            Part lastName = request.getPart("lastName");

            Part dob = request.getPart("param");
            Part countryId = request.getPart("countryId");
//            Part userId = request.getPart("username");

//            Scanner playerIdS = new Scanner(playerId.getInputStream());
            Scanner firstNameS = new Scanner(firstName.getInputStream());
            Scanner lastNameS = new Scanner(lastName.getInputStream());

            Scanner dobS = new Scanner(dob.getInputStream());
            Scanner countryIdS = new Scanner(countryId.getInputStream());
//            Scanner userIdS = new Scanner(userId.getInputStream());

//            String playerId1 = playerIdS.nextLine();
            String firstName1 = firstNameS.nextLine();
            String lastName1 = lastNameS.nextLine();

            String dob1 = dobS.nextLine();

            String countryId1 = countryIdS.nextLine();
//            String userId1 = userIdS.nextLine();

            System.out.println(firstName1 + "|" + lastName1);
            Part photo = request.getPart("file1");

            getRegistryID getId = new getRegistryID();
            String id = getId.getID(17);

            DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            Date date = new Date();
            String entryDate = dateFormat.format(date);
//            System.out.println("Entry Date="+entryDate);
            // Connect to MySql
            DBAccess dbconn = new DBAccess();
            Connection con = dbconn.dbsrc();

            PreparedStatement ps = con.prepareStatement("insert into match_referee_profile values(?,?,?,?,?,?)");

            ps.setString(1, id);
            ps.setString(2, firstName1);
            ps.setString(3, lastName1);
            // size must be converted to int otherwise it results in error
            ps.setBinaryStream(4, photo.getInputStream(), (int) photo.getSize());
            ps.setString(5, dob1);
            ps.setString(6, countryId1);
            ps.executeUpdate();
            PreparedStatement ps1 = con.prepareStatement("insert into match_referee values(?,?,?)");
            ps1.setString(1, id);
            ps1.setString(2, firstName1);
            ps1.setString(3, id);
            ps1.executeUpdate();
//            con.commit();
            con.close();
            out.println("Added Match Referee Profile Successfully.");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
