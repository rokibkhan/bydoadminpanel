/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import static com.appul.servlet.InventoryUpload.logger;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;

@WebServlet(name = "VoucherPosting", urlPatterns = {"/VoucherPosting"})

public class InventoryPosting extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();

            String fileName = request.getParameter("fileName");
            System.out.println("fileName" + fileName);
            getRegistryID getId = new getRegistryID();
//            String idS = getId.getID(5);

            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            Object[] object = null;
            Query q1 = null;
            Query q2 = null;
            Query q21 = null;
            Query q3 = null;
            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            int productId = 0;
            int productSizeId = 0;
            int quantity = 0;
            int productInventoryId = 0;
            getRegistryID regId = new getRegistryID();
            String id = "";
            String id2 = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }
            try {

                q1 = dbsession.createSQLQuery("select product_id,product_size_id,quantity from inventory_buffer where file_name='" + fileName + "' ");

                for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                    object = (Object[]) itr.next();

                    productId = Integer.parseInt(object[0].toString());
                    productSizeId = Integer.parseInt(object[1].toString());
                    quantity = Integer.parseInt(object[2].toString());
                    q21 = dbsession.createSQLQuery("select id from product_inventory where product_id=" + productId + " and product_size_id=" + productSizeId + " ");
                    if (q21.list().size() > 0) {
                        productInventoryId = Integer.parseInt(q21.uniqueResult().toString());

                        id2 = regId.getID(23);
                        q1 = dbsession.createSQLQuery("UPDATE PRODUCT_INVENTORY SET QUANTITY = QUANTITY+ " + quantity + ",MOD_DATE=now(),MOD_USER='" + userId + "',MOD_TERM='" + hostname + "',MOD_IP='" + ipAddress + "'  where product_id=" + productId + " and product_size_id=" + productSizeId + "  ");
                        q1.executeUpdate();
                        q3 = dbsession.createSQLQuery("INSERT INTO TRANSACTIONS (ID,TXN_DT,TXN_TYPE,PRODUCT_INVENTORY_ID,WAREHOUSE_ID,CONSUMER_ID,QUANTITY,RATE,DISCOUNT,AMOUNT,ADD_DATE,ADD_USER,ADD_TERM,ADD_IP) VALUES( " + id2 + ",now(),'I'," + productInventoryId + ",1," + 1 + "," + quantity + ",0,0,0,now(),'" + userId + "','" + hostname + "','" + ipAddress + "') ");
                        q3.executeUpdate();
                    } else {
                        id = regId.getID(19);
                        BigDecimal id1 = new BigDecimal(id);

                        id2 = regId.getID(23);
                        q2 = dbsession.createSQLQuery("INSERT INTO PRODUCT_INVENTORY (ID,PRODUCT_ID,PRODUCT_SIZE_ID,WAREHOUSE_ID,QUANTITY,STATUS,ADD_DATE,ADD_USER,ADD_TERM,ADD_IP) VALUES( " + id1 + "," + productId + "," + productSizeId + ",1," + quantity + ",1,now(),'" + userId + "','" + hostname + "','" + ipAddress + "') ");
                        q2.executeUpdate();
                        q3 = dbsession.createSQLQuery("INSERT INTO TRANSACTIONS (ID,TXN_DT,TXN_TYPE,PRODUCT_INVENTORY_ID,WAREHOUSE_ID,CONSUMER_ID,QUANTITY,RATE,AMOUNT,ADD_DATE,ADD_USER,ADD_TERM,ADD_IP) VALUES( " + id2 + ",now(),'I'," + id1 + ",1," + 1 + "," + quantity + ",0,0,now(),'" + userId + "','" + hostname + "','" + ipAddress + "') ");
                        q3.executeUpdate();
                    }

                }

                Query qUploadLog = dbsession.createSQLQuery("update upload_log set status='Y' where file_name = '" + fileName + "' ");
                qUploadLog.executeUpdate();

                Query qPFSheetHist = dbsession.createSQLQuery("insert into inventory_buffer_hist(file_name,product_id,product_size_id,quantity) select file_name,product_id,product_size_id,quantity from  inventory_buffer where file_name='" + fileName + "' ");
                qPFSheetHist.executeUpdate();
                Query qPFSheetBufferDel = dbsession.createSQLQuery("delete from  inventory_buffer where file_name='" + fileName + "' ");
                qPFSheetBufferDel.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
                dbtrx.rollback();
                dbsession.close();
                logger.info("Inventory psoting Failed");
                System.out.println("Inventory psoting Failed");

            } finally {

                dbsession.flush();
                dbtrx.commit();
                dbsession.close();
                logger.info("Inventory psoting completed");
                System.out.println("Inventory psoting completed");
                out.println("Inventory posting Successfully!");

            }

        } catch (Exception e) {
        } finally {
            out.close();
        }
    }
}
