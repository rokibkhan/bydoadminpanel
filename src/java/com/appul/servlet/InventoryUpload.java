/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

/**
 *
 * @author Akter
 */
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.JsonObject;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.hibernate.Query;
import org.hibernate.Session;

//@WebServlet(name = "MemberImageUpload", urlPatterns = {"/MemberImageUpload"})
//@MultipartConfig
public class InventoryUpload extends HttpServlet {

    public static Logger logger = Logger.getLogger("InventoryUpload.class");
    private static final long serialVersionUID = 1L;
    private static final String TMP_DIR_PATH = "/temp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = "/upload";
    private File destinationDir;

    public InventoryUpload() {
        super();
    }

    public void init(ServletConfig config) throws ServletException {

        super.init(config);
        String realPath = getServletContext().getRealPath(TMP_DIR_PATH);
        tmpDir = new File(realPath);
        if (!tmpDir.isDirectory()) {
            throw new ServletException(TMP_DIR_PATH + " is not a directory");
        }

        realPath = getServletContext().getRealPath(DESTINATION_DIR_PATH);
        destinationDir = new File(realPath);
        if (!destinationDir.isDirectory()) {
            throw new ServletException(DESTINATION_DIR_PATH + " is not a directory");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();

        //set content type and header attributes
        request.getSession(false);
        response.setContentType("text/html");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        String userId = request.getSession(true).getAttribute("username").toString();
//        String orderId = request.getSession(true).getAttribute("orderNo").toString();
        String orderId = "123456";

        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();

        //Set the size threshold, above which content will be stored on disk.
        fileItemFactory.setSizeThreshold(1 * 1024 * 1024 * 10); //1 MB

        //Set the temporary directory to store the uploaded files of size above threshold.
        fileItemFactory.setRepository(tmpDir);

        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
        JsonObject myObj = new JsonObject();

        String fileName = null;
        String fullName = null;
        File file = null;

        try {

            //Parse the request
            List items = uploadHandler.parseRequest(request);
            Iterator iterator = items.iterator();
            while (iterator.hasNext()) {
                FileItem item = (FileItem) iterator.next();

                //Handle Form Fields
                if (item.isFormField()) {
                    System.out.println("Field Name = " + item.getFieldName() + ", Value = " + item.getString());
                    logger.info("Field Name = " + item.getFieldName() + ", Value = " + item.getString());
                    if (item.getFieldName().trim().equalsIgnoreCase("filename")) {
                        fileName = item.getString().trim();
                    }
                } //Handle Uploaded files.
                else {
                    System.out.println("Field Name = " + item.getFieldName()
                            + ", File Name = " + item.getName()
                            + ", Content type = " + item.getContentType()
                            + ", File Size = " + item.getSize());
                    fullName = item.getName().trim();
                    logger.info("Field Name = " + item.getFieldName()
                            + ", File Name = " + item.getName()
                            + ", Content type = " + item.getContentType()
                            + ", File Size = " + item.getSize());

                    //Write file to the ultimate location.
                    file = new File(destinationDir, item.getName());
                    item.write(file);
                }

            }

            Session dbsession = null;

            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            try {
                Query q1 = dbsession.createSQLQuery("SELECT * FROM UPLOAD_LOG WHERE FILE_NAME='" + fullName + "'");
                if (q1.list().isEmpty()) {

                    int count = 0;
                    String extension = FilenameUtils.getExtension(fullName);
                    if (extension.trim().equalsIgnoreCase("xlsx")) {
                        count = processExcelFile(file, fullName, userId, orderId);
                    } else if (extension.trim().equalsIgnoreCase("xls")) {
                        //process your binary excel file
                        count = processXlslFile(file, fullName, userId, orderId);
                    }
                    if (extension.trim().equalsIgnoreCase("csv")) {
                        //process your CSV file
//                count = processExcelFile(file,fullName);
                    }

//                    myObj.addProperty("success", true);
//                    myObj.addProperty("message", count + " item(s) were processed for file " + fullName);
//                    out.println(myObj.toString());
                    out.println(count + " item(s) were processed for file " + fullName);
                    logger.info("message" + count + " item(s) were processed for file " + fullName);

                } else {

//                    myObj.addProperty("error: ", fullName + " already exists! Please try another file.");
//                    out.println(myObj.toString());
                    out.println(fullName + " already exists! Please try another file.");
                    System.out.println(fullName + " already exists! Please try another file.");
                }

            } catch (Exception e) {
                logger.info(e.getMessage());
            } finally {
                dbsession.close();
            }

        } catch (FileUploadException ex) {
            log("Error encountered while parsing the request", ex);
            myObj.addProperty("success", false);
            out.println(myObj.toString());
            logger.info("Error encountered while parsing the request", ex);
        } catch (Exception ex) {
            log("Error encountered while uploading file", ex);
            myObj.addProperty("success", false);
            out.println(myObj.toString());
            logger.info("Error encountered while uploading file", ex);
        }

        out.close();

    }

    private int processExcelFile(File file, String fileName, String userId, String orderNo) {

        int count = 0;

        Session dbsession = null;

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        String styleNo = "";

        int xxs = 0;
        int xs = 0;
        int s = 0;
        int m = 0;
        int l = 0;
        int xl = 0;
        int xxl = 0;
        int xxxl = 0;
        int xxxxl = 0;
        int quantity = 0;
        int productId = 0;

        InetAddress ip;
        String hostname = "";
        String ipAddress = "";
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            ipAddress = ip.getHostAddress();

        } catch (UnknownHostException e) {

            e.printStackTrace();
        }
        int errorCode = 0;

        try {
            // Creating Input Stream
            FileInputStream myInput = new FileInputStream(file);

            XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);

            // Get the first sheet from workbook
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);

            Iterator<Row> rowIter = mySheet.rowIterator();
            Object[] obj = null;
            while (rowIter.hasNext()) {

                XSSFRow myRow = (XSSFRow) rowIter.next();
                Iterator<Cell> cellIter = myRow.cellIterator();
                while (cellIter.hasNext()) {

                    XSSFCell myCell = (XSSFCell) cellIter.next();
//                    System.out.println("Cell column index: " + myCell.getColumnIndex());

                    if (myCell.getColumnIndex() == 0) {

                        switch (myCell.getCellType()) {
                            case HSSFCell.CELL_TYPE_STRING:

                                styleNo = myCell.getRichStringCellValue().getString();
                                break;
                            default:
//                                System.out.println("Cell Value: " + myCell.getRawValue());
//                                pfId = myCell.getRawValue();
                                break;
                        }
                    } else if (myCell.getColumnIndex() == 1) {
                        xxs = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 2) {
                        xs = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 3) {
                        s = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 4) {
                        m = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 5) {
                        l = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 6) {
                        xl = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 7) {
                        xxl = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 8) {
                        xxxl = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 9) {
                        xxxxl = (int) myCell.getNumericCellValue();
                    }

                    if (myCell.getColumnIndex() == 0 && !myCell.getStringCellValue().trim().equals("")) {
                        count++;
                        quantity = quantity + xxs + xs + s + m + l + xl + xxl + xxxl + xxxxl;
                    }

                }

                if (!styleNo.equalsIgnoreCase("")) {

                    Query q1 = dbsession.createSQLQuery("select product_id  from appuldb.product where product_code='" + styleNo + "' ");
                    if (q1.list().size() > 0) {
                        productId = Integer.valueOf(q1.uniqueResult().toString());

                        if (xxs > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",1," + xxs + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xs > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",2," + xs + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (s > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",3," + s + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (m > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",4," + m + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (l > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",5," + l + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",6," + xl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xxl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",7," + xxl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xxxl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",8," + xxxl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xxxxl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",9," + xxxxl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                    }

                }
            }
            getRegistryID getVid = new getRegistryID();
            String vId = getVid.getID(4);
            Query qUploadLog = dbsession.createSQLQuery("INSERT INTO appuldb.upload_log(FILE_NAME,REC_COUNT,STATUS,UPLOAD_TIME,USER_ID,UPLOAD_TERM,ORDER_NO,quantity) VALUES('" + fileName + "'," + count + ",'P',now(),'" + userId + "','" + ipAddress + "','" + orderNo + "'," + quantity + " )");
            qUploadLog.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Upload Failed");
            System.out.println("Upload Failed");

        } finally {

            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            logger.info("Upload Successful");
            System.out.println("Upload Successful");

        }
        return count;

    }

    private int processXlslFile(File file, String fileName, String userId, String orderNo) {

        int count = 0;

        Session dbsession = null;

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        String styleNo = "";

        int xxs = 0;
        int xs = 0;
        int s = 0;
        int m = 0;
        int l = 0;
        int xl = 0;
        int xxl = 0;
        int xxxl = 0;
        int xxxxl = 0;
        int quantity = 0;
        int productId = 0;
        InetAddress ip;
        String hostname = "";
        String ipAddress = "";
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            ipAddress = ip.getHostAddress();

        } catch (UnknownHostException e) {

            e.printStackTrace();
        }
        int errorCode = 0;

        try {
            // Creating Input Stream
            FileInputStream myInput = new FileInputStream(file);

            HSSFWorkbook myWorkBook = new HSSFWorkbook(myInput);
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator<Row> rowIter = mySheet.rowIterator();
            Object[] obj = null;
            while (rowIter.hasNext()) {

                HSSFRow myRow = (HSSFRow) rowIter.next();
                Iterator<Cell> cellIter = myRow.cellIterator();
                while (cellIter.hasNext()) {

                    HSSFCell myCell = (HSSFCell) cellIter.next();
//                    System.out.println("Cell column index: " + myCell.getColumnIndex());

                    if (myCell.getColumnIndex() == 0) {

                        switch (myCell.getCellType()) {
                            case HSSFCell.CELL_TYPE_STRING:

                                styleNo = myCell.getRichStringCellValue().getString();
                                break;
                            default:
//                                System.out.println("Cell Value: " + myCell.getRawValue());
//                                pfId = myCell.getRawValue();
                                break;
                        }
                    } else if (myCell.getColumnIndex() == 1) {
                        xxs = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 2) {
                        xs = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 3) {
                        s = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 4) {
                        m = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 5) {
                        l = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 6) {
                        xl = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 7) {
                        xxl = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 8) {
                        xxxl = (int) myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 9) {
                        xxxxl = (int) myCell.getNumericCellValue();
                    }

                    if (myCell.getColumnIndex() == 0 && !myCell.getStringCellValue().trim().equals("")) {
                        count++;
                        quantity = quantity + xxs + xs + s + m + l + xl + xxl + xxxl + xxxxl;

                    }

                }

                if (!styleNo.equalsIgnoreCase("")) {

                    Query q1 = dbsession.createSQLQuery("select product_id  from appuldb.product where product_code='" + styleNo + "' ");
                    if (q1.list().size() > 0) {
                        productId = Integer.valueOf(q1.uniqueResult().toString());

                        if (xxs > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",1," + xxs + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xs > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",2," + xs + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (s > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",3," + s + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (m > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",4," + m + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (l > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",5," + l + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",6," + xl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xxl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",7," + xxl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xxxl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",8," + xxxl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                        if (xxxxl > 0) {
                            Query qInventoryBuffer = dbsession.createSQLQuery("INSERT INTO appuldb.inventory_buffer(file_name,product_id,product_size_id,quantity) VALUES('" + fileName + "'," + productId + ",9," + xxxxl + ")");
                            qInventoryBuffer.executeUpdate();
                        }
                    }

                }

            }
            getRegistryID getVid = new getRegistryID();
            String vId = getVid.getID(4);
            Query qUploadLog = dbsession.createSQLQuery("INSERT INTO appuldb.upload_log(FILE_NAME,REC_COUNT,STATUS,UPLOAD_TIME,USER_ID,UPLOAD_TERM,ORDER_NO,quantity) VALUES('" + fileName + "'," + count + ",'P',now(),'" + userId + "','" + ipAddress + "','" + orderNo + "'," + quantity + " )");
            qUploadLog.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Upload Failed");
            System.out.println("Upload Failed");

        } finally {

            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            logger.info("Upload Successful");
            System.out.println("Upload Successful");

        }
        return count;

    }

}
