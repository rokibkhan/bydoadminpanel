/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import static jdk.nashorn.internal.objects.NativeError.getFileName;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author HP
 */
@WebServlet(name = "FileUpload", urlPatterns = {"/FileUpload"})
public class FileUpload extends HttpServlet {

    
    private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 500 * 1024;
    private int maxMemSize = 400 * 1024;
    private File file;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FileUpload</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FileUpload at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        System.out.println("process ==== "+request.getParts());
        
        try 
		{
		
                       //isMultipart = ServletFileUpload.isMultipartContent(request);	
			//logger.debug("process mobile resu  130 ");
			//String path = getServletContext().getRealPath("/img2/");
                        String path = "D:\\";
			String FileNamePrefix = "";
			//FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			Collection<Part> parts = new ArrayList<Part>();
			try {
                            
				parts = request.getParts();
                                
				System.out.println("process mobile resu  131 partsize==== "+parts.size());
			} catch (Exception e) {
				System.out.println("process mobile resu  13000 partsize==== "+e.getMessage());
				// TODO: handle exception
			}
		    
			System.out.println("2");
                        
			if(parts != null && parts.size()>0) {
                            System.out.println("3");
				Part filePart_recordFile =  request.getPart("file");
				if(filePart_recordFile != null)
				{
                                    System.out.println("4");
					String Value = (String) getFileName(filePart_recordFile);
					System.out.println("process mobile resu  131 ");
					if(Value != null)
					{
						//Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("recordFile = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						if(Value != null && !Value.equalsIgnoreCase(""))
						{					
							String FileName = FileNamePrefix + "" + "recordFile" + "" + Value;
							//appUser.profileImageFile = (FileName);
							
							uploadFile(filePart_recordFile, FileName, path);
						}
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
					        //System.out.println("process profile image "+appUser.profileImageFile);
				}
				else {
					//appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
					//System.out.println("process profile image from elseeeeee===  "+appUser.profileImageFile);
					
				}
			}
			else {
				//appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
			}
			
			
			
			//boolean result = (boolean) app_userDAO.updateWithuserName(appUser);
		
		//return result;
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return false;
    }
    
    private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}
    
    public static void uploadFile(Part filePart, String fileName, String path)
		{

		 System.out.println("fileNamefileNamefileNamefileName "+fileName+"      "+path);
		    OutputStream out = null;
		    InputStream filecontent = null;    
		    File dir=new File(path);
		    
		    if(!dir.exists())
	        {
		    	dir.mkdir();
		    	System.out.println("created directory " + path);
	        }

		    try 
			{
		        out = new FileOutputStream(new File(path + File.separator
		                + fileName));
		        filecontent = filePart.getInputStream();

		        int read = 0;
		        final byte[] bytes = new byte[1024];
		       
		        //MediaMetadataRetriever 
		        while ((read = filecontent.read(bytes)) != -1) 
				{
		            out.write(bytes, 0, read);
		        }
		      //  out.
		        System.out.println("New file " + fileName + " created at " + path);

		    }
			catch (IOException fne) 
			{
		    	System.out.println("You either did not specify a file to upload or are "
		                + "trying to upload a file to a protected or nonexistent "
		                + "location.");
		    	System.out.println("ERROR: " + fne.getMessage());
		    }
			finally 
			{
		        if (out != null) 
				{
		            try 
					{
						out.close();
					}
					catch (IOException e) 
					{
						e.printStackTrace();
					}
		        }
		        if (filecontent != null) 
				{
		            try 
					{
						filecontent.close();
					}
					catch (IOException e) 
					{
						e.printStackTrace();
					}
		        }	       
		    }		  
		}


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
