/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appul.servlet;



import com.appul.util.UserHelper;
import com.appul.util.DBAccess;
import com.appul.util.HibernateUtil;
import com.appul.entity.SyRoleAct;
import java.sql.*;
import javax.servlet.http.HttpSession;
import java.util.Iterator;
import org.hibernate.*;
/**
 *
 * @author Akter
 */
public class UserRights {

    public static RightsBean getright(RightsBean bean){
        String username = bean.getUsername().trim().toUpperCase();
        String pagename = bean.getPage().trim();
        Session dbsession=null;
        DBAccess dbsrc = new DBAccess();
        UserHelper userhelper = new UserHelper();
        bean.setRead("N");
        bean.setWrite("N");
      if (username != null && username.equals("") == false && pagename != null && pagename.equals("") == false) {
            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();
            Query q1 = null;
            SyRoleAct rights=null;
            q1=dbsession.createQuery("from SyRoleAct right where right.id.actId in(select actId from SyActivity as act where act.actScreen='"+pagename+"') and right.id.userId='"+username+"'");
            for(Iterator itr1 = q1.list().iterator(); itr1.hasNext();){
                rights=(SyRoleAct) itr1.next();
                bean.setRead(rights.getActRead());
                bean.setWrite(rights.getActWrite());
            }
            dbsession.close();
      }
       return bean;
    }

}
