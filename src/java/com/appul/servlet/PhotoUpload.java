/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.DBAccess;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "PhotoUpload", urlPatterns = {"/PhotoUpload"})
@MultipartConfig
public class PhotoUpload extends HttpServlet {

    private boolean isMultipart;
    private String filePath;
    
//    private int maxFileSize = 500 * 1024;
//    private int maxMemSize = 400 * 1024;
    
    // private int maxFileSize = 500 * 1024;
 //   private int maxMemSize = 400 * 1024;
    
    private int maxFileSize = 5 * 1024 * 1024;  //5242880; // 5MB -> 5 * 1024 * 1024
    private int maxMemSize = 4 * 1024 * 1024;
    
    private File file;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(35);
        int photoId = Integer.parseInt(idS);

        String sessionIdH = "";
        String userNameH = "";
        String strMsg = "";

        HttpSession session = request.getSession(false);
        userNameH = session.getAttribute("username").toString().toUpperCase();
        sessionIdH = session.getId();

        filePath = GlobalVariable.imagePhotoUploadPath;

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        //  java.io.PrintWriter out = response.getWriter();

        if (!isMultipart) {

            strMsg = "ERROR!!! Form enctype type is not multipart/form-data";
            response.sendRedirect(GlobalVariable.baseUrl + "/photoManagement/photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator item = fileItems.iterator();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());
            
            DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
            Date dateFileToday = new Date();
            String filePrefixToday = formatterFileToday.format(dateFileToday);

            // long uniqueNumber = System.currentTimeMillis();
            String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

            String name = "";
            String value = "";
            String pictureCategoryId = "";
            String photoCaption = "";

            String fieldName = "";
            String fileName = "";
            String contentType = "";

            while (item.hasNext()) {
                FileItem fi = (FileItem) item.next();
                if (!fi.isFormField()) {

                    // Get the uploaded file parameters
                    fieldName = fi.getFieldName();
                    System.out.println("FileName:: " + fieldName);
                    fileName = fi.getName();
                    contentType = fi.getContentType();
                    System.out.println("FileName contentType:: " + contentType);
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    if (fileName.lastIndexOf("\\") >= 0) {
                        //  System.out.println("FileName0::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //   file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
                        file = new File(filePath + fileName1);
                    } else {
                        //  System.out.println("FileName::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        file = new File(filePath + fileName1);
                    }
                    fi.write(file);
                    out.println("Uploaded Filename: " + fileName + "<br>");
                    out.println("Uploaded Filename: " + fileName1 + "<br>");
                } else {

                    name = fi.getFieldName();
                    value = fi.getString();
                    System.out.println("FileName name:: " + name);
                    System.out.println("FileName value:: " + value);
                }

                if (name.equalsIgnoreCase("pictureCategoryId")) {
                    pictureCategoryId = value;
                }
                if (name.equalsIgnoreCase("photoCaption")) {
                    photoCaption = value;
                }

            }

//            out.println("photoCaption: " + photoCaption + "<br>");
//            out.println("fileName1: " + fileName1 + "<br>");
            System.out.println("Photo userNameHAA:: " + userNameH);
            System.out.println("Photo sessionIdHAA:: " + sessionIdH);

            //  if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
            //if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {
            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateX = new Date();
            String adddate = dateFormatX.format(dateX);
            String adduser = userNameH;
            String addterm = InetAddress.getLocalHost().getHostName().toString();
            String addip = InetAddress.getLocalHost().getHostAddress().toString();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String entryDate = dateFormat.format(date);

            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();
            int pictureCategory = 1;

//            PreparedStatement ps = con.prepareStatement("INSERT INTO picture_gallery_info('id_picture','picture_category','picture_title','picture_caption','picture_orginal_name','picture_name','picture_thumb') "
//                    + "values(?,?,?,?,?,?,?)");
//
//            ps.setInt(1, photoId);
//            ps.setInt(2, pictureCategory);
//            ps.setString(3, photoCaption);
//            ps.setString(4, photoCaption);
//            ps.setString(5, fileName);
//            ps.setString(6, fileName1);
//            ps.setString(7, fileName1);
//
//            ps.executeUpdate();
//            con.commit();
//            con.close();
//            dbsession.close();
            //insert gallary
            //String pictureCategoryId = "1";
            String pictureTitle = photoCaption;
            String pictureCaption = photoCaption;
            String pictureOrginalName = fileName;
            String pictureName = fileName1;
            String pictureThumb = fileName1;
            String showingOrder = "1";

            //   String techDistrictId = request.getParameter("techDistrictId").trim();
            Query q4 = dbsession.createSQLQuery("INSERT INTO picture_gallery_info("
                    + "id_picture,"
                    + "picture_category,"
                    + "picture_title,"
                    + "picture_caption,"
                    + "picture_orginal_name,"
                    + "picture_name,"
                    + "picture_thumb,"
                    + "showing_order,"
                    + "ADD_DATE,"
                    + "ADD_USER,"
                    + "ADD_TERM,"
                    + "ADD_IP) values( "
                    + "'" + photoId + "',"
                    + "'" + pictureCategoryId + "',"
                    + "'" + pictureTitle + "',"
                    + "'" + pictureCaption + "',"
                    + "'" + pictureOrginalName + "',"
                    + "'" + pictureName + "',"
                    + "'" + pictureThumb + "',"
                    + "'" + showingOrder + "',"
                    + "'" + adddate + "',"
                    + "'" + adduser + "',"
                    + "'" + addterm + "',"
                    + "'" + addip + "')");

            q4.executeUpdate();
            dbtrx.commit();
            dbsession.flush();
            dbsession.close();

            if (dbtrx.wasCommitted()) {

                strMsg = "Photo Added Successfully";
                response.sendRedirect(GlobalVariable.baseUrl + "/photoManagement/photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                return;
            } else {
                dbtrx.rollback();

                strMsg = "Error!!! When photo add";
                response.sendRedirect(GlobalVariable.baseUrl + "/photoManagement/photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                return;

            }

            //    out.println("photoCaption: " + photoCaption + "<br>");
            //    out.println("fileName1: " + fileName1 + "<br>");
        } catch (Exception ex) {
            System.out.println(ex);
        }

        //  try {
        //      String fileUploadPath = GlobalVariable.imagePhotoUploadPath;
        //      String userId = request.getSession(true).getAttribute("username").toString();
        //     out.println("Photo Image." + userId);

        /*
            if (!ServletFileUpload.isMultipartContent(request)) {
                out.println("Nothing to upload");
                return;
            }

            FileItemFactory itemfactory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(itemfactory);
            try {
                List<fileitem> items = upload.parseRequest(request);
                for (fileitem item : items) {

                    String contentType = item.getContentType();
                    
                    out.println("Nothing to upload");
                    if (!contentType.equals("image/png")) {
                        out.println("only png format image files supported");
                        continue;
                    }
                    File uploadDir = new File(fileUploadPath);
                    File file = File.createTempFile("img", ".png", uploadDir);
                    item.write(file);

                    out.println("<br>File Saved Successfully<br>");
                }
            } catch (FileUploadException e) {

                out.println("<br>upload fail<br>");
            } catch (Exception ex) {

                out.println("<br>can't save<br>");
            }
         */
//            Part bannerTitleP = request.getPart("bannerTitle");
//            Part bannerDescP = request.getPart("bannerDesc");
//            Part bannerShowingOrderP = request.getPart("bannerShowingOrder");
//            Part bannerStatusP = request.getPart("status");
//            Part file1P = request.getPart("file1");
//
//            Scanner bannerTitleS = new Scanner(bannerTitleP.getInputStream());
//            Scanner bannerDescS = new Scanner(bannerDescP.getInputStream());
//            Scanner bannerShowingOrderS = new Scanner(bannerShowingOrderP.getInputStream());
//            Scanner bannerStatusS = new Scanner(bannerStatusP.getInputStream());
//
//            String bannerTitle = bannerTitleS.nextLine();
//            String bannerDesc = bannerDescS.nextLine();
//            String bannerShowingOrder = bannerShowingOrderS.nextLine();
//            String bannerStatus = bannerStatusS.nextLine();
//
//            getRegistryID getId = new getRegistryID();
//            String idS = getId.getID(18);
//            int bannerId = Integer.parseInt(idS);
//
//            InetAddress ip;
//            String hostname = "";
//            String ipAddress = "";
//            try {
//                ip = InetAddress.getLocalHost();
//                hostname = ip.getHostName();
//                ipAddress = ip.getHostAddress();
//
//            } catch (UnknownHostException e) {
//
//                e.printStackTrace();
//            }
//
//            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            Date date = new Date();
//            String entryDate = dateFormat.format(date);
//            Session dbsession = HibernateUtil.getSessionFactory().openSession();
//            SessionImpl sessionImpl = (SessionImpl) dbsession;
//            Connection con = sessionImpl.connection();
//
//            PreparedStatement ps = con.prepareStatement("insert into banner values(?,?,?,?,?,?)");
//
//            ps.setInt(1, bannerId);
//            ps.setString(2, bannerTitle);
//            ps.setString(3, bannerDesc);
//            ps.setString(4, bannerShowingOrder);
//            ps.setBinaryStream(5, file1P.getInputStream(), (int) file1P.getSize());
//            ps.setString(6, bannerStatus);
//
//            ps.executeUpdate();
//            con.commit();
//            con.close();
//            dbsession.close();
        //    out.println("Successfully Added Banner Image.");
        //       } catch (Exception ex) {
        //          System.out.println(ex.getMessage());
        //      } finally {
        //         out.close();
        //    }
    }
}
