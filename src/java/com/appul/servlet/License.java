package com.appul.servlet;

import com.appul.util.DateTime;
import com.appul.util.HibernateUtil;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

public class License {

    public static Logger logger = Logger.getLogger("License.java");

    public static Integer checked() throws Exception {

        int i = 0;
        String macstr = "";
        String boolString = null;
        String padstr = "0";
        String curval = "";
        int ret = 9;
        String now = DateTime.getDateTime();
    
        //Probook
//        String licvalid1 = "14:58:D0:18:11:90";
//        String licvalid0 = "18:CF:5E:3F:A5:3D";
        String licvalid1 = "";
        String licvalid0 = "";
        String licString1 = KeyValidation();
//        System.out.println("licString1===" + licString1);
//        String licvalid11 = licString1.substring(0, licString1.lastIndexOf("|")).trim();
//        String licString2 = licString1.substring(licString1.lastIndexOf("|") + 1, licString1.length()).trim();
//        String licvalid01 = licString2.substring(0, licString2.lastIndexOf("|")).trim();
//        String dateString1 = licString2.substring(licString2.lastIndexOf("|") + 1, licString2.length()).trim();

        String licvalid11 = licString1.substring(0, 44).trim();
//        String licString2 = licString1.substring(licString1.lastIndexOf("|") + 1, licString1.length()).trim();
        String licvalid01 = licString1.substring(45, 89).trim();
        String dateString1 = licString1.substring(90, 114).trim();
//        System.out.println("licvalid11===" + licvalid11);
//        System.out.println("licvalid01===" + licvalid01);
//        System.out.println("dateString===" + dateString1);

        Encryption enc = new Encryption();
        licvalid1 = enc.getDecrypt(licvalid11);
//        System.out.println("licvalid1===" + licvalid1);
        licvalid0 = enc.getDecrypt(licvalid01);
//        System.out.println("licvalid0===" + licvalid0);
        long dateString = Long.parseLong(enc.getDecrypt(dateString1));

//        System.out.println("dateString===" + dateString);

        if (Long.parseLong(now.substring(0, 8)) > dateString) {
            logger.info("Invalid License String");
            System.out.println("Invalid License String");
            return Integer.valueOf(9);
        } else {
            InetAddress inadd = InetAddress.getLocalHost();

            NetworkInterface ni = NetworkInterface.getByInetAddress(inadd);
//            System.out.println("inadd===========" + inadd);
//            System.out.println("ni===========" + ni);
            byte[] mac = null;
            try {
                mac = ni.getHardwareAddress();

                mac = ni.getHardwareAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            System.out.println("mac===========" + mac);
            if (mac == null) {
                logger.info("System Error !");
                System.out.println("System Error !");
                return Integer.valueOf(9);
            }

            for (i = 0; i < mac.length; i++) {
                curval = Integer.toHexString(mac[i]);

                if ((!curval.equals("0")) && (curval.length() > 1)) {
                    curval = curval.substring(curval.length() - 2, curval.length());
                }

                boolString = null;
                boolString = getBitArray(Integer.parseInt(curval, 16));
                boolString = lPad(boolString, padstr, 8);
                if ((macstr.length() == 2) || (macstr.length() == 5) || (macstr.length() == 8) || (macstr.length() == 11) || (macstr.length() == 14)) {
                    macstr = macstr + ":";
                }
                macstr = macstr + Integer.toHexString(Integer.parseInt(boolString.substring(0, 4), 2)) + Integer.toHexString(Integer.parseInt(boolString.substring(4, 8), 2));
            }

            macstr = macstr.toUpperCase();

//            System.out.println(macstr);

            if ((licvalid0.equals(macstr) == true) || (licvalid1.equals(macstr) == true)) {
                ret = 0;
                logger.info("License Passed !");
                System.out.println("License Passed!");
            } else {
                ret = 1;
                logger.info("Invalid License String");
                System.out.println("Invalid License String");
            }

        }
        return Integer.valueOf(ret);

    }

    private static String getBitArray(int charInt) {
        String bitString = Integer.toBinaryString(charInt);
        return bitString;
    }

    public static String lPad(String stringToPad, String padder, int size) {
        if (padder.length() == 0) {
            return stringToPad;
        }

        while ((stringToPad.length() > 0) && (stringToPad.length() < size)) {
            stringToPad = padder + stringToPad;
        }
        return stringToPad;
    }

    public static String KeyValidation() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = session.beginTransaction();
        String licString = "";

        try {

            Query qLicense = session.createSQLQuery("SELECT key_string FROM license where now() between ed and td ");
            if (!qLicense.list().isEmpty()) {
                for (Iterator itr0 = qLicense.list().iterator(); itr0.hasNext();) {

                    licString = itr0.next().toString();
//                    System.out.println(licString);
                }
            }
        } catch (Exception e) {
            logger.info(e.getMessage());

        } finally {
            dbtrx.commit();
            session.flush();
            session.close();

        }

        return licString;

    }

}
