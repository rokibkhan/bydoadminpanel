/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import com.appul.entity.SyUser;
import com.appul.entity.SyRoles;
import java.util.Iterator;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class getValue {

    public static String retValue(String input, String key) {

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        Query q1 = null;

        if (key.equals("roleid")) {
            //SyRoles roles=new SyRoles();
            q1 = dbsession.createQuery("from SyRoles roles where roles.roleId='" + input.toString() + "'");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                SyRoles roles = (SyRoles) itr1.next();
                return roles.getRoleDesc();
            }
            return "";
        }

        if (key.equals("rolename")) {
            //SyRoles roles=new SyRoles();
            q1 = dbsession.createQuery("from SyRoles roles where roles.roleDesc='" + input + "'");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                SyRoles roles = (SyRoles) itr1.next();
                return roles.getRoleId();
            }
            return "";
        }

        if (key.equals("userid")) {
            //SyRoles roles=new SyRoles();
            q1 = dbsession.createQuery("from SyUser user where user.userId='" + input.toUpperCase() + "'");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                SyUser users = (SyUser) itr1.next();
                return users.getUserName();
            }
            return "";
        }
        
        if (key.equals("username")) {
            //SyRoles roles=new SyRoles();
            q1 = dbsession.createQuery("from SyUser user where user.userName='" + input.toUpperCase() + "'");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                SyUser users = (SyUser) itr1.next();
                return users.getUserId();
            }
            return "";
        }
        dbsession.close();
        return "";
    }
}
