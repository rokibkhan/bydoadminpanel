/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import com.appul.util.UserHelper;

import java.sql.SQLException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Akter
 */
public class UserCheck {

    public static UserBean login(UserBean bean) throws SQLException, Exception {
        String username = bean.getUsername().toUpperCase();
        String clearpass = bean.getPassword();
        String correctpass = "";
        String correctpass1 = "";
        String givenpass = null;
        String givenpass1 = null;
        int storeId = 0;

        Session dbsession = null;
        UserHelper userhelper = new UserHelper();
        License lic = new License();
    //   int licRet = lic.checked();
              int licRet = 0;
//        System.out.println("licRet======"+licRet);
        if (licRet == 0) {
            if (username != null && username.equals("") == false && clearpass != null) {
                correctpass1 = userhelper.getUserPass(username);
//            System.out.println("Encrypted DB Password :" + correctpass1);
                if (!correctpass1.equalsIgnoreCase("123")) {
                    correctpass = Encryption.getDecrypt(correctpass1.trim());

                    givenpass1 = Encryption.getEncrypt(clearpass.trim());

                    /*                System.out.println("Entered Password clearpass: " + clearpass);
                System.out.println("Entered Password clearpass: " + givenpass1);
                System.out.println("DEC DB Password correctpass:" + correctpass);

                     */
                    if (clearpass.length() > 0) {

                        givenpass = clearpass.trim();
                    } else {
                        givenpass = "";
                    }

                    if ((correctpass != null) && (correctpass.equals("") == false) && (correctpass.equals(clearpass))) {
                        bean.setValid(true);

                        dbsession = HibernateUtil.getSessionFactory().openSession();
                        org.hibernate.Transaction tx = null;
                        tx = dbsession.beginTransaction();
                        try {

                            Query q = dbsession.createSQLQuery("select store_id from store_user where user_id = '" + username + "'");

                            if (!q.list().isEmpty()) {
                                storeId = Integer.parseInt(q.uniqueResult().toString());
                            } else {
                                System.out.println("Do not Get Store User ID of  " + username);
                            }

                        } catch (Exception e) {
                            System.out.println("Do not Get Store User ID of  " + username);

                        }

                        tx.commit();
                        dbsession.flush();
                        dbsession.close();

                        bean.setStoreId(storeId);

                    } else {
                        bean.setValid(false);

                    }
                }
            } else {
                bean.setValid(false);
            }
        } else {
            bean.setValid(false);
            System.out.println("License Validation Failed");
        }

        return bean;
    }

}
