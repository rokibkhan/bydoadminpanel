/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.client.utils.URIBuilder;

/**
 *
 * @author Akter
 */
public class SendSMS {

    public static Logger logger = Logger.getLogger("SendSMS");
    private static String USER_AGENT = "Mozilla/5.0";

    @SuppressWarnings("empty-statement")
    public static String sendSMS(String msg, String receipentMobileNo) throws IOException, URISyntaxException {

//        String urlString = "http://www.bangladeshsms.com/smsapi?api_key=C20013475bbdd6cbdd2ac1.58430946&type=text&senderid=8804445629730&msg=test&contacts=+8801755569701";
        String sendUrlHost = GlobalVariable.sendUrlHost;
        String sendUrlApi = GlobalVariable.sendUrlApi;
        String apiKey = GlobalVariable.apiKey;
        String type = GlobalVariable.type;
        String senderid = GlobalVariable.senderid;
        String ret = "";

        try {

            URIBuilder builder = new URIBuilder()
                    .setScheme("http")
                    .setHost(sendUrlHost)
                    .setPath(sendUrlApi)
                    .addParameter("api_key", apiKey)
                    .addParameter("contacts", receipentMobileNo)
                    .addParameter("senderid", senderid)
                    .addParameter("type", type)
                    .addParameter("msg", msg);

            URL url = builder.build().toURL();

            try {
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("HEAD");
                // optional default is GET
                con.setRequestMethod("GET");
                con.setConnectTimeout(5000); //set timeout to 5 seconds
                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);
                if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'GET' request to URL : " + url);
                    System.out.println("Response Code : " + responseCode);
                    logger.info("Response Code : " + responseCode);

                    BufferedReader in;
                    in = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    ret = in.readLine();
                    in.close();

                    logger.info("Response from SMS Server : " + ret);
                    System.out.println("Response from SMS Server  : " + ret);
                    return ret;
                } else {
                    return "sending failed!";
                }

            } catch (java.net.SocketTimeoutException e) {
                return "Exception or server unreachable";
            } catch (java.io.IOException e) {
                return "Exception or server unreachable";
            }

        } catch (IOException | URISyntaxException e) {
            return "Exception or server unreachable";
        }

    }

//    public static void main(String args[]) throws IOException, URISyntaxException {
//        String sms = "This is test Message";
//        String receipentMobileNo = "+8801955538279";
//        System.out.println("SMS : " + sms);
//        System.out.println("receipentMobileNo : " + receipentMobileNo);
//        System.out.println(SendSMS(sms, receipentMobileNo));
//    }
    
    
    public static String pushNotification(String msg, String receipentMobileNo) throws IOException, URISyntaxException {

//        String urlString = "http://www.bangladeshsms.com/smsapi?api_key=C20013475bbdd6cbdd2ac1.58430946&type=text&senderid=8804445629730&msg=test&contacts=+8801755569701";
        String sendUrlHost = GlobalVariable.sendUrlHost;
        String sendUrlApi = GlobalVariable.sendUrlApi;
        String apiKey = GlobalVariable.apiKey;
        String type = GlobalVariable.type;
        String senderid = GlobalVariable.senderid;
        String ret = "";

        try {

            URIBuilder builder = new URIBuilder()
                    .setScheme("http")
                    .setHost(sendUrlHost)
                    .setPath(sendUrlApi)
                    .addParameter("api_key", apiKey)
                    .addParameter("contacts", receipentMobileNo)
                    .addParameter("senderid", senderid)
                    .addParameter("type", type)
                    .addParameter("msg", msg);

            URL url = builder.build().toURL();

            try {
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("HEAD");
                // optional default is GET
                con.setRequestMethod("GET");
                con.setConnectTimeout(5000); //set timeout to 5 seconds
                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);
                if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'GET' request to URL : " + url);
                    System.out.println("Response Code : " + responseCode);
                    logger.info("Response Code : " + responseCode);

                    BufferedReader in;
                    in = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    ret = in.readLine();
                    in.close();

                    logger.info("Response from SMS Server : " + ret);
                    System.out.println("Response from SMS Server  : " + ret);
                    return ret;
                } else {
                    return "sending failed!";
                }

            } catch (java.net.SocketTimeoutException e) {
                return "Exception or server unreachable";
            } catch (java.io.IOException e) {
                return "Exception or server unreachable";
            }

        } catch (IOException | URISyntaxException e) {
            return "Exception or server unreachable";
        }

    }
}
