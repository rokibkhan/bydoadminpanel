package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * MemberPasscodeCheck generated by hbm2java
 */
public class MemberPasscodeCheck  implements java.io.Serializable {


     private int id;
     private Member member;
     private String mobileCode;
     private byte mobileCodeChecked;
     private String emailCode;
     private byte emailCodeChecked;
     private String addUser;
     private Date addDate;
     private String addIp;
     private String addTerm;
     private String modUser;
     private Date modDate;
     private String modIp;
     private String modTerm;

    public MemberPasscodeCheck() {
    }

	
    public MemberPasscodeCheck(int id, Member member, byte mobileCodeChecked, byte emailCodeChecked) {
        this.id = id;
        this.member = member;
        this.mobileCodeChecked = mobileCodeChecked;
        this.emailCodeChecked = emailCodeChecked;
    }
    public MemberPasscodeCheck(int id, Member member, String mobileCode, byte mobileCodeChecked, String emailCode, byte emailCodeChecked, String addUser, Date addDate, String addIp, String addTerm, String modUser, Date modDate, String modIp, String modTerm) {
       this.id = id;
       this.member = member;
       this.mobileCode = mobileCode;
       this.mobileCodeChecked = mobileCodeChecked;
       this.emailCode = emailCode;
       this.emailCodeChecked = emailCodeChecked;
       this.addUser = addUser;
       this.addDate = addDate;
       this.addIp = addIp;
       this.addTerm = addTerm;
       this.modUser = modUser;
       this.modDate = modDate;
       this.modIp = modIp;
       this.modTerm = modTerm;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Member getMember() {
        return this.member;
    }
    
    public void setMember(Member member) {
        this.member = member;
    }
    public String getMobileCode() {
        return this.mobileCode;
    }
    
    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }
    public byte getMobileCodeChecked() {
        return this.mobileCodeChecked;
    }
    
    public void setMobileCodeChecked(byte mobileCodeChecked) {
        this.mobileCodeChecked = mobileCodeChecked;
    }
    public String getEmailCode() {
        return this.emailCode;
    }
    
    public void setEmailCode(String emailCode) {
        this.emailCode = emailCode;
    }
    public byte getEmailCodeChecked() {
        return this.emailCodeChecked;
    }
    
    public void setEmailCodeChecked(byte emailCodeChecked) {
        this.emailCodeChecked = emailCodeChecked;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public Date getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }




}


