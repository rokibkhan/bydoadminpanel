package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1



/**
 * VideoSlug generated by hbm2java
 */
public class VideoSlug  implements java.io.Serializable {


     private int id;
     private int videoId;
     private int slugId;

    public VideoSlug() {
    }

    public VideoSlug(int id, int videoId, int slugId) {
       this.id = id;
       this.videoId = videoId;
       this.slugId = slugId;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public int getVideoId() {
        return this.videoId;
    }
    
    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }
    public int getSlugId() {
        return this.slugId;
    }
    
    public void setSlugId(int slugId) {
        this.slugId = slugId;
    }




}


