package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * SyModule generated by hbm2java
 */
public class SyModule  implements java.io.Serializable {


     private int id;
     private String name;
     private String desc;
     private String status;
     private Date ed;
     private Date td;
     private Date addDate;
     private String addUser;
     private String addTerm;
     private String addIp;
     private Date modDate;
     private String modUser;
     private String modTerm;
     private String modIp;

    public SyModule() {
    }

	
    public SyModule(int id, String name, String desc, String status, Date ed, Date td) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.status = status;
        this.ed = ed;
        this.td = td;
    }
    public SyModule(int id, String name, String desc, String status, Date ed, Date td, Date addDate, String addUser, String addTerm, String addIp, Date modDate, String modUser, String modTerm, String modIp) {
       this.id = id;
       this.name = name;
       this.desc = desc;
       this.status = status;
       this.ed = ed;
       this.td = td;
       this.addDate = addDate;
       this.addUser = addUser;
       this.addTerm = addTerm;
       this.addIp = addIp;
       this.modDate = modDate;
       this.modUser = modUser;
       this.modTerm = modTerm;
       this.modIp = modIp;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getDesc() {
        return this.desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    public Date getEd() {
        return this.ed;
    }
    
    public void setEd(Date ed) {
        this.ed = ed;
    }
    public Date getTd() {
        return this.td;
    }
    
    public void setTd(Date td) {
        this.td = td;
    }
    public Date getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }




}


