package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1



/**
 * AlbumSlug generated by hbm2java
 */
public class AlbumSlug  implements java.io.Serializable {


     private int id;
     private IebSlug iebSlug;
     private PictureCategory pictureCategory;

    public AlbumSlug() {
    }

    public AlbumSlug(int id, IebSlug iebSlug, PictureCategory pictureCategory) {
       this.id = id;
       this.iebSlug = iebSlug;
       this.pictureCategory = pictureCategory;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public IebSlug getIebSlug() {
        return this.iebSlug;
    }
    
    public void setIebSlug(IebSlug iebSlug) {
        this.iebSlug = iebSlug;
    }
    public PictureCategory getPictureCategory() {
        return this.pictureCategory;
    }
    
    public void setPictureCategory(PictureCategory pictureCategory) {
        this.pictureCategory = pictureCategory;
    }




}


