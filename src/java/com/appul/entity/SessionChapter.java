package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * SessionChapter generated by hbm2java
 */
public class SessionChapter  implements java.io.Serializable {


     private int id;
     private ChapterInfo chapterInfo;
     private SessionInfo sessionInfo;
     private Byte status;
     private String addUser;
     private Date addDate;
     private String addIp;
     private String addTerm;
     private String modUser;
     private Date modDate;
     private String modIp;
     private String modTerm;

    public SessionChapter() {
    }

	
    public SessionChapter(int id, ChapterInfo chapterInfo, SessionInfo sessionInfo) {
        this.id = id;
        this.chapterInfo = chapterInfo;
        this.sessionInfo = sessionInfo;
    }
    public SessionChapter(int id, ChapterInfo chapterInfo, SessionInfo sessionInfo, Byte status, String addUser, Date addDate, String addIp, String addTerm, String modUser, Date modDate, String modIp, String modTerm) {
       this.id = id;
       this.chapterInfo = chapterInfo;
       this.sessionInfo = sessionInfo;
       this.status = status;
       this.addUser = addUser;
       this.addDate = addDate;
       this.addIp = addIp;
       this.addTerm = addTerm;
       this.modUser = modUser;
       this.modDate = modDate;
       this.modIp = modIp;
       this.modTerm = modTerm;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public ChapterInfo getChapterInfo() {
        return this.chapterInfo;
    }
    
    public void setChapterInfo(ChapterInfo chapterInfo) {
        this.chapterInfo = chapterInfo;
    }
    public SessionInfo getSessionInfo() {
        return this.sessionInfo;
    }
    
    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }
    public Byte getStatus() {
        return this.status;
    }
    
    public void setStatus(Byte status) {
        this.status = status;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public Date getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }




}


