package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1



/**
 * PageInfo generated by hbm2java
 */
public class PageInfo  implements java.io.Serializable {


     private Integer idPage;
     private PageCategory pageCategory;
     private String pageTitle;
     private String pageShortDesc;
     private String pageDesc;
     private Byte published;
     private String addUser;
     private String addDate;
     private String addTerm;
     private String addIp;
     private String modUser;
     private String modDate;
     private String modTerm;
     private String modIp;
     private String orderby;

    public PageInfo() {
    }

	
    public PageInfo(String pageTitle, String pageShortDesc) {
        this.pageTitle = pageTitle;
        this.pageShortDesc = pageShortDesc;
    }
    public PageInfo(PageCategory pageCategory, String pageTitle, String pageShortDesc, String pageDesc, Byte published, String addUser, String addDate, String addTerm, String addIp, String modUser, String modDate, String modTerm, String modIp, String orderby) {
       this.pageCategory = pageCategory;
       this.pageTitle = pageTitle;
       this.pageShortDesc = pageShortDesc;
       this.pageDesc = pageDesc;
       this.published = published;
       this.addUser = addUser;
       this.addDate = addDate;
       this.addTerm = addTerm;
       this.addIp = addIp;
       this.modUser = modUser;
       this.modDate = modDate;
       this.modTerm = modTerm;
       this.modIp = modIp;
       this.orderby = orderby;
    }
   
    public Integer getIdPage() {
        return this.idPage;
    }
    
    public void setIdPage(Integer idPage) {
        this.idPage = idPage;
    }
    public PageCategory getPageCategory() {
        return this.pageCategory;
    }
    
    public void setPageCategory(PageCategory pageCategory) {
        this.pageCategory = pageCategory;
    }
    public String getPageTitle() {
        return this.pageTitle;
    }
    
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    public String getPageShortDesc() {
        return this.pageShortDesc;
    }
    
    public void setPageShortDesc(String pageShortDesc) {
        this.pageShortDesc = pageShortDesc;
    }
    public String getPageDesc() {
        return this.pageDesc;
    }
    
    public void setPageDesc(String pageDesc) {
        this.pageDesc = pageDesc;
    }
    public Byte getPublished() {
        return this.published;
    }
    
    public void setPublished(Byte published) {
        this.published = published;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public String getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    public String getModDate() {
        return this.modDate;
    }
    
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }
    public String getOrderby() {
        return this.orderby;
    }
    
    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }




}


