package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1



/**
 * MediaInfo generated by hbm2java
 */
public class MediaInfo  implements java.io.Serializable {


     private int idMedia;
     private MediaCategory mediaCategory;
     private String mediaTitle;
     private String mediaCaption;
     private String mediaOrginalName;
     private String mediaName;
     private String mediaThumb;
     private Integer showingOrder;
     private String featureImage;
     private String image1;
     private String image2;
     private String image3;
     private String image4;
     private Byte published;
     private String addUser;
     private String addDate;
     private String addTerm;
     private String addIp;
     private String modUser;
     private String modDate;
     private String modTerm;
     private String modIp;

    public MediaInfo() {
    }

	
    public MediaInfo(int idMedia, MediaCategory mediaCategory, String mediaTitle) {
        this.idMedia = idMedia;
        this.mediaCategory = mediaCategory;
        this.mediaTitle = mediaTitle;
    }
    public MediaInfo(int idMedia, MediaCategory mediaCategory, String mediaTitle, String mediaCaption, String mediaOrginalName, String mediaName, String mediaThumb, Integer showingOrder, String featureImage, String image1, String image2, String image3, String image4, Byte published, String addUser, String addDate, String addTerm, String addIp, String modUser, String modDate, String modTerm, String modIp) {
       this.idMedia = idMedia;
       this.mediaCategory = mediaCategory;
       this.mediaTitle = mediaTitle;
       this.mediaCaption = mediaCaption;
       this.mediaOrginalName = mediaOrginalName;
       this.mediaName = mediaName;
       this.mediaThumb = mediaThumb;
       this.showingOrder = showingOrder;
       this.featureImage = featureImage;
       this.image1 = image1;
       this.image2 = image2;
       this.image3 = image3;
       this.image4 = image4;
       this.published = published;
       this.addUser = addUser;
       this.addDate = addDate;
       this.addTerm = addTerm;
       this.addIp = addIp;
       this.modUser = modUser;
       this.modDate = modDate;
       this.modTerm = modTerm;
       this.modIp = modIp;
    }
   
    public int getIdMedia() {
        return this.idMedia;
    }
    
    public void setIdMedia(int idMedia) {
        this.idMedia = idMedia;
    }
    public MediaCategory getMediaCategory() {
        return this.mediaCategory;
    }
    
    public void setMediaCategory(MediaCategory mediaCategory) {
        this.mediaCategory = mediaCategory;
    }
    public String getMediaTitle() {
        return this.mediaTitle;
    }
    
    public void setMediaTitle(String mediaTitle) {
        this.mediaTitle = mediaTitle;
    }
    public String getMediaCaption() {
        return this.mediaCaption;
    }
    
    public void setMediaCaption(String mediaCaption) {
        this.mediaCaption = mediaCaption;
    }
    public String getMediaOrginalName() {
        return this.mediaOrginalName;
    }
    
    public void setMediaOrginalName(String mediaOrginalName) {
        this.mediaOrginalName = mediaOrginalName;
    }
    public String getMediaName() {
        return this.mediaName;
    }
    
    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }
    public String getMediaThumb() {
        return this.mediaThumb;
    }
    
    public void setMediaThumb(String mediaThumb) {
        this.mediaThumb = mediaThumb;
    }
    public Integer getShowingOrder() {
        return this.showingOrder;
    }
    
    public void setShowingOrder(Integer showingOrder) {
        this.showingOrder = showingOrder;
    }
    public String getFeatureImage() {
        return this.featureImage;
    }
    
    public void setFeatureImage(String featureImage) {
        this.featureImage = featureImage;
    }
    public String getImage1() {
        return this.image1;
    }
    
    public void setImage1(String image1) {
        this.image1 = image1;
    }
    public String getImage2() {
        return this.image2;
    }
    
    public void setImage2(String image2) {
        this.image2 = image2;
    }
    public String getImage3() {
        return this.image3;
    }
    
    public void setImage3(String image3) {
        this.image3 = image3;
    }
    public String getImage4() {
        return this.image4;
    }
    
    public void setImage4(String image4) {
        this.image4 = image4;
    }
    public Byte getPublished() {
        return this.published;
    }
    
    public void setPublished(Byte published) {
        this.published = published;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public String getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    public String getModDate() {
        return this.modDate;
    }
    
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }




}


