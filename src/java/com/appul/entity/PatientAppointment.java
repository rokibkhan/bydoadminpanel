package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * PatientAppointment generated by hbm2java
 */
public class PatientAppointment  implements java.io.Serializable {


     private int id;
     private DoctorTimeslot doctorTimeslot;
     private Member member;
     private PatientInfo patientInfo;
     private Integer pqueryId;
     private String appointmentTime;
     private String appointmentMsg;
     private String doctorMsg;
     private byte status;
     private String addUser;
     private Date addDate;
     private String addTerm;
     private String addIp;
     private String modUser;
     private Date modDate;
     private String modTerm;
     private String modIp;

    public PatientAppointment() {
    }

	
    public PatientAppointment(int id, Member member, PatientInfo patientInfo, byte status) {
        this.id = id;
        this.member = member;
        this.patientInfo = patientInfo;
        this.status = status;
    }
    public PatientAppointment(int id, DoctorTimeslot doctorTimeslot, Member member, PatientInfo patientInfo, Integer pqueryId, String appointmentTime, String appointmentMsg, String doctorMsg, byte status, String addUser, Date addDate, String addTerm, String addIp, String modUser, Date modDate, String modTerm, String modIp) {
       this.id = id;
       this.doctorTimeslot = doctorTimeslot;
       this.member = member;
       this.patientInfo = patientInfo;
       this.pqueryId = pqueryId;
       this.appointmentTime = appointmentTime;
       this.appointmentMsg = appointmentMsg;
       this.doctorMsg = doctorMsg;
       this.status = status;
       this.addUser = addUser;
       this.addDate = addDate;
       this.addTerm = addTerm;
       this.addIp = addIp;
       this.modUser = modUser;
       this.modDate = modDate;
       this.modTerm = modTerm;
       this.modIp = modIp;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public DoctorTimeslot getDoctorTimeslot() {
        return this.doctorTimeslot;
    }
    
    public void setDoctorTimeslot(DoctorTimeslot doctorTimeslot) {
        this.doctorTimeslot = doctorTimeslot;
    }
    public Member getMember() {
        return this.member;
    }
    
    public void setMember(Member member) {
        this.member = member;
    }
    public PatientInfo getPatientInfo() {
        return this.patientInfo;
    }
    
    public void setPatientInfo(PatientInfo patientInfo) {
        this.patientInfo = patientInfo;
    }
    public Integer getPqueryId() {
        return this.pqueryId;
    }
    
    public void setPqueryId(Integer pqueryId) {
        this.pqueryId = pqueryId;
    }
    public String getAppointmentTime() {
        return this.appointmentTime;
    }
    
    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }
    public String getAppointmentMsg() {
        return this.appointmentMsg;
    }
    
    public void setAppointmentMsg(String appointmentMsg) {
        this.appointmentMsg = appointmentMsg;
    }
    public String getDoctorMsg() {
        return this.doctorMsg;
    }
    
    public void setDoctorMsg(String doctorMsg) {
        this.doctorMsg = doctorMsg;
    }
    public byte getStatus() {
        return this.status;
    }
    
    public void setStatus(byte status) {
        this.status = status;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public Date getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }




}


