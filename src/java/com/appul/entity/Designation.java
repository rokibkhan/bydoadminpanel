package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1



/**
 * Designation generated by hbm2java
 */
public class Designation  implements java.io.Serializable {


     private int id;
     private String designationName;
     private String shortName;
     private int orderby;

    public Designation() {
    }

	
    public Designation(int id, String shortName, int orderby) {
        this.id = id;
        this.shortName = shortName;
        this.orderby = orderby;
    }
    public Designation(int id, String designationName, String shortName, int orderby) {
       this.id = id;
       this.designationName = designationName;
       this.shortName = shortName;
       this.orderby = orderby;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getDesignationName() {
        return this.designationName;
    }
    
    public void setDesignationName(String designationName) {
        this.designationName = designationName;
    }
    public String getShortName() {
        return this.shortName;
    }
    
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    public int getOrderby() {
        return this.orderby;
    }
    
    public void setOrderby(int orderby) {
        this.orderby = orderby;
    }




}


