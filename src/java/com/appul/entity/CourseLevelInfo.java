package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * CourseLevelInfo generated by hbm2java
 */
public class CourseLevelInfo  implements java.io.Serializable {


     private int levelId;
     private String levelShotrname;
     private String levelLongname;
     private String levelDetails;
     private Set courseInfos = new HashSet(0);

    public CourseLevelInfo() {
    }

	
    public CourseLevelInfo(int levelId) {
        this.levelId = levelId;
    }
    public CourseLevelInfo(int levelId, String levelShotrname, String levelLongname, String levelDetails, Set courseInfos) {
       this.levelId = levelId;
       this.levelShotrname = levelShotrname;
       this.levelLongname = levelLongname;
       this.levelDetails = levelDetails;
       this.courseInfos = courseInfos;
    }
   
    public int getLevelId() {
        return this.levelId;
    }
    
    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }
    public String getLevelShotrname() {
        return this.levelShotrname;
    }
    
    public void setLevelShotrname(String levelShotrname) {
        this.levelShotrname = levelShotrname;
    }
    public String getLevelLongname() {
        return this.levelLongname;
    }
    
    public void setLevelLongname(String levelLongname) {
        this.levelLongname = levelLongname;
    }
    public String getLevelDetails() {
        return this.levelDetails;
    }
    
    public void setLevelDetails(String levelDetails) {
        this.levelDetails = levelDetails;
    }
    public Set getCourseInfos() {
        return this.courseInfos;
    }
    
    public void setCourseInfos(Set courseInfos) {
        this.courseInfos = courseInfos;
    }




}


