package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * ClassComment generated by hbm2java
 */
public class ClassComment  implements java.io.Serializable {


     private int id;
     private int parent;
     private String commmentDesc;
     private int classId;
     private int commmentFrom;
     private Date commmentDate;
     private byte published;
     private String addDate;
     private String addIp;
     private String addTerm;
     private String addUser;
     private String modDate;
     private String modIp;
     private String modTerm;
     private String modUser;

    public ClassComment() {
    }

	
    public ClassComment(int id, int parent, int classId, int commmentFrom, byte published) {
        this.id = id;
        this.parent = parent;
        this.classId = classId;
        this.commmentFrom = commmentFrom;
        this.published = published;
    }
    public ClassComment(int id, int parent, String commmentDesc, int classId, int commmentFrom, Date commmentDate, byte published, String addDate, String addIp, String addTerm, String addUser, String modDate, String modIp, String modTerm, String modUser) {
       this.id = id;
       this.parent = parent;
       this.commmentDesc = commmentDesc;
       this.classId = classId;
       this.commmentFrom = commmentFrom;
       this.commmentDate = commmentDate;
       this.published = published;
       this.addDate = addDate;
       this.addIp = addIp;
       this.addTerm = addTerm;
       this.addUser = addUser;
       this.modDate = modDate;
       this.modIp = modIp;
       this.modTerm = modTerm;
       this.modUser = modUser;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public int getParent() {
        return this.parent;
    }
    
    public void setParent(int parent) {
        this.parent = parent;
    }
    public String getCommmentDesc() {
        return this.commmentDesc;
    }
    
    public void setCommmentDesc(String commmentDesc) {
        this.commmentDesc = commmentDesc;
    }
    public int getClassId() {
        return this.classId;
    }
    
    public void setClassId(int classId) {
        this.classId = classId;
    }
    public int getCommmentFrom() {
        return this.commmentFrom;
    }
    
    public void setCommmentFrom(int commmentFrom) {
        this.commmentFrom = commmentFrom;
    }
    public Date getCommmentDate() {
        return this.commmentDate;
    }
    
    public void setCommmentDate(Date commmentDate) {
        this.commmentDate = commmentDate;
    }
    public byte getPublished() {
        return this.published;
    }
    
    public void setPublished(byte published) {
        this.published = published;
    }
    public String getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public String getModDate() {
        return this.modDate;
    }
    
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }




}


