package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1



/**
 * DoctorInfo generated by hbm2java
 */
public class DoctorInfo  implements java.io.Serializable {


     private int id;
     private Member member;
     private String weekStartDay;
     private String weekEndDay;
     private String dailyStartTime;
     private String dailyEndTime;
     private String weekOffDay;
     private Byte status;
     private String addUser;
     private String addDate;
     private String addTerm;
     private String addIp;
     private String modUser;
     private String modDate;
     private String modTerm;
     private String modIp;

    public DoctorInfo() {
    }

	
    public DoctorInfo(int id, Member member) {
        this.id = id;
        this.member = member;
    }
    public DoctorInfo(int id, Member member, String weekStartDay, String weekEndDay, String dailyStartTime, String dailyEndTime, String weekOffDay, Byte status, String addUser, String addDate, String addTerm, String addIp, String modUser, String modDate, String modTerm, String modIp) {
       this.id = id;
       this.member = member;
       this.weekStartDay = weekStartDay;
       this.weekEndDay = weekEndDay;
       this.dailyStartTime = dailyStartTime;
       this.dailyEndTime = dailyEndTime;
       this.weekOffDay = weekOffDay;
       this.status = status;
       this.addUser = addUser;
       this.addDate = addDate;
       this.addTerm = addTerm;
       this.addIp = addIp;
       this.modUser = modUser;
       this.modDate = modDate;
       this.modTerm = modTerm;
       this.modIp = modIp;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Member getMember() {
        return this.member;
    }
    
    public void setMember(Member member) {
        this.member = member;
    }
    public String getWeekStartDay() {
        return this.weekStartDay;
    }
    
    public void setWeekStartDay(String weekStartDay) {
        this.weekStartDay = weekStartDay;
    }
    public String getWeekEndDay() {
        return this.weekEndDay;
    }
    
    public void setWeekEndDay(String weekEndDay) {
        this.weekEndDay = weekEndDay;
    }
    public String getDailyStartTime() {
        return this.dailyStartTime;
    }
    
    public void setDailyStartTime(String dailyStartTime) {
        this.dailyStartTime = dailyStartTime;
    }
    public String getDailyEndTime() {
        return this.dailyEndTime;
    }
    
    public void setDailyEndTime(String dailyEndTime) {
        this.dailyEndTime = dailyEndTime;
    }
    public String getWeekOffDay() {
        return this.weekOffDay;
    }
    
    public void setWeekOffDay(String weekOffDay) {
        this.weekOffDay = weekOffDay;
    }
    public Byte getStatus() {
        return this.status;
    }
    
    public void setStatus(Byte status) {
        this.status = status;
    }
    public String getAddUser() {
        return this.addUser;
    }
    
    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }
    public String getAddDate() {
        return this.addDate;
    }
    
    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }
    public String getAddTerm() {
        return this.addTerm;
    }
    
    public void setAddTerm(String addTerm) {
        this.addTerm = addTerm;
    }
    public String getAddIp() {
        return this.addIp;
    }
    
    public void setAddIp(String addIp) {
        this.addIp = addIp;
    }
    public String getModUser() {
        return this.modUser;
    }
    
    public void setModUser(String modUser) {
        this.modUser = modUser;
    }
    public String getModDate() {
        return this.modDate;
    }
    
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }
    public String getModTerm() {
        return this.modTerm;
    }
    
    public void setModTerm(String modTerm) {
        this.modTerm = modTerm;
    }
    public String getModIp() {
        return this.modIp;
    }
    
    public void setModIp(String modIp) {
        this.modIp = modIp;
    }




}


