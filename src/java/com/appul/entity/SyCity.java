package com.appul.entity;
// Generated Dec 4, 2020 11:36:34 AM by Hibernate Tools 4.3.1



/**
 * SyCity generated by hbm2java
 */
public class SyCity  implements java.io.Serializable {


     private String cityCode;
     private String cityName;
     private String countryCode;

    public SyCity() {
    }

	
    public SyCity(String cityCode) {
        this.cityCode = cityCode;
    }
    public SyCity(String cityCode, String cityName, String countryCode) {
       this.cityCode = cityCode;
       this.cityName = cityName;
       this.countryCode = countryCode;
    }
   
    public String getCityCode() {
        return this.cityCode;
    }
    
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public String getCityName() {
        return this.cityName;
    }
    
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public String getCountryCode() {
        return this.countryCode;
    }
    
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }




}


