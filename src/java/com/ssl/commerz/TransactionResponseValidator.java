package com.ssl.commerz;

import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import java.util.Iterator;
import java.util.Map;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * This class handles the Response parameters redirected from payment success
 * page. Validates those parameters fetched from payment page response and
 * returns true for successful transaction and false otherwise.
 */
public class TransactionResponseValidator {

    /**
     *
     * @param request
     * @return
     * @throws Exception Send Received params from your success resoponse (POST
     * ) in this Map</>
     */
    public boolean receiveSuccessResponse(Map<String, String> request) throws Exception {

        String trxId = request.get("tran_id");
        System.out.println("TRVClasaa :: trxId::" + trxId);
        /**
         * Get your AMOUNT and Currency FROM DB to initiate this Transaction
         */
        // String amount = "150";
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        String amount = "";
        String currency = "BDT";
        Query feeSQL = null;
        Object[] feeObject = null;

        String memberShipFeeTransId = "";
        String memberShipFeeMemberId = "";
        String memberShipFeePaidDate = "";
        String memberShipFeeAmount = "";
        String memberShipFeeCurrency = "BDT";

        feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + trxId + "");

        System.out.println("TRVClasaa :: feeSQL::" + feeSQL);
        if (!feeSQL.list().isEmpty()) {
            for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                feeObject = (Object[]) feeItr.next();
                amount = feeObject[7].toString();

            }
        }
        System.out.println("TRVClasaa :: amount::" + amount);
        // Set your store Id and store password and define TestMode
        // SSLCommerz sslcz = new SSLCommerz("testbox", "qwerty", true);
        SSLCommerz sslcz = new SSLCommerz(GlobalVariable.SSLCommerzStoreId, GlobalVariable.SSLCommerzStorePass, Boolean.parseBoolean(GlobalVariable.SSLCommerzStoreMode));

        /**
         * If following order validation returns true, then process transaction
         * as success. if this following validation returns false , then query
         * status if failed of canceled. Check request.get("status") for this
         * purpose
         */
        
        boolean bbbb = sslcz.orderValidate(trxId, amount, currency, request);
        
        
        System.out.println("TRVClasaa :: bbbb::" + bbbb);
        
        return bbbb;

    }
}
