

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showSyUserDetails(arg1, arg2) {
            console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');
        }


        function syUserInfoUpdateOpt(arg1, arg2) {
            console.log("User :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Update User Details");

            $.post("syUserListUserDetails.jsp", {userId: arg1, sessionid: arg2}, function (data) {
//
//
//                console.log(data);
//
//                console.log("invoiceDetails :: " + data[0].invoiceDetails);
//
//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');

        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">User Management</a></li>                    
                    <li class="active">Users</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Login</th>
                                    <th>Name</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Role</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String userId = "";
                                    String userName = "";
                                    String userDeptName = "";
                                    String userEmail = "";
                                    String userMsisdn = "";
                                    String syStatus = "";
                                    String userStatusDesc = "";
                                    String userRole = "";
                                    String btnColorClass = "";
                                    String levelStatusColorClass = "";

                                    //   Query q1 = dbsession.createQuery("from SyUser syUs ORDER BY syUs.userId ASC");
                                    //    Query q1 = dbsession.createQuery("from SyUser syUs WHERE syUs.syStatus ='1' ORDER BY syUs.userId ASC");
                                    //  q1.setMaxResults(50);
//                                    Query invItemsSQL = dbsession.createSQLQuery("select p.product_code,p.product_name,ps.product_size_inch,i.store_id,i.QUANTITY1,i.QUANTITY2,i.RATE,i.DISCOUNT_AMOUNT,i.TOTAL_AMOUNT,u1.uom uom1,u2.carton_size,u2.uom uom2,pcd.product_grade "
//                                                    + "from invoice_items i,product_class pc,product_class_defination pcd,product p,product_size ps,product_uom u1,carton_size u2 "
//                                                    + "where i.INV_NO= '" + invNo + "' "
//                                                    + "and i.PRODUCT_CLASS_ID=pc.id "
//                                                    + "and pc.product_id=p.product_id "
//                                                    + "and ps.id=p.product_size_id "
//                                                    + "and p.product_uom2=u2.id "
//                                                    + "and p.product_uom1=u1.id "
//                                                    + "and pc.class_id=pcd.id");
//                                    Query usrSQL = dbsession.createSQLQuery("select u.USER_ID,u.USER_NAME,d.DEPT_NAME,st.STATUS_DESC,r.ROLE_DESC "
//                                            + "FROM sy_user u,sy_dept d ,sy_status st,sy_user_role ur,sy_roles r"
//                                            + " WHERE u.USER_DEPT = d.DEPT_ID"
//                                            + " AND u.USER_STATUS = st.STATUS_CODE"
//                                            + " AND u.USER_ID = ur.USER_ID"
//                                            + " AND r.ROLE_ID = ur.ROLE_ID"
//                                            + " ORDER BY u.USER_ID ASC");
                                    Query usrSQL = dbsession.createSQLQuery("SELECT u.USER_ID,u.USER_NAME,d.DEPT_NAME,st.STATUS_DESC,r.ROLE_DESC "
                                            + "FROM sy_dept d ,sy_status st,sy_user u "
                                            + "LEFT OUTER JOIN sy_user_role ur ON u.USER_ID = ur.USER_ID "
                                            + "LEFT OUTER JOIN sy_roles r ON  r.ROLE_ID = ur.ROLE_ID "
                                            + "WHERE u.USER_ID != 'ADMIN' AND u.USER_DEPT = d.DEPT_ID AND u.USER_STATUS = st.STATUS_CODE "
                                            + "ORDER BY u.USER_ID ASC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            userId = obj[0].toString().trim();
                                            userName = obj[1].toString().trim();
                                            userDeptName = obj[2].toString().trim();
                                            userStatusDesc = obj[3].toString().trim();
                                            userRole = obj[4] == null ? "" : obj[4].toString().trim();

                                            if (userStatusDesc.equalsIgnoreCase("INACTIVE")) {
                                                btnColorClass = "btn-danger";
                                                levelStatusColorClass = "label-danger";
                                            } else {
                                                btnColorClass = "btn-primary";
                                                levelStatusColorClass = "label-primary";
                                            }
//        String noSpaceStr = str.replaceAll("\\s", ""); // using built in method  
//        System.out.println(noSpaceStr);  

                                            String updateProfileUrl = GlobalVariable.baseUrl + "/userManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&userIdX=" + userId + "&selectedTab=profile";
                                            String updateChangePassUrl = GlobalVariable.baseUrl + "/userManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&userIdX=" + userId + "&selectedTab=changePass";
                                            

                                %>


                                <tr>
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(userId);%></td>
                                    <td><% out.println(userName); %></td>
                                    <td><% out.print(userDeptName); %></td>
                                    <td class="text-center"><span class="label label-megna label-rounded <% out.print(levelStatusColorClass); %>"><% out.print(userStatusDesc);%></span></td>

                                    <td><% out.print(userRole);%></td>
                                    <td class="text-center">
                                        
                                        <a id="btnShowSyUserDetails<% out.print(userId);%>" onclick="showSyUserDetails(<% out.print("'" + userId + "','" + sessionid + "'");%>);" title="User details" class="btn <%  out.print(btnColorClass); %> btn-sm"><i class="icon-info"></i></a>


                                        <a href="<% out.print(updateProfileUrl); %>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a href="<% out.print(updateChangePassUrl); %>" title="Change Password" class="btn btn-primary btn-sm"><i class="icon-key"></i></a>
                                        

                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                    %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>