<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String selectRoleId = request.getParameter("roleID");
    String selectRoleName = request.getParameter("roleName");
    String sessionid = request.getParameter("sessionid");

    //  String selectRoleId = "1083";
    String roleId = "";
    String roleActId = "";
    String roleActDesc = "";
    String roleActScreen = "";
    String userMsisdn = "";
    String syStatus = "";
    String selectRoleDetailsTopSection = "";
    String selectRoleDetailsMidSection = "";
    String selectRoleDetailsBottomSection = "";
    String btnColorClass = "";
    String levelStatusColorClass = "";
    String btnInvInfo = "";
    String selectRoleDetails = "";

    String allActivityConStr = "";

    String checkActRoleActivity = "";

    String multiSelectCSS = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/multiselect/css/multi-select.css";
    String multiSelectJs = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/multiselect/js/jquery.multi-select.js";

    selectRoleDetailsTopSection = "<div class=\"white-box1 printableArea1\">"
            + "<p><strong>Role Id : " + selectRoleId + "</strong></p>"
            + "<p><strong>Role Name : " + selectRoleName + "</strong></p></div>";

    selectRoleDetailsMidSection = "<div class=\"col-md-12\" style=\"\">"
            + "<div class=\"table-responsive1\" style=\"\">";

    Query usrSQL = dbsession.createSQLQuery("SELECT ra.ROLE_ID,ra.ACT_ID,act.ACT_DESC,act.ACT_SCREEN "
            + "FROM sy_role_act ra "
            + "LEFT JOIN sy_activity act ON ra.ACT_ID = act.ACT_ID "
            + "WHERE ra.ROLE_ID ='" + selectRoleId + "'");
    int ix = 1;
    if (!usrSQL.list().isEmpty()) {
        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            roleId = obj[0].toString().trim();
            roleActId = obj[1].toString().trim();
            roleActDesc = obj[2].toString().trim();
            roleActScreen = obj[3].toString().trim();

//            
//            selectRoleDetailsMidSection += "<tr>"
//                    + "<td class=\"text-center\">"+ ix +"</td>"
//                    + "<td>" + roleActId + "</td>"
//                    + "<td>" + roleActDesc + "</td>"
//                    + "<td>" + roleActScreen + "</td>"                   
//                    + "</tr>";
            ix++;
        }
    }

    SyActivity syactivity = null;
    String actid = null;
    String actdesc = null;
    String readallow = "";
    String writeallow = "";
    String readcheck = "";
    String writecheck = "";
    int rownum = 0;
    String actStr = "";
    String selectedOpt = "";

    Query q1 = dbsession.createQuery("from SyActivity as activity order by actId asc");

    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
        rownum += 1;
        syactivity = (SyActivity) itr.next();
        actid = syactivity.getActId();
        actdesc = syactivity.getActDesc();

        String checkRoleActivityExist = new getRoleActivity().getRoleActivityID(selectRoleId, actid);

        System.out.println("checkRoleActivityExist " + checkRoleActivityExist);

        //       if (currentUserPass.equals(yourPassEnc)) {
        if (checkRoleActivityExist.equals("YES")) {

            selectedOpt = "selected";
        } else {
            selectedOpt = "";
        }

        actStr += "<option value=\"" + actid + "\"" + selectedOpt + ">" + actid + ":&nbsp;" + actdesc + "</option>";

    }

    allActivityConStr = "<form name=\"roleActivityUpdateFormX\" id=\"roleActivityUpdateFormX\" method=\"POST\" accept-charset=\"UTF-8\">"
            + "<input type=\"hidden\" id=\"roleId\" name=\"roleId\" value=\"" + selectRoleId + "\">"
            + "<input type=\"hidden\" id=\"roleName\" name=\"roleName\" value=\"" + selectRoleName + "\">"
            + "<link href=\"" + multiSelectCSS + "\" rel=\"stylesheet\" type=\"text/css\" />"
            + "<div class=\"row custom-bottom-margin-5x\">"
            + "<div class=\"col-sm-10 offset-1 systemRoleXcon\">"
            + "<p class=\"text-success text-center1\">Note: You need to choose complete hierarchy (parents) to get any action works.</p>"
            + "<select class=\"form-control customInput-sm\" id=\"public-methods\" name=\"public-methods\" multiple>"
            + actStr
            + "</select>"
            + "<div class=\"button-box m-t-20\">"
            + "<a id=\"select-all\" class=\"btn btn-danger btn-outline\" href=\"#\">select all</a>"
            + "<a id=\"deselect-all\" class=\"btn btn-info btn-outline\" href=\"#\">deselect all</a>"
            + "</div>"
            + "<script type=\"text/javascript\" src=\"" + multiSelectJs + "\"></script>"
            + "<script>"
            + "$(function(){"
            + "$(\"#public-methods\").multiSelect({"
            + "selectableHeader: \"<div class='activityHeader'>Available Role Activity</div>\","
            + "selectionHeader: \"<div class='activityHeader'>Assigned Role Activity</div>\","
            + "});"
            + "$('#select-all').click(function() {"
            + "$('#public-methods').multiSelect('select_all');"
            + "return false;"
            + "});"
            + "$('#deselect-all').click(function() {"
            + "$('#public-methods').multiSelect('deselect_all');"
            + "return false;"
            + "});"
            + "});"
            + "</script>"
            + "</div>"
            + "</div>"           
            + "</form>";

    selectRoleDetailsBottomSection = "</div>"
            + "</div>";
            

    selectRoleDetails = selectRoleDetailsTopSection + selectRoleDetailsMidSection + allActivityConStr + selectRoleDetailsBottomSection;

    //     + "<a id=\"btnShowUpdateActivityOption\" onclick=\"showUpdateActivityOption('" + selectRoleId + "','" + selectRoleName + "','" + sessionid + "')\" class=\"btn btn-primary\"> Update Role Activity </a> &nbsp;"
    btnInvInfo = "<div class=\"clearfix\"></div>"
            + "<div class=\"text-right\">"
            + "<a id=\"btnUpdateActivityOptionSubmit\" onclick=\"updateActivityOptionSubmit('" + selectRoleId + "','"+selectRoleName +"','" + sessionid + "')\"  class=\"btn btn-info waves-effect waves-light\">Update Role Activity</a> &nbsp;"
            + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>"
            + "</form></div>"
            + "<br/>";

    json = new JSONObject();
    json.put("selectRoleId", selectRoleId);
    json.put("selectRoleDetails", selectRoleDetails);
    json.put("btnInvInfo", btnInvInfo);
    jsonArr.add(json);

    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>