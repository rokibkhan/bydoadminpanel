<%-- 
    Document   : syRoleCheck
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>


<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>


<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<%
    String username = null;
    String logstat = null;
    String sessionid = null;
    sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }

    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }
    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }


%>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self ==top)
                    window.open("logout.jsp","_top","");
            }

            if (window.history) {
                window.history.forward(1);
            }
        </script>


        <title>User Role Confirmation</title>
        <%

            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }

            //                   out.println(username);
            //                   out.println(logstat);
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }
        %>
    </head>
    <body onLoad="breakOut()">

        <%
            //out.println(request.getParameter("rowcount"));
            String rec = request.getParameter("rowcount").toString();
            int reccount = Integer.parseInt(rec);
            String userid = request.getParameter("userId");
            String fullname = request.getParameter("fullName");
            String adduser = request.getParameter("entryUserID");
            String addterm = request.getParameter("entryTerm");
            String addip = request.getParameter("entryIP");
        %>
        <h1>User's Role(s) Confirmation</h1>        
        <br>
        <form name="frmUserRole" id="frmUserRole" method="post" action="syUserRoleInsert.jsp?sessionid=<%out.println(sessionid);%>"> <%-- onSubmit="return dataInsert()" --%>
            <table class="tab1">
               
                <thead>
                    <tr>
                        <th>Property</th>
                        <th>Value</th>
                    </tr>
                </thead>
                    <tr >
                        <td align="left" width="150">User ID</td>
                        <td id=><input type="hidden" id="userid" name="userid" value="<% out.println(userid);%>"><% out.println(userid);%></td>
                    </tr>
                    <tr >
                        <td align="left">Full Name</td>
                        <td id=><input type="hidden" id="fullname" name="fullname" value="<% out.println(fullname);%>"><% out.println(fullname);%></td>
                    </tr>                    
                    <tr >
                        <td align="left" width="150">Your ID</td>
                        <td id=><input type="hidden" id="adduser" name="adduser" value="<% out.println(adduser);%>"><% out.println(adduser);%></td>
                    </tr>
                    <tr >
                        <td align="left" width="150">Your Computer</td>
                        <td id=><input type="hidden" id="addterm" name="addterm" value="<% out.println(addterm);%>"><% out.println(addterm);%></td>
                    </tr>
                    <tr >
                        <td align="left" width="150">Your IP</td>
                        <td id=><input type="hidden" id="addip" name="addip" value="<% out.println(addip);%>"><% out.println(addip);%></td>
                    </tr>

            </table>
            <br>
            <b>Role to be Granted</b>[Any change will effect after user's next login]<br><br>
            <table class="tab1">
                <thead>
                <tr >
                    <th style="width: 200px">Role ID</th>
                    <th>Title</th>
                </tr>
            </thead>
                <%
                    //int i = 1;
                    int j = 0;
                    SyRoles syRoles = new SyRoles();
                    String actid = null;
                    String roleDesc = null;

                    String roleid[] = request.getParameterValues("SecondList");

                    for (int i = 0; i < roleid.length; i++) {
                        Query q = dbsession.createQuery("select a.roleDesc from SyRoles as a where a.roleId = '" + roleid[i].toString().trim() + "'");
            
                        roleDesc = (String) q.uniqueResult();


                %>

                <tr <%if ((i % 2) == 0) {%><%} else {%><%}%>>
                    <td><input type="hidden" id="roleid" name="roleid" value="<% out.println(roleid[i]);%>"><% out.println(roleid[i]);%></td>
                    <td><input type="hidden" id="rolename" name="rolename" value="<% out.println(roleDesc);%>"><% out.println(roleDesc);%></td>
                </tr>
                <%
                    }
                      dbsession.close();
                %>
                <tr >                    
                    <td class="button" align="right">
                        <INPUT TYPE="button" VALUE="Back" onClick="history.go(-1);return true;">
                    </td>
                    <td> <input type="submit" value="Confirm" name="btnSubmit">
                    </td>
                </tr>
            </table>
        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
