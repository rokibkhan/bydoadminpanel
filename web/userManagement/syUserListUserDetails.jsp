<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String selectInvId = request.getParameter("userId");
    String sessionid = request.getParameter("sessionid");

    //  String selectInvId = "1083";
    String userId = "";
    String userName = "";
    String userDeptName = "";
    String userEmail = "";
    String userMsisdn = "";
    String syStatus = "";
    String userStatusDesc = "";
    String userRole = "";
    String btnColorClass = "";
    String levelStatusColorClass = "";
    String btnInvInfo = "";

    Query usrSQL = dbsession.createSQLQuery("SELECT u.USER_ID,u.USER_NAME,d.DEPT_NAME,st.STATUS_DESC,r.ROLE_DESC "
            + "FROM sy_dept d ,sy_status st,sy_user u "
            + "LEFT OUTER JOIN sy_user_role ur ON u.USER_ID = ur.USER_ID "
            + "LEFT OUTER JOIN sy_roles r ON  r.ROLE_ID = ur.ROLE_ID "
            + "WHERE u.USER_ID = AND (u.USER_DEPT = d.DEPT_ID AND u.USER_STATUS = st.STATUS_CODE)");

    if (!usrSQL.list().isEmpty()) {
        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            userId = obj[0].toString().trim();
            userName = obj[1].toString().trim();
            userDeptName = obj[2].toString().trim();
            userStatusDesc = obj[3].toString().trim();
            userRole = obj[4] == null ? "" : obj[4].toString().trim();

            if (userStatusDesc.equalsIgnoreCase("INACTIVE")) {
                btnColorClass = "btn-danger";
                levelStatusColorClass = "label-danger";
            } else {
                btnColorClass = "btn-primary";
                levelStatusColorClass = "label-primary";
            }
        }
    }

   
    

    invoiceDetailsTopSection = "<div class=\"white-box printableArea\">"
            + "<h4><b>&nbsp;</b> <span class=\"pull-right\">Invoice No#" + invNo1 + "</span></h4>"
            + "<hr>"
            + "<div class=\"row\">"
            + "<div class=\"col-md-12\">"
            + "<div class=\"pull-left\">"
            + "<address>"
            + "<h3> &nbsp;<b class=\"text-danger\">" + invStoreName + "</b></h3>"
            + "<p class=\"text-muted m-l-5\">" + invStoreAddressX + "</p>"
            + "</address>"
            + "</div>"
            + "<div class=\"pull-right text-right\">"
            + "<address>"
            + "<h3>To,</h3>"
            + "<h4 class=\"font-bold\">" + consumerName + "</h4>"
            + "<p class=\"text-muted m-l-30\">" + consumerAddressX + "</p>"
            + "<p class=\"m-t-30\"><b>Invoice Date :</b> <i class=\"fa fa-calendar\"></i> " + invDate1 + "</p>"
            + "</address>"
            + "</div>"
            + "</div>";

    invoiceDetailsMidSection = "<div class=\"col-md-12\">"
            + "<div class=\"table-responsive m-t-40\" style=\"clear: both;\">"
            + "<table class=\"table table-hover color-table1 primary-table1 table-custom-padding-5x\">"
            + "<thead>"
            + "<tr>"
            + "<th class=\"text-center\">#</th>"
            + "<th>Product Code</th>"
            + "<th>Name</th>"
            + "<th>Grade</th>"
            + "<th>Size</th>"
            + "<th class=\"text-center\">Quantity</th>"
            + "<th class=\"text-right\">Rate</th>"
            + "<th class=\"text-right\">Discount</th>"
            + "<th class=\"text-right\">Total</th>"
            + "</tr>"
            + "</thead>"
            + "<tbody>";

    String product_code = "";
    String product_name = "";
    String product_size_inch = "";
    String store_id = "";
    String QUANTITY1 = "";
    String QUANTITY2 = "";
    String RATE = "";
    String RATE1 = "";
    String DISCOUNT_AMOUNT = "";
    String DISCOUNT_AMOUNT1 = "";
    String TOTAL_AMOUNT = "";
    String TOTAL_AMOUNT1 = "";
    String uom1 = "";
    String carton_size = "";
    String uom2 = "";
    String product_grade = "";
    String quantityUom = "";

    Query invItemsSQL = dbsession.createSQLQuery("select p.product_code,p.product_name,ps.product_size_inch,i.store_id,i.QUANTITY1,i.QUANTITY2,i.RATE,i.DISCOUNT_AMOUNT,i.TOTAL_AMOUNT,u1.uom uom1,u2.carton_size,u2.uom uom2,pcd.product_grade "
            + "from invoice_items i,product_class pc,product_class_defination pcd,product p,product_size ps,product_uom u1,carton_size u2 "
            + "where i.INV_NO= '" + invNo + "' "
            + "and i.PRODUCT_CLASS_ID=pc.id "
            + "and pc.product_id=p.product_id "
            + "and ps.id=p.product_size_id "
            + "and p.product_uom2=u2.id "
            + "and p.product_uom1=u1.id "
            + "and pc.class_id=pcd.id");

    if (!invItemsSQL.list().isEmpty()) {
        for (Iterator it1 = invItemsSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            product_code = obj[0].toString().trim();
            product_name = obj[1].toString().trim();
            product_size_inch = obj[2].toString().trim();

            QUANTITY1 = obj[4].toString().trim();
            QUANTITY2 = obj[5].toString().trim();

            RATE = obj[6].toString().trim();
            RATE1 = df.format(Double.parseDouble(RATE));

            DISCOUNT_AMOUNT = obj[7].toString().trim();
            DISCOUNT_AMOUNT1 = df.format(Double.parseDouble(DISCOUNT_AMOUNT));

            TOTAL_AMOUNT = obj[8].toString().trim();
            TOTAL_AMOUNT1 = df.format(Double.parseDouble(TOTAL_AMOUNT));

            uom1 = obj[9].toString().trim();
            carton_size = obj[10].toString().trim();
            uom2 = obj[11].toString().trim();
            product_grade = obj[12].toString().trim();

            quantityUom = QUANTITY1 + " " + uom1 + "  " + QUANTITY2 + " " + uom2;

            invoiceDetailsMidSection += "<tr>"
                    + "<td class=\"text-center\">1</td>"
                    + "<td>" + product_code + "</td>"
                    + "<td>" + product_name + "</td>"
                    + "<td>" + product_grade + "</td>"
                    + "<td>" + product_size_inch + "</td>"
                    + "<td>" + quantityUom + "</td>"
                    + "<td class=\"text-right\">" + RATE1 + "</td>"
                    + "<td class=\"text-right\">" + DISCOUNT_AMOUNT1 + "</td>"
                    + "<td class=\"text-right\">" + TOTAL_AMOUNT1 + "</td>"
                    + "</tr>";

        }
    }

    invoiceDetailsBottomSection = "<tr>"
            + "<td colspan=\"7\" class=\"text-right\"> Sub Total </td>"
            + "<td class=\"text-right\"> 0.00 </td>"
            + "<td class=\"text-right\"> " + invSubTotalAmount1 + " </td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "</div>"
            + "<div class=\"col-md-12\">"
            + "<div class=\" col-md-6 pull-right m-t-30 text-right\">"
            + "<div class=\"table-responsive\">"
            + "<table class=\"table color-bordered-table table-custom-padding-5x\">"
            + "<tbody>"
            + "<tr><td><b>Sub Total</b> </td><td>" + invSubTotalAmount1 + "</td></tr>"
            + "<tr><td><b>Carrying</b></td><td>" + invFreight1 + "</td></tr>"
            + "<tr><td><b>Discount</b></td><td>" + invDiscountAmount1 + "</td></tr>"
            + "<tr><td><b>Sales Tax</b></td><td>" + invSalesTax1 + "</td></tr>"
            + "<tr><td><h4><b>Total</b></h4></td><td><h4>" + invTotalAmount1 + "</h4></td></tr>"
            + "<tr><td><b>Payment</b></td><td>" + invPaymentAmount1 + "</td></tr>"
            + "<tr><td><b>Balance/Due</b></td><td>" + invNetAmount1 + "</td></tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "</div>"
            + "</div>"
            + "<div class=\"clearfix\"></div>"
            + "<hr>"
            + "<div class=\"col-md-12 text-left\" style=\"border-top: 1px solid #eceeef;\">"
            + "<P>In word:" + invAmountInWord + "</p>"
            + "</div>"
            + "</div>"
            + "</div>";

    invoiceDetails = invoiceDetailsTopSection + invoiceDetailsMidSection + invoiceDetailsBottomSection;

//    out.println(invoiceDetails);
    String btnPaymentInfo = "";

    if (Double.parseDouble(invNetAmount) > 0) {
        btnPaymentInfo = "<a id=\"btnShowInvoicePaymentOption\" onclick=\"showInvoicePaymentOption(" + selectInvId + ",'" + sessionid + "')\" class=\"btn btn-primary\"> Proceed to payment </a> ";
    }

    String invPrintUrl = GlobalVariable.baseUrl + "/sales/invoicePrint.jsp?sessionid=" + sessionid + "&invNo=" + selectInvId;
//String invPrintUrl = "";
    btnInvInfo = "<div class=\"clearfix\"></div>"
            + "<div class=\"text-right\">"
            + btnPaymentInfo
            + "<a title=\"Print Invoice\" href=\"" + invPrintUrl + "\" target=\"_blank\" id=\"print\" class=\"btn btn-default btn-outline\" type=\"button\"> <span><i class=\"fa fa-print\"></i> Print</span> </a> "
            + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>"
            + "</div>"
            + "<br/>";

    json = new JSONObject();
    json.put("selectInvId", selectInvId);
    json.put("invoiceDetails", invoiceDetails);
    json.put("btnInvInfo", btnInvInfo);
    jsonArr.add(json);

    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>