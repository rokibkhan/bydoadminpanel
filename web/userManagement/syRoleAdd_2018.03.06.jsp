<%-- 
    Document   : scSupplierList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">
<%
    Session dbsession = null;
    String qryparam = "";

    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }
%>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>

<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
    }
    .bp_valid {
        color:green;
    }
</style>

<%
    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

   

%>
<script type="text/javascript">
    function AJAXInteraction(url, callback) {

        var req = init();
        req.onreadystatechange = processRequest;

        function init() {
            if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        function processRequest () {
            // readyState of 4 signifies request is complete
            if (req.readyState == 4) {
                // status of 200 signifies sucessful HTTP call
                if (req.status == 200) {
                    if (callback) callback(req.responseXML);
                }
            }
        }

        this.doGet = function() {
            // make a HTTP GET request to the URL asynchronously
            req.open("GET", url, true);
            req.send(null);
        }
    }

    function validateRoleId() {
        var form= document.getElementById("addrole");
        var target = document.getElementById("roleId");
        var url = "validate?id=" + encodeURIComponent(target.value)+"&key=roleid";
        var ajax = new AJAXInteraction(url, validateCallback);
        ajax.doGet();
    }

    function validateCallback(responseXML) {
        // see "The Callback Function" below for more details
        var form= document.getElementById("addrole");
        var msg = responseXML.
            getElementsByTagName("valid")[0].firstChild.nodeValue;

        
        if (msg == "false") {

            var mdiv = document.getElementById("roleIdMessage");
            //var mdiv=form.roleIdMessage;

            // set the style on the div to invalid

            mdiv.className = "bp_invalid";
            //mdiv.style.color= red;
            mdiv.innerHTML = "Duplicate ID";
            var submitBtn = document.getElementById("addrole").submit;
            submitBtn.disabled = true;

        } else {

            var mdiv = document.getElementById("roleIdMessage");
            //var mdiv=form.roleIdMessage;
            // set the style on the div to valid

            mdiv.className = "bp_valid";
            mdiv.innerHTML = "Valid ID";
            var submitBtn = document.getElementById("addrole").submit;
            submitBtn.disabled = false;
        }
        
    } 
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp","_top","");
            }
        </script>

        <title>Add New Supplier</title>
        <%
             
           
            String read = request.getParameter("read");
            String write = request.getParameter("write");
  
        %>

    </head>
    <body> <%-- onLoad="init()" --%>
        <%
            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            Query q1 = null;
//            SyCity sycity = null;
//            SyCountry sycountry = null;

            int rownum = 0;
            String oddeven = null;
            getRegistryID getregid = new getRegistryID();
            String regID = null;
            int regCode = 5;
            regID = getregid.getID(regCode);


        %>
        <script type="text/javascript">
            function dataValidate(){
                var form = document.getElementById("addrole");

                if(form.roleId.value.toString()==null || form.roleId.value.toString()==""){
                    alert("Missing required data - Role ID");
                    return false;
                }

                if(form.roleName.value.toString()==null || form.roleName.value.toString()==""){
                    alert("Missing required data - Role Title");
                    return false;
                }
                if(form.SecondList.value.toString()==null || form.SecondList.value.toString()==""){
                    alert("Must Grant a Data From List box");
                    return false;
                }

                return true;
            }
        </script>


        <script language="javaScript" type="text/javascript">
            function move(fbox,tbox)
            {
                var arrFbox = new Array();
                var arrTbox = new Array();
                var arrLookup = new Array();

                var i;
                for(i=0; i<tbox.options.length; i++)
                {
                    arrLookup[tbox.options[i].text] = tbox.options[i].value;
                    arrTbox[i] = tbox.options[i].text;
                }
                var fLength =0;
                var tLength = arrTbox.length;

                for(i=0; i<fbox.options.length; i++)
                {
                    arrLookup[fbox.options[i].text] = fbox.options[i].value;
                    if(fbox.options[i].selected && fbox.options[i].value!="")
                    {
                        arrTbox[tLength] = fbox.options[i].text;
                        tLength++;
                        //                        var grantButton = document.getElementById("addrole").submit;
                        //                        grantButton.disabled = true;

                    }
                    else
                    {
                        arrFbox[fLength] = fbox.options[i].text;
                        fLength++;
                    }
                }
                //arrFbox.sort();
                //arrTbox.sort();
                fbox.length = 0;
                tbox.length = 0;
                var c;
                for(c=0; c<arrFbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrFbox[c]];
                    no.text = arrFbox[c];
                    fbox[c] = no;
                }
                for(c=0; c<arrTbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrTbox[c]];
                    no.text = arrTbox[c];
                    tbox[c] = no;
                }
            }
            function selectAll(box)
            {
                for(var i=0; i<box.length; i++)
                {
                    box[i].selected = true;
                }
            }
            
            function GetCourse(semisterId)
            {
                if(window.XMLHttpRequest)
                {
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {
                    xmlhttp = new ActiveXObject("Microsoft.XHLHTTP");
                }

                xmlhttp.onreadystatechange = function()
                {
                    if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        var L = document.getElementById("templist");
                        L.innerHTML = xmlhttp.responseText;
                    }
                }

                var url = "getCourse.jsp?semisterId="+semisterId+"";
                xmlhttp.open("Get",url,true);
                xmlhttp.send();
            }
                

        </script>
        <script type="text/javascript">
            
            
            function buttonValid(){
                var arrFbox = new Array();
                var arrTbox = new Array();
                var arrLookup = new Array();

                var i;
                for(i=0; i<tbox.options.length; i++)
                {
                    arrLookup[tbox.options[i].text] = tbox.options[i].value;
                    arrTbox[i] = tbox.options[i].text;
                }
                var fLength =0;
                var tLength = arrTbox.length;

                for(i=0; i<fbox.options.length; i++)
                {
                    arrLookup[fbox.options[i].text] = fbox.options[i].value;
                    if(fbox.options[i].selected && fbox.options[i].value!="")
                    {
                        arrTbox[tLength] = fbox.options[i].text;
                        tLength++;
                        //                        var grantButton = document.getElementById("addrole").submit;
                        //                        grantButton.disabled = true;

                    }
                    else
                    {
                        arrFbox[fLength] = fbox.options[i].text;
                        fLength++;
                    }
                }
                //arrFbox.sort();
                //arrTbox.sort();
                fbox.length = 0;
                tbox.length = 0;
                var c;
                for(c=0; c<arrFbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrFbox[c]];
                    no.text = arrFbox[c];
                    fbox[c] = no;
                }
                for(c=0; c<arrTbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrTbox[c]];
                    no.text = arrTbox[c];
                    tbox[c] = no;
                }
            }

            
        </script>
        <h1>Add Role</h1>
        <br>
        <form name="addrole" id="addrole" method="post" action="syRole_check.jsp?sessionid=<%out.println(sessionid);%>" onSubmit="return dataValidate()">
            <table class="tab">
                <tbody>
                    <tr >
                        <td align="left" width="100">Role ID <font color="red">*</font></td>
                        <td align="left" width="300"><input  class="textbox" size="50" type="text" id="roleId" name="roleId" onkeyup="validateRoleId()"></td>
                        <td align="left" width="100"><div id="roleIdMessage"></div></td>
                    </tr>

                    <tr >
                        <td align="left">Role Title <font color="red">*</font></td>
                        <td align="left"><input class="textbox"type="text" size="50" name="roleName" id="roleName"></td>
                        <input type="hidden" size="50" id="entryUserID" name="entryUserID" value="<% out.println(username);%>">
                        <input type="hidden" size="50" id="entryTerm" name="entryTerm" value="<%out.println(InetAddress.getLocalHost().getHostName().toString());%>">
                        <input type="hidden" size="50" id="entryIP" name="entryIP" value="<%out.println(InetAddress.getLocalHost().getHostAddress().toString());%>">
                    </tr>

                </tbody>
            </table>
            
            <script type="text/javascript">
                function buttonAvailable(box)
                {
                    var msg = document.getElementById("FirstList");
                    var form = document.getElementById("addrole");
                
                    for(var i=0; i<box.length; i++)
                    {
                        box.selected= true;
                        var revokeButton = document.getElementById("addrole").revoke;
                        revokeButton.disabled = true;
                        var grantButton = document.getElementById("addrole").grant;
                        grantButton.disabled = false;
                    }
                }
                
                function buttonAvailableSecond(boxSecond)
                {
                    var msg = document.getElementById("FirstList");
                    var form = document.getElementById("addrole");
                
                    for(var i=0; i<boxSecond.length; i++)
                    {
                        boxSecond[i].selected = true;
                        var grantButton = document.getElementById("addrole").grant;
                        grantButton.disabled = true;
                        var revokeButton = document.getElementById("addrole").revoke;
                        revokeButton.disabled = false;
                    }
                }
            
            </script>
                    <table class="tab1">
                <tr style="color:red; font-size: 15px"><td>Note: You need to choose complete hierarchy (parents) to get any action works.</td></tr>
            </table>
                    <table class="tab" id="roleact">
                <tbody>
                    <tr>
                        <td align="center"><font color="black" size="3">Available Role</font></td>
                        <td></td>
                        <td></td>
                        <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="black" size="3">Active Role</font></td>
                    </tr>
                    <tr align="center" valign="middle">
                        <td align="right">
                            <!--                            <span id="templist"></span>-->
                            <select name="FirstList" size="40" multiple id="FirstList" style="width: 250px;" onchange="buttonAvailable('FirstList')">
                                <%
                                    SyActivity syactivity = null;
                                    String actid = null;
                                    String actdesc = null;
                                    String readallow = "";
                                    String writeallow = "";
                                    String readcheck = "";
                                    String writecheck = "";
                                    rownum = 0;

                                    q1 = dbsession.createQuery("from SyActivity as activity order by actId asc");

                                    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                        rownum += 1;
                                        syactivity = (SyActivity) itr.next();
                                        actid = syactivity.getActId();
                                        actdesc = syactivity.getActDesc();


                                %>
                                <option value="<%out.println(actid);%>"><%out.println(actid);%>:&nbsp;<%out.println(actdesc);%></option>
                                <%
                                    }
                                    dbsession.close();

                                %>
                            </select>
                        </td>
                        <td align="right">
                            <input type="button" onclick="move(this.form.SecondList,this.form.FirstList)" value="Revoke" name="revoke" style="height: 30px">
                        </td>
                        <td align="left">
                            <input type="button" onclick="move(this.form.FirstList,this.form.SecondList)" value="Grant" name="grant" style="height: 30px">
                        </td>
                        <td align="left">
                            <span id="templist"></span>
                            <select name="SecondList" size="40" multiple id="SecondList" style="width: 250px;" onchange="buttonAvailableSecond('SecondList')">

                            </select> 
                        </td>
                         <input type="hidden" name="rowcount" id="rowcount" value=<% out.println(rownum);%>>
                    </tr>
                    <tr>
                    <td class="label" align="right"></td>
                    <td class="button" align="right"><input type="reset" value="Reset" name="reset"><input type="submit" value="Submit" name="submit" onclick="selectAll(document.addrole.SecondList);"></td>
                    <td class="label" align="right"></td>
                    <td class="label" align="right"></td>
                </tr>
                </tbody>
            </table>                    

        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
