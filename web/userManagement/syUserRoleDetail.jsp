<%-- 
    Document   : scSupplierList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
            Session dbsession = null;
            String qryparam = "";
            /*            out.println(session.getId()); */
            if (session.isNew()) {
                response.sendRedirect("logout.jsp");
            }

%>

<!--<link rel="stylesheet" type="text/css" href="css/styledetail.css">-->
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<%
            String username = null;
            String logstat = null;
            String sessionid = null;
            sessionid = request.getParameter("sessionid").trim();

  

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        

        <title>User's Role Detail: <% request.getParameter("userid");%>(<% request.getParameter("fullname");%>)</title>
        <%


                    if (session.getAttribute("username") != null) {
                        username = session.getAttribute("username").toString().toUpperCase();
                    }
                    if (session.getAttribute("logstat") != null) {
                        logstat = session.getAttribute("logstat").toString();
                    }

                    //                   out.println(username);
                    //                   out.println(logstat);
                    if (username == null || logstat.equals("Y") == false) {
                        response.sendRedirect("logout.jsp");
                        // return;
                    }

                    String read = request.getParameter("read");
                    String write = request.getParameter("write");
                    String curroleid=null;

                    dbsession = HibernateUtil.getSessionFactory().openSession();
                    org.hibernate.Transaction dbtrx = null;
                    dbtrx = dbsession.beginTransaction();

                    Query q1 = null;
                    Query q2 = null;
                    String roleid = "";

        %>
    </head>
    <body >

        <%

                    if (request.getParameter("userid") != null) {
                        String userid = request.getParameter("userid");
                        String fullname = request.getParameter("fullname").toString();
        %>
        <h1>User's Role Detail: <% out.println(fullname);%></h1>

        <br>

        <table class="tab1">
            <thead>
                <tr >
                    <th width="100">Role ID</th>
                    <th width="200">Role Name</th>
                    <th width="200">Granted By</th>
                    <th width="250">Last Grant Date</th>
                    <th width="50">IP</th>
                    <th>Computer</th>
                    <th>Action</th>
                </tr>
            </thead>
                <%
                    //id.roleId,addDate,addUsr,addTerm,addIp,modUsrId,modTerm,modIp,lastModDate
                    SyUserRole userrole=null;
                    String rolename="";
                    String grantuser="";
                    String grantusername="";
                    Date grantdate=null;
                    String grantip="";
                    String grantterm="";
                    int rownum=0;
                    q1 = dbsession.createQuery("from SyUserRole where id.userId='" + userid + "'");
                    for (Iterator itr=q1.list().iterator();itr.hasNext();){
                        userrole =(SyUserRole) itr.next();
                        roleid=userrole.getId().getRoleId();
                        grantuser=userrole.getModUsrId();
                        grantusername=new getUser().getUserName(grantuser);
                        rolename=new getRole().getRoleName(roleid);
                        grantdate=userrole.getLastModDate();
                        grantip=userrole.getModIp();
                        grantterm=userrole.getModTerm();
                        rownum += 1 ;
                %>
                <tr >
                    <td><% out.println(roleid);%></td>
                    <td><% out.println(rolename);%></td>
                    <td><% out.println(grantusername);%></td>
                    <td><% out.println(grantdate.toString());%></td>
                    <td><% out.println(grantip);%></td>
                    <td><% out.println(grantterm);%></td>
                     <td><A HREF='syRoleDetail.jsp?roleid=<%out.println(roleid);%>&rolename=<%out.println(rolename);%>&sessionid=<%out.println(sessionid);%>'>Detail</A></td>

                </tr>
                <%}%>
            
        </table>
        <% if ((curroleid!=null) && (curroleid.equals("")!=false)){%>
        <br>
        <br>
        Role Permissions: <% out.println(rolename);%>
        <br>
        <table class="table">
            <thead>
                <tr >
                    <th align="left" width="200"><b>Activity</b></th>
                    <th align="left" width="100"><b>Allow Read?</b></th>
                    <th align="left" width="100"><b>Allow Write?</b></th>
                    <th align="left" width="100"><b>Added By</b></th>
                    <th align="left" width="200"><b>Effective Date</b></th>
                    <th align="left" width="50"><b>Last Modified By</b></th>
                    <th align="left" width="200"><b>Last Modification Date</b></th>
                </tr>
            </thead>

                <%

                                        String actid = "";
                                        String actdesc = "";
                                        String actread;
                                        String actwrite;
                                        String adduser = "";
                                        Date adddate;
                                        String moduser = "";
                                        Date moddate;
                                        qryparam="";
                                        rownum = 0;
                                        String oddeven = null;
                                        getAbbr abbr = new getAbbr();

                                        if (qryparam != "" && qryparam != null) {
                                            //q1=dbsession.createQuery("select ra.id.actId,act.actDesc,ra.actRead,ra.actWrite,ra.addUsr,ra.addDate,ra.modUsrId,ra.lastModDate from SyRoleAct ra, SyActivity act where ra.id.actId=act.actId and ra.id.roleId='"+qryparam+"'");
                                            q2 = dbsession.createQuery("from SyRoleAct ra, SyActivity act where ra.id.actId=act.actId and ra.id.roleId='" + qryparam + "'");
                                       
                                        Iterator itr02 = q2.iterate();
                                        while (itr02.hasNext()) {
                                            Object[] pair = (Object[]) itr02.next();
                                            SyRoleAct roleact = (SyRoleAct) pair[0];
                                            SyActivity act = (SyActivity) pair[1];
                                            //for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {

                                            actid = roleact.getId().getActId();
                                            actdesc = act.getActDesc();
                                            rolename = request.getParameter("rolename").toString();
                                            actread = roleact.getActRead();
                                            actwrite = roleact.getActWrite();
                                            adduser = roleact.getAddUsr();
                                            adddate = roleact.getAddDate();
                                            moduser = roleact.getModUsrId();
                                            moddate = roleact.getLastModDate();
                                            rownum += 1;

                %>
                <% if ((rownum % 2) == 0) {%>
                <tr >
                    <%} else {%>
                <tr >
                    <%}%>
                    <td><b><% out.println(actdesc);%></b></td>
                    <td align="center"> <% out.println(abbr.getLong(actread));%></td>
                    <td align="center"> <% out.println(abbr.getLong(actwrite));%></td>
                    <td> <% out.println(adduser);%></td>
                    <td> <% out.println(adddate);%></td>
                    <td> <% out.println(moduser);%></td>
                    <td> <% out.println(moddate);%></td>
                </tr>
                <%      }
                   }
                                          dbsession.close();
                  %>
            
        </table>
        <%}
      }%>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
