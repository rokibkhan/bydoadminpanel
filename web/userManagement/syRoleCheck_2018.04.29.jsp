<%-- 
    Document   : syRoleCheck
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">

 
<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<script type="text/javascript">
    function dataInsert() {
        var TabParent = document.getElementById('tab_role');
        roleid = TabParent.getElementsByTagName('tr')[0].getElementsByTagName('td')[1].innerHTML;
        rolename = TabParent.getElementsByTagName('tr')[1].getElementsByTagName('td')[1].innerHTML;
        adduser = TabParent.getElementsByTagName('tr')[2].getElementsByTagName('td')[1].innerHTML;
        addterm = TabParent.getElementsByTagName('tr')[3].getElementsByTagName('td')[1].innerHTML;
        addip = TabParent.getElementsByTagName('tr')[4].getElementsByTagName('td')[1].innerHTML;

        if (roleid != null && roleid != "") {
            alert(roleid + ":" + rolename);
 

            var rowCount = $("#tab_actions tr").length;
            if (rowCount > 1) {
                var table = document.getElementById('tab_actions');
                rows = table.getElementsByTagName('tr'), i, j, cells, ActionId;

                for (i = 0, j = rows.length; i < j; ++i) {
                    cells = rows[i].getElementsByTagName('td');
                    if (!cells.length) {
                        continue;
                    }
                    actionId = cells[0].innerHTML;
                    readAllow = cells[2].innerHTML;
                    writeAllow = cells[3].innerHTML;
                    alert(actionId + ":" + readAllow + ":" + writeAllow);
                }
                return false;
            }
        }
        return false;
    }
</script>



        <%
            //out.println(request.getParameter("rowcount"));
            String rec = request.getParameter("rowcount").toString().trim();
            int reccount = Integer.parseInt(rec);
            String roleid = request.getParameter("roleId");
            String rolename = request.getParameter("roleName");
            String adduser = request.getParameter("entryUserID");
            String addterm = request.getParameter("entryTerm");
            String addip = request.getParameter("entryIP");
            if (roleid == null) {
                roleid = "";
            }

        %>
        <h1>New Role Confirmation</h1>
        <br>
        <form name="frmRoleAct" id="frmRoleAct" method="post" action="syRoleInsert.jsp?sessionid=<%out.println(sessionid);%>"> <%-- onSubmit="return dataInsert()" --%>
            <table class="tab1"id="tab_role" border="0">

                <tbody>
                    <tr >
                        <td align="left" width="150"><b>Role ID</b></td>
                        <td id=><input type="hidden" id="roleid" name="roleid" value="<% out.println(roleid);%>"><% out.println(roleid);%></td>
                    </tr>
                    <tr >
                        <td align="left"><b>Role Title</b></td>
                        <td id=><input type="hidden" id="rolename" name="rolename" value="<% out.println(rolename);%>"><% out.println(rolename);%></td>
                    </tr>                    
                    <tr >
                        <td align="left" width="150"><b>User</b></td>
                        <td id=><input type="hidden" id="adduser" name="adduser" value="<% out.println(adduser);%>"><% out.println(adduser);%></td>
                    </tr>
                    <tr >
                        <td align="left" width="150"><b>Computer</b></td>
                        <td id=><input type="hidden" id="addterm" name="addterm" value="<% out.println(addterm);%>"><% out.println(addterm);%></td>
                    </tr>
                    <tr >
                        <td align="left" width="150"><b>IP</b></td>
                        <td id=><input type="hidden" id="addip" name="addip" value="<% out.println(addip);%>"><% out.println(addip);%></td>
                    </tr>

                </tbody>
            </table>
            <br>
            <b>Actions to be allowed ...</b><br>

            <table class="tab1" id="tab_actions">
                <thead>
                    <tr >
                        <th><b>Action ID</b></th>
                        <th><b>Function</b></th>
                        <th><b>Read?</b></th>
                        <th><b>Write?</b></th>
                    </tr>
                </thead>
                <% 
                    String fieldname = null;
                    String readfield = null;
                    String writefield = null;
                    String titlefield = null;
                    String readval = null;
                    String writeval = null;
                    String actid = null;
                    String actdesc = null;

                    while (i <= reccount) {
                        fieldname = Integer.toString(i);
                        titlefield = "D" + fieldname;
                        readfield = "R" + fieldname;
                        writefield = "W" + fieldname;
                        actdesc = request.getParameter(titlefield);
                        actid = request.getParameter(fieldname);
                        readval = request.getParameter(readfield);
                        writeval = request.getParameter(writefield);

                        if (readval == null) {
                            readval = "N";
                        } else if (readval.equals("Y")) {
                            readval = "Y";
                        } else {
                            readval = "N";
                        }
                        if (writeval == null) {
                            writeval = "N";
                        } else if (writeval.equals("Y")) {
                            writeval = "Y";
                        } else {
                            writeval = "N";
                        }
                        //out.println(readval+":"+writeval);

                        if (readval.equals("Y") || writeval.equals("Y")) {
                %>

                <tr <%if ((i % 2) == 0) {%><%} else {%><%}%>>                    
                    <td><input type="hidden" id="actid" name="actid" value="<% out.println(actid);%>"><% out.println(actid);%></td>
                    <td><input type="hidden" id="actdesc" name="actdesc" value="<% out.println(actdesc);%>"><% out.println(actdesc);%></td>
                    <td align="center"><input type="hidden" id="readval" name="readval" value="<% out.println(readval);%>"><% if (readval.equals("Y")) {%>Yes<%} else {%>No<%}%></td>
                    <td align="center"><input type="hidden" id="writeval" name="writeval" value="<% out.println(writeval);%>"><% if (writeval.equals("Y")) {%>Yes<%} else {%>No<%}%></td>
                </tr>
                <%}
                        i += 1;
                    }
                %>
            </table>

            <table>
                <tr >
                    <td class="button" align="right">
                        <INPUT TYPE="button" VALUE="Back" onClick="history.go(-1);
                                return true;">
                        <input type="submit" value="Confirm" name="btnSubmit">
                    </td>
                </tr>
            </table>
        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
