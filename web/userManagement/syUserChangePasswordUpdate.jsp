<%-- 
    Document   : dataInsert
    Created on : Nov 20, 2010, 4:53:05 AM
    Author     : Akter
--%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.*" %>
<%@page import="java.net.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">



<%
    Session dbsession = null;
    String qryparam = "";

    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>


        <%
            String userid = request.getParameter("userId").trim().toUpperCase();
            String yourPass = request.getParameter("yourPass").trim();
            String newPass = request.getParameter("newPass").trim();
            String newPassEnc = new Encryption().getEncrypt(newPass);
            String adduser = request.getParameter("entryUserID");
            String addterm = request.getParameter("entryTerm");
            String addip = request.getParameter("entryIP");
            String yourPassEnc = new Encryption().getEncrypt(yourPass);
            boolean validUser = new isExists().isExist(userid, "userid");
            SyUser syuser = null;

            if (validUser == false) {
        %><h1 class="bp_invalid">Invalid User ID ...</h1><%
                return;
            }

            String correctPassEnc = new getUser().getUserPass(username);

            if (correctPassEnc.equals(yourPassEnc) == false) {
        %><h1 class="bp_invalid">Your password is incorrect...</h1><%
                return;
            }

            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();
            getDate date = new getDate();

            SyStatus systat = null;
            Query q3 = dbsession.createQuery("from SyStatus where statusCode=0");
            Iterator itr3 = q3.list().iterator();
            if (itr3.hasNext()) {
                systat = (SyStatus) itr3.next();
            }

            Query q4 = dbsession.createQuery("from SyUser where userId='" + userid.trim() + "'");
            Iterator itr4 = q4.list().iterator();
            if (itr4.hasNext()) {
                syuser = (SyUser) itr4.next();
            }
            syuser.setUserPassword(newPassEnc);
            syuser.setModUsrId(adduser.trim());
            syuser.setModTerm(addterm.trim());
            syuser.setModIp(addip.trim());
            syuser.setSyStatus(systat);
            syuser.setLastModDate(date.serverDate());
            dbsession.update(syuser);

            SyAuditTrail audit = new SyAuditTrail();
            getRegistryID regId = new getRegistryID();
            getDate gdate = new getDate();
            String eventid = "";
            eventid = regId.getID(16);
            BigDecimal eventId = new BigDecimal(eventid);

            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

            audit.setEvtId(eventId);
            audit.setEvtDatetime(gdate.serverDate());
            audit.setEvtEmpId(username);
            audit.setEvtCode("USER-ADD");
            audit.setEvtDetail("Audit for USER Password Change");
            audit.setEvtRef01("");
            audit.setEvtRef02("");
            audit.setEvtRef03("");
            audit.setEvtRef04("");
            audit.setEvtRef05(hostname + "," + ipAddress);
            dbsession.save(audit);

            dbsession.flush();
            dbtrx.commit();
            dbsession.close();

            if (dbtrx.wasCommitted()) {
                response.sendRedirect("success.jsp?sessionid=" + sessionid);
            } else {
                dbtrx.rollback();
                response.sendRedirect("fail.jsp?sessionid=" + sessionid);
            }

        %>

    </body>
</html>
