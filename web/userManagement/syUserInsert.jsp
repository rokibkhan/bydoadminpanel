<%-- 
    Document   : dataInsert
    Created on : Nov 20, 2010, 4:53:05 AM
    Author     : Akter
--%>


<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>

<%    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Information Management</title>
    </head>
    <body>
        <h1>updating database, please wait ...</h1>
    </body>
</html>
<%
    String userid = request.getParameter("userid").trim().toUpperCase();
    String fullname = request.getParameter("fullname").trim();

    String userPass = request.getParameter("userpass").trim();
    String encpass = new Encryption().getEncrypt(userPass);
    System.out.println("userPass" + userPass + "encpass" + encpass);
    String userdept = request.getParameter("userdept").trim();
    String userstat = request.getParameter("userstat").trim();
    int intStatus = Integer.parseInt(userstat);
    String usermobile = request.getParameter("usermobile");
    String useremail = request.getParameter("useremail");
    String adduser = request.getParameter("adduser");
    String addterm = request.getParameter("addterm");
    String addip = request.getParameter("addip");
    String act = request.getParameter("act");

    boolean update = false;
    //          out.println(request.getParameter("roleid"));
    getDate date = new getDate();

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    //out.println(roleid);
    SyUser syuser = null;
    SyStatus systat = null;
    SyDept codept = null;

    if (act.equals("edit")) {
        update = true;
    }

    getStringToDatetime strtodate = new getStringToDatetime();
    Date expdate = strtodate.dateTime("2099-12-31 23:59:59");

    syuser = new SyUser();
    systat = new SyStatus();
    codept = new SyDept();

    if (update == true) {
        Query q4 = dbsession.createQuery("from SyUser where userId='" + userid.trim() + "'");
        Iterator itr4 = q4.list().iterator();
        if (itr4.hasNext()) {
            syuser = (SyUser) itr4.next();
        }

        syuser.setUserName(fullname.trim());
        //syrole.setAddDate(date.serverDate());
        //syrole.setAddUsr(adduser.trim());
        //syrole.setAddTerm(addterm.trim());
        //syrole.setAddIp(addip.trim());

        codept.setDeptId(userdept);
        syuser.setSyDept(codept);
        syuser.setUserPassword(encpass);
        syuser.setUserMsisdn(usermobile.trim());
        syuser.setUserEmail(useremail.trim());
        syuser.setModUsrId(adduser.trim());
        syuser.setModTerm(addterm.trim());
        syuser.setModIp(addip.trim());
        systat.setStatusCode(intStatus);
        syuser.setSyStatus(systat);
        syuser.setLastModDate(date.serverDate());
        syuser.setUserExpDate(expdate);
        dbsession.update(syuser);

    } else {
        syuser.setUserId(userid.trim());
        syuser.setUserName(fullname.trim());
        syuser.setUserPassword(encpass);
        codept.setDeptId(userdept);
        syuser.setSyDept(codept);
        systat.setStatusCode(intStatus);
        syuser.setSyStatus(systat);
        syuser.setAddDate(date.serverDate());
        syuser.setAddUsr(adduser.trim());
        syuser.setAddTerm(addterm.trim());
        syuser.setAddIp(addip.trim());
        syuser.setModUsrId(adduser.trim());
        syuser.setModTerm(addterm.trim());
        syuser.setModIp(addip.trim());
        syuser.setUserMsisdn(usermobile.trim());
        syuser.setUserEmail(useremail.trim());
        syuser.setLastModDate(date.serverDate());
        syuser.setUserExpDate(expdate);
        dbsession.save(syuser);
    }
//    dbtrx = dbsession.beginTransaction();

    SyAuditTrail audit = new SyAuditTrail();
    getRegistryID regId = new getRegistryID();
    getDate gdate = new getDate();
    String eventid = "";
    eventid = regId.getID(16);
    BigDecimal eventId = new BigDecimal(eventid);

    audit.setEvtId(eventId);
    audit.setEvtDatetime(gdate.serverDate());
    audit.setEvtEmpId(username);
    if (act.equalsIgnoreCase("add")) {
        audit.setEvtCode("USER-ADD");
        audit.setEvtDetail("Audit for USER CREATION");
    } else {
        audit.setEvtCode("USER-EDIT");
        audit.setEvtDetail("Audit for USER-edit");

    }
    audit.setEvtRef01("");
    audit.setEvtRef02("");
    audit.setEvtRef03("");
    audit.setEvtRef04("");
    audit.setEvtRef05(addterm + "," + addip);
    dbsession.save(audit);

    dbsession.flush();
    dbtrx.commit();
    dbsession.close();

    if (dbtrx.wasCommitted()) {
        response.sendRedirect("success.jsp?sessionid=" + sessionid);
    } else {
        dbtrx.rollback();
        response.sendRedirect("fail.jsp?sessionid=" + sessionid);
    }
%>
