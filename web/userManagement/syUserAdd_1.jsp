<%-- 
    Document   : syUserAdd
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">
<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>
<%
    Session dbsession = null;
    String qryparam = "";

    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }
%>

<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>


<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
    }
    .bp_valid {
        color:green;
    }
</style>

<%
    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>
<script type="text/javascript">
    function AJAXInteraction(url, callback) {

        var req = init();
        req.onreadystatechange = processRequest;

        function init() {
            if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        function processRequest () {
            // readyState of 4 signifies request is complete
            if (req.readyState == 4) {
                // status of 200 signifies sucessful HTTP call
                if (req.status == 200) {
                    if (callback) callback(req.responseXML);
                }
            }
        }

        this.doGet = function() {
            // make a HTTP GET request to the URL asynchronously
            req.open("GET", url, true);
            req.send(null);
        }
    }

    function validateUserId() {
        var form= document.getElementById("adduser");
        var target = document.getElementById("userId");
        var url = "validate?id=" + encodeURIComponent(target.value)+"&key=userid";
        var ajax = new AJAXInteraction(url, validateCallback);
        ajax.doGet();
    }

    function validateCallback(responseXML) {
        // see "The Callback Function" below for more details
        var form= document.getElementById("adduser");
        var msg = responseXML.
            getElementsByTagName("valid")[0].firstChild.nodeValue;

        
        if (msg == "false") {

            var mdiv = document.getElementById("userIdMessage");
            //var mdiv=form.roleIdMessage;

            // set the style on the div to invalid

            mdiv.className = "bp_invalid";
            //mdiv.style.color= red;
            mdiv.innerHTML = "Duplicate ID";
            var submitBtn = document.getElementById("adduser").submit;
            submitBtn.disabled = true;

        } else {

            var mdiv = document.getElementById("userIdMessage");
            //var mdiv=form.roleIdMessage;
            // set the style on the div to valid

            mdiv.className = "bp_valid";
            mdiv.innerHTML = "Valid ID";
            var submitBtn = document.getElementById("adduser").submit;
            submitBtn.disabled = false;
        }
        
    } 
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp","_top","");
            }
        </script>
        <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
        <%-- <script type="text/javascript" src="cityscript.js"></script> --%>
        <%-- <link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" /> --%>
        <title>Add New User</title>
        <%
            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }
            String read = request.getParameter("read");
            String write = request.getParameter("write");
        %>

    </head>
    <body onLoad="breakOut()"> <%-- onLoad="init()" --%>
        <%
            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            Query q1 = null;

            int rownum = 0;
            String oddeven = null;
            getRegistryID getregid = new getRegistryID();
            String regID = null;
            int regCode = 5;
            regID = getregid.getID(regCode);
            
        InetAddress ip;
        String hostname = "";
        String ipAddress = "";
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            ipAddress = ip.getHostAddress();
     
        } catch (UnknownHostException e) {
 
            e.printStackTrace();
        }


        %>
        <script type="text/javascript">
            function dataValidate(){
                var form = document.getElementById("adduser");

                if(form.userId.value.toString()==null || form.userId.value.toString()==""){
                    alert("Missing required data - User ID");
                    return false;
                }

                if(form.userName.value.toString()==null || form.userName.value.toString()==""){
                    alert("Missing required data - User Name");
                    return false;
                }
                if(form.userPass.value.toString()==null || form.userPass.value.toString()==""){
                    alert("Missing required data - Password");
                    return false;
                }

                if(form.userDept.value.toString()==null || form.userDept.value.toString()=="SELECT"){
                    alert("Please select the deprtment");
                    return false;
                }

                if(form.userRePass.value.toString()==null || form.userRePass.value.toString()==""){
                    alert("Missing required data - Re-enter Password");
                    return false;
                }
                if(form.userRePass.value.toString() != form.userPass.value.toString()){
                    alert("Password Confirmation Error");
                    return false;
                }

            <%--
            var chkd=$("input[type='checkbox']:checked").length;
            if (chkd<1){
                alert("No action has been selected");
                return false;
            }
            --%>
                    return true;
                }
        </script>

        <script type="text/javascript">
            function isValidEmail(){
                var form=document.getElementById("adduser");
                var email=form.userEmail.value.toString();
                //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
                //eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", email);
                var pattern= new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
                if (pattern.test(email)==false){
                    alert("Please enter a valid email ID");
                    form.userEmail.value="";
                }
                
            }
            function isValidPhone(){
                var form=document.getElementById("adduser");
                var phone=form.userMSISDN.value.toString();
                //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
                //eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", email);
                var pattern= new RegExp(/\D/);
                //alert(pattern.test(phone));
                
                if (pattern.test(phone)==true){
                    alert("Invalid Phone number format");
                    form.userMSISDN.value="";
                }
                
            }
            
            function dataLength(){
                var form = document.getElementById("adduser");
                if(form.userId.value.length>10){
                    alert("User Id Must Be 10 Character");
                    form.userId.value="";
                    return false;
                }
                if(form.userName.value.length>40){
                    alert("User Name Must Be 40 Character");
                    form.userName.value="";
                    return false;
                }
                if(form.userPass.value.length>30){
                    alert("Password Must Be 30 Character");
                    form.userPass.value="";
                    return false;
                }
                if(form.userRePass.value.length>30){
                    alert("Password Must Be 30 Character");
                    form.userRePass.value="";
                    return false;
                }
                if(form.userMSISDN.value.length>30){
                    alert("Mobile No Must Be 30 Character");
                    form.userMSISDN.value="";
                    return false;
                }
                if(form.userEmail.value.length>50){
                    alert("User Email Must Be 50 Character");
                    form.userEmail.value="";
                    return false;
                }
                return true;
            }
        </script>

        <form name="adduser" id="adduser" method="post" action="syUserCheck.jsp?sessionid=<%out.println(sessionid);%>&act=add" onSubmit="return dataValidate()">
            
        <fieldset style="border: 1px solid silver;  width: 600px;">
                <legend style="font-size: 25px;" >Add New User</legend>
                
                <table  class="tab">
                
                    <tr >
                        <td align="left" width="150">User ID <font color="red">*</font></td>
                        <td align="left" width="250"><input class="textbox" style="width:98%;"size="50" type="text" id="userId" name="userId" onkeyup="validateUserId();dataLength()" onBlur="validateUserId()"></td>
                        <td align="left" width="100"><div id="userIdMessage"></div></td>
                    </tr>

                    <tr >
                        <td align="left">User Name <font color="red">*</font></td>
                        <td align="left"><input class="textbox" style="width:98%;" type="text" size="50" name="userName" id="userName" onkeyup="dataLength()"></td>
                    </tr>
                    <tr >
                        <td align="left">Password <font color="red">*</font></td>
                        <td align="left"><input class="textbox" style="width:98%;" type="password" size="50" name="userPass" id="userPass" onkeyup="dataLength()"></td>
                    </tr>
                    <tr >
                        <td align="left">Re-enter Password <font color="red">*</font></td>
                        <td align="left"><input class="textbox" style="width:98%;" type="password" size="50" name="userRePass" id="userRePass" onkeyup="dataLength()"></td>
                    </tr>
                    <tr >
                        <td align="left">Department <font color="red">*</font></td>
                        <td align="left"><select id="userDept" name="userDept" class="textbox1" style="width:100%;">
                                <%
                                    q1 = dbsession.createQuery("from SyDept dept order by deptName asc");
                                %><option value="">SELECT</option><%
                                    for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                                        SyDept dept = (SyDept) itr1.next();
                                %> <option value=" <% out.println(dept.getDeptId());%> "> <% out.println(dept.getDeptName());%> </option> <%
                                    }
                                      dbsession.close();
                                %>
                            </select>
                        </td>                        
                    </tr>
                    <tr >
                        <td align="left">Mobile</td>
                        <td align="left"><input class="textbox" style="width:98%;" type="text" size="50" name="userMSISDN" id="userMSISDN" onkeyup="dataLength()" onblur="isValidPhone()"></td>
                    </tr>
                    <tr >
                        <td align="left">Email</td>
                        <td align="left"><input class="textbox" style="width:98%;" type="text" size="50" name="userEmail" id="userEmail" onkeyup="dataLength()" onBlur="isValidEmail()"></td>
                    </tr>
                    <tr>
                        <td class="label" align="right"></td>
                        <td class="button" align="right"><input type="reset" value="Reset" name="reset"><input type="submit" value="Submit" name="submit"></td>
                        <input type="hidden" size="50" id="entryUserID" name="entryUserID" value="<% out.println(username);%>">
                        <input type="hidden" size="50" id="entryTerm" name="entryTerm" value="<%out.println(hostname);%>">
                        <input type="hidden" size="50" id="entryIP" name="entryIP" value="<%out.println(ipAddress);%>">
                        <input type="hidden" size="50" id="userStat" name="userStat" value="0">
                
                    </tr>

                    
            </table>
        </fieldset>
        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
