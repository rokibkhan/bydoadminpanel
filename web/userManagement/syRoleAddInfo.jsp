<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;
    int responseCode = 0;
    String responseMsg = "";
    String responseMsgText = "";
    String userIdP = "";
    String sessionidP = "";
    String uRoleP = "";
    String roleName = "";
    String roleDesc = "";
    String allRoleStr = "";
    String allRoleInfo = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    getDate date = new getDate();

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        userIdP = request.getParameter("userId").trim();
        sessionidP = request.getParameter("sessionid").trim();
        uRoleP = request.getParameter("uRole").trim();

        roleName = request.getParameter("roleName").trim();
        roleDesc = request.getParameter("roleDesc").trim();

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        userStoreIdH = session.getAttribute("storeId").toString();

        if (sessionidP.equals(sessionIdH)) {

            System.out.println("Password Token Match:: ");

            String currentRole = new getRole().getRoleName(roleName);

            System.out.println("currentRole " + currentRole);

            if (currentRole == "") {

                String adduser = userNameH;
                String addterm = InetAddress.getLocalHost().getHostName().toString();
                String addip = InetAddress.getLocalHost().getHostAddress().toString();
                String newPassEnc = "";
                SyRoles syrole = null;

                syrole = new SyRoles();

                syrole.setRoleId(roleName);
                syrole.setRoleDesc(roleDesc);

                syrole.setAddDate(date.serverDate());
                syrole.setAddUsr(adduser.trim());
                syrole.setAddTerm(addterm.trim());
                syrole.setAddIp(addip.trim());

                syrole.setModUsrId(adduser.trim());
                syrole.setModTerm(addterm.trim());
                syrole.setModIp(addip.trim());
                syrole.setLastModDate(date.serverDate());

                dbsession.save(syrole);

                dbtrx.commit();

                if (dbtrx.wasCommitted()) {
                    //   response.sendRedirect("success.jsp?sessionid=" + sessionid);

                    allRoleStr = "<label class=\"control-label col-md-3\">Role</label>"
                            + "<div class=\"col-md-6\">"
                            + "<select class=\"form-control input-sm customInput-sm\" style=\"height: 30px;\" id=\"syUserRole\" name=\"syUserRole\">"
                            + "<option value=\"\">SELECT ROLE</option>";

                    String roleIdX = "";
                    String roleDescX = "";
                    Query qrX = dbsession.createQuery("from SyRoles WHERE roleId !='" + uRoleP + "' order by roleId asc");

                    for (Iterator itr1 = qrX.list().iterator(); itr1.hasNext();) {
                        SyRoles role = (SyRoles) itr1.next();
                        roleIdX = role.getRoleId().trim();
                        roleDescX = role.getRoleDesc().trim();
                        allRoleStr += "<option  value=\"" + roleIdX + "\">" + roleDescX + "</option>";

                    }

                    allRoleStr += "</select"
                            + "<div id=\"syUserRoleErr\" class=\"help-block with-errors\"></div>"
                            + "</div>"
                            + "<div class=\"col-sm-2\"><a onclick=\"showAddNewSystemRoleInfo('" + userIdP + "','" + sessionidP + "')\" title=\"Add New Role\" class=\"btn btn-info btn-sm\"><i class=\"fa fa-plus-circle\"></i></a></div>";

                    allRoleInfo = allRoleStr;
                    responseCode = 1;
                    responseMsgText = "Success! New role added.";
                    responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<strong>Success!</strong> New role added."
                            + "</div>";
                } else {
                    dbtrx.rollback();
                    //   response.sendRedirect("fail.jsp?sessionid=" + sessionid);
                    responseMsgText = "Error! Please try again.";
                    responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<strong>Error!</strong> Please try again</a>."
                            + "</div>";
                }

            } else {
                responseMsgText = "Error! Role name already exits.";
                responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                        + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                        + "<strong>Error!</strong> Role name already exits."
                        + "</div>";
            }

        } else {
            responseMsgText = "Error! Token not match.";
            responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<strong>Error!</strong> Token not match."
                    + "</div>";
        }

    } else {

        responseMsgText = "Error! Please login and try again.";
        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    json = new JSONObject();
    json.put("roleName", roleName);
    json.put("roleDesc", roleDesc);
    json.put("allRoleInfo", allRoleInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    json.put("responseMsgText", responseMsgText);
    jsonArr.add(json);
    out.print(jsonArr);

    dbsession.flush();
    dbsession.close();


%>   