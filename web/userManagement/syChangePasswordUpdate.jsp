
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>

<%    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>

<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
        width: 600px;
    }
    .bp_valid {
        color:green;
    }
</style>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change User Password</title>
    </head>
    <body>
        <%
            String userid = request.getParameter("userId").trim().toUpperCase();
            String yourPass = request.getParameter("yourPass").trim();
            String newPass = request.getParameter("newPass").trim();
            String adduser = request.getParameter("entryUserID");
            String addterm = request.getParameter("entryTerm");
            String addip = request.getParameter("entryIP");
            boolean validUser = new isExists().isExist(userid, "userid");
            SyUser syuser = null;

            String newPassEnc = new Encryption().getEncrypt(newPass);
            String yourPassEnc = new Encryption().getEncrypt(yourPass);

            if (validUser == false) {
        %><h1 class="bp_invalid">Invalid User Name ...</h1><%
                return;
            }

            String correctUserPass = new getPassWord().getPass(userid);

            if (correctUserPass.equals(yourPassEnc) == false) {
        %><h1 class="bp_invalid">Your old password is incorrect</h1><%
                return;
            }

            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();
            getDate date = new getDate();

            SyStatus systat = null;
            Query q3 = dbsession.createQuery("from SyStatus where statusCode=0");
            Iterator itr3 = q3.list().iterator();
            if (itr3.hasNext()) {
                systat = (SyStatus) itr3.next();
            }

            Query q4 = dbsession.createSQLQuery("UPDATE  SY_USER SET USER_PASSWORD='" + newPassEnc + "',MOD_USR_ID='" + adduser + "',MOD_TERM='" + addterm + "',MOD_IP='" + addip + "',LAST_MOD_DATE=now() where user_Id='" + userid.trim() + "'");

            q4.executeUpdate();
            dbsession.flush();
            dbtrx.commit();
            dbsession.close();

            if (dbtrx.wasCommitted()) {
                response.sendRedirect("success.jsp?sessionid=" + sessionid);
            } else {
                dbtrx.rollback();
                response.sendRedirect("fail.jsp?sessionid=" + sessionid);
            }

        %>

    </body>
</html>
