<%-- 
    Document   : scSupplierList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">
<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    //response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>
<%
    Session dbsession = null;
    String qryparam = "";

    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }
%>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">


<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>

<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
    }
    .bp_valid {
        color:green;
    }
</style>

<%
    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
//                        out.println("("+sessionid+")");
//                        out.println("("+session.getId()+")");
//                response.sendRedirect("logout.jsp");
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    //                   out.println(username);
    //                   out.println(logstat);
    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp", "_top", "");
            }
        </script>


        <title>Manage User Role</title>
        <%
            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }
            //                   out.println(username);
            //                   out.println(logstat);
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }
            String read = request.getParameter("read");
            String write = request.getParameter("write");

            String userid = request.getParameter("userid");
            String fullname = request.getParameter("fullname");

            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

        %>
        <script language="javaScript" type="text/javascript">
            function move(fbox, tbox)
            {
                var arrFbox = new Array();
                var arrTbox = new Array();
                var arrLookup = new Array();

                var i;
                for (i = 0; i < tbox.options.length; i++)
                {
                    arrLookup[tbox.options[i].text] = tbox.options[i].value;
                    arrTbox[i] = tbox.options[i].text;
                }
                var fLength = 0;
                var tLength = arrTbox.length;

                for (i = 0; i < fbox.options.length; i++)
                {
                    arrLookup[fbox.options[i].text] = fbox.options[i].value;
                    if (fbox.options[i].selected && fbox.options[i].value != "")
                    {
                        arrTbox[tLength] = fbox.options[i].text;
                        tLength++;
                    }
                    else
                    {
                        arrFbox[fLength] = fbox.options[i].text;
                        fLength++;
                    }
                }
                //arrFbox.sort();
                //arrTbox.sort();
                fbox.length = 0;
                tbox.length = 0;
                var c;
                for (c = 0; c < arrFbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrFbox[c]];
                    no.text = arrFbox[c];
                    fbox[c] = no;
                }
                for (c = 0; c < arrTbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrTbox[c]];
                    no.text = arrTbox[c];
                    tbox[c] = no;
                }
            }
            function selectAll(box)
            {
                for (var i = 0; i < box.length; i++)
                {
                    box[i].selected = true;
                }
            }

            function GetCourse(semisterId)
            {
                if (window.XMLHttpRequest)
                {
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {
                    xmlhttp = new ActiveXObject("Microsoft.XHLHTTP");
                }

                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        var L = document.getElementById("templist");
                        L.innerHTML = xmlhttp.responseText;
                    }
                }

                var url = "getCourse.jsp?semisterId=" + semisterId + "";
                xmlhttp.open("Get", url, true);
                xmlhttp.send();
            }


        </script>
        <script type="text/javascript">
            function dataValidate() {
                var form = document.getElementById("adduserrole");

                if (form.SecondList.value.toString() == null || form.SecondList.value.toString() == "") {
                    alert("Must Grant a Data From List box");
                    return false;
                }
                return true;
            }
        </script>
        <script type="text/javascript">
            function buttonAvailable(box)
            {
                var msg = document.getElementById("FirstList");
                var form = document.getElementById("adduserrole");

                for (var i = 0; i < box.length; i++)
                {
                    box.selected = true;
                    var revokeButton = document.getElementById("adduserrole").revoke;
                    revokeButton.disabled = true;
                    var grantButton = document.getElementById("adduserrole").grant;
                    grantButton.disabled = false;
                }
            }

            function buttonAvailableSecond(boxSecond)
            {
                var msg = document.getElementById("FirstList");
                var form = document.getElementById("adduserrole");

                for (var i = 0; i < boxSecond.length; i++)
                {
                    boxSecond[i].selected = true;
                    var grantButton = document.getElementById("adduserrole").grant;
                    grantButton.disabled = true;
                    var revokeButton = document.getElementById("adduserrole").revoke;
                    revokeButton.disabled = false;
                }
            }

        </script>

    </head>
    <body onLoad="breakOut()"> <%-- onLoad="init()" --%>
        <h1>Manage Role for</h1>
        <h2><%out.println(fullname + "[" + userid + "]");%></h2>
        <br>        
        <form name="adduserrole" id="adduserrole" method="post" action="syUserRoleCheck.jsp?sessionid=<%out.println(sessionid);%>" onsubmit="return dataValidate()">
 
            <input type="hidden" id="userId" size="50" name="userId" value="<%out.println(userid);%>">
            <input type="hidden" id="fullName" size="50" name="fullName" value="<%out.println(fullname);%>">
            <table class="tab">
                <tbody>

                    <tr align="center" style="color:red; ">
                        <td colspan="3"><font size="2">Note: You need to choose Option to get any action works.</font></td>
                    </tr>
                    <tr>
                        <td align="left"><font color="black" size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Available User's Role</font></td>
                        <td></td>
                        <td align="left">&nbsp;&nbsp;&nbsp;<font color="black" size="3">Active User's Role</font></td>
                    </tr>

                    <tr align="center" valign="middle" style="width: 100%">
                        <td align="right" style=" width: 40%">
                            <!--                            <span id="templist"></span>-->
                            <select name="FirstList" size="15" multiple id="FirstList" style="width: 250px;" onchange="buttonAvailable('FirstList')">
                                <%

                                    String roleid = "";
                                    String rolename = "";
                                    SyUserRole userRole = null;
                                    SyRoles role = null;
                                    int rownum = 0;
                                    String oddeven = null;
                                    Query q3 = dbsession.createQuery("from SyRoles as a where a.id.roleId not in (select userrole.id.roleId from SyUserRole userrole where userrole.id.userId ='" + userid + "')");
                                    for (Iterator itr = q3.list().iterator(); itr.hasNext();) {
                                        role = (SyRoles) itr.next();
                                        roleid = role.getRoleId();
                                        rolename = role.getRoleDesc();

                                %>
                                <option value="<%out.println(roleid);%>"><%out.println(roleid);%></option>
                                <%
                                    }%>

                            </select>
                        </td>


                        <td align="center" style="width: 20%">
                            <input align="" type="button" onclick="move(this.form.SecondList, this.form.FirstList)" value="Revoke" name="revoke" style="height: 30px">
                            <input type="button" onclick="move(this.form.FirstList, this.form.SecondList)" value="Grant" name="grant" style="height: 30px">
                        </td>
                        <td align="left" style="width: 40%">
                            <span id="templist"></span>
                            <select name="SecondList" size="15" multiple id="SecondList" style="width: 250px;" onchange="buttonAvailableSecond('SecondList')">
                                <%
                                    Query q1 = null;
                                    Query q2 = null;

                                    q2 = dbsession.createQuery("from SyUserRole as userrole where userrole.id.userId='" + userid + "'");
                                    for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                        userRole = (SyUserRole) itr2.next();
                                        roleid = userRole.getId().getRoleId();

                                        q1 = dbsession.createQuery("from SyRoles as a where a.roleId ='" + roleid + "'");
                                        //q1 = dbsession.createQuery("from SyUser where userId in(select id.userId from SyUserRole where upper(id.roleId) like upper('%" + qryparam + "%') or upper(id.roleId) in( select roleId from SyRoles where upper(roleDesc) like upper('%" + qryparam + "%'))) order by userName asc");

                                        for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                                            role = (SyRoles) itr1.next();
                                            rolename = role.getRoleDesc();

                                            // dbtrx.commit();
                                            //dbsession.close();
                                %>

                                <option value="<%out.println(roleid);%>"><%out.println(roleid);%></option>
                                <%}
                                    }
                                    dbsession.close();
                                %>

                            </select> 
                        </td>
                    </tr>

                    <tr class="">
                        <td colspan="3" class="button" align="right"><input type="reset" value="Reset" name="reset"><input type="submit" value="Submit" name="submit" onclick="selectAll(document.adduserrole.SecondList);"></td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" size="50" id="entryUserID" name="entryUserID" value="<% out.println(username);%>">
            <input type="hidden" size="50" id="entryTerm" name="entryTerm" value="<%out.println(hostname);%>">
            <input type="hidden" size="50" id="entryIP" name="entryIP" value="<%out.println(ipAddress);%>">
            <input type="hidden" name="rowcount" id="rowcount" value=<% out.println(rownum);%>>
        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
