<%-- 
    Document   : scSupplierList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">
<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    //response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>
<%
    Session dbsession = null;
    String qryparam = "";

    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }
%>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>


<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
    }
    .bp_valid {
        color:green;
    }
</style>

<%
    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp","_top","");
            }
        </script>

        <script language="javaScript" type="text/javascript">
            function move(fbox,tbox)
            {
                var arrFbox = new Array();
                var arrTbox = new Array();
                var arrLookup = new Array();

                var i;
                for(i=0; i<tbox.options.length; i++)
                {
                    arrLookup[tbox.options[i].text] = tbox.options[i].value;
                    arrTbox[i] = tbox.options[i].text;
                }
                var fLength =0;
                var tLength = arrTbox.length;

                for(i=0; i<fbox.options.length; i++)
                {
                    arrLookup[fbox.options[i].text] = fbox.options[i].value;
                    if(fbox.options[i].selected && fbox.options[i].value!="")
                    {
                        arrTbox[tLength] = fbox.options[i].text;
                        // alert(fbox.name);
                        tLength++;
                    }
                    else
                    {
                        arrFbox[fLength] = fbox.options[i].text;
                        fLength++;
                    }
                }
                //                arrFbox.sort();
                //              arrTbox.sort();
                fbox.length = 0;
                tbox.length = 0;
                var c;
                for(c=0; c<arrFbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrFbox[c]];
                    no.text = arrFbox[c];
                    fbox[c] = no;
                }
                for(c=0; c<arrTbox.length; c++)
                {
                    var no = new Option();
                    no.value = arrLookup[arrTbox[c]];
                    no.text = arrTbox[c];
                    tbox[c] = no;
                }
            }
            function selectAll(box)
            {
                for(var i=0; i<box.length; i++)
                {
                    box[i].selected = true;
                }
            }
            
            function changePage()
            {
                if(window.XMLHttpRequest)
                {
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {
                    xmlhttp = new ActiveXObject("Microsoft.XHLHTTP");
                }

                xmlhttp.onreadystatechange = function()
                {
                    if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        var L = document.getElementById("templist");
                        L.innerHTML = xmlhttp.responseText;
                    }
                }

                var url = "getCourse.jsp?semisterId="+semisterId+"";
                xmlhttp.open("Get",url,true);
                xmlhttp.send();
            }
                

        </script>


        <title>Add New Supplier</title>
        <%
            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }
            //                   out.println(username);
            //                   out.println(logstat);
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }
            String read = request.getParameter("read");
            String write = request.getParameter("write");
            /*
            out.println(username);
            out.println(read);
            out.println(write);
             */
        %>

    </head>
    <body onLoad="breakOut()" > <%-- onLoad="init()" --%>
        <%
            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            Query q1 = null;

            SyCity sycity = null;
            SyCountry sycountry = null;
            int rownum = 0;
            String oddeven = null;
            getRegistryID getregid = new getRegistryID();
            String regID = null;
            int regCode = 5;
            regID = getregid.getID(regCode);


        %>
        <script type="text/javascript">
            function dataValidate(){
                var form = document.getElementById("editrole");

                if(form.roleId.value.toString()==null || form.roleId.value.toString()==""){
                    alert("Missing required data - Role ID");
                    return false;
                }

                if(form.roleName.value.toString()==null || form.roleName.value.toString()==""){
                    alert("Missing required data - Role Title");
                    return false;
                }
                if(form.SecondList.value.toString()==null || form.SecondList.value.toString()==""){
                    alert("Must Grant a Data From List box");
                    return false;
                }

     
                return true;
            }
        </script>
        <%
            String roleid = request.getParameter("roleid").trim();
            String rolename = request.getParameter("rolename");
        %>
        <script type="text/javascript">
            function buttonAvailable(box)
            {
                var msg = document.getElementById("FirstList");
                var form = document.getElementById("addrole");
                
                for(var i=0; i<box.length; i++)
                {
                    box.selected= true;
                    var revokeButton = document.getElementById("editrole").revoke;
                    revokeButton.disabled = true;
                    var grantButton = document.getElementById("editrole").grant;
                    grantButton.disabled = false;
                }
            }
                
            function buttonAvailableSecond(boxSecond)
            {
                var msg = document.getElementById("FirstList");
                var form = document.getElementById("addrole");
                
                for(var i=0; i<boxSecond.length; i++)
                {
                    boxSecond[i].selected = true;
                    var grantButton = document.getElementById("editrole").grant;
                    grantButton.disabled = true;
                    var revokeButton = document.getElementById("editrole").revoke;
                    revokeButton.disabled = false;
                }
            }
            
        </script>


        <h1>Edit Role</h1>
        <br>
        <form name="editrole" id="editrole" method="post" action="syRole_check.jsp?sessionid=<%out.println(sessionid);%>" onSubmit="return dataValidate()">
            <table class="tab1">
                
            <thead>
                <tr >
                    <th align="left"><b>Property</b></th>
                    <th align="left"><b>Value</b></th>
                    
                </tr>
            </thead>
                    <tr >
                        <td align="left" width="100">Role ID <font color="red">*</font></td>
                        <td align="left" width="300"><input size="50" type="hidden" value="<%out.println(roleid);%>" id="roleId" name="roleId"><%out.println(roleid);%></td>
                    </tr>

                    <tr >
                        <td align="left">Role Title <font color="red">*</font></td>
                        <td align="left" width="300"><input size="50" type="hidden" value="<%out.println(rolename);%>" id="roleName" name="roleName"><%out.println(rolename);%></td>
                    
                        <input type="hidden" size="50" id="entryUserID" name="entryUserID" value="<% out.println(username);%>">
                        <input type="hidden" size="50" id="entryTerm" name="entryTerm" value="<%out.println(InetAddress.getLocalHost().getHostName().toString());%>">
                        <input type="hidden" size="50" id="entryIP" name="entryIP" value="<%out.println(InetAddress.getLocalHost().getHostAddress().toString());%>">
                    </tr>

                </tbody>
            </table>
            
                    <table class="tab1">
                <tr style="color:red; font-size: 15px"><td>Note: You need to choose complete hierarchy (parents) to get any action works.</td></tr>
            </table>
                    <table class="tab" id="roleact">
 
                    <tr>
                        <td align="center"><font color="black" size="3">Available Role</font></td>
                        <td></td>
                        <td></td>
                        <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="black" size="3">Active Role</font></td>
                    </tr>

                    <tr align="center" valign="middle" class="">
                        <td align="right">
                            <!--                            <span id="templist"></span>-->
                            <select name="FirstList" size="40" multiple id="FirstList" style="width: 250px;" onchange="buttonAvailable('FirstList')">
                                <%
                                    SyActivity syactivity = null;
                                    String actid = null;
                                    String actdesc = null;
                                    String readallow = "";
                                    String writeallow = "";
                                    String readcheck = "";
                                    String writecheck = "";
                                    rownum = 0;


                                    Query q4 = null;
                                    Query q5 = null;


                                    Query q3 = dbsession.createQuery("from SyActivity as a where a.actId not in (select roleact.id.actId from SyRoleAct roleact where roleact.id.roleId ='" + roleid + "')");
                                    for (Iterator itr = q3.list().iterator(); itr.hasNext();) {
                                        rownum += 1;
                                        syactivity = (SyActivity) itr.next();
                                        actid = syactivity.getActId();
                                        actdesc = syactivity.getActDesc();


                                %>
                                <option value="<%out.println(actid);%>"><%out.println(actid);%>:&nbsp;<%out.println(actdesc);%></option>
                                <%
                                    }%>
                            </select>
                        </td>


                        <td>
                            <input type="button" onclick="move(this.form.SecondList,this.form.FirstList)" value="Revoke" name="revoke" style="height: 30px">
                        </td>
                        <td>
                            <input type="button" onclick="move(this.form.FirstList,this.form.SecondList)" value="Grant" name="grant" style="height: 30px">
                        </td>
                        <td align="left">
                            <span id="templist"></span>
                            <select name="SecondList" size="40" multiple id="SecondList" style="width: 250px;" onchange="buttonAvailableSecond('SecondList')">
                                <%

                                    Query q2 = null;
                                    q2 = dbsession.createQuery("from SyRoleAct roleact where roleact.id.roleId='" + roleid + "'");
                                    for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                        SyRoleAct roleact = (SyRoleAct) itr2.next();
                                        actid = roleact.getId().getActId();
                                        q1 = dbsession.createQuery("from SyActivity as a where a.actId='" + actid + "'");

                                        for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                            rownum += 1;
                                            syactivity = (SyActivity) itr.next();

                                            actdesc = syactivity.getActDesc();


                                %>
                                <option value="<%out.println(actid);%>"><%out.println(actid);%>:&nbsp;<%out.println(actdesc);%></option>
                                <% }
                                    }
                                    dbsession.close();
                                %>
                            </select> 
                            <input type="hidden" name="rowcount" id="rowcount" value=<% out.println(rownum);%>>
                        </td>
                    </tr>
                <tr>
                    <td class="label" align="right"></td>
                    <td class="button" align="right"><input type="reset" value="Reset" name="reset"><input type="submit" value="Submit" name="submit" onclick="selectAll(document.editrole.SecondList);"></td>
                    <td class="label" align="right"></td>
                    <td class="label" align="right"></td>
                </tr>
                
                
            </table>                    
            
        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
