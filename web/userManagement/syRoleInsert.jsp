<%-- 
    Document   : dataInsert
    Created on : Nov 20, 2010, 4:53:05 AM
    Author     : Akter
--%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.*" %>
<%@page import="java.net.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">

 
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>




<script src="../js/jquery.autocomplete.js"></script>
<link rel="stylesheet" type="text/css" href="../css/jquery.autocomplete.css" />
<script src="../jquery/jquery-ui.js"></script>
<link rel="stylesheet" href="../jquery/jquery-ui.css" />


<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">


<%
    String roleid = request.getParameter("roleid").trim().toUpperCase();
    String rolename = request.getParameter("rolename");
    String adduser = request.getParameter("adduser");
    String addterm = request.getParameter("addterm");
    String addip = request.getParameter("addip");

    String actid[] = request.getParameterValues("actid");
//String actdesc[]=request.getParameterValues("actdesc");
    String readval[] = request.getParameterValues("readval");
    String writeval[] = request.getParameterValues("writeval");

    boolean update = false;
    //          out.println(request.getParameter("roleid"));

    getDate date = new getDate();

    dbsession = HibernateUtil.getSessionFactory().openSession();
    
    dbtrx = dbsession.beginTransaction();

    //out.println(roleid);
    SyRoles syrole = null;
    Query q1 = dbsession.createQuery("from SyRoles as syroles where upper(roleId) = '" + roleid + "'");
    for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
        syrole = (SyRoles) itr1.next();
        out.println(syrole.getRoleId());
        update = true;
    }
    //out.println(update);
    //try {

    if (update == true) {
        syrole.setRoleDesc(rolename.trim());
        //syrole.setAddDate(date.serverDate());
        //syrole.setAddUsr(adduser.trim());
        //syrole.setAddTerm(addterm.trim());
        //syrole.setAddIp(addip.trim());
        syrole.setModUsrId(adduser.trim());
        syrole.setModTerm(addterm.trim());
        syrole.setModIp(addip.trim());
        syrole.setLastModDate(date.serverDate());
        dbsession.update(syrole);

        Query q2 = dbsession.createQuery("from SyRoleAct roleact where roleact.id.roleId ='" + roleid + "'");
        SyRoleActId roleactid = null;

        for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
            SyRoleAct syroleact = (SyRoleAct) itr2.next();
            //roleactid=syroleact.getId();
            dbsession.delete(syroleact);
        }

    } else {
        syrole = new SyRoles();
        syrole.setRoleId(roleid.trim());
        syrole.setRoleDesc(rolename.trim());
        syrole.setAddDate(date.serverDate());
        syrole.setAddUsr(adduser.trim());
        syrole.setAddTerm(addterm.trim());
        syrole.setAddIp(addip.trim());
        syrole.setModUsrId(adduser.trim());
        syrole.setModTerm(addterm.trim());
        syrole.setModIp(addip.trim());
        syrole.setLastModDate(date.serverDate());
        dbsession.save(syrole);

    }
    SyRoleActId roleactid = null;
//    int i = 0;
//    int j = 0;
    for (i = 0; i < actid.length; i++) {

        SyRoleAct syroleact = new SyRoleAct();
        roleactid = new SyRoleActId(roleid.trim(), actid[i].trim());

        syroleact.setId(roleactid);
        syroleact.setActRead("Y");
        syroleact.setActWrite("Y");
        syroleact.setAddDate(date.serverDate());
        syroleact.setAddUsr(adduser.trim());
        syroleact.setAddTerm(addterm.trim());
        syroleact.setAddIp(addip.trim());
        syroleact.setModUsrId(adduser.trim());
        syroleact.setModTerm(addterm.trim());
        syroleact.setModIp(addip.trim());
        syroleact.setLastModDate(date.serverDate());
        dbsession.save(syroleact);
    }

    SyAuditTrail audit = new SyAuditTrail();
    getRegistryID regId = new getRegistryID();
    getDate gdate = new getDate();
    String eventid = "";
    eventid = regId.getID(16);
    BigDecimal eventId = new BigDecimal(eventid);

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    audit.setEvtId(eventId);
    audit.setEvtDatetime(gdate.serverDate());
    audit.setEvtEmpId(userNameH);
    if (update = !true) {
        audit.setEvtCode("ROLE-ADD");
        audit.setEvtDetail("Audit for ROLE CREATION");
    } else {
        audit.setEvtCode("ROLE-EDIT");
        audit.setEvtDetail("Audit for ROLE-edit");

    }
    audit.setEvtRef01("");
    audit.setEvtRef02("");
    audit.setEvtRef03("");
    audit.setEvtRef04("");
    audit.setEvtRef05(hostname + "," + ipAddress);
    dbsession.save(audit);

    dbsession.flush();
    dbtrx.commit();
    dbsession.close();

    if (dbtrx.wasCommitted()) {
        response.sendRedirect("success.jsp?sessionid=" + sessionid);
    } else {
        dbtrx.rollback();
        response.sendRedirect("fail.jsp?sessionid=" + sessionid);
    }
  
    String strMsg ="";
 if (dbtrx.wasCommitted()) {
        strMsg = "Sucessfully Executed!";
        response.sendRedirect("syRoleAdd.jsp?sessionid=" + sessionid + "&strMsg=" + strMsg); 
    } else {
        dbtrx.rollback();
        strMsg = "Submit is unsuccessful";
        response.sendRedirect("syRoleAdd.jsp?sessionid=" + sessionid + "&strMsg=" + strMsg);
    }

%>
