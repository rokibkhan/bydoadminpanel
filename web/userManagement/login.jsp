<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("storeId").toString();

    //  String sessionidH = request.getParameter("sessionid").trim();
    //  sessionidH = session.getId();
//    String sessionIdH = session.getId();
//    System.out.println("LogIn sessionIdH :: " + sessionIdH);
    //   System.out.println("LogIn getAttribute(username) :: " + session.getAttribute("username"));
//    String userNameH = session.getAttribute("username").toString(); 
//    System.out.println("LogIn userNameH :: "+userNameH);

    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        String sessionIdH = session.getId();
        System.out.println("LogIn sessionIdH :: " + sessionIdH);
        
        String userNameH = session.getAttribute("username").toString();
        System.out.println("LogIn userNameH :: " + userNameH);
        
        String userStoreIdH = session.getAttribute("storeId").toString();
        System.out.println("LogIn userStoreIdH :: " + userStoreIdH);
        
        response.sendRedirect(GlobalVariable.baseUrl+"/home.jsp?sessionid="+sessionIdH);

    } else {
        System.out.println("LogIn Required :: ");
    }

//    String userIdH = session.getAttribute("username").toString().toUpperCase(); 
//    String userStoreIdH = session.getAttribute("storeId").toString();
//    
//System.out.println("sessionIdH :: "+sessionIdH);
//    
//System.out.println("userIdH :: "+userIdH);
//
//// response.sendRedirect(GlobalVariable.baseUrl+"/userManagement/login.jsp");
//    
//if(!userIdH.equals("") && !sessionIdH.equals("")){
//    
//    System.out.println("Not equals :: ");
//}else{
//
//System.out.println("Equals :: ");
//}
%>


<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/login-icon-logo-en---.png">
        <title>ByDoAcademy Admin Login</title>
        <!-- Bootstrap Core CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/colors/blue.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/html5shiv/html5shiv.js"></script>
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/html5shiv/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="login-register">
            <div class="login-box">
                <div class="white-box">

                    <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/loginServlet">


                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <div class="user-thumb text-center"> 
                                    <!--<img class="img-circle" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/rupantor-icon-logo-en.png" alt="home" />-->

                                    <h3><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/logo_login.png" alt="IEB" /></h3>
                                </div>
                            </div>
                        </div>
                        <h3 class="box-title m-b-20">Sign In</h3>

                        <div style="display: block; font-size:  small;color: red;  " > 
                            <%
                                String rtnUser = "";

                                try {
                                    rtnUser = session.getAttribute("rtnUser").toString();
                                    if (rtnUser.equalsIgnoreCase("1") || rtnUser != null) {
                                        out.println("User or Password does not match! ");
                                    } else {
                                        out.print(" ");
                                    }
                                } catch (Exception ex) {
                                    // out.print(ex.getMessage());
                                }

                            %>

                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input id="username"name="username" class="form-control" type="text" required="" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input   id="password" name="password" class="form-control" type="password" required="" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox checkbox-primary pull-left p-t-0">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup"> Remember me </label>
                                </div>
                                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot password?</a> </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal" id="recoverform" action="index.html">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <h3>Recover Password</h3>
                                <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- jQuery -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/bootstrap/dist/js/tether.min.js"></script>
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/custom.min.js"></script>
        <!--Style Switcher -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    </body>

</html>
