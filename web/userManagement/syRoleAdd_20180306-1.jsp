
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>





<script type="text/javascript">
    function AJAXInteraction(url, callback) {

        var req = init();
        req.onreadystatechange = processRequest;

        function init() {
            if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        function processRequest() {
            // readyState of 4 signifies request is complete
            if (req.readyState == 4) {
                // status of 200 signifies sucessful HTTP call
                if (req.status == 200) {
                    if (callback)
                        callback(req.responseXML);
                }
            }
        }

        this.doGet = function () {
            // make a HTTP GET request to the URL asynchronously
            req.open("GET", url, true);
            req.send(null);
        }
    }

    function validateRoleId() {
        var form = document.getElementById("addrole");
        var target = document.getElementById("roleId");
        var url = "validate?id=" + encodeURIComponent(target.value) + "&key=roleid";
        var ajax = new AJAXInteraction(url, validateCallback);
        ajax.doGet();
    }

    function validateCallback(responseXML) {
        // see "The Callback Function" below for more details
        var form = document.getElementById("addrole");
        var msg = responseXML.
                getElementsByTagName("valid")[0].firstChild.nodeValue;


        if (msg == "false") {

            var mdiv = document.getElementById("roleIdMessage");
            //var mdiv=form.roleIdMessage;

            // set the style on the div to invalid

            mdiv.className = "bp_invalid";
            //mdiv.style.color= red;
            mdiv.innerHTML = "Duplicate ID";
            var submitBtn = document.getElementById("addrole").submit;
            submitBtn.disabled = true;

        } else {

            var mdiv = document.getElementById("roleIdMessage");
            //var mdiv=form.roleIdMessage;
            // set the style on the div to valid

            mdiv.className = "bp_valid";
            mdiv.innerHTML = "Valid ID";
            var submitBtn = document.getElementById("addrole").submit;
            submitBtn.disabled = false;
        }

    }
</script>

<%            String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%            dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
//            SyCity sycity = null;
//            SyCountry sycountry = null;

    int rownum = 0;
    String oddeven = null;
    getRegistryID getregid = new getRegistryID();
    String regID = null;
    int regCode = 5;
    regID = getregid.getID(regCode);


%>
<script type="text/javascript">
    function dataValidate() {
        var form = document.getElementById("addrole");

        if (form.roleId.value.toString() == null || form.roleId.value.toString() == "") {
            alert("Missing required data - Role ID");
            return false;
        }

        if (form.roleName.value.toString() == null || form.roleName.value.toString() == "") {
            alert("Missing required data - Role Title");
            return false;
        }
        if (form.SecondList.value.toString() == null || form.SecondList.value.toString() == "") {
            alert("Must Grant a Data From List box");
            return false;
        }

        return true;
    }
</script>


<script language="javaScript" type="text/javascript">
    function move(fbox, tbox)
    {
        var arrFbox = new Array();
        var arrTbox = new Array();
        var arrLookup = new Array();

        var i;
        for (i = 0; i < tbox.options.length; i++)
        {
            arrLookup[tbox.options[i].text] = tbox.options[i].value;
            arrTbox[i] = tbox.options[i].text;
        }
        var fLength = 0;
        var tLength = arrTbox.length;

        for (i = 0; i < fbox.options.length; i++)
        {
            arrLookup[fbox.options[i].text] = fbox.options[i].value;
            if (fbox.options[i].selected && fbox.options[i].value != "")
            {
                arrTbox[tLength] = fbox.options[i].text;
                tLength++;
                //                        var grantButton = document.getElementById("addrole").submit;
                //                        grantButton.disabled = true;

            } else
            {
                arrFbox[fLength] = fbox.options[i].text;
                fLength++;
            }
        }
        //arrFbox.sort();
        //arrTbox.sort();
        fbox.length = 0;
        tbox.length = 0;
        var c;
        for (c = 0; c < arrFbox.length; c++)
        {
            var no = new Option();
            no.value = arrLookup[arrFbox[c]];
            no.text = arrFbox[c];
            fbox[c] = no;
        }
        for (c = 0; c < arrTbox.length; c++)
        {
            var no = new Option();
            no.value = arrLookup[arrTbox[c]];
            no.text = arrTbox[c];
            tbox[c] = no;
        }
    }
    function selectAll(box)
    {
        for (var i = 0; i < box.length; i++)
        {
            box[i].selected = true;
        }
    }

    function GetCourse(semisterId)
    {
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        } else
        {
            xmlhttp = new ActiveXObject("Microsoft.XHLHTTP");
        }

        xmlhttp.onreadystatechange = function ()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                var L = document.getElementById("templist");
                L.innerHTML = xmlhttp.responseText;
            }
        }

        var url = "getCourse.jsp?semisterId=" + semisterId + "";
        xmlhttp.open("Get", url, true);
        xmlhttp.send();
    }


</script>
<script type="text/javascript">


    function buttonValid() {
        var arrFbox = new Array();
        var arrTbox = new Array();
        var arrLookup = new Array();

        var i;
        for (i = 0; i < tbox.options.length; i++)
        {
            arrLookup[tbox.options[i].text] = tbox.options[i].value;
            arrTbox[i] = tbox.options[i].text;
        }
        var fLength = 0;
        var tLength = arrTbox.length;

        for (i = 0; i < fbox.options.length; i++)
        {
            arrLookup[fbox.options[i].text] = fbox.options[i].value;
            if (fbox.options[i].selected && fbox.options[i].value != "")
            {
                arrTbox[tLength] = fbox.options[i].text;
                tLength++;
                //                        var grantButton = document.getElementById("addrole").submit;
                //                        grantButton.disabled = true;

            } else
            {
                arrFbox[fLength] = fbox.options[i].text;
                fLength++;
            }
        }
        //arrFbox.sort();
        //arrTbox.sort();
        fbox.length = 0;
        tbox.length = 0;
        var c;
        for (c = 0; c < arrFbox.length; c++)
        {
            var no = new Option();
            no.value = arrLookup[arrFbox[c]];
            no.text = arrFbox[c];
            fbox[c] = no;
        }
        for (c = 0; c < arrTbox.length; c++)
        {
            var no = new Option();
            no.value = arrLookup[arrTbox[c]];
            no.text = arrTbox[c];
            tbox[c] = no;
        }
    }


</script>

<%    dbsession = HibernateUtil.getSessionFactory().openSession();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Role</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">User Management</a></li>
                    <li class="active">Add Role</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <form name="addrole" id="addrole" method="post" action="syRole_check.jsp?sessionid=<%out.print(sessionid);%>" onSubmit="return dataValidate()" class="form-horizontal">

                     
                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="roleId" class="control-label col-sm-3">Role ID</label>
                            <div class="col-sm-4">
                                <input type="text" id="roleId" name="roleId"  placeholder="Role ID" class="form-control input-sm" required>
                                <div  id="roleIdErr" class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="roleName" class="control-label col-sm-3">Role Title</label>
                            <div class="col-sm-4">
                                <input type="text" id="roleName" name="roleName" placeholder="Role Title" class="form-control input-sm" required>
                                <div id="roleNameErr" class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row custom-bottom-margin-5x">

                            <script type="text/javascript">
                                function buttonAvailable(box)
                                {
                                    var msg = document.getElementById("FirstList");
                                    var form = document.getElementById("addrole");

                                    for (var i = 0; i < box.length; i++)
                                    {
                                        box.selected = true;
                                        var revokeButton = document.getElementById("addrole").revoke;
                                        revokeButton.disabled = true;
                                        var grantButton = document.getElementById("addrole").grant;
                                        grantButton.disabled = false;
                                    }
                                }

                                function buttonAvailableSecond(boxSecond)
                                {
                                    var msg = document.getElementById("FirstList");
                                    var form = document.getElementById("addrole");

                                    for (var i = 0; i < boxSecond.length; i++)
                                    {
                                        boxSecond[i].selected = true;
                                        var grantButton = document.getElementById("addrole").grant;
                                        grantButton.disabled = true;
                                        var revokeButton = document.getElementById("addrole").revoke;
                                        revokeButton.disabled = false;
                                    }
                                }

                            </script>
                            <div class="col-sm-9 offset-2">
                                <h4 class="text-success text-center1">Note: You need to choose complete hierarchy (parents) to get any action works.</h4>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5>Available Role Activity</h5>
                                        <select class="form-control customInput-sm" name="FirstList" multiple id="FirstList" style="width: 250px1;  height: 700px;" onchange="buttonAvailable('FirstList')">
                                            <%
                                                SyActivity syactivity = null;
                                                String actid = null;
                                                String actdesc = null;
                                                String readallow = "";
                                                String writeallow = "";
                                                String readcheck = "";
                                                String writecheck = "";
                                                rownum = 0;

                                                q1 = dbsession.createQuery("from SyActivity as activity order by actId asc");

                                                for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                                    rownum += 1;
                                                    syactivity = (SyActivity) itr.next();
                                                    actid = syactivity.getActId();
                                                    actdesc = syactivity.getActDesc();

                                            %>
                                            <option value="<%out.println(actid);%>"><%out.println(actid);%>:&nbsp;<%out.println(actdesc);%></option>
                                            <%
                                                }
                                                dbsession.close();

                                            %>
                                        </select>
                                        
                                    </div>
                                        <div class="col-md-2" style="padding-top: 300px;">
                                        <input class="btn btn-sm btn-info" type="button" onclick="move(this.form.SecondList, this.form.FirstList)" value="Revoke" name="revoke" style="height: 30px">
                                        <input class="btn btn-sm btn-warning" type="button" onclick="move(this.form.FirstList, this.form.SecondList)" value="Grant" name="grant" style="height: 30px">
                                    </div>
                                    <div class="col-md-4"> 
                                        <h5>Active Role Activity</h5>
                                        <span id="templist"></span>
                                        <select class="form-control input-sm customInput-sm" name="SecondList" size="40" multiple id="SecondList" style="width: 250px1; height: 700px;" onchange="buttonAvailableSecond('SecondList')">

                                        </select> 
                                    </div>                              
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 offset-5 text-center1">
                                <br>
                         
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<% //out.print(username);%>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<% //out.print(hostname);%>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<% //out.print(ipAddress);%>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <input type="hidden" name="rowcount" id="rowcount" value="<% out.println(rownum);%>">
                                <input type="reset" value="Reset" name="reset" class="btn btn-danger">
                                <input type="submit" value="Submit" name="submit" onclick="selectAll(document.addrole.SecondList);" class="btn btn-info">
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>