
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">



<script>





</script>
<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    String tabProfileActive = "";
    String tabActivityActive = "";
    String tabMessagesActive = "";
    String tabSettingsActive = "";
    String tabChangePassActive = "";
    String selUserId = "";
    String selUserName = "";
    String selUserEmail = "";
    String selUserMoble = "";
    String selUserDeptId = "";
    String selUserDept = "";
    String selectedUserId = request.getParameter("userIdX") == null ? "" : request.getParameter("userIdX").trim();

    System.out.println("selectedUserId :: " + selectedUserId);

    if (!selectedUserId.equals("")) {

        System.out.println("selectedUserId IN :: " + selectedUserId);

        Query qU = dbsession.createQuery("from SyUser syUs WHERE syUs.userId ='" + selectedUserId + "'");
        SyUser syUser = null;
        for (Iterator itr = qU.list().iterator(); itr.hasNext();) {
            syUser = (SyUser) itr.next();
            selUserId = syUser.getUserId();
            selUserName = syUser.getUserName();
            selUserEmail = syUser.getUserEmail();
            selUserMoble = syUser.getUserMsisdn();
            selUserDeptId = syUser.getSyDept().getDeptId();
            selUserDept = syUser.getSyDept().getDeptName();

        }

//        dbsession.flush();
//        dbsession.close();
        String selectedTab = request.getParameter("selectedTab") == null ? "" : request.getParameter("selectedTab").trim();
        if (!selectedTab.equals("")) {

            if (selectedTab.equals("profile")) {
                tabProfileActive = "active";
            } else if (selectedTab.equals("activity")) {
                tabActivityActive = "active";
            } else if (selectedTab.equals("messages")) {
                tabMessagesActive = "active";
            } else if (selectedTab.equals("settings")) {
                tabSettingsActive = "active";
            } else if (selectedTab.equals("changePass")) {
                tabChangePassActive = "active";
            }

        } else {
            tabProfileActive = "active";
        }
    } else {

        System.out.println("selectedUserId OUt ::");

        //  response.sendRedirect(GlobalVariable.baseUrl + "/home.jsp");
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><%out.print(selUserId);%> Profile Details</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">User Management</a></li>
                    <li class="active">User Profile</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="col-md-4 col-xs-12">
                    <div class="white-box">
                        <div class="user-bg"> <img width="100%" alt="user" src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/large/img1.jpg">
                            <div class="overlay-box">
                                <div class="user-content">
                                    <a href="javascript:void(0)"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/no_image.jpg" class="thumb-lg img-circle" alt="img"></a>
                                    <h4 class="text-white"><% out.print(selUserName);%></h4>
                                    <h5 class="text-white"><% out.print(selUserEmail);%></h5> </div>
                            </div>
                        </div>
                        <div class="user-btm-box">
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-purple"><i class="ti-facebook"></i></p>
                                <h1>258</h1> </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-blue"><i class="ti-twitter"></i></p>
                                <h1>125</h1> </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-danger"><i class="ti-dribbble"></i></p>
                                <h1>556</h1> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="white-box">
                        <ul class="nav customtab nav-tabs" role="tablist">                            
                            <li role="presentation" class="nav-item"><a href="#profile" class="nav-link <% out.print(tabProfileActive); %>" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profile</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#activity" class="nav-link <% out.print(tabActivityActive); %>" aria-controls="activity" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Activity</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#messages" class="nav-link <% out.print(tabMessagesActive); %>" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Message</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#settings" class="nav-link <% out.print(tabSettingsActive); %>" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Setting</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#changePass" class="nav-link <% out.print(tabChangePassActive); %>" aria-controls="changePass" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Change Password</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <% out.print(tabProfileActive); %>" id="profile">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                        <br>
                                        <p class="text-muted"><%out.print(selUserName);%></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                        <br>
                                        <p class="text-muted"><%out.print(selUserMoble);%></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted"><%out.print(selUserEmail);%></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Department</strong>
                                        <br>
                                        <p class="text-muted"><%out.print(selUserDept);%></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal form-material1" id="updateProfileFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">User Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserId" name="syUserId" value="<%out.print(selUserId);%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserName" name="syUserName" value="<%out.print(selUserName);%>" placeholder="Full Name" class="form-control input-sm" required>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Email</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserEmail" name="syUserEmail" value="<%out.print(selUserEmail);%>" placeholder="Email" class="form-control input-sm" required>
                                                    <div id="syUserEmailErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Mobile</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserMobile" name="syUserMobile" value="<%out.print(selUserMoble);%>" placeholder="Mobile" class="form-control input-sm" required>
                                                    <div id="syUserMobileErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label class="control-label col-md-3">Department</label>
                                                <div class="col-md-6">
                                                    <select class="form-control input-sm customInput-sm" style="height: 30px;" id="syUserDept" name="syUserDept">
                                                        <option value="">SELECT</option>
                                                        <%
                                                            String dept1 = "";
                                                            String deptSelected = "";
                                                            Query q1 = dbsession.createQuery("from SyDept dept order by deptName asc");
                                                            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                                                                SyDept dept = (SyDept) itr1.next();
                                                                dept1 = dept.getDeptId().trim();

                                                                if (dept1.equals(selUserDeptId.trim())) {

                                                                    deptSelected = "selected";

                                                                } else {
                                                                    deptSelected = "";
                                                                }

                                                        %> 
                                                        <option  value="<% out.print(dept.getDeptId().trim());%>"  <% out.print(deptSelected);%>> <% out.print(dept.getDeptName());%> </option> 
                                                        <%
                                                            }

                                                        %>

                                                    </select>
                                                    <div id="syUserDeptErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="control-label col-sm-3"></label>
                                                <div class="col-sm-offset-3 col-sm-8">
                                                    <button id="btnUpdateProfileFrm" type="submit" class="btn btn-info">Update</button>
                                                </div>
                                            </div>
                                        </form> 
                                    </div>
                                </div>

                                <h4 class="font-bold1 m-t-30">Role Info</h4>
                                <hr>
                                <div class="row">
                                    <script type="text/javascript">

                                        function showAddNewSystemRoleInfo(arg1, arg2) {
                                            console.log("User :: " + arg1);

                                            var argopt = 'ASD';
                                            var bodyCon = '<label class="control-label col-md-3">Role Info</label>'
                                                    + '<div class="col-md-6">'
                                                    + '<div class="col-md-6" style="padding-left:1px;padding-right:1px;">'
                                                    + '<input id="roleName" id="roleName" type="text" class="form-control input-sm" placeholder="Role name" aria-describedby="roleName">'
                                                    + '<div id="roleNameErr" class="help-block with-errors"></div>'
                                                    + '</div>'
                                                    + '<div class="col-md-6" style="padding-left:1px;padding-right:1px;">'
                                                    + '<input id="roleDesc" id="roleDesc" type="text" class="form-control input-sm" placeholder="Role description" aria-describedby="roleDesc">'
                                                    + '<div id="roleDescErr" class="help-block with-errors"></div>'
                                                    + '</div>'
                                                    + '<div id="roleErr" class="help-block with-errors"></div>'
                                                    + '</div>'
                                                    + '<div class="col-md-2" style="padding-left:1px;padding-right:1px;">'
                                                    + '<a id="btnAddNewSystemRoleInfo" class="btn btn-primary btn-sm" style="cursor:pointer;color:#fff" onclick="addNewSystemRoleInfo(\'' + arg1 + '\',\'' + arg2 + '\')">Confirm</a>'
                                                    + '</div>';

                                            $("#syRoleContainerBox").html(bodyCon);
                                            $("#btnAddOrChangeSystemUserRoleInfo").hide();

                                        }

                                        function addNewSystemRoleInfo(arg1, arg2) {
                                            console.log("addNewSystemRoleInfo :: " + arg1);
                                            $("#btnAddNewSystemRoleInfo").button("loading");
                                            var roleName = $("#roleName").val();
                                            var roleDesc = $("#roleDesc").val();
                                            var syUserRole = $("#syUserRole").val();
                                            console.log("roleName :: " + roleName);
                                            console.log("roleDesc :: " + roleDesc);
                                            if (roleName == "" || roleName == 0) {
                                                $("#roleName").focus();
                                                $("#roleNameErr").html("Role name is required");
                                                $("#btnAddNewSystemRoleInfo").button("reset");
                                                return false;
                                            } else {
                                                $("#roleNameErr").html("");
                                            }
                                            if (roleDesc == "" || roleDesc == 0) {
                                                $("#roleDesc").focus();
                                                $("#roleDescErr").html("Description is required");
                                                $("#btnAddNewSystemRoleInfo").button("reset");
                                                return false;
                                            } else {
                                                $("#roleDescErr").html("");
                                            }
                                            $.post("syRoleAddInfo.jsp", {userId: arg1, sessionid: arg2, uRole: syUserRole, roleName: roleName, roleDesc: roleDesc},
                                                    function (data) {


                                                        console.log(data);
                                                        if (data[0].responseCode == 1) {
                                                            $("#syRoleContainerBox").html(data[0].allRoleInfo);
                                                            $("#btnAddOrChangeSystemUserRoleInfo").show();
                                                            $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");
                                                        } else {
                                                            $("#btnAddNewSystemRoleInfo").button("reset");
                                                            $("#roleErr").html(data[0].responseMsgText);
                                                            $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");
                                                        }

                                                    }, "json");

                                        }




                                        function addOrChangeSystemUserRoleInfo(arg1, arg2) {
                                            var roleID, syUserRole, syUserRoleText;
                                            console.log("addOrChangeSystemUserRoleInfo :: " + arg1);
                                            roleID = $("#syUserRoleID").val();
                                            syUserRole = $("#syUserRole").val();
                                            syUserRoleText = $("#syUserRole option:selected").text();
                                            console.log("UserID :: " + arg1);
                                            console.log("roleID :: " + roleID);
                                            console.log("syUserRole :: " + syUserRole);
                                            console.log("syUserRoleText :: " + syUserRoleText);

                                            if (syUserRole == "" || syUserRole == 0) {
                                                $("#syUserRole").focus();
                                                $("#syUserRoleErr").html("Role name is required");
                                                $("#btnAddOrChangeSystemUserRoleInfo").button("reset");
                                                return false;
                                            } else {
                                                $("#syUserRoleErr").html("");
                                            }
                                            if (roleID == syUserRole) {
                                                $("#syUserRole").focus();
                                                $("#syUserRoleErr").html("New role same as exiting role");
                                                $("#btnAddOrChangeSystemUserRoleInfo").button("reset");
                                                return false;
                                            } else {
                                                $("#syUserRoleErr").html("");
                                            }


                                            $("#btnAddOrChangeSystemUserRoleInfo").button("loading");
                                            $.post("syUserRoleAddInfo.jsp", {userId: arg1, sessionid: arg2, uNewRoleText: syUserRoleText, uNewRole: syUserRole, uOldRole: roleID},
                                                    function (data) {


                                                        console.log(data);
                                                        if (data[0].responseCode == 1) {
                                                            //   $("#syRoleContainerBox").html(data[0].allRoleInfo);

                                                            $("#syUserRoleIDText").val(data[0].roleIDText);
                                                            $("#syUserRoleID").val(data[0].roleID);
                                                            //  $("#syUserRole").val('');   
                                                            $("#roleSelectBoxCon").html(data[0].roleListStr);
                                                            $("#btnAddOrChangeSystemUserRoleInfo").button("reset");
                                                            $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");
                                                        } else {
                                                            $("#btnAddOrChangeSystemUserRoleInfo").button("reset");
                                                            $("#roleErr").html(data[0].responseMsgText);
                                                            $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");
                                                        }

                                                    }, "json");

                                        }

                                    </script>

                                    <%                                        String selUserRoleId = "";
                                        String selUserRoleDesc = "";
                                        Query qUr = dbsession.createQuery("from SyUserRole syUr WHERE syUr.syUser ='" + selUserId + "'");
                                        SyUserRole syUserRole = null;
                                        for (Iterator itr = qUr.list().iterator(); itr.hasNext();) {
                                            syUserRole = (SyUserRole) itr.next();
                                            selUserRoleId = syUserRole.getSyRoles().getRoleId();
                                            selUserRoleDesc = syUserRole.getSyRoles().getRoleDesc();
                                        }

                                    %>
                                    <div class="col-md-12">
                                        <form class="form-horizontal form-material1" id="changeRoleFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">User Role</label>
                                                <div class="col-sm-6">
                                                    <input id="syUserRoleIDText" name="syUserRoleIDText"  type="text" value="<%out.print(selUserRoleDesc);%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <input type="hidden" name="syUserRoleID" id="syUserRoleID" value="<%out.print(selUserRoleId);%>">
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row custom-bottom-margin-5x" id="syRoleContainerBox">
                                                <label class="control-label col-md-3">Role</label>
                                                <div class="col-md-6" id="roleSelectBoxCon">
                                                    <select class="form-control input-sm customInput-sm" style="height: 30px;" id="syUserRole" name="syUserRole">
                                                        <option value="">SELECT ROLE</option>
                                                        <%
                                                            String roleID = "";
                                                            String roleDesc = "";
                                                            Query qr = dbsession.createQuery("from SyRoles WHERE roleId !='" + selUserRoleId + "' AND roleId !='ADMIN_ROLE' order by roleId asc");
                                                            for (Iterator itr1 = qr.list().iterator(); itr1.hasNext();) {
                                                                SyRoles role = (SyRoles) itr1.next();
                                                                roleID = role.getRoleId().trim();
                                                                roleDesc = role.getRoleDesc().trim();
                                                        %> 
                                                        <option  value="<% out.print(roleID);%>"> <% out.print(roleDesc);%> </option> 
                                                        <%
                                                            }
                                                            dbsession.flush();
                                                            dbsession.close();
                                                        %>

                                                    </select>
                                                    <div id="syUserRoleErr" class="help-block with-errors"></div>
                                                </div>
                                                <div class="col-sm-2"><a onclick="showAddNewSystemRoleInfo('<% out.print(selUserId); %>', '<% out.print(session.getId());%>');" title="Add New Role" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i></a></div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-sm-3"></label>
                                                <div class="col-sm-offset-3 col-sm-8">
                                                    <a onclick="addOrChangeSystemUserRoleInfo('<% out.print(selUserId); %>', '<% out.print(session.getId());%>');" id="btnAddOrChangeSystemUserRoleInfo" class="btn btn-info">Add/Change User Role</a>
                                                </div>
                                            </div>
                                        </form> 

                                    </div>                                        
                                </div>


                            </div>
                            <!-- /#profile -->
                            <div class="tab-pane <% out.print(tabActivityActive);%>" id="activity">
                                <div class="steamline1">

                                    <div class="col-md-12">
                                        <div class="form-group row custom-bottom-margin-5x">
                                            <h3>Role: <%=selUserRoleDesc%></h3>

                                        </div>
                                    </div>




                                    <div class="table-responsive">
                                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Activity ID</th>
                                                    <th>Activity Description</th>

                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                                <%
                                                    dbsession = HibernateUtil.getSessionFactory().openSession();
                                                    dbtrx = dbsession.beginTransaction();

                                                    int ix = 1;

                                                    Object[] activityObj = null;

                                                    SyActivity syActivity = null;
                                                    String actid = null;
                                                    String actdesc = null;
                                                    String actParentCheck = "";
                                                    String actParentCheckOpt = "";
                                                    String actParentCheckOpt1 = "";
                                                    // Query q = dbsession.createQuery("from SyActivity as a order by a.actId asc");
                                                    Query activitySQL = dbsession.createSQLQuery("SELECT a.ACT_ID,a.ACT_DESC,a.ACT_PARENT  "
                                                            + " FROM sy_activity a,sy_role_act ra "
                                                            + " WHERE a.ACT_ID = ra.ACT_ID AND ra.ROLE_ID = '" + selUserRoleId + "' "
                                                            + " ORDER BY a.ACT_ID ASC");
                                                    if (!activitySQL.list().isEmpty()) {
                                                        for (Iterator activityItr = activitySQL.list().iterator(); activityItr.hasNext();) {

                                                            activityObj = (Object[]) activityItr.next();
                                                            actid = activityObj[0].toString().trim();
                                                            actdesc = activityObj[1].toString().trim();
                                                            actParentCheck = activityObj[2].toString().trim();

                                                            if (actParentCheck.equals("0")) {
                                                                actParentCheckOpt = "style=\"font-weight: bold;\"";
                                                                actParentCheckOpt1 = "";
                                                            } else {
                                                                actParentCheckOpt = "";
                                                                actParentCheckOpt1 = "&nbsp;";
                                                            }

                                                            // dbtrx.commit();
                                                            //dbsession.close();
                                                %>

                                                <tr >
                                                    <td  <% out.print(actParentCheckOpt);%>><% out.print(ix);%></td>
                                                    <td  <% out.print(actParentCheckOpt);%>>  <% out.print(actParentCheckOpt1);%> <% out.print(actid);%></td>
                                                    <td  <% out.print(actParentCheckOpt);%>> <% out.print(actParentCheckOpt1);%> <% out.print(actdesc);%></td>

                                                    <td class="text-center">
                                                        &nbsp;
                                                    </td>
                                                </tr>

                                                <%
                                                            ix++;

                                                        }
                                                    }
                                                    dbsession.clear();
                                                    dbsession.close();
                                                %>


                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                            <!-- /#profile -->
                            <div class="tab-pane <% out.print(tabMessagesActive); %>" id="messages">
                                <div class="steamline">
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/genu.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"> <a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <div class="m-t-20 row">
                                                    <div class="col-md-2 col-xs-12"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img1.jpg" alt="user" class="img-responsive" /></div>
                                                    <div class="col-md-9 col-xs-12">
                                                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa</p> <a href="#" class="btn btn-success"> Design weblayout</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/sonu.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p>assign a new task <a href="#"> Design weblayout</a></p>
                                                <div class="m-t-20 row"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img1.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img2.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img3.jpg" alt="user" class="col-md-3 col-xs-12" /></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/ritesh.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/govinda.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p>assign a new task <a href="#"> Design weblayout</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /#messages-->
                            <div class="tab-pane <% out.print(tabSettingsActive); %>" id="settings">
                                <form class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12">Full Name</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Password</label>
                                        <div class="col-md-12">
                                            <input type="password" value="password" class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Phone No</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="123 456 7890" class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Message</label>
                                        <div class="col-md-12">
                                            <textarea rows="5" class="form-control form-control-line"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Select Country</label>
                                        <div class="col-sm-12">
                                            <select class="form-control form-control-line">
                                                <option>London</option>
                                                <option>India</option>
                                                <option>Usa</option>
                                                <option>Canada</option>
                                                <option>Thailand</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">Update Profile</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /#settings -->           
                            <div class="tab-pane <% out.print(tabChangePassActive); %>" id="changePass">
                                <script>
                                    $(function () {

                                        //hang on event of form with id=myform
                                        $("#changePassFrm").submit(function (e) {

                                            //prevent Default functionality
                                            e.preventDefault();

                                            var userIdOptX, newPassword, newPassword1, passwordOptToken;

                                            userIdOptX = $("#userIdOptX").val();
                                            newPassword = $("#newPassword").val();
                                            newPassword1 = $("#newPassword1").val();


                                            if (newPassword == '') {
                                                $("#newPassword").focus();
                                                $("#newPassword").css({"background-color": "#fff", "border": "1px solid red"});
                                                // $("#newPasswordErr").html("New password is required.");
                                                return false;
                                            }

                                            if (newPassword1 == '') {
                                                $("#newPassword1").focus();
                                                $("#newPassword1").css({"background-color": "#fff", "border": "1px solid red"});
                                                // $("#newPassword1Err").html("Retype password is required.");
                                                return false;
                                            } else {
                                                $("#newPassword1rr").html("Passwords do not match.");
                                            }

                                            if (newPassword != newPassword1) {
                                                //  alert("Passwords do not match.");                                              
                                                $("#newPassword1").focus();
                                                $("#newPassword1Err").html("Passwords do not match.");
                                                return false;
                                            } else {
                                                $("#newPassword1Err").html("");
                                            }

                                            passwordOptToken = '<%out.print(sessionid);%>';

                                            $("#btnChangePassFrm").button('loading');

                                            $.post("syUserListUpdateUserPasswordChangeProcess.jsp", {
                                                userIdOptX: userIdOptX, newPass: newPassword, passwordOptToken: passwordOptToken
                                            }, function (data) {


//                                                console.log(data);
//                                                console.log("responseCode :: " + data[0].responseCode);
//                                                console.log("responseMsg :: " + data[0].responseMsg);
                                                if (data[0].responseCode == 1) {
                                                    $("#newPassword").val('');
                                                    $("#newPassword1").val('');
                                                    $("#btnChangePassFrm").button('reset');
                                                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");

                                                } else {
                                                    $("#btnChangePassFrm").button('reset');
                                                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");

                                                }



                                            }, "json");

                                            //	});

                                        });


                                    });

                                </script>

                                <form class="form-horizontal form-material1" id="changePassFrm">


                                    <input type="hidden" id="userIdOptX" name="userIdOptX" value="<% out.print(selectedUserId);
                                           %>">
                                    <div class="form-group row custom-bottom-margin-5x">
                                        <label for="inputName4" class="control-label col-sm-4">Password</label>
                                        <div class="col-sm-6">
                                            <input type="password" class="form-control input-sm" id="newPassword" name="newPassword" placeholder="Password" required>
                                            <div id="newPasswordErr" class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row custom-bottom-margin-5x">
                                        <label for="inputName4" class="control-label col-sm-4">Confirm Password</label>
                                        <div class="col-sm-6">
                                            <input type="password" class="form-control input-sm" id="newPassword1" name="newPassword1" placeholder="Confirm Password" required>
                                            <div id="newPassword1Err" class="help-block with-errors"></div>
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="control-label col-sm-4"></label>
                                        <div class="col-sm-offset-3 col-sm-8">
                                            <button id="btnChangePassFrm" type="submit" class="btn btn-info">Change Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /#changePass -->

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>