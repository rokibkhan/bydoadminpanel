

<%@page import="com.appul.entity.SyRoles"%>
<%@page import="java.util.Date"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Roles List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">User Management</a></li>                    
                    <li class="active">Roles</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                   

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Role ID</th>
                                    <th>Role Name</th>
                                    <th class="text-center">Created By</th>
                                    <th class="text-center">Created On</th>
                                    <th class="text-center">Last Modified By</th>
                                    <th class="text-center">Last Modified On</th>
                                </tr>
                            </thead>
                            <tbody>


                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();
                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    Query q1 = null;
                                    String roleId = "";
                                    String roleName = "";
                                    String adduser = "";
                                    Date adddate;
                                    String moduser = "";
                                    Date moddate;

                                    SyRoles role = null;
                                    int rownum = 0;
                                    String oddeven = null;

                                    q1 = dbsession.createQuery("from SyRoles as syroles WHERE roleId !='ADMIN_ROLE' order by roleDesc ASC");

                                    for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                                        role = (SyRoles) itr1.next();
                                        roleId = role.getRoleId();
                                        roleName = role.getRoleDesc();
                                        adddate = role.getAddDate();
                                        adduser = role.getAddUsr();
                                        moddate = role.getLastModDate();
                                        moduser = role.getModUsrId();
                                        rownum += 1;

                                        String updateActivityUrl = GlobalVariable.baseUrl + "/userManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&roleIdX=" + roleId;
                                        // dbtrx.commit();
                                        //dbsession.close();
                                %>

                                <tr >
                                    <td><% out.print(ix);%></td>
                                    <td> <% out.print(roleId);%></td>
                                    <td> <% out.print(roleName);%></td>
                                    <td> <% out.print(adduser);%></td>
                                    <td> <% out.print(adddate);%></td>
                                    <td> <% out.print(moduser);%></td>
                                    <td> <% out.print(moddate);%></td>

                                    
                                </tr>

                                <%
                                        ix++;

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                    %>


                            </tbody>
                        </table>
                    </div>

                    

                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>