<%-- 
    Document   : syRoleCheck
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>

<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>


<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<script type="text/javascript">
    function dataInsert(){
        var TabParent=document.getElementById('tab_role');
        roleid=TabParent.getElementsByTagName('tr')[0].getElementsByTagName('td')[1].innerHTML;
        rolename=TabParent.getElementsByTagName('tr')[1].getElementsByTagName('td')[1].innerHTML;
        adduser=TabParent.getElementsByTagName('tr')[2].getElementsByTagName('td')[1].innerHTML;
        addterm=TabParent.getElementsByTagName('tr')[3].getElementsByTagName('td')[1].innerHTML;
        addip=TabParent.getElementsByTagName('tr')[4].getElementsByTagName('td')[1].innerHTML;
        
        if (roleid!=null && roleid!=""){
            alert(roleid+":"+rolename);
    <%
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();



    %>

                var rowCount = $("#tab_actions tr").length;
                if (rowCount> 1){
                    var table = document.getElementById('tab_actions');
                    rows = table.getElementsByTagName('tr'), i, j, cells, ActionId;

                    for (i = 0, j = rows.length; i < j; ++i) {
                        cells = rows[i].getElementsByTagName('td');
                        if (!cells.length) {
                            continue;
                        }
                        actionId = cells[0].innerHTML;
                        readAllow=cells[2].innerHTML;
                        writeAllow=cells[3].innerHTML;
                        alert(actionId+":"+readAllow+":"+writeAllow);
                    }
                    return false;
                }
            }
            return false;
        }
</script>


<%
    String username = null;
    String logstat = null;
    String sessionid = null;
    sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }

    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }
    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }


%>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self ==top)
                    window.open("logout.jsp","_top","");
            }

            if (window.history) {
                window.history.forward(1);
            }
        </script>


        <title>New Role Confirmation</title>
        <%


            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }

            //                   out.println(username);
            //                   out.println(logstat);
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }
        %>
    </head>
    <body onLoad="breakOut()">

        <%

            //out.println(request.getParameter("rowcount"));
            String userid = request.getParameter("userId").toUpperCase();
            String fullname = request.getParameter("userName");
            String userpass = request.getParameter("userPass");
            String userdept = request.getParameter("userDept").trim();
            String usermobile = request.getParameter("userMSISDN");
            String useremail = request.getParameter("userEmail");
            String adduser = request.getParameter("entryUserID");
            String addterm = request.getParameter("entryTerm");
            String addip = request.getParameter("entryIP");
            String userstat = request.getParameter("userStat");
            String act = request.getParameter("act");

            if (userid == null) {
                userid = "";
            }
            if (fullname == null) {
                fullname = "";
            }
            if (userpass == null) {
                userpass = "";
            }
            if (userdept == null) {
                userdept = "";
            }
            if (usermobile == null) {
                usermobile = "";
            }
            if (useremail == null) {
                useremail = "";
            }
            if (adduser == null) {
                adduser = "";
            }
            if (addterm == null) {
                addterm = "";
            }
            if (addip == null) {
                addip = "";
            }
            if (userstat == null) {
                userstat = "";
            }
            if (act == null) {
                act = "";
            }


        %>
        <h1>User Profile Confirmation</h1>
        <br>
        <form name="frmUser" id="frmUser" method="post" action="syUserInsert.jsp?sessionid=<%out.println(sessionid);%>&act=<%out.println(act);%>"> <%-- onSubmit="return dataInsert()" --%>
       <table class="tab1">
            <thead>
                <tr >
                    <th align="left" width="150"><b>Property</b></th>
                    <th align="left" width="20"><b>Value</b></th>
                </tr>
            </thead>
                    <tr >
                        <td align="left" width="150">User ID</td>
                        <td id=><input type="hidden" id="userid" name="userid" value="<% out.println(userid);%>"><% out.println(userid);%></td>
                    </tr>
                    <tr >
                        <td align="left">User Name</td>
                        <td id=><input type="hidden" id="fullname" name="fullname" value="<% out.println(fullname);%>"><% out.println(fullname);%></td>
                    </tr>
                    <tr >
                        <td align="left" width="150">Department</td>
                        <td id=><input type="hidden" id="userdept" name="userdept" value="<% out.println(userdept);%>"><%
                            Query q2 = dbsession.createQuery("from SyDept dept where dept.deptId ='" + userdept + "'");
                            for (Iterator itr1 = q2.list().iterator(); itr1.hasNext();) {
                                SyDept dept = (SyDept) itr1.next();

                                out.println(dept.getDeptName());
                            }
                              dbsession.close();
                            %></td>
                    </tr>
                    <tr >
                        <td align="left">Mobile No.</td>
                        <td id=><input type="hidden" id="usermobile" name="usermobile" value="<% out.println(usermobile);%>"><% out.println(usermobile);%></td>
                    </tr>
                    <tr >
                        <td align="left">Email</td>
                        <td id=><input type="hidden" id="useremail" name="useremail" value="<% out.println(useremail);%>"><% out.println(useremail);%></td>
                                                     <input type="hidden" id="adduser" name="adduser" value="<% out.println(adduser);%>">
                            <input type="hidden" id="addterm" name="addterm" value="<% out.println(addterm);%>">
                            <input type="hidden" id="addip" name="addip" value="<% out.println(addip);%>">
                            <input type="hidden" id="userpass" name="userpass" value="<% out.println(userpass);%>">
                            <input type="hidden" id="userstat" name="userstat" value="<% out.println(userstat);%>">

                    </tr>
                    <tr>
                        <td class="button" align="left">
                            <INPUT TYPE="button" VALUE="Back" onClick="history.go(-1);return true;">
                        </td>
                        <td>
                            <input type="submit" value="Confirm" name="btnSubmit">
                        </td>
                    </tr>
                </tbody>
            </table>            

        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
