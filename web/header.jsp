<%@page import="com.appul.util.GlobalVariable"%>


<%
    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();

    } else {
     //   System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
        return;
    }

  //  System.out.println("Header sessionIdH :: " + sessionIdH);
  //  System.out.println("Header userNameH :: " + userNameH);

//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("storeId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("storeId").toString();
//System.out.println("uidH :: "+uidH);

%>


<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="Mir Tahajjat Hossain,Development Manager,Abu Hasan Masud ,Sr. Software Developer" name="author">
        <meta content="tahajjatcse@gmail.com,abuhasanmasud@gmail.com" name="contact">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" pageEncoding="UTF-8"/>
        <meta name="description" content="">  	

        <title><% // =request.getAttribute("TITLE"); %> ByDoAcademy admin dashboard</title>

        <link rel="shortcut icon" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/favicon.ico">
        <!-- Bootstrap Core CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- toast CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
        <!-- morris CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/customCSS.css" id="customCSS" rel="stylesheet">

        <!-- color CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/colors/blue.css" id="theme" rel="stylesheet">
        <!-- jQuery -->
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>


        <script type="text/javascript">
            /*
             * Param arg1->Field Value
             * Param arg2->Max character lenght
             * Param arg3->Input filed name
             * Param arg4->Error show container name
             */
            function countShortDescriptionChars(arg1, arg2, arg3, arg4) {

                console.log("countShortDescriptionChars");
//        console.log("arg1 :: " + arg1);
//        console.log("arg2 :: " + arg2);
//        console.log("arg3 :: " + arg3);
//        console.log("arg4 :: " + arg4);

                var maxLength = arg2;

                var strLength = arg1.value.length;
                var charRemain = (maxLength - strLength);

                if (charRemain < 0) {

                    var charExceeded = (strLength - maxLength);

                    document.getElementById(arg3).focus();
                    document.getElementById(arg4).innerHTML = '<span style="color: red;">You have exceeded the limit of ' + maxLength + ' characters.Please remove ' + charExceeded + ' characters</span>';
                } else {
                    document.getElementById(arg3).focus();
                    document.getElementById(arg4).innerHTML = charRemain + ' characters remaining';
                }

            }


            /*
             * Param arg1->Input field name
             * Param arg2->Max size MB
             * Param arg3->Error show container name
             * Param arg4->Button ID for button disable
             */
            function uplaodFileTypeJPGPNGAndSizeCheck(arg1, arg2, arg3, arg4) {

                console.log("uplaodFileTypeJPGPNGAndSizeCheck");

                console.log("arg1 :: " + arg1);
                console.log("arg2 :: " + arg2);
                console.log("arg3 :: " + arg3);

                // vs_visualsearch_upload_picture
                // document.getElementById('file').files[0].name

                var fileName = document.getElementById(arg1).files[0].name;
                var fileSize = document.getElementById(arg1).files[0].size;
                var fileType = document.getElementById(arg1).files[0].type;

                var vsUPimage = document.getElementById(arg1).value;
                if (vsUPimage != '')
                {
                    var checkimg = vsUPimage.toLowerCase();
                    if (!checkimg.match(/(\.jpg|\.png|\.JPG|\.PNG|\.jpeg|\.JPEG)$/)) { // validation of file extension using regular expression before file upload
                        document.getElementById(arg1).focus();
                        document.getElementById(arg3).innerHTML = "Invalid file type.Only JPEG or PNG files are allowed to upload";
                        document.getElementById(arg4).disabled = true;
                        return false;
                    } else {
                        document.getElementById(arg3).innerHTML = "";
                    }




                    //10485760 //10 MB
                    // 1048576  // 1MB
                    // 2097152 // 2MB
                    // 3145728 //3MB
                    // 5242880   // 5MB

                    //   echo "Sorry, File too large.";



                    var checkSizeValue = arg2 * (1024 * 1024);

                    console.log("fileSize :: " + fileSize);
                    console.log("checkSizeValue :: " + checkSizeValue);

                    if (fileSize > checkSizeValue)  // validation according to file size
                    {
                        document.getElementById(arg3).innerHTML = "Sorry, File size too large.Maximum " + arg2 + "MB files are allowed to upload.";
                        document.getElementById(arg4).disabled = true;
                        return false;
                    } else {
                        document.getElementById(arg3).innerHTML = "";
                    }
                    document.getElementById(arg4).disabled = false;
                    return true;

                } else {
                    document.getElementById(arg4).disabled = true;
                    return false;
                }
            }


            /*
             * Param arg1->Input field name
             * Param arg2->Max size MB
             * Param arg3->Error show container name
             * Param arg4->Button ID for button disable
             */
            function uplaodFileTypePDFAndSizeCheck(arg1, arg2, arg3, arg4) {

                console.log("uplaodFileTypePDFAndSizeCheck");

                console.log("arg1 :: " + arg1);
                console.log("arg2 :: " + arg2);
                console.log("arg3 :: " + arg3);
                console.log("arg4 :: " + arg4);
                // vs_visualsearch_upload_picture
                // document.getElementById('file').files[0].name

                var fileName = document.getElementById(arg1).files[0].name;
                var fileSize = document.getElementById(arg1).files[0].size;
                var fileType = document.getElementById(arg1).files[0].type;

                var vsUPimage = document.getElementById(arg1).value;
                if (vsUPimage != '')
                {
                    var checkimg = vsUPimage.toLowerCase();
                    if (!checkimg.match(/(\.pdf|\.PDF)$/)) { // validation of file extension using regular expression before file upload
                        document.getElementById(arg1).focus();
                        document.getElementById(arg3).innerHTML = "Invalid file type.Only PDF files are allowed to upload";
                        document.getElementById(arg4).disabled = true;
                        return false;
                    } else {
                        document.getElementById(arg3).innerHTML = "";
                    }




                    //10485760 //10 MB
                    // 1048576  // 1MB
                    // 2097152 // 2MB
                    // 3145728 //3MB
                    // 5242880   // 5MB

                    //   echo "Sorry, File too large.";


                    var checkSizeValue = arg2 * (1024 * 1024);
                    
                    console.log("fileSize :: " + fileSize);
                    console.log("checkSizeValue :: " + checkSizeValue);

                    if (fileSize > checkSizeValue)  // validation according to file size
                    {
                        document.getElementById(arg3).innerHTML = "Sorry, File size too large.Maximum " + arg2 + "MB files are allowed to upload.";
                        document.getElementById(arg4).disabled = true;
                        return false;
                    } else {
                        document.getElementById(arg3).innerHTML = "";
                    }
                    document.getElementById(arg4).disabled = false;
                    return true;

                } else {
                    document.getElementById(arg4).disabled = true;
                    return false;
                }
            }
            
            
            /*
             * Param arg1->Input field name
             * Param arg2->Max size MB
             * Param arg3->Error show container name
             * Param arg4->Button ID for button disable
             */
            function uplaodFileTypePPTXAndSizeCheck(arg1, arg2, arg3, arg4) {

                console.log("uplaodFileTypePPTXAndSizeCheck");

                console.log("arg1 :: " + arg1);
                console.log("arg2 :: " + arg2);
                console.log("arg3 :: " + arg3);
                console.log("arg4 :: " + arg4);
                // vs_visualsearch_upload_picture
                // document.getElementById('file').files[0].name

                var fileName = document.getElementById(arg1).files[0].name;
                var fileSize = document.getElementById(arg1).files[0].size;
                var fileType = document.getElementById(arg1).files[0].type;

                var vsUPimage = document.getElementById(arg1).value;
                if (vsUPimage != '')
                {
                    var checkimg = vsUPimage.toLowerCase();
                    if (!checkimg.match(/(\.pptx|\.PPTX)$/)) { // validation of file extension using regular expression before file upload
                        document.getElementById(arg1).focus();
                        document.getElementById(arg3).innerHTML = "Invalid file type.Only PPTX files are allowed to upload";
                        document.getElementById(arg4).disabled = true;
                        return false;
                    } else {
                        document.getElementById(arg3).innerHTML = "";
                    }




                    //10485760 //10 MB
                    // 1048576  // 1MB
                    // 2097152 // 2MB
                    // 3145728 //3MB
                    // 5242880   // 5MB

                    //   echo "Sorry, File too large.";


                    var checkSizeValue = arg2 * (1024 * 1024);
                    
                    console.log("fileSize :: " + fileSize);
                    console.log("checkSizeValue :: " + checkSizeValue);

                    if (fileSize > checkSizeValue)  // validation according to file size
                    {
                        document.getElementById(arg3).innerHTML = "Sorry, File size too large.Maximum " + arg2 + "MB files are allowed to upload.";
                        document.getElementById(arg4).disabled = true;
                        return false;
                    } else {
                        document.getElementById(arg3).innerHTML = "";
                    }
                    document.getElementById(arg4).disabled = false;
                    return true;

                } else {
                    document.getElementById(arg4).disabled = true;
                    return false;
                }
            }
            
            
            function relatedDocumentUplaodFileTypeSize() {

                // vs_visualsearch_upload_picture
                // document.getElementById('file').files[0].name

                var fileName = document.getElementById('fileNID').files[0].name;
                var fileSize = document.getElementById('fileNID').files[0].size;
                var fileType = document.getElementById('fileNID').files[0].type;

                var vsUPimage = document.getElementById("fileNID").value;
                if (vsUPimage != '')
                {
                    var checkimg = vsUPimage.toLowerCase();
                    //   if (!checkimg.match(/(\.jpg|\.png|\.JPG|\.PNG|\.jpeg|\.JPEG)$/)) { // validation of file extension using regular expression before file upload
                    if (!checkimg.match(/(\.zip|\.ZIP)$/)) { // validation of file extension using regular expression before file upload
                        document.getElementById("fileNID").focus();
                        document.getElementById("fileNIDErr").innerHTML = "Invalid file type.Only ZIP file are allowed to upload";
                        document.getElementById("btnRelDoc").disabled = true;
                        return false;
                    } else {
                        document.getElementById("fileNIDErr").innerHTML = "";
                    }




                    //10485760 //10 MB
                    // 1048576  // 1MB
                    // 2097152 // 2MB
                    // 3145728 //2MB
                    // 5242880   // 5MB
                    // 1048576
                    //   echo "Sorry, File too large.";

                    if (fileSize > 5242880)  // validation according to file size
                    {
                        document.getElementById("fileNIDErr").innerHTML = "File size too large.Only 5MB file are allowed to upload";
                        document.getElementById("btnRelDoc").disabled = true;
                        return false;
                    } else {
                        document.getElementById("fileNIDErr").innerHTML = "";
                    }
                    document.getElementById("btnRelDoc").disabled = false;
                    return true;

                } else {
                    document.getElementById("btnRelDoc").disabled = true;
                    return false;
                }
            }

        </script>



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/html5shiv/html5shiv.js"></script>
        <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/html5shiv/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part">
                        <a class="logo" href="<%out.print(GlobalVariable.baseUrl);%>/home.jsp?sessionid=<% out.print(sessionIdH);%>">
                            
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/logo_home.png" alt="home" />
                            </span>
                        </a>

                    </div>

                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>

                    </ul>
                    <ul class="nav navbar-top-links navbar-right pull-right">

                        <li class="dropdown">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> 
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/no_image.jpg" alt="user-img" width="36" class="img-circle">
                                <b class="hidden-xs"><% out.print(userNameH);%></b> 
                            </a>

                            <ul class="dropdown-menu dropdown-user animated flipInY">
                                <li><a href="<%out.print(GlobalVariable.baseUrl);%>/userManagement/syUserMyProfile.jsp?sessionid=<% out.print(sessionIdH);%>"><i class="ti-user"></i> My Profile</a></li>
                                <li><a href="<%out.print(GlobalVariable.baseUrl);%>/userManagement/syUserMyProfile.jsp?selectedTab=changePass&sessionid=<% out.print(sessionIdH);%>"><i class="ti-wallet"></i> Change Password</a></li>
                                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<%out.print(GlobalVariable.baseUrl);%>/userManagement/logout.jsp?sessionid=<% out.print(sessionIdH);%>"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
                        <!--<li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>-->
                        <!-- /.dropdown -->
                    </ul>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>


