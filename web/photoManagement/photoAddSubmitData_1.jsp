<%-- 
    Document   : Sysytem Tech dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();
    getRegistryID regId = new getRegistryID();

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    
    

    //  String sessionid = request.getParameter("sessionid").trim();
    //   if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        File file;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;
        /*
        String filePath = "c:/apache-tomcat/webapps/data/";
         */
        //   String filePath = "D:/NetbeansDevelopment/IEBADMIN/web/upload/photos/";
        String filePath = GlobalVariable.imagePhotoUploadPath;

        String contentType = request.getContentType();

        if ((contentType.indexOf("multipart/form-data") >= 0)) {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(maxMemSize);
            factory.setRepository(new File("c:\\temp"));
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(maxFileSize);
            try {
                List fileItems = upload.parseRequest(request);
                Iterator i = fileItems.iterator();
                while (i.hasNext()) {
                    FileItem fi = (FileItem) i.next();
                    if (!fi.isFormField()) {
                        String fieldName = fi.getFieldName();
                        String fileName = fi.getName();
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();
                        //    file = new File(filePath + "yourFileName");
                        file = new File(filePath + fileName);
                        fi.write(file);
                        //   out.println("Uploaded Filename: " + filePath + fileName + "<br>");
                        strMsg = "Uploaded Filename: " + filePath + fileName + "<br>";
                        response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        } else {
            //  out.println("Error in file upload.");

            strMsg = "Error in file upload.";
            response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        }
        /*
        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);
        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();

        String act = request.getParameter("act");

        SyAuditTrail audit = new SyAuditTrail();
        getDate gdate = new getDate();
        String eventid = "";
        eventid = regId.getID(16);
        BigDecimal eventId = new BigDecimal(eventid);

        audit.setEvtId(eventId);
        audit.setEvtDatetime(gdate.serverDate());
        audit.setEvtEmpId(username);
        if (act.equalsIgnoreCase("add")) {
            audit.setEvtCode("USER-ADD");
            audit.setEvtDetail("Audit for USER CREATION");
        } else {
            audit.setEvtCode("USER-EDIT");
            audit.setEvtDetail("Audit for USER-edit");

        }
        audit.setEvtRef01("");
        audit.setEvtRef02("");
        audit.setEvtRef03("");
        audit.setEvtRef04("");
        audit.setEvtRef05(addterm + "," + addip);
        dbsession.save(audit);

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            // response.sendRedirect("success.jsp?sessionid=" + sessionid);
            if (act.equals("add")) {
                strMsg = "New Tech Added Successfully";
                response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            } else {
                strMsg = "Tech Information Updated";
                response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            }
        } else {
            dbtrx.rollback();
            // response.sendRedirect("fail.jsp?sessionid=" + sessionid);
            if (act.equals("add")) {
                strMsg = "Error!!! When information add";
                response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            } else {
                strMsg = "Error!!! When information upadate";
                response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            }
        }
         */

    } else {
        //if session not found message here 
        System.out.println("photoAddSubmitData LogIn Required :: ");
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }
%>
<%
dbsession.clear();
dbsession.close();
%>
