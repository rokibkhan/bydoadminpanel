
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.GlCode"%>



<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>








<script>

//    $(function () {
//        var focus = 0;
//        $("#photoCaption").focusout(
//                function () {
//                    focus++;
//                    console.log("checkTechnicianNameAvaliablity:: " + focus);
//                    var photoCaption = $("#photoCaption").val();
//
//                    console.log("photoCaption:: " + photoCaption);
//
//                    $("#photoCaptionErr").html('<i class="fa fa-spin fa-spinner"></i> Technician ID checking...');
//
//
//
//                });
//
//    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var photoCaption = $("#photoCaption").val();
        var techPassword = $("#techPassword").val();
        var techPasswordConfirm = $("#techPasswordConfirm").val();
        var techFullName = $("#techFullName").val();
        var techMobile = $("#techMobile").val();
        var techEmail = $("#techEmail").val();
        var techTerritory = $("#techTerritory").val();

        if (photoCaption == null || photoCaption == "") {
            $("#photoCaption").focus();
            $("#photoCaptionErr").addClass("help-block with-errors").html("Technician name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#photoCaptionErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (techPassword == null || techPassword == "") {
            $("#techPassword").focus();
            $("#techPasswordErr").addClass("help-block with-errors").html("Password is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#techPasswordErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (techPasswordConfirm == null || techPasswordConfirm == "") {
            $("#techPasswordConfirm").focus();
            $("#techPasswordConfirmErr").addClass("help-block with-errors").html("Password confirm is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#techPasswordConfirmErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (techFullName == null || techFullName == "") {
            $("#techFullName").focus();
            $("#techFullNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#techFullNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }

        if (techMobile == null || techMobile == "") {
            $("#techMobile").focus();
            $("#techMobileErr").addClass("help-block with-errors").html("Mobile number is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#techMobileErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        if (techTerritoryId == null || techTerritoryId == "") {
            $("#techTerritoryId").focus();
            $("#techTerritoryIdErr").addClass("help-block with-errors").html("Territory is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#techTerritoryIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        return true;
    }

    function territoryDistrictListShow(arg1) {
        console.log("territoryDistrictListInfo:: " + arg1);
        var techTerritoryId = $("#techTerritoryId").val();

        console.log("arg1 :: " + arg1);
        console.log("techTerritoryId :: " + techTerritoryId);

        var sessionid = "";

        if (techTerritoryId != '') {
            $.post("technicianTerritoryWiseDistrictListShow.jsp", {techTerritoryId: techTerritoryId, sessionid: sessionid}, function (data) {

                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#techDistrictId").html(data[0].responseData);
                    $("#techDistrictId").prop('disabled', false);
                    //   $("#productCode").focus();
                    //    $("#productCodeErr").html(data[0].responseMsg).addClass("help-block with-errors text-danger");
                    //    $(':input[type="submit"]').prop('disabled', true);
                } else {
                    $("#techDistrictId").prop('disabled', true);
                }


            }, "json");
        } else {
            $("#techDistrictId").html("<option value=\"\">Select Thana</option>");
            $("#techDistrictId").prop('disabled', true);
        }

    }


</script>
<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    Session dbsessionAlbum = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxAlbum = null;
    dbtrxAlbum = dbsessionAlbum.beginTransaction();

    String photoAlbumIdX = request.getParameter("photoAlbumId") == null ? "" : request.getParameter("photoAlbumId").trim();

    Object[] albObj = null;

    String albumId = "";
    String albumName = "";
    String albumShortName = "";

    Query albSQL = dbsessionAlbum.createSQLQuery("SELECT * FROM  picture_category  WHERE id_cat = '" + photoAlbumIdX + "'");

    if (!albSQL.list().isEmpty()) {
        for (Iterator itAlb = albSQL.list().iterator(); itAlb.hasNext();) {

            albObj = (Object[]) itAlb.next();
            albumId = albObj[0] == null ? "" : albObj[0].toString().trim();
            albumName = albObj[1] == null ? "" : albObj[1].toString().trim();
            albumShortName = albObj[2] == null ? "" : albObj[2].toString().trim();

        }
    }


%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Edit Photo Album</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Photo Management</a></li>
                    <li class="active">Photo Album</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-6"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="techAdd" id="techAdd" method="POST" action="photoAlbumEditSubmitData.jsp?sessionid=<%=sessionid%>&act=add" onSubmit="return fromDataSubmitValidation1()">

                                <input type="hidden" id="albumId" name="albumId"  value="<%=albumId%>">

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Album Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="photoAlbumName" name="photoAlbumName"  value="<%=albumName%>" placeholder="Photo Album Name" class="form-control input-sm" >
                                        <div  id="photoAlbumNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Album Short Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="photoAlbumShortName" name="photoAlbumShortName"  value="<%=albumShortName%>" placeholder="Photo Album Name" class="form-control input-sm" >
                                        <div  id="photoAlbumShortNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>






                                <div class="form-group row">
                                    <label class="control-label col-sm-3"></label>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="hidden" id="techEmail" name="techEmail" value="">
                                        <input type="hidden" id="entryTechnicianID" name="entryTechnicianID" value="<% //out.print(username);%>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<% //out.print(hostname);%>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<% //out.print(ipAddress);%>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->


    <%
        dbsessionAlbum.clear();
        dbsessionAlbum.close();
        %>

    <%@ include file="../footer.jsp" %>