<%-- 
    Document   : Sysytem Tech dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Logger logger = Logger.getLogger("ticketAddSubmitData_jsp.class");
    
    
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();
    getRegistryID regId = new getRegistryID();

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    //  if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        String pictureTitle = request.getParameter("photoCaption").trim();
        //   Part pictureTitle = request.getPart("photoCaption");
        File file;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;
        /*
        String filePath = "c:/apache-tomcat/webapps/data/";
         */
        //   String filePath = "D:/NetbeansDevelopment/IEBADMIN/web/upload/photos/";
        String filePath = GlobalVariable.imagePhotoUploadPath;

        String contentType = request.getContentType();

        if ((contentType.indexOf("multipart/form-data") >= 0)) {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(maxMemSize);
            factory.setRepository(new File("c:\\temp"));
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(maxFileSize);
            try {
                List fileItems = upload.parseRequest(request);
                Iterator i = fileItems.iterator();
                while (i.hasNext()) {
                    FileItem fi = (FileItem) i.next();
                    if (!fi.isFormField()) {
                        String fieldName = fi.getFieldName();
                        String fileName = fi.getName();
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();
                        //    file = new File(filePath + "yourFileName");
                        //  file = new File(filePath + fileName);

                        Random random = new Random();
                        int rand1 = Math.abs(random.nextInt());
                        int rand2 = Math.abs(random.nextInt());

                        long uniqueNumber = System.currentTimeMillis();

                        String fileName1 = uniqueNumber + "_" + rand1 + "_" + rand2 + ".JPEG";
                        file = new File(filePath + fileName1);
                        fi.write(file);
                        //   out.println("Uploaded Filename: " + filePath + fileName + "<br>");

                        //insert gallary
                        String pictureCategoryId = "1";
//                        Part pictureTitle = request.getPart("photoCaption");
//                        Part pictureCaption = pictureTitle;
                        String pictureCaption = pictureTitle;
                        String pictureOrginalName = fileName;
                        String pictureName = fileName1;
                        String pictureThumb = fileName1;
                        String showingOrder = "1";

                        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date dateX = new Date();
                        String adddate = dateFormatX.format(dateX);
                        String adduser = userNameH;
                        String addterm = InetAddress.getLocalHost().getHostName().toString();
                        String addip = InetAddress.getLocalHost().getHostAddress().toString();

                        
                        
//category_name varchar(45) 
//category_short_name varchar(45) 
//showing_order varchar(45) 
//feature_image varchar(45) 
//image1 varchar(45) 
//image2 varchar(45) 
//image3 varchar(45) 
//image4 varchar(45) 
//published tinyint(2) 
//add_user varchar(45) 
//add_date varchar(45) 
//add_term varchar(45) 
//add_ip varchar(45) 
//mod_user varchar(45) 
//mod_date varchar(45) 
//mod_term varchar(45) 
//mod_ip
                        
                        
                        //   String techDistrictId = request.getParameter("techDistrictId").trim();
                        Query q4 = dbsession.createSQLQuery("INSERT INTO picture_gallery_info("
                                + "picture_category,"
                                + "picture_title,"
                                + "picture_caption,"
                                + "picture_orginal_name,"
                                + "picture_name,"
                                + "picture_thumb,"
                                + "showing_order,"
                                + "add_date,"
                                + "add_user,"
                                + "add_term,"
                                + "add_ip) values( "
                                + "'" + pictureCategoryId + "',"
                                + "'" + pictureTitle + "',"
                                + "'" + pictureCaption + "',"
                                + "'" + pictureOrginalName + "',"
                                + "'" + pictureName + "',"
                                + "'" + pictureThumb + "',"
                                + "'" + showingOrder + "',"
                                + "'" + adddate + "',"
                                + "'" + adduser + "',"
                                + "'" + addterm + "',"
                                + "'" + addip + "')");

                        q4.executeUpdate();
                        if (dbtrx.wasCommitted()) {

                            strMsg = "Photo Added Successfully";
                            response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

                        } else {
                            dbtrx.rollback();

                            strMsg = "Error!!! When photo add";
                            response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

                        }

                        //    System.out.println("Technician id = " + technicianId);
                        //   strMsg = "Uploaded Filename: " + filePath + fileName + "<br>";
                        //   response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        } else {
            //  out.println("Error in file upload.");

            strMsg = "Error in file upload.";
            response.sendRedirect("photoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        }

    } else {
        //if session not found message here      
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }
%>
<%
dbsession.clear();
dbsession.close();
%>
