<%-- 
    Document   : subjectAddSubmitData
    Created on : Nov 26, 2020, 11:37:49 PM
    Author     : rokib
--%>

<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>

<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Session dbsession = null;
    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";
    
    
    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        userStoreIdH = session.getAttribute("storeId").toString();
        int userStoreIdHi = Integer.parseInt(userStoreIdH);

        String subjectName = request.getParameter("subjectName");
        String subjectShortName = request.getParameter("subjectShortName");
        String subjectDescription = request.getParameter("subjectDescription");
        
        
        
        /*String videoCategoryId = request.getParameter("videoCategoryId");
        String videoSessionId = request.getParameter("videoSessionId");
        String videoTeacherId = request.getParameter("videoTeacherId");
        String videoPaymentTypeId = request.getParameter("videoPaymentTypeId");*/
        String subjectShowingOrder = request.getParameter("subjectShowingOrder");
        
        String subjectCourseId = request.getParameter("videoCourseId");
        //String videoLink = request.getParameter("videoLink");

        // https://www.youtube.com/watch?v=1QpB8icfz4I
        //String[] data1 = videoLink.split("v=", 2);
        //System.out.println("videoLinkID = " + data1[1]); //New York,USA

        //String videoEmdedLink = "https://www.youtube.com/embed/" + data1[1];
        String featureImageLink =  "default.jpg";

        String image1 =  "default.jpg";
        String image2 =   "default.jpg";
        String image3 =   "default.jpg";
        String image4 =   "default.jpg";

        //String videoCaption = request.getParameter("videoCaption");
        getRegistryID getId = new getRegistryID();
        String idMA = getId.getID(85);
        int subjectId = Integer.parseInt(idMA);
        

        //    String videoPhoto = request.getParameter("videoPhoto");
//        System.out.println(""+videoPhoto);
//        System.out.println("CheckSticky: " + request.getParameter("checkbox_selection"));
//        String checkboxSelection = request.getParameter("checkbox_selection");
//        int stickyVal = -1;
//        if(checkboxSelection == null){
//            System.out.println("null paisi");
//            stickyVal = 0;
//        }
//        else if(checkboxSelection.equals("1")){
//            stickyVal = 1;
//            System.out.println("1 paisi");
//        }
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
      
        //String videoCategoryId = "1";
        String showingOrder = "1";


        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);
        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();
        
        Query q = dbsession.createSQLQuery("INSERT INTO subject_info("
                + "id_subject,"
                + "course_id,"
                + "subject_name,"  
                + "subject_short_name,"
                + "subject_desc,"                
                + "feature_image,"
                + "image1,"
                + "image2,"
                + "image3,"
                + "image4,"
                + "showing_order,"
                + "add_date,"
                + "add_user,"
                + "add_term,"
                + "add_ip) values( "
                + "'" + subjectId + "',"
                + "'" + subjectCourseId + "',"
                + "'" + subjectName + "',"
                + "'" + subjectShortName + "',"                
                + "'" + subjectDescription + "',"
                + "'" + featureImageLink + "',"        
                + "'" + image1 + "',"
                + "'" + image2 + "',"
                + "'" + image3 + "',"
                + "'" + image4 + "',"
                + "'" + subjectShowingOrder + "',"
                + "'" + adddate + "',"
                + "'" + adduser + "',"
                + "'" + addterm + "',"
                + "'" + addip + "')");

        q.executeUpdate();
         

        /*VideoGalleryInfo video = new VideoGalleryInfo();
        VideoCategory videoCat = new VideoCategory();

        video.setIdVideo(videoId);

        videoCat.setIdCat(Integer.parseInt(videoCategoryId));
        video.setVideoCategory(videoCat);

        video.setVideoTitle(videoTitle);
        video.setVideoCaption(videoCaption);
        video.setVideoEmdedLink(videoLink);
        video.setVideoOrginalLink(videoLink);
        video.setFeatureImage(featureImageLink);
        video.setImage1(image1);
        video.setImage2(image2);
        video.setImage3(image3);
        video.setImage4(image4);
        video.setShowingOrder(Integer.parseInt(showingOrder));
        video.setAddDate(adddate);
        video.setAddUser(adduser);
        video.setAddTerm(addterm);
        video.setAddIp(addip);
        dbsession.save(video);
        */
        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            
        responseCode = "1";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        //json.put("sessionConInfo", centerInfoCon);
        //json.put("requestId", sessionId);
        jsonArr.add(json);

            //strMsg = "Subject Added Successfully";
            //response.sendRedirect("subjectAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        } else {
            dbtrx.rollback();

            
        responseCode = "0";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        //json.put("sessionConInfo", centerInfoCon);
        //json.put("requestId", sessionId);
        jsonArr.add(json);
            //strMsg = "Error!!! When Subject add";
            //response.sendRedirect("subjectAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        }
        out.println(jsonArr);

    } else {
        //if session not found message here      
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }
%>


