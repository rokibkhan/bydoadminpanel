<%-- 
    Document   : sessionAdd
    Created on : Nov 26, 2020, 9:54:55 PM
    Author     : rokib
--%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });
    
    
    
    
function SessionInfoSave()
{
    var sessionName = $("#sessionName").val();
    var sessionShortName = $("#sessionShortName").val();
    var sessionDescription = $("#sessionDescription").val();
    var sessionShowingOrder = $("#sessionShowingOrder").val();
    var videoSubjectId = $("#videoSubjectId").val();
    
    alert("ok");
    if(sessionName == null || sessionName == "" || sessionShortName == "" || sessionDescription == "" || sessionShowingOrder == "" || videoSubjectId == "" || videoSubjectId == null ||  sessionDescription == null || sessionShowingOrder == null)
    {
        alert("please input required field");
        return false;
    }
    
    url = '<%=GlobalVariable.baseUrl%>' + '/courseManagement/sessionAddSubmitData.jsp';
    $.post(url, {sessionName: sessionName,sessionShortName: sessionShortName,sessionDescription: sessionDescription,sessionShowingOrder: sessionShowingOrder,videoSubjectId:videoSubjectId}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == "1") {

                    alert("Saved Successfully");
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    //$("#sessionConInfo").html(data[0].sessionConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                else {
                    alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    //$("#sessionConInfo").html("");

                }



            }, "json");
}





    function fromDataSubmitValidation() {


        var videoTitle = $("#videoTitle").val();

        if (videoTitle == null || videoTitle == "") {
            $("#videoTitle").focus();
            $("#videoTitleErr").addClass("help-block with-errors").html("Page title is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#videoTitleErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        return true;
    }
    
    
    
    function showVideoSubjectInfo(arg1) {
            var courseID, formX, btnInvInfo, url, sessionid, cccll;
            console.log("showDivisonCenterInfo -> arg1 :: " + arg1 + " arg2:: ");

            //  ("#rupantorLGModalFooter").html(btnInvInfo);
            //  document.getElementById("committeeTypeSearch").disabled = true;
            courseID = $("#videoCourseId").val();

            url = '<%=GlobalVariable.baseUrl%>' + '/videoManagement/videoSubjectInfoShow.jsp';

            sessionarg1 = "";

            $.post(url, {sessionId: sessionarg1, courseId: courseID}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {
                    
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    $("#subjectConInfo").html(data[0].subjectConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    //alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    $("#subjectConInfo").html("");

                }



            }, "json");


        }

    
function showVideoSessionInfo(arg1) {
            var subjectID, formX, btnInvInfo, url, sessionid, cccll;
            console.log("showVideoSessionInfo -> arg1 :: " + arg1 + " arg2:: ");

            //  ("#rupantorLGModalFooter").html(btnInvInfo);
            //  document.getElementById("committeeTypeSearch").disabled = true;
            subjectID = $("#videoSubjectId").val();

            url = '<%=GlobalVariable.baseUrl%>' + '/videoManagement/videoSessionInfoShow.jsp';

            sessionarg1 = "";

            $.post(url, {sessionId: sessionarg1, subjectId: subjectID}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {

                    //alert("ok");
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    $("#sessionConInfo").html(data[0].sessionConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    //alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    $("#sessionConInfo").html("");

                }



            }, "json");


        }
        
        
        function showChapterInfo(arg1) {
            var sessionID, formX, btnInvInfo, url, sessionid, cccll;
            console.log("showVideoSessionInfo -> arg1 :: " + arg1 + " arg2:: ");

            //  ("#rupantorLGModalFooter").html(btnInvInfo);
            //  document.getElementById("committeeTypeSearch").disabled = true;
            
            //alert("c");
            sessionID = $("#videoSessionId").val();

            url = '<%=GlobalVariable.baseUrl%>' + '/courseManagement/videoChapterInfoShow.jsp';

            sessionarg1 = "";

            $.post(url, {sessionId: sessionarg1, vsessionId: sessionID}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {

                    //alert("ok");
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    $("#chapterConInfo").html(data[0].chapterConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    //alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    $("#chapterConInfo").html("");

                }



            }, "json");


        }



</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>

<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
//            SyCity sycity = null;
//            SyCountry sycountry = null;

    int rownum = 0;
    String oddeven = null;
    getRegistryID getregid = new getRegistryID();
    String regID = null;
    int regCode = 5;
    regID = getregid.getID(regCode);
    

    Query eventCategorySQL = null;
    Object eventCategoryObj[] = null;

    Query videoTeacherSQL = null;
    Object videoTeacherObj[] = null;
    
    Query videoCategorySQL = null;
    Object videoCategoryObj[] = null;
    
    Query videoPaymentSQL = null;
    Object videoPaymentObj[] = null;
    
    Query videoCourseSQL = null;
    Object videoCourseObj[] = null;
    
    String videoTeacherId = "";
    String videoTeacherName = "";
    String videoTeacherOptionCon = "";

    videoTeacherSQL = dbsession.createSQLQuery("select * FROM member where id in (select member_id from member_type where member_type_id = 2 ) ORDER BY id");

    for (Iterator eventCategoryItr = videoTeacherSQL.list().iterator(); eventCategoryItr.hasNext();) {
        videoTeacherObj = (Object[]) eventCategoryItr.next();
        videoTeacherId = videoTeacherObj[0].toString();
        videoTeacherName = videoTeacherObj[2].toString();

        videoTeacherOptionCon = videoTeacherOptionCon + "<option value=\"" + videoTeacherId + "\">" + videoTeacherName + "</option>";
    }
    
    

    

    String videoCourseId = "";
    String videoCourseName = "";
    String videoCourseOptionCon = "";

    videoCourseSQL = dbsession.createSQLQuery("select * FROM course_info ORDER BY id_course ASC");

    for (Iterator eventCategoryItr = videoCourseSQL.list().iterator(); eventCategoryItr.hasNext();) {
        videoCourseObj = (Object[]) eventCategoryItr.next();
        videoCourseId = videoCourseObj[0].toString();
        videoCourseName = videoCourseObj[2].toString();

        videoCourseOptionCon = videoCourseOptionCon + "<option value=\"" + videoCourseId + "\">" + videoCourseName + "</option>";
    }







%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Session</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Course Management</a></li>
                    <li class="active">Add Session</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                <!--    <form accept-charset="UTF-8" method="post" action="#"  class="form-horizontal">-->


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="sessionName" class="control-label col-sm-2">Session Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="sessionName" name="sessionName"  placeholder="Session Name" class="form-control input-sm" required>
                                <div  id="sessionNameErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="sessionShortName" class="control-label col-sm-2">Session Short Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="sessionShortName" name="sessionShortName"  placeholder="Session Short Name" class="form-control input-sm" required>
                                <div  id="videoTitleErr" class="help-block with-errors"></div>
                            </div>
                        </div>


                        

                        <div class="form-group row">
                            <label for="sessionDescription" class="control-label col-sm-2">Session Description</label>
                            <div class="col-sm-9">
                                <input type="text" id="sessionDescription" name="sessionDescription" placeholder="Session Description" class="form-control"/>
                                <div id="videoLinkErr" class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                        
                        <div class="col-md-3">                               

                                <select  name="videoCourseId" id="videoCourseId" onchange="showVideoSubjectInfo(this.value)" class="form-control input-sm customInput-sm" required>
                                    <option value="">Select Course Name </option>
                                    <%=videoCourseOptionCon%>
                                </select>
                                <div  id="eventCategoryIdErr" class="help-block with-errors"></div>
                            </div>
                                
                                
                                <div class="col-md-3">    
                                    
                                    <div id='subjectConInfo'>
                                        
                                        <select name="divisionCenterIdSearch" id="divisionCenterIdSearch"  class="form-control input-sm" required>
                                            <option value="">Select Subject For</option>
                                            

                                        </select>

                                        <div  id="divisionCenterIdSearchErr" class="help-block with-errors"></div>
                                    </div>

                                
                                
                                <div  id="eventCategoryIdErr" class="help-block with-errors"></div>
                            </div>
                                
                                
                                
                                                                
                                

                                
                                
                                
                                
                                
                                
                                

                                
                              
                        
                                

                                
                                  <div class="form-group row">
                            <label for="sessionShowingOrder" class="control-label col-sm-2">Showing Order</label>
                            <div class="col-sm-6">
                                <input type="text" id="sessionShowingOrder" name="sessionShowingOrder" placeholder="Session Showing Order" class="form-control"/>
                                <div id="sessionShowingOrderErr" class="help-block with-errors"></div>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" onclick="SessionInfoSave()" class="btn btn-info">Submit</button>
                            </div>
                        </div>

                  <!--  </form> -->

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->
<%
dbsession.clear();
dbsession.close();
%>


    <%@ include file="../footer.jsp" %>

