<%-- 
    Document   : subjectAdd
    Created on : Nov 26, 2020, 11:31:15 PM
    Author     : rokib
--%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>




<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });



function SubjectInfoSave()
{
    var subjectName = $("#subjectName").val();
    var subjectShortName = $("#subjectShortName").val();
    var subjectDescription = $("#subjectDescription").val();
    var subjectShowingOrder = $("#subjectShowingOrder").val();
    var videoCourseId = $("#videoCourseId").val();
    
    //alert(subjectName);
    if(subjectName == null || subjectName == "" || subjectShortName == "" || subjectDescription == "" || subjectShowingOrder == "" || videoCourseId == "" || videoCourseId == null ||  subjectDescription == null || subjectShowingOrder == null)
    {
        alert("please input required field");
        return false;
    }
    
    url = '<%=GlobalVariable.baseUrl%>' + '/courseManagement/subjectAddSubmitData.jsp';
    $.post(url, {subjectName: subjectName,subjectShortName: subjectShortName,subjectDescription: subjectDescription,subjectShowingOrder: subjectShowingOrder,videoCourseId:videoCourseId}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {

                    alert("Saved Successfully");
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    //$("#sessionConInfo").html(data[0].sessionConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    //$("#sessionConInfo").html("");

                }



            }, "json");
}

    function fromDataSubmitValidation() {


        var videoTitle = $("#videoTitle").val();

        if (videoTitle == null || videoTitle == "") {
            $("#videoTitle").focus();
            $("#videoTitleErr").addClass("help-block with-errors").html("Page title is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#videoTitleErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        return true;
    }
    
    
    
    function showVideoSubjectInfo(arg1) {
            var courseID, formX, btnInvInfo, url, sessionid, cccll;
            console.log("showDivisonCenterInfo -> arg1 :: " + arg1 + " arg2:: ");

            //  ("#rupantorLGModalFooter").html(btnInvInfo);
            //  document.getElementById("committeeTypeSearch").disabled = true;
            courseID = $("#videoCourseId").val();

            url = '<%=GlobalVariable.baseUrl%>' + '/courseManagement/videoSubjectInfoShow.jsp';

            sessionarg1 = "";

            $.post(url, {sessionId: sessionarg1, courseId: courseID}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {
                    
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    $("#subjectConInfo").html(data[0].subjectConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    //alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    $("#subjectConInfo").html("");

                }



            }, "json");


        }

    
function showVideoSessionInfo(arg1) {
            var subjectID, formX, btnInvInfo, url, sessionid, cccll;
            console.log("showVideoSessionInfo -> arg1 :: " + arg1 + " arg2:: ");

            //  ("#rupantorLGModalFooter").html(btnInvInfo);
            //  document.getElementById("committeeTypeSearch").disabled = true;
            subjectID = $("#videoSubjectId").val();

            url = '<%=GlobalVariable.baseUrl%>' + '/videoManagement/videoSessionInfoShow.jsp';

            sessionarg1 = "";

            $.post(url, {sessionId: sessionarg1, subjectId: subjectID}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == "1") {

                    //alert("ok");
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    $("#sessionConInfo").html(data[0].sessionConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                else {
                    alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    $("#sessionConInfo").html("");

                }



            }, "json");


        }
        
        
        function showChapterInfo(arg1) {
            var sessionID, formX, btnInvInfo, url, sessionid, cccll;
            console.log("showVideoSessionInfo -> arg1 :: " + arg1 + " arg2:: ");

            //  ("#rupantorLGModalFooter").html(btnInvInfo);
            //  document.getElementById("committeeTypeSearch").disabled = true;
            
            //alert("c");
            sessionID = $("#videoSessionId").val();

            url = '<%=GlobalVariable.baseUrl%>' + '/courseManagement/videoChapterInfoShow.jsp';

            sessionarg1 = "";

            $.post(url, {sessionId: sessionarg1, vsessionId: sessionID}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {

                    //alert("ok");
                    //$("#subjectConInfo").html("");
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    $("#chapterConInfo").html(data[0].chapterConInfo);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    //alert("failed");
                    //document.getElementById("divisionCenterIdSearch").disabled = true;

                    $("#chapterConInfo").html("");

                }



            }, "json");


        }



</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>

<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
//            SyCity sycity = null;
//            SyCountry sycountry = null;

    int rownum = 0;
    String oddeven = null;
    getRegistryID getregid = new getRegistryID();
    String regID = null;
    int regCode = 5;
    regID = getregid.getID(regCode);
    

    Query eventCategorySQL = null;
    Object eventCategoryObj[] = null;

    Query videoTeacherSQL = null;
    Object videoTeacherObj[] = null;
    
    Query videoCategorySQL = null;
    Object videoCategoryObj[] = null;
    
    Query videoPaymentSQL = null;
    Object videoPaymentObj[] = null;
    
    Query videoCourseSQL = null;
    Object videoCourseObj[] = null;
    
    String videoTeacherId = "";
    String videoTeacherName = "";
    String videoTeacherOptionCon = "";

    videoTeacherSQL = dbsession.createSQLQuery("select * FROM member where id in (select member_id from member_type where member_type_id = 2 ) ORDER BY id");

    for (Iterator eventCategoryItr = videoTeacherSQL.list().iterator(); eventCategoryItr.hasNext();) {
        videoTeacherObj = (Object[]) eventCategoryItr.next();
        videoTeacherId = videoTeacherObj[0].toString();
        videoTeacherName = videoTeacherObj[2].toString();

        videoTeacherOptionCon = videoTeacherOptionCon + "<option value=\"" + videoTeacherId + "\">" + videoTeacherName + "</option>";
    }
    
    

    

    String videoCourseId = "";
    String videoCourseName = "";
    String videoCourseOptionCon = "";

    videoCourseSQL = dbsession.createSQLQuery("select * FROM course_info ORDER BY id_course ASC");

    for (Iterator eventCategoryItr = videoCourseSQL.list().iterator(); eventCategoryItr.hasNext();) {
        videoCourseObj = (Object[]) eventCategoryItr.next();
        videoCourseId = videoCourseObj[0].toString();
        videoCourseName = videoCourseObj[2].toString();

        videoCourseOptionCon = videoCourseOptionCon + "<option value=\"" + videoCourseId + "\">" + videoCourseName + "</option>";
    }







%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Subject</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Course Management</a></li>
                    <li class="active">Add Subject</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                   <!-- <form accept-charset="UTF-8" method="post" action="#" class="form-horizontal">-->


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="subjectName" class="control-label col-sm-2">Subject Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="subjectName" name="subjectName"  placeholder="Subject Name" class="form-control input-sm" required>
                                <div  id="subjectTitleErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="subjectShortName" class="control-label col-sm-2">Subject Short Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="subjectShortName" name="subjectShortName"  placeholder="Subject Short Name" class="form-control input-sm" required>
                                <div  id="subjectShortNameErr" class="help-block with-errors"></div>
                            </div>
                        </div>


                        

                        <div class="form-group row">
                            <label for="subjectDescription" class="control-label col-sm-2">Subject Description</label>
                            <div class="col-sm-9">
                                <input type="text" id="subjectDescription" name="subjectDescription" placeholder="Subject Description" class="form-control"/>
                                <div id="subjectDescriptionErr" class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            
                              
                            <label for="subjectShowingOrder" class="control-label col-sm-2">Showing Order</label>
                            <div class="col-sm-3">
                                <input type="text" id="subjectShowingOrder" name="subjectShowingOrder" placeholder="Subject Showing Order" class="form-control"/>
                                <div id="subjectShowingOrderErr" class="help-block with-errors"></div>
                            </div>
                        
                        <div class="col-md-3">                               

                                <select  name="videoCourseId" id="videoCourseId" onchange="showVideoSubjectInfo(this.value)" class="form-control input-sm customInput-sm" required>
                                    <option value="">Select Course Name </option>
                                    <%=videoCourseOptionCon%>
                                </select>
                                <div  id="eventCategoryIdErr" class="help-block with-errors"></div>
                            </div>
                                
                                

                                
                                
                                <div  id="eventCategoryIdErr" class="help-block with-errors"></div>
                                
                                
                                
                                  
                        </div>
                            </div>
                                
                                
                                
                                                                
                                

                                
                                
                                
                                
                                
                                
                                

                                
                              
                        
                                

                        <div class="form-group">

                            <div class="input-file-container">  
                            </div>

                            <!--                                        <label for="video_photo" class="control-label">Select image</label>
                                                                    
                                                                    <input type="file" id="video_photo" >-->
                        </div>
                                
                              


                        <!--                                    <div class="form-group">
                                                                <label class="container_label">Sticky video
                                                                    <input type="checkbox" name="selected" value="1">
                                                                    <span class="checkmark_label"></span>
                                                                </label>
                                                            </div>-->

                        <style>
                            .container_label {
                                display: block;
                                position: relative;
                                padding-left: 35px;
                                margin-bottom: 12px;
                                cursor: pointer;
                                font-size: 22px;
                                -webkit-user-select: none;
                                -moz-user-select: none;
                                -ms-user-select: none;
                                user-select: none;
                            }

                            /* Hide the browser's default checkbox */
                            .container_label input {
                                position: absolute;
                                opacity: 0;
                                cursor: pointer;
                                height: 0;
                                width: 0;
                            }

                            /* Create a custom checkbox */
                            .checkmark_label {
                                position: absolute;
                                top: 0;
                                left: 0;
                                height: 25px;
                                width: 25px;
                                background-color: #eee;
                            }

                            /* On mouse-over, add a grey background color */
                            .container_label:hover input ~ .checkmark_label {
                                background-color: #ccc;
                            }

                            /* When the checkbox is checked, add a blue background */
                            .container_label input:checked ~ .checkmark_label {
                                background-color: #2196F3;
                            }

                            /* Create the checkmark/indicator (hidden when not checked) */
                            .checkmark_label:after {
                                content: "";
                                position: absolute;
                                display: none;
                            }

                            /* Show the checkmark when checked */
                            .container_label input:checked ~ .checkmark_label:after {
                                display: block;
                            }

                            /* Style the checkmark/indicator */
                            .container_label .checkmark_label:after {
                                left: 9px;
                                top: 5px;
                                width: 5px;
                                height: 10px;
                                border: solid white;
                                border-width: 0 3px 3px 0;
                                -webkit-transform: rotate(45deg);
                                -ms-transform: rotate(45deg);
                                transform: rotate(45deg);
                            }
                        </style>





                        <div class="form-group row">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" onclick="SubjectInfoSave()"  class="btn btn-info">Submit</button>
                            </div>
                        </div>

                  <!--  </form>-->

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->
<%
dbsession.clear();
dbsession.close();
%>


    <%@ include file="../footer.jsp" %>

