
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.MemberType"%>
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">



<script type="text/javascript">




    function printInvoiceInfo(arg1) {
        var divToPrint = document.getElementById("divToPrint" + arg1);
        var popupWin = window.open('', '_blank', 'width=500,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }



    function showTotalMemberShipRenewalFee(arg1, arg2, arg3) {
        var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll, renewalType, renewalTypeId, memberTypeAnnaralSubscriptionFee, renewalTotalAmount;
        console.log("showTotalMemberShipRenewalFee -> arg1 :: " + arg1 + " arg2:: " + arg2);


        renewalTypeId = document.getElementById("memberRenewalTypeId").value;
        memberTypeAnnaralSubscriptionFee = document.getElementById("memberTypeAnnaralSubscriptionFee").value;

        renewalType = "Annual";

        if (renewalTypeId != '') {


            renewalTotalAmount = renewalTypeId * memberTypeAnnaralSubscriptionFee;
            document.getElementById("memberTypeTotalAmount").value = renewalTotalAmount;

            url = '<%=GlobalVariable.baseUrl%>' + '/member/memberRenewalTotalFeeShow.jsp';

//            $.post(url, {sessionId: arg1, memberId: arg2, memberTypeId: arg3, renewalType:renewalType,renewalTypeId: renewalTypeId}, function (data) {
////
//                console.log(data);
//
//                if (data[0].responseCode == 1) {
//
//                    rupantorLGModal.modal('hide');
//                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
//                    $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);
//
//                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
//                }
//                if (data[0].responseCode == 0) {
//                    rupantorLGModal.modal('hide');
//                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
//                    $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);
//                    $("#globalAlertInfoBoxConTT").show().css({"display": "block", "margin-top": "5px"});
//                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
//
//                }
//
//
//
//            }, "json");

        } else {

            document.getElementById("memberTypeTotalAmount").value = '';
        }
    }

</script>



<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Event Category</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Events Management</a></li>  
                    <li class="active">Event Category Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">

                <div class="col-md-12 col-xs-12">
                    <div class="white-box">

                        

                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">

                                    <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="eventsCategoryAddSubmitData.jsp?sessionid=<%=sessionid%>&act=add" onSubmit="return fromDataSubmitValidation()">



                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label for="eventTitle" class="control-label col-md-3">Event Category Name</label>
                                            <div class="col-md-8">
                                                <input type="text" id="eventCategoryName" name="eventCategoryName"  placeholder="Event Category Name" class="form-control" required>
                                                <div  id="eventCategoryNameErr" class="help-block with-errors"></div>
                                            </div>
                                        </div>

                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label for="eventTitle" class="control-label col-md-3">Showing Order</label>
                                            <div class="col-md-4">
                                                <input type="text" id="eventCategoryOrder" name="eventCategoryOrder"  placeholder="Showing Order" class="form-control" required>
                                                <div  id="eventCategoryOrderErr" class="help-block with-errors"></div>
                                            </div>
                                        </div>

                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label class="control-label col-sm-3"></label>
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                                <input type="hidden" id="userstat" name="userstat" value="1">
                                                <button type="submit" class="btn btn-info" id="eventSubmitBtn">Submit</button>
                                            </div>
                                        </div>
                                    </form> 
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
    %>

    <%@ include file="../footer.jsp" %>