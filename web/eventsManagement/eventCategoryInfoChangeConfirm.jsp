<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String eventId = request.getParameter("eventId");
    String eventName = request.getParameter("eventName");
    String eventCategoryId = request.getParameter("eventCategoryId");

    System.out.println("eventCategoryInfoChangeConfirm :: eventId " + eventId);
    System.out.println("eventCategoryInfoChangeConfirm :: eventName " + eventName);
    System.out.println("eventCategoryInfoChangeConfirm :: eventCategoryId " + eventCategoryId);
    /*
    q1 = dbsession.createSQLQuery("SELECT * FROM event_info WHERE  id_event=" + eventId + " AND event_category =" + eventCategoryId + "");
    System.out.println("eventCategoryInfoChangeConfirm :: q1 " + q1);
    if (!q1.list().isEmpty()) {

        responseCode = "0";
        responseMsg = "Error!!! Already category exits this event";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", eventId);
        jsonArr.add(json);
    } else {
        
     */

    Query eventCenterAddSQL = dbsession.createSQLQuery("UPDATE event_info "
            + "SET event_category=\"" + eventCategoryId + "\""
            + " WHERE id_event =\"" + eventId + "\"");

    System.out.println("eventCategoryInfoChangeConfirm :: eventCenterAddSQL " + eventCenterAddSQL);

    eventCenterAddSQL.executeUpdate();

    dbtrx.commit();

    if (dbtrx.wasCommitted()) {
        //  strMsg = "Success !!! New Committee added";
        //  response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

        responseCode = "1";
        responseMsg = "Success!!! Event Category change for event successfully";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        // show event Assign center,division,tag
        String eventCategoryInfoContainer = "";

        Query categorySQL = null;
        Object categoryObj[] = null;

        String categoryName = "";
        String btnKeyNotesSpeaker = "";
        categorySQL = dbsession.createSQLQuery("select * FROM event_category WHERE id_cat ='" + eventCategoryId + "'");

        for (Iterator categoryItr = categorySQL.list().iterator(); categoryItr.hasNext();) {
            categoryObj = (Object[]) categoryItr.next();
            categoryName = categoryObj[1].toString();
        }

        String agrXcenter = "'" + eventId + "','','" + session.getId() + "'";

        if (eventCategoryId.equals("1")) {
            btnKeyNotesSpeaker = categoryName + "<br/><br/><br/><a href=\"\" title=\"Add Speaker Info for this Event\"  class=\"btn btn-primary btn-xs  m-b-5\"><i class=\"fa fa-plus\"></i> Add Speaker</a>"
                    + "<br><a onClick=\"changeEventCategoryInfo(" + agrXcenter + ");\" title=\"Change Category for this Event\"  class=\"btn btn-danger btn-xs  m-b-5\">Change Category</a>";
        } else {
            btnKeyNotesSpeaker = categoryName + "<a onClick=\"changeEventCategoryInfo(" + agrXcenter + ");\" title=\"Change Category for this Event\"  class=\"btn btn-danger btn-xs  m-b-5\">Change Category</a>";
        }

        eventCategoryInfoContainer = btnKeyNotesSpeaker;

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("eventCategoryInfoContainer", eventCategoryInfoContainer);
        json.put("requestId", eventId);
        jsonArr.add(json);

    } else {
        dbtrx.rollback();
        //   strMsg = "Error!!! When Committee add";
        //   response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

        responseCode = "0";
        responseMsg = "Error!!! When category update this event";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", eventId);
        jsonArr.add(json);
    }

    // }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>