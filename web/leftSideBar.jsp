<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.GlobalVariable"%>

<%

    String sessionid = request.getParameter("sessionid").trim();

    sessionid = session.getId();
    String uid = session.getAttribute("username").toString().toUpperCase();
    String userStoreId = session.getAttribute("storeId").toString();

    Session dbsession = null;

    List<String> menuList = null;
    //out.println(session.getAttribute("username"));
    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query qActList;
    //  qActList = dbsession.createSQLQuery("SELECT  DISTINCT A.ACT_ID,A.ACT_PARENT,A.ACT_DESC,A.ACT_SCREEN,R.ACT_READ,R.ACT_WRITE,A.ICON FROM SY_ACTIVITY A,SY_ROLE_ACT R WHERE A.ACT_ID=R.ACT_ID AND R.ROLE_ID in (SELECT ROLE_ID FROM SY_USER_ROLE WHERE USER_ID= '" + uid + "') ORDER BY A.ACT_ID");
    qActList = dbsession.createSQLQuery("SELECT  DISTINCT A.ACT_ID,A.ACT_PARENT,A.ACT_DESC,A.ACT_SCREEN,R.ACT_READ,R.ACT_WRITE,A.ICON FROM sy_activity A,sy_role_act R WHERE A.ACT_ID=R.ACT_ID AND R.ROLE_ID in (SELECT ROLE_ID FROM sy_user_role WHERE USER_ID= '" + uid + "') ORDER BY A.ACT_ID");

    menuList = qActList.list();

//out.println(menuList);

%>



<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                    </span> 
                </div>
                <!-- /input-group -->
            </li>


            <%                Iterator it = menuList.iterator();
                Object[] obj = null;
                Object[] obj2 = null;
                Object[] obj3 = null;
                String levelCheck = "";
                String levelCheckInner = "";
                String textWtihLinkOpt = "";
                String textWtihLinkOpt1 = "";
                String textWtihLinkOptS = "";
                String textWtihLinkOptS1 = "";
                String textWtihLinkOptT = "";
                String textWtihLinkOptT1 = "";
                String menuSecondLevel = "";
                String menuSecondLevel1 = "";
                String menuSecondLevelNew = "";
                String menuSecondLevelNew1 = "";
                String menuThirdLevel = "";
                String menuThirdLevel1 = "";
                String menuThirdLevelNew = "";
                String menuThirdLevelNew1 = "";
                String hasChild = "";
                String leftMenuStr = "";
                String leftMenuStrS = "";
                int i = 0;
                int j = 0;
                int cnt = 0;
                String actID = "";
                String actParent = "";
                String actDesc = "";
                String actScreen = "";
                String actRead = "";
                String actWrite = "";
                String actIcon = "";

                String actIDS = "";
                String actParentS = "";
                String actDescS = "";
                String actScreenS = "";
                String actReadS = "";
                String actWriteS = "";
                String actIconS = "";

                String actIDT = "";
                String actParentT = "";
                String actDescT = "";
                String actScreenT = "";
                String actReadT = "";
                String actWriteT = "";
                String actIconT = "";

                for (Iterator itr1 = qActList.list().iterator(); itr1.hasNext();) {

                    obj = (Object[]) itr1.next();

                    actID = obj[0].toString();
                    actParent = obj[1].toString();
                    actDesc = obj[2].toString();
                    actScreen = obj[3].toString();
                    actRead = obj[4].toString();
                    actWrite = obj[5].toString();
                    actIcon = obj[6].toString();

                    //level 1
                    if (actParent.equals("0")) {

                        if (actScreen.toString().equals("blank")) {
                            levelCheck = "javascript:void(0)";
                            hasChild = "<span class=\"fa arrow\"></span>";
                            textWtihLinkOpt = "<a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIcon + "<span class=\"hide-menu\">" + actDesc + hasChild + "</span></a>";
                            textWtihLinkOpt1 = "<li><a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIcon + "<span class=\"hide-menu\">" + actDesc + hasChild + "</span></a>";

                            //     menuSecondLevel1 ="<ul class=\"nav nav-second-level\"><li><a href=\"javascript:void(0)\"><i class=\"ti-user\"></i> My Profile</a></li><li><a href=\"javascript:void(0)\"><i class=\"ti-wallet\"></i> My Balance</a></li></ul>";
                            menuSecondLevel1 = "<ul class=\"nav nav-second-level\">";

                            obj2 = null;
                            for (Iterator itr2 = qActList.list().iterator(); itr2.hasNext();) {

                                obj2 = (Object[]) itr2.next();

                                actIDS = obj2[0].toString();
                                actParentS = obj2[1].toString();
                                actDescS = obj2[2].toString();
                                actScreenS = obj2[3].toString();
                                actReadS = obj2[4].toString();
                                actWriteS = obj2[5].toString();
                                actIconS = obj2[6].toString();

                                //level 2
                                if (actID.equals(actParentS)) {

                                    if (actScreenS.toString().equals("blank")) {
                                        levelCheck = "javascript:void(0)";
                                        hasChild = "<span class=\"fa arrow\"></span>";
                                        textWtihLinkOptS = "<a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIconS + "<span class=\"hide-menu\">" + actDescS + hasChild + "</span></a>";
                                        textWtihLinkOptS1 = "<li><a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIconS + "<span class=\"hide-menu\">" + actDescS + hasChild + "</span></a>";

                                        menuThirdLevel1 = "<ul class=\"nav nav-third-level\">";

                                        obj3 = null;
                                        for (Iterator itr3 = qActList.list().iterator(); itr3.hasNext();) {

                                            obj3 = (Object[]) itr3.next();

                                            actIDT = obj3[0].toString();
                                            actParentT = obj3[1].toString();
                                            actDescT = obj3[2].toString();
                                            actScreenT = obj3[3].toString();
                                            actReadT = obj3[4].toString();
                                            actWriteT = obj3[5].toString();
                                            actIconT = obj3[6].toString();

                                            //level 3
                                            if (actIDS.equals(actParentT)) {
                                                //   System.out.println(" actDescLevel3 " + actDescT);

                                                levelCheck = GlobalVariable.baseUrl + "/" + actScreenT + ".jsp?sessionid=" + sessionid + "&read=" + actReadT + "&write=" + actWriteT;
                                                textWtihLinkOptT = "<li><a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIconT + "<span class=\"hide-menu\">" + actDescT + "</span></a></li>";

                                                menuThirdLevel1 += textWtihLinkOptT;

                                            }//end level3 if
                                        }//end level3 for

                                        menuThirdLevel1 += "</ul>";

                                        menuSecondLevel1 += textWtihLinkOptS1 + menuThirdLevel1 + "</li>";

                                    } else {
                                        levelCheck = GlobalVariable.baseUrl + "/" + actScreenS + ".jsp?sessionid=" + sessionid + "&read=" + actReadS + "&write=" + actWriteS;
                                        hasChild = "";
                                        textWtihLinkOptS = "<a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIconS + "<span class=\"hide-menu\">" + actDescS + "</span></a>";
                                        textWtihLinkOptS1 = "<li><a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIconS + "<span class=\"hide-menu\">" + actDescS + "</span></a></li>";

                                        menuSecondLevel1 += textWtihLinkOptS1;

                                    }

                                    //  menuSecondLevel1 += "<li><a href=\"javascript:void(0)\"><i class=\"ti-user\"></i> " + actDescS + "</a></li>";
                                }//end level2 if

                            }//end level2 for

                            menuSecondLevel1 += "</ul>";

                            menuSecondLevel = textWtihLinkOpt1 + menuSecondLevel1 + "</li>";

                            out.print(menuSecondLevel);

                        } else {
                            levelCheck = GlobalVariable.baseUrl + "/" + actScreen + ".jsp?sessionid=" + sessionid + "&read=" + actRead + "&write=" + actWrite;
                            hasChild = "";
                            textWtihLinkOpt = "<a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIcon + "<span class=\"hide-menu\">" + actDesc + "</span></a>";
                            textWtihLinkOpt1 = "<li><a href=\"" + levelCheck + "\" class=\"waves-effect\">" + actIcon + "<span class=\"hide-menu\">" + actDesc + "</span></a></li>";

                            //  leftMenuStr += textWtihLinkOpt1;
                            out.print(textWtihLinkOpt1);
                        }

                    }

                }


            %>


            <%                
                dbsession.flush();
                dbsession.clear();
                dbsession.close();
            %>





        </ul>
    </div>
</div>
<!-- Left navbar-header end -->


