<%-- 
    Document   : Sysytem User dataInsert
    Created on : February 08, 2019, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Session dbsession = null;
    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        userStoreIdH = session.getAttribute("storeId").toString();

        String storiesTitle = request.getParameter("storiesTitle");
        String storiesShortDesc = request.getParameter("storiesShortDesc");
        String storiesDesc = request.getParameter("storiesDesc");

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);
        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();

        Query q = dbsession.createSQLQuery("INSERT INTO success_story("
                + "story_title,"
                + "story_short_desc,"
                + "story_desc, "
                + "add_user, "
                + "add_date, "
                + "add_term,"
                + "add_ip) values( "
                + "'" + storiesTitle + "',"
                + "'" + storiesShortDesc + "',"
                + "'" + storiesDesc + "',"
                + "'" + adduser + "',"
                + "'" + adddate + "',"
                + "'" + addterm + "',"
                + "'" + addip + "')");
        q.executeUpdate();
        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            strMsg = "Stories Added Successfully";
            response.sendRedirect(GlobalVariable.baseUrl + "/storiesManagement/storiesAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        } else {
            dbtrx.rollback();
            strMsg = "Error!!! When Stories add";
            response.sendRedirect(GlobalVariable.baseUrl + "/storiesManagement/storiesAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        }

    } else {
        //if session not found message here      
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }
%>
