
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>







<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {


        var jobTitle = $("#jobTitle").val();

        if (jobTitle == null || jobTitle == "") {
            $("#jobTitle").focus();
            $("#jobTitleErr").addClass("help-block with-errors").html("Page title is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#jobTitleErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        return true;
    }


</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>

<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
//            SyCity sycity = null;
//            SyCountry sycountry = null;

    String jobid = request.getParameter("jobid");
    Query usrSQL = dbsession.createSQLQuery("SELECT * "
            + "FROM  job_post  WHERE ID = " + jobid);

    String jobId = "";
    String ejobCategoryId = "";
    String ejobDesignation = "";
    String ejobDescription = "";
    String ejobAge = "";
    String ejobGender = "";
    String ejobSalaryMin = "";
    String ejobSalaryMax = "";
    String ejobSalaryNego = "";
    String ejobExperience = "";
    String ejobType = "";
    String ejobCompany = "";
    String ejobLocation = "";
    String ejobApplicationDeadline = "";
    String syStatus = "";
    String userStatusDesc = "";
    String userRole = "";
    String btnColorClass = "";
    String levelStatusColorClass = "";
    String deadline = "";

    if (!usrSQL.list().isEmpty()) {
        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {
            obj = (Object[]) it1.next();
            jobId = obj[0] != null ? obj[0].toString().trim() : "";
            ejobCategoryId = obj[1] != null ? obj[1].toString().trim() : "";
            ejobDesignation = obj[2] != null ? obj[2].toString().trim() : "";
            ejobDescription = obj[3] != null ? obj[3].toString().trim() : "";
            ejobAge = obj[4] != null ? obj[4].toString().trim() : "";
            ejobGender = obj[5] != null ? obj[5].toString().trim() : "";
            ejobSalaryMin = obj[6] != null ? obj[6].toString().trim() : "";
            ejobSalaryMax = obj[7] != null ? obj[7].toString().trim() : "";
            ejobSalaryNego = obj[8] != null ? obj[8].toString().trim() : "";
            if (ejobSalaryNego.equals("0")) {
                ejobSalaryNego = "No";
            } else {
                ejobSalaryNego = "Yes";
            }
            ejobExperience = obj[9] != null ? obj[9].toString().trim() : "";
            ejobType = obj[10] != null ? obj[10].toString().trim() : "";
            ejobCompany = obj[11] != null ? obj[11].toString().trim() : "";
            ejobLocation = obj[12] != null ? obj[12].toString().trim() : "";
            ejobApplicationDeadline = obj[13] != null ? obj[13].toString().trim() : "";
            
            System.out.println("UTT " + ejobApplicationDeadline);
            
            deadline = ejobApplicationDeadline.split(" ")[0];

        }

    }
    int rownum = 0;
    String oddeven = null;
    getRegistryID getregid = new getRegistryID();
    String regID = null;
    int regCode = 5;
    regID = getregid.getID(regCode);


%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Edit Job</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Job Management</a></li>
                    <li class="active">Add Job</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <form name="addrole" id="addrole" method="post" action="jobEditSubmitData.jsp?sessionid=<%out.print(sessionid);%>&act=add" onSubmit="return fromDataSubmitValidation()" enctype="multipart/formdata" class="form-horizontal">


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="jobCategory" class="control-label">Job Category: &nbsp</label> <br>
                            <select name="jobCategory" id="jobCategory" >
                                <%

                                    String jobPostID = "";
                                    String jobPostCategoryName = "";

                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    usrSQL = dbsession.createSQLQuery("SELECT ni.ID, ni.CATEGORY_NAME "
                                            + "FROM  job_post_category  ni ORDER BY ni.ID");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            jobPostID = obj[0] != null ? obj[0].toString().trim() : "";
                                            jobPostCategoryName = obj[1] != null ? obj[1].toString().trim() : "";


                                %>
                                <option value="<%=jobPostID%>" <% if (ejobCategoryId.equals(jobPostID)) {  %> selected <% }%>   ><%=jobPostCategoryName%></option>


                                <%  }
                                    } %>
                            </select>
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>



                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="jobGender" class="control-label">Gender: &nbsp</label> <br>
                            <select name="jobGender" id="jobPost">
                                <option value="male" <%if(ejobGender.equals("male")) {%> selected <% } %> >Male</option>
                                <option value="female"<%if(ejobGender.equals("female")) {%> selected <% } %> >Female</option>
                                <option value="both" <%if(ejobGender.equals("both")) {%> selected <% } %> >Male/Female</option>
                            </select>
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>




                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="jobPost" class="control-label">Job Post: &nbsp</label> <br>
                            <select name="jobPost" id="jobPost">
                                <%

                                    String jobTypeId = "";

                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    usrSQL = dbsession.createSQLQuery("SELECT ni.ID, ni.JOB_TYPE "
                                            + "FROM  job_post_type  ni ORDER BY ni.ID");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            jobTypeId = obj[0] != null ? obj[0].toString().trim() : "";
                                            String jobType = obj[1] != null ? obj[1].toString().trim() : "";


                                %>
                                <option value="<%=jobTypeId%>"  <% if (ejobType.equals(jobTypeId)) {  %> selected <% }%> ><%=jobType%></option>


                                <%  }
                                    } %>
                            </select>
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>






                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="company" class="control-label">Company: &nbsp</label> <br>
                            <select name="company" id="company">
                                <%

                                    String companyId = "";
                                    String companyName = "";

                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    usrSQL = dbsession.createSQLQuery("SELECT ni.ID, ni.COMPANY_NAME "
                                            + "FROM  company  ni ORDER BY ni.ID");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            companyId = obj[0] != null ? obj[0].toString().trim() : "";
                                            companyName = obj[1] != null ? obj[1].toString().trim() : "";


                                %>
                                <option value="<%=companyId%>"  <% if (ejobCompany.equals(companyId)) {  %> selected <% }%>   ><%=companyName%></option>


                                <%  }
                                    }%>
                            </select>
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="jobTitle" class="control-label">Designation</label>
                            <input type="text" id="jobTitle" name="jobDesignation"  placeholder="Job Designation for the post" class="form-control input-sm" required value="<%=ejobDesignation%>">
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="jobAge" class="control-label">Age limit</label>
                            <input type="text" id="jobAge" name="jobAge"  placeholder="Age limit for the post" class="form-control input-sm" required value="<%=ejobAge%>">
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>



                        <link href="<%=cssLinkPmtWysihtml5%>" rel="stylesheet" type="text/css"/>  
                        <div class="form-group">
                            <label for="jobContent" class="control-label">Job Description</label>
                            <textarea id="jobContent" name="jobDescription" placeholder="Detail job description" class="form-control" cols="10" rows="15" ><%=ejobDescription%></textarea>
                            <div id="jobContentErr" class="help-block with-errors"></div>

                            <script src="<%=jsLinkPmtWysihtml51%>"></script>
                            <script src="<%=jsLinkPmtWysihtml52%>"></script>
                            <script type="text/javascript">
                        $(function () {
                            $("#jobContent").wysihtml5();

                        });
                            </script> 
                        </div>


                        <div class="form-group row custom-bottom-margin-5x" id="container_min_salary">
                            <label for="jobTitle" class="control-label">Minimum Job Salary</label>
                            <input type="text" id="jobTitle" name="jobSalaryMin"  placeholder="Minimum Job Salary" class="form-control input-sm" value="<%=ejobSalaryMin%>">
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>


                        <div class="form-group row custom-bottom-margin-5x" id="container_max_salary">
                            <label for="jobTitle" class="control-label">Maximum Job Salary</label>
                            <input type="text" id="jobTitle" name="jobSalaryMax"  placeholder="Maximum Job Salary" class="form-control input-sm" value="<%=ejobSalaryMax%>">
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>

                        <input type="hidden" name="jobid" value="<%=jobid%>">  


                        <style>
                            .container_label {
                                display: block;
                                position: relative;
                                padding-left: 35px;
                                margin-bottom: 12px;
                                cursor: pointer;
                                font-size: 22px;
                                -webkit-user-select: none;
                                -moz-user-select: none;
                                -ms-user-select: none;
                                user-select: none;
                            }

                            /* Hide the browser's default checkbox */
                            .container_label input {
                                position: absolute;
                                opacity: 0;
                                cursor: pointer;
                                height: 0;
                                width: 0;
                            }

                            /* Create a custom checkbox */
                            .checkmark_label {
                                position: absolute;
                                top: 0;
                                left: 0;
                                height: 25px;
                                width: 25px;
                                background-color: #eee;
                            }

                            /* On mouse-over, add a grey background color */
                            .container_label:hover input ~ .checkmark_label {
                                background-color: #ccc;
                            }

                            /* When the checkbox is checked, add a blue background */
                            .container_label input:checked ~ .checkmark_label {
                                background-color: #2196F3;
                            }

                            /* Create the checkmark/indicator (hidden when not checked) */
                            .checkmark_label:after {
                                content: "";
                                position: absolute;
                                display: none;
                            }

                            /* Show the checkmark when checked */
                            .container_label input:checked ~ .checkmark_label:after {
                                display: block;
                            }

                            /* Style the checkmark/indicator */
                            .container_label .checkmark_label:after {
                                left: 9px;
                                top: 5px;
                                width: 5px;
                                height: 10px;
                                border: solid white;
                                border-width: 0 3px 3px 0;
                                -webkit-transform: rotate(45deg);
                                -ms-transform: rotate(45deg);
                                transform: rotate(45deg);
                            }
                        </style>



                        <div class="form-group">
                            <label class="container_label">Salary negotiable 
                                <input type="checkbox" name="salary_nego" value="1" id="negoCheckbox" <% if (ejobSalaryNego.equals("Yes")) { %> checked="true" <% }%> >
                                <span class="checkmark_label"></span>
                            </label>
                        </div>


                        <div class="form-group">
                            <label for="newsContent" class="control-label">Job experience</label>
                            <textarea id="newsShortContent" name="jobExperience" placeholder="Experience required for the post" class="form-control" cols="10" rows="10"><%=ejobExperience%></textarea>
                            <div id="newsShortContentErr" class="help-block with-errors"></div>

                            <script type="text/javascript">
                                $(function () {
                                    $("#newsShortContent").wysihtml5();

                                });

                                $("#negoCheckbox").change(function () {
                                    if (this.checked) {
                                        console.log("Selected");





                                        $("#container_min_salary").hide();
                                        $("#container_max_salary").hide();



                                    } else {
                                        console.log("Unchecked");
                                        $("#container_min_salary").show();
                                        $("#container_max_salary").show();

//                                        $("#container_max_salary").prop('required', true);
//                                        $("#container_min_salary").prop('required', true);
                                    }
                                });
                            </script> 
                        </div>



                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="jobTitle" class="control-label">Location</label>
                            <input type="text" id="jobTitle" name="jobLocation"  placeholder="Location of working place" class="form-control input-sm" required value="<%=ejobLocation%>">
                            <div  id="jobTitleErr" class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="eventDate">Application deadline for the post </label>       


                            <input  class="form-control" type="date" name="jobDeadline" id="eventDate" value="<%=deadline%>"   pattern="MM-dd-yyyy" >
                        </div>






                        <div class="form-group row">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

<%
dbsession.clear();
dbsession.close();
%>


    <%@ include file="../footer.jsp" %>