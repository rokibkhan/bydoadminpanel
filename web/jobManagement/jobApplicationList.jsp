

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);

        String jobIdX = request.getParameter("jobIdX") == null ? "" : request.getParameter("jobIdX").trim();
        
        
         dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    Query chkApplySQL = null;

                                    int ix = 1;
                                    String jobId = "";
                                    String jobCategoryId = "";
                                    String jobDesignation = "";
                                    String jobDescription = "";
                                    String jobAge = "";
                                    String jobGender = "";
                                    String jobSalaryMin = "";
                                    String jobSalaryMax = "";
                                    String jobSalaryNego = "";
                                    String jobExperience = "";
                                    String jobType = "";
                                    String jobCompany = "";
                                    String jobLocation = "";
                                    String jobApplicationDeadline = "";
                                    String syStatus = "";
                                    String userStatusDesc = "";
                                    String userRole = "";
                                    String btnColorClass = "";
                                    String levelStatusColorClass = "";
                                    String deadline = "";
                                    String applicantListBtn = "";
                                    String applicationUrl = "";

        Query usrSQL = dbsession.createSQLQuery("SELECT * "
                + "FROM  job_post  ni ORDER BY ni.ID");

        if (!usrSQL.list().isEmpty()) {
            for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                obj = (Object[]) it1.next();
                jobId = obj[0] != null ? obj[0].toString().trim() : "";
                jobCategoryId = obj[1] != null ? obj[1].toString().trim() : "";
                jobDesignation = obj[2] != null ? obj[2].toString().trim() : "";
                jobDescription = obj[3] != null ? obj[3].toString().trim() : "";
                jobAge = obj[4] != null ? obj[4].toString().trim() : "";
                jobGender = obj[5] != null ? obj[5].toString().trim() : "";
                jobSalaryMin = obj[6] != null ? obj[6].toString().trim() : "";
                jobSalaryMax = obj[7] != null ? obj[7].toString().trim() : "";
                jobSalaryNego = obj[8] != null ? obj[8].toString().trim() : "";
                if (jobSalaryNego.equals("0")) {
                    jobSalaryNego = "No";
                } else {
                    jobSalaryNego = "Yes";
                }
                jobExperience = obj[9] != null ? obj[9].toString().trim() : "";
                jobType = obj[10] != null ? obj[10].toString().trim() : "";
                jobCompany = obj[11] != null ? obj[11].toString().trim() : "";
                jobLocation = obj[12] != null ? obj[12].toString().trim() : "";
                jobApplicationDeadline = obj[13] != null ? obj[13].toString().trim() : "";
                deadline = jobApplicationDeadline.split(" ")[0];

                Query us1 = dbsession.createSQLQuery("SELECT * "
                        + "FROM  job_post_category  WHERE ID = " + jobCategoryId);

                Iterator i1 = us1.list().iterator();
                Object[] o1 = (Object[]) i1.next();
                String jobCategoryName = o1[1].toString();

                us1 = dbsession.createSQLQuery("SELECT * "
                        + "FROM  job_post_type  WHERE ID = " + jobType);
                i1 = us1.list().iterator();
                o1 = (Object[]) i1.next();
                jobType = o1[1].toString();

                us1 = dbsession.createSQLQuery("SELECT * "
                        + "FROM  company  WHERE ID = " + jobCompany);
                i1 = us1.list().iterator();
                o1 = (Object[]) i1.next();
                jobCompany = o1[1].toString();
            }
        }

    %>



    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Application List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Job Management</a></li>                    
                    <li class="active">Application</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">
                    Job Details:<%=jobIdX%>

                </div>
            </div>

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Applicant Info</th>
                                    <th>Experience</th>
                                    <th>Expected Salary</th>

                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String jobAppId = "";
                                    String jobAppMemberId = "";
                                    String jobAppMemberIDX = "";
                                    String jobAppMemberName = "";
                                    String jobAppExpectedSalary = "";
                                    String jobAppApplyDate = "";
                                    
//                                    Query usrSQL = dbsession.createSQLQuery("SELECT u.USER_ID,u.USER_NAME,d.DEPT_NAME,st.STATUS_DESC,r.ROLE_DESC "
//                                            + "FROM sy_dept d ,sy_status st,sy_user u "
//                                            + "LEFT OUTER JOIN sy_user_role ur ON u.USER_ID = ur.USER_ID "
//                                            + "LEFT OUTER JOIN sy_roles r ON  r.ROLE_ID = ur.ROLE_ID "
//                                            + "WHERE u.USER_DEPT = d.DEPT_ID AND u.USER_STATUS = st.STATUS_CODE "
//                                            + "ORDER BY u.USER_ID ASC");
                                    Query usrSQL = dbsession.createSQLQuery("SELECT * "
                                            + "FROM  member_applied_job ni WHERE ni.job_id = '" + jobIdX + "' "
                                            + "ORDER BY ni.ID");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            jobAppId = obj[0] != null ? obj[0].toString().trim() : "";
                                            jobAppMemberId = obj[1] != null ? obj[1].toString().trim() : "";
                                            jobAppExpectedSalary = obj[3] != null ? obj[3].toString().trim() : "";
                                            jobAppApplyDate = obj[4] != null ? obj[4].toString().trim() : "";

                                            Query us1 = dbsession.createSQLQuery("SELECT * "
                                                    + "FROM  member  WHERE id = " + jobAppMemberId);

                                            Iterator i1 = us1.list().iterator();
                                            Object[] o1 = (Object[]) i1.next();
                                            jobAppMemberIDX = o1[1].toString();
                                            jobAppMemberName = o1[2].toString();

                                            String updateProfileUrl = GlobalVariable.baseUrl + "/jobManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&jobIdX=" + jobAppId + "&selectedTab=profile";
                                            String updateChangePassUrl = GlobalVariable.baseUrl + "/jobManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&jobIdX=" + jobAppId + "&selectedTab=changePass";


                                %>

                            <style>
                                td {
                                    text-align: center;
                                    vertical-align: middle;
                                }

                            </style>


                            <tr id="infoBox<%=jobAppId%>">
                                <td><% out.print(ix);%></td>
                                <td><% out.print(jobAppMemberName + "<br/>" + jobAppMemberIDX);%></td>
                                <td><% out.print(jobAppExpectedSalary);%></td>
                                <td><% out.println(jobAppApplyDate); %></td>

                                <td class="text-center">

                                    <a onclick="jobEditInfo(<% out.print("'" + jobAppId + "','" + sessionid + "'");%>)" href="jobEdit.jsp?sessionid=<%=sessionid%>&jobid=<%=jobAppId%>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                    <a onclick="jobDeleteInfo(<% out.print("'" + jobAppId + "','" + sessionid + "'");%>)" title="Delete this job" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                </td>
                            </tr>

                            <%
                                        ix++;
                                    }

                                }
                                dbsession.clear();
                                dbsession.close();
                            %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>