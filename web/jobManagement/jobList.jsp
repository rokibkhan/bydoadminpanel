

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showJobDetails(arg1, arg2) {

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Details for job #" + arg1);



            $.post("jobShowProcess.jsp", {jobId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    rupantorLGModal.find("#rupantorLGModalTitle").html("Job #" + arg1 + "<br/>" + "Job designation: " + data[0].jobDesignation);
                    rupantorLGModal.find("#rupantorLGModalBody").html("Job description: " + data[0].jobDescription
                            + "<br/>" + "Age limit: " + data[0].jobAge
                            + "<br/>" + "Gender: " + data[0].jobGender

                            + "<br/>" + "Salary Minimum: " + data[0].jobSalaryMin
                            + "<br/>" + "Salary Maximum: " + data[0].jobSalaryMax
                            + "<br/>" + "Salary negotiable: " + data[0].jobSalaryNego
                            + "<br/>" + "Experience: " + data[0].jobExperience
                            + "<br/>" + "Company: " + data[0].jobCompany
                            + "<br/>" + "Location: " + data[0].jobLocation
                            + "<br/>" + "Application Deadline: " + data[0].deadline);

//                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
//                    $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h2>"+ data[0].responseMsg + "</h2></td>");
//                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");



            rupantorLGModal.modal('show');
        }


        function syUserInfoUpdateOpt(arg1, arg2) {
            console.log("User :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Update User Details");

            $.post("syUserListUserDetails.jsp", {userId: arg1, sessionid: arg2}, function (data) {
//
//
//                console.log(data);
//
//                console.log("invoiceDetails :: " + data[0].invoiceDetails);
//
//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            rupantorLGModal.modal('show');

        }






        var modifyFlag = false;


        function jobDeleteInfo(arg1, arg2) {

            console.log("jobDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Job Delete confirmation");
            btnInfo = "<a onclick=\"jobDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            if (modifyFlag === false) {
                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());
                modifyFlag = true;
            }

//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }

        function jobEditInfo(arg1, arg2) {
            console.log("jobEdit: " + arg1 + "-" + arg2);
        }



        function jobDeleteInfoConfirm(arg1, arg2) {
            console.log("jobDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');

//            $.post(
//                    "jobDeleteProcess.jsp", 
//                    {userId: arg1, sessionid: arg2},
//                    function(data){
//                
//                    }
//          );

            $.post("jobDeleteProcess.jsp", {jobId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Job List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Job Management</a></li>                    
                    <li class="active">Job</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Job category</th>
                                    <th>Designation</th>
                                    <th>Description</th>
                                    <th>Age limit</th>
                                    <th>Gender</th>
                                    <th>Job salary (min)</th>
                                    <th>Job salary (max)</th>                                    
                                    <th>Salary negotiable</th>
                                    <th>Job experience</th>
                                    <th>Job type</th>
                                    <th>Company</th>
                                    <th>Location</th>
                                    <th>Deadline</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    Query chkApplySQL = null;

                                    int ix = 1;
                                    String jobId = "";
                                    String jobCategoryId = "";
                                    String jobDesignation = "";
                                    String jobDescription = "";
                                    String jobAge = "";
                                    String jobGender = "";
                                    String jobSalaryMin = "";
                                    String jobSalaryMax = "";
                                    String jobSalaryNego = "";
                                    String jobExperience = "";
                                    String jobType = "";
                                    String jobCompany = "";
                                    String jobLocation = "";
                                    String jobApplicationDeadline = "";
                                    String syStatus = "";
                                    String userStatusDesc = "";
                                    String userRole = "";
                                    String btnColorClass = "";
                                    String levelStatusColorClass = "";
                                    String deadline = "";
                                    String applicantListBtn = "";
                                    String applicationUrl = "";

//                                    Query usrSQL = dbsession.createSQLQuery("SELECT u.USER_ID,u.USER_NAME,d.DEPT_NAME,st.STATUS_DESC,r.ROLE_DESC "
//                                            + "FROM sy_dept d ,sy_status st,sy_user u "
//                                            + "LEFT OUTER JOIN sy_user_role ur ON u.USER_ID = ur.USER_ID "
//                                            + "LEFT OUTER JOIN sy_roles r ON  r.ROLE_ID = ur.ROLE_ID "
//                                            + "WHERE u.USER_DEPT = d.DEPT_ID AND u.USER_STATUS = st.STATUS_CODE "
//                                            + "ORDER BY u.USER_ID ASC");
                                    Query usrSQL = dbsession.createSQLQuery("SELECT * "
                                            + "FROM  job_post  ni ORDER BY ni.ID");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            jobId = obj[0] != null ? obj[0].toString().trim() : "";
                                            jobCategoryId = obj[1] != null ? obj[1].toString().trim() : "";
                                            jobDesignation = obj[2] != null ? obj[2].toString().trim() : "";
                                            jobDescription = obj[3] != null ? obj[3].toString().trim() : "";
                                            jobAge = obj[4] != null ? obj[4].toString().trim() : "";
                                            jobGender = obj[5] != null ? obj[5].toString().trim() : "";
                                            jobSalaryMin = obj[6] != null ? obj[6].toString().trim() : "";
                                            jobSalaryMax = obj[7] != null ? obj[7].toString().trim() : "";
                                            jobSalaryNego = obj[8] != null ? obj[8].toString().trim() : "";
                                            if (jobSalaryNego.equals("0")) {
                                                jobSalaryNego = "No";
                                            } else {
                                                jobSalaryNego = "Yes";
                                            }
                                            jobExperience = obj[9] != null ? obj[9].toString().trim() : "";
                                            jobType = obj[10] != null ? obj[10].toString().trim() : "";
                                            jobCompany = obj[11] != null ? obj[11].toString().trim() : "";
                                            jobLocation = obj[12] != null ? obj[12].toString().trim() : "";
                                            jobApplicationDeadline = obj[13] != null ? obj[13].toString().trim() : "";
                                            deadline = jobApplicationDeadline.split(" ")[0];

                                            Query us1 = dbsession.createSQLQuery("SELECT * "
                                                    + "FROM  job_post_category  WHERE ID = " + jobCategoryId);

                                            Iterator i1 = us1.list().iterator();
                                            Object[] o1 = (Object[]) i1.next();
                                            String jobCategoryName = o1[1].toString();

                                            us1 = dbsession.createSQLQuery("SELECT * "
                                                    + "FROM  job_post_type  WHERE ID = " + jobType);
                                            i1 = us1.list().iterator();
                                            o1 = (Object[]) i1.next();
                                            jobType = o1[1].toString();

                                            us1 = dbsession.createSQLQuery("SELECT * "
                                                    + "FROM  company  WHERE ID = " + jobCompany);
                                            i1 = us1.list().iterator();
                                            o1 = (Object[]) i1.next();
                                            jobCompany = o1[1].toString();

                                            String updateProfileUrl = GlobalVariable.baseUrl + "/jobManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&jobIdX=" + jobId + "&selectedTab=profile";
                                            String updateChangePassUrl = GlobalVariable.baseUrl + "/jobManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&jobIdX=" + jobId + "&selectedTab=changePass";

                                            applicationUrl = GlobalVariable.baseUrl + "/jobManagement/jobApplicationList.jsp?sessionid=" + session.getId() + "&jobIdX=" + jobId + "&selectedTab=profile";

                                            chkApplySQL = dbsession.createSQLQuery("SELECT * FROM  member_applied_job WHERE job_id = '" + jobId + "'");

                                            if (!chkApplySQL.list().isEmpty()) {
                                                applicantListBtn = "<a href=\"" + applicationUrl + "\" title=\"Applicant List\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-codiepie\"></i></a>";
                                            } else {
                                                applicantListBtn = "";
                                            }


                                %>

                            <style>
                                td {
                                    text-align: center;
                                    vertical-align: middle;
                                }

                            </style>


                            <tr id="infoBox<%=jobId%>">
                                <td><% out.print(ix);%></td>
                                <td><% out.print(jobCategoryName);%></td>
                                <td><% out.print(jobDesignation);%></td>
                                <td><% out.println(jobDescription); %></td>
                                <td><% out.println(jobAge); %></td>

                                <td><% out.println(jobGender); %></td>

                                <td><% out.print(jobSalaryMin); %></td>
                                <td><% out.print(jobSalaryMax); %></td>
                                <td><% out.print(jobSalaryNego); %></td>
                                <td><% out.print(jobExperience); %></td>
                                <td><% out.print(jobType); %></td>
                                <td><% out.print(jobCompany); %></td>
                                <td><% out.print(jobLocation); %></td>
                                <td><% out.print(deadline); %></td>
                                <td class="text-center">

                                    <a id="btnShowSyUserDetails<% out.print(jobId);%>" onclick="showJobDetails(<% out.print("'" + jobId + "','" + sessionid + "'");%>);" title="Job details" class="btn btn-primary btn-sm"><i class="icon-info"></i></a>

                                    <%=applicantListBtn%>

                                    <a onclick="jobEditInfo(<% out.print("'" + jobId + "','" + sessionid + "'");%>)" href="jobEdit.jsp?sessionid=<%=sessionid%>&jobid=<%=jobId%>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                    <a onclick="jobDeleteInfo(<% out.print("'" + jobId + "','" + sessionid + "'");%>)" title="Delete this job" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                </td>
                            </tr>

                            <%
                                        ix++;
                                    }

                                }
                                dbsession.clear();
                                dbsession.close();
                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>