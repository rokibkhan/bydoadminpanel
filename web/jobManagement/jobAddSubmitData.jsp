<%-- 
    Document   : Sysytem User dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Session dbsession = null;
    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        userStoreIdH = session.getAttribute("storeId").toString();
        int userStoreIdHi = Integer.parseInt(userStoreIdH);

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(26);
        int jobID = Integer.parseInt(idS);

        String jobDesignation = request.getParameter("jobDesignation");
        String jobDescription = request.getParameter("jobDescription");
        String jobAge = request.getParameter("jobAge");
        String jobGender = request.getParameter("jobGender");
        String jobSalaryMin = request.getParameter("jobSalaryMin");
        String jobSalaryMax = request.getParameter("jobSalaryMax");

        String jobSalaryNego = request.getParameter("salary_nego") != null ? "1" : "0";

        String jobApplicationDeadline = request.getParameter("eventDate");
        String jobExperience = request.getParameter("jobExperience");
        String location = request.getParameter("jobLocation");
        String jobPostCategoryId = request.getParameter("jobCategory");
        String jobPostTypeID = request.getParameter("jobPostType");
        String companyID = request.getParameter("company");


        String deadline = request.getParameter("jobDeadline").toString();

        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = formatter1.parse(deadline);

        System.out.println("" + jobDesignation + "-" + jobDescription + "-" + jobApplicationDeadline + "-" + jobSalaryMin + "-" + jobSalaryMax + "-" + jobSalaryNego + "-");

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);

        System.out.println("adddate   adddate:: " + adddate);

        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();

        /*

        Query q = dbsession.createSQLQuery("INSERT INTO job_post("
                + "ID,JOB_POST_CATEGORY_ID,JOB_DESIGNATION,JOB_DESCRIPTION, JOB_AGE, GENDER, JOB_SALARY_MIN, JOB_SALARY_MAX, "
                + "JOB_SALARY_NEGOTIABLE, JOB_EXPERIENCE, JOB_TYPE_ID, COMPANY_ID,  LOCATION, APPLICATION_DEADLINE, ADD_USER)"
                + " VALUES ( \"" + jobID + "\".\"" + jobPostCategoryId + "\",\""
                + jobDesignation
                + "\",\""
                + jobDescription + "\", \""
                + jobAge + "\", \""
                + jobGender + "\""
                + ",\"" + jobSalaryMin
                + "\", \"" + jobSalaryMax + "\", \""
                + jobSalaryNego + "\",\""
                + jobExperience + "\", "
                + "\"" + jobPostTypeID + "\", "
                + "\"" + companyID + "\", "
                + "\"" + location + "\", "
                + "\"" + deadline + "\",\" "
                + userNameH + "\"" + ")");

        q.executeUpdate();
        
         */
        JobPost job = new JobPost();
        JobPostCategory jobCat = new JobPostCategory();

        job.setId(jobID);

        jobCat.setId(Integer.parseInt(jobPostCategoryId));
        job.setJobPostCategory(jobCat);

        job.setJobDesignation(jobDesignation);
        job.setApplicationDeadline(date1);

        job.setJobDescription(jobDescription);
        job.setJobExperience(jobExperience);

        job.setJobSalaryMin(jobSalaryMin);
        job.setJobSalaryMax(jobSalaryMax);
        job.setJobSalaryNegotiable(Byte.parseByte(jobSalaryNego));

        job.setJobAge(jobAge);
        job.setGender(jobGender);
        job.setLocation(location);

        job.setCompanyId(Integer.parseInt(companyID));
        job.setJobTypeId(Integer.parseInt(jobPostTypeID));

        job.setAddDate(adddate);
        job.setAddUser(adduser);
        job.setAddTerm(addterm);
        job.setAddIp(addip);

        dbsession.save(job);

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            strMsg = "Success !!! New Job added";
            response.sendRedirect(GlobalVariable.baseUrl + "/jobManagement/jobAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
        } else {
            dbtrx.rollback();
            strMsg = "Error!!! When Job add";
            response.sendRedirect(GlobalVariable.baseUrl + "/jobManagement/jobAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
        }

    } else {
        strMsg = "Error!!! When adding Job.Please try again";
        response.sendRedirect(GlobalVariable.baseUrl + "/jobManagement/jobAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
    }


%>
