

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){





        var modifyFlag = false;


        function newsDeleteInfo(arg1, arg2) {

            console.log("newsDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("News Delete confirmation");
            btnInfo = "<a onclick=\"newsDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            if (modifyFlag === false) {
                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());
                modifyFlag = true;
            }

//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }

        function newsEditInfo(arg1, arg2) {
            console.log("newsEdit: " + arg1 + "-" + arg2);
        }



        function newsDeleteInfoConfirm(arg1, arg2) {
            console.log("newsDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');

//            $.post(
//                    "newsDeleteProcess.jsp", 
//                    {userId: arg1, sessionid: arg2},
//                    function(data){
//                
//                    }
//          );

            $.post("newsDeleteProcess.jsp", {memberXId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=\"6\" style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }

        function addCenterForNews(arg1, arg2, arg3) {
            console.log("addCenterForNews :: " + arg1);
            var taskLGModal, btnConfirmInfo, url;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addCenterForNewsConfirmBtn\" onclick=\"addCenterForNewsConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Center for News");

            url = '<%=GlobalVariable.baseUrl%>' + "/newsManagement/newsCenterInfoShow.jsp";

            console.log("url :: " + url);

            $.post(url, {memberXId: arg1, newsName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {

                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addCenterForNewsConfirm(arg1, arg2, arg3) {
            console.log("addCenterForNewsConfirm :: " + arg1);
            var taskLGModal, url, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Center is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            url = '<%=GlobalVariable.baseUrl%>' + "/newsManagement/newsCenterInfoAddConfirm.jsp";

            console.log("url :: " + url);


            $.post(url, {memberXId: arg1, newsName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }


        function addDivisionForNews(arg1, arg2, arg3) {
            console.log("addDivisionForNews :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addDivisionForNewsConfirmBtn\" onclick=\"addDivisionForNewsConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Division for News");

            $.post("newsDivisionInfoShow.jsp", {memberXId: arg1, newsName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addDivisionForNewsConfirm(arg1, arg2, arg3) {
            console.log("addDivisionForNewsConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Division is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("newsDivisionInfoAddConfirm.jsp", {memberXId: arg1, newsName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }




    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Medicine List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Medicine Management</a></li>                    
                    <li class="active">Medicine</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 3%;">#</th>
                                    <th class="text-center" style="width: 30%;">Name</th>
                                    <th class="text-center" style="width: 15%;">Geniric Name</th>
                                    <th class="text-center" style="width: 7%;">Type</th>
                                    <th class="text-center">Assign</th>
                                    <th class="text-center" style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;

                                    String memberSQL = null;
                                    String memberCountSQL = null;
                                    Query memberSQLQry = null;
                                    Object[] memberObj = null;

                                    String memberXId = "";
                                    String medicineName = "";
                                    String medicineGeneticName = "";
                                    String description = "";
                                    String uses = "";
                                    String composition = "";
                                    String dosageChild = "";
                                    String dosageAdult = "";
                                    String dosageRenal = "";
                                    String dosageAdministration = "";
                                    String contraindication = "";

                                    String sideEffects = "";

                                    String precaution = "";
                                    String uses_in_ptegnancy = "";

                                    String uses_in_paediatric = "";
                                    String therapeutic_class = "";
                                    String mode_of_action = "";
                                    String interaction = "";

                                    String storage_condition = "";

                                    String package_size_price = "";

                                    String preparation = "";
                                    String attachment = "";
                                    String feature_image = "";
                                    String featureImageLink = "";
                                    String medicineStructureShortName = "";
                                    String medicineStructureName = "";
                                    String medicineStructureMG_ML = "";
                                    String typeShortName = "";
                                    String typeName = "";
                                    String typeDetails = "";

                                    String filterSQLStr = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT m.id, m.medicine_name,m.medicine_genetic_name, "
                                            + "m.description,m.uses,m.composition,m.dosage_adult,   "
                                            + " m.dosage_child,m.dosage_renal,m.dosage_administration,m.contraindication,"
                                            + "m.side_effects,m.precaution,m.uses_in_ptegnancy,"
                                            + "m.uses_in_paediatric,m.therapeutic_class,m.mode_of_action,m.interaction, "
                                            + "m.storage_condition,m.package_size_price,m.preparation,m.attachment,"
                                            + "m.feature_image,"
                                            + "ms.SHORT_NAME,ms.NAME,ms.MG_ML,"
                                            + "mt.SHORT_NAME typeShortName,mt.NAME typeName,mt.details  typeDetails   "
                                            + "FROM  medicine_info m,medicine_structure ms,medicine_type mt "
                                            + "WHERE m.published = 1 "
                                            + "AND m.structure = ms.ID "
                                            + "AND m.medicine_type = mt.ID "
                                            + " " + filterSQLStr + " "
                                            + "ORDER BY m.id ASC  ");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator memberItr = usrSQL.list().iterator(); memberItr.hasNext();) {

                                            memberObj = (Object[]) memberItr.next();
                                            memberXId = memberObj[0].toString();
                                            medicineName = memberObj[1] == null ? "" : memberObj[1].toString();
                                            medicineGeneticName = memberObj[2] == null ? "" : memberObj[2].toString();

                                            description = memberObj[3] == null ? "" : memberObj[3].toString();
                                            //   memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;

                                            //     logger.info("API :: medicineInfo API pictureLink ::" + memberXPictureLink);                
                                            uses = memberObj[4] == null ? "" : memberObj[4].toString();
                                            composition = memberObj[5] == null ? "" : memberObj[5].toString();
                                            dosageAdult = memberObj[6] == null ? "" : memberObj[6].toString();

                                            dosageRenal = memberObj[7] == null ? "" : memberObj[7].toString();
                                            dosageChild = memberObj[8] == null ? "" : memberObj[8].toString();
                                            dosageAdministration = memberObj[9] == null ? "" : memberObj[9].toString();
                                            contraindication = memberObj[10] == null ? "" : memberObj[10].toString();

                                            sideEffects = memberObj[11] == null ? "" : memberObj[11].toString();
                                            precaution = memberObj[12] == null ? "" : memberObj[12].toString();
                                            uses_in_ptegnancy = memberObj[13] == null ? "" : memberObj[13].toString();

                                            uses_in_paediatric = memberObj[14] == null ? "" : memberObj[14].toString();
                                            therapeutic_class = memberObj[15] == null ? "" : memberObj[15].toString();
                                            mode_of_action = memberObj[16] == null ? "" : memberObj[16].toString();
                                            interaction = memberObj[17] == null ? "" : memberObj[17].toString();

                                            storage_condition = memberObj[18] == null ? "" : memberObj[18].toString();
                                            package_size_price = memberObj[19] == null ? "" : memberObj[19].toString();
                                            preparation = memberObj[20] == null ? "" : memberObj[20].toString();
                                            attachment = memberObj[21] == null ? "" : memberObj[21].toString();

                                            feature_image = memberObj[22] == null ? "" : memberObj[22].toString();

                                            featureImageLink = "";

                                            medicineStructureShortName = memberObj[23] == null ? "" : memberObj[23].toString();
                                            medicineStructureName = memberObj[24] == null ? "" : memberObj[24].toString();
                                            medicineStructureMG_ML = memberObj[25] == null ? "" : memberObj[25].toString();

                                            typeShortName = memberObj[26] == null ? "" : memberObj[26].toString();
                                            typeName = memberObj[27] == null ? "" : memberObj[27].toString();
                                            typeDetails = memberObj[28] == null ? "" : memberObj[28].toString();

                                %>


                                <tr id="infoBox<%=memberXId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(medicineName);%></td>
                                    <td><% out.println(medicineGeneticName); %></td>
                                    <td><% out.print(medicineStructureName);%></td>
                                    <td class="text-center" id="assignBox<%=memberXId%>">



                                    </td>
                                    <td class="text-left" style="padding-left: 10px;">



                                        <a href="newsDetails.jsp?sessionid=<%=sessionid%>&newsid=<%=memberXId%>" title="News Deatils" class="btn btn-primary btn-sm"><i class="icon-info"></i></a>
                                        <a onclick="newsEditInfo(<% out.print("'" + memberXId + "','" + sessionid + "'");%>)" href="newsEdit.jsp?sessionid=<%=sessionid%>&newsid=<%=memberXId%>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onclick="newsDeleteInfo(<% out.print("'" + memberXId + "','" + sessionid + "'");%>)" title="Delete this news" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%    dbsession.clear();
        dbsession.close();
        %>


    <%@ include file="../footer.jsp" %>