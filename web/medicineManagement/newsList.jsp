

<%@page import="com.appul.entity.NewsDivision"%>
<%@page import="com.appul.entity.NewsCenter"%>
<%@page import="com.appul.entity.EventDivision"%>
<%@page import="com.appul.entity.EventCenter"%>
<%@page import="com.appul.entity.AlbumEvent"%>
<%@page import="com.appul.entity.EventSlug"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){





        var modifyFlag = false;


        function newsDeleteInfo(arg1, arg2) {

            console.log("newsDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("News Delete confirmation");
            btnInfo = "<a onclick=\"newsDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            if (modifyFlag === false) {
                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());
                modifyFlag = true;
            }

//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }

        function newsEditInfo(arg1, arg2) {
            console.log("newsEdit: " + arg1 + "-" + arg2);
        }



        function newsDeleteInfoConfirm(arg1, arg2) {
            console.log("newsDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');

//            $.post(
//                    "newsDeleteProcess.jsp", 
//                    {userId: arg1, sessionid: arg2},
//                    function(data){
//                
//                    }
//          );

            $.post("newsDeleteProcess.jsp", {newsId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=\"6\" style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }

        function addCenterForNews(arg1, arg2, arg3) {
            console.log("addCenterForNews :: " + arg1);
            var taskLGModal, btnConfirmInfo, url;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addCenterForNewsConfirmBtn\" onclick=\"addCenterForNewsConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Center for News");

            url = '<%=GlobalVariable.baseUrl%>' + "/newsManagement/newsCenterInfoShow.jsp";

            console.log("url :: " + url);

            $.post(url, {newsId: arg1, newsName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {

                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addCenterForNewsConfirm(arg1, arg2, arg3) {
            console.log("addCenterForNewsConfirm :: " + arg1);
            var taskLGModal, url, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Center is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            url = '<%=GlobalVariable.baseUrl%>' + "/newsManagement/newsCenterInfoAddConfirm.jsp";

            console.log("url :: " + url);


            $.post(url, {newsId: arg1, newsName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }


        function addDivisionForNews(arg1, arg2, arg3) {
            console.log("addDivisionForNews :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addDivisionForNewsConfirmBtn\" onclick=\"addDivisionForNewsConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Division for News");

            $.post("newsDivisionInfoShow.jsp", {newsId: arg1, newsName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addDivisionForNewsConfirm(arg1, arg2, arg3) {
            console.log("addDivisionForNewsConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Division is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("newsDivisionInfoAddConfirm.jsp", {newsId: arg1, newsName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }




    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">News List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">News Management</a></li>                    
                    <li class="active">News</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 3%;">#</th>
                                    <th class="text-center" style="width: 30%;">Title</th>
                                    <th class="text-center" style="width: 15%;">Featured Images</th>
                                    <th class="text-center" style="width: 7%;">Date</th>
                                    <th class="text-center">Assign News</th>
                                    <th class="text-center" style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String newsId = "";
                                    String newsTitle = "";
                                    String newsSlug = "";
                                    String newsShortDetails = "";
                                    String newsDetails = "";
                                    String newsDateTime = "";
                                    String featureImage = "";
                                    String featureImage1 = "";
                                    String featureImageUrl = "";
                                    String userRole = "";
                                    String btnColorClass = "";
                                    String levelStatusColorClass = "";

                                    String updateProfileUrl = "";
                                    String updateChangePassUrl = "";

                                    String agrX = "";
                                    String agrXcenter = "";
                                    String btnAddCenter = "";
                                    String btnAddSubCenter = "";
                                    String btnAddDivision = "";
                                    String btnAddAlbum = "";
                                    String btnAddNews = "";
                                    String btnAddTag = "";
                                    String btnAddVideo = "";

                                    String centerDivisionTagContainer = "";

                                    String centerListContainer = "";
                                    NewsCenter nwc = null;
                                    String centerListCenterName = "";
                                    Query centerListSQL = null;

                                    String divisionListContainer = "";
                                    NewsDivision nwd = null;
                                    String divisionListCenterName = "";
                                    Query divisionListSQL = null;

                                    String tagListContainer = "";
                                    EventSlug alt = null;
                                    String tagListCenterName = "";
                                    Query tagListSQL = null;

                                    Query usrSQL = dbsession.createSQLQuery("SELECT ni.id_news, ni.news_title, ni.news_short_desc, ni.news_desc, "
                                            + "ni.news_date, ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
                                            + "FROM  news_info ni ORDER BY ni.id_news DESC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            newsId = obj[0].toString().trim();
                                            newsTitle = obj[1].toString().trim();
                                            newsShortDetails = obj[2].toString().trim();
                                            newsDetails = obj[3].toString().trim();

                                            newsDateTime = obj[4] == null ? "" : obj[4].toString().trim();

                                            featureImage = obj[5] == null ? "" : obj[5].toString().trim();
                                            featureImageUrl = GlobalVariable.imageNewsDirLink + featureImage;

                                            featureImage1 = "<img width=\"200\" src=\"" + featureImageUrl + "\" alt=\"" + newsTitle + "\">";

                                            //       String updateProfileUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=profile";
                                            //      String updateChangePassUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=changePass";
                                            agrX = "'" + newsId + "','" + session.getId() + "'";
                                           
                                          //  agrXcenter = "\"" + newsId + "\",\"" + newsTitle + "\",\"" + session.getId() + "\"";
                                            agrXcenter = "'" + newsId + "','','" + session.getId() + "'";
                                            
                                            btnAddCenter = "<a onClick=\"addCenterForNews(" + agrXcenter + ");\" title=\"Add Center  for this Event\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Center</a>";
                                            //     btnAddSubCenter = "<a onClick=\"addSubCenterForNews(" + agrXcenter + ");\" title=\"Add SubCenter  for this Event\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Sub Center</a>";

                                            btnAddDivision = "<a onClick=\"addDivisionForNews(" + agrXcenter + ");\" title=\"Add Division  for this Event\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Division</a>";

                                            // show news Assign center,division,tag
                                            centerListContainer = "";
                                            centerListSQL = dbsession.createQuery(" FROM NewsCenter where newsInfo = '" + newsId + "'");
                                            if (!centerListSQL.list().isEmpty()) {
                                                for (Iterator centerListItr = centerListSQL.list().iterator(); centerListItr.hasNext();) {
                                                    nwc = (NewsCenter) centerListItr.next();
                                                    centerListCenterName = nwc.getCenter().getCenterName();

                                                    centerListContainer = centerListContainer + centerListCenterName + ",";
                                                }
                                            } else {
                                                centerListContainer = "";
                                            }
                                            divisionListContainer = "";
                                            divisionListSQL = dbsession.createQuery(" FROM NewsDivision where newsInfo = '" + newsId + "'");
                                            if (!divisionListSQL.list().isEmpty()) {
                                                for (Iterator divisionListItr = divisionListSQL.list().iterator(); divisionListItr.hasNext();) {
                                                    nwd = (NewsDivision) divisionListItr.next();
                                                    divisionListCenterName = nwd.getMemberDivision().getMemDivisionName();

                                                    divisionListContainer = divisionListContainer + divisionListCenterName + ",";
                                                }
                                            } else {
                                                divisionListContainer = "";
                                            }

//            tagListSQL = dbsession.createQuery(" FROM AlbumSlug where pictureCategory = '" + newsId + "'");
//            if (!tagListSQL.list().isEmpty()) {
//                for (Iterator tagListItr = tagListSQL.list().iterator(); tagListItr.hasNext();) {
//                    alt = (AlbumSlug) tagListItr.next();
//                    tagListCenterName = alt.get
//
//                    tagListContainer = tagListContainer + tagListCenterName + ",";
//                }
//            } else {
//                tagListContainer = "";
//            }
                                            centerDivisionTagContainer = centerListContainer + divisionListContainer;


                                %>


                                <tr id="infoBox<%=newsId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(newsTitle);%></td>
                                    <td><% out.println(featureImage1); %></td>
                                    <td><% out.print(newsDateTime);%></td>
                                    <td class="text-center" id="assignBox<%=newsId%>">

                                        <%=centerDivisionTagContainer%>

                                    </td>
                                    <td class="text-left" style="padding-left: 10px;">
                                        <%=btnAddCenter%>
                                        <%=btnAddSubCenter%>
                                        <%=btnAddDivision%>



                                        <a href="newsDetails.jsp?sessionid=<%=sessionid%>&newsid=<%=newsId%>" title="News Deatils" class="btn btn-primary btn-sm"><i class="icon-info"></i></a>
                                        <a onclick="newsEditInfo(<% out.print("'" + newsId + "','" + sessionid + "'");%>)" href="newsEdit.jsp?sessionid=<%=sessionid%>&newsid=<%=newsId%>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onclick="newsDeleteInfo(<% out.print("'" + newsId + "','" + sessionid + "'");%>)" title="Delete this news" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%    dbsession.clear();
        dbsession.close();
    %>


    <%@ include file="../footer.jsp" %>