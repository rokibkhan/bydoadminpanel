<%-- 
    Document   : medicineInfoAddSubmitData 
    Created on : Nov 07, 2020, 11:22:05 AM
    Author     : Tahajjat
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String adddate = dateFormatX.format(dateX);

    System.out.println("adddate   adddate:: " + adddate);

    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        userStoreIdH = session.getAttribute("storeId").toString();
        int userStoreIdHi = Integer.parseInt(userStoreIdH);

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(86); // Medicine ID
        int mdID = Integer.parseInt(idS);

        String medicineName = request.getParameter("medicineName");
        String genericName = request.getParameter("genericName");
        String medicineCompany = request.getParameter("medicineCompany");
        String medicineStructure = request.getParameter("medicineStructure");
        String medicineType = request.getParameter("medicineType");
        String medicineDesc = request.getParameter("medicineDesc");
        String therapeuticClass = request.getParameter("medicineShortDesc");
        String dosageAdministration = request.getParameter("dosageAdministration");
        String interaction = request.getParameter("interaction");

        System.out.println("mdID :: " + mdID);
        System.out.println("medicineName :: " + medicineName);
        System.out.println("genericName  ::" + genericName);
        System.out.println("medicineCompany  ::" + medicineCompany);
        System.out.println("medicineStructure  ::" + medicineStructure);
        System.out.println("medicineType   ::" + medicineType);
        System.out.println("medicineDesc  :: " + medicineDesc);
        System.out.println("therapeuticClass  :: " + therapeuticClass);
        System.out.println("dosageAdministration  :: " + dosageAdministration);
        System.out.println("interaction  :: " + interaction);

        /*
        String jobSalaryMax = request.getParameter("jobSalaryMax");

        String jobSalaryNego = request.getParameter("salary_nego") != null ? "1" : "0";

        String jobApplicationDeadline = request.getParameter("eventDate");
        String jobExperience = request.getParameter("jobExperience");
        String location = request.getParameter("jobLocation");
        String jobPostCategoryId = request.getParameter("jobCategory");
        String jobPostTypeID = request.getParameter("jobPostType");
        String companyID = request.getParameter("company");


        String deadline = request.getParameter("jobDeadline").toString();
         */
        //       SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        //       Date date1 = formatter1.parse(deadline);
        //  System.out.println("" + jobDesignation + "-" + jobDescription + "-" + jobApplicationDeadline + "-" + jobSalaryMin + "-" + jobSalaryMax + "-" + jobSalaryNego + "-");
        //  System.out.println("" + jobDesignation + "-" + jobDescription + "-" + jobApplicationDeadline + "-" + jobSalaryMin + "-" + jobSalaryMax + "-" + jobSalaryNego + "-");
        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();

        MedicineInfo medicine = new MedicineInfo();
        MedicineCompany medicineCom = new MedicineCompany();
        MedicineStructure medicineStuc = new MedicineStructure();
        MedicineType medicineTyp = new MedicineType();

        medicine.setId(mdID);

        medicine.setMedicineName(medicineName);
        medicine.setMedicineGeneticName(genericName);

        medicineCom.setId(Integer.parseInt(medicineCompany));
        medicine.setMedicineCompany(medicineCom);

        medicineStuc.setId(Integer.parseInt(medicineStructure));
        medicine.setMedicineStructure(medicineStuc);

        medicineTyp.setId(Integer.parseInt(medicineType));
        medicine.setMedicineType(medicineTyp);

        medicine.setDescription(medicineDesc);
        medicine.setUses(medicineDesc);

        medicine.setDosageAdministration(dosageAdministration);

        medicine.setTherapeuticClass(therapeuticClass);
        medicine.setInteraction(interaction);
        

        //  JobPost job = new JobPost();
        //   JobPostCategory jobCat = new JobPostCategory();
        //  job.setId(jobID);
        //   jobCat.setId(Integer.parseInt(jobPostCategoryId));
        //   job.setJobPostCategory(jobCat);
        //   job.setJobDesignation(jobDesignation);
        //    job.setApplicationDeadline(date1);
        //    job.setJobDescription(jobDescription);
        //    job.setJobExperience(jobExperience);
        //   job.setJobSalaryMin(jobSalaryMin);
        //    job.setJobSalaryMax(jobSalaryMax);
        //    job.setJobSalaryNegotiable(Byte.parseByte(jobSalaryNego));
        //   job.setJobAge(jobAge);
        //   job.setGender(jobGender);
        //   job.setLocation(location);
        //     job.setCompanyId(Integer.parseInt(companyID));
        //    job.setJobTypeId(Integer.parseInt(jobPostTypeID));
        medicine.setAddDate(adddate);
        medicine.setAddUser(adduser);
        medicine.setAddTerm(addterm);
        medicine.setAddIp(addip);

        dbsession.save(medicine);

        /*
        
        Query q4 = dbsession.createSQLQuery("INSERT INTO medicine_info("
                    + "id,"
                    + "structure,"
                    + "medicine_name,"
                    + "news_short_desc,"
                    + "news_desc,"
                    + "news_date_time,"
                    + "showing_order,"
                    + "feature_image,"
                    + "ADD_DATE,"
                    + "ADD_USER,"
                    + "ADD_TERM,"
                    + "ADD_IP) values( "
                    + "'" + mdID + "',"
                    + "'" + medicineStructure + "',"
                    + "'" + newsTitle + "',"
                    + "'" + newsShortDesc + "',"
                    + "'" + newsDesc + "',"
                    + "'" + adddate + "',"
                    + "'" + showingOrder + "',"
                    + "'" + fileName1 + "',"
                    + "'" + adddate + "',"
                    + "'" + adduser + "',"
                    + "'" + addterm + "',"
                    + "'" + addip + "')");

            q4.executeUpdate();
         */
        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            strMsg = "Success !!! New Medicine Info Added Successfully";
            response.sendRedirect(GlobalVariable.baseUrl + "/medicineManagement/medicineInfoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
        } else {
            dbtrx.rollback();
            strMsg = "Error!!! When Medicine Info Add";
            response.sendRedirect(GlobalVariable.baseUrl + "/medicineManagement/medicineInfoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
        }

    } else {
        strMsg = "Error!!! When adding Medicine.Please try again";
        response.sendRedirect(GlobalVariable.baseUrl + "/medicineManagement/medicineInfoAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
    }


%>
