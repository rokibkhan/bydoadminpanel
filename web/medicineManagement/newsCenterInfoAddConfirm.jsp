<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String newsId = request.getParameter("newsId");
    String newsName = request.getParameter("newsName");
    String centerDivisionTagId = request.getParameter("centerDivisionTagId");

    System.out.println("newsCenterInfoAddConfirm :: newsId " + newsId);
    System.out.println("newsCenterInfoAddConfirm :: newsName " + newsName);
    System.out.println("newsCenterInfoAddConfirm :: centerDivisionTagId " + centerDivisionTagId);

    q1 = dbsession.createSQLQuery("SELECT * FROM news_center WHERE NEWS_ID=" + newsId + " AND CENTER_ID =" + centerDivisionTagId + "");
    System.out.println("newsCenterInfoAddConfirm :: q1 " + q1);
    if (!q1.list().isEmpty()) {

        responseCode = "0";
        responseMsg = "Error!!! Already news assign this center";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", newsId);
        jsonArr.add(json);
    } else {

        //insert news id and center id in news_center table
        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(51);
        int newsCenterId = Integer.parseInt(idS);

        Query newsCenterAddSQL = dbsession.createSQLQuery("INSERT INTO news_center("
                + "ID,"
                + "NEWS_ID,"
                + "CENTER_ID"
                + ") VALUES("
                + "'" + newsCenterId + "',"
                + "'" + newsId + "',"
                + "'" + centerDivisionTagId + "'"
                + "  ) ");

        System.out.println("newsCenterInfoAddConfirm :: newsCenterAddSQL " + newsCenterAddSQL);

        newsCenterAddSQL.executeUpdate();

        dbtrx.commit();

        if (dbtrx.wasCommitted()) {
            //  strMsg = "Success !!! New Committee added";
            //  response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "1";
            responseMsg = "Success!!! Center add for news successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            // show news Assign center,division,tag
            String centerDivisionTagContainer = "";

            String centerListContainer = "";
            NewsCenter nwc = null;
            String centerListCenterName = "";
            Query centerListSQL = null;

            String divisionListContainer = "";
            NewsDivision nwd = null;
            String divisionListCenterName = "";
            Query divisionListSQL = null;

            String tagListContainer = "";
            EventSlug alt = null;
            String tagListCenterName = "";
            Query tagListSQL = null;

            centerListSQL = dbsession.createQuery(" FROM NewsCenter where newsInfo = '" + newsId + "'");
            if (!centerListSQL.list().isEmpty()) {
                for (Iterator centerListItr = centerListSQL.list().iterator(); centerListItr.hasNext();) {
                    nwc = (NewsCenter) centerListItr.next();
                    centerListCenterName = nwc.getCenter().getCenterName();

                    centerListContainer = centerListContainer + centerListCenterName + ",";
                }
            } else {
                centerListContainer = "";
            }

            divisionListSQL = dbsession.createQuery(" FROM NewsDivision where newsInfo = '" + newsId + "'");
            if (!divisionListSQL.list().isEmpty()) {
                for (Iterator divisionListItr = divisionListSQL.list().iterator(); divisionListItr.hasNext();) {
                    nwd = (NewsDivision) divisionListItr.next();
                    divisionListCenterName = nwd.getMemberDivision().getMemDivisionName();

                    divisionListContainer = divisionListContainer + divisionListCenterName + ",";
                }
            } else {
                divisionListContainer = "";
            }

//            tagListSQL = dbsession.createQuery(" FROM AlbumSlug where pictureCategory = '" + newsId + "'");
//            if (!tagListSQL.list().isEmpty()) {
//                for (Iterator tagListItr = tagListSQL.list().iterator(); tagListItr.hasNext();) {
//                    alt = (AlbumSlug) tagListItr.next();
//                    tagListCenterName = alt.get
//
//                    tagListContainer = tagListContainer + tagListCenterName + ",";
//                }
//            } else {
//                tagListContainer = "";
//            }
            centerDivisionTagContainer = centerListContainer + divisionListContainer;

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("centerDivisionTagContainer", centerDivisionTagContainer);
            json.put("requestId", newsId);
            jsonArr.add(json);

        } else {
            dbtrx.rollback();
            //   strMsg = "Error!!! When Committee add";
            //   response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "0";
            responseMsg = "Error!!! When news assign this center";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("requestId", newsId);
            jsonArr.add(json);
        }

    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>