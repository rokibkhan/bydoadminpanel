

<%@page import="com.appul.entity.FinancialYear"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%> 
<%@page import="com.appul.util.StringToDate"%> 
<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String departmentPgSessionid = request.getParameter("sessionid").trim();

        departmentPgSessionid = session.getId();
        System.out.println(departmentPgSessionid);
    %>
    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Reference List</h4>  <a title="Add Employee" href="#">
                    <a title="Add New Reference" href="<%out.print(GlobalVariable.baseUrl);
                       %>/payroll/billReferenceAdd.jsp?sessionid=<%out.print(departmentPgSessionid);
                       %>">
                        <button class="btn btn-info btn-rounded waves-effect waves-light"><i class="icon-plus"></i><span> &nbsp;Add Reference</span></button>
                    </a>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Reference List</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Angular Js"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                <tr >
                                    <th align="center"><b>Financial Year</b></th>
                                    <th align="center"><b>Start Date</b></th>
                                    <th align="center"><b>End Date</b></th>
                                    <th align="center"><b>Status</b></th>
                                    <th align="center"><b></b>Action</th>
                                </tr>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();
//                                    org.hibernate.Transaction dbtrx = null;
                                    dbtrx = dbsession.beginTransaction();

                                    Query q1 = null;
                                    String billReference = "";
                                    Date startDate = null;
                                    Date endDate = null;
                                    String status = "";
                                    Date billGendate = null;
                                    String billGenid = "";
                                    String genTerm = "";
                                    Date billPostdate = null;
                                    String billPostUserid = "";
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                    String pageNumberS = request.getParameter("pageNumber") == null ? "1" : request.getParameter("pageNumber").trim();
                                    String activeClass = "class=\"active\"";
                                    int pageNumber = Integer.parseInt(pageNumberS);
                                    int pageSize = 10;

                                    int rownum = 0;
                                    String oddeven = null;
                                    FinancialYear financialYear = null;

                                    q1 = dbsession.createQuery("from FinancialYear    order by id");
                                    q1.setFirstResult((pageNumber - 1) * pageSize);
                                    q1.setMaxResults(pageSize);

                                    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                        financialYear = (FinancialYear) itr.next();
                                        billReference = financialYear.getFinancialYear();
                                        startDate = financialYear.getEd();
                                        endDate = financialYear.getTd();
                                        status = financialYear.getStatus();
                                        String Edate = sdf.format(endDate);
                                %>


                                <tr >

                                    <td align="center"> <% out.println(billReference);%></td>
                                    <td align="center"> <% out.println(startDate);%></td>
                                    <td align="center"> <% out.println(Edate);%></td>
                                    <td align="center">  <% if (status.equalsIgnoreCase("O")) {
                                            out.println("Open");
                                        } else if (status.equalsIgnoreCase("G")) {
                                            out.println("Generating");
                                        } else if (status.equalsIgnoreCase("H")) {
                                            out.println("OnHold");
                                        } else if (status.equalsIgnoreCase("P")) {
                                            out.println("Post");
                                        } else if (status.equalsIgnoreCase("C")) {
                                            out.println("Complete");
                                        }
                                        %></td>


                                    <td>
                                        <a title="Edit" href="#"><i class="ti-pencil-alt"></i></a>

                                    </td>
                                </tr>

                                <%                                    }

                                    dbtrx.commit();
                                    dbsession.clear();
                                    dbsession.close();
                                %>
                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <%if (pageNumber == 1) {%>
                            <li class="disabled"><a title="Salary Reference" href="#"><i class="fa fa-angle-left"></i></a> </li> <% } %><% else { %>  <li><a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=<%=pageNumber - 1%>"> <i class="fa fa-angle-left"></i></a> </li> <%}%>

                            <li <%if (pageNumber == 1) {
                                    out.println(activeClass);
                                }%>> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=1">1</a> </li>
                            <li <%if (pageNumber == 2) {
                                    out.println(activeClass);
                                }%>> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=2">2</a> </li>
                            <li <%if (pageNumber == 3) {
                                    out.println(activeClass);
                                }%>> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=3">3</a> </li>
                            <li <%if (pageNumber == 4) {
                                    out.println(activeClass);
                                }%>> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=4">4</a> </li>
                            <li <%if (pageNumber == 5) {
                                    out.println(activeClass);
                                }%>> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=5">5</a> </li>
                                <%if (pageNumber > 5) {%> 
                            <li class="disabled"> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=5">...</a> </li>
                            <li <%if (pageNumber > 5) {
                                    out.println(activeClass);
                                }%>> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=<%=pageNumber%>"><%=pageNumber%></a> </li> <%}%>
                            <li <%if (pageNumber == (pageNumber + 1)) {
                                    out.println(activeClass);
                                }%>> <a title="Salary Reference" href="<%out.print(GlobalVariable.baseUrl);%>/payroll/billReferenceList.jsp?sessionid=<%=departmentPgSessionid%>&pageNumber=<%=pageNumber + 1%>"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>