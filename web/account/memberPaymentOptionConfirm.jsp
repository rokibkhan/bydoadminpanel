<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String sessionIdH = session.getId();
    String userNameH = session.getAttribute("username").toString().toUpperCase();

    Query feeSQL = null;
    Object[] feeObject = null;

    String memberShipFeeTransId = "";
    String memberShipFeeMemberId = "";
    String memberShipFeePaidDate = "";
    String memberShipFeeYear = "";
    String memberShipFeeAmount = "";
    String memberShipFeeRefNo = "";
    String memberShipFeeRefDesc = "";
    String memberShipFeeTransDate = "";
    String memberShipFeeStatus = "";
    String memberShipFeeStatus1 = "";
    String memberShipFeeBillType = "";
    String memberShipFeeCurrency = "BDT";
    String memberPaymentDetailsInfo = "";
    String memberPaymentBtn = "";

    Object memberObj[] = null;
    Query memberSQL = null;

    Query memberFeeUpdateSQL = null;

    String memberId = "";
    String memberIEBId = "";
    String memberName = "";
    String memberFatherName = "";
    String memberMotherName = "";
    String memberBirthPlace = "";
    String memberBirthDate = "";
    String memberBloodGroup = "";
    String memberCountryCode = "";
    String memberGender = "";
    String memberGender1 = "";
    String memberApplyFor = "";
    String memberApplyFor1 = "";
    String memberOldNumber = "";
    String memberEmail = "";
    String memberMobile = "";
    String pictureName = "";
    String pictureLink = "";

    String transationInfoCon = "";
    String mainInfoContainer = "";
    String btnInvInfo = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    String newsName = "";
//memberId: arg1, txnId: arg2, sessionid: arg3
    String memberSelId = request.getParameter("memberId");
    String txnId = request.getParameter("txnId");
    String sessionSelId = request.getParameter("sessionid");
    String paymentRefNo = request.getParameter("paymentRefNo");
    String paymentRefDesc = request.getParameter("paymentRefDesc");
    //paymentRefNo: paymentRefNo, paymentRefDesc:

   
    feeSQL = dbsession.createSQLQuery("select * from member_fee WHERE txn_id= '" + txnId + "'");

    
    if (!feeSQL.list().isEmpty()) {

        //  membershipNo = memberShipType1 + membershipNo;
        memberFeeUpdateSQL = dbsession.createSQLQuery("update member_fee set ref_no='" + paymentRefNo + "',ref_description='" + paymentRefDesc + "',status='1',paid_date=now(),mod_user='" + userNameH + "',mod_date= now() WHERE txn_id=" + txnId + " ");
        memberFeeUpdateSQL.executeUpdate();

        dbtrx.commit();

        if (dbtrx.wasCommitted()) {

            for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                feeObject = (Object[]) feeItr.next();
                memberShipFeeTransId = feeObject[0].toString();
                memberShipFeeMemberId = feeObject[1].toString();
                memberShipFeeTransDate = feeObject[4].toString();
                memberShipFeePaidDate = feeObject[5] == null ? "" : feeObject[5].toString().trim();
                memberShipFeeYear = feeObject[6] == null ? "" : feeObject[6].toString().trim();
                memberShipFeeAmount = feeObject[7].toString();
                
                
                
            memberShipFeeRefNo = feeObject[2] == null ? "" : feeObject[2].toString().trim();
            memberShipFeeRefDesc = feeObject[3] == null ? "" : feeObject[3].toString().trim();
            memberShipFeeStatus = feeObject[8] == null ? "" : feeObject[8].toString().trim();

                if (memberShipFeeStatus.equals("1")) {
                    memberShipFeeStatus1 = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
                } else if (memberShipFeeStatus.equals("0")) {

                    memberShipFeeStatus1 = "<span class=\"btn btn-danger btn-sm waves-effect\">Due</span>";

                } else {
                    memberShipFeeStatus1 = "<span class=\"btn btn-warning btn-sm waves-effect\">Fail</span>";
                }

                memberShipFeeBillType = feeObject[10] == null ? "" : feeObject[10].toString().trim();
            }

            String feeStatus = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";

            String billPrintBtn = "<a onclick=\"printInvoiceInfo('" + memberShipFeeTransId + "');\"  class=\"btn btn-primary btn-sm\">&nbsp; <i class=\"fa fa-file\"></i>&nbsp; </a>";

            String divToPrint = "<div id=\"divToPrint" + memberShipFeeTransId + "\" style=\"display:none;\"><!-- Start:  Print Container  -->"
                    + "<html>"
                    + "<head><title>Bill Invoice Print</title></head>"
                    + "<body>"
                    + "<div style=\"width: 790px; margin: 0px auto; margin-top: 1.5cm;\">"
                    + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                    + "<tr>"
                    + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\"></td>"
                    + "<td style=\"text-align: center;\">"
                    + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span><br>"
                    + "<span style=\"font-size: 20px; font-weight: bold;\">The Institution of Engineers, Bangladesh (IEB)</span>"
                    + "</td>"
                    + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                    + "</tr>"
                    + "</table>"
                    + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                    + "<tr>"
                    + "<td style=\"text-align: center;\">"
                    + "<table>"
                    + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + memberName + "</td></tr>"
                    + "<tr><td style=\"text-align: left;\"><strong>Member ID </strong></td><td>:" + memberIEBId + "</td></tr>"
                    + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + memberMobile + "</td></tr>"
                    + "</table>"
                    + "</td>"
                    + "<td style=\"text-align: center;\">"
                    + "<table>"
                    + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeePaidDate + "</td></tr>"
                    + "<tr><td><strong>Invoice No</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                    + "<tr><td><strong>Status</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                    + "</table>"
                    + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                    + "<thead>"
                    + "<tr>"
                    + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                    + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                    + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody style=\"font-size: 14px; text-align: center;\">"
                    + "<tr>"
                    + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeYear + "</td>"
                    + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeBillType + "</td>"
                    + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                    + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                    + "</tr>"
                    + "</tbody>"
                    + "</table> <!--End : invoice product list details-->"
                    + "<table style=\"width: 95%; border-top:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                    + "<tr>"
                    + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                    + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                    + "Rama,Dhaka-1000,Bangladesh.<br>"
                    + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                    + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</div>"
                    + "</body>"
                    + "</html>"
                    + "</div><!-- End:  Print Container  -->";

            mainInfoContainer = "<td>#</td>"
                    + "<td>" + memberShipFeeTransId + "</td>"
                    + "<td>" + memberShipFeePaidDate + "</td>"
                    + "<td>" + memberShipFeeAmount + "</td>"
                    + "<td>" + feeStatus + "</td>"
                    + "<td>" + paymentRefNo + "</td>"
                    + "<td>" + paymentRefDesc + "</td>"
                    + "<td>" + billPrintBtn + divToPrint + "</td>";

            responseCode = "1";
            responseMsg = "Success!!! Payment Transaction completed successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("mainInfoContainer", mainInfoContainer);
            json.put("btnInvInfo", btnInvInfo);
            json.put("requestId", txnId);
            jsonArr.add(json);

        } else {

            responseCode = "0";
            responseMsg = "Error!!! Not Updated Transaction Info";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("mainInfoContainer", mainInfoContainer);
            json.put("btnInvInfo", btnInvInfo);
            json.put("requestId", txnId);
            jsonArr.add(json);
        }

    } else {

        responseCode = "0";
        responseMsg = "Error!!! No Transaction info found";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("mainInfoContainer", mainInfoContainer);
        json.put("btnInvInfo", btnInvInfo);
        json.put("requestId", txnId);
        jsonArr.add(json);
    }
    out.println(jsonArr);

    dbsession.flush();
    dbsession.close();

%>