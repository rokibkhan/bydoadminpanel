
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">



<script>





</script>
<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    Object memberObj[] = null;
    Query memberSQL = null;
    String memberId = "";
    String memberIEBId = "";
    String memberName = "";
    String memberFatherName = "";
    String memberMotherName = "";
    String memberBirthPlace = "";
    String memberBirthDate = "";
    String memberBloodGroup = "";
    String memberCountryCode = "";
    String memberGender = "";
    String memberGender1 = "";
    String memberApplyFor = "";
    String memberApplyFor1 = "";
    String memberOldNumber = "";
    String memberEmail = "";
    String memberMobile = "";

    String tabProfileActive = "";
    String tabActivityActive = "";
    String tabMessagesActive = "";
    String tabSettingsActive = "";
    String tabRecomendationActive = "";
    String tabDocumentActive = "";
    String tabChangePassActive = "";

    String selUserId = "";
    String selUserName = "";
    String selUserEmail = "";
    String selUserMoble = "";
    String selUserDeptId = "";
    String selUserDept = "";
    String pictureName = "";
    String pictureLink = "";
    String selectedUserId = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

    System.out.println("selectedUserId :: " + selectedUserId);

    if (!selectedUserId.equals("")) {

        System.out.println("selectedUserId IN :: " + selectedUserId);

        memberSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id,  "
                + " m.member_name, m.father_name, m.mother_name, "
                + "m.place_of_birth,m.dob, m.picture_name,m.blood_group, "
                + "m.country_code,m.gender,m.apply_for,m.old_member_id  "
                + "FROM  member m "
                + "WHERE m.id = '" + selectedUserId + "'"
                + "ORDER BY m.id DESC");

        if (!memberSQL.list().isEmpty()) {
            for (Iterator it1 = memberSQL.list().iterator(); it1.hasNext();) {

                memberObj = (Object[]) it1.next();
                memberId = memberObj[0].toString().trim();
                memberIEBId = memberObj[1] == null ? "" : memberObj[1].toString().trim();
                memberName = memberObj[2] == null ? "" : memberObj[2].toString().trim();
                memberFatherName = memberObj[3] == null ? "" : memberObj[3].toString().trim();
                memberMotherName = memberObj[4] == null ? "" : memberObj[4].toString().trim();
                memberBirthPlace = memberObj[5] == null ? "" : memberObj[5].toString().trim();
                memberBirthDate = memberObj[6] == null ? "" : memberObj[6].toString().trim();
                memberBloodGroup = memberObj[8] == null ? "" : memberObj[8].toString().trim();
                memberCountryCode = memberObj[9] == null ? "" : memberObj[9].toString().trim();
                memberGender = memberObj[10] == null ? "" : memberObj[10].toString().trim();

                if (memberGender.equals("M")) {
                    memberGender1 = "Male";
                } else {
                    memberGender1 = "Female";
                }

                memberApplyFor = memberObj[11] == null ? "" : memberObj[11].toString().trim();
                if (memberApplyFor.equals("1")) {
                    memberApplyFor1 = "Fellow";
                }
                if (memberApplyFor.equals("2")) {
                    memberApplyFor1 = "Member";
                }
                if (memberApplyFor.equals("3")) {
                    memberApplyFor1 = "Associate Member";
                }

                memberOldNumber = memberObj[11] == null ? "" : memberObj[11].toString().trim();

                pictureName = memberObj[7] == null ? "" : memberObj[7].toString().trim();

                pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

            }
        }

//        dbsession.flush();
//        dbsession.close();
        String selectedTab = request.getParameter("selectedTab") == null ? "" : request.getParameter("selectedTab").trim();
        if (!selectedTab.equals("")) {

            if (selectedTab.equals("profile")) {
                tabProfileActive = "active";
            } else if (selectedTab.equals("activity")) {
                tabActivityActive = "active";
            } else if (selectedTab.equals("messages")) {
                tabMessagesActive = "active";
            } else if (selectedTab.equals("settings")) {
                tabSettingsActive = "active";
            } else if (selectedTab.equals("recomendation")) {
                tabRecomendationActive = "active";
            } else if (selectedTab.equals("document")) {
                tabDocumentActive = "active";
            } else if (selectedTab.equals("changePass")) {
                tabChangePassActive = "active";
            }

        } else {
            tabProfileActive = "active";
        }
    } else {

        System.out.println("selectedUserId OUt ::");

        //  response.sendRedirect(GlobalVariable.baseUrl + "/home.jsp");
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><%out.print(selUserId);%>Member Details</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Member Details</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">

                <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <ul class="nav customtab nav-tabs" role="tablist">                            
                            <li role="presentation" class="nav-item"><a href="#profile" class="nav-link <% out.print(tabProfileActive); %>" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Personal Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#activity" class="nav-link <% out.print(tabActivityActive); %>" aria-controls="activity" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Education Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#messages" class="nav-link <% out.print(tabMessagesActive); %>" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Professional Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#settings" class="nav-link <% out.print(tabSettingsActive); %>" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Payment Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#recomendation" class="nav-link <% out.print(tabRecomendationActive); %>" aria-controls="recomendation" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Recommendation Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#changePass" class="nav-link <% out.print(tabChangePassActive); %>" aria-controls="changePass" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Change Password</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <% out.print(tabProfileActive); %>" id="profile">
                                <h4 class="font-bold1 m-t-30">Personal Information</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-5">
                                        <form class="form-horizontal form-material1" id="updateProfileFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Full Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserId" name="syUserId" value="<%out.print(memberName);%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Father Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserId" name="syUserId" value="<%=memberFatherName%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Mother Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserName" name="syUserName" value="<%out.print(memberMotherName);%>" placeholder="Full Name" class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Gender</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserName" name="syUserName" value="<%=memberGender1%>"  class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>



                                        </form> 
                                    </div>
                                    <div class="col-md-5">
                                        <form class="form-horizontal form-material1" id="updateProfileFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Date of Birth</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserId" name="syUserId" value="<%=memberBirthDate%>"  class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Blood Group</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserId" name="syUserId" value="<%=memberBloodGroup%>" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Place of Birth</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserName" name="syUserName" value="<%=memberBirthPlace%>" class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Country</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="syUserName" name="syUserName" value="<%=memberCountryCode%>" class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>



                                        </form> 
                                    </div>
                                    <div class="col-md-2">
                                        <%=pictureLink%>
                                    </div>
                                </div>

                                <h4 class="font-bold1 m-t-30">Address Info</h4>
                                <hr>
                                <%
                                    Query madrSQL = null;
                                    Object[] madrObject = null;

                                    Query padrSQL = null;
                                    Object[] padrObject = null;

                                    int mAddressId = 0;
                                    String mCountryId = "";
                                    String mDistrictId = "";
                                    String mThanaId = "";
                                    String mThanaName = "";
                                    String mThanaDistrictName = "";
                                    String mAddressLine1 = "";
                                    String mAddressLine2 = "";
                                    String mZipOffice = "";
                                    String mZipCode = "";

                                    int pAddressId = 0;
                                    String pCountryId = "";
                                    String pDistrictId = "";
                                    String pThanaId = "";
                                    String pThanaName = "";
                                    String pThanaDistrictName = "";
                                    String pAddressLine1 = "";
                                    String pAddressLine2 = "";
                                    String pZipOffice = "";
                                    String pZipCode = "";

                                    String mAddressStr = "";
                                    String pAddressStr = "";

                                    Thana thana = null;

                                    Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address where member_id= " + selectedUserId + " and address_Type='M' ) ");
                                    for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
                                        madrObject = (Object[]) itr2.next();
                                        mAddressLine1 = madrObject[1].toString();
                                        mAddressLine2 = madrObject[2].toString();
                                        //   mThanaId = madrObject[3].toString();
                                        mThanaId = madrObject[3] == null ? "" : madrObject[3].toString().trim();
                                        System.out.println("mThanaId:: " + mThanaId);

                                        if (!mThanaId.equals("")) {
                                            Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                            for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
                                                thana = (Thana) itrmThana.next();

                                                mThanaName = thana.getThanaName();
                                                mThanaDistrictName = thana.getDistrict().getDistrictName();
                                            }
                                        } else {
                                            mThanaName = "";
                                            mThanaDistrictName = "";
                                        }

                                        mZipCode = madrObject[4] == null ? "" : madrObject[4].toString().trim();
                                        mZipOffice = madrObject[8] == null ? "" : madrObject[8].toString().trim();

                                        mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

                                    }
                                    Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address where member_id= " + selectedUserId + " and address_Type='P' ) ");
                                    for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

                                        padrObject = (Object[]) itr3.next();
                                        pAddressLine1 = padrObject[1].toString();
                                        pAddressLine2 = padrObject[2].toString();
                                        //  pThanaId = padrObject[3].toString();
                                        pThanaId = padrObject[3] == null ? "" : padrObject[3].toString().trim();
                                        if (!pThanaId.equals("")) {
                                            Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                            for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
                                                thana = (Thana) itrpThana.next();

                                                pThanaName = thana.getThanaName();
                                                pThanaDistrictName = thana.getDistrict().getDistrictName();
                                            }
                                        } else {
                                            pThanaName = "";
                                            pThanaDistrictName = "";
                                        }

                                        pZipCode = padrObject[4] == null ? "" : padrObject[4].toString().trim();
                                        pZipOffice = padrObject[8] == null ? "" : padrObject[8].toString().trim();

                                        pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

                                    }

                                %>
                                <div class="row">                                      
                                    <div class="col-md-4">
                                        <p class="m-2 text-center"><strong>Mailing Address</strong></p>
                                        <address class="m-2 text-center">
                                            <%=mAddressStr%>
                                        </address>
                                    </div>   
                                    <div class="col-md-4">
                                        <p class="m-2 text-center"><strong>Permanent Address</strong></p>
                                        <address class="m-2 text-center">
                                            <%=pAddressStr%>
                                        </address>

                                    </div>   
                                </div>



                                <h4 class="font-bold1 m-t-30">Documents Info</h4>
                                <hr>

                                <div class="row">
                                    <%                                                Query relDocSQL = null;
                                        Object[] relDocbject = null;

                                        String documentId = "";
                                        String documentName = "";
                                        String documentName1 = "";
                                        String documentLink = "";

                                        relDocSQL = dbsession.createSQLQuery("SELECT * FROM member_document_info WHERE member_id = '" + selectedUserId + "'");
                                        for (Iterator relDocItr = relDocSQL.list().iterator();
                                                relDocItr.hasNext();) {
                                            relDocbject = (Object[]) relDocItr.next();

                                            documentId = relDocbject[2].toString();
                                            documentName = relDocbject[3].toString();
                                            documentName1 = relDocbject[4].toString();

                                            documentLink = GlobalVariable.imageDirLink + "document/" + documentName1;

                                        }

                                    %>
                                    <div class="col-10 offset-1">
                                        <div class="form-row form-group">
                                            <label class="form-control-label col-3 text-right" for="inputName" style="">Required Document : </label>

                                            <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                            <div class="col-8">
                                                <a href="<%=documentLink%>"><%=documentName%></a>
                                            </div>

                                        </div> 


                                    </div>
                                </div>






                                <div class="row">
                                    <div class="col-md-6 offset-md-4" style="padding-left: 25px;">








                                        <a href="<% out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=session.getId()%>" class="btn btn-inverse waves-effect waves-light m-r-10"> <i class="fa fa-long-arrow-left"></i> Back</a>

                                    </div>
                                </div>

                            </div>
                            <!-- /#profile -->
                            <div class="tab-pane <% out.print(tabActivityActive); %>" id="activity">

                                <h4 class="font-bold1 m-t-30">Education Info</h4>
                                <hr>
                                <div class="row">                                      
                                    <div class="col-md-12">

                                        <%
                                            Query eduTempSQL = null;
                                            //    Query q2 = null;
                                            int degreeId = 0;
                                            String degreeName = "";
                                            String instituteName = "";
                                            String boardUniversityName = "";
                                            String yearOfPassing = "";
                                            String resultTypeName = "";
                                            String result = "";

                                            MemberEducationInfo mei = null;

                                            eduTempSQL = dbsession.createQuery("from MemberEducationInfo where  member_id=" + selectedUserId + " ");

                                            MemberTemp member = null;
                                            String memberSubdivisionName = "";
                                            String memberSubdivisionFullName = "";
                                            int educationfInfoId = 0;

                                        %>


                                        <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Degree</th>
                                                    <th scope="col">Board/University</th>
                                                    <th scope="col">Year of Passing</th>
                                                    <th scope="col">Score/Class</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%                                                    for (Iterator eduTempItr = eduTempSQL.list().iterator(); eduTempItr.hasNext();) {
                                                        mei = (MemberEducationInfo) eduTempItr.next();

                                                        educationfInfoId = mei.getId();
                                                        degreeId = mei.getDegree().getDegreeId();
                                                        degreeName = mei.getDegree().getDegreeName();

                                                        instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                        boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                        yearOfPassing = mei.getYearOfPassing();
                                                        resultTypeName = mei.getResultType().getResultTypeName();
                                                        result = mei.getResult();

                                                        //                                    if (degreeId == 3 || degreeId == 4) {
                                                        //
                                                        //                                        q2 = dbsession.createQuery("from MemberTemp as member WHERE id=" + registerAppId + " ");
                                                        //
                                                        //                                        for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                                        //                                            member = (MemberTemp) itr2.next();
                                                        //
                                                        //                                         //   memberSubdivisionName = member.getSubDivision().getSubDivisionName();
                                                        //                                         //   memberSubdivisionFullName = member.getSubDivision().getFullName();
                                                        //                                        // memberSubdivisionName = member.getS
                                                        //                                        }
                                                        //                                    } else {
                                                        memberSubdivisionFullName = "";
                                                        //                                   }

                                                        degreeName = degreeName + memberSubdivisionFullName;
                                                %>
                                                <tr>
                                                    <td><%=degreeName%></td>
                                                    <td><%=boardUniversityName%></td>
                                                    <td><%=yearOfPassing%></td>
                                                    <td><%=result%></td>

                                                </tr>
                                                <%
                                                    }


                                                %>

                                            </tbody>
                                        </table>

                                        <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>                                                
                                                    <th scope="col">Subject</th>
                                                    <th scope="col">University</th>
                                                </tr>
                                                <%                                                    Object[] subjectObject = null;
                                                    String subjectName = "";
                                                    String subjectUniversityName = "";

                                                    Query subjectSQL = dbsession.createSQLQuery("SELECT usb.subject_long_name,u.university_long_name FROM "
                                                            + "member_subject_info msb,university_subject usb,university u "
                                                            + "WHERE msb.subject_id = usb.subject_id "
                                                            + "AND usb.university_id = u.university_id "
                                                            + "AND msb.member_id='" + selectedUserId + "'");
                                                    if (!subjectSQL.list().isEmpty()) {
                                                        for (Iterator subjectItr = subjectSQL.list().iterator(); subjectItr.hasNext();) {
                                                            subjectObject = (Object[]) subjectItr.next();
                                                            subjectName = subjectObject[0].toString();
                                                            subjectUniversityName = subjectObject[1].toString();
                                                %>

                                                <tr>
                                                    <td><%=subjectName%></td>
                                                    <td><%=subjectUniversityName%></td>
                                                </tr>

                                                <%
                                                        }
                                                    }
                                                %>

                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                            <!-- /#profile -->
                            <div class="tab-pane <% out.print(tabMessagesActive); %>" id="messages">

                                <h4 class="font-bold1 m-t-30">Professional Information</h4>
                                <hr>
                                <div class="row">                                      
                                    <div class="col-md-12">
                                        <%
                                            Query profTempSQL = null;
                                            //    Query q2 = null;
                                            int profTempId = 0;
                                            String organizationName = "";
                                            String designationName = "";
                                            String startDateProTemp = "";
                                            String endDateProTemp = "";

                                            MemberProfessionalInfo profTemp = null;

                                            profTempSQL = dbsession.createQuery("from MemberProfessionalInfo where  member_id=" + selectedUserId + " ");


                                        %>
                                        <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Organization</th>
                                                    <th scope="col">Designation</th>
                                                    <th scope="col">Start Date</th>
                                                    <th scope="col">End Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%                                                for (Iterator profTempItr = profTempSQL.list().iterator(); profTempItr.hasNext();) {
                                                        profTemp = (MemberProfessionalInfo) profTempItr.next();

                                                        organizationName = profTemp.getCompanyName().toString();
                                                        designationName = profTemp.getDesignation().toString();
                                                        startDateProTemp = profTemp.getFromDate().toString();
                                                        endDateProTemp = profTemp.getTillDate().toString();


                                                %>
                                                <tr>
                                                    <td><%=organizationName%></td>
                                                    <td><%=designationName%></td>
                                                    <td><%=startDateProTemp%></td>
                                                    <td><%=endDateProTemp%></td>

                                                </tr>
                                                <%
                                                    }

                                                %>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                            <!-- /#messages-->
                            <div class="tab-pane <% out.print(tabSettingsActive); %>" id="settings">

                                <h4 class="font-bold1 m-t-30">Payment Information</h4>
                                <hr>
                                <%
                                    //   MemberFeeTemp mpi = null;

                                    Query feeSQL = null;
                                    Object[] feeObject = null;

                                    String memberShipFeeTransId = "";
                                    String memberShipFeeMemberId = "";
                                    String memberShipFeePaidDate = "";
                                    String memberShipFeeAmount = "";
                                    String memberShipFeeRefNo = "";
                                    String memberShipFeeRefDesc = "";
                                    String memberShipFeeStatus = "";
                                    String memberShipFeeStatus1 = "";
                                    String memberShipFeeCurrency = "BDT";

                                    // feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
                                    feeSQL = dbsession.createSQLQuery("select * from member_fee WHERE member_id= '" + selectedUserId + "'");


                                %>
                                <div class="row">
                                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Txn No.</th>
                                                <th scope="col">Payment Date</th>
                                                <th scope="col">Amount</th>
                                                <th scope="col">Status</th> 
                                                <th scope="col">Payment Info</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%                                                if (!feeSQL.list().isEmpty()) {
                                                    int ip = 1;
                                                    for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                                                        feeObject = (Object[]) feeItr.next();
                                                        memberShipFeeTransId = feeObject[0].toString();
                                                        memberShipFeeMemberId = feeObject[1].toString();
                                                        memberShipFeePaidDate = feeObject[5].toString();
                                                        memberShipFeeAmount = feeObject[7].toString();
                                                        memberShipFeeRefNo = feeObject[2].toString();
                                                        memberShipFeeRefDesc = feeObject[3].toString();
                                                        memberShipFeeStatus = feeObject[8].toString();

                                                        System.out.println("memberShipFeeStatus :: " + memberShipFeeStatus);

                                                        if (memberShipFeeStatus.equals("1")) {
                                                            memberShipFeeStatus1 = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
                                                        } else {
                                                            memberShipFeeStatus1 = "<span class=\"btn btn-danger btn-sm waves-effect\">Fail</span>";
                                                            // payOnline = "<a title=\"Pay Online\" href=\"" + GlobalVariable.baseUrl + "/member/memberPaymentProcessing.jsp?sessionid=" + session.getId() + "&act=add&&memberFeeId=" + memberFeeId + "\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-credit-card\"></i></a>";
                                                        }

                                                        //    System.out.println("memberShipFeeTransId :: " + memberShipFeeTransId);
                                                        //   System.out.println("memberShipFeeMemberId :: " + memberShipFeeMemberId);
                                                        //   System.out.println("memberShipFeePaidDate :: " + memberShipFeePaidDate);
                                                        //  System.out.println("memberShipFeeAmount :: " + memberShipFeeAmount);

                                            %>
                                            <tr>
                                                <td><%=ip%></td>
                                                <td><%=memberShipFeeTransId%></td>
                                                <td><%=memberShipFeePaidDate%></td>
                                                <td><%=memberShipFeeAmount%></td> 
                                                <td><%=memberShipFeeStatus1%></td> 
                                                <td><%=memberShipFeeRefDesc%></td> 

                                            </tr>

                                            <%
                                                        ip++;
                                                    }
                                                }
                                            %>
                                        </tbody>
                                    </table>

                                </div>   
                                <!-- .Payment Information-->

                            </div>
                            <!-- /#settings -->   
                            <div class="tab-pane <% out.print(tabRecomendationActive); %>" id="recomendation">

                                <h4 class="font-bold1 m-t-30">Recommendation Info</h4>
                                <hr>

                                <%                                    Object[] objProposer = null;
                                    String reqMemProposerId = "";
                                    String reqMemProposerMemberId = "";
                                    String reqMemProposerName = "";
                                    String reqMemProposerApproveStatus = "";
                                    String reqMemProposerApproveStatusText = "";
                                    String reqMemProposerRequestDate = "";
                                    String reqMemProposerAppRejDate = "";
                                    int pr = 1;
                                    String proposerContainerBox = "";

                                    Query proposerSQL = dbsession.createSQLQuery("SELECT mp.id,m.member_id, m.member_name, "
                                            + "mp.status, mp.request_date, mp.app_rej_date  "
                                            + "FROM  member_proposer_info mp,member m  "
                                            + "WHERE mp.member_id = '" + selectedUserId + "' AND mp.member_id = m.id AND mp.proposer_id=m.id "
                                            + "ORDER BY mp.id DESC");

                                    if (!proposerSQL.list().isEmpty()) {
                                        for (Iterator itPro = proposerSQL.list().iterator(); itPro.hasNext();) {

                                            objProposer = (Object[]) itPro.next();
                                            reqMemProposerId = objProposer[0].toString().trim();
                                            reqMemProposerMemberId = objProposer[1].toString().trim();
                                            reqMemProposerName = objProposer[2].toString().trim();
                                            reqMemProposerApproveStatus = objProposer[3].toString().trim();
                                            if (reqMemProposerApproveStatus.equals("0")) {
                                                // reqMemProposerApproveStatusText = "Wating for Approval";
                                                reqMemProposerApproveStatusText = "<button class=\"btn btn-warning waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-link\"></i></span>Wating for Approval</button>";
                                            }
                                            if (reqMemProposerApproveStatus.equals("1")) {
                                                //reqMemProposerApproveStatusText = "Approved";
                                                reqMemProposerApproveStatusText = "<button class=\"btn btn-success waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-check\"></i></span>Approved</button>";
                                            }
                                            if (reqMemProposerApproveStatus.equals("2")) {
                                                // reqMemProposerApproveStatusText = "Decline";
                                                reqMemProposerApproveStatusText = "<button class=\"btn btn-success waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-times\"></i></span>Decline</button>";
                                            }
                                            reqMemProposerRequestDate = objProposer[4].toString().trim();
                                            reqMemProposerAppRejDate = objProposer[5] == null ? "" : objProposer[5].toString().trim();

                                            proposerContainerBox = proposerContainerBox + ""
                                                    + "<div class=\"col-md-4\">"
                                                    + "<h5 class=\"font-bold1 text-center\">Proposer 1</h5>"
                                                    + "<form class=\"form-horizontal form-material1\">"
                                                    + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                                    + "<div class=\"col-sm-12\">"
                                                    + "<input type=\"text\" value=\"" + reqMemProposerName + "\"  class=\"form-control input-sm\" disabled>"
                                                    + "</div>"
                                                    + "</div>"
                                                    + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                                    + "<div class=\"col-sm-12\">"
                                                    + "<input type=\"text\" value=\"" + reqMemProposerMemberId + "\"  class=\"form-control input-sm\" disabled>"
                                                    + "</div>"
                                                    + "</div>"
                                                    + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                                    + "<div class=\"col-sm-12\">"
                                                    + "" + reqMemProposerApproveStatusText + ""
                                                    + "</div>"
                                                    + "</div>"
                                                    + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                                    + "<div class=\"col-sm-12\">"
                                                    + "<input type=\"text\" value=\"" + reqMemProposerRequestDate + "\"  class=\"form-control input-sm\" disabled>"
                                                    + "</div>"
                                                    + "</div>"
                                                    + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                                    + "<div class=\"col-sm-12\">"
                                                    + "<input type=\"text\" value=\"" + reqMemProposerAppRejDate + "\"  class=\"form-control input-sm\" disabled>"
                                                    + "</div>"
                                                    + "</div>"
                                                    + "</form>"
                                                    + "</div>";

                                            pr++;
                                        }
                                    }

                                %>

                                <div class="row">
                                    <%=proposerContainerBox%>                                    
                                </div>

                            </div>
                            <!-- /#recomendation -->

                            <div class="tab-pane <% out.print(tabChangePassActive); %>" id="changePass">



                            </div>
                            <!-- /#changePass -->

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>