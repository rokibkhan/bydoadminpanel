<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">


        function memberFeeYearDeleteInfo(arg1, arg2) {
            console.log("memberFeeYearDeleteInfo :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"memberFeeYearDeleteInfoConfirmBtn\" onclick=\"memberFeeYearDeleteInfoConfirm(" + arg1 + ",'" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');
        }

        function memberFeeYearDeleteInfoConfirm(arg1, arg2) {
            console.log("memberFeeYearDeleteInfoConfirm :: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');


            $.post("memberFeeYearDeleteInfoProcess.jsp", {deleteItemId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");
        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Member Fee Year List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Finance & Account</a></li>                    
                    <li class="active">Member Fee Year</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th style="width: 3%;">#</th>
                                    <th class="text-center">Year</th>
                                    <th class="text-center">Showing Order</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String memberFeeYearId = "";
                                    String memberFeeYearName = "";
                                    String memberFeeYearOrder = "";

                                    Object memberFeeYearObj[] = null;
                                    Query memberFeeYearSQL = null;

                                    String memberFeeYearEditUrl = "";
                                    String agrX = "";

                                    memberFeeYearSQL = dbsession.createSQLQuery("SELECT * FROM  member_fees_year ORDER BY ordering DESC");

                                    if (!memberFeeYearSQL.list().isEmpty()) {
                                        for (Iterator it1 = memberFeeYearSQL.list().iterator(); it1.hasNext();) {

                                            memberFeeYearObj = (Object[]) it1.next();
                                            memberFeeYearId = memberFeeYearObj[0].toString().trim();
                                            memberFeeYearName = memberFeeYearObj[1] == null ? "" : memberFeeYearObj[1].toString().trim();
                                            memberFeeYearOrder = memberFeeYearObj[2] == null ? "" : memberFeeYearObj[2].toString().trim();

                                            memberFeeYearEditUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberFeeYearId=" + memberFeeYearId + "";

                                            agrX = "'" + memberFeeYearId + "','" + session.getId() + "'";

                                %>


                                <tr id="infoBox<%=memberFeeYearId%>">
                                    <td><%=ix%></td>
                                    <td class="text-center"><%=memberFeeYearName%></td>
                                    <td class="text-center"><%=memberFeeYearOrder%></td>

                                    <td class="text-center">


                                        <a onClick="memberFeeYearDeleteInfo(<%=agrX%>);" title="Delete Feee Year" class="btn btn-primary btn-sm"><i class="icon-trash"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                %>



                            </tbody>
                        </table>
                    </div>

                    
                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>