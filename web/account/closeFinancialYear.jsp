
<%-- 
    Document   : designationAdd
    Created on : December 20, 2018, 01:04:45 PM
    Author     : Akter & Tahajjat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<% request.setAttribute("TITLE", "New Invoice Generation");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>


<link rel="stylesheet" href="../jquery/jquery-ui.css" />

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"></script>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">

<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
%>

<%
    String read = "read";
    String write = "write";
    dbsession = HibernateUtil.getSessionFactory().openSession();

    dbtrx = dbsession.beginTransaction();
    FinancialYear billReferences = new FinancialYear();
    Map row = null;
    Map row1 = null;
    String billRef = null;
    String status = null;
    String billMsg = null;
    String sql = "select FINANCIAL_YEAR,STATUS,REMARKS from financial_year where   (ED,TD)=(select min(ED),min(TD) from financial_year where   STATUS in('G','H','P','O')) ORDER BY ED,TD";
    SQLQuery query = dbsession.createSQLQuery(sql);

    Object objBillRef[] = new Object[5];
    for (Iterator itrBillRef = query.list().iterator(); itrBillRef.hasNext();) {
        objBillRef = (Object[]) itrBillRef.next();
        billRef = objBillRef[0].toString();
        status = objBillRef[1].toString();
        billMsg = objBillRef[2] == null ? "" : objBillRef[2].toString().trim();
    }

    /**/
    query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
    List data = query.list();
    for (Object object : data) {
        row = (Map) object;

    }

%>
<!-- Page Content -->
<div id="page-wrapper"> 
    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Close Financial Year</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Account</a></li>
                    <li class="active">Close Financial Year</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>

        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-12"> 

                            <form class="form-horizontal" name="salaryAdd" id="salaryAdd" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/account/closedFinancialYearRef.jsp?sessionid=<%out.print(sessionid);%>&billReference=<%=billRef%>" >

                                <%
                                    if (status != null) {
                                        if (status.equalsIgnoreCase("g")) {
                                            out.println("<font color='green' size='3'>Financial Year (" + billRef + ") Process Ongoing</font>");
                                        } else if (status.equalsIgnoreCase("p")) {
                                            out.println("<font color='green' size='3'>Financial Year (" + billRef + ") Posting Ongoing</font>");
                                        } else if (status.equalsIgnoreCase("h")) {
                                            out.println("<font color='green' size='3'>Financial Year (" + billRef + ") Waiting For Posting Do You Want to Re-Process</font>");
                                %>



                                <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                    <thead>
                                        <tr  >

                                            <th align="center"> <b>Bill Reference</b></th>
                                            <th align="center" <b>Status</b></th>

                                            <th align="center" <b>Bill Message</b></th>
                                        </tr>
                                    </thead>
                                    <tr >
                                        <td align="center"><% out.println(billRef);%></td>
                                        <td align="center"><% if (status.equalsIgnoreCase("h")) {
                                                out.println("On Hold");
                                            }
                                            %></td>

                                        <td align="center"><% out.println(billMsg);%></td>


                                    </tr>    
                                    <tr>
                                        <td colspan="2" align="right">
                                            <input type="submit" value="Yes" name="Yes">

                                        </td>
                                        <td  align="right">

                                            <input type="button" value="No" name="No" onclick=" location.href = 'hotBillGenerationRef.jsp?sessionid=<%=sessionid%>'">
                                        </td>

                                    </tr>

                                </table>
                                <%
                                } else if (status.equalsIgnoreCase("O")) {
                                    String sq = "select REMARKS from financial_year where TD=(select min(TD) from financial_year where STATUS in('O'))";
                                    SQLQuery query1 = dbsession.createSQLQuery(sq);
                                    query1.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
                                    List data1 = query1.list();
                                    for (Object object : data1) {
                                        row1 = (Map) object;
                                    }

                                    if (row1.get("REMARKS") == null) {
                                        out.println("<font color='green' size='3'>Financial Year (" + billRef + ") is open and you have no Payroll message,add  message or Run Payroll</font>");
                                %>
                                <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                    <thead>
                                        <tr >

                                            <th align="center" >Financial Year</th>
                                            <th align="center" >Status</th>

                                            <th align="center" >Payroll Message</th>
                                        </tr>
                                    </thead>

                                    <tr >
                                        <td align="center"><% out.println(billRef);%></td>
                                        <td align="center"><% if (status.equalsIgnoreCase("o")) {
                                                out.println("Open");
                                            }
                                            %></td>

                                        <td align="center"><% out.println(billMsg);%></td>

                                    </tr>    
                                    <tr>
                                        <td colspan="2" align="right">
                                            <input type="button"  value="Add Message" name="Add Message" onclick=" location.href = 'billReferenceEdit.jsp?sessionid=<%=sessionid%>&read=<%=read%>&write=<%=write%>&billReference=<%=billRef%>'">

                                        </td>
                                        <td  align="right">

                                            <input type="submit" value="Run Payroll" name="RunPayroll">
                                        </td>
                                    </tr>


                                </table>





                                <%} else {
                                    out.println("<font color='green' size='3'>Financial Year (" + billRef + ") is open to Process</font>");
                                %> 

                                <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                    <thead>
                                        <tr  >

                                            <th align="center" <b>Financial Year</b></th>
                                            <th align="center" <b>Status</b></th>

                                            <th align="center" <b>Message</b></th>
                                        </tr>
                                    </thead>
                                    <tr >
                                        <td align="center"><% out.println(billRef);%></td>
                                        <td align="center"><% if (status.equalsIgnoreCase("o")) {
                                                out.println("Open");
                                            }
                                            %></td>

                                        <td align="center"><% out.println(billMsg);%></td>


                                    </tr>    
                                    <tr>
                                        <td colspan="2" align="right">

                                            <input type="button" value="Cencel" name="Cencel" onclick="location.href = 'blank.jsp?sessionid=<%=sessionid%>'">
                                        </td>
                                        <td  align="right">
                                            <input type="submit" value="Process" name="RunPayroll">

                                        </td>
                                    </tr>

                                </table>


                                <%
                                            }

                                        }
                                    } else {
                                        out.println("<font color='green' size='3'>Please Insert Financial Year </font>");
                                    }

                                %>
                            </form>
                            <%            dbtrx.commit();
                                dbsession.close();
                            %>


                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>

        </div>
        <!-- /.row -->
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);
            %>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>