<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

        int ix = 1;

        // String targetpage = "memberListFellow.jsp";
        String targetpageWithSession = "memberFineCheck.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        int limit = 100;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();

        //  System.out.println("filterSQLStr :: " + filterSQLStr);
        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
        int pageNbr = Integer.parseInt(pageNbr1);

        //  System.out.println("filter :: " + filter);
        //  System.out.println("pageNbr :: " + pageNbr);
        String memberId = "";
        String memberIEBId = "";
        String memberName = "";
        String memberPictureName = "";
        String memberPictureUrl = "";
        String memberPicture = "";
        String memberMobile = "";
        String memberEmail = "";
        String memberType = "";

        String mOthersInfo = "";
        String memberUniversityName = "";
        String memberDivisionName = "";
        String memberCenterName = "";
        String agrX = "";

        String memberDetailsUrl = "";
        String memberPaymentInfoUrl = "";

        String txn_id = "";
        String ref_no = "";
        String ref_description = "";
        String txn_date = "";

        String paid_date = "";
        String fee_year = "";
        String feeYearText = "";
        String amount = "";
        String transactionStatus = "";
        String transactionStatusText = "";
        String due_date = "";
        String bill_type = "";
        String billTypeText = "";
        String pay_option = "";

        Query feeSQL = null;
        Object[] feeObject = null;

        String memberShipFeeTransId = "";
        String memberShipFeeTxnDate = "";
        String memberShipFeeAmount = "";
        String memberShipFeeCurrency = "BDT";
        String memberShipFeeBillType = "";
        String memberShipFeeBillYear = "";
        String transactionContainer = "";
        String transactionContainerFull = "";

        double totalDueAmount = 0.0d;
        String fineAmount = "";
        double totalDueWithFineAmount = 0.0d;
    %>

    <script type="text/javascript">

        function showMemebrSearchSubCenterInfo(arg1) {


            console.log("showMemebrSearchSubCenterInfo arg1:: " + arg1);
            var arg2 = "";

            $.post("memberCenterWiseSubCenterInfoShow.jsp", {centerId: arg1, addressInfo: arg2}, function (data) {

                if (data[0].responseCode == 1) {
                    $("#showSubCenterInfo").html(data[0].subCenterInfo);
                }

            }, "json");
        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Fine Check List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Account</a></li>                    
                    <li class="active">Fine</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">                    


                </div>
                <!-- /.white-box -->
            </div>

            <div class="col-md-12">
                <div class="white-box">

                    <ul>
                        <li><a href="#">Remove Previous Year Fine Data</a></li>
                        <li><a href="#">Insert Current Year Fine Data</a></li>
                        <li><a href="#">Checking Data</a></li>
                        <li><a href="#">Show Fine Data</a></li>
                        <li><a href="#">Process Fine Transaction</a></li>
                    </ul>



                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>


    <%@ include file="../footer.jsp" %>