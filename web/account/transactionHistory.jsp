<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

        int ix = 1;

        // String targetpage = "memberListFellow.jsp";
        String targetpageWithSession = "transactionHistory.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        String periodp = "";
        int limit = 100;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        String period = request.getParameter("period") == null ? "Today" : request.getParameter("period").trim();

        //  out.print("<h2 align=\"center\">period :: " + period + "</h2>");
        periodp = "period=" + period + "&";

        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();

        //  System.out.println("filterSQLStr :: " + filterSQLStr);
        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
        int pageNbr = Integer.parseInt(pageNbr1);

        //  System.out.println("filter :: " + filter);
        //  System.out.println("pageNbr :: " + pageNbr);
        String memberId = "";
        String memberIEBId = "";
        String memberName = "";
        String memberPictureName = "";
        String memberPictureUrl = "";
        String memberPicture = "";
        String memberMobile = "";
        String memberEmail = "";
        String memberAddress = "";

        String mOthersInfo = "";
        String memberUniversityName = "";
        String memberDivisionName = "";
        String memberCenterName = "";
        String agrX = "";

        String memberDetailsUrl = "";

        String txn_id = "";
        String ref_no = "";
        String ref_description = "";
        String txn_date = "";

        String paid_date = "";
        String fee_year = "";
        String feeYearText = "";
        String amount = "";
        String transactionStatus = "";
        String transactionStatusText = "";
        String due_date = "";
        String bill_type = "";
        String billTypeText = "";
        String pay_option = "";

        String Today = "";
        String Yesterday = "";
        String ThisWeek = "";
        String LastWeek = "";
        String ThisMonth = "";
        String LastMonth = "";
        String ThisYear = "";
        String LastYear = "";
        String Custom = "";

        String moduleType = "All";
        String ticketStatusType = "All";
        //     String period = "Today";
        //  String period = "Yesterday";
        String fromDate = "";
        String toDate = "";

        String customDateBox = "style=\"display: none;\"";
        String offsetAddNocustomDateBox = "offset-md-5";
        String fromCustomDate = "";
        String toCustomDate = "";

        DateFormat dFTodayHms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dFToday = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dFY = new SimpleDateFormat("Y");
        DateFormat dateFormatEventM = new SimpleDateFormat("MMM");
        DateFormat dateFormatEventD = new SimpleDateFormat("dd");
        DateFormat dateFormatEventY = new SimpleDateFormat("Y");

        DateFormat justDay = new SimpleDateFormat("yyyyMMdd");
        Date thisMorningMidnight = justDay.parse(justDay.format(new Date()));
        Date dNow = new Date();
        Date dNowYesterday = new Date(thisMorningMidnight.getTime() - 24 * 60 * 60 * 1000);

        // start of the next week
        //  cal.add(Calendar.WEEK_OF_YEAR, 1);
        //    out.print("<h2 align=\"center\">Start of the next week:   " + cal.getTime() + "</h2>");
        //    out.print("<h2 align=\"center\">yesterday1 :: " + dFToday.format(dNowYesterday) + "</h2>");
        //   SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        //    out.print("<h2 align=\"center\">" + ft.format(dNow) + "</h2>");
        //   out.print("<h2 align=\"center\">" + dFToday.format(dNow) + "</h2>");
        //   out.print("<h2 align=\"center\">" + dFTodayHms.format(dNow) + "</h2>");
        //   out.print("<h2 align=\"center\">" + dFTodayHms.format(dNow) + "</h2>");
//Today
        //String fromDate = date("Y-m-d")." "."00:00:00";
        //String toDate = date("Y-m-d")." "."23:59:59";
        //Today
        //   String fromDate = dFToday.format(dNow) + " 00:00:00";
        //    String toDate = dFToday.format(dNow) + " 23:59:59";
        //    out.print("<h2 align=\"center\">fromDate :: " + fromDate + "</h2>");
        //    out.print("<h2 align=\"center\">toDate :: " + toDate + "</h2>");
//// Get calendar set to current date and time
//Calendar c = GregorianCalendar.getInstance();
//
//out.print("<h2 align=\"center\">Current week = " + Calendar.DAY_OF_WEEK + "</h2>");
//
//// Set the calendar to monday of the current week
//c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
//out.print("<h2 align=\"center\">Current week = " + Calendar.DAY_OF_WEEK + "</h2>");
//
//// Print dates of the current week starting on Monday
//DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//String startDate = "", endDate = "";
//
//startDate = df.format(c.getTime());
//c.add(Calendar.DATE, 6);
//endDate = df.format(c.getTime());
//
//out.print("<h2 align=\"center\">Start Date = " + startDate + "</h2>");
//out.print("<h2 align=\"center\">End Date = " + endDate + "</h2>");
    %>

    <script type="text/javascript">

        function showMemebrSearchSubCenterInfo(arg1) {


            console.log("showMemebrSearchSubCenterInfo arg1:: " + arg1);
            var arg2 = "";

            $.post("memberCenterWiseSubCenterInfoShow.jsp", {centerId: arg1, addressInfo: arg2}, function (data) {

                if (data[0].responseCode == 1) {
                    $("#showSubCenterInfo").html(data[0].subCenterInfo);
                }

            }, "json");
        }



        function changeCustomPeriodField(value) {
            var dataval = value;
            //	  alert(dataval);
            if (dataval == "Custom") {
                $('#frmInfo').show();
                $('#toInfo').show();
                $("#offsetAddNocustomDateBoxXn").removeClass("offset-md-5");
            } else {
                $('#frmInfo').hide();
                $('#toInfo').hide();
                $("#offsetAddNocustomDateBoxXn").addClass("offset-md-5");
            }
        }


    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Search Transaction</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Account</a></li>                    
                    <li class="active">Search Transaction</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">
                    <%
                        String searchStartDate = "";
                        String searchEndDate = "";

                        String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();
                        String searchQuery = "";
                        if (!searchString.equals("")) {
                            // searchQuery = "WHERE member_name like '%" + searchString + "%' OR email_id like '%" + searchString + "%' OR member_id like '%" + searchString + "%' or m.mobile like '%" + searchString + "%'";

                            //  searchQuery = "WHERE mf.txn_id like '%" + searchString + "%' OR m.member_id like '%" + searchString + "%' OR m.member_name like '%" + searchString + "%' or m.mobile like '%" + searchString + "%' OR m.email_id like '%" + searchString + "%'";
                            searchQuery = "WHERE mf.status in (0,1) AND mf.txn_id like '%" + searchString + "%'";

                        } else {
                            searchQuery = "WHERE mf.status in (0,1) ";
                        }

                        if (period.equals("Today")) {
                            fromDate = dFToday.format(dNow) + " 00:00:00";
                            toDate = dFToday.format(dNow) + " 23:59:59";

                            // out.print("<h2 align=\"center\">Today fromDate :: " + fromDate + "</h2>");
                            //  out.print("<h2 align=\"center\">Today toDate :: " + toDate + "</h2>");
                        }
                        if (period.equals("Yesterday")) {
                            fromDate = dFToday.format(dNowYesterday) + " 00:00:00";
                            toDate = dFToday.format(dNowYesterday) + " 23:59:59";

                            //  out.print("<h2 align=\"center\">Yesterday fromDate :: " + fromDate + "</h2>");
                            //   out.print("<h2 align=\"center\">Yesterday toDate :: " + toDate + "</h2>");
                        }

                        if (period.equals("ThisWeek")) {

                            // get today and clear time of day
                            Calendar cal = Calendar.getInstance();

                            cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
                            Date firstDayOfWeek = cal.getTime();
                            String startofThisWeek = dFToday.format(firstDayOfWeek);
                            Date startofThisWeek1 = justDay.parse(justDay.format(firstDayOfWeek));
                            Date lastDayOfWeekT = new Date(startofThisWeek1.getTime() + 6 * 24 * 60 * 60 * 1000);
                            String lastDayOfThisWeek = dFToday.format(lastDayOfWeekT);

                            //   out.print("<h2 align=\"center\">First Day of this week:       " + startofThisWeek + "</h2>");
                            //   out.print("<h2 align=\"center\">Last Day of this week:       " + lastDayOfThisWeek + "</h2>");
                            fromDate = startofThisWeek + " 00:00:00";
                            toDate = lastDayOfThisWeek + " 23:59:59";

                            //  out.print("<h2 align=\"center\">ThisWeek fromDate :: " + fromDate + "</h2>");
                            //   out.print("<h2 align=\"center\">ThisWeek toDate :: " + toDate + "</h2>");
                        }

                        if (period.equals("ThisMonth")) {

                            Calendar firstDayCalendar = Calendar.getInstance();   // this takes current date
                            firstDayCalendar.set(Calendar.DAY_OF_MONTH, 1);
                            Date firstDayOfMonth = firstDayCalendar.getTime();

                            Calendar lastDayCalendar = Calendar.getInstance();
                            //  lastDayCalendar.setTime(dNow);

                            lastDayCalendar.add(Calendar.MONTH, 1);
                            lastDayCalendar.set(Calendar.DAY_OF_MONTH, 1);
                            lastDayCalendar.add(Calendar.DATE, -1);

                            Date lastDayOfMonth = lastDayCalendar.getTime();

                            //  out.print("<h2 align=\"center\">First Day of Month            : " + dFToday.format(firstDayOfMonth) + "</h2>");
                            //   out.print("<h2 align=\"center\">Last Day of Month: " + dFToday.format(lastDayOfMonth) + "</h2>");
                            //  out.print("<h2 align=\"center\">Current week = " + Calendar.DAY_OF_WEEK + "</h2>");
                            fromDate = dFToday.format(firstDayOfMonth) + " 00:00:00";
                            toDate = dFToday.format(lastDayOfMonth) + " 23:59:59";

                            //   out.print("<h2 align=\"center\">ThisMonth fromDate :: " + fromDate + "</h2>");
                            //   out.print("<h2 align=\"center\">ThisMonth toDate :: " + toDate + "</h2>");
                        }

                        if (period.equals("ThisYear")) {
                            fromDate = dFY.format(dNow) + "-01-01 00:00:00";
                            toDate = dFY.format(dNow) + "-12-31 23:59:59";

                            //   out.print("<h2 align=\"center\">ThisYear fromDate :: " + fromDate + "</h2>");
                            //   out.print("<h2 align=\"center\">ThisYear toDate :: " + toDate + "</h2>");
                        }
                        if (period.equals("Custom")) {
                            
                            customDateBox = "";

                            fromCustomDate = request.getParameter("fromCustomDate") == null ? "" : request.getParameter("fromCustomDate").trim();
                            toCustomDate = request.getParameter("toCustomDate") == null ? "" : request.getParameter("toCustomDate").trim();

                            fromDate = fromCustomDate + "-01-01 00:00:00";
                            toDate = toCustomDate + "-12-31 23:59:59";
                            
                            periodp = "period=" + period + "&fromCustomDate=" + fromCustomDate + "&toCustomDate=" + toCustomDate + "&";

//                            fromDate = dFY.format(dNow) + "-01-01 00:00:00";
//                            toDate = dFY.format(dNow) + "-12-31 23:59:59";
                            //   out.print("<h2 align=\"center\">Custom fromDate :: " + fromDate + "</h2>");
                           //    out.print("<h2 align=\"center\">Custom toDate :: " + toDate + "</h2>");
                        }

//                        if ($period == "Today") {
//
//                            //Today
//                            $newDateTime = date("Y-m-d");
//
//                            $fromDate = $newDateTime.
//                            " "."00:00:00";
//                                        $toDate = $newDateTime.
//                        
//                        " "."23:59:59";
//                                }
                        String searchQueryT = " WHERE mf.mod_date BETWEEN '" + fromDate + "' AND '" + toDate + "'  ";

//                        out.print("<h2 align=\"center\">searchQueryT :: " + searchQueryT + "</h2>");
//
//                        String searchCountSQLT = "SELECT count(*) FROM  member_fee  mf "
//                                + "" + searchQueryT + " "
//                                + "ORDER BY mf.mod_date DESC";
//
//                        out.print("<h2 align=\"center\">searchCountSQLT :: " + searchCountSQLT + "</h2>");
//
//                        String searchSQLT = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "mf.txn_id,mf.ref_no,mf.ref_description,mf.txn_date, "
//                                + "mf.paid_date,mf.fee_year,mf.amount,mf.status transactionStatus,  "
//                                + "mf.due_date,mf.bill_type,mf.pay_option,fy.member_fees_year_name   "
//                                + "FROM  member_fee mf "
//                                + "LEFT JOIN member m ON m.id = mf.member_id  "
//                                + "LEFT JOIN member_fees_year fy ON fy.member_fees_year_id = mf.fee_year  "
//                                + "" + searchQueryT + " "
//                                + "ORDER BY mf.mod_date DESC  LIMIT " + start + ", " + limit + "";
//
//                        out.print("<h2 align=\"center\">searchSQLT :: " + searchSQLT + "</h2>");
                    %>

                    <form class="form-group"  name="memberPaymentAdd" id="memberPaymentAdd" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/account/transactionHistory.jsp?sessionid=<%out.print(sessionid);%>" >


                        <div class="row">


                            <div class="col-md-3 offset-md-1">
                                <div class="form-group">
                                    <input type="text" id="searchString" name="searchString" value="<%=searchString%>" class="form-control" placeholder="Search by Transaction ID"  disabled="">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-md-3 m-t-10">Type</label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="ticketStatusType" name="ticketStatusType" disabled="">
                                            <option value="All">All</option>
                                            <option value="0">Online</option>
                                            <option value="1">Cash</option>
                                        </select>                                             
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-md-4 m-t-10">Status</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="ticketStatusType" name="ticketStatusType" disabled="">
                                            <option value="All">All</option>
                                            <option value="0">Paid</option>
                                            <option value="1">Due</option>
                                            <option value="5">Fail</option>
                                            <option value="4">Null</option>
                                        </select>                                             
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-3 m-t-10">Date</label>
                                    <div class="col-md-9">
                                        <%

                                            if (period.equals("Today")) {
                                                Today = " selected=\"selected\"";
                                            } else {
                                                Today = "";
                                            }
                                            if (period.equals("Yesterday")) {
                                                Yesterday = " selected=\"selected\"";
                                            } else {
                                                Yesterday = "";
                                            }
                                            if (period.equals("ThisWeek")) {
                                                ThisWeek = " selected=\"selected\"";
                                            } else {
                                                ThisWeek = "";
                                            }
                                            if (period.equals("ThisMonth")) {
                                                ThisMonth = " selected=\"selected\"";
                                            } else {
                                                ThisMonth = "";
                                            }

                                            if (period.equals("ThisYear")) {
                                                ThisYear = " selected=\"selected\"";
                                            } else {
                                                ThisYear = "";
                                            }

                                            if (period.equals("Custom")) {
                                                Custom = " selected=\"selected\"";
                                            } else {
                                                Custom = "";
                                            }

                                        %>

                                        <select class="form-control" id="period" name="period" onchange="changeCustomPeriodField(this.value);">
                                            <option value="Today" <%= Today%>>Today</option>
                                            <option value="Yesterday" <%= Yesterday%>>Yesterday</option>
                                            <option value="ThisWeek" <%= ThisWeek%>>This Week</option>
                                            <option value="ThisMonth" <%= ThisMonth%>>This Month</option>
                                            <option value="ThisYear" <%= ThisYear%>>This Year</option>
                                            <option value="Custom" <%=Custom%>>Custom</option>
                                        </select>                                             
                                    </div>
                                </div>
                            </div>



                        </div>
                        <!-- .row -->

                        <div class="row">
                            <div class="col-md-4 offset-md-1" id="frmInfo" <%=customDateBox%>>
                                <div class="form-group">
                                    <label class="control-label col-md-3 m-t-10">From Date</label>
                                    <div class="col-md-9">
                                        <input type="text" id="fromCustomDate" name="fromCustomDate" value="<%=fromCustomDate%>" class="form-control" placeholder="From date(YYYY-MM-DD)">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="toInfo" <%=customDateBox%>>
                                <div class="form-group">
                                    <label class="control-label col-md-3 m-t-10">To Date</label>
                                    <div class="col-md-9">
                                        <input type="text" id="toCustomDate" name="toCustomDate" value="<%=toCustomDate%>" class="form-control" placeholder="To date(YYYY-MM-DD)">
                                    </div>
                                </div>
                            </div>
                            <div id="offsetAddNocustomDateBoxXn" class="col-md-3 <%=offsetAddNocustomDateBox%>">

                                <button type="submit" name="btnSearch" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i> Search</button> 

                                <!--                                <button type="submit" name="btnSearch" class="btn waves-effect waves-light btn-success"><i class="fa fa-file-excel-o"></i> Export</button>-->

                            </div>


                        </div>
                        <!-- .row -->


                    </form>



                </div>
                <!-- /.white-box -->
            </div>

            <div class="col-md-12">
                <div class="white-box">




                    <%
                        //String searchCountSQL = "SELECT count(*) FROM  member m,member_type mt "
                        //        + "WHERE m.status = 1 "
                        //        + "AND m.id = mt.member_id "
                        //       + " " + filterSQLStr + " "
                        //       + "ORDER BY m.id DESC";

//                        String searchCountSQL = "SELECT count(*) FROM  member_fee  mf "
//                                + "LEFT JOIN member m ON m.id = mf.member_id  "
//                                + "" + searchQuery + " "
//                                + "ORDER BY mf.txn_id DESC";
//                        String searchCountSQL = "SELECT count(*) FROM  member_fee  mf "
//                                + "" + searchQuery + " "
//                                + "ORDER BY mf.txn_id DESC";
                        String searchCountSQL = "SELECT count(*) FROM  member_fee  mf "
                                + "" + searchQueryT + " "
                                + "ORDER BY mf.mod_date DESC";

                        String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        if (pageNbr != 0) {
                            start = (pageNbr - 1) * limit;
                        } else {
                            start = 0;
                        }

                        //   System.out.println("startLLLL :: " + start);
                        //    System.out.println("limitLLLL :: " + limit);
                        // Initial page num setup
                        if (pageNbr == 0) {
                            pageNbr = 1;
                        }
                        prev = pageNbr - 1;
                        next = pageNbr + 1;
                        lastpage1 = Math.ceil((double) total_pages / limit);
                        //   DecimalFormat df = new DecimalFormat("#.##");
                        //  System.out.print(df.format(lastpage1));

                        // lastpage = (int) Math.ceil(total_pages / limit);
                        lastpage = (int) lastpage1;
                        LastPagem1 = (int) Math.round(lastpage - 1);

                        //  System.out.println("total_pages :: " + total_pages);
                        //   System.out.println("limit :: " + limit);
                        //   System.out.println("lastpage1 :: " + lastpage1);
                        //   System.out.println("lastpage :: " + lastpage);
                        //   System.out.println("LastPagem1 :: " + LastPagem1);
                        String paginate = "";
                        if (lastpage > 1) {
                            paginate = paginate + "<ul class='pagination' style=\"margin:0\">";

                            // Previous
                            if (pageNbr > 1) {
                                p = "page=" + prev;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
                                //   paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p + "\">prev</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>previous</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
                            }

                            int counter = 1;
                            // Pages
                            if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

                                for (counter = 1; counter <= lastpage; counter++) {
                                    if (counter == pageNbr) {
                                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                    } else {
                                        p = "page=" + counter;
                                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                        //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p + "\">" + counter + "</a></li>";
                                    }
                                }
                            } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
                                // Beginning only hide later pages
                                if (pageNbr < 1 + (stages * 2)) {
                                    for (counter = 1; counter < 4 + (stages * 2); counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // Middle hide some front and some back
                                else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // End only hide early pages
                                else {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                                    for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                }
                            }

                            // Next
                            if (pageNbr < counter - 1) {
                                p = "page=" + next;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
                                //  paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + periodp + filterp + p + "\"'>next</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>next</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
                            }

                            paginate = paginate + "</ul>";
                        }

                        int minPaginationIndexLimit = ((pageNbr - 1) * limit);
                        int maxPaginationIndexLimit = (pageNbr * limit);
                        int lastProductIndex = total_pages;
                        int minPaginationIndexLimitPlus = minPaginationIndexLimit + 1;
//                        if(minPaginationIndexLimitPlus < limit){
//                            minPaginationIndexLimitPlus = 1;
//                        }

                        if (maxPaginationIndexLimit >= lastProductIndex) {
                            maxPaginationIndexLimit = lastProductIndex;
                        }

//                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "md.mem_division_name,c.center_name   "
//                                + "FROM  member m ,member_type mt,member_division md,center c "
//                                + "WHERE m.status = 1 "
//                                + "AND m.member_division_id = md.mem_division_id "
//                                + "AND m.center_id = c.center_id "
//                                + "AND m.id = mt.member_id "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY m.id DESC  LIMIT " + start + ", " + limit + "";
//                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "mf.txn_id,mf.ref_no,mf.ref_description,mf.txn_date, "
//                                + "mf.paid_date,mf.fee_year,mf.amount,mf.status transactionStatus,  "
//                                + "mf.due_date,mf.bill_type,mf.pay_option,fy.member_fees_year_name   "
//                                + "FROM  member_fee mf "
//                                + "LEFT JOIN member m ON m.id = mf.member_id  "
//                                + "LEFT JOIN member_fees_year fy ON fy.member_fees_year_id = mf.fee_year  "
//                                + "" + searchQuery + " "
//                                + "ORDER BY mf.txn_id DESC  LIMIT " + start + ", " + limit + "";
                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
                                + "mf.txn_id,mf.ref_no,mf.ref_description,mf.txn_date, "
                                + "mf.paid_date,mf.fee_year,mf.amount,mf.status transactionStatus,  "
                                + "mf.due_date,mf.bill_type,mf.pay_option,fy.member_fees_year_name   "
                                + "FROM  member_fee mf "
                                + "LEFT JOIN member m ON m.id = mf.member_id  "
                                + "LEFT JOIN member_fees_year fy ON fy.member_fees_year_id = mf.fee_year  "
                                + "" + searchQueryT + " "
                                + "ORDER BY mf.mod_date DESC  LIMIT " + start + ", " + limit + "";

                        //   System.out.println("searchCountSQL :: " + searchCountSQL);
                        //    System.out.println("searchSQL :: " + searchSQL);

                    %>
                    <div class="row">

                        <div class="col-md-8" style="padding: 20px 0;">
                            <%=paginate%>
                        </div>

                        <div class="col-md-4" style="padding-top: 23px;">
                            <p class="text-right">
                                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">#</th>
                                        <th class="text-center" style="width: 5%;">ID</th>
                                        <th class="text-center" style="width: 5%;">Date</th>
                                        <th class="text-center" style="width: 5%;">Amount</th>
                                        <th class="text-center" >Bill Type</th>
                                        <th class="text-center" >FeeYear</th>
                                        <th class="text-center" style="width: 5%;">Status</th>
                                        <th class="text-center" style="width: 5%;">Ref</th>
                                        <th class="text-center" style="width: 5%;">Ref Desc</th>
                                        <th class="text-center" style="width: 20%;">Member Info</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <%
                                        Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                                        if (!searchSQLQry.list().isEmpty()) {
                                            for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                                                searchObj = (Object[]) searchItr.next();
                                                memberId = searchObj[0].toString();
                                                memberIEBId = searchObj[1] == null ? "" : searchObj[1].toString();
                                                memberName = searchObj[2] == null ? "" : searchObj[2].toString();

                                                memberPictureName = searchObj[3] == null ? "" : searchObj[3].toString();
                                                memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPictureName;
                                                memberPicture = "<img width=\"120\" src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\">";

                                                memberMobile = searchObj[4] == null ? "" : searchObj[4].toString();
                                                memberEmail = searchObj[5] == null ? "" : searchObj[5].toString();

                                                //     memberDivisionName = searchObj[7] == null ? "" : searchObj[7].toString();
                                                //      memberCenterName = searchObj[8] == null ? "" : searchObj[8].toString();
                                                mOthersInfo = "<strong>MemberID:</strong>" + memberIEBId + "<br>"
                                                        + "<strong>Name:</strong>" + memberName + "<br>"
                                                        + "<strong>Mobiile:</strong>" + memberMobile + "<br>"
                                                        + "<strong>Mobiile:</strong>" + memberEmail + "<br>"
                                                        + "<strong>Division:</strong>" + memberDivisionName + "<br>"
                                                        + "<strong>Center:</strong>" + memberCenterName + "<br>"
                                                        + "<strong>University:</strong>" + memberUniversityName;

                                                txn_id = searchObj[7] == null ? "" : searchObj[7].toString();
                                                ref_no = searchObj[8] == null ? "" : searchObj[8].toString();
                                                ref_description = searchObj[9] == null ? "" : searchObj[9].toString();
                                                txn_date = searchObj[10] == null ? "" : searchObj[10].toString();

                                                paid_date = searchObj[11] == null ? "" : searchObj[11].toString();
                                                fee_year = searchObj[12] == null ? "" : searchObj[12].toString();
                                                amount = searchObj[13] == null ? "" : searchObj[13].toString();

                                                pay_option = searchObj[17] == null ? "" : searchObj[17].toString();

                                                if (pay_option.equals("1")) {
                                                    pay_option = "Online";
                                                } else {
                                                    pay_option = "Cash";
                                                }

                                                transactionStatus = searchObj[14] == null ? "" : searchObj[14].toString();

                                                if (transactionStatus.equals("1")) {
                                                    transactionStatusText = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
                                                    pay_option = pay_option;
                                                }
                                                if (transactionStatus.equals("0")) {
                                                    transactionStatusText = "<span class=\"btn btn-danger btn-sm waves-effect\">Due</span>";
                                                    pay_option = "";

                                                }
                                                if (transactionStatus.equals("2")) {
                                                    transactionStatusText = "<span class=\"btn btn-info btn-sm waves-effect\">Fail</span>";
                                                    pay_option = "";

                                                }

                                                if (transactionStatus.equals("")) {
                                                    transactionStatusText = "<span class=\"btn btn-danger btn-sm waves-effect\">Null</span>";
                                                    pay_option = "";
                                                }

                                                due_date = searchObj[15] == null ? "" : searchObj[15].toString();
                                                bill_type = searchObj[16] == null ? "" : searchObj[16].toString();

                                                //   1=Renewal,2=Registration ,3=Life, 4=Above65,5=Unknown
                                                if (bill_type.equals("1")) {
                                                    billTypeText = "Renewal";
                                                }
                                                if (bill_type.equals("2")) {
                                                    billTypeText = "Registration";
                                                }
                                                if (bill_type.equals("3")) {
                                                    billTypeText = "Life";
                                                }
                                                if (bill_type.equals("4")) {
                                                    billTypeText = "Above65";
                                                }
                                                if (bill_type.equals("5")) {
                                                    billTypeText = "Unknown";
                                                }

                                                feeYearText = searchObj[18] == null ? "" : searchObj[18].toString();

                                                //   memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
%>
                                    <tr id="infoBox<%=memberId%>">
                                        <td><% out.print(minPaginationIndexLimitPlus);%></td>
                                        <td><%=txn_id%></td>
                                        <td><%=txn_date%></td>
                                        <td><%=amount%></td>
                                        <td><%=billTypeText%></td>
                                        <td><%=feeYearText%></td>
                                        <td><%=transactionStatusText%><br/><%=pay_option%></td>
                                        <td><%=ref_no%></td>
                                        <td><%=ref_description%></td>
                                        <td><%=mOthersInfo%></td>

                                    </tr>
                                    <%
                                            minPaginationIndexLimitPlus++;
                                        }
                                    } else {
                                    %>
                                    <tr>
                                        <td colspan="8" class="text-center">There no data found</td>

                                    </tr>
                                    <%
                                        }
                                    %>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <%
                        //  }
                        dbsession.clear();
                        dbsession.close();
                    %>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <%=paginate%>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>