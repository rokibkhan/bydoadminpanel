
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>





<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getRegistryID regId = new getRegistryID();

    Query resultBufferSQL = null;
    Object resultBufferObj[] = null;

    Query resultFileSQL = null;

    Query studentResultSQL = null;

    String resultId = "";
    String studentId = "";
    String studentName = "";
    String studentDOB = "";
    String studentPassingYear = "";
    String studentResult = "";
    String universityId = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        String bulkFileId = request.getParameter("bulkFileId").trim();
        String act = request.getParameter("act").trim();

        System.out.println("bulkResultSheetDetailsProcessSubmitData bulkFileId :: " + bulkFileId);

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //  Date dateX = new Date();        
        java.util.Date dateX = new java.util.Date();
        String adddate = dateFormatX.format(dateX);

        InetAddress ip;
        String hostname = "";
        String ipAddress = "";
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            ipAddress = ip.getHostAddress();

        } catch (UnknownHostException e) {

            e.printStackTrace();
        }

        resultBufferSQL = dbsession.createSQLQuery("SELECT * FROM university_result_buffer WHERE file_id='" + bulkFileId + "'");
        System.out.println("bulkResultSheetDetailsProcessSubmitData resultSQL :: " + resultBufferSQL);

        String universityResultId = "";
        if (!resultBufferSQL.list().isEmpty()) {

            for (Iterator resultBufferItr = resultBufferSQL.list().iterator(); resultBufferItr.hasNext();) {

                resultBufferObj = (Object[]) resultBufferItr.next();
                resultId = resultBufferObj[0].toString().trim();
                studentId = resultBufferObj[1].toString().trim();
                studentName = resultBufferObj[2].toString().trim();
                studentDOB = resultBufferObj[3].toString().trim();
                studentPassingYear = resultBufferObj[4].toString().trim();
                studentResult = resultBufferObj[5].toString().trim();
                universityId = resultBufferObj[6].toString().trim();

              //  universityResultId = regId.getID(57);//UNIVERSITY_RESULT_ID
              universityResultId = resultId;

                System.out.println("bulkResultSheetDetailsProcessSubmitData universityResultId :: " + universityResultId);
                studentResultSQL = dbsession.createSQLQuery("INSERT INTO university_result("
                        + "result_id,"
                        + "student_id,"
                        + "student_name,"
                        + "student_birth_date,"
                        + "passing_year,"
                        + "student_result,"
                        + "university_id,"
                        + "status,"
                        + "add_user,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip) VALUES("
                        + "'" + universityResultId + "',"
                        + "'" + studentId + "',"
                        + "'" + studentName + "',"
                        + "'" + studentDOB + "',"
                        + "'" + studentPassingYear + "',"
                        + "'" + studentResult + "',"
                        + "'" + universityId + "',"
                        + "'1',"
                        + "'" + userNameH + "',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "')");

                System.out.println("bulkResultSheetDetailsProcessSubmitData studentResultSQL :: " + studentResultSQL);
                studentResultSQL.executeUpdate();

                universityResultId = "";
                resultId = "";
                studentId = "";
                studentName = "";
                studentDOB = "";
                studentPassingYear = "";
                studentResult = "";
                universityId = "";
                
                System.out.println("bulkResultSheetDetailsProcessSubmitData universityResultId END :: " + universityResultId);

            }

            //update university_result_file table Status
            resultFileSQL = dbsession.createSQLQuery("UPDATE university_result_file SET "
                    + "status = '1', "
                    + "mod_user = '" + userNameH + "',"
                    + "mod_date = '" + adddate + "',"
                    + "mod_ip = '" + ipAddress + "' "
                    + "WHERE id =" + bulkFileId + "");

            System.out.println("bulkResultSheetDetailsProcessSubmitData resultFileSQL :: " + resultFileSQL);
            resultFileSQL.executeUpdate();

        }

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {

            strMsg = "Result Sheet Process Successfully";

//            strMsg = "Success!!! Member Training Information added ";
            response.sendRedirect("bulkResultSheetList.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! When Result Sheet Process. ";
            dbtrx.rollback();
            response.sendRedirect("bulkResultSheetDetails.jsp?sessionid=" + sessionIdH + "&resultFileIdX=" + bulkFileId + "&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
        return;
    }

%>
