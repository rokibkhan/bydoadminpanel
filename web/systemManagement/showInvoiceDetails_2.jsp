<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String selectInvId = request.getParameter("invoiceId");
    String sessionid = request.getParameter("sessionid");

    //  String selectInvId = "1083";
    String invoiceDetailsTopSection = "";
    String invoiceDetailsMidSection = "";
    String invoiceDetailsBottomSection = "";
    String invoiceDetailsAllSection = "";
    String invoiceDetails = "";
    String btnInvInfo = "";

    int ix = 1;
    String invNo = "";
    String invStore = "";
    String invStoreName = "";
    String invStoreAddress = "";
    String invStoreAddress2 = "";
    String invStoreThana = "";
    String invStoreDistrict = "";
    String invStoreZipCode = "";
    String invStorePhone = "";
    String invStoreEmail = "";
    String invStoreAddressX = "";
    String invNo1 = "";
    String invDate = "";
    String invDate1 = "";
    int consumerId = 0;
    String consumerEmail = "";
    String consumerFirstName = "";
    String consumerLastName = "";
    String consumerName = "";
    String consumerAddress = "";
    String consumerAddress1 = "";
    String consumerZipCode = "";
    String consumerThana = "";
    String consumerDistrict = "";
    String consumerAddressX = "";
    String consumerThanaZip = "";
    String invSubTotalAmount = "";
    String invSubTotalAmount1 = "";
    String invSalesTax = "";
    String invSalesTax1 = "";
    String invHandlingCharge = "";
    String invHandlingCharge1 = "";
    String invFreight = "";
    String invFreight1 = "";
    String invDiscountAmount = "";
    String invDiscountAmount1 = "";
    String invTotalAmount = "";
    String invTotalAmount1 = "";
    String invPaymentAmount = "";
    String invPaymentAmount1 = "";
    String invNetAmount = "";
    String invNetAmount1 = "";
    String invAmount = "";
    String invAmountInWord = "";
    String invAmount1 = "";
    String status = "";
    String timeZone = "America/Chicago";

    TimeZoneConversion tz = new TimeZoneConversion();

    DecimalFormat df = new DecimalFormat("#,###,###,##0.00");

    StringToDate sTd = new StringToDate();

    Query q1 = dbsession.createQuery("from InvoiceMain AS invMn WHERE invMn.invNo =  '" + selectInvId + "'");

    for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {

        InvoiceMain invoiceMain = (InvoiceMain) itr1.next();
        invNo = invoiceMain.getInvNo();
        invStore = invoiceMain.getStore().getId().toString();

        invStoreName = invoiceMain.getStore().getStoreName();
        invStoreAddress = invoiceMain.getStore().getAddressBook().getAddress1();
        invStoreAddress2 = invoiceMain.getStore().getAddressBook().getAddress2() == null ? "" : " ,<br/>" + invoiceMain.getStore().getAddressBook().getAddress2();

        invStoreThana = invoiceMain.getStore().getAddressBook().getThana().getThanaName();
        invStoreDistrict = invoiceMain.getStore().getAddressBook().getThana().getDistrict().getDistrictName();
        invStoreZipCode = invoiceMain.getStore().getAddressBook().getZipcode();

        invStoreAddressX = invStoreAddress;

        invStoreAddressX = invStoreAddressX + invStoreAddress2;

        invStoreAddressX = invStoreAddressX + " ,<br/>" + invStoreThana + ", " + invStoreDistrict;

        if (invStoreZipCode != null && !invStoreZipCode.isEmpty()) {
            invStoreAddressX = invStoreAddressX + " " + invStoreZipCode;
        }

        invNo1 = invStore + '-' + invNo;
        invDate = (invoiceMain.getAddDate().toString()).substring(0, 19);

        invDate1 = sTd.getDate(invDate);

        consumerId = invoiceMain.getConsumer().getId();

        invSubTotalAmount = invoiceMain.getSubTotal().toString();
        invSubTotalAmount1 = df.format(Double.parseDouble(invSubTotalAmount));

        invSalesTax = invoiceMain.getSalesTax().toString();
        invSalesTax1 = df.format(Double.parseDouble(invSalesTax));

        invHandlingCharge = invoiceMain.getHandlingCharge().toString();
        invHandlingCharge1 = df.format(Double.parseDouble(invHandlingCharge));

        invFreight = invoiceMain.getFreight().toString();
        invFreight1 = df.format(Double.parseDouble(invFreight));

        invDiscountAmount = invoiceMain.getDiscountAmount().toString();
        invDiscountAmount1 = df.format(Double.parseDouble(invDiscountAmount));

        invTotalAmount = invoiceMain.getTotalAmount().toString();
        invTotalAmount1 = df.format(Double.parseDouble(invTotalAmount));

        invPaymentAmount = invoiceMain.getPaymentAmount().toString();
        invPaymentAmount1 = df.format(Double.parseDouble(invPaymentAmount));

        invNetAmount = invoiceMain.getNetAmount().toString();
        invNetAmount1 = df.format(Double.parseDouble(invNetAmount));
        
        System.out.println("invTotalAmount: "+Double.parseDouble(invTotalAmount));
//        invAmountInWord = servlet.EnglishNumberInWord.convert(java.math.BigDecimal.valueOf(Double.parseDouble(invTotalAmount)));
        invAmountInWord = EnglishNumberInWord.convert(invTotalAmount);

        System.out.println("invAmountInWord "+invAmountInWord);
        status = invoiceMain.getStatus();

        if (status.equalsIgnoreCase("C")) {
            status = "Payment Pending";
        } else if (status.equalsIgnoreCase("P")) {
            status = "Payment Complete";
        } else if (status.equalsIgnoreCase("D")) {
            status = "Delivered";
        }
    }

    Query invAddressSQL = dbsession.createSQLQuery("select ab.first_name,ab.last_name,ab.ADDRESS_1,ab.ADDRESS_2,ab.ZIPCODE,t.THANA_NAME,d.DISTRICT_NAME from consumer_address ca,address_book ab,thana t,district d "
            + "where ca.consumer_id='" + consumerId + "' "
            + "and ca.address_id=ab.id "
            + "and ab.thana_id=t.id "
            + "and t.district_id=d.id");

    if (!invAddressSQL.list().isEmpty()) {
        for (Iterator it4 = invAddressSQL.list().iterator(); it4.hasNext();) {

            obj = (Object[]) it4.next();

            consumerFirstName = obj[0].toString().trim();
            consumerLastName = obj[1].toString().trim();

            consumerName = consumerFirstName + " " + consumerLastName;

            consumerAddress = obj[2].toString().trim();
            consumerAddress1 = obj[3].toString().trim();
            consumerZipCode = obj[4].toString().trim();
            consumerThana = obj[5].toString().trim();
            consumerDistrict = obj[6].toString().trim();

            consumerAddressX = consumerAddress;

            if (consumerAddress1 != null && !consumerAddress1.isEmpty()) {
                consumerAddressX = consumerAddressX + " ,<br/>" + consumerAddress1;
            }
            if (consumerThana != null && !consumerThana.isEmpty()) {
                consumerAddressX = consumerAddressX + " ,<br/>" + consumerThana;
            }

            if (consumerDistrict != null && !consumerDistrict.isEmpty()) {
                consumerAddressX = consumerAddressX + " ,<br/>" + consumerDistrict;
            }

            if (consumerZipCode != null && !consumerZipCode.isEmpty()) {
                consumerAddressX = consumerAddressX + " " + consumerZipCode;
            }

        }
    }

    invoiceDetailsTopSection = "<div class=\"white-box printableArea\">"
            + "<h4><b>&nbsp;</b> <span class=\"pull-right\">Invoice No#" + invNo1 + "</span></h4>"
            + "<hr>"
            + "<div class=\"row\">"
            + "<div class=\"col-md-12\">"
            + "<div class=\"pull-left\">"
            + "<address>"
            + "<h3> &nbsp;<b class=\"text-danger\">" + invStoreName + "</b></h3>"
            + "<p class=\"text-muted m-l-5\">" + invStoreAddressX + "</p>"
            + "</address>"
            + "</div>"
            + "<div class=\"pull-right text-right\">"
            + "<address>"
            + "<h3>To,</h3>"
            + "<h4 class=\"font-bold\">" + consumerName + "</h4>"
            + "<p class=\"text-muted m-l-30\">" + consumerAddressX + "</p>"
            + "<p class=\"m-t-30\"><b>Invoice Date :</b> <i class=\"fa fa-calendar\"></i> " + invDate1 + "</p>"
            + "</address>"
            + "</div>"
            + "</div>";

    invoiceDetailsMidSection = "<div class=\"col-md-12\">"
            + "<div class=\"table-responsive m-t-40\" style=\"clear: both;\">"
            + "<table class=\"table table-hover color-table1 primary-table1 table-custom-padding-5x\">"
            + "<thead>"
            + "<tr>"
            + "<th class=\"text-center\">#</th>"
            + "<th>Product Code</th>"
            + "<th>Name</th>"
            + "<th>Grade</th>"
            + "<th>Size</th>"
            + "<th class=\"text-center\">Quantity</th>"
            + "<th class=\"text-right\">Rate</th>"
            + "<th class=\"text-right\">Discount</th>"
            + "<th class=\"text-right\">Total</th>"
            + "</tr>"
            + "</thead>"
            + "<tbody>";

    String product_code = "";
    String product_name = "";
    String product_size_inch = "";
    String store_id = "";
    String QUANTITY1 = "";
    String QUANTITY2 = "";
    String RATE = "";
    String RATE1 = "";
    String DISCOUNT_AMOUNT = "";
    String DISCOUNT_AMOUNT1 = "";
    String TOTAL_AMOUNT = "";
    String TOTAL_AMOUNT1 = "";
    String uom1 = "";
    String carton_size = "";
    String uom2 = "";
    String product_grade = "";
    String quantityUom = "";

    Query invItemsSQL = dbsession.createSQLQuery("select p.product_code,p.product_name,ps.product_size_inch,i.store_id,i.QUANTITY1,i.QUANTITY2,i.RATE,i.DISCOUNT_AMOUNT,i.TOTAL_AMOUNT,u1.uom uom1,u2.carton_size,u2.uom uom2,pcd.product_grade "
            + "from invoice_items i,product_class pc,product_class_defination pcd,product p,product_size ps,product_uom u1,carton_size u2 "
            + "where i.INV_NO= '" + invNo + "' "
            + "and i.PRODUCT_CLASS_ID=pc.id "
            + "and pc.product_id=p.product_id "
            + "and ps.id=p.product_size_id "
            + "and p.product_uom2=u2.id "
            + "and p.product_uom1=u1.id "
            + "and pc.class_id=pcd.id");

    if (!invItemsSQL.list().isEmpty()) {
        for (Iterator it1 = invItemsSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            product_code = obj[0].toString().trim();
            product_name = obj[1].toString().trim();
            product_size_inch = obj[2].toString().trim();

            QUANTITY1 = obj[4].toString().trim();
            QUANTITY2 = obj[5].toString().trim();

            RATE = obj[6].toString().trim();
            RATE1 = df.format(Double.parseDouble(RATE));

            DISCOUNT_AMOUNT = obj[7].toString().trim();
            DISCOUNT_AMOUNT1 = df.format(Double.parseDouble(DISCOUNT_AMOUNT));

            TOTAL_AMOUNT = obj[8].toString().trim();
            TOTAL_AMOUNT1 = df.format(Double.parseDouble(TOTAL_AMOUNT));

            uom1 = obj[9].toString().trim();
            carton_size = obj[10].toString().trim();
            uom2 = obj[11].toString().trim();
            product_grade = obj[12].toString().trim();

            quantityUom = QUANTITY1 + " " + uom1 + "  " + QUANTITY2 + " " + uom2;

            invoiceDetailsMidSection += "<tr>"
                    + "<td class=\"text-center\">1</td>"
                    + "<td>" + product_code + "</td>"
                    + "<td>" + product_name + "</td>"
                    + "<td>" + product_grade + "</td>"
                    + "<td>" + product_size_inch + "</td>"
                    + "<td>" + quantityUom + "</td>"
                    + "<td class=\"text-right\">" + RATE1 + "</td>"
                    + "<td class=\"text-right\">" + DISCOUNT_AMOUNT1 + "</td>"
                    + "<td class=\"text-right\">" + TOTAL_AMOUNT1 + "</td>"
                    + "</tr>";

        }
    }

    invoiceDetailsBottomSection = "<tr>"
            + "<td colspan=\"7\" class=\"text-right\"> Sub Total </td>"
            + "<td class=\"text-right\"> 0.00 </td>"
            + "<td class=\"text-right\"> " + invSubTotalAmount1 + " </td>"
            + "</tr>"
            + "</tbody>"
            + "</table>"
            + "</div>"
            + "</div>"
            + "<div class=\"col-md-12\">"
                + "<div class=\" col-md-6 pull-right m-t-30 text-right\">"
                    + "<div class=\"table-responsive\">"
                    + "<table class=\"table color-bordered-table table-custom-padding-5x\">"
                    + "<tbody>"
                    + "<tr><td><b>Sub Total</b> </td><td>" + invSubTotalAmount1 + "</td></tr>"
                    + "<tr><td><b>Carrying</b></td><td>" + invFreight1 + "</td></tr>"
                    + "<tr><td><b>Discount</b></td><td>" + invDiscountAmount1 + "</td></tr>"
                    + "<tr><td><b>Sales Tax</b></td><td>" + invSalesTax1 + "</td></tr>"
                    + "<tr><td><h4><b>Total</b></h4></td><td><h4>" + invTotalAmount1 + "</h4></td></tr>"
                    + "<tr><td><b>Payment</b></td><td>" + invPaymentAmount1 + "</td></tr>"
                    + "<tr><td><b>Balance/Due</b></td><td>" + invNetAmount1 + "</td></tr>"
                    + "</tbody>"
                    + "</table>"
                    + "</div>"
                + "</div>"
            + "</div>"
            + "<div class=\"clearfix\"></div>"
            + "<hr>"
            + "<div class=\"col-md-12 text-left\" style=\"border-top: 1px solid #eceeef;\">"
            + "<P>In word:" + invAmountInWord + "</p>"
            + "</div>"
            + "</div>"
            + "</div>";

    invoiceDetails = invoiceDetailsTopSection + invoiceDetailsMidSection + invoiceDetailsBottomSection;

//    out.println(invoiceDetails);
    String btnPaymentInfo = "";

    if (Double.parseDouble(invNetAmount) > 0) {
        btnPaymentInfo = "<a id=\"btnShowInvoicePaymentOption\" onclick=\"showInvoicePaymentOption("+selectInvId+",'"+sessionid+"')\" class=\"btn btn-primary\"> Proceed to payment </a> ";
    }

    String invPrintUrl = GlobalVariable.baseUrl + "/sales/invoicePrint.jsp?sessionid=" + sessionid + "&invNo=" + selectInvId;
//String invPrintUrl = "";
    btnInvInfo = "<div class=\"clearfix\"></div>"
            + "<div class=\"text-right\">"
            + btnPaymentInfo
            + "<a title=\"Print Invoice\" href=\"" + invPrintUrl + "\" target=\"_blank\" id=\"print\" class=\"btn btn-default btn-outline\" type=\"button\"> <span><i class=\"fa fa-print\"></i> Print</span> </a> "
            + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>"
            + "</div>"
            + "<br/>";

    json = new JSONObject();
    json.put("selectInvId", selectInvId);
    json.put("invoiceDetails", invoiceDetails);
    json.put("btnInvInfo", btnInvInfo);
    jsonArr.add(json);

    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>