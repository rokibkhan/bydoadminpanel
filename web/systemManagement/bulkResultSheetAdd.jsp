
<%@page import="com.appul.entity.SyDept"%>
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>








<script>

//    $(function () {
//        var focus = 0;
//        $("#universityName").focusout(
//                function () {
//                    focus++;
//                    console.log("checkUserNameAvaliablity:: " + focus);
//                    var universityName = $("#universityName").val();
//
//                    console.log("universityName:: " + universityName);
//
//                    $("#universityNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');
//
//
//
//                });
//
//    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var universityName = $("#universityName").val();
        var universityShortName = $("#universityShortName").val();
        var universityCountryName = $("#universityCountryName").val();


        if (universityName == null || universityName == "") {
            $("#universityName").focus();
            $("#universityNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#universityNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (universityShortName == null || universityShortName == "") {
            $("#universityShortName").focus();
            $("#universityShortNameErr").addClass("help-block with-errors").html("Short Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#universityShortNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }

        if (universityCountryName == null || universityCountryName == "") {
            $("#universityCountryName").focus();
            $("#universityCountryNameErr").addClass("help-block with-errors").html("Country is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#universityCountryNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }




        return true;
    }


</script>
<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    dbtrx = dbsession.beginTransaction();

    //  String sessionid = request.getParameter("sessionid").trim();
    // if (session.getAttribute("username") != null) {
    String sessionIdHx = session.getId();
    String usernameHx = session.getAttribute("username").toString().toUpperCase();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    String universityNameId = request.getParameter("universityIdX") == null ? "" : request.getParameter("universityIdX").trim();

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Bulk Result Sheet</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">System Management</a></li>
                    <li class="active">Add Bulk Result</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-8"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="<% out.print(GlobalVariable.baseUrl);%>/BulkResultSheetUpload?sessionid=<%out.print(sessionid);%>&act=add" onSubmit="return fromDataSubmitValidation1()"  enctype="multipart/form-data">

                                <input type="hidden" id="universityNameOld" name="universityNameOld" value="ACCREDITED">

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">University *</label>
                                    <div class="col-sm-9">
                                        <select id="universityName" name="universityName" class="form-control input-sm customInput-sm" style="height: 30px;"  required>
                                            <option value="">Select University</option>
                                            <%
                                                Query universitySQL = null;
                                                Object[] universityObj = null;
                                                String universityId = "";
                                                String universityShortName = "";
                                                String universityLongName = "";
                                                String universityName = "";
                                                String universityOptions = "";
                                                String universityOptionsSelect = "";
                                                universitySQL = dbsession.createSQLQuery("select * FROM university  WHERE university_old_name = 'ACCREDITED' AND check_roll = '1' ORDER BY university_long_name ASC");
                                                for (Iterator universityItr = universitySQL.list().iterator(); universityItr.hasNext();) {
                                                    universityObj = (Object[]) universityItr.next();
                                                    universityId = universityObj[0].toString();
                                                    universityShortName = universityObj[1].toString();
                                                    universityLongName = universityObj[2].toString() == null ? "" : universityObj[2].toString();

                                                    universityName = universityLongName;

                                                    if (!universityNameId.equals("")) {

                                                        if (universityId.equals(universityNameId)) {
                                                            universityOptionsSelect = " selected";
                                                        } else {
                                                            universityOptionsSelect = "";
                                                        }
                                                    }

                                                    universityOptions = universityOptions + "<option value=\"" + universityId + "\"  " + universityOptionsSelect + ">" + universityName + "</option>";

                                                }
                                            %> 
                                            <%=universityOptions%>


                                        </select>
                                        <div  id="universityNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">File<font color="red">*</font></label>
                                    <div class="col-sm-6">
                                        <input type="file" style="padding-top: 0; padding-left: 0;" class="form-control input-sm" id="fileName" name="fileName" placeholder="Click for file..."  required="required" >
                                        <span id ="fileNameErr"></span>
                                    </div>

                                </div>



                                <div class="form-group row">
                                    <label class="control-label col-sm-3"></label>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<%=usernameHx%>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<% //out.print(hostname);%>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<% //out.print(ipAddress);%>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->

        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);
        %>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>


    </div>
    <!-- /.container-fluid -->
    <%
        dbsession.flush();
        dbsession.close();
    %>


    <%@ include file="../footer.jsp" %>