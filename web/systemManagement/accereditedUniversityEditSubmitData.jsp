<%-- 
    Document   : Sysytem User dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        String universityNameOld = request.getParameter("universityNameOld").trim();

        String universityName = request.getParameter("universityName").trim();

        //    System.out.println("userPass:: " + userPass + " encpass :: " + encpass);
        String universityShortName = request.getParameter("universityShortName").trim();
        String universityWebsite = request.getParameter("universityWebsite").trim();
        String universityCountryName = request.getParameter("universityCountryName").trim();

        String universityResultCheck = request.getParameter("universityResultCheck") == null ? "0" : request.getParameter("universityResultCheck").trim();

        System.out.println("universityShortName:: " + universityShortName + " universityName :: " + universityName + " universityCountryName :: " + universityCountryName);

        String universityId = request.getParameter("universityId");
        //check university name already exits

        //update university_result_file table Status
        Query accUniUpdateSQL = dbsession.createSQLQuery("UPDATE university SET "
                + "university_short_name = '" + universityShortName + "', "
                + "university_long_name = '" + universityName + "',"
                + "website = '" + universityWebsite + "',"
                + "check_roll = '" + universityResultCheck + "', "
                + "fax = '" + universityCountryName + "' "
                + "WHERE university_id =" + universityId + "");

        System.out.println("accUniUpdateSQL " + accUniUpdateSQL);

        accUniUpdateSQL.executeUpdate();

        dbtrx.commit();
        dbsession.flush();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            // response.sendRedirect("success.jsp?sessionid=" + sessionid);
            strMsg = "University Update Successfully";
            //  response.sendRedirect("accereditedUniversityAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

            response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/accereditedUniversityEdit.jsp?sessionid=" + sessionIdH + "&universityIdX=" + universityId + "&strMsg=" + strMsg);
        } else {
            dbtrx.rollback();
            // response.sendRedirect("fail.jsp?sessionid=" + sessionid);

            strMsg = "Error!!! When information update";
            // response.sendRedirect("accereditedUniversityAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/accereditedUniversityEdit.jsp?sessionid=" + sessionIdH + "&universityIdX=" + universityId + "&strMsg=" + strMsg);

        }

    } else {
        //if session not found message here      
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }
%>
