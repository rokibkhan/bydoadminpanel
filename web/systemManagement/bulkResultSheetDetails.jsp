<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>


<script type="text/javascript">

</script>
<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
        dbtrx = dbsession.beginTransaction();

        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
            //        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

        String resultFileId = request.getParameter("resultFileIdX") == null ? "" : request.getParameter("resultFileIdX").trim();
        Query resultFileSQL = null;
        Object resultFileObj[] = null;
        String resultFileName = "";
        String resultFileRecCount = "";
        String resultFileStatus = "";
        String resultFileStatusText = "";
        String resultFileUniversityName = "";
        String resultFileInfo = "";

        if (!resultFileId.equals("")) {

            resultFileSQL = dbsession.createSQLQuery("SELECT uf.id,uf.file_name,uf.rec_count,uf.status,u.university_long_name "
                    + "FROM university_result_file uf,university u "
                    + "WHERE uf.university_id = u.university_id "
                    + "ORDER BY uf.id ASC");

            System.out.println("resultFileSQL::" + resultFileSQL);

            if (!resultFileSQL.list().isEmpty()) {
                for (Iterator it1 = resultFileSQL.list().iterator(); it1.hasNext();) {

                    resultFileObj = (Object[]) it1.next();
                    resultFileName = resultFileObj[1].toString().trim();
                    resultFileRecCount = resultFileObj[2].toString().trim();
                    resultFileStatus = resultFileObj[3].toString().trim();
                    resultFileUniversityName = resultFileObj[4].toString().trim();

                    if (resultFileStatus.equals("1")) {
                        resultFileStatusText = "disable";
                    } else {
                        resultFileStatusText = "";
                    }
                }
                resultFileInfo = "<strong>File Name :</strong>" + resultFileName + " <br/> <strong>Total Record :</strong>" + resultFileRecCount + " <br/> <strong>University :</strong>" + resultFileUniversityName;
            } else {
                resultFileInfo = "";
            }

        } else {
            resultFileInfo = "";
        }
    %>


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Result Sheet Details</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">System Management</a></li>                    
                    <li class="active">Result Sheet Details</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-12">
                            <%=resultFileInfo%>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-12">


                            <div class="table-responsive">
                                <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="text-center">Roll</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">DOB</th>
                                            <th class="text-center">Passing Year</th>
                                            <th class="text-center">Result</th>
                                            <th class="text-center">University</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%
                                            int ix = 1;
                                            Query resultSQL = null;
                                            Object resultObj[] = null;
                                            String resultId = "";
                                            String studentId = "";
                                            String studentName = "";
                                            String studentDOB = "";
                                            String studentPassingYear = "";
                                            String studentResult = "";
                                            String universityName = "";

                                            resultSQL = dbsession.createSQLQuery("SELECT urb.result_id,urb.student_id,urb.student_name,urb.student_birth_date,"
                                                    + "urb.passing_year ,urb.student_result ,u.university_long_name "
                                                    + "FROM university_result_buffer urb,university u "
                                                    + "WHERE urb.university_id = u.university_id "
                                                    + "AND urb.file_id = " + resultFileId + " "
                                                    + "ORDER BY urb.result_id ASC");

                                            System.out.println("resultSQL::" + resultSQL);

                                            if (!resultSQL.list().isEmpty()) {
                                                for (Iterator it1 = resultSQL.list().iterator(); it1.hasNext();) {

                                                    resultObj = (Object[]) it1.next();
                                                    resultId = resultObj[0].toString().trim();
                                                    studentId = resultObj[1].toString().trim();
                                                    studentName = resultObj[2].toString().trim();
                                                    studentDOB = resultObj[3].toString().trim();
                                                    studentPassingYear = resultObj[4].toString().trim();
                                                    studentResult = resultObj[5].toString().trim();
                                                    universityName = resultObj[6].toString().trim();


                                        %>


                                        <tr>
                                            <td><%=ix%></td>
                                            <td><%=studentId%></td>
                                            <td><%=studentName%></td>
                                            <td><%=studentDOB%></td>     
                                            <td class="text-center"><%=studentPassingYear%></td>   
                                            <td><%=studentResult%></td>     
                                            <td><%=universityName%></td>                                      
                                        </tr>

                                        <%
                                                    ix++;
                                                }

                                            }
                                            dbsession.clear();
                                            dbsession.close();
                                        %>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="bulkResultSheetDetailsProcessSubmitData.jsp?sessionid=<%out.println(sessionid);%>&act=add">

                                <input type="hidden" id="bulkFileId" name="bulkFileId" value="<%=resultFileId%>">

                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<% //out.print(username);%>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<% //out.print(hostname);%>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<% //out.print(ipAddress);%>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <%
                                            if (!resultFileStatus.equals("1")) {
                                        %>
                                        <button type="submit" class="btn btn-info">Start Result Sheet Process</button>
                                        <% }
                                            %>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>