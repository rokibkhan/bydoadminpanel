
<%@page import="com.appul.entity.SyDept"%>
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>








<script>

//    $(function () {
//        var focus = 0;
//        $("#universityName").focusout(
//                function () {
//                    focus++;
//                    console.log("checkUserNameAvaliablity:: " + focus);
//                    var universityName = $("#universityName").val();
//
//                    console.log("universityName:: " + universityName);
//
//                    $("#universityNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');
//
//
//
//                });
//
//    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var universityName = $("#universityName").val();
        var universityShortName = $("#universityShortName").val();
        var universityCountryName = $("#universityCountryName").val();


        if (universityName == null || universityName == "") {
            $("#universityName").focus();
            $("#universityNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#universityNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (universityShortName == null || universityShortName == "") {
            $("#universityShortName").focus();
            $("#universityShortNameErr").addClass("help-block with-errors").html("Short Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#universityShortNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }

        if (universityCountryName == null || universityCountryName == "") {
            $("#universityCountryName").focus();
            $("#universityCountryNameErr").addClass("help-block with-errors").html("Country is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#universityCountryNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }




        return true;
    }


</script>
<%    dbsession = HibernateUtil.getSessionFactory().openSession();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    String universityNameId = request.getParameter("universityIdX") == null ? "" : request.getParameter("universityIdX").trim();

    Query universitySQL = null;
    Object universityObj[] = null;
    String universityLongName = "";
    String universityShortName = "";
    String universityCheckRoll = "";
    String universityCheckRollChecked = "";
    String universityCountry = "";
    String universityWebsite = "";
    String resultFileUniversityName = "";
    String resultFileInfo = "";

    if (!universityNameId.equals("")) {
        universitySQL = dbsession.createSQLQuery("SELECT * FROM university WHERE university_id = '" + universityNameId + "'");

        System.out.println("universitySQL  ::" + universitySQL);

        if (!universitySQL.list().isEmpty()) {
            for (Iterator universityItr = universitySQL.list().iterator(); universityItr.hasNext();) {

                universityObj = (Object[]) universityItr.next();
                universityShortName = universityObj[1].toString().trim();
                universityLongName = universityObj[2].toString().trim();
                universityWebsite = universityObj[4].toString().trim();
                universityCountry = universityObj[6].toString().trim();
                universityCheckRoll = universityObj[9].toString().trim();

                if (universityCheckRoll.equals("1")) {
                    universityCheckRollChecked = " checked";
                } else {
                    universityCheckRollChecked = "";
                }
            }
        }
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Edit Accredited University</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">System Management</a></li>
                    <li class="active">Edit University</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-6"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="accereditedUniversityEditSubmitData.jsp?sessionid=<%out.println(sessionid);%>&act=add" onSubmit="return fromDataSubmitValidation()">

                                <input type="hidden" id="universityNameOld" name="universityNameOld" value="ACCREDITED">
                                <input type="hidden" id="universityId" name="universityId" value="<%=universityNameId%>">
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Name *</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="universityName" name="universityName" value="<%=universityLongName%>"  placeholder="University Name" class="form-control input-sm" required>
                                        <div  id="universityNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Short Name *</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="universityShortName" name="universityShortName" value="<%=universityShortName%>"  placeholder="University Short Name" class="form-control input-sm" required>
                                        <div  id="universityShortNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Website</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="universityWebsite" name="universityWebsite" value="<%=universityWebsite%>"  placeholder="University Website" class="form-control input-sm">
                                        <div  id="universityWebsiteErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Country Name*</label>
                                    <div class="col-sm-9">
                                        <select id="universityCountryName" name="universityCountryName" class="form-control input-sm customInput-sm" style="height: 30px;"  required>
                                            <option value="">SELECT</option>
                                            <%
                                                Object countryObj[] = null;
                                                String countryName = "";
                                                String countryNameSelected = "";
                                                Query countrySQL = dbsession.createSQLQuery("SELECT * FROM sy_country order by COUNTRY_NAME ASC");
                                                if (!countrySQL.list().isEmpty()) {
                                                    for (Iterator countryIt1 = countrySQL.list().iterator(); countryIt1.hasNext();) {

                                                        countryObj = (Object[]) countryIt1.next();
                                                        countryName = countryObj[1].toString().trim();
                                                        if (countryName.equals(universityCountry)) {
                                                            countryNameSelected = " selected";
                                                        } else {
                                                            countryNameSelected = "";
                                                        }
                                            %> 
                                            <option value="<%=countryName%>"  <%=countryNameSelected%>> <%=countryName%> </option> 
                                            <%
                                                    }
                                                }
                                                dbsession.flush();
                                                dbsession.close();
                                            %>

                                        </select>
                                        <div  id="universityCountryNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-9 offset-md-3">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="universityResultCheck" name="universityResultCheck" value="1" <%=universityCheckRollChecked%>> 
                                            <label class="form-check-label" for="universityResultCheck" style="margin-top: 2px;">&nbsp;&nbsp;Allow Result Check</label>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <label class="control-label col-sm-3"></label>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<% //out.print(username);%>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<% //out.print(hostname);%>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<% //out.print(ipAddress);%>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->

        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);
            %>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>