<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Logger logger = Logger.getLogger("ticketAddSubmitData_jsp.class");

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object uniObj[] = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String selectUniversityId = request.getParameter("universityId");
    String selectUniversityName = "";
    String sessionid = request.getParameter("sessionid");

    Query uniSQL = dbsession.createSQLQuery("SELECT * FROM university WHERE university_id = '" + selectUniversityId + "'");

   // System.out.println("uniSQL::" + uniSQL);
    if (!uniSQL.list().isEmpty()) {
        for (Iterator uniIt1 = uniSQL.list().iterator(); uniIt1.hasNext();) {

            uniObj = (Object[]) uniIt1.next();
            selectUniversityName = uniObj[2].toString().trim();

            //System.out.println("selectUniversityName::" + selectUniversityName);
        }
    }

    String subjectId = "";
    String subjectName = "";
    String subjectDepartment = "";
    String subjectUniversity = "";
    String subjectDivision = "";
    String subject_division_id = "";

    String selectRoleDetailsTopSection = "";
    String selectRoleDetailsMidSection = "";
    String selectRoleDetailsBottomSection = "";

    String btnColorClass = "";
    String btnInvInfo = "";
    String selectSubjectDetails = "";

    String allActivityConStr = "";

    selectRoleDetailsTopSection = "<div class=\"white-box1 printableArea1\">"
            + "<h4 class=\"text-center\">" + selectUniversityName + "</h4></div>";

    selectRoleDetailsMidSection = "<div class=\"col-md-12\">"
            + "<div class=\"table-responsive\" style=\"\">"
            + "<table class=\"table table-bordered table-hover color-table1 primary-table1 table-custom-padding-5x\">"
            + "<thead>"
            + "<tr>"
            + "<th class=\"text-center\" width=\"5%\">#</th>"
            + "<th class=\"text-center\">Subject</th>"
            + "<th class=\"text-center\">IEB Division</th>"
            + "</tr>"
            + "</thead>"
            + "<tbody>";

    Query usrSQL = dbsession.createSQLQuery("SELECT sb.subject_id,sb.subject_long_name,u.university_long_name,md.full_name "
            + "FROM university_subject sb,university u,member_division md "
            + "WHERE  sb.university_id = u.university_id AND sb.ieb_division_id = md.mem_division_id "
            + "AND sb.university_id ='" + selectUniversityId + "'");
    int ix = 1;
    if (!usrSQL.list().isEmpty()) {
        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            subjectId = obj[0].toString().trim();
            subjectName = obj[1].toString().trim();
            subjectUniversity = obj[2].toString().trim();
            subjectDivision = obj[3].toString().trim();

            selectRoleDetailsMidSection += "<tr>"
                    + "<td class=\"text-center\">" + ix + "</td>"
                    + "<td>" + subjectName + "</td>"
                    + "<td>" + subjectDivision + "</td>"
                    + "</tr>";
            ix++;
        }
    }

    selectRoleDetailsBottomSection = "</tbody>"
            + "</table>"
            + "</div>"
            + "</div>";

    selectSubjectDetails = selectRoleDetailsTopSection + selectRoleDetailsMidSection + allActivityConStr + selectRoleDetailsBottomSection;

    //     + "<a id=\"btnShowUpdateActivityOption\" onclick=\"showUpdateActivityOption('" + selectRoleId + "','" + selectRoleName + "','" + sessionid + "')\" class=\"btn btn-primary\"> Update Role Activity </a> &nbsp;"
    btnInvInfo = "<div class=\"clearfix\"></div>"
            + "<div class=\"text-right\">"
            + "<a id=\"universitySubjectAddOptionBtn\" onclick=\"universitySubjectAddOption('" + selectUniversityId + "','" + selectUniversityName + "','" + sessionid + "')\"  class=\"btn btn-info waves-effect waves-light\">Add Subject</a> &nbsp;"
            + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>"
            + "</form></div>"
            + "<br/>";

    json = new JSONObject();
    json.put("selectUniversityId", selectUniversityId);
    json.put("selectUniSubjectDetails", selectSubjectDetails);
    json.put("btnInvInfo", btnInvInfo);
    jsonArr.add(json);

    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>