

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String messageSessiossId = request.getParameter("sessionid").trim();

        messageSessiossId = session.getId();
        System.out.println(messageSessiossId);
    %>

    <script type="text/javascript">

        modifyFlag = false;
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessiossd: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        
        




        function messageDeleteInfo(arg1, arg2) {

            console.log("messageDeleteInfo::: " + arg1);

            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            
            rupantorLGModal.find("#rupantorLGModalTitle").text("Messages Delete confirmation");
            btnConfirmInfo = "<a id=\"messageDeleteInfoConfirmBtn\" onclick=\"messageDeleteInfoConfirm(" + arg1 + ",'" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);
            rupantorLGModal.modal('show');
        }


        function messageDeleteInfoConfirm(arg1, arg2) {
            console.log("messageDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');



            $.post("messageDeleteProcess.jsp", {messageId: arg1, sessiossd: arg2}, function (data) {

                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data[0].requestId).html('<td align="center" colspan="4"><h5>' + data[0].responseMsg + '</h5></td>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");

        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .message title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="message-title">Messages List</h4>

            </div>
            <!-- /.message title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Messages Management</a></li>                    
                    <li class="active">Messages</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width:10%; ">Name</th>
                                    <th style="width:10%; ">Designation</th>
                                    <th class="text-center">Details</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String messageId = "";
                                    String messageName = "";
                                    String messageTitle = "";
                                    String messageShortDetails = "";
                                    String messageDetails = "";
                                    String messagePublished = "";
                                    String messageAddUser = "";
                                    String messageAddDate = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT mf.id_message, mf.message_name,mf.message_title, mf.message_short_desc, mf.message_desc, "
                                            + "mf.feature_image, mf.published, mf.add_user,mf.add_date  "
                                            + " FROM  message_from mf ORDER BY mf.showing_order ASC");

                                    if (!usrSQL.list().isEmpty()) {

                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();

                                            messageId = obj[0].toString().trim();
                                            messageName = obj[1].toString().trim();
                                            messageTitle = obj[2].toString().trim();
                                            messageShortDetails = obj[3].toString().trim();
                                            messageDetails = obj[4].toString().trim();
                                            messagePublished = obj[5].toString().trim();

                                            String messageEditUrl = GlobalVariable.baseUrl + "/messageManagement/messageEdit.jsp?sessionid=" + session.getId() + "&messageIdX=" + messageId + "&selectedTab=profile";
                                            String messageDeleteUrl = GlobalVariable.baseUrl + "/messageManagement/message.jsp?sessionid=" + session.getId() + "&messageIdX=" + messageId + "&selectedTab=changePass";


                                %>


                                <tr id="infoBox<%=messageId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(messageName);%></td>
                                    <td><% out.print(messageTitle);%></td>
                                    <td><% out.println(messageDetails); %></td>
                                    <td class="text-center">

                                        <a id="btnShowSyUserDetails<% out.print(messageId);%>" onclick="showPageDetails(<% out.print("'" + messageId + "','" + sessionid + "'");%>);" title="User details" class="btn btn-primary btn-sm"><i class="icon-info"></i></a>


                                        <a onclick="messageEditInfo(<% out.print("'" + messageId + "','" + sessionid + "'");%>)" href="messageEdit.jsp?sessiossd=<%=sessionid%>&messageid=<%=messageId%>" title="Edit Page" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onclick="messageDeleteInfo(<% out.print("'" + messageId + "','" + sessionid + "'");%>)" title="Delete this message" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>