<%-- 
    Document   : Sysytem Tech dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Logger logger = Logger.getLogger("photoAlbumAddSubmitData_jsp.class");

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID regId = new getRegistryID();
    String photoAlbumId = regId.getID(16);

    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    //  if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        String photoAlbumName = request.getParameter("photoAlbumName").trim();
        String photoAlbumShortName = request.getParameter("photoAlbumShortName").trim();
        String showingOrder = "1";
        Byte published = 1;
        String feature_image = "no_image.jpg";

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);
        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();
        
        /*

//category_name varchar(45) 
//category_short_name varchar(45) 
//showing_order varchar(45) 
//feature_image varchar(45) 
//image1 varchar(45) 
//image2 varchar(45) 
//image3 varchar(45) 
//image4 varchar(45) 
//published tinyint(2) 
//add_user varchar(45) 
//add_date varchar(45) 
//add_term varchar(45) 
//add_ip varchar(45) 
//mod_user varchar(45) 
//mod_date varchar(45) 
//mod_term varchar(45) 
//mod_ip
        //   String techDistrictId = request.getParameter("techDistrictId").trim();
        Query q4 = dbsession.createSQLQuery("INSERT INTO picture_category("
                + "id_cat,"
                + "category_name,"
                + "category_short_name,"
                + "showing_order,"
                + "feature_image,"
                + "image1,"
                + "image2,"
                + "image3,"
                + "image4,"
                + "published,"
                + "add_date,"
                + "add_user,"
                + "add_term,"
                + "add_ip,"
                + "mod_date,"
                + "mod_user,"
                + "mod_term,"
                + "mod_ip"
                + ") values( "
                + "'" + photoAlbumId + "',"
                + "'" + photoAlbumName + "',"
                + "'" + photoAlbumShortName + "',"
                + "'" + showingOrder + "',"
                + "'" + feature_image + "',"
                + "'" + feature_image + "',"
                + "'" + feature_image + "',"
                + "'" + feature_image + "',"
                + "'" + feature_image + "',"
                + "'" + published + "',"
                + "'" + adddate + "',"
                + "'" + adduser + "',"
                + "'" + addterm + "',"
                + "'" + addip + "',"
                + "'" + adddate + "',"
                + "'" + adduser + "',"
                + "'" + addterm + "',"
                + "'" + addip + "'"
                + ")");

        System.out.println("q4:: " + q4);
        q4.executeUpdate();
*/
        PictureCategory album = new PictureCategory();

        album.setIdCat(Integer.parseInt(photoAlbumId));
        album.setCategoryName(photoAlbumName);
        album.setCategoryShortName(photoAlbumShortName);
        album.setFeatureImage(feature_image);
        album.setImage1(feature_image);
        album.setImage2(feature_image);
        album.setImage3(feature_image);
        album.setImage4(feature_image);
        album.setShowingOrder(showingOrder);
        album.setPublished(published);

        album.setAddDate(adddate);
        album.setAddUser(adduser);
        album.setAddTerm(addterm);
        album.setAddIp(addip);

        album.setModDate(adddate);
        album.setModUser(adduser);
        album.setModTerm(addterm);
        album.setModIp(addip);

        dbsession.save(album);

        if (dbtrx.wasCommitted()) {

            strMsg = "Photo Album Added Successfully";
            response.sendRedirect("photoAlbumAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        } else {
            dbtrx.rollback();

            strMsg = "Error!!! When Photo Album add";
            response.sendRedirect("photoAlbumAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        }
    } else {
        //if session not found message here      
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }

    dbsession.clear();
    dbsession.close();
%>
