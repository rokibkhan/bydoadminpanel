<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);

        String committeeIdSel = request.getParameter("committeeId") == null ? "" : request.getParameter("committeeId").trim();

        if (committeeIdSel.equals("")) {

            //redirect committee list
            response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeList.jsp");
        }


    %>



    <script type="text/javascript">

        $(function () {
            var focus = 0;
            $("#memberShipId").focusout(
                    //  $("#memberShipId").onchange(
                            function () {
                                focus++;
                                console.log("checkUserNameAvaliablity:: " + focus);
                                var memberShipId = $("#memberShipId").val();

                                console.log("memberShipId " + memberShipId);

                                $("#memberShipIdErr").html('<i class="fa fa-spin fa-spinner"></i> Member info checking...');

                                //   showMemberDetailsInformation



                                showMemberDetailsInformation('<%=session.getId()%>');


                            });

                });


        function showMemberDetailsInformation(arg1) {
            console.log("showMemberDetailsInformation ");
            var committeeId, memberShipId, btnInvInfo, url, sessionid, cccll;
            committeeId = $("#committeeId").val();


            memberShipId = $("#memberShipId").val();

            console.log("committeeId::  " + committeeId);
            console.log("memberShipId ::  " + memberShipId);

            if (committeeId != '' && memberShipId != '') {

                url = '<%=GlobalVariable.baseUrl%>' + '/committeeManagement/committeeMemberDetailsShowProcess.jsp';

                $.post(url, {sessionId: arg1, committeeId: committeeId, memberShipId: memberShipId}, function (data) {

                    console.log(data);

                    if (data[0].responseCode == 1) {

                        $("#showMemberDetailsInformationBox").html(data[0].memberInfoCon);
                        $("#memberStatusInCommittee").html(data[0].memberStatusInCommittee);


                        //   $("#acttionBox" + data[0].requestId).html("<a class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approved</a>");
                        //	    	 	$("#globalAlertInfoBoxCon").show();
                        //$("#btnSubscribeEmailOpt").button('reset');
                        //		$("#btnSubscribeEmailOpt").button('complete') // button text will be "finished!"
                        // $("#btnSubscribeEmailOpt").addClass("disabled");
                        //  $("#msgSubscribeEmailOpt").html(data[0].responseMsg);
                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                    }
                    if (data[0].responseCode == 0) {

                        $("#showMemberDetailsInformationBox").html(data[0].memberInfoCon);

                        //  $("#btnSubscribeEmailOpt").button('reset');
                        // $("#errMsgShow").html(data[0].responseMsg);
                        //   rupantorLGModal.find("#rupantorLGModalBody").html(data[0].responseMsg);
                        // $("#globalAlertInfoBoxCon").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                        $("#globalAlertInfoBoxConTT").show().css({"display": "block", "margin-top": "5px"});
                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");

                    }

                }, "json");



            } else {
                $("#showMemberDetailsInformationBox").html("");
            }

            //   $("#showMemberDetailsInformationBox").html(committeeId + " :: " + memberShipId);

        }


        function deleteCommitteeMemberInfo(arg1, arg2) {
            console.log("deleteCommitteeMemberInfo :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"deleteCommitteeMemberInfoConfirmBtn\" onclick=\"deleteCommitteeMemberInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');
        }

        function deleteCommitteeMemberInfoConfirm(arg1, arg2) {
            console.log("deleteCommitteeMemberInfoConfirm :: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');


            $.post("committeeMemberInfoDeleteProcess.php", {committeeId: arg1, committeeMemberId: arg2}, function (data) {


                console.log(data);

                if (data.responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data.responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data.requestId).html('<h5>' + data.responseMsg + '</h5>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data.responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");
        }

    </script>

    <%        Session dbsessionCommitteeAdd = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrxCommitteeAdd = dbsessionCommitteeAdd.beginTransaction();

        int ix = 1;
        String committeeId = "";
        String committeeType = "";
        String committeeName = "";
        String committeeShortName = "";
        String committeeDuration = "";
        String committeeDesc = "";
        String committeeStatus = "";
        Object objCmmt[] = null;

        String status1 = "";
        String btnActivePoll = "";
        String btnCompletePoll = "";
        String committeeMemberBtn = "";
        String agrX = "";

        String committeeComTypeId = "";
        String committeeTypeText = "";

        Query q1 = null;
        Object objcmType[] = null;

        Query usrSQL = dbsessionCommitteeAdd.createSQLQuery("SELECT c.id_committee, c.committee_type, c.committee_name, "
                + "c.committee_short_name, c.committee_duration, c.committee_desc, c.status, "
                + "c.feature_image,c.com_cat_id  "
                + "FROM  committee c "
                + "WHERE  c.id_committee = '" + committeeIdSel + "'");

        if (!usrSQL.list().isEmpty()) {
            for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                objCmmt = (Object[]) it1.next();
                committeeId = objCmmt[0].toString().trim();
                committeeType = obj[1] == null ? "" : objCmmt[1].toString().trim();
                committeeName = obj[2] == null ? "" : objCmmt[2].toString().trim();
                committeeShortName = objCmmt[3] == null ? "" : objCmmt[3].toString().trim();
                committeeDuration = obj[4] == null ? "" : objCmmt[4].toString().trim();
                //        committeeDesc = obj[5] == null ? "" : objCmmt[5].toString().trim();
                committeeStatus = obj[6] == null ? "" : objCmmt[6].toString().trim();

                agrX = "";

                if (committeeStatus.equals("0")) {
                    status1 = "InActive";
                    btnActivePoll = "";
                    //      btnActivePoll = "&nbsp;<a title=\"Status change to current\" onClick=\"committeeStatusChangeInfo("+agrX+");\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";
                    status1 = status1 + " " + btnActivePoll;
                    committeeMemberBtn = "";
                }

                if (committeeStatus.equals("1")) {
                    status1 = "Active";
                    btnCompletePoll = "&nbsp;<a title=\"Status change to old\" onClick=\"committeeStatusChangeInfo(" + agrX + ");\" class=\"btn btn-primary\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";

                    status1 = status1 + btnCompletePoll;
                    committeeMemberBtn = "<a title=\"Add committee member\"  class=\"btn btn-primary\" href=\"" + GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "\"><i class=\"fa fa-plus\"></i> Add Committee Member</a>";
                }

                //    techTeritoryName = obj[5] == null ?"" : obj[5].toString().trim();
                String committeeDetailsUrl = GlobalVariable.baseUrl + "/committeeManagement/committeeDetails.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                String updateProfileUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                String updateChangePassUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=changePass";

                committeeComTypeId = objCmmt[8] == null ? "" : objCmmt[8].toString().trim();

                System.out.println("committeeComTypeId::" + committeeComTypeId);
                System.out.println("committeeType::" + committeeType);

                if (committeeType.equals("HQ")) {
                    committeeTypeText = "Head Quarter";
                } else {

                    //division
                    if (committeeComTypeId.equals("2")) {

                        q1 = dbsessionCommitteeAdd.createSQLQuery("select * FROM member_division WHERE mem_division_id = '" + committeeType + "'");
                        if (!q1.list().isEmpty()) {
                            for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                objcmType = (Object[]) divisionItr.next();
                                committeeTypeText = objcmType[1].toString();
                            }
                        }

                    }
                    //centre
                    if (committeeComTypeId.equals("3")) {
                    }
                    //sub-centre
                    if (committeeComTypeId.equals("4")) {
                    }
                    //Overseas Chapter  Committee
                    if (committeeComTypeId.equals("5")) {
                    }
                    //Student Chapter  Committee
                    if (committeeComTypeId.equals("6")) {
                    }

                }
            }
        }
    %>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Committee Details</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Committee Management</a></li>                    
                    <li class="active">Details</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">&nbsp;&nbsp;<%=committeeName%></h3>
                    <h3 class="box-title m-b-0">&nbsp;&nbsp;<%=committeeTypeText%></h3>
                    <h3 class="box-title m-b-0">&nbsp;&nbsp;<%=committeeDuration%></h3>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row">
                        <div class="col-md-6"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="committeeMemberAddSubmitData.jsp?sessionid=<%=sessionid%>&act=add" onSubmit="return fromDataSubmitValidation()">
                                <input type="hidden" id="committeeId" name="committeeId" value="<%=committeeId%>">
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeName" class="control-label col-sm-3">Member</label>
                                    <div class="col-sm-9">

                                        <input name="memberShipId" id="memberShipId" type="text" class="form-control" placeholder="Membership ID">

                                        <div  id="memberShipIdErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeName" class="control-label col-sm-3">Committee Designation</label>
                                    <div class="col-sm-9">
                                        <select name="committeeMemberDesignation" id="committeeMemberDesignation" class="form-control input-sm">
                                            <option value="">Select Designation</option>
                                            <%
                                                Object[] objDesg = null;
                                                String designationId = "";
                                                String designationName = "";

                                                Query desgSQL = dbsessionCommitteeAdd.createSQLQuery("select * FROM committee_designation WHERE committee_id ='" + committeeIdSel + "'  ORDER BY orderby ASC");
                                                if (!desgSQL.list().isEmpty()) {
                                                    for (Iterator itrDesg = desgSQL.list().iterator(); itrDesg.hasNext();) {
                                                        objDesg = (Object[]) itrDesg.next();
                                                        designationId = objDesg[0].toString();
                                                        designationName = objDesg[1].toString();


                                            %>
                                            <option value="<%=designationId%>"><%=designationName%></option>

                                            <%
                                                    }
                                                }
                                            %>

                                        </select>

                                        <div  id="committeeMemberDesignationErr" class="help-block with-errors"></div>


                                    </div>
                                </div>


                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeName" class="control-label col-sm-3">Committee Department</label>
                                    <div class="col-sm-9">
                                        <select name="committeeMemberDepartment" id="committeeMemberDepartment" class="form-control input-sm">
                                            <option value="">Select Department</option>
                                            <%
                                                Object[] objDept = null;
                                                String departmentId = "";
                                                String departmentName = "";

                                                Query deptSQL = dbsessionCommitteeAdd.createSQLQuery("select * FROM committee_department WHERE committee_id ='" + committeeIdSel + "'  ORDER BY orderby ASC");
                                                if (!deptSQL.list().isEmpty()) {
                                                    for (Iterator itrDesg = deptSQL.list().iterator(); itrDesg.hasNext();) {
                                                        objDept = (Object[]) itrDesg.next();
                                                        departmentId = objDept[0].toString();
                                                        departmentName = objDept[1].toString();


                                            %>
                                            <option value="<%=departmentId%>"><%=departmentName%></option>

                                            <%
                                                    }
                                                }
                                            %>

                                        </select>

                                        <div  id="committeeMemberDepartmentErr" class="help-block with-errors"></div>


                                    </div>
                                </div>





                                <div class="form-group row">
                                    <label class="control-label col-sm-3"></label>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);                        ?>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);                        ?>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);                        ?>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6" id="showMemberDetailsInformationBox"> </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Committee Member List</h3>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <%                Object objCmmtMem[] = null;
                String idCommitteeMember = "";
                String memberIEBId = "";
                String committeeMemberName = "";
                String committeeDesignationName = "";
                String committeeDepartmentName = "";
                String committeeDepartmentName1 = "";
                String profilePicture = "";
                String profilePictureUrl = "";
                String btnCommitteeMemberEdit = "";
                String btnCommitteeMemberDelete = "";
                String argXn = "";

                Query cmmtMemSQL = dbsessionCommitteeAdd.createSQLQuery("SELECT "
                        + "cm.id_committee_member,m.member_id,m.member_name , "
                        + "cd.designation_name, cdp.department_name "
                        + "FROM  committee_member cm  "
                        + "LEFT JOIN committee c ON c.id_committee = cm.id_committtee   "
                        + "LEFT JOIN member m ON m.id = cm.member_id   "
                        + "LEFT JOIN committee_designation  cd ON cd.id = cm.committee_designation  "
                        + "LEFT JOIN committee_department  cdp ON cdp.id = cm.committee_department "
                        + "WHERE  c.id_committee = '" + committeeIdSel + "' "
                        + "ORDER BY cm.committee_showing_order ASC");

                if (!cmmtMemSQL.list().isEmpty()) {
                    for (Iterator itCM = cmmtMemSQL.list().iterator(); itCM.hasNext();) {

                        objCmmtMem = (Object[]) itCM.next();
                        idCommitteeMember = objCmmtMem[0].toString().trim();
                        memberIEBId = objCmmtMem[1].toString().trim();
                        committeeMemberName = objCmmtMem[2].toString().trim();
                        committeeDesignationName = objCmmtMem[3].toString().trim();
                        committeeDepartmentName = objCmmtMem[4] == null ? "" : objCmmtMem[4].toString().trim();

                        if (!committeeDepartmentName.equals("")) {
                            committeeDepartmentName1 = "<p>(" + committeeDepartmentName + ")</p>";
                        }

                        //   out.println(idCommitteeMember);
                        //    profilePicture = "M_38661.jpg";  
                        profilePicture = "no_image.jpg";
                        profilePictureUrl = GlobalVariable.imageDirLink + profilePicture;

                        argXn = "'" + session.getId() + "','" + idCommitteeMember + "'";

                        btnCommitteeMemberEdit = "<a title=\"Edit\" href=\"" + GlobalVariable.baseUrl + "/committeeManagement/committeeMemberUpdate.php?sessionid=" + session.getId() + "&idCommitteeMember=" + idCommitteeMember + "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil-square-o\"></i></a>";
                        btnCommitteeMemberDelete = "<a onClick=\"deleteCommitteeMemberInfo(" + argXn + ");\" title=\"Delete comittee member\"  class=\"btn btn-primary\"><i class=\"fa fa-trash-o\"></i></a>";


            %>


            <!-- .col -->
            <div class="col-md-4 col-sm-4">
                <div class="white-box" id="infoBox<%=idCommitteeMember%>" style="min-height: 260px; padding: 5px;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                            <a href="contact-detail.html">
                                <img src="<%=profilePictureUrl%>" alt="<%=committeeMemberName%>" class="img-circle img-responsive">
                            </a>
                            <h5 class="box-title1 m-b-0" style="line-height1: 20px;"><%=committeeDesignationName%></h5> 
                            <%=committeeDepartmentName1%>

                            <%=btnCommitteeMemberEdit%>
                            <%=btnCommitteeMemberDelete%>

                        </div>
                        <div class="col-md-8 col-sm-8">
                            <h3 class="box-title m-b-0"><%=committeeMemberName%></h3> 
                            <small><% //=designationName%></small>

                            <address>
                                795 Folsom Ave, Suite 600 San Francisco, CADGE 94107
                                <br/>
                                <br/>
                                <abbr title="Phone">Office :</abbr> <% // $OfficePhone; %><br/>
                                <abbr title="Phone">Home :</abbr> <% // $homePhome; %><br/>
                                <abbr title="Phone">Mobile :</abbr> <% // $mobile_number; %><br/>
                            </address>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->

            <% 
                    }
                }


            %>


        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%    dbsessionCommitteeAdd.clear();
        dbsessionCommitteeAdd.close();
        %>


    <%@ include file="../footer.jsp" %>