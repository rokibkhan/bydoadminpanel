<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);

        String committeeIdSel = request.getParameter("committeeId") == null ? "" : request.getParameter("committeeId").trim();

        if (committeeIdSel.equals("")) {

            //redirect committee list
            response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeList.jsp");
        }


    %>



    <script type="text/javascript">

        function deleteCommitteeMemberInfo(arg1, arg2) {
            console.log("deleteCommitteeMemberInfo :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"deleteCommitteeMemberInfoConfirmBtn\" onclick=\"deleteCommitteeMemberInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');
        }

        function deleteCommitteeMemberInfoConfirm(arg1, arg2) {
            console.log("deleteCommitteeMemberInfoConfirm :: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');


            $.post("committeeMemberInfoDeleteProcess.php", {committeeId: arg1, committeeMemberId: arg2}, function (data) {


                console.log(data);

                if (data.responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data.responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data.requestId).html('<h5>' + data.responseMsg + '</h5>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data.responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");
        }

    </script>

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();

        dbtrx = dbsession.beginTransaction();

        int ix = 1;
        String committeeId = "";
        String committeeType = "";
        String committeeName = "";
        String committeeShortName = "";
        String committeeDuration = "";
        String committeeDesc = "";
        String committeeStatus = "";
        Object objCmmt[] = null;

        String status1 = "";
        String btnActivePoll = "";
        String btnCompletePoll = "";
        String committeeMemberBtn = "";
        String agrX = "";

        Query usrSQL = dbsession.createSQLQuery("SELECT c.id_committee, c.committee_type, c.committee_name, "
                + "c.committee_short_name, c.committee_duration, c.committee_desc, c.status, "
                + "c.feature_image,c.com_cat_id  "
                + "FROM  committee c "
                + "WHERE  c.id_committee = '" + committeeIdSel + "'");

        if (!usrSQL.list().isEmpty()) {
            for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                objCmmt = (Object[]) it1.next();
                committeeId = objCmmt[0].toString().trim();
                committeeType = obj[1] == null ? "" : objCmmt[1].toString().trim();
                committeeName = obj[2] == null ? "" : objCmmt[2].toString().trim();
                committeeShortName = objCmmt[3] == null ? "" : objCmmt[3].toString().trim();
                committeeDuration = obj[4] == null ? "" : objCmmt[4].toString().trim();
                //        committeeDesc = obj[5] == null ? "" : objCmmt[5].toString().trim();
                committeeStatus = obj[6] == null ? "" : objCmmt[6].toString().trim();

                agrX = "";

                if (committeeStatus.equals("0")) {
                    status1 = "InActive";
                    btnActivePoll = "";
                    //      btnActivePoll = "&nbsp;<a title=\"Status change to current\" onClick=\"committeeStatusChangeInfo("+agrX+");\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";
                    status1 = status1 + " " + btnActivePoll;
                    committeeMemberBtn = "";
                }

                if (committeeStatus.equals("1")) {
                    status1 = "Active";
                    btnCompletePoll = "&nbsp;<a title=\"Status change to old\" onClick=\"committeeStatusChangeInfo(" + agrX + ");\" class=\"btn btn-primary\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";

                    status1 = status1 + btnCompletePoll;
                    committeeMemberBtn = "<a title=\"Add committee member\"  class=\"btn btn-primary\" href=\""+GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "\"><i class=\"fa fa-plus\"></i> Add Committee Member</a>";
                }

                //    techTeritoryName = obj[5] == null ?"" : obj[5].toString().trim();
                String committeeDetailsUrl = GlobalVariable.baseUrl + "/committeeManagement/committeeDetails.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                String updateProfileUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                String updateChangePassUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=changePass";
            }
        }
    %>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Committee Details</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Committee Management</a></li>                    
                    <li class="active">Details</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Name: <%=committeeName%></h3>
                    <h3 class="box-title m-b-0">Duration: <%=committeeDuration%></h3>
                    <h3><%=committeeMemberBtn%></h3>
                </div>
            </div>
        </div>
        <!-- .row -->
        <div class="row">
            <%
                Object objCmmtMem[] = null;
                String idCommitteeMember = "";
                String memberIEBId = "";
                String memberName = "";
                String committeeDesignationName = "";
                String committeeDepartmentName = "";
                String committeeDepartmentName1 = "";
                String profilePicture = "";
                String profilePictureUrl = "";
                String btnCommitteeMemberEdit = "";
                String btnCommitteeMemberDelete = "";
                String argXn = "";

                Query cmmtMemSQL = dbsession.createSQLQuery("SELECT cm.id_committee_member,m.member_id,m.member_name , cd.designation_name, cdp.department_name "
                        + "FROM  committee_member cm  "
                        + "LEFT JOIN member m ON m.id = cm.member_id   "
                        + "LEFT JOIN committee_designation  cd ON cd.id = cm.committee_designation  "
                        + "LEFT JOIN committee_department  cdp ON cdp.id = cm.committee_department "
                        + "ORDER BY cm.committee_showing_order ASC");

                if (!cmmtMemSQL.list().isEmpty()) {
                    for (Iterator itCM = cmmtMemSQL.list().iterator(); itCM.hasNext();) {

                        objCmmtMem = (Object[]) itCM.next();
                        idCommitteeMember = objCmmtMem[0].toString().trim();
                        memberIEBId = objCmmtMem[1].toString().trim();
                        memberName = objCmmtMem[2].toString().trim();
                        committeeDesignationName = objCmmtMem[3].toString().trim();
                        committeeDepartmentName = objCmmtMem[4] == null ? "" : objCmmtMem[4].toString().trim();
                        
                        if(!committeeDepartmentName.equals("")){
                           committeeDepartmentName1 = "<p>(" + committeeDepartmentName + ")</p>" ;
                        }
                        

                        //   out.println(idCommitteeMember);
                        //    profilePicture = "M_38661.jpg";  
                        profilePicture = "no_image.jpg";
                        profilePictureUrl = GlobalVariable.imageDirLink + profilePicture;

                        argXn = "'" + session.getId() + "','" + idCommitteeMember + "'";

                        btnCommitteeMemberEdit = "<a title=\"Edit\" href=\"" + GlobalVariable.baseUrl +"/committeeManagement/committeeMemberUpdate.php?sessionid=" + session.getId() + "&idCommitteeMember=" + idCommitteeMember + "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil-square-o\"></i></a>";
                        btnCommitteeMemberDelete = "<a onClick=\"deleteCommitteeMemberInfo(" + argXn + ");\" title=\"Delete comittee member\"  class=\"btn btn-primary\"><i class=\"fa fa-trash-o\"></i></a>";


            %>


            <!-- .col -->
            <div class="col-md-4 col-sm-4">
                <div class="white-box" id="infoBox<%=idCommitteeMember%>" style="min-height: 260px; padding: 5px;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                            <a href="contact-detail.html"><img src="<%=profilePictureUrl%>" alt="<%=memberName%>" class="img-circle img-responsive"></a>
                            <h5 class="box-title1 m-b-0" style="line-height1: 20px;"><%=committeeDesignationName%></h5> 
                            <%=committeeDepartmentName1%>

                            <%=btnCommitteeMemberEdit%>
                            <%=btnCommitteeMemberDelete%>

                        </div>
                        <div class="col-md-8 col-sm-8">
                            <h3 class="box-title m-b-0"><%=memberName%></h3> 
                            <small><% //=designationName%></small>

                            <address>
                                795 Folsom Ave, Suite 600 San Francisco, CADGE 94107
                                <br/>
                                <br/>
                                <abbr title="Phone">Office :</abbr> <% // $OfficePhone; %><br/>
                                <abbr title="Phone">Home :</abbr> <% // $homePhome; %><br/>
                                <abbr title="Phone">Mobile :</abbr> <% // $mobile_number; %><br/>
                            </address>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->

            <% 
                    }
                }

                %>


        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->
<%
dbsession.clear();
dbsession.close();
%>



    <%@ include file="../footer.jsp" %>