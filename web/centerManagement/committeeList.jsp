<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showSyUserDetails(arg1, arg2) {
            console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');
        }


        function syUserInfoUpdateOpt(arg1, arg2) {
            console.log("User :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Update User Details");

            $.post("syUserListUserDetails.jsp", {userId: arg1, sessionid: arg2}, function (data) {
//
//
//                console.log(data);
//
//                console.log("invoiceDetails :: " + data[0].invoiceDetails);
//
//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');

        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Manage Committee</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Committee Management</a></li>                    
                    <li class="active">Committees</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center" style="width: 35%;">Name</th>
                                    <th class="text-center">Committee For</th>
                                    <th class="text-center">Duration</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center" style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();
                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String committeeId = "";
                                    String committeeType = "";
                                    String committeeTypeText = "";
                                    String committeeName = "";
                                    String committeeShortName = "";
                                    String committeeComTypeId = "";
                                    String committeeDuration = "";
                                    String committeeDesc = "";
                                    String committeeStatus = "";
                                    Object objCmmt[] = null;

                                    String status1 = "";
                                    String btnActivePoll = "";
                                    String btnCompletePoll = "";
                                    String committeeMemberBtn = "";

                                    String committeeDesignationBtn = "";
                                    String committeeDepartmentBtn = "";
                                    String agrX = "";

                                    Query q1 = null;
                                    Object objcmType[] = null;

                                    String committeeForId = "";
                                    String committeeForShortName = "";
                                    String committeeForName = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT c.id_committee, c.committee_type, c.committee_name, "
                                            + "c.committee_short_name, c.committee_duration, c.committee_desc, c.status, "
                                            + "c.feature_image,c.com_cat_id  "
                                            + "FROM  committee c ORDER BY c.showing_order ASC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            objCmmt = (Object[]) it1.next();
                                            committeeId = objCmmt[0].toString().trim();
                                            committeeType = objCmmt[1] == null ? "" : objCmmt[1].toString().trim();

                                            committeeName = objCmmt[2] == null ? "" : objCmmt[2].toString().trim();
                                            committeeShortName = objCmmt[3] == null ? "" : objCmmt[3].toString().trim();
                                            committeeDuration = objCmmt[4] == null ? "" : objCmmt[4].toString().trim();
                                            //        committeeDesc = objCmmt[5] == null ? "" : objCmmt[5].toString().trim();
                                            committeeStatus = objCmmt[6] == null ? "" : objCmmt[6].toString().trim();

                                            agrX = "";

                                            if (committeeStatus.equals("0")) {
                                                status1 = "InActive";
                                                btnActivePoll = "";
                                                //      btnActivePoll = "&nbsp;<a title=\"Status change to current\" onClick=\"committeeStatusChangeInfo("+agrX+");\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";
                                                status1 = status1 + " " + btnActivePoll;
                                                committeeMemberBtn = "";
                                            }

                                            if (committeeStatus.equals("1")) {
                                                status1 = "Active";
                                                btnCompletePoll = "&nbsp;<a title=\"Status change to old\" onClick=\"committeeStatusChangeInfo(" + agrX + ");\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";

                                                status1 = status1 + btnCompletePoll;
                                                committeeMemberBtn = "<a title=\"Add committee member\"  class=\"btn btn-primary btn-sm  m-b-5\" href=\"committeeMemberAdd.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "\"><i class=\"fa fa-plus\"></i> Add Member</a>";
                                            }

                                            committeeDesignationBtn = "<a title=\"Add committee Designation\"  class=\"btn btn-success btn-sm m-b-5\" href=\"committeeDesignationAdd.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "\"><i class=\"fa fa-plus\"></i> Add Designation</a>";
                                            committeeDepartmentBtn = "<a title=\"Add committee Department\"  class=\"btn btn-info btn-sm m-b-5\" href=\"committeeDepartmentAdd.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "\"><i class=\"fa fa-plus\"></i> Add Department</a>";

                                            String committeeDetailsUrl = GlobalVariable.baseUrl + "/committeeManagement/committeeDetails.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                                            String updateProfileUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                                            String updateChangePassUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=changePass";

                                            committeeComTypeId = objCmmt[8] == null ? "" : objCmmt[8].toString().trim();

                                            if (committeeType.equals("HQ")) {
                                                committeeTypeText = "Head Quarter";
                                            } else {

                                                //division
                                                if (committeeComTypeId.equals("2")) {

                                                    q1 = dbsession.createSQLQuery("select * FROM member_division WHERE mem_division_id = '" + committeeType + "'");
                                                    if (!q1.list().isEmpty()) {
                                                        for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                                            objcmType = (Object[]) divisionItr.next();
                                                            committeeTypeText = objcmType[1].toString();
                                                            
                                                            committeeTypeText = committeeTypeText +" Division";
                                                        }
                                                    } else {
                                                        committeeTypeText = "";
                                                    }

                                                }
                                                //centre
                                                if (committeeComTypeId.equals("3")) {

                                                    q1 = dbsession.createSQLQuery("select * FROM center WHERE center_type_id = '3' AND center_id = '" + committeeType + "'");
                                                    if (!q1.list().isEmpty()) {
                                                        for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                                            objcmType = (Object[]) divisionItr.next();
                                                            committeeTypeText = objcmType[1].toString();
                                                            
                                                            committeeTypeText = committeeTypeText +" Center";
                                                        }
                                                    } else {
                                                        committeeTypeText = "";
                                                    }
                                                }
                                                //sub-centre
                                                if (committeeComTypeId.equals("4")) {
                                                    q1 = dbsession.createSQLQuery("select * FROM center WHERE center_type_id = '4' AND center_id = '" + committeeType + "'");
                                                    if (!q1.list().isEmpty()) {
                                                        for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                                            objcmType = (Object[]) divisionItr.next();
                                                            committeeTypeText = objcmType[1].toString();
                                                            
                                                            committeeTypeText = committeeTypeText +" Sub Center";
                                                        }
                                                    
                                                    } else {
                                                        committeeTypeText = "";
                                                    }
                                                }
                                                //Overseas Chapter  Committee
                                                if (committeeComTypeId.equals("5")) {
                                                    committeeTypeText = "Head Quarter";
                                                }
                                                //Student Chapter  Committee
                                                if (committeeComTypeId.equals("6")) {
                                                    committeeTypeText = "Head Quarter";
                                                }

                                            }


                                %>


                                <tr id="infoBox<%=committeeId%>">
                                    <td><%=ix%></td>
                                    <td><%=committeeName%></td>
                                    <td><%=committeeTypeText%></td>
                                    <td class="text-center"><%=committeeDuration%></td>
                                    <td><%=status1%></td>
                                    <td class="text-left" style="padding-left: 8px; padding-top: 5px; padding-bottom: 5px;">

                                        <%=committeeMemberBtn%>
                                        <%=committeeDesignationBtn%>
                                        <%=committeeDepartmentBtn%>
                                        <br>


                                        <a title="Committee Details" href="<%=committeeDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></a>

                                        <a title="Edit Committee" href="<% //out.print(updateProfileUrl); %>" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a  title="delete committee" class="btn btn-primary btn-sm"><i class="icon-trash"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>



                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->
    <%    dbsession.clear();
        dbsession.close();
        %>



    <%@ include file="../footer.jsp" %>