
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>







<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var divisionContent = $("#divisionContent").val();
        /*
         var syUserPassword = $("#syUserPassword").val();
         var syUserPasswordConfirm = $("#syUserPasswordConfirm").val();
         var syUserFullName = $("#syUserFullName").val();
         var syUserMobile = $("#syUserMobile").val();
         var syUserEmail = $("#syUserEmail").val();
         var syUserDept = $("#syUserDept").val();
         */
        if (divisionContent == null || newsTitle == "") {
            $("#divisionContent").focus();
            $("#divisionContentErr").addClass("help-block with-errors").html("Centre details is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#divisionContentErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        /*
         if (syUserPassword == null || syUserPassword == "") {
         $("#syUserPassword").focus();
         $("#syUserPasswordErr").addClass("help-block with-errors").html("Password is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserPasswordConfirm == null || syUserPasswordConfirm == "") {
         $("#syUserPasswordConfirm").focus();
         $("#syUserPasswordConfirmErr").addClass("help-block with-errors").html("Password confirm is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordConfirmErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserFullName == null || syUserFullName == "") {
         $("#syUserFullName").focus();
         $("#syUserFullNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserFullNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserMobile == null || syUserMobile == "") {
         $("#syUserMobile").focus();
         $("#syUserMobileErr").addClass("help-block with-errors").html("Mobile number is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserMobileErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserEmail == null || syUserEmail == "") {
         $("#syUserEmail").focus();
         $("#syUserEmailErr").addClass("help-block with-errors").html("Email is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserEmailErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         
         if (syUserDept == null || syUserDept == "") {
         $("#syUserDept").focus();
         $("#syUserDeptErr").addClass("help-block with-errors").html("Department is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserDeptErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         */

        return true;
    }


</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";


%>


<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String divisionIdX = request.getParameter("divisionId") == null ? "" : request.getParameter("divisionId").trim();

    Query divisionSQL = null;
    Object divisionObj[] = null;
    String divisionId = "";
    String divisionName = "";
    String divisionShortName = "";
    String divisionSlug = "";
    String divisionShortDetails = "";
    String divisionShortDetails1 = "";
    String divisionShortDetailsX = "";
    String divisionDetails = "";
    String divisionDateTime = "";
    String divisionFeatureImage = "";
    String divisionFeatureImage1 = "";
    String divisionFeatureImageUrl = "";
    String divisionInfoFirstContainer = "";
    String divisionInfoContainer = "";
    String divisionDetailsUrl = "";

    divisionSQL = dbsession.createSQLQuery("select * FROM member_division WHERE mem_division_id = '" + divisionIdX + "'");

    for (Iterator divisionItr = divisionSQL.list().iterator(); divisionItr.hasNext();) {
        divisionObj = (Object[]) divisionItr.next();
        divisionId = divisionObj[0].toString();
        divisionShortName = divisionObj[1].toString();
        divisionName = divisionObj[3].toString();
        divisionDetails = divisionObj[5].toString();
        divisionFeatureImage = divisionObj[6].toString();

        divisionFeatureImageUrl = GlobalVariable.imageDirLink + divisionFeatureImage;

        //   divisionFeatureImageUrl = "<img width=\"200\" src=\"" + featureImageUrl + "\" alt=\"" + newsTitle + "\">";
    }

%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Edit Division Details</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Division Management</a></li>
                    <li class="active">Division Edit</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <form name="addrole" id="addrole" method="post" action="divisionEditSubmitData.jsp?sessionid=<%out.print(sessionid);%>&act=add" onSubmit="return fromDataSubmitValidation()" enctype="multipart/formdata" class="form-horizontal">



                        <input type="hidden" name="divisionId" value="<%=divisionIdX%>">  


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="newsTitle" class="control-label col-md-1">Division</label>
                            <div class="col-md-6">
                                <input type="text" id="divisionName" name="divisionName" class="form-control input-sm" required value="<%=divisionName%>" disabled>
                                <div  id="centerNameErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <link href="<%=cssLinkPmtWysihtml5%>" rel="stylesheet" type="text/css"/>  
                        <div class="form-group">
                            <label for="divisionContent" class="control-label col-md-1">Details</label>
                            <div class="col-md-10">
                                <textarea id="divisionContent" name="divisionContent" placeholder="News Content" class="form-control" cols="10" rows="15" accesskey="">
                                    <%=divisionDetails%>

                                </textarea>
                                <div id="divisionContentErr" class="help-block with-errors"></div>

                                <script src="<%=jsLinkPmtWysihtml51%>"></script>
                                <script src="<%=jsLinkPmtWysihtml52%>"></script>
                                <script type="text/javascript">
                        $(function () {
                            $("#divisionContent").wysihtml5();

                        });
                                </script> 
                            </div>
                        </div>









                        <!--                                    <div class="form-group">
                                                                <label class="container_label">Sticky news
                                                                    <input type="checkbox" name="selected" value="1">
                                                                    <span class="checkmark_label"></span>
                                                                </label>
                                                            </div>-->

                        <style>
                            .container_label {
                                display: block;
                                position: relative;
                                padding-left: 35px;
                                margin-bottom: 12px;
                                cursor: pointer;
                                font-size: 22px;
                                -webkit-user-select: none;
                                -moz-user-select: none;
                                -ms-user-select: none;
                                user-select: none;
                            }

                            /* Hide the browser's default checkbox */
                            .container_label input {
                                position: absolute;
                                opacity: 0;
                                cursor: pointer;
                                height: 0;
                                width: 0;
                            }

                            /* Create a custom checkbox */
                            .checkmark_label {
                                position: absolute;
                                top: 0;
                                left: 0;
                                height: 25px;
                                width: 25px;
                                background-color: #eee;
                            }

                            /* On mouse-over, add a grey background color */
                            .container_label:hover input ~ .checkmark_label {
                                background-color: #ccc;
                            }

                            /* When the checkbox is checked, add a blue background */
                            .container_label input:checked ~ .checkmark_label {
                                background-color: #2196F3;
                            }

                            /* Create the checkmark/indicator (hidden when not checked) */
                            .checkmark_label:after {
                                content: "";
                                position: absolute;
                                display: none;
                            }

                            /* Show the checkmark when checked */
                            .container_label input:checked ~ .checkmark_label:after {
                                display: block;
                            }

                            /* Style the checkmark/indicator */
                            .container_label .checkmark_label:after {
                                left: 9px;
                                top: 5px;
                                width: 5px;
                                height: 10px;
                                border: solid white;
                                border-width: 0 3px 3px 0;
                                -webkit-transform: rotate(45deg);
                                -ms-transform: rotate(45deg);
                                transform: rotate(45deg);
                            }
                        </style>




                        <div class="form-group row">
                            <div class="offset-md-1 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" class="btn btn-info">Update</button>
                            </div>
                        </div>

                    </form>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>


    <%@ include file="../footer.jsp" %>