<%@page import="java.net.InetAddress"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Query q2 = null;
    Object obj[] = null;

    String divisionId = "";
    String divisionShortName = "";
    String divisionName = "";
    String divisionOptionCon = "";
    String divisionInfoCon = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    getRegistryID regId = new getRegistryID();
    String subscribeId = regId.getID(14);
    //memberId: arg2,jobId:arg3

    String sessionId = request.getParameter("sessionId").trim();
    String committeeTypeId = request.getParameter("committeeTypeId").trim();
    String expected_salary = "100000";

    String name = "";
    System.out.println("sessionId " + sessionId);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String adddate = dateFormatX.format(dateX);
    String adduser = "";
    String addterm = InetAddress.getLocalHost().getHostName().toString();
    String addip = InetAddress.getLocalHost().getHostAddress().toString();
    //central
    if (committeeTypeId.equals("1")) {

        //q1 = dbsession.createSQLQuery("SELECT * FROM member_applied_job WHERE member_id = '" + memberId + "' AND job_id='" + jobId + "'");
        responseCode = "1";
        responseMsg = "Error!!! Central Info Not Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                + "<option value=\"HQ\">Head Quarter</option>"
                + "</select>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);
    }//end central
    //division
    if (committeeTypeId.equals("2")) {
        //  q1 = dbsession.createSQLQuery("SELECT * FROM member_applied_job WHERE member_id = '" + memberId + "' AND job_id='" + jobId + "'");
        q1 = dbsession.createSQLQuery("select * FROM member_division ORDER BY mem_division_id ASC");

        for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
            obj = (Object[]) divisionItr.next();
            divisionId = obj[0].toString();
            divisionShortName = obj[1].toString();
            divisionName = obj[3].toString();
            divisionOptionCon = divisionOptionCon + "<option value=\""+ divisionId +"\">" + divisionName + "</option>";
        }

        divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                + "<option>Select Division </option>"
                + "" + divisionOptionCon + ""
                + "</select>";

        responseCode = "1";
        responseMsg = "Success!!! Division Info Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);

    }
    //centre
    if (committeeTypeId.equals("3")) {

        q1 = dbsession.createSQLQuery("select * FROM center WHERE center_type_id = '3' ORDER BY center_name ASC");

        for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
            obj = (Object[]) divisionItr.next();
            divisionId = obj[0].toString();
            divisionName = obj[1].toString();
            divisionOptionCon = divisionOptionCon + "<option value=\""+ divisionId +"\">" + divisionName + "</option>";
        }

        divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                + "<option>Select Centre </option>"
                + "" + divisionOptionCon + ""
                + "</select>";

        responseCode = "1";
        responseMsg = "Success!!! Division Info Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);

    }
    //sub-centre
    if (committeeTypeId.equals("4")) {
        q1 = dbsession.createSQLQuery("select * FROM center WHERE center_type_id = '4' ORDER BY center_name ASC");

        for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
            obj = (Object[]) divisionItr.next();
            divisionId = obj[0].toString();
            divisionName = obj[1].toString();
            divisionOptionCon = divisionOptionCon + "<option value=\""+ divisionId +"\">" + divisionName + "</option>";
        }

        divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                + "<option>Select Centre </option>"
                + "" + divisionOptionCon + ""
                + "</select>";

        responseCode = "1";
        responseMsg = "Success!!! Division Info Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);
    }
    //Overseas Chapter  Committee
    if (committeeTypeId.equals("5")) {
        responseCode = "1";
        responseMsg = "Error!!! Central Info Not Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";
        
        divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                + "<option>Select Overseas Chapter</option>"
                + "</select>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);
    }
    //Student Chapter  Committee
    if (committeeTypeId.equals("6")) {
        responseCode = "1";
        responseMsg = "Error!!! Central Info Not Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";
        
        divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                + "<option>Select Student Chapter</option>"
                + "</select>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);
    }
    //Mohila committee
    if (committeeTypeId.equals("7")) {
        responseCode = "1";
        responseMsg = "Error!!! Central Info Not Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";
        divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                + "<option value=\"HQ\">Head Quarter</option>"
                + "</select>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);
    }
    //Council committee
    if (committeeTypeId.equals("8")) {
        responseCode = "0";
        responseMsg = "Error!!! Central Info Not Found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("responseData", divisionInfoCon);
        json.put("requestType", committeeTypeId);
        jsonArr.add(json);
    }

    //  if (q1.list().isEmpty()) {
//        //   Query q4 = dbsession.createSQLQuery("Insert  member_proposer_info_temp SET status='1',app_rej_date='" + adddate + "' WHERE id='" + requestId + "'");
//        Query q4 = dbsession.createSQLQuery("INSERT INTO member_applied_job(member_id,job_id,expected_salary, apply_date) VALUES (\" " + memberId + "\",\"" + jobId + "\",\"" + expected_salary + "\", now() )");
//        q4.executeUpdate();
//
//        dbtrx.commit();
//        if (dbtrx.wasCommitted()) {
//            responseCode = "1";
//            responseMsg = "Success!!! Job apply complete";
//            responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
//                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
//                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
//                    + "</div>";
//
//        } else {
//            responseCode = "0";
//            responseMsg = "Error!!! When Job apply";
//
//            responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
//                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
//                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i>" + responseMsg + "</span>"
//                    + "</div>";
//        }
//    } else {
//
//        responseCode = "0";
//        responseMsg = "Error!!! Already Applied";
//        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
//                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
//                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
//                + "</div>";
//
//        json.put("responseCode", responseCode);
//        json.put("responseMsg", responseMsg);
//        json.put("responseMsgHtml", responseMsgHtml);
//        json.put("requestId", jobId);
//        jsonArr.add(json);
//    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>