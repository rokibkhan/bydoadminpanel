<%-- 
    Document   : videoSessionInfoShow
    Created on : Nov 14, 2020, 9:35:08 PM
    Author     : rokib
--%>

<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    String videoName = "";

    String subjectId = request.getParameter("subjectId");

    System.out.println("videoSessionInfoShow :: subjectId " + subjectId);
    
        Query subjectTypeSQL = null;
        Object sessionTypeObj[] = null;
        String sessionId = "";
        String sessionName = "";       
        String sessionConInfo = "";

    q1 = dbsession.createSQLQuery("SELECT * FROM session_info where subject_id = '"+subjectId+"' ORDER BY id_session ASC");
    if (!q1.list().isEmpty()) {
        
        /*for (Iterator nameItr = q1.list().iterator(); nameItr.hasNext();) {
            obj = (Object[]) nameItr.next();
            videoName = obj[2].toString();
        }*/
        
        
        for (Iterator committeeTypeItr = q1.list().iterator(); committeeTypeItr.hasNext();) {

                                                        sessionTypeObj = (Object[]) committeeTypeItr.next();
                                                        sessionId = sessionTypeObj[0].toString().trim();
                                                        sessionName = sessionTypeObj[2].toString().trim();
                                                        //committeeTypeShortName = committeeTypeObj[2].toString().trim();

                                                        sessionConInfo = sessionConInfo + "<option value=\"" + sessionId + "\" " + sessionName + ">" + sessionName + "</option>";
                                                    }

        /*centerSQL = dbsession.createSQLQuery("select * FROM center WHERE center_type_id = '3' ORDER BY center_name ASC");

        for (Iterator centerItr = centerSQL.list().iterator(); centerItr.hasNext();) {
            centerObj = (Object[]) centerItr.next();
            centerId = centerObj[0].toString();
            centerName = centerObj[1].toString();

            centerOptionCon = centerOptionCon + "<option value=\"" + centerId + "\">" + centerName + "</option>";
        }*/

        centerInfoCon = "<select  name=\"videoSessionId\" id=\"videoSessionId\" onchange=\"showChapterInfo(this.value)\"  class=\"form-control input-sm customInput-sm\" required>"
                + "<option value=\"\" >Select Session </option>"
                + "" + sessionConInfo + ""
                + "</select>";

        /*mainInfoContainer = "<div class=\"row\">"
                + "<div class=\"col-md-11 offset-md-1\" id=\"centerDivisionTagAllErr\"></div>"
                + "<div class=\"col-md-11 offset-md-1\">"
                + "<div class=\"form-group row custom-bottom-margin-5x\">"
                + "<label class=\"control-label col-md-2\">Video:</label>"
                + "<div class=\"col-md-10\">"
                + " " + videoName + " "
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row custom-bottom-margin-5x\">"
                + "<label class=\"control-label col-md-2\">Center</label>"
                + "<div class=\"col-md-6\">"
                + " " + centerInfoCon + " "
                + "<div  id=\"centerDivisionTagIdErr\" class=\"help-block with-errors\"></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "</div>";*/

        responseCode = "1";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("sessionConInfo", centerInfoCon);
        json.put("requestId", sessionId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! No Subject info found";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> "+ responseMsg +"</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", sessionId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>

