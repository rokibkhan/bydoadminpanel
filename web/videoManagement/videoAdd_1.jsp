
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>







<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {


        var videoTitle = $("#videoTitle").val();

        if (videoTitle == null || videoTitle == "") {
            $("#videoTitle").focus();
            $("#videoTitleErr").addClass("help-block with-errors").html("Page title is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#videoTitleErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        return true;
    }


</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>

<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
//            SyCity sycity = null;
//            SyCountry sycountry = null;

    int rownum = 0;
    String oddeven = null;
    getRegistryID getregid = new getRegistryID();
    String regID = null;
    int regCode = 5;
    regID = getregid.getID(regCode);


%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Video</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Video Management</a></li>
                    <li class="active">Add Video</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <form name="addrole" id="addrole" method="post" action="videoAddSubmitData.jsp?sessionid=<%out.print(sessionid);%>&act=add" onSubmit="return fromDataSubmitValidation()" enctype="multipart/formdata" class="form-horizontal">


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="videoTitle" class="control-label col-sm-2">Title</label>
                            <div class="col-sm-9">
                                <input type="text" id="videoTitle" name="videoTitle"  placeholder="Video Title" class="form-control input-sm" required>
                                <div  id="videoTitleErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="videoTitle" class="control-label col-sm-2">Caption</label>
                            <div class="col-sm-9">
                                <input type="text" id="videoCaption" name="videoCaption"  placeholder="Video Caption" class="form-control input-sm" required>
                                <div  id="videoTitleErr" class="help-block with-errors"></div>
                            </div>
                        </div>


                        

                        <div class="form-group row">
                            <label for="videoCaption" class="control-label col-sm-2">Video Link</label>
                            <div class="col-sm-9">
                                <input type="text" id="videoLink" name="videoLink" placeholder="Video Link" class="form-control"/>
                                <div id="videoLinkErr" class="help-block with-errors"></div>
                            </div>
                        </div>



                        <div class="form-group">

                            <div class="input-file-container">  
                            </div>

                            <!--                                        <label for="video_photo" class="control-label">Select image</label>
                                                                    
                                                                    <input type="file" id="video_photo" >-->
                        </div>


                        <!--                                    <div class="form-group">
                                                                <label class="container_label">Sticky video
                                                                    <input type="checkbox" name="selected" value="1">
                                                                    <span class="checkmark_label"></span>
                                                                </label>
                                                            </div>-->

                        <style>
                            .container_label {
                                display: block;
                                position: relative;
                                padding-left: 35px;
                                margin-bottom: 12px;
                                cursor: pointer;
                                font-size: 22px;
                                -webkit-user-select: none;
                                -moz-user-select: none;
                                -ms-user-select: none;
                                user-select: none;
                            }

                            /* Hide the browser's default checkbox */
                            .container_label input {
                                position: absolute;
                                opacity: 0;
                                cursor: pointer;
                                height: 0;
                                width: 0;
                            }

                            /* Create a custom checkbox */
                            .checkmark_label {
                                position: absolute;
                                top: 0;
                                left: 0;
                                height: 25px;
                                width: 25px;
                                background-color: #eee;
                            }

                            /* On mouse-over, add a grey background color */
                            .container_label:hover input ~ .checkmark_label {
                                background-color: #ccc;
                            }

                            /* When the checkbox is checked, add a blue background */
                            .container_label input:checked ~ .checkmark_label {
                                background-color: #2196F3;
                            }

                            /* Create the checkmark/indicator (hidden when not checked) */
                            .checkmark_label:after {
                                content: "";
                                position: absolute;
                                display: none;
                            }

                            /* Show the checkmark when checked */
                            .container_label input:checked ~ .checkmark_label:after {
                                display: block;
                            }

                            /* Style the checkmark/indicator */
                            .container_label .checkmark_label:after {
                                left: 9px;
                                top: 5px;
                                width: 5px;
                                height: 10px;
                                border: solid white;
                                border-width: 0 3px 3px 0;
                                -webkit-transform: rotate(45deg);
                                -ms-transform: rotate(45deg);
                                transform: rotate(45deg);
                            }
                        </style>





                        <div class="form-group row">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

<%
dbsession.clear();
dbsession.close();
%>

    <%@ include file="../footer.jsp" %>