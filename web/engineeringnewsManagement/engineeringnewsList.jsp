

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">




        function engineeringnewsDeleteInfo(arg1, arg2) {

            console.log("JournalDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Journal Delete confirmation");
            btnInfo = "<a onclick=\"engineeringnewsDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            rupantorLGModal.find("#rupantorLGModalBody").html('');

            //   rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());

            rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo);


//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }


        function engineeringnewsDeleteInfoConfirm(arg1, arg2) {
            console.log("JournalDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');



            $.post("engineeringnewsDeleteProcess.jsp", {engineeringnewsId: arg1, sessionid: arg2}, function (data) {



                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=\"6\" style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Engineering News List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Engineering News Management</a></li>                    
                    <li class="active">Engineering News</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center">Cover Page</th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Details</th>
                                    <th class="text-center">Link</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%

                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();
                                    DateFormat dateFormatJournal = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat dateFormatJournalM = new SimpleDateFormat("MMM");
                                    DateFormat dateFormatJournalD = new SimpleDateFormat("dd");
                                    DateFormat dateFormatJournalY = new SimpleDateFormat("Y");

                                    Date dateJournalM = null;
                                    Date dateJournalD = null;
                                    Date dateJournalY = null;
                                    String newDateJournalM = "";
                                    String newDateJournalD = "";
                                    String newDateJournalY = "";

                                    Query engineeringnewsSQL = null;
                                    Object engineeringnewsObj[] = null;
                                    String engineeringnewsId = "";
                                    String engineeringnewsTitle = "";
                                    String engineeringnewsAuthor = "";
                                    String engineeringnewsDetials = "";
                                    String engineeringnewsDivisionName = "";
                                    String engineeringnewsCoverPage = "";
                                    String engineeringnewsCoverPageUrl = "";
                                    String engineeringnewsCoverPage1 = "";

                                    String engineeringnewsDateTime = "";
                                    String engineeringnewsMonth = "";
                                    String engineeringnewsDay = "";
                                    String engineeringnewsYear = "";

                                    String engineeringnewsDownloadUrl = "";
                                    String engineeringnewsDetailsUrl = "";
                                    String engineeringnewsInfoContainer = "";
                                    int ix = 1;

                                    engineeringnewsSQL = dbsession.createSQLQuery("SELECT jn.id_engineeringnews,jn.engineeringnews_title,jn.engineeringnews_desc,"
                                            + "jn.engineeringnews_link,jn.engineeringnews_date,jn.cover_page_picture "
                                            + "FROM engineeringnews_info jn "
                                            + "WHERE jn.published  = '1' "
                                            + "ORDER BY jn.id_engineeringnews DESC");

                                    System.out.println("SQL:: " + engineeringnewsSQL);

                                    if (!engineeringnewsSQL.list().isEmpty()) {
                                        for (Iterator itJournal = engineeringnewsSQL.list().iterator(); itJournal.hasNext();) {

                                            engineeringnewsObj = (Object[]) itJournal.next();
                                            engineeringnewsId = engineeringnewsObj[0].toString().trim();
                                            engineeringnewsTitle = engineeringnewsObj[1].toString().trim();

                                            engineeringnewsDetials = engineeringnewsObj[2].toString().trim();

                                            engineeringnewsDateTime = engineeringnewsObj[4].toString().trim();

                                            dateJournalM = dateFormatJournal.parse(engineeringnewsDateTime);
                                            newDateJournalM = dateFormatJournalM.format(dateJournalM);

                                            dateJournalD = dateFormatJournal.parse(engineeringnewsDateTime);
                                            newDateJournalD = dateFormatJournalD.format(dateJournalD);

                                            dateJournalY = dateFormatJournal.parse(engineeringnewsDateTime);
                                            newDateJournalY = dateFormatJournalY.format(dateJournalY);

                                            engineeringnewsDownloadUrl = engineeringnewsObj[3].toString().trim();

                                            engineeringnewsDetailsUrl = GlobalVariable.baseUrl + "/engineeringnews/engineeringnewsDetails.jsp?engineeringnewsIdX=" + engineeringnewsId;

                                            engineeringnewsCoverPage = engineeringnewsObj[5].toString().trim();

                                           // engineeringnewsCoverPageUrl = GlobalVariable.engineeringnewsDirLink + engineeringnewsCoverPage;
                                            engineeringnewsCoverPageUrl = GlobalVariable.imageDirLink +"/engineeringnews/" + engineeringnewsCoverPage;

                                            engineeringnewsCoverPage1 = "<img width=\"150\" src=\"" + engineeringnewsCoverPageUrl + "\" alt=\"" + engineeringnewsTitle + "\">";


                                %>


                                <tr id="infoBox<%=engineeringnewsId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><%=engineeringnewsCoverPage1%></td>
                                    <td><% out.print(engineeringnewsTitle);%></td>
                                    <td><% out.print(engineeringnewsDetials);%></td>
                                    <td><a href="<%=engineeringnewsDownloadUrl%>">Download</a></td>
                                    <td class="text-center">

                                        <a onclick="newsEditInfo(<% out.print("'" + engineeringnewsId + "','" + sessionid + "'");%>)" href="engineeringnewsEdit.jsp?sessionid=<%=sessionid%>&engineeringnewsid=<%=engineeringnewsId%>" title="Edit engineeringnews" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onclick="engineeringnewsDeleteInfo(<% out.print("'" + engineeringnewsId + "','" + sessionid + "'");%>)" title="Delete this engineeringnews" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%    dbsession.clear();
        dbsession.close();
    %>


    <%@ include file="../footer.jsp" %>