<%@page import="com.appul.entity.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>


<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">





<script type="text/javascript">


    function showDistrictWiseThanaInfo(arg1, arg2) {
        console.log("agr1:: " + arg1);
        console.log("arg2:: " + arg2);

        $.post("memberEditDistrictWiseThanaInfoShow.jsp", {districtId: arg1, addressInfo: arg2}, function (data) {

            if (data[0].responseCode == 1) {
                $("#showThanaInfo" + data[0].addressType).html(data[0].thanaInfo);
            }

        }, "json");

    }


    function educationDeleteInfo(arg1, arg2) {

        console.log("educationDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"educationDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Education Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function educationDeleteInfoConfirm(arg1, arg2) {
        console.log("educationDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("educationDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#eduInfoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }


    function professionDeleteInfo(arg1, arg2) {

        console.log("professionDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"professionDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Profession Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function professionDeleteInfoConfirm(arg1, arg2) {
        console.log("professionDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("professionDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#proInfoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }



    function memberFeeDeleteInfo(arg1, arg2) {

        console.log("memberFeeDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"memberFeeDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Member Payment Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function memberFeeDeleteInfoConfirm(arg1, arg2) {
        console.log("memberFeeDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("memberFeeDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#memberFeeInfoBox" + data[0].selectInvId).html("<td colspan=\"10\" style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }


    function proposerDeleteInfo(arg1, arg2) {

        console.log("proposerDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"proposerDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Proposer Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function proposerDeleteInfoConfirm(arg1, arg2) {
        console.log("proposerDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("proposerDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#proposerInfoBox" + data[0].selectInvId).html("<td colspan=\"7\" style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }

    function documentDeleteInfo(arg1, arg2) {

        console.log("documentDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"documentDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Documents Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function documentDeleteInfoConfirm(arg1, arg2) {
        console.log("documentDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("documentDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#documentInfoBox" + data[0].selectInvId).html("<td colspan=\"4\" style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }


    function showDivisionWisSubDivisionInfo(arg1) {
        console.log("showDivisionWisSubDivisionInfo agr1:: " + arg1);

        $.post("memberEditDivisionWisSubDivisionInfoShow.jsp", {divisionId: arg1}, function (data) {

            if (data[0].responseCode == 1) {
                $("#showSubDivision").html(data[0].subDivisionInfo);
            }

        }, "json");

    }


</script>

<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    String memberIdH = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

    System.out.println("memberIdH :: " + memberIdH);

    String tabProfileActive = "";
    String tabAddressActive = "";
    String tabEducationActive = "";
    String tabProfessionalActive = "";
    String tabPaymentActive = "";
    String tabRecomendationActive = "";
    String tabDocumentActive = "";
    String tabChangePictureActive = "";
    String tabChangePassActive = "";
    String tabCenterDivisionActive = "";

    String selectedTab = request.getParameter("selectedTab") == null ? "" : request.getParameter("selectedTab").trim();
    if (!selectedTab.equals("")) {

        if (selectedTab.equals("profile")) {
            tabProfileActive = "active";
        } else if (selectedTab.equals("address")) {
            tabAddressActive = "active";
        } else if (selectedTab.equals("education")) {
            tabEducationActive = "active";
        } else if (selectedTab.equals("professional")) {
            tabProfessionalActive = "active";
        } else if (selectedTab.equals("payment")) {
            tabPaymentActive = "active";
        } else if (selectedTab.equals("recomendation")) {
            tabRecomendationActive = "active";
        } else if (selectedTab.equals("document")) {
            tabDocumentActive = "active";
        } else if (selectedTab.equals("changePicture")) {
            tabChangePictureActive = "active";
            StringBuffer expurl = request.getRequestURL().append('?').append(request.getQueryString()).append("&pageLoad=2");

            //   int pageLoadnX = 1;
            String pageLoadnX = request.getParameter("pageLoad") == null ? "" : request.getParameter("pageLoad").trim();

//            System.out.println("expurl" + expurl);
//            if (!pageLoadnX.equals("2")) {
//                out.print("<script type=\"text/javascript\">");
//                out.print("window.location.href=\" " + expurl + "\";");
//                out.print("</script>");
//            }
        } else if (selectedTab.equals("centerDivision")) {
            tabCenterDivisionActive = "active";
        } else {
            tabProfileActive = "active";
        }

    } else {
        tabProfileActive = "active";
    }

    Object memberObj[] = null;
    Query memberSQL = null;

    String memberIEBId = "";
    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String checkMale = "";
    String checkFemale = "";
    String mobileNo = "";
    String userEmail = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    int centerId = 0;
    String centerName = "";

    int subDivisionId = 0;
    String subDivisionName = "";

    int divisionId = 0;
    String divisionName = "";

    String pictureName = "";
    String pictureLink = "";

    Member member = null;
    AddressBook addressBook = null;

    memberSQL = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

    Object[] object = null;

    for (Iterator itr = memberSQL.list().iterator(); itr.hasNext();) {
        member = (Member) itr.next();

        memberIEBId = member.getMemberId();
        memberName = member.getMemberName();
        fatherName = member.getFatherName() == null ? "" : member.getFatherName();
        motherName = member.getMotherName() == null ? "" : member.getMotherName();
        placeOfBirth = member.getPlaceOfBirth() == null ? "" : member.getPlaceOfBirth();
        dob = member.getDob();
        gender = member.getGender().trim();
        if (gender.equals("1")) {
            gender = "Male";
            checkMale = " checked";
        } else {
            gender = "Female";
            checkFemale = " checked";
        }
        mobileNo = member.getMobile();
        phone1 = member.getPhone1();
        phone2 = member.getPhone2();
        userEmail = member.getEmailId();
        bloodGroup = member.getBloodGroup();

        pictureName = member.getPictureName().toString();

        pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

        centerId = member.getCenter().getCenterId();
        centerName = member.getCenter().getCenterName();

        subDivisionId = member.getSubDivision().getSubDivisionId();
        subDivisionName = member.getSubDivision().getSubDivisionName();

        divisionId = member.getSubDivision().getMemberDivision().getMemDivisionId();
        //  divisionName = member.getSubDivision().getMemberDivision().getMemDivisionName();
        divisionName = member.getSubDivision().getMemberDivision().getFullName();
    }


%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Member Edit</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Member Edit</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">

                <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <ul class="nav customtab nav-tabs" role="tablist">                            
                            <li role="presentation" class="nav-item"><a href="#profile" class="nav-link <% out.print(tabProfileActive); %>" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Personal</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#address" class="nav-link <% out.print(tabAddressActive); %>" aria-controls="address" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Address</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#education" class="nav-link <% out.print(tabEducationActive); %>" aria-controls="education" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Education</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#professional" class="nav-link <% out.print(tabProfessionalActive); %>" aria-controls="professional" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Professional</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#payment" class="nav-link <% out.print(tabPaymentActive); %>" aria-controls="payment" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Payment</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#recomendation" class="nav-link <% out.print(tabRecomendationActive); %>" aria-controls="recomendation" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Recommendation</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#document" class="nav-link <% out.print(tabDocumentActive); %>" aria-controls="document" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Documents</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#changePicture" class="nav-link <% out.print(tabChangePictureActive); %>" aria-controls="changePicture" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Change Picture</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#centerDivision" class="nav-link <% out.print(tabCenterDivisionActive); %>" aria-controls="centerDivision" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Center Division</span></a></li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <% out.print(tabProfileActive);%>" id="profile">
                                <h4 class="font-bold1 m-t-30">Personal Information</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberEditPersonalInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">

                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Member ID</label>
                                                <div class="col-sm-9">

                                                    <input  value="<%=memberIEBId%>" type="text" class="form-control" disabled="">                                    
                                                </div>
                                            </div>

                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Name</label>
                                                <div class="col-sm-9">
                                                    <input name="memberName" id="memberName" value="<%=memberName%>" type="hidden"> 
                                                    <input  value="<%=memberName%>" placeholder="Full name" type="text" class="form-control" disabled="">                                    
                                                </div>
                                            </div>
                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Father's Name</label>
                                                <div class="col-sm-9">
                                                    <input name="fatherName" id="fatherName" value="<%=fatherName%>" placeholder="Father's name" type="text" class="form-control" required="">                                    
                                                </div>
                                            </div>

                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mother's Name</label>
                                                <div class="col-sm-9">
                                                    <input name="motherName" id="motherName" value="<%=motherName%>" placeholder="Mother's name" type="text" class="form-control" required="">                                    
                                                </div>
                                            </div>


                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Email</label>
                                                <div class="col-sm-9">
                                                    <input name="memberEmail" id="memberEmail" value="<%=userEmail%>" placeholder="Email address" type="text" class="form-control" required="">                                    
                                                </div>                                    
                                            </div>

                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mobile</label>
                                                <div class="col-sm-9">
                                                    <input name="memberMobile" id="memberMobile" value="<%=mobileNo%>" placeholder="Mobile number" type="text" class="form-control" required="">                                    
                                                </div>                                     
                                            </div>
                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Blood Group</label>
                                                <div class="col-sm-9">
                                                    <%//=bloodGroup%>
                                                    <select name="memberBloodGroup" id="memberBloodGroup" class="form-control" required="">
                                                        <option value="">Select Blood Group</option>
                                                        <%
                                                            String blood1 = "";
                                                            String blood2 = "";
                                                            String blood3 = "";
                                                            String blood4 = "";
                                                            String blood5 = "";
                                                            String blood6 = "";
                                                            String blood7 = "";
                                                            String blood8 = "";

                                                            if (bloodGroup.equals("O+")) {
                                                                blood1 = " selected";
                                                            }
                                                            if (bloodGroup.equals("O-")) {
                                                                blood2 = " selected";
                                                            }
                                                            if (bloodGroup.equals("A+")) {
                                                                blood3 = " selected";
                                                            }
                                                            if (bloodGroup.equals("A-")) {
                                                                blood4 = " selected";
                                                            }
                                                            if (bloodGroup.equals("B+")) {
                                                                blood5 = " selected";
                                                            }
                                                            if (bloodGroup.equals("B-")) {
                                                                blood6 = " selected";
                                                            }
                                                            if (bloodGroup.equals("AB+")) {
                                                                blood7 = " selected";
                                                            }
                                                            if (bloodGroup.equals("AB-")) {
                                                                blood8 = " selected";
                                                            }

                                                        %>
                                                        <option value="O+" <%=blood1%>>O+</option>
                                                        <option value="O-" <%=blood2%>>O-</option>
                                                        <option value="A+" <%=blood3%>>A+</option>
                                                        <option value="A-" <%=blood4%>>A-</option>
                                                        <option value="B+" <%=blood5%>>B+</option>
                                                        <option value="B-" <%=blood6%>>B-</option>
                                                        <option value="AB+" <%=blood7%>>AB+</option>
                                                        <option value="AB-" <%=blood8%>>AB-</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Gender</label>
                                                <div class="col-sm-9">

                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                            <input id="customRadioInline1" name="memberGender" value="1" type="radio" class="form-check-input" <%=checkMale%>>Male
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input id="customRadioInline1" name="memberGender" value="2" type="radio" class="form-check-input" <%=checkFemale%>>Female
                                                        </label>
                                                    </div>                                    
                                                </div>
                                            </div>

                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Date of Birth</label>
                                                <div class="col-sm-6">
                                                    <input name="memberDOB" id="memberDOB" value="<%=dob%>" placeholder="Date of Birth(YYYY-MM-DD)" type="text" class="form-control" required="">                                    
                                                </div>                                     
                                            </div>
                                            <div class="form-group d-flex align-items-center">
                                                <label class="form-control-label col-sm-3 ml-3 font-weight-bold">&nbsp;</label>
                                                <div class="col-sm-9">
                                                    <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                    <button type="submit" class="btn btn-primary btn-sm ">Update</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /#profile -->

                            <div class="tab-pane <% out.print(tabAddressActive); %>" id="address">


                                <%
                                    Query mAddressSQL = null;
                                    Object mAddressObj[] = null;

                                    String mAddressId = "";
                                    String mCountryId = "";
                                    int mDistrictId = 0;
                                    String mThanaId = "";
                                    String mThanaName = "";
                                    String mThanaDistrictName = "";
                                    String mAddressLine1 = "";
                                    String mAddressLine2 = "";
                                    String mZipOffice = "";
                                    String mZipCode = "";
                                    String mAddressStr = "";
                                    String mThanaOptStr = "";
                                    String mAddressThanaSelect = "";

                                    Query pAddressSQL = null;
                                    Object pAddressObj[] = null;
                                    String pAddressId = "";
                                    String pCountryId = "";
                                    int pDistrictId = 0;
                                    String pThanaId = "";
                                    String pThanaName = "";
                                    String pThanaDistrictName = "";
                                    String pAddressLine1 = "";
                                    String pAddressLine2 = "";
                                    String pZipOffice = "";
                                    String pZipCode = "";
                                    String pAddressStr = "";
                                    String pThanaOptStr = "";
                                    String pAddressThanaSelect = "";

                                    Thana thana = null;

                                    mAddressSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address where member_id= " + memberIdH + " and address_Type='M' ) ");
                                    for (Iterator mAddressItr = mAddressSQL.list().iterator(); mAddressItr.hasNext();) {
                                        mAddressObj = (Object[]) mAddressItr.next();
                                        mAddressId = mAddressObj[0].toString();
                                        mAddressLine1 = mAddressObj[1] == null ? "" : mAddressObj[1].toString();
                                        mAddressLine2 = mAddressObj[2] == null ? "" : mAddressObj[2].toString();
                                        mThanaId = mAddressObj[3] == null ? "" : mAddressObj[3].toString();

                                        if (!mThanaId.equals("")) {
                                            Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                            for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
                                                thana = (Thana) itrmThana.next();

                                                mThanaName = thana.getThanaName();
                                                mDistrictId = thana.getDistrict().getId();
                                                mThanaDistrictName = thana.getDistrict().getDistrictName();
                                            }

                                            mThanaOptStr = "<option value=\"" + mThanaId + "\">" + mThanaName + "</option>";
                                        }

                                        System.out.println("mDistrictId::" + mDistrictId);
                                        System.out.println("mThanaDistrictName::" + mThanaDistrictName);
                                        System.out.println("mThanaId::" + mThanaId);
                                        System.out.println("mThanaOptStr::" + mThanaOptStr);

                                        mZipCode = mAddressObj[4] == null ? "" : mAddressObj[4].toString();
                                        mZipOffice = mAddressObj[8] == null ? "" : mAddressObj[8].toString();
                                    }

                                    mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

                                    pAddressSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address where member_id= " + memberIdH + " and address_Type='P' ) ");
                                    for (Iterator pAddressItr = pAddressSQL.list().iterator(); pAddressItr.hasNext();) {

                                        pAddressObj = (Object[]) pAddressItr.next();
                                        pAddressId = pAddressObj[0].toString();
                                        pAddressLine1 = pAddressObj[1] == null ? "" : pAddressObj[1].toString();
                                        pAddressLine2 = pAddressObj[2] == null ? "" : pAddressObj[2].toString();

                                        pThanaId = pAddressObj[3] == null ? "" : pAddressObj[3].toString();

                                        if (!pThanaId.equals("")) {

                                            Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + pThanaId + "'");
                                            for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
                                                thana = (Thana) itrpThana.next();

                                                pThanaName = thana.getThanaName();
                                                pDistrictId = thana.getDistrict().getId();
                                                pThanaDistrictName = thana.getDistrict().getDistrictName();
                                            }

                                            pThanaOptStr = "<option value=\"" + pThanaId + "\">" + pThanaName + "</option>";
                                        }

                                        System.out.println("pDistrictId::" + pDistrictId);
                                        System.out.println("pThanaId::" + pThanaId);
                                        System.out.println("pThanaOptStr::" + pThanaOptStr);

                                        pZipCode = pAddressObj[4] == null ? "" : pAddressObj[4].toString();
                                        pZipOffice = pAddressObj[8] == null ? "" : pAddressObj[8].toString();

                                    }

                                    pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;
                                %>

                                <div class="row">                                      
                                    <div class="col-md-6">
                                        <div class="row">                                      
                                            <div class="col-md-12">
                                                <h4 class="font-bold1 m-t-30">Mailing Address</h4>
                                                <hr>
                                                <p><%=mAddressStr%></p>
                                            </div>                                                
                                        </div> 
                                        <div class="row">                                      
                                            <div class="col-md-10">
                                                <h4 class="font-bold1 m-t-30">Change Mailing Address Info</h4>
                                                <hr>

                                                <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberEditMailingAddressSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">


                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-3 text-right" for="inputName">Country *</label>
                                                        <div class="col-7">
                                                            <select  id="mAddressCountry" name="mAddressCountry" class="form-control input-sm customInput-sm" required>
                                                                <option value="">Select Country</option>
                                                                <%
                                                                    Query countryMASQL = null;
                                                                    Object[] objectMA = null;
                                                                    String countryCodeMA = "";
                                                                    String countryNameMA = "";
                                                                    String selectedMA = "";
                                                                    countryMASQL = dbsession.createSQLQuery("SELECT * FROM sy_country ORDER BY COUNTRY_NAME ASC");
                                                                    for (Iterator itrMA = countryMASQL.list().iterator(); itrMA.hasNext();) {
                                                                        objectMA = (Object[]) itrMA.next();
                                                                        countryCodeMA = objectMA[0].toString();
                                                                        countryNameMA = objectMA[1].toString();
                                                                        if (countryCodeMA.equals("BD")) {
                                                                            selectedMA = " selected";
                                                                        } else {
                                                                            selectedMA = "";
                                                                        }


                                                                %> 
                                                                <option value=" <%=countryCodeMA%> " <%=selectedMA%>> <%=countryNameMA%> </option> 
                                                                <%
                                                                    }

                                                                %>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-3 text-right" for="inputName">District *</label>
                                                        <div class="col-7">
                                                            <select  id="mAddressDistrict" name="mAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'MA')" class="form-control input-sm customInput-sm" required>
                                                                <option value="">Select District</option>
                                                                <%                                                                    System.out.println("mDistrictId::" + mDistrictId);
                                                                    System.out.println("mThanaDistrictName::" + mThanaDistrictName);
                                                                    System.out.println("mThanaId::" + mThanaId);
                                                                    System.out.println("mThanaOptStr::" + mThanaOptStr);

                                                                    Query districtMASQL = null;
                                                                    Object[] objectMAd = null;
                                                                    String districtIdMA = "";
                                                                    String districtNameMA = "";
                                                                    String districtMA = "";
                                                                    districtMASQL = dbsession.createSQLQuery("SELECT * FROM district ORDER BY DISTRICT_NAME ASC");
                                                                    for (Iterator itrMAd = districtMASQL.list().iterator(); itrMAd.hasNext();) {
                                                                        objectMAd = (Object[]) itrMAd.next();
                                                                        districtIdMA = objectMAd[0].toString();
                                                                        districtNameMA = objectMAd[1].toString();

                                                                        if (districtIdMA.equals(Integer.toString(mDistrictId))) {
                                                                            districtMA = " selected";
                                                                        } else {
                                                                            districtMA = "";
                                                                        }


                                                                %> 
                                                                <option value="<%=districtIdMA%>" <%=districtMA%>> <%=districtNameMA%> </option> 
                                                                <%
                                                                    }
                                                                %>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-3 text-right" for="inputName">Thana *</label>
                                                        <div class="col-7" id="showThanaInfoMA">                                                    
                                                            <select  id="mAddressThana" name="mAddressThana" class="form-control input-sm customInput-sm" required>

                                                                <%=mThanaOptStr%>
                                                            </select>
                                                        </div>
                                                    </div>



                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Address Line1</label>
                                                        <div class="col-sm-9">
                                                            <input name="mAddressLine1" id="mAddressLine1" value="<%=mAddressLine1%>" placeholder="Address Line1" type="text" class="form-control">                                    
                                                        </div>
                                                    </div>

                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Address Line2</label>
                                                        <div class="col-sm-9">
                                                            <input name="mAddressLine2" id="mAddressLine2" value="<%=mAddressLine2%>" placeholder="Address Line2" type="text" class="form-control">                                    
                                                        </div>                                    
                                                    </div>

                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-md-3 text-right" for="inputName">Post *</label>
                                                        <div class="col-md-4">
                                                            <input name="mAddressPostOffice" id="mAddressPostOffice" value="<%=mZipOffice%>" type="text" class="form-control" placeholder="Post Office" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="mAddressPostCode" id="mAddressPostCode" value="<%=mZipCode%>" type="text" class="form-control" placeholder="Post Code" required>
                                                        </div>
                                                    </div>



                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-sm-3 ml-3 font-weight-bold">&nbsp;</label>
                                                        <div class="col-sm-9">
                                                            <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                            <input name="mAddressId" id="mAddressId" value="<%=mAddressId%>"  type="hidden">                                    
                                                            <button type="submit" class="btn btn-primary btn-sm ">Update</button>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>                                                
                                        </div>
                                        <!-- /#row Mailing Address -->
                                    </div>                                      
                                    <div class="col-md-6">
                                        <div class="row">                                      
                                            <div class="col-md-12">
                                                <h4 class="font-bold1 m-t-30">Permanent Address</h4>
                                                <hr>
                                                <p><%=pAddressStr%></p>
                                            </div>                                                
                                        </div> 
                                        <div class="row">                                      
                                            <div class="col-md-12">
                                                <h4 class="font-bold1 m-t-30">Change Permanent Address Info</h4>
                                                <hr>
                                                <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberEditPermanentAddressSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">


                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-3 text-right" for="inputName">Country *</label>
                                                        <div class="col-7">
                                                            <select  id="pAddressCountry" name="pAddressCountry" class="form-control input-sm customInput-sm" required>
                                                                <option value="">Select Country</option>
                                                                <%
                                                                    Query countryPASQL = null;
                                                                    Object[] objectPA = null;
                                                                    String countryCodePA = "";
                                                                    String countryNamePA = "";
                                                                    String selectedPA = "";
                                                                    countryPASQL = dbsession.createSQLQuery("SELECT * FROM sy_country ORDER BY COUNTRY_NAME ASC");
                                                                    for (Iterator itrPA = countryPASQL.list().iterator(); itrPA.hasNext();) {
                                                                        objectPA = (Object[]) itrPA.next();
                                                                        countryCodePA = objectPA[0].toString();
                                                                        countryNamePA = objectPA[1].toString();
                                                                        if (countryCodePA.equals("BD")) {
                                                                            selectedPA = " selected";
                                                                        } else {
                                                                            selectedPA = "";
                                                                        }


                                                                %> 
                                                                <option value=" <%=countryCodePA%> " <%=selectedPA%>> <%=countryNamePA%> </option> 
                                                                <%
                                                                    }

                                                                %>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-3 text-right" for="inputName">District *</label>
                                                        <div class="col-7">
                                                            <select  id="pAddressDistrict" name="pAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'PA')" class="form-control input-sm customInput-sm" required>
                                                                <option value="">Select District</option>
                                                                <%                                                                    System.out.println("pDistrictId::" + pDistrictId);
                                                                    System.out.println("pThanaDistrictName::" + pThanaDistrictName);
                                                                    System.out.println("pThanaId::" + pThanaId);
                                                                    System.out.println("pThanaOptStr::" + pThanaOptStr);

                                                                    Query districtPASQL = null;
                                                                    Object[] objectPAd = null;
                                                                    String districtIdPA = "";
                                                                    String districtNamePA = "";
                                                                    String districtPA = "";
                                                                    districtPASQL = dbsession.createSQLQuery("SELECT * FROM district ORDER BY DISTRICT_NAME ASC");
                                                                    for (Iterator itrPAd = districtPASQL.list().iterator(); itrPAd.hasNext();) {
                                                                        objectPAd = (Object[]) itrPAd.next();
                                                                        districtIdPA = objectPAd[0].toString();
                                                                        districtNamePA = objectPAd[1].toString();

                                                                        if (districtIdPA.equals(Integer.toString(pDistrictId))) {
                                                                            districtPA = " selected";
                                                                        } else {
                                                                            districtPA = "";
                                                                        }


                                                                %> 
                                                                <option value="<%=districtIdPA%>" <%=districtPA%>> <%=districtNamePA%> </option> 
                                                                <%
                                                                    }
                                                                %>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-3 text-right" for="inputName">Thana *</label>
                                                        <div class="col-7" id="showThanaInfoPA">                                                    
                                                            <select  id="pAddressThana" name="pAddressThana" class="form-control input-sm customInput-sm" required>

                                                                <%=pThanaOptStr%>
                                                            </select>
                                                        </div>
                                                    </div>



                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Address Line1</label>
                                                        <div class="col-sm-9">
                                                            <input name="pAddressLine1" id="pAddressLine1" value="<%=pAddressLine1%>" placeholder="Address Line1" type="text" class="form-control">                                    
                                                        </div>
                                                    </div>

                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Address Line2</label>
                                                        <div class="col-sm-9">
                                                            <input name="pAddressLine2" id="pAddressLine2" value="<%=pAddressLine2%>" placeholder="Address Line2" type="text" class="form-control">                                    
                                                        </div>                                    
                                                    </div>

                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-md-3 text-right" for="inputName">Post *</label>
                                                        <div class="col-md-4">
                                                            <input name="pAddressPostOffice" id="pAddressPostOffice" value="<%=pZipOffice%>" type="text" class="form-control" placeholder="Post Office" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input name="pAddressPostCode" id="pAddressPostCode" value="<%=pZipCode%>" type="text" class="form-control" placeholder="Post Code" required>
                                                        </div>
                                                    </div>



                                                    <div class="form-group d-flex align-items-center">
                                                        <label class="form-control-label col-sm-3 ml-3 font-weight-bold">&nbsp;</label>
                                                        <div class="col-sm-9">
                                                            <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                            <input name="pAddressId" id="pAddressId" value="<%=pAddressId%>"  type="hidden">                                    
                                                            <button type="submit" class="btn btn-primary btn-sm ">Update</button>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>                                                
                                        </div>
                                        <!-- /#row Permanent Address -->
                                    </div>                                                   
                                </div>


                            </div>
                            <!-- /#address -->
                            <div class="tab-pane <% out.print(tabEducationActive); %>" id="education">

                                <h4 class="font-bold1 m-t-30">Education Info</h4>
                                <hr>
                                <%
                                    Query educationSQL = null;
                                    int degreeId = 0;
                                    String degreeName = "";
                                    String instituteName = "";
                                    String boardUniversityName = "";
                                    String yearOfPassing = "";
                                    String resultTypeName = "";
                                    String result = "";

                                    MemberEducationInfo mei = null;

                                    educationSQL = dbsession.createQuery("from MemberEducationInfo where  member_id=" + memberIdH + " ");

                                    String memberSubdivisionName = "";
                                    String memberSubdivisionFullName = "";
                                    int educationfInfoId = 0;

                                %>

                                <div class="row">                                      
                                    <div class="col-md-12">

                                        <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Degree</th>
                                                    <th scope="col">Board/University</th>
                                                    <th scope="col">Year of Passing</th>
                                                    <th scope="col">Score/Class</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%                                                    for (Iterator itr = educationSQL.list().iterator(); itr.hasNext();) {
                                                        mei = (MemberEducationInfo) itr.next();

                                                        educationfInfoId = mei.getId();
                                                        degreeId = mei.getDegree().getDegreeId();
                                                        degreeName = mei.getDegree().getDegreeName();

                                                        instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                        boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                        yearOfPassing = mei.getYearOfPassing();
                                                        resultTypeName = mei.getResultType().getResultTypeName();
                                                        result = mei.getResult();

                                                        if (degreeId == 3 || degreeId == 4) {

                                                            Query q2 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

                                                            for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                                                member = (Member) itr2.next();

                                                                memberSubdivisionName = member.getSubDivision().getSubDivisionName();
                                                                memberSubdivisionFullName = member.getSubDivision().getFullName();
                                                            }
                                                        } else {
                                                            memberSubdivisionFullName = "";
                                                        }

                                                        degreeName = degreeName + memberSubdivisionFullName;
                                                %>
                                                <tr id="eduInfoBox<%=educationfInfoId%>">
                                                    <td><%=degreeName%></td>
                                                    <td><%=boardUniversityName%></td>
                                                    <td><%=yearOfPassing%></td>
                                                    <td><%=result%></td>
                                                    <td style="cursor:pointer">


                                                        <a title="Remove" onclick="educationDeleteInfo(<% out.print("'" + educationfInfoId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 


                                                    </td>

                                                </tr>
                                                <%
                                                    }


                                                %>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>



                                <!-- ============ End Education information Table ================== -->

                                <div class="container">
                                    <div class="row my-4">
                                        <div class="col-12">
                                            <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                                <i class="fa fa-list-alt"></i> Add Education Information
                                            </div>
                                        </div>

                                    </div>
                                </div>



                                <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberEditEducationInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">

                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Degree :</label>
                                        <div class="col-sm-6">
                                            <select name="memberDegreeTypeId" id="memberDegreeTypeId" class="form-control" required="">
                                                <option value="">Select Degree</option>
                                                <%
                                                    Object[] degreeObj = null;
                                                    String degreeIdX = "";
                                                    String degreeNameX = "";
                                                    Query degreeSQL = dbsession.createSQLQuery("select * FROM degree ORDER BY degree_id DESC");
                                                    for (Iterator itr5 = degreeSQL.list().iterator(); itr5.hasNext();) {
                                                        degreeObj = (Object[]) itr5.next();
                                                        degreeIdX = degreeObj[0].toString();
                                                        degreeNameX = degreeObj[1].toString();
                                                %>
                                                <option value="<%=degreeIdX%>"><%=degreeNameX%></option>

                                                <%
                                                    }

                                                %>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Institution :</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="institutionName" name="institutionName" placeholder="Institution" required="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Board/University :</label>
                                        <div class="col-sm-6">
                                            <select name="memberBoardUniversity" id="memberBoardUniversity" class="form-control chosen" required="">
                                                <option value="">Select Board/University</option>
                                                <%                                                    Object[] universityObj = null;
                                                    String universityId = "";
                                                    String universityShortName = "";
                                                    String universityLongName = "";
                                                    String universityName = "";
                                                    String degreeNameX1 = "";
                                                    Query universitySQL = dbsession.createSQLQuery("select * FROM university ORDER BY university_short_name ASC");
                                                    for (Iterator itr6 = universitySQL.list().iterator(); itr6.hasNext();) {
                                                        universityObj = (Object[]) itr6.next();
                                                        universityId = universityObj[0].toString();
                                                        universityShortName = universityObj[1].toString();
                                                        universityLongName = universityObj[2].toString() == null ? "" : universityObj[2].toString();

                                                        if (universityLongName.equals("")) {
                                                            universityName = universityShortName;
                                                        } else {
                                                            universityName = universityLongName + "(" + universityShortName + ")";
                                                        }

                                                %>
                                                <option value="<%=universityId%>"><%=universityName%></option>

                                                <%
                                                    }

                                                %>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Year of Passing :</label>
                                        <div class="col-sm-6">
                                            <select id="memberPassingYear" name="memberPassingYear"   class="custom-select" required="">
                                                <option value="" selected>Year</option>
                                                <option value="1960">1960</option>
                                                <option value="1961">1961</option>
                                                <option value="1962">1962</option>
                                                <option value="1963">1963</option>
                                                <option value="1964">1964</option>
                                                <option value="1965">1965</option>
                                                <option value="1966">1966</option>
                                                <option value="1967">1967</option>
                                                <option value="1968">1968</option>
                                                <option value="1969">1969</option>
                                                <option value="1970">1970</option>
                                                <option value="1971">1971</option>
                                                <option value="1972">1972</option>
                                                <option value="1973">1973</option>
                                                <option value="1974">1974</option>
                                                <option value="1975">1975</option>
                                                <option value="1976">1976</option>
                                                <option value="1977">1977</option>
                                                <option value="1978">1978</option>
                                                <option value="1979">1979</option>
                                                <option value="1980">1980</option>
                                                <option value="1981">1981</option>
                                                <option value="1982">1982</option>
                                                <option value="1983">1983</option>
                                                <option value="1984">1984</option>
                                                <option value="1985">1985</option>
                                                <option value="1986">1986</option>
                                                <option value="1987">1987</option>
                                                <option value="1988">1988</option>
                                                <option value="1989">1989</option>
                                                <option value="1990">1990</option>
                                                <option value="1991">1991</option>
                                                <option value="1992">1992</option>
                                                <option value="1993">1993</option>
                                                <option value="1994">1994</option>
                                                <option value="1995">1995</option>
                                                <option value="1996">1996</option>
                                                <option value="1997">1997</option>
                                                <option value="1998">1998</option>
                                                <option value="1999">1999</option>
                                                <option value="2000">2000</option>
                                                <option value="2001">2001</option>
                                                <option value="2002">2002</option>
                                                <option value="2003">2003</option>
                                                <option value="2004">2004</option>
                                                <option value="2005">2005</option>
                                                <option value="2006">2006</option>
                                                <option value="2007">2007</option>
                                                <option value="2008">2008</option>
                                                <option value="2009">2009</option>
                                                <option value="2010">2010</option>
                                                <option value="2011">2011</option>
                                                <option value="2012">2012</option>
                                                <option value="2013">2013</option>
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>									
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Result Type :</label>
                                        <div class="col-sm-6">          
                                            <select name="memberResultTypeId" id="memberResultTypeId" class="form-control" required="">
                                                <option value="">Select Result Type </option>
                                                <%                                                    Object[] resultObj = null;
                                                    String resultTypeIdX = "";
                                                    String resultTypeNameX = "";
                                                    Query resultSQL = dbsession.createSQLQuery("select * FROM result_type ORDER BY result_type_id ASC");
                                                    for (Iterator itr7 = resultSQL.list().iterator(); itr7.hasNext();) {
                                                        resultObj = (Object[]) itr7.next();
                                                        resultTypeIdX = resultObj[0].toString();
                                                        resultTypeNameX = resultObj[1].toString();
                                                %>
                                                <option value="<%=resultTypeIdX%>"><%=resultTypeNameX%></option>

                                                <%
                                                    }


                                                %>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Score/Class :</label>
                                        <div class="col-sm-6">
                                            <input name="memberScoreClass" id="memberScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required>
                                        </div>
                                    </div>

                                    <div class="form-group row">

                                        <div class="col-sm-6 offset-sm-3">
                                            <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                            <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                            <button type="button" class="btn btn-primary">Reset</button>
                                        </div>
                                    </div>



                                </form>
                                <!-- ================ End billing information form ================= -->


                            </div>
                            <!-- /#education -->
                            <div class="tab-pane <% out.print(tabProfessionalActive); %>" id="professional">

                                <h4 class="font-bold1 m-t-30">Professional Information</h4>
                                <hr>
                                <div class="row">                                      
                                    <div class="col-md-12">

                                        <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Name of The Organization</th>
                                                    <th scope="col">Designation</th>
                                                    <th scope="col">From Date</th>
                                                    <th scope="col">To Date</th>
                                                    <th scope="col">Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%

                                                    Query professionSQL = null;
                                                    String organizationName = "";
                                                    String organizationType = "";
                                                    String organizationAddress = "";
                                                    String designationName = "";
                                                    String fromDate = "";
                                                    String toDate = "";
                                                    String startDate = "";
                                                    String tillDate = "";
                                                    int profInfoId = 0;
                                                    MemberProfessionalInfo mpi = null;

                                                    professionSQL = dbsession.createQuery("from MemberProfessionalInfo WHERE  member_id=" + memberIdH + " ");

                                                    for (Iterator itr = professionSQL.list().iterator(); itr.hasNext();) {
                                                        mpi = (MemberProfessionalInfo) itr.next();
                                                        profInfoId = mpi.getId();
                                                        organizationName = mpi.getCompanyName() == null ? "" : mpi.getCompanyName();

                                                        organizationType = mpi.getCompanyType() == null ? "" : mpi.getCompanyType();
                                                        designationName = mpi.getDesignation() == null ? "" : mpi.getDesignation();
                                                        startDate = mpi.getFromDate().toString() == null ? "" : mpi.getFromDate().toString();
                                                        tillDate = mpi.getTillDate().toString() == null ? "" : mpi.getTillDate().toString();
                                                        organizationAddress = mpi.getCompanyAddress() == null ? "" : mpi.getCompanyAddress();


                                                %>
                                                <tr id="proInfoBox<%=profInfoId%>">
                                                    <td><%=organizationName%></td>
                                                    <td><%=designationName%></td> 
                                                    <td><%=startDate%></td>
                                                    <td><%=tillDate%></td>
                                                    <td style="cursor:pointer">

                                                        <a title="Remove" onclick="professionDeleteInfo(<% out.print("'" + profInfoId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 

                                                </tr>

                                                <%                                }


                                                %>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- ============ End Education information Table ================== -->

                                <div class="row">                                      
                                    <div class="col-md-12">

                                        <div class="container">
                                            <div class="row my-4">
                                                <div class="col-12">
                                                    <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                                        <i class="fa fa-list-alt"></i> Add Professional Information
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberEditProfessionalInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*Name of The Organization :</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" id="memProfOrgName" name="memProfOrgName" placeholder="Name of The Organization" required="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*Designation :</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" id="memProfDesignation" name="memProfDesignation" placeholder="Designation" required="">
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                                                <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*Start Date :</label>
                                                <div class="col-sm-6">

                                                    <input type="text" class="form-control" id="startDate" name="startDate"  placeholder="yyyy-mm-dd" required=""> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                </div>
                                                <!-- Date Picker Plugin JavaScript -->
                                                <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                                                <script type="text/javascript">
                                            jQuery('#startDate').datepicker({
                                                autoclose: true,
                                                todayHighlight: true,
                                                format: 'yyyy-mm-dd'
                                            });
                                                </script>  
                                            </div>



                                            <div class="form-group row">
                                                <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                                                <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*End Date :</label>
                                                <div class="col-sm-6">

                                                    <input type="text" class="form-control" id="endDate" name="endDate"  placeholder="yyyy-mm-dd" required=""> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                </div>
                                                <!-- Date Picker Plugin JavaScript -->
                                                <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                                                <script type="text/javascript">
                                            jQuery('#endDate').datepicker({
                                                autoclose: true,
                                                todayHighlight: true,
                                                format: 'yyyy-mm-dd'
                                            });
                                                </script>  
                                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                                    <input type="checkbox" class="custom-control-input" id="customControlInline">
                                                    <label class="custom-control-label" for="customControlInline">Continue</label>
                                                </div>
                                            </div>

                                            <div class="form-group row">

                                                <div class="col-sm-6 offset-sm-4">
                                                    <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                    <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                                    <button type="button" class="btn btn-primary">Reset</button>
                                                </div>
                                            </div>




                                        </form>



                                    </div>
                                </div>


                            </div>
                            <!-- /#professional-->
                            <div class="tab-pane <% out.print(tabPaymentActive); %>" id="payment">

                                <h4 class="font-bold1 m-t-30">Payment Information</h4>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sl. No.</th>
                                                    <th scope="col">Bill No.</th>
                                                    <th scope="col">Bill Year</th>
                                                    <th scope="col">Payment Date</th>
                                                    <th scope="col">Bill Type</th>
                                                    <th scope="col">Bill Amount</th>
                                                    <th scope="col">Due Date</th>
                                                    <th scope="col">View Bill</th>
                                                    <th scope="col">Status</th> 
                                                    <th scope="col">Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                    int pi = 1;
                                                    int memberFeeId = 0;
                                                    String memberFeeName = "";
                                                    String memberFeeshipId = "";
                                                    String memberFeePhone = "";
                                                    String memberFeeBillAmount = "";
                                                    String memberFeeBillYear = "";
                                                    int memberFeeStatus = 0;
                                                    String memberFeeStatus1 = "";
                                                    String memberFeeBillType = "";
                                                    String memberFeeDueDate = "";
                                                    String memberFeePaymentDate = "";
                                                    String payOnline = "";
                                                    String divToPrint = "";
                                                    String memberFeeActionBtn = "";
                                                    Query memberFeeSQL = null;
                                                    Object memberFeeObj[] = null;

                                                    MemberFee mFeeInfo = null;

                                                    memberFeeSQL = dbsession.createQuery("from MemberFee WHERE  member_id=" + memberIdH + " ");

                                                    for (Iterator memberFeeItr = memberFeeSQL.list().iterator(); memberFeeItr.hasNext();) {
                                                        mFeeInfo = (MemberFee) memberFeeItr.next();
                                                        memberFeeId = mFeeInfo.getTxnId();
                                                        memberFeeName = mFeeInfo.getMember().getMemberName().toString();
                                                        memberFeeshipId = mFeeInfo.getMember().getMemberId().toString();
                                                        memberFeePhone = mFeeInfo.getMember().getMobile().toString();
                                                        memberFeeBillAmount = mFeeInfo.getAmount() == null ? "0" : mFeeInfo.getAmount().toString();

                                                        memberFeeBillYear = mFeeInfo.getMemberFeesYear().getMemberFeesYearName() == null ? "" : mFeeInfo.getMemberFeesYear().getMemberFeesYearName();
                                                        memberFeeStatus = mFeeInfo.getStatus();
                                                        if (memberFeeStatus == 1) {
                                                            memberFeeStatus1 = "Paid";
                                                            payOnline = "";
                                                        } else {
                                                            memberFeeStatus1 = "Due";
                                                            //  payOnline = "<a title=\"Pay Online\" href=\"" + GlobalVariable.baseUrl + "/member/memberPaymentProcessing.jsp?sessionid=" + session.getId() + "&act=add&&memberFeeId=" + memberFeeId + "\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-credit-card\"></i></a>";

                                                        }

                                                        memberFeeBillType = mFeeInfo.getBillType();
                                                        memberFeeDueDate = mFeeInfo.getDueDate() == null ? "" : mFeeInfo.getDueDate().toString();
                                                        memberFeePaymentDate = mFeeInfo.getPaidDate() == null ? "" : mFeeInfo.getPaidDate().toString();

                                                        memberFeeActionBtn = "<a title=\"Remove\" onclick=\"memberFeeDeleteInfo('" + memberFeeId + "','" + sessionIdH + "')\" ><i class=\"fa fa-trash\"></i> </a>";

                                                        divToPrint = "<div id=\"divToPrint" + memberFeeId + "\" style=\"display:none;\"><!-- Start:  Print Container  -->"
                                                                + "<html>"
                                                                + "<head><title>Bill Invoice Print</title></head>"
                                                                + "<body>"
                                                                + "<div style=\"width: 790px; margin: 0px auto; margin-top: 1.5cm;\">"
                                                                + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                + "<tr>"
                                                                + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\"></td>"
                                                                + "<td style=\"text-align: center;\">"
                                                                + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span><br>"
                                                                + "<span style=\"font-size: 20px; font-weight: bold;\">The Institution of Engineers, Bangladesh (IEB)</span>"
                                                                + "</td>"
                                                                + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                                + "</tr>"
                                                                + "</table>"
                                                                + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                + "<tr>"
                                                                + "<td style=\"text-align: center;\">"
                                                                + "<table>"
                                                                + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + memberFeeName + "</td></tr>"
                                                                + "<tr><td style=\"text-align: left;\"><strong>Member ID </strong></td><td>:" + memberFeeshipId + "</td></tr>"
                                                                + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + memberFeePhone + "</td></tr>"
                                                                + "</table>"
                                                                + "</td>"
                                                                + "<td style=\"text-align: center;\">"
                                                                + "<table>"
                                                                + "<tr><td><strong>Date</strong></td><td>:" + memberFeePaymentDate + "</td></tr>"
                                                                + "<tr><td><strong>Invoice No.</strong></td><td>:" + memberFeeId + "</td></tr>"
                                                                + "<tr><td><strong>Status.</strong></td><td>:" + memberFeeStatus1 + "</td></tr>"
                                                                + "</table>"
                                                                + "</td>"
                                                                + "</tr>"
                                                                + "</table>"
                                                                + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                + "<thead>"
                                                                + "<tr>"
                                                                + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                                + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                                + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                                + "</tr>"
                                                                + "</thead>"
                                                                + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                                + "<tr>"
                                                                + "<td style=\"border: 1px solid #ccc;\">" + memberFeeBillYear + "</td>"
                                                                + "<td style=\"border: 1px solid #ccc;\">" + memberFeeBillType + "</td>"
                                                                + "<td style=\"border: 1px solid #ccc;\">" + memberFeeBillAmount + "</td>"
                                                                + "</tr>"
                                                                + "<tr>"
                                                                + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                                + "<td style=\"border: 1px solid #ccc;\">" + memberFeeBillAmount + "</td>"
                                                                + "</tr>"
                                                                + "</tbody>"
                                                                + "</table> <!--End : invoice product list details-->"
                                                                + "<table style=\"width: 95%; border-top:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                + "<tr>"
                                                                + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                                + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                                + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                                + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                                + "</td>"
                                                                + "</tr>"
                                                                + "</table>"
                                                                + "</div>"
                                                                + "</body>"
                                                                + "</html>"
                                                                + "</div><!-- End:  Print Container  -->";


                                                %>
                                                <tr id="memberFeeInfoBox<%=memberFeeId%>">
                                                    <td><%=pi%></td>
                                                    <td><%=memberFeeId%></td>
                                                    <td><%=memberFeeBillYear%></td>
                                                    <td><%=memberFeePaymentDate%></td>
                                                    <td><%=memberFeeBillType%></td> 
                                                    <td><%=memberFeeBillAmount%></td>
                                                    <td><%=memberFeeDueDate%></td> 
                                                    <td style="cursor:pointer">
                                                        <a onclick="printInvoiceInfo('<%=memberFeeId%>');"  class="btn btn-default btn-sm">&nbsp; <i class="fa fa-file"></i>&nbsp; </a>

                                                        <%=divToPrint%>
                                                    </td>
                                                    <td><%=memberFeeStatus1%></td>
                                                    <td><%=memberFeeActionBtn%></td>                                                           

                                                </tr>

                                                <%
                                                        pi++;
                                                    }


                                                %>


                                            </tbody>
                                        </table>

                                    </div>
                                </div>


                                <!-- .Payment Information-->

                            </div>
                            <!-- /#payment -->   
                            <div class="tab-pane <% out.print(tabRecomendationActive); %>" id="recomendation">

                                <h4 class="font-bold1 m-t-30">Recommendation Info</h4>
                                <hr>

                                <%
                                    Object[] objProposer = null;
                                    String reqMemProposerId = "";
                                    String reqMemProposerMemberId = "";
                                    String reqMemProposerName = "";
                                    String reqMemProposerApproveStatus = "";
                                    String reqMemProposerApproveStatusText = "";
                                    String reqMemProposerRequestDate = "";
                                    String reqMemProposerAppRejDate = "";
                                    int mrec = 1;
                                    String proposerActionBtn = "";
                                    String proposerConInfo = "";

                                    Query proposerSQL = dbsession.createSQLQuery("SELECT mp.id,m.member_id, m.member_name, "
                                            + "mp.status, mp.request_date, mp.app_rej_date  "
                                            + "FROM  member_proposer_info mp,member m  "
                                            + "WHERE mp.member_id = '" + memberIdH + "' AND mp.proposer_id=m.id "
                                            + "ORDER BY mp.id DESC");

                                    if (!proposerSQL.list().isEmpty()) {
                                        for (Iterator itPro = proposerSQL.list().iterator(); itPro.hasNext();) {

                                            objProposer = (Object[]) itPro.next();
                                            reqMemProposerId = objProposer[0].toString().trim();
                                            reqMemProposerMemberId = objProposer[1].toString().trim();
                                            reqMemProposerName = objProposer[2].toString().trim();
                                            reqMemProposerApproveStatus = objProposer[3].toString().trim();
                                            if (reqMemProposerApproveStatus.equals("0")) {
                                                reqMemProposerApproveStatusText = "Wating for Approval";
                                                // reqMemProposerApproveStatusText = "<button class=\"btn btn-warning waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-link\"></i></span>Wating for Approval</button>";
                                            }
                                            if (reqMemProposerApproveStatus.equals("1")) {
                                                reqMemProposerApproveStatusText = "Approved";
                                                //reqMemProposerApproveStatusText = "<button class=\"btn btn-success waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-check\"></i></span>Approved</button>";
                                            }
                                            if (reqMemProposerApproveStatus.equals("2")) {
                                                reqMemProposerApproveStatusText = "Decline";
                                                //reqMemProposerApproveStatusText = "<button class=\"btn btn-success waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-times\"></i></span>Decline</button>";
                                            }
                                            reqMemProposerRequestDate = objProposer[4].toString().trim();
                                            reqMemProposerAppRejDate = objProposer[5] == null ? "" : objProposer[5].toString().trim();

                                            proposerActionBtn = "<a title=\"Remove\" onclick=\"proposerDeleteInfo('" + reqMemProposerId + "','" + sessionIdH + "')\" ><i class=\"fa fa-trash\"></i> </a>";

                                            proposerConInfo = proposerConInfo + ""
                                                    + "<tr id=\"proposerInfoBox" + reqMemProposerId + "\">"
                                                    + "<td> " + mrec + " </td>"
                                                    + "<td> " + reqMemProposerMemberId + "</td>"
                                                    + "<td> " + reqMemProposerName + " </td>"
                                                    + "<td> " + reqMemProposerRequestDate + " </td>"
                                                    + "<td> " + reqMemProposerAppRejDate + " </td>"
                                                    + "<td> " + reqMemProposerApproveStatusText + " </td>"
                                                    + "<td style=\"cursor:pointer\"> " + proposerActionBtn + " </td>"
                                                    + "</tr>";

                                            mrec++;
                                        }
                                    } else {

                                        proposerConInfo = "<tr><td colspan=\"7\">Proposer Info Not Found</td></tr>";
                                    }

                                %>



                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">SL</th>
                                                    <th scope="col">Member ID</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Request Date</th>
                                                    <th scope="col">Appr./Rej. Date</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%=proposerConInfo%>

                                            </tbody>
                                        </table>
                                    </div>                                  
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <h4 class="font-bold1 m-t-30">Add Recommendation Info</h4>
                                        <hr>
                                        <form id="loginform" class="form-horizontal" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberEditRecomendationInfoSubmitData.jsp" onsubmit="return recomendationCheckingForMembership()">
                                            <div class="row m-t-20">
                                                <div class="col-md-12">
                                                    <div class="form-row form-group">
                                                        <label class="col-md-2 col-form-label text-right m-t-5">Proposer</label>
                                                        <div class="col-md-8">
                                                            <input name="proposerMemberId" id="proposerMemberId" type="text" class="form-control" placeholder="Member ID (Ex. F01234)" required>                                                            
                                                            <div id="proposerMemberIdErr"></div>
                                                        </div>                                                    
                                                    </div>   
                                                    <div class="form-row form-group">                                                
                                                        <div class="col-md-4 offset-md-2">
                                                            <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                            <button type="submit" class="btn btn-block btn-primary" id="recomendationSubmitBtn" >Submit</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>



                            </div>
                            <!-- /#recomendation -->

                            <div class="tab-pane <% out.print(tabDocumentActive); %>" id="document">
                                <h4 class="font-bold1 m-t-30">Documents Info</h4>
                                <hr>


                                <%
                                    Query relDocSQL = null;
                                    Object[] relDocbject = null;
                                    int dd = 1;
                                    String documentId = "";
                                    String documentOrgninalName = "";
                                    String documentName = "";
                                    String documentLink = "";
                                    String documentUrl = "";
                                    String documentConInfo = "";
                                    String documentActionBtn = "";

                                    relDocSQL = dbsession.createSQLQuery("SELECT * FROM member_document_info WHERE member_id = '" + memberIdH + "'");
                                    for (Iterator relDocItr = relDocSQL.list().iterator();
                                            relDocItr.hasNext();) {
                                        relDocbject = (Object[]) relDocItr.next();

                                        documentId = relDocbject[0].toString();
                                        documentOrgninalName = relDocbject[3].toString();
                                        documentName = relDocbject[4].toString();

                                        documentLink = GlobalVariable.imageMemberDocumentDirLink + documentName;

                                        documentUrl = "<a target=\"_blank\" href=\" " + documentLink + " \" class=\"btn btn-default btn-sm\">&nbsp; <i class=\"fa fa-file\"></i>&nbsp; </a>";

                                        documentActionBtn = "<a title=\"Remove\" onclick=\"documentDeleteInfo('" + documentId + "','" + sessionIdH + "')\" ><i class=\"fa fa-trash\"></i> </a>";

                                        documentConInfo = documentConInfo + ""
                                                + "<tr id=\"documentInfoBox" + documentId + "\">"
                                                + "<td> " + dd + " </td>"
                                                + "<td> " + documentOrgninalName + "</td>"
                                                + "<td> " + documentUrl + " </td>"
                                                + "<td style=\"cursor:pointer\"> " + documentActionBtn + " </td>"
                                                + "</tr>";
                                        dd++;
                                    }

                                %>
                                <div class="row">
                                    <div class="col-md-8 offset-md-2">
                                        <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">SL</th>
                                                    <th scope="col">Document Name</th>
                                                    <th scope="col">Document Link</th>
                                                    <th scope="col">Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%=documentConInfo%>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="font-bold1 m-t-30">Change Documents Info</h4>
                                        <hr>
                                        <form id="loginform" class="form-horizontal" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/MemberEditRelatedDocumentsUpload" enctype="multipart/form-data">
                                            <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                            <div class="row">
                                                <div class="col-md-10 offset-md-1 m-t-20">
                                                    <div class="form-row form-group">
                                                        <label class="form-control-label col-md-2 text-right m-t-5" for="inputName" style="margin-top: 0.25rem;">Required Document</label>

                                                        <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                                        <div class="col-md-4">
                                                            <input name="fileNID" id="fileNID" onchange="relatedDocumentUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required>  
                                                            <div id="fileNIDErr"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <button id="btnRelDoc" type="submit" class="btn btn-primary btn-sm">Submit</button>
                                                        </div>
                                                    </div> 


                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="form-row form-group">                                                
                                                        <div class="col-md-5 offset-md-5">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 


                                            <div class="row">
                                                <div class="col-md-10 offset-md-2">
                                                    <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .zip files allow. Please add below documents when create zip file.  
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   i. Certificates: SSC, HSC/Diploma, B.Sc. Engineering copy
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  ii. Transcripts: HSC/Diploma, B.Sc. Engineering copy
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Certificates of the others professional bodies (if any) copy
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Photocopy of NID/Smart card copy
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  iv. Recognized training record of last two (2) years copy
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   v. Details records of 150 words (member) and 200 words (fellow) on experience demonstrating competencies & abilities. copy
                                                    </p>
                                                </div>
                                            </div>



                                        </form> 


                                    </div>
                                </div>
                            </div>
                            <!-- /#document -->

                            <div class="tab-pane <% out.print(tabChangePictureActive); %>" id="changePicture">


                                <div class="row"> 
                                    <div class="col-sm-4">
                                        <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/MemberPictureUpload" onSubmit="return fromDataSubmitValidation()" enctype="multipart/form-data">

                                            <br/>
                                            <br/>


                                            <div class="form-group row">
                                                <label for="eventShortDesc" class="col-sm-3 col-form-label text-right">Picture</label>
                                                <div class="col-sm-9">  
                                                    <input name="file" id="file" accept="image/*" type="file" class="form-control input-sm" >                                        
                                                </div>
                                            </div>



                                            <div class="form-group row">
                                                <div class="col-sm-9 offset-md-3">
                                                    <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                    <button type="submit" class="btn btn-primary mr-3 btn-sm">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-4">                                        
                                        <%=pictureLink%>

                                        <p class="text-center" style="width: 150px; margin-top: 2px;"><a href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberEdit.jsp?sessionid=<%=session.getId() + "&selectedTab=changePicture&&memberId=" + memberIdH%>" class="btn btn-primary btn-sm  btn-block text-center">Click For Check Photo</a></p>

                                    </div>
                                </div>

                            </div>
                            <!-- /#changePicture -->
                            <div class="tab-pane <% out.print(tabCenterDivisionActive);%>" id="centerDivision">

                                <div class="row"> 
                                    <div class="col-sm-6">
                                        <p class="font-bold">Center : <%=centerName%></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="font-bold">Division : <%=divisionName%> </p>
                                        <p class="font-bold">Sub-Division : <%=subDivisionName%></p>
                                    </div>
                                </div>

                                <div class="row m-t-15"> 
                                    <div class="col-sm-4">
                                        <h4 class="font-bold1">Change Center Info</h4>
                                        <hr>

                                        <form class="form-horizontal" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberEditCenterInfoSubmitData.jsp" onSubmit="return fromDataSubmitValidation()">
                                            <div class="form-group row">
                                                <label for="inputName" class="col-md-3 text-right m-t-10">Center</label>
                                                <div class="col-md-6">
                                                    <select  id="memberEditCenterId" name="memberEditCenterId" class="form-control input-sm customInput-sm" required>
                                                        <option value="">Select Center</option>
                                                        <%
                                                            Query centerEditSQL = null;
                                                            Object[] centerEditObject = null;
                                                            String centerEditId = "";
                                                            String centerEditName = "";
                                                            String centerEditSelected = "";
                                                            centerEditSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_type_id ='3' ORDER BY center_id ASC");
                                                            for (Iterator centerEditItr = centerEditSQL.list().iterator(); centerEditItr.hasNext();) {
                                                                centerEditObject = (Object[]) centerEditItr.next();
                                                                centerEditId = centerEditObject[0].toString();
                                                                centerEditName = centerEditObject[1].toString();

                                                                if (centerEditId.equals(Integer.toString(centerId))) {
                                                                    centerEditSelected = " selected";
                                                                } else {
                                                                    centerEditSelected = "";
                                                                }


                                                        %> 
                                                        <option value="<%=centerEditId%>" <%=centerEditSelected%>> <%=centerEditName%> </option> 
                                                        <%
                                                            }

                                                        %>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 offset-md-3">
                                                    <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                    <button type="submit" class="btn btn-primary mr-3 btn-sm">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-6 offset-md-2">
                                        <h4 class="font-bold1">Change Division Info</h4>
                                        <hr>
                                        <%
                                            //    subDivisionId = member.getSubDivision().getSubDivisionId();
                                            //    subDivisionName = member.getSubDivision().getSubDivisionName();
                                            //    divisionId = member.getSubDivision().getMemberDivision().getMemDivisionId();
                                            //    divisionName = member.getSubDivision().getMemberDivision().getMemDivisionName();

                                        %>

                                        <form id="loginform" class="form-horizontal" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberEditDivisionInfoSubmitData.jsp">


                                            <div class="form-row form-group">
                                                <label class="col-md-3 col-form-label text-right m-t-10" for="inputName">Division:</label>
                                                <div class="col-md-8">
                                                    <select name="memberDivisionId" id="memberDivisionId" onchange="showDivisionWisSubDivisionInfo(this.value)" class="form-control" required>
                                                        <option value="">Select Division</option>
                                                        <%
                                                            Object[] objectEditDiv = null;
                                                            String divisionEditId = "";
                                                            String divisionEditName = "";
                                                            String divisionEditOptions = "";
                                                            Query divisionEditSQL = dbsession.createSQLQuery("select * FROM member_division ORDER BY mem_division_name ASC");
                                                            for (Iterator divisionEditItr = divisionEditSQL.list().iterator(); divisionEditItr.hasNext();) {
                                                                objectEditDiv = (Object[]) divisionEditItr.next();
                                                                divisionEditId = objectEditDiv[0].toString();
                                                               // divisionEditName = objectEditDiv[1].toString();
                                                                divisionEditName = objectEditDiv[3].toString();
                                                                divisionEditOptions = divisionEditOptions + "<option value=\"" + divisionEditId + "\">" + divisionEditName + "</option>";

                                                            }
                                                        %>
                                                        <%=divisionEditOptions%>
                                                    </select>
                                                </div>                                                    
                                            </div>                                                   


                                            <div class="form-row form-group">
                                                <label class="col-md-3 col-form-label text-right m-t-10" for="inputName">Sub-Division:</label>
                                                <div class="col-md-8" id="showSubDivision">
                                                    <select name="memberSubDivision" id="memberSubDivision" class="form-control" required>
                                                        <option value="">Select Sub-Division</option>

                                                    </select>
                                                </div>                                                    
                                            </div>                                                   

                                            <div class="form-row form-group">
                                                <div class="col-md-2 offset-md-3">
                                                    <input type="hidden" name="memberEditId" id="memberEditId" value="<%=memberIdH%>">
                                                    <button type="submit" class="btn btn-block btn-primary btn-sm">Submit</button> 
                                                </div>
                                            </div>
                                        </form>



                                    </div>
                                </div>

                            </div>
                            <!-- /#centerDivision -->



                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%        dbsession.clear();
    dbsession.close();
    %>

    <%@ include file="../footer.jsp" %>