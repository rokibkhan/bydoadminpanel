<%@page import="java.util.Date"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.*"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();

    String showTabOption;
    if (regStep.equals("8")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureName = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    String membershipApplyingFor = "";
    String membershipApplyingFor1 = "";
    String presentIEBmembershipNumber = "";

    q1 = dbsession.createSQLQuery("select * from member_temp WHERE id= " + regMemId + "");
    for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        memberName = object[4].toString();
        fatherName = object[5] == null ? "" : object[5].toString();
        motherName = object[6] == null ? "" : object[6].toString();
        placeOfBirth = object[7].toString();
        datefBirth = object[8].toString();
        age = object[9] == null ? "" : object[9].toString();
        nationality = object[10] == null ? "" : object[10].toString();
        gender = object[11].toString();

        membershipApplyingFor1 = object[13] == null ? "" : object[13].toString();
        if (membershipApplyingFor1.equals("1")) {
            membershipApplyingFor = " Fellow ";
        } else if (membershipApplyingFor1.equals("2")) {
            membershipApplyingFor = "Member";
        } else {
            membershipApplyingFor = "Associate Member";
        }

        presentIEBmembershipNumber = object[13] == null ? "" : object[13].toString();

        phone1 = object[14] == null ? "" : object[14].toString();
        phone2 = object[15] == null ? "" : object[15].toString();
        mobileNumber = object[16] == null ? "" : object[16].toString();
        userEmail = object[17].toString();
        pictureName = object[25].toString();

        pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

    }

%>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about1.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>




<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Member Info</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Member Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">



                    <div class="row">
                        <div class="col-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 1: Personal Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="#">
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputName">Name:</label>
                                                        <%=memberName%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputName">Father Name:</label>
                                                        <%=fatherName%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputName">Mother Name:</label>
                                                        <%=motherName%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputName">Gender:</label>
                                                        <%=gender%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputDOB">Date of Birth:</label>
                                                        <%=datefBirth%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Age:</label>
                                                        <%=age%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Place of Birth:</label>
                                                        <%=placeOfBirth%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Nationality:</label>
                                                        <%=nationality%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Office Phone:</label>
                                                        <%=phone1%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Residence Phone:</label>
                                                        <%=phone2%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Mobile:</label>
                                                        <%=mobileNumber%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Email:</label>
                                                        <%=userEmail%>
                                                    </div>
                                                </div>







                                                <div class="form-row form-group m-0">
                                                    <div class="col-12">
                                                        <label for="inputName">Membership Applying For:</label>
                                                        <%=membershipApplyingFor%>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-12">
                                                        <label for="inputName">Present IEB Membership Number:</span></label>
                                                        <%=presentIEBmembershipNumber%>
                                                    </div>
                                                </div>




                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingOneVerify">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneVerify" aria-expanded="true" aria-controls="collapseOneVerify">
                                                <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 2: Verification</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOneVerify" class="collapse" aria-labelledby="headingOneVerify" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationVerificationSubmitData.jsp">
                                                <p class="m-2">Please check your mobile inbox and add verification code and click verify button </p>

                                                <div class="form-row form-group">
                                                    <label class="col-2 text-right" for="inputName">Reg No:</label>
                                                    <div class="col-6">
                                                        <p><%=regMemberTempId%></p>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group">
                                                    <label class="col-2 text-right" for="inputName">Mobile:</label>
                                                    <div class="col-4">
                                                        <p><%=mobileNumber%></p>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group">
                                                    <label class="col-2 text-right" for="inputName">Verification Code</label>
                                                    <div class="col-2">
                                                        <input name="memberVerifyCode" id="memberVerifyCode" type="text" class="form-control" placeholder="Code" disabled="" required>
                                                    </div>
                                                    <div class="col-3">
                                                        <button type="submit" class="btn btn-primary" disabled="">Already Verified</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFourMyPicture">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseMyPicture" aria-expanded="false" aria-controls="collapseMyPicture">
                                                <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 3: My Picture Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseMyPicture" class="collapse" aria-labelledby="headingMyPicture" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationMyPictureUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-10 offset-1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">My Picture</label>


                                                            <div class="col-4">
                                                                <%=pictureLink%>
                                                            </div>
                                                            <div class="col-4">
                                                                &nbsp;
                                                            </div>
                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .JPGE and .PNG files allow and file not more then 5MB.</p>
                                                    </div>
                                                </div>



                                            </form>

                                        </div>
                                    </div>
                                </div>                   

                                <div class="card">
                                    <div class="card-header" id="headingOneAddressInfo">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneAddressInfo" aria-expanded="true" aria-controls="collapseOneAddressInfo">
                                                <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 4: Address Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOneAddressInfo" class="collapse" aria-labelledby="headingOneAddressInfo" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationAddressInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Mailing Address</p>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Country:</label>
                                                            <div class="col-7">

                                                                <select  id="memberMailingAddressCountry" name="memberMailingAddressCountry" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select Country</option>
                                                                    <%
                                                                        Query countryMASQL = null;
                                                                        Object[] objectMA = null;
                                                                        String countryCodeMA = "";
                                                                        String countryNameMA = "";
                                                                        String selectedMA = "";
                                                                        countryMASQL = dbsession.createSQLQuery("SELECT * FROM sy_country ORDER BY COUNTRY_NAME ASC");
                                                                        for (Iterator itrMA = countryMASQL.list().iterator(); itrMA.hasNext();) {
                                                                            objectMA = (Object[]) itrMA.next();
                                                                            countryCodeMA = objectMA[0].toString();
                                                                            countryNameMA = objectMA[1].toString();
                                                                            if (countryCodeMA.equals("BD")) {
                                                                                selectedMA = " selected";
                                                                            } else {
                                                                                selectedMA = "";
                                                                            }


                                                                    %> 
                                                                    <option value=" <%=countryCodeMA%> " <%=selectedMA%>> <%=countryNameMA%> </option> 
                                                                    <%
                                                                        }

                                                                    %>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">District:</label>
                                                            <div class="col-7">
                                                                <select  id="memberMailingAddressDistrict" name="memberMailingAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'MA')" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select District</option>
                                                                    <%                                                            Query districtMASQL = null;
                                                                        Object[] objectMAd = null;
                                                                        String districtIdMA = "";
                                                                        String districtNameMA = "";
                                                                        String districtMA = "";
                                                                        districtMASQL = dbsession.createSQLQuery("SELECT * FROM district ORDER BY DISTRICT_NAME ASC");
                                                                        for (Iterator itrMAd = districtMASQL.list().iterator(); itrMAd.hasNext();) {
                                                                            objectMAd = (Object[]) itrMAd.next();
                                                                            districtIdMA = objectMAd[0].toString();
                                                                            districtNameMA = objectMAd[1].toString();


                                                                    %> 
                                                                    <option value=" <%=districtIdMA%> "> <%=districtNameMA%> </option> 
                                                                    <%
                                                                        }
                                                                    %>

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Thana:</label>
                                                            <div class="col-7" id="showThanaInfoMA">                                                    
                                                                <select  id="memberMailingAddressThana" name="memberMailingAddressThana" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select Thana</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Address Line1:</label>
                                                            <div class="col-7">
                                                                <input name="memberMailingAddressLine1" id="memberMailingAddressLine1" type="text" class="form-control" placeholder="Address Line1" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Address Line2:</label>
                                                            <div class="col-7">
                                                                <input name="memberMailingAddressLine2" id="memberMailingAddressLine2" type="text" class="form-control" placeholder="Address Line2">
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Post Office:</label>
                                                            <div class="col-4">
                                                                <input name="memberMailingAddressPostOffice" id="memberMailingAddressPostOffice" type="text" class="form-control" placeholder="Post Office" required>
                                                            </div>
                                                            <div class="col-3">
                                                                <input name="memberMailingAddressPostCode" id="memberMailingAddressPostCode" type="text" class="form-control" placeholder="Post Code" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Permanent Address</p>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Country:</label>
                                                            <div class="col-7">
                                                                <select  id="memberPermanentAddressCountry" name="memberPermanentAddressCountry" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select Country</option>
                                                                    <%
                                                                        Query countryPASQL = null;
                                                                        Object[] objectPA = null;
                                                                        String countryCodePA = "";
                                                                        String countryNamePA = "";
                                                                        String selectedPA = "";
                                                                        countryPASQL = dbsession.createSQLQuery("SELECT * FROM sy_country ORDER BY COUNTRY_NAME ASC");
                                                                        for (Iterator itrPA = countryPASQL.list().iterator(); itrPA.hasNext();) {
                                                                            objectPA = (Object[]) itrPA.next();
                                                                            countryCodePA = objectPA[0].toString();
                                                                            countryNamePA = objectPA[1].toString();
                                                                            if (countryCodePA.equals("BD")) {
                                                                                selectedPA = " selected";
                                                                            } else {
                                                                                selectedPA = "";
                                                                            }


                                                                    %> 
                                                                    <option value=" <%=countryCodePA%> " <%=selectedPA%>> <%=countryNamePA%> </option> 
                                                                    <%
                                                                        }

                                                                    %>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">District:</label>
                                                            <div class="col-7">
                                                                <select  id="memberPermanentAddressDistrict" name="memberPermanentAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'PA')" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select District</option>
                                                                    <%                                                            Query districtPASQL = null;
                                                                        Object[] objectPAd = null;
                                                                        String districtIdPA = "";
                                                                        String districtNamePA = "";
                                                                        String districtPA = "";
                                                                        districtPASQL = dbsession.createSQLQuery("SELECT * FROM district ORDER BY DISTRICT_NAME ASC");
                                                                        for (Iterator itrPAd = districtPASQL.list().iterator(); itrPAd.hasNext();) {
                                                                            objectPAd = (Object[]) itrPAd.next();
                                                                            districtIdPA = objectPAd[0].toString();
                                                                            districtNamePA = objectPAd[1].toString();


                                                                    %> 
                                                                    <option value=" <%=districtIdPA%> "> <%=districtNamePA%> </option> 
                                                                    <%
                                                                        }
                                                                    %>

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Thana:</label>
                                                            <div class="col-7" id="showThanaInfoPA">                                                    
                                                                <select  id="memberPermanentAddressThana" name="memberPermanentAddressThana" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select Thana</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Address Line1:</label>
                                                            <div class="col-7">
                                                                <input name="memberPermanentAddressLine1" id="memberPermanentAddressLine1" type="text" class="form-control" placeholder="Address Line1" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Address Line2:</label>
                                                            <div class="col-7">
                                                                <input name="memberPermanentAddressLine2" id="memberPermanentAddressLine2" type="text" class="form-control" placeholder="Address Line2">
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-3 text-right" for="inputName">Post Office:</label>
                                                            <div class="col-4">
                                                                <input name="memberPermanentAddressPostOffice" id="memberPermanentAddressPostOffice" type="text" class="form-control" placeholder="Post Office" required>
                                                            </div>
                                                            <div class="col-3">
                                                                <input name="memberPermanentAddressPostCode" id="memberPermanentAddressPostCode" type="text" class="form-control" placeholder="Post Code" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 5: Education Information</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            <%
                                                Query eduTempSQL = null;
                                                //    Query q2 = null;
                                                int degreeId = 0;
                                                String degreeName = "";
                                                String instituteName = "";
                                                String boardUniversityName = "";
                                                String yearOfPassing = "";
                                                String resultTypeName = "";
                                                String result = "";

                                                MemberEducationInfoTemp mei = null;

                                                eduTempSQL = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + regMemId + " ");

                                                MemberTemp member = null;
                                                String memberSubdivisionName = "";
                                                String memberSubdivisionFullName = "";
                                                int educationfInfoId = 0;

                                            %>


                                            <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Degree</th>
                                                        <th scope="col">Board/University</th>
                                                        <th scope="col">Year of Passing</th>
                                                        <th scope="col">Score/Class</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <%                                            for (Iterator eduTempItr = eduTempSQL.list().iterator(); eduTempItr.hasNext();) {
                                                            mei = (MemberEducationInfoTemp) eduTempItr.next();

                                                            educationfInfoId = mei.getId();
                                                            degreeId = mei.getDegree().getDegreeId();
                                                            degreeName = mei.getDegree().getDegreeName();

                                                            instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                            boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                            yearOfPassing = mei.getYearOfPassing();
                                                            resultTypeName = mei.getResultType().getResultTypeName();
                                                            result = mei.getResult();

                                                            //                                    if (degreeId == 3 || degreeId == 4) {
                                                            //
                                                            //                                        q2 = dbsession.createQuery("from MemberTemp as member WHERE id=" + registerAppId + " ");
                                                            //
                                                            //                                        for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                                            //                                            member = (MemberTemp) itr2.next();
                                                            //
                                                            //                                         //   memberSubdivisionName = member.getSubDivision().getSubDivisionName();
                                                            //                                         //   memberSubdivisionFullName = member.getSubDivision().getFullName();
                                                            //                                        // memberSubdivisionName = member.getS
                                                            //                                        }
                                                            //                                    } else {
                                                            memberSubdivisionFullName = "";
                                                            //                                   }

                                                            degreeName = degreeName + memberSubdivisionFullName;
                                                    %>
                                                    <tr>
                                                        <td><%=degreeName%></td>
                                                        <td><%=boardUniversityName%></td>
                                                        <td><%=yearOfPassing%></td>
                                                        <td><%=result%></td>

                                                    </tr>
                                                    <%
                                                        }


                                                    %>

                                                </tbody>
                                            </table>






                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 6: Professional Record</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">

                                            <%                                        Query profTempSQL = null;
                                                //    Query q2 = null;
                                                int profTempId = 0;
                                                String organizationName = "";
                                                String designationName = "";
                                                String startDateProTemp = "";
                                                String endDateProTemp = "";

                                                MemberProfessionalInfoTemp profTemp = null;

                                                profTempSQL = dbsession.createQuery("from MemberProfessionalInfoTemp where  member_id=" + regMemId + " ");


                                            %>
                                            <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Organization</th>
                                                        <th scope="col">Designation</th>
                                                        <th scope="col">Start Date</th>
                                                        <th scope="col">End Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <%                                                for (Iterator profTempItr = profTempSQL.list().iterator(); profTempItr.hasNext();) {
                                                            profTemp = (MemberProfessionalInfoTemp) profTempItr.next();

                                                            organizationName = profTemp.getCompanyName().toString();
                                                            designationName = profTemp.getDesignation().toString();
                                                            startDateProTemp = profTemp.getFromDate().toString();
                                                            endDateProTemp = profTemp.getTillDate().toString();


                                                    %>
                                                    <tr>
                                                        <td><%=organizationName%></td>
                                                        <td><%=designationName%></td>
                                                        <td><%=startDateProTemp%></td>
                                                        <td><%=endDateProTemp%></td>

                                                    </tr>
                                                    <%
                                                        }

                                                    %>

                                                </tbody>
                                            </table>



                                        </div>
                                    </div>
                                </div>

                                <div class="card" id="section8">
                                    <div class="card-header" id="headingFourDocuments">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourDocuments" aria-expanded="false" aria-controls="collapseFourDocuments">
                                                <span class="h4">Section 7: Related Documents Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourDocuments" class="collapse <%=showTabOption%>" aria-labelledby="headingFourDocuments" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/MemberAddRelatedDocumentsUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-md-10 offset-md-1 m-t-20">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-2 text-right m-t-5" for="inputName" style="margin-top: 0.25rem;">Required Document</label>

                                                            <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                                            <div class="col-md-4">
                                                                <input name="fileNID" id="fileNID" onchange="relatedDocumentUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required>  
                                                                <div id="fileNIDErr"></div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <button id="btnRelDoc" type="submit" class="btn btn-primary btn-sm">Submit</button>
                                                            </div>
                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-md-5 offset-md-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 


                                                <div class="row">
                                                    <div class="col-md-10 offset-md-2">
                                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .zip files allow. Please add below documents when create zip file.  
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   i. Certificates: SSC, HSC/Diploma, B.Sc. Engineering copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  ii. Transcripts: HSC/Diploma, B.Sc. Engineering copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Certificates of the others professional bodies (if any) copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Photocopy of NID/Smart card copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  iv. Recognized training record of last two (2) years copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   v. Details records of 150 words (member) and 200 words (fellow) on experience demonstrating competencies & abilities. copy
                                                        </p>
                                                    </div>
                                                </div>



                                            </form>

                                        </div>
                                    </div>
                                </div>  
                                <div class="card">
                                    <div class="card-header" id="headingFourRecom">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRecom" aria-expanded="false" aria-controls="collapseFourRecom">
                                                <span class="h4">Section 8: Recommendation Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRecom" class="collapse" aria-labelledby="headingFourRecom" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationRecomendationInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                                <div class="row">
                                                    <div class="col-4">
                                                        <p class="m-2 text-center text-dark">Proposer</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="proposerMemberId" id="proposerMemberId" type="text" class="form-control" placeholder="Membership No" required disabled="">
                                                            </div>                                                    
                                                        </div>    
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required disabled="">
                                                            </div>                                                    
                                                        </div> 


                                                    </div>
                                                    <div class="col-4">
                                                        <p class="m-2 text-center text-dark">Seconder |</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="seconderMemberId" id="seconderMemberId" type="text" class="form-control" placeholder="Membership No" required disabled="">
                                                            </div>                                                    
                                                        </div>   
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="seconderMemberName" id="seconderMemberName" type="text" class="form-control" placeholder="Member name" required disabled="">
                                                            </div>                                                    
                                                        </div> 


                                                    </div>
                                                    <div class="col-4">
                                                        <p class="m-2 text-center text-dark">Seconder ||</p>

                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="thirdMemberId" id="thirdMemberId" type="text" class="form-control" placeholder="Membership No" disabled="">
                                                            </div>                                                    
                                                        </div>   
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="thirdMemberName" id="thirdMemberName" type="text" class="form-control" placeholder="Member name" disabled="">
                                                            </div>                                                    
                                                        </div> 

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </form>
                                            <div class="row">
                                                <div class="col-12">
                                                    <p class="text-dark" style="font-size: 0.8rem;">Note |: Proposer and Seconder must be at least 
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.Two Fellows and one Member for Fellowship
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii.One Fellow and two Members for Membership
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii.Two Members for Associate Membership
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                                <div class="card" id="section9">
                                    <div class="card-header" id="headingFourRegFee">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRegFee" aria-expanded="false" aria-controls="collapseFourRegFee">
                                                <span class="h4">Section 9: Registration Fee Payment Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRegFee" class="collapse" aria-labelledby="collapseFourRegFee" data-parent="#accordion">
                                        <div class="card-body">


                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationFeePaymentOptionSubmitData.jsp">



                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                                <thead>

                                                                <th scope="col" class="bg-primary text-white"><strong>Category</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Entrance Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Annual Subscription Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Diploma Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>ID Card Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Total Amount (Tk.)</strong></th>

                                                                </thead>
                                                                <tbody>
                                                                    <%

                                                                        Object[] memTypeObject = null;
                                                                        Query memTypeSQL = null;

                                                                        String membershipApplyingType = "";
                                                                        String membershipEntranceFee = "";
                                                                        String membershipAnnualSubscriptionFee = "";
                                                                        String membershipDiplomaFee = "";
                                                                        String membershipIDCardFee = "";
                                                                        double membershipTotalPayableFee = 0.00;

                                                                        double membershipEntranceFee1 = 0.00;
                                                                        double membershipAnnualSubscriptionFee1 = 0.00;
                                                                        double membershipDiplomaFee1 = 0.00;
                                                                        double membershipIDCardFee1 = 0.00;

                                                                        //SELECT * FROM `member_type_info` 
                                                                        memTypeSQL = dbsession.createSQLQuery("SELECT * FROM member_type_info WHERE member_type_id = '" + membershipApplyingFor1 + "'");

                                                                        for (Iterator memTypeItr = memTypeSQL.list().iterator(); memTypeItr.hasNext();) {
                                                                            memTypeObject = (Object[]) memTypeItr.next();

                                                                            membershipApplyingType = memTypeObject[1].toString();
                                                                            membershipEntranceFee = memTypeObject[4].toString();
                                                                            membershipAnnualSubscriptionFee = memTypeObject[6].toString();
                                                                            membershipDiplomaFee = memTypeObject[7].toString();
                                                                            membershipIDCardFee = memTypeObject[3].toString();

                                                                            membershipEntranceFee1 = Double.parseDouble(membershipEntranceFee);
                                                                            System.out.println("Double Entry Fee:: " + membershipEntranceFee1);
                                                                            membershipAnnualSubscriptionFee1 = Double.parseDouble(membershipAnnualSubscriptionFee);
                                                                            membershipDiplomaFee1 = Double.parseDouble(membershipDiplomaFee);
                                                                            membershipIDCardFee1 = Double.parseDouble(membershipIDCardFee);

                                                                            //  membershipTotalFee = Integer.sum(membershipEntranceFee1,membershipAnnualSubscriptionFee1,membershipDiplomaFee1,membershipIDCardFee1);
                                                                            membershipTotalPayableFee = membershipEntranceFee1 + membershipAnnualSubscriptionFee1 + membershipDiplomaFee1 + membershipIDCardFee1;

                                                                    %>
                                                                    <tr>
                                                                        <td><strong><%=membershipApplyingType%></strong></td>
                                                                        <td><%=membershipEntranceFee%></td>
                                                                        <td><%=membershipAnnualSubscriptionFee%></td>
                                                                        <td><%=membershipDiplomaFee%></td>
                                                                        <td><%=membershipIDCardFee%></td>
                                                                        <td><strong><%=membershipTotalPayableFee%></strong></td>
                                                                    </tr>
                                                                    <% }%>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div> 
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     


                                            </form>





                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>






                </div>
                <!-- /.white-box -->
            </div>

        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>                                                   