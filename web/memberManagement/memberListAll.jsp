<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">
    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

        int ix = 1;

        // String targetpage = "memberListFellow.jsp";
        String targetpageWithSession = "memberListAll.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        int limit = 100;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();

        if (!searchString.equals("")) {
            filterSQLStr = " AND (m.member_name like '%" + searchString + "%' OR m.email_id like '%" + searchString + "%' OR m.member_id like '%" + searchString + "%' OR m.mobile like '%" + searchString + "%' OR m.place_of_birth like '%" + searchString + "%' )";
            filterp = "&searchString=" + searchString + "&";
            
        } else {

            filterSQLStr = " ";
            filterp = "&searchString=" + searchString + "&";
            System.out.println("searchString filterSQLStr :: " + filterSQLStr);
            System.out.println("searchString filterp :: " + filterp);
        }

        

        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
        int pageNbr = Integer.parseInt(pageNbr1);

        

        String memberId = "";
        String memberIEBId = "";
        String memberName = "";
        String memberPictureName = "";
        String memberPictureUrl = "";
        String memberPicture = "";

        String memberMobile = "";
        String memberMobile1 = "";
        String memberMobile2 = "";

        String memberEmail = "";
        String memberAddress = "";

        String status1 = "";
        String mOthersInfo = "";
        String memberUniversityName = "";
        String memberDivisionName = "";
        String memberCenterName = "";
        String memberTypeId = "";
        String committeeMemberBtn = "";
        String agrX = "";

        String memberDetailsUrl = "";
        String memberEditUrl = "";
        String memberUpgradeUrl = "";

    %>

    <script type="text/javascript">
        //     $(function(){
        function memberUpdgrateRequestByAdmin(arg1, arg2, arg3) {
            console.log("memberUpdgrateRequestByAdmin :: " + arg1);
            console.log("memberUpdgrateRequestByAdmin :: " + arg2);
            console.log("memberUpdgrateRequestByAdmin :: " + arg3);

            // $("#memberUpdgrateRequestByAdminBtn").button("loading");

            var taskLGModal, typeInfoCon, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            taskLGModal.find("#taskLGModalTitle").text("Member Upgrade Request");




            if (arg3 == '1') {
                typeInfoCon = "No Upgrade Option";
                btnConfirmInfo = "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            }
            if (arg3 == "2") {
                typeInfoCon = "<div class=\"row\">"
                        + "<div class=\"col-md-10 offset-md-1\""
                        + "<form class=\"form-horizontal\">"
                        + "<div class=\"form-group\">"
                        + "<label for=\"fellowMemberType\" class=\"control-label col-md-4 text-right m-t-10\">Upgrade Type</label>"
                        + "<div class=\"col-md-7\">"
                        + "<select class=\"form-control\" name=\"fellowMemberType\" id=\"fellowMemberType\">"
                        + "<option value=\"1\">Fellow</option>"
                        + "</select>"
                        + "</div>"
                        + "</div>"
                        + "</form>"
                        + "</div>"
                        + "</div>";

                btnConfirmInfo = "<a id=\"memberUpdgrateRequestByAdminConfirmBtn\" onclick=\"memberUpdgrateRequestByAdminConfirm('" + arg1 + "','" + arg2 + "','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            }
            if (arg3 == "3") {
                typeInfoCon = "<div class=\"row\">"
                        + "<div class=\"col-md-10 offset-md-1\""
                        + "<form class=\"form-horizontal\">"
                        + "<div class=\"form-group\">"
                        + "<label for=\"fellowMemberType\" class=\"control-label col-md-4 text-right m-t-10\">Upgrade Type</label>"
                        + "<div class=\"col-md-7\">"
                        + "<select class=\"form-control\" name=\"fellowMemberType\" id=\"fellowMemberType\">"
                        + "<option value=\"\">Select Upgrade Type</option>"
                        + "<option value=\"1\">Fellow</option>"
                        + "<option value=\"2\">Member</option>"
                        + "</select>"
                        + "</div>"
                        + "</div>"
                        + "</form>"
                        + "</div>"
                        + "</div>";
                btnConfirmInfo = "<a id=\"memberUpdgrateRequestByAdminConfirmBtn\" onclick=\"memberUpdgrateRequestByAdminConfirm('" + arg1 + "','" + arg2 + "','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            }
            taskLGModal.find("#taskLGModalBody").html(typeInfoCon);
            taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);
            taskLGModal.modal('show');
        }
        function memberUpdgrateRequestByAdminConfirm(arg1, arg2, arg3) {

            var taskLGModal, fellowMemberType, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("memberUpdgrateRequestByAdminConfirm :: " + arg1);
            console.log("memberUpdgrateRequestByAdminConfirm :: " + arg2);
            console.log("memberUpdgrateRequestByAdminConfirm :: " + arg3);

            $("#memberUpdgrateRequestByAdminConfirmBtn").button("loading");
            taskLGModal = $('#taskLGModal');

            fellowMemberType = document.getElementById("fellowMemberType").value;


            if (fellowMemberType == '') {
                $("#memberUpdgrateRequestByAdminConfirmBtn").button("reset");
                $("#fellowMemberType").focus();
                return false;
            }


            $.post("memberUpdgrateRequestByAdminConfirm.jsp", {memberId: arg1, sessionid: arg2, oldType: arg3, fellowMemberType: fellowMemberType}, function (data) {

                console.log(data);

                if (data[0].responseCode == 1) {
                    // $("#btnInvoicePaymentSubmit").button("reset");
                    taskLGModal.modal('hide');
                    var btnInfoX = data[0].requestId;

                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    //  $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    taskLGModal.find("#taskLGModalBody").html(data[0].responseMsgHtml);
                    taskLGModal.modal('show');
                }

//                taskLGModal.find("#taskLGModalBody").html(data[0].invoiceDetails);
//                taskLGModal.find("#taskLGModalFooter").html(data[0].btnInvInfo);
//                taskLGModal.modal('show');

            }, "json");

        }







    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">All Type Member List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Member</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group"  name="memberListAll" id="memberListAll" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%out.print(sessionid);%>" >
                        <input type="hidden" id="sessionid" name="sessionid" value="<%out.print(sessionid);%>"> 
                        <div class="input-group">
                            <input type="text" id="searchString" name="searchString" value="<%=searchString%>" class="form-control" placeholder="Membership ID Or Name Or Mobile Number"> 
                            <span class="input-group-btn">
                                <button type="submit" name="btnSearch" class="btn waves-effect waves-light btn-info">
                                    <i class="fa fa-search"></i> Search Member
                                </button>
                            </span> 
                        </div>
                    </form>

                    <%
                        String searchCountSQL = "SELECT count(*) FROM  member m,member_type mt "
                                + "WHERE m.status = 1 "
                                + "AND m.id = mt.member_id "
                                + " " + filterSQLStr + " "
                                + "ORDER BY m.id DESC";
                        

                        String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        

                        if (pageNbr != 0) {
                            start = (pageNbr - 1) * limit;
                        } else {
                            start = 0;
                        }

                        

                        // Initial page num setup
                        if (pageNbr == 0) {
                            pageNbr = 1;
                        }
                        prev = pageNbr - 1;
                        next = pageNbr + 1;
                        lastpage1 = Math.ceil((double) total_pages / limit);
                        //   DecimalFormat df = new DecimalFormat("#.##");
                        //  System.out.print(df.format(lastpage1));

                        // lastpage = (int) Math.ceil(total_pages / limit);
                        lastpage = (int) lastpage1;
                        LastPagem1 = (int) Math.round(lastpage - 1);

                        

                        String paginate = "";
                        if (lastpage > 1) {
                            paginate = paginate + "<ul class='pagination' style=\"margin:0\">";

                            // Previous
                            if (pageNbr > 1) {
                                p = "page=" + prev;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>previous</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
                            }

                            int counter = 1;
                            // Pages
                            if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

                                for (counter = 1; counter <= lastpage; counter++) {
                                    if (counter == pageNbr) {
                                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                    } else {
                                        p = "page=" + counter;
                                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                    }
                                }
                            } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
                                // Beginning only hide later pages
                                if (pageNbr < 1 + (stages * 2)) {
                                    for (counter = 1; counter < 4 + (stages * 2); counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // Middle hide some front and some back
                                else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // End only hide early pages
                                else {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                                    for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                }
                            }

                            // Next
                            if (pageNbr < counter - 1) {
                                p = "page=" + next;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>next</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
                            }

                            paginate = paginate + "</ul>";
                        }

                        int minPaginationIndexLimit = ((pageNbr - 1) * limit);
                        int maxPaginationIndexLimit = (pageNbr * limit);
                        int lastProductIndex = total_pages;
                        int minPaginationIndexLimitPlus = minPaginationIndexLimit + 1;
//                        if(minPaginationIndexLimitPlus < limit){
//                            minPaginationIndexLimitPlus = 1;
//                        }

                        if (maxPaginationIndexLimit >= lastProductIndex) {
                            maxPaginationIndexLimit = lastProductIndex;
                        }

                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
                                + "md.mem_division_name,c.center_name,   "
                                + "mt.member_type_id "
                                + "FROM  member m ,member_type mt,member_division md,center c "
                                + "WHERE m.status = 1 "
                                + "AND m.member_division_id = md.mem_division_id "
                                + "AND m.center_id = c.center_id "
                                + "AND m.id = mt.member_id "
                                + " " + filterSQLStr + " "
                                + "ORDER BY m.id DESC  LIMIT " + start + ", " + limit + "";

                       

                    %>

                    <div class="row">

                        <div class="col-md-8" style="padding: 20px 0;">
                            <%=paginate%>
                        </div>

                        <div class="col-md-4" style="padding-top: 23px;">
                            <p class="text-right">
                                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
                            </p>

                        </div>
                    </div>
                    <div class="row">   
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                    <thead>
                                        <tr>
                                            <th style="width: 3%;">#</th>
                                            <th class="text-center" style="width: 5%;">Picture</th>
                                            <th class="text-center" style="width: 6%;">IEB ID</th>
                                            <th class="text-center" style="width: 20%;">Name</th>
                                            <th class="text-center">Mobile</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center" style="width: 20%;">Others Info</th>
                                            <th class="text-center" style="width: 5%;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%

                                            Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                                            if (!searchSQLQry.list().isEmpty()) {
                                                for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                                                    searchObj = (Object[]) searchItr.next();
                                                    memberId = searchObj[0].toString();
                                                    memberIEBId = searchObj[1] == null ? "" : searchObj[1].toString();
                                                    memberName = searchObj[2] == null ? "" : searchObj[2].toString();

                                                    memberPictureName = searchObj[3] == null ? "" : searchObj[3].toString();
                                                    memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPictureName;
                                                    memberPicture = "<img width=\"120\" src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\">";

                                                    memberMobile = searchObj[4] == null ? "" : searchObj[4].toString();
                                                    if (!memberMobile.equals("")) {

                                                        memberMobile1 = memberMobile.substring(0, 4);
                                                        memberMobile2 = memberMobile1 + "xxxxxxx";
                                                    }

                                                    memberEmail = searchObj[5] == null ? "" : searchObj[5].toString();

                                                    memberDivisionName = searchObj[7] == null ? "" : searchObj[7].toString();
                                                    memberCenterName = searchObj[8] == null ? "" : searchObj[8].toString();

                                                    memberTypeId = searchObj[9] == null ? "" : searchObj[9].toString();

                                                    agrX = "'" + memberId + "','" + session.getId() + "','" + memberTypeId + "'";
                                                    if (!memberTypeId.equals("1")) {
                                                        memberUpgradeUrl = "<a onclick=\"memberUpdgrateRequestByAdmin(" + agrX + ")\" class=\"btn btn-success btn-sm\"> <i class=\"fa fa-info-circle\"></i>upgrade</a>";
                                                    } else {
                                                        memberUpgradeUrl = "";
                                                    }

                                                    mOthersInfo = "<strong>University:</strong>" + memberUniversityName + "<br>"
                                                            + "<strong>Division:</strong>" + memberDivisionName + "<br>"
                                                            + "<strong>Center:</strong>" + memberCenterName;

                                                    memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
                                                    memberEditUrl = GlobalVariable.baseUrl + "/memberManagement/memberEdit.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";


                                        %>


                                        <tr id="infoBox<%=memberId%>">
                                            <td><% out.print(ix);%></td>
                                            <td><%=memberPicture%></td>
                                            <td><%=memberIEBId%></td>
                                            <td><%=memberName%></td>
                                            <td class="text-center"><%=memberMobile2%></td>
                                            <td class="text-center"><%=memberEmail%></td>
                                            <td class="text-center"><%=memberAddress%></td>
                                            <td class="text-center">


                                                <a title="<%=memberName%> details" href="<%=memberDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i> details</a>
                                                <a title="<%=memberName%> details edit" href="<%=memberEditUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i> edit</a>
                                                <%=memberUpgradeUrl%>



                                            </td>
                                        </tr>

                                        <%
                                                minPaginationIndexLimitPlus++;
                                                ix++;
                                            }
                                        } else {
                                        %>
                                        <tr>
                                            <td colspan="8" class="text-center">There no data found</td>

                                        </tr>
                                        <%
                                            }
                                        %> 


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <%
                        dbsession.clear();
                        dbsession.close();
                    %>


                    <div class="row">
                        <div class="col-md-12 text-right">
                            <%=paginate%>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>