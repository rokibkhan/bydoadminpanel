
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

//    if (session.isNew()) {
//        response.sendRedirect("logout.jsp");
//    }
    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        memberIdH = request.getParameter("memberEditId").trim();

        System.out.println("memberIdH " + memberIdH);

        getRegistryID regId = new getRegistryID();

        MemberProfessionalInfo memberProfessionalInfo = null;
        String act = request.getParameter("act").trim();

        if (act.equals("edit")) {
            String profInfoId = request.getParameter("profInfoId").trim();
            Query q4 = dbsession.createQuery("from MemberProfessionalInfo where id='" + profInfoId + "' ");
            Iterator itr4 = q4.list().iterator();
            if (itr4.hasNext()) {
                memberProfessionalInfo = (MemberProfessionalInfo) itr4.next();
            }

            String memProfOrgName = request.getParameter("memProfOrgName").trim();
            String memProfDesignation = request.getParameter("memProfDesignation").trim();
            String startDate = request.getParameter("startDate").trim();
            String endDate = request.getParameter("endDate").trim();
            String comapyType = request.getParameter("comapyType") == null ? "" : request.getParameter("comapyType").trim();
            String jobDescription = request.getParameter("jobDescription") == null ? "" : request.getParameter("jobDescription").trim();

            java.util.Date startDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(startDate);
            java.util.Date endDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(endDate);

//            memberProfessionalInfo = new MemberProfessionalInfo();
            Member member = new Member();
            int memberId1 = 0;
            String memberId = request.getParameter("memberEditId").trim();
            memberId1 = Integer.parseInt(memberId);

            member.setId(memberId1);
            memberProfessionalInfo.setMember(member);
            memberProfessionalInfo.setCompanyName(memProfOrgName);
            memberProfessionalInfo.setDesignation(memProfDesignation);
            memberProfessionalInfo.setFromDate(startDate1);
            memberProfessionalInfo.setTillDate(endDate1);
            memberProfessionalInfo.setCompanyType(comapyType);
            memberProfessionalInfo.setJd(jobDescription);
            dbsession.update(memberProfessionalInfo);
            strMsg = "Information updated successfully.";
        } else if (act.equals("add")) {

            String memProfOrgName = request.getParameter("memProfOrgName").trim();
            String memProfDesignation = request.getParameter("memProfDesignation").trim();
            String startDate = request.getParameter("startDate").trim();
            String endDate = request.getParameter("endDate").trim();
            String comapyType = request.getParameter("comapyType") == null ? "" : request.getParameter("comapyType").trim();
            String jobDescription = request.getParameter("jobDescription") == null ? "" : request.getParameter("jobDescription").trim();

            java.util.Date startDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(startDate);
            java.util.Date endDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(endDate);

            String profInfoId = regId.getID(37);
            memberProfessionalInfo = new MemberProfessionalInfo();
            Member member = new Member();
            int memberId1 = 0;
            int profInfoId1 = 0;
            String memberId = request.getParameter("memberEditId").trim();
            memberId1 = Integer.parseInt(memberId);
            profInfoId1 = Integer.parseInt(profInfoId);

            member.setId(memberId1);
            memberProfessionalInfo.setId(profInfoId1);
            memberProfessionalInfo.setMember(member);
            memberProfessionalInfo.setCompanyName(memProfOrgName);
            memberProfessionalInfo.setDesignation(memProfDesignation);
            memberProfessionalInfo.setFromDate(startDate1);
            memberProfessionalInfo.setTillDate(endDate1);
            memberProfessionalInfo.setCompanyType(comapyType);
            memberProfessionalInfo.setJd(jobDescription);
            dbsession.save(memberProfessionalInfo);
            strMsg = "Information saved successfully.";

        } else if (act.equals("del")) {
            String profInfoId = request.getParameter("profInfoId").trim();
            Query q4 = dbsession.createQuery("delete from MemberProfessionalInfo where id='" + profInfoId + "' ");
            q4.executeUpdate();
            strMsg = "Information removed successfully.";
        }

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            strMsg = "Success!!! Member Professional Information added ";
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=professional&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! When Professional Information added ";
            dbtrx.rollback();
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=professional&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
        return;
    }

%>
