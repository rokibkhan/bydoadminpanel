<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String voterFeeYearId = request.getParameter("voterFeeYearId") == null ? "" : request.getParameter("voterFeeYearId").trim();
        String memberSearchCenterId = request.getParameter("memberSearchCenterId") == null ? "" : request.getParameter("memberSearchCenterId").trim();
        String memberSearchSubCenterId = request.getParameter("memberSearchSubCenterId") == null ? "" : request.getParameter("memberSearchSubCenterId").trim();

        String memberSearchCenterName = request.getParameter("memberSearchCenterName") == null ? "" : request.getParameter("memberSearchCenterName").trim();
        String memberSearchSubCenterName = request.getParameter("memberSearchSubCenterName") == null ? "" : request.getParameter("memberSearchSubCenterName").trim();

    %>

    <script type="text/javascript">

        function showMemebrSearchSubCenterInfo(arg1) {

            var selectedCenterId, selectedCenterText;
            console.log("showMemebrSearchSubCenterInfo arg1:: " + arg1);
            var arg2 = "";

            selectedCenterId = $("#memberSearchCenterId").val();

            if (selectedCenterId != "") {
                selectedCenterText = $("#memberSearchCenterId option:selected").text();
            } else {
                selectedCenterText = "NO";
            }

            console.log("selectedCenterId::" + selectedCenterId);
            console.log("selectCenterText::" + selectedCenterText);

            $.post("memberCenterWiseSubCenterInfoShow.jsp", {centerId: arg1, addressInfo: arg2}, function (data) {

                console.log(data);
//                if (data[0].responseCode == 1) {
//                    $("#showSubCenterInfo").html(data[0].subCenterInfo);
//                }

                $("#showSubCenterInfo").html(data[0].subCenterInfo);


            }, "json");
        }


        function showMemebrSearchCenterNameInfo(arg1) {

            var selectedCenterId, selectedCenterText, selectedCenterName;
            console.log("showMemebrSearchCenterNameInfo arg1:: " + arg1);
            var arg2 = "";

            selectedCenterId = $("#memberSearchCenterId").val();

            if (selectedCenterId != "") {
                selectedCenterText = $("#memberSearchCenterId option:selected").text();
                document.getElementById("memberSearchCenterName").value = selectedCenterText
            } else {
                selectedCenterText = "";
                document.getElementById("memberSearchCenterName").value = "";
            }

            selectedCenterName = $("#memberSearchCenterName").val();

            console.log("selectedCenterId::" + selectedCenterId);
            console.log("selectedCenterText::" + selectedCenterText);
            console.log("selectedCenterName::" + selectedCenterName);

        }


        function showMemebrSearchSubCenterNameInfo(arg1) {

            var selectedSubCenterId, selectedSubCenterText, selectedSubCenterName;
            console.log("showMemebrSearchSubCenterNameInfo arg1:: " + arg1);
            var arg2 = "";

            selectedSubCenterId = $("#memberSearchSubCenterId").val();

            if (selectedSubCenterId != "") {
                selectedSubCenterText = $("#memberSearchSubCenterId option:selected").text();
                document.getElementById("memberSearchSubCenterName").value = selectedSubCenterText
            } else {
                selectedSubCenterText = "";
                document.getElementById("memberSearchSubCenterName").value = "";
            }

            selectedSubCenterName = $("#memberSearchSubCenterName").val();

            console.log("selectedSubCenterId::" + selectedSubCenterId);
            console.log("selectedSubCenterText::" + selectedSubCenterText);
            console.log("selectedSubCenterName::" + selectedSubCenterName);



        }


        $(function () {
            console.log("ready!");


            $('#searchStockItem').click(function () {
                $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListCenterSubmit.jsp?sessionid=<%out.print(sessionid);%>');

                            $("#stockReportFrm").submit();
                        });


                        $('#exportStockItem').click(function () {
                            $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListCenterExport.jsp?sessionid=<%out.print(sessionid);%>');
                                        //    target="_blank"
                                        $('#stockReportFrm').attr('target', '_blank');
                                        $("#stockReportFrm").submit();
                                    });


//        $('input').keydown(function (e) {
//            //      $("input").keypress(function (event) {
//            if (e.which == 13) {
//                e.preventDefault();
//                $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/stockReport.jsp?sessionid=<%out.print(sessionid);%>');
//                $("#stockReportFrm").submit();
//            }
//        });




                                    $("#stockReportFrm").keypress(function (event) {
                                        if (event.which == 13) {
                                            event.preventDefault();
                                            $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListCenterSubmit.jsp?sessionid=<%out.print(sessionid);%>');
                                                            $("#stockReportFrm").submit();
                                                        }
                                                    });



                                                });

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Center Voter List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Center Voter</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">


                    <form class="form-horizontal" name="stockReportFrm" id="stockReportFrm" method="GET">
                        <input type="hidden" id="sessionid" name="sessionid" value="<%out.print(sessionid);%>"> 
                        <input type="hidden" id="memberSearchCenterName" name="memberSearchCenterName" value="<%=memberSearchCenterName%>"> 
                        <input type="hidden" id="memberSearchSubCenterName" name="memberSearchSubCenterName" value="<%=memberSearchSubCenterName%>"> 
                        <div class="row">
                            <div class="col-md-3 offset-md-1">

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-4 font-weight-bold text-right">Fee Year</label>
                                    <div class="col-md-8">  
                                        <select name="voterFeeYearId" id="voterFeeYearId" class="form-control" required="">
                                            <option value="">Select Fee Year</option>
                                            <%                                                            
                                                Object[] feeYearObj = null;
                                                Query feeYearSQL = null;

                                                String feeYearId = "";
                                                String feeYearName = "";
                                                String feeYearOptions = "";
                                                String feeYearOptionsSelect = "";
                                                int length = 0;
                                                feeYearSQL = dbsession.createSQLQuery("select * FROM member_fees_year ORDER BY member_fees_year_id ASC");
                                                System.out.println("feeYearSQL :: " + feeYearSQL);
                                                for (Iterator feeYearItr = feeYearSQL.list().iterator(); feeYearItr.hasNext();) {
                                                    feeYearObj = (Object[]) feeYearItr.next();
                                                    feeYearId = feeYearObj[0].toString();
                                                    // divisionEditName = objectEditDiv[1].toString();
                                                    feeYearName = feeYearObj[1].toString();

                                                    length = feeYearName.length(); // length == 8

                                                    if (!voterFeeYearId.equals("")) {

                                                        if (voterFeeYearId.equals(feeYearName)) {
                                                            feeYearOptionsSelect = " selected";
                                                        } else {
                                                            feeYearOptionsSelect = "";
                                                        }

                                                    }
                                                    if (length == 9) {

                                                        feeYearOptions = feeYearOptions + "<option value=\"" + feeYearName + "\" " + feeYearOptionsSelect + ">" + feeYearName + "</option>";

                                                    }

                                                }
                                            %>
                                            <%=feeYearOptions%>
                                        </select>
                                    </div>
                                </div>                                
                            </div>



                            <div class="col-md-3">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 font-weight-bold text-right">Center</label>
                                    <div class="col-md-9">  
                                        <select  id="memberSearchCenterId" name="memberSearchCenterId" class="form-control input-sm customInput-sm" onchange="showMemebrSearchSubCenterInfo(this.value);showMemebrSearchCenterNameInfo(this.value)" required="">
                                            <option value="">Select Center</option>
                                            <%
                                                Query centerEditSQL = null;
                                                Object[] centerEditObject = null;
                                                String centerEditId = "";
                                                String centerEditName = "";
                                                String centerEditSelected = "";
                                                centerEditSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_type_id ='3' ORDER BY center_id ASC");
                                                for (Iterator centerEditItr = centerEditSQL.list().iterator(); centerEditItr.hasNext();) {
                                                    centerEditObject = (Object[]) centerEditItr.next();
                                                    centerEditId = centerEditObject[0].toString();
                                                    centerEditName = centerEditObject[1].toString();

                                                    if (!memberSearchCenterId.equals("")) {

                                                        if (memberSearchCenterId.equals(centerEditId)) {
                                                            centerEditSelected = " selected";
                                                        } else {
                                                            centerEditSelected = "";
                                                        }

                                                    }

                                            %> 
                                            <option value="<%=centerEditId%>" <%=centerEditSelected%>> <%=centerEditName%> </option> 
                                            <%
                                                }

                                            %>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group d-flex align-items-center">

                                    <label class="form-control-label col-md-4 font-weight-bold text-right">Sub Center</label>
                                    <div class="col-md-8" id="showSubCenterInfo">  
                                        <select  id="memberSearchSubCenterId" name="memberSearchSubCenterId" class="form-control input-sm customInput-sm" onchange="showMemebrSearchSubCenterNameInfo(this.value)">
                                            <option value="">Select Sub Center</option>

                                            <%                                                if (!memberSearchCenterId.equals("")) {

                                                    Query subCenterEditSQL = null;
                                                    Object[] subCenterEditObject = null;
                                                    String subCenterEditId = "";
                                                    String subCenterEditName = "";
                                                    String subCenterEditSelected = "";
                                                    subCenterEditSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_parent_id ='" + memberSearchCenterId + "' ORDER BY center_id ASC");
                                                    for (Iterator subCenterEditItr = subCenterEditSQL.list().iterator(); subCenterEditItr.hasNext();) {
                                                        subCenterEditObject = (Object[]) subCenterEditItr.next();
                                                        subCenterEditId = subCenterEditObject[0].toString();
                                                        subCenterEditName = subCenterEditObject[1].toString();

                                                        if (!memberSearchSubCenterId.equals("")) {

                                                            if (memberSearchSubCenterId.equals(subCenterEditId)) {
                                                                subCenterEditSelected = " selected";
                                                            } else {
                                                                subCenterEditSelected = "";
                                                            }

                                                        }


                                            %> 
                                            <option value="<%=subCenterEditId%>" <%=subCenterEditSelected%>> <%=subCenterEditName%> </option> 
                                            <%
                                                    }
                                                }
                                            %>

                                        </select>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-4 offset-4">      
                                <input type="button"  id="searchStockItem" name="searchStockItem" class="btn btn-info" value="Submit">
                                <input type="button"  id="exportStockItem" name="exportStockItem" class="btn btn-info" value="Export">
                            </div>
                        </div>

                    </form>
                </div>
                <!-- /.white-box -->
            </div>


        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>


    <%@ include file="../footer.jsp" %>