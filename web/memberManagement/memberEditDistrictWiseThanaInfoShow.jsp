<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;
    int responseCode = 0;
    String responseMsg = "";
    String thanaInfo = "";
    String userIdOptX = "";
    String newPass = "";
    String passwordOptToken = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String districtId = request.getParameter("districtId") == null ? "" : request.getParameter("districtId").trim();
    String addressInfo = request.getParameter("addressInfo") == null ? "" : request.getParameter("addressInfo").trim();

    if (districtId != null && addressInfo != null) {

        Query thanaSQL = null;
        Object[] object = null;

        String thanaId = "";
        String thanaName = "";
        String thanaOption = "";
        thanaSQL = dbsession.createSQLQuery("SELECT * FROM thana WHERE DISTRICT_ID='" + districtId + "' ORDER BY THANA_NAME ASC");
        for (Iterator itr = thanaSQL.list().iterator(); itr.hasNext();) {
            object = (Object[]) itr.next();
            thanaId = object[0].toString();
            thanaName = object[1].toString();

            thanaOption = thanaOption + "<option value=\"" + thanaId + "\">" + thanaName + "</option>";

        }

        if (addressInfo.equals("MA")) {
            thanaInfo = "<select  id=\"mAddressThana\" name=\"mAddressThana\" class=\"form-control input-sm customInput-sm\" required>"
                    + "<option value=\"\">Select Thana</option>"
                    + "" + thanaOption + ""
                    + "</select>";
        }
        if (addressInfo.equals("PA")) {
            thanaInfo = "<select  id=\"pAddressThana\" name=\"pAddressThana\" class=\"form-control input-sm customInput-sm\" required>"
                    + "<option value=\"\">Select Thana</option>"
                    + "" + thanaOption + ""
                    + "</select>";
        }

        responseCode = 1;
        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Success!</strong> Thana Info show."
                + "</div>";
        /*
//        System.out.println("ChangePassword userNameH :: " + userNameH);
//        System.out.println("ChangePassword sessionIdH :: " + sessionIdH);
//        System.out.println("ChangePassword userStoreIdH :: " + userStoreIdH);
        if (passwordOptToken.equals(sessionIdH)) {

            System.out.println("Password Token Match:: ");

            String newPassEnc = new Encryption().getEncrypt(newPass);

            String currentUserPass = new getPassWord().getPass(userIdOptX);

            System.out.println("currentUserPass:: " + currentUserPass);

            if (currentUserPass.equals(newPassEnc)) {

                responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                        + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                        + "<strong>Error!</strong> Old password same as new password."
                        + "</div>";

            } else {

                String adduser = userNameH;
                //  String addterm = request.getParameter("entryTerm");
                String addterm = InetAddress.getLocalHost().getHostName().toString();
                //  String addip = request.getParameter("entryIP");
                String addip = InetAddress.getLocalHost().getHostAddress().toString();

                System.out.println("Old Password Match:: ");

                Query q4 = dbsession.createSQLQuery("UPDATE  SY_USER SET USER_PASSWORD='" + newPassEnc + "',MOD_USR_ID='" + adduser + "',MOD_TERM='" + addterm + "',MOD_IP='" + addip + "',LAST_MOD_DATE=now() where user_Id='" + userIdOptX + "'");

                q4.executeUpdate();
                dbsession.flush();
                dbtrx.commit();
                dbsession.close();

                if (dbtrx.wasCommitted()) {
                    //   response.sendRedirect("success.jsp?sessionid=" + sessionid);
                    responseCode = 1;
                    responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<strong>Success!</strong> Password updated."
                            + "</div>";
                } else {
                    dbtrx.rollback();
                    //   response.sendRedirect("fail.jsp?sessionid=" + sessionid);
                    responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<strong>Error!</strong> Please try again</a>."
                            + "</div>";
                }

            }

        } else {

            responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<strong>Error!</strong> Password token not match."
                    + "</div>";
        }
         */

    } else {

        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    json = new JSONObject();
    json.put("addressType", addressInfo);
    json.put("thanaInfo", thanaInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    jsonArr.add(json);
    out.print(jsonArr);

%>