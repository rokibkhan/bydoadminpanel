
<%@page import="java.util.Date"%>
<%@page import="com.appul.entity.AddressBook"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">





<script type="text/javascript">
    function educationDeleteInfo(arg1, arg2) {

        console.log("educationDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"educationDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Education Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function educationDeleteInfoConfirm(arg1, arg2) {
        console.log("educationDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("educationDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#eduInfoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }


    function professionDeleteInfo(arg1, arg2) {

        console.log("professionDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"professionDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Profession Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function professionDeleteInfoConfirm(arg1, arg2) {
        console.log("professionDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("professionDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#proInfoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }

</script>

<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    String memberIdH = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

    System.out.println("memberIdH :: " + memberIdH);

    String tabProfileActive = "";
    String tabAddressActive = "";
    String tabEducationActive = "";
    String tabProfessionalActive = "";
    String tabSettingsActive = "";
    String tabRecomendationActive = "";
    String tabDocumentActive = "";
    String tabChangePictureActive = "";
    String tabChangePassActive = "";

    String selectedTab = request.getParameter("selectedTab") == null ? "" : request.getParameter("selectedTab").trim();
    if (!selectedTab.equals("")) {

        if (selectedTab.equals("profile")) {
            tabProfileActive = "active";
        } else if (selectedTab.equals("address")) {
            tabAddressActive = "active";
        } else if (selectedTab.equals("education")) {
            tabEducationActive = "active";
        } else if (selectedTab.equals("professional")) {
            tabProfessionalActive = "active";
        } else if (selectedTab.equals("settings")) {
            tabSettingsActive = "active";
        } else if (selectedTab.equals("recomendation")) {
            tabRecomendationActive = "active";
        } else if (selectedTab.equals("document")) {
            tabDocumentActive = "active";
        } else if (selectedTab.equals("changePicture")) {
            tabChangePictureActive = "active";
            StringBuffer expurl = request.getRequestURL().append('?').append(request.getQueryString()).append("&pageLoad=2");

            //   int pageLoadnX = 1;
            String pageLoadnX = request.getParameter("pageLoad") == null ? "" : request.getParameter("pageLoad").trim();

//            System.out.println("expurl" + expurl);
//            if (!pageLoadnX.equals("2")) {
//                out.print("<script type=\"text/javascript\">");
//                out.print("window.location.href=\" " + expurl + "\";");
//                out.print("</script>");
//            }
        } else if (selectedTab.equals("documents")) {
            tabChangePassActive = "active";
        }

    } else {
        tabProfileActive = "active";
    }

   


%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Member Info</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Member Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">

                <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <ul class="nav customtab nav-tabs" role="tablist">                            
                            <li role="presentation" class="nav-item"><a href="#profile" class="nav-link <% out.print(tabProfileActive); %>" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Personal Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#changePicture" class="nav-link <% out.print(tabChangePictureActive); %>" aria-controls="changePicture" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Add Picture</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#address" class="nav-link <% out.print(tabAddressActive); %>" aria-controls="address" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Address Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#education" class="nav-link <% out.print(tabEducationActive); %>" aria-controls="education" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Education Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#professional" class="nav-link <% out.print(tabProfessionalActive); %>" aria-controls="professional" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Professional Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#settings" class="nav-link <% out.print(tabSettingsActive); %>" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Payment Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#documents" class="nav-link <% out.print(tabChangePassActive); %>" aria-controls="documents" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Documents Info</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#recomendation" class="nav-link <% out.print(tabRecomendationActive); %>" aria-controls="recomendation" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Recommendation Info</span></a></li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <% out.print(tabProfileActive);%>" id="profile">
                                <h4 class="font-bold1 m-t-30">Personal Information</h4>
                                <hr>
                                
                            </div>
                            <!-- /#profile -->
                            
                            <div class="tab-pane <% out.print(tabChangePictureActive); %>" id="changePicture">


                               

                            </div>
                            <!-- /#changePicture -->

                            <div class="tab-pane <% out.print(tabAddressActive); %>" id="address">



                            </div>
                            <!-- /#address -->
                            <div class="tab-pane <% out.print(tabEducationActive); %>" id="education">

                                <h4 class="font-bold1 m-t-30">Education Info</h4>
                                <hr>
                                

                            </div>
                            <!-- /#education -->
                            <div class="tab-pane <% out.print(tabProfessionalActive); %>" id="professional">

                                <h4 class="font-bold1 m-t-30">Professional Information</h4>
                                <hr>
                                


                            </div>
                            <!-- /#professional-->
                            <div class="tab-pane <% out.print(tabSettingsActive); %>" id="settings">

                                <h4 class="font-bold1 m-t-30">Payment Information</h4>
                                <hr>


                                <!-- .Payment Information-->

                            </div>
                            <!-- /#settings -->   
                            

                            <div class="tab-pane <% out.print(tabChangePassActive); %>" id="documents">



                            </div>
                            <!-- /#documents -->

                            <div class="tab-pane <% out.print(tabRecomendationActive); %>" id="recomendation">

                                <h4 class="font-bold1 m-t-30">Recommendation Info</h4>
                                <hr>



                            </div>
                            <!-- /#recomendation -->



                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>