<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showSyUserDetails(arg1, arg2) {
            console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');
        }


        function syUserInfoUpdateOpt(arg1, arg2) {
            console.log("User :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Update User Details");

            $.post("syUserListUserDetails.jsp", {userId: arg1, sessionid: arg2}, function (data) {
//
//
//                console.log(data);
//
//                console.log("invoiceDetails :: " + data[0].invoiceDetails);
//
//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');

        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">All Type Member List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Member</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <%
                        String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();
                        String searchQuery = "";
                        if (!searchString.equals("")) {
                            searchQuery = "WHERE m.memberName like '%" + searchString + "%' OR m.emailId like '%" + searchString + "%' OR m.memberId like '%" + searchString + "%' or m.mobile like '%" + searchString + "%'";

                        } else {
                            searchQuery = "";
                        }
                    %>

                    <form class="form-group"  name="memberListAll" id="memberListAll" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%out.print(sessionid);%>" >
                        <div class="input-group">
                            <input type="text" id="searchString" name="searchString" value="<%=searchString%>" class="form-control" placeholder="Membership ID Or Name Or Mobile Number"> 
                            <span class="input-group-btn">
                                <button type="submit" name="btnSearch" class="btn waves-effect waves-light btn-info">
                                    <i class="fa fa-search"></i> Search Member
                                </button>
                            </span> 
                        </div>
                    </form>

                    <%
                        dbsession = HibernateUtil.getSessionFactory().openSession();
                        dbtrx = dbsession.beginTransaction();

                        int ix = 1;
                        int memberId = 0;
                        
                        String memberIEBId = "";
                        String memberName = "";
                        String memberPictureName = "";
                        String memberPictureUrl = "";
                        String memberPicture = "";
                        String memberMobile = "";
                        String memberEmail = "";
                        String memberAddress = "";

                        Object memberObj[] = null;
                        Query memberSQL = null;

                        String status1 = "";
                        String reqMemStatus = "";
                        String btnActivePoll = "";
                        String btnCompletePoll = "";
                        String committeeMemberBtn = "";
                        String agrX = "";

//                        memberSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id, "
//                                + " m.member_name, m.father_name, m.mother_name, "
//                                + "m.place_of_birth,m.dob ,m.status,m.picture_name,m.mobile,m.email_id   "
//                                + "FROM  member m "
//                                + " " + searchQuery + " "
//                                + "ORDER BY m.id DESC  LIMIT 0,100");
                        String pageNumberS = request.getParameter("pageNumber") == null ? "1" : request.getParameter("pageNumber").trim();
                        String activeClass = "class=\"active\"";
                        int pageNumber = Integer.parseInt(pageNumberS);
                        int pageSize = 100;
                        memberSQL = dbsession.createQuery("from Member as m   " + searchQuery + "  ORDER BY m.id DESC");
                        memberSQL.setFirstResult((pageNumber - 1) * pageSize);
                        memberSQL.setMaxResults(pageSize);

                       // if (!memberSQL.list().isEmpty()) {
                    %>
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th style="width: 3%;">#</th>
                                    <th class="text-center" style="width: 5%;">Picture</th>
                                    <th class="text-center" style="width: 6%;">IEB ID</th>
                                    <th class="text-center" style="width: 20%;">Name</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    for (Iterator it1 = memberSQL.list().iterator(); it1.hasNext();) {

                                        Member member = (Member) it1.next();
                                        memberId = member.getId();
                                        
                                        memberIEBId = member.getMemberId().toString();
                                        memberName = member.getMemberName().toString();
                                    
                                        memberPictureName = member.getPictureName().toString();
                                        memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPictureName;
                                        memberPicture = "<img width=\"120\" src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\">";

                                        memberMobile = member.getMobile().toString();
                                        memberEmail = member.getEmailId().toString();

                                        String memberPaymentInfoUrl = GlobalVariable.baseUrl + "/memberManagement/memberPaymentHistoryInfo.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
                                        String memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
                                        String memberEditUrl = GlobalVariable.baseUrl + "/memberManagement/memberEdit.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";


                                %>


                                <tr id="infoBox<%=memberId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><%=memberPicture%></td>
                                    <td><%=memberIEBId%></td>
                                    <td><%=memberName%></td>
                                    <td class="text-center"><%=memberMobile%></td>
                                    <td class="text-center"><%=memberEmail%></td>
                                    <td class="text-center"><%=memberAddress%></td>
                                    <td class="text-center">

                                        
                                        <a title="<%=memberName%> details" href="<%=memberDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i> details</a>
                                        <a title="<%=memberName%> details edit" href="<%=memberEditUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i> Edit</a>



                                    </td>
                                </tr>

                                <%
                                        ix++;
                                    }
                                %>




                            </tbody>
                        </table>
                    </div>
                    <%
                      //  }
                        dbsession.clear();
                        dbsession.close();
                        %>


                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <%if (pageNumber == 1) {%>
                            <li class="disabled"><a title="Voucher List" href="#"><i class="fa fa-angle-left"></i></a> </li> <% } %><% else { %>  <li><a title="Add New Employee" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=<%=pageNumber - 1%>"> <i class="fa fa-angle-left"></i></a> </li> <%}%>

                            <li <%if (pageNumber == 1) {
                                    out.println(activeClass);
                                }%>> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=1">1</a> </li>
                            <li <%if (pageNumber == 2) {
                                    out.println(activeClass);
                                }%>> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=2">2</a> </li>
                            <li <%if (pageNumber == 3) {
                                    out.println(activeClass);
                                }%>> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=3">3</a> </li>
                            <li <%if (pageNumber == 4) {
                                    out.println(activeClass);
                                }%>> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=4">4</a> </li>
                            <li <%if (pageNumber == 5) {
                                    out.println(activeClass);
                                }%>> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=5">5</a> </li>
                                <%if (pageNumber > 5) {%> 
                            <li class="disabled"> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=5">...</a> </li>
                            <li <%if (pageNumber > 5) {
                                    out.println(activeClass);
                                }%>> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=<%=pageNumber%>"><%=pageNumber%></a> </li> <%}%>
                            <li <%if (pageNumber == (pageNumber + 1)) {
                                    out.println(activeClass);
                                }%>> <a title="Voucher List" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%=sessionid%>&pageNumber=<%=pageNumber + 1%>"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>