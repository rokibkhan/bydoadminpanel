<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.util.Date" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getRegistryID getId = new getRegistryID();

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = session.getId();
    String userNameH = "";
    String memberIdH = "";

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());

    long uniqueNumber = System.currentTimeMillis();

    String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

    String addterm = InetAddress.getLocalHost().getHostName().toString();
    String addip = InetAddress.getLocalHost().getHostAddress().toString();

    String adduser = "REGISTER";
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String adddate = dateFormatX.format(dateX);

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    String entryDate = dateFormat.format(date);

    String ed = entryDate;
    String td = "2099-12-31";

//        System.out.println("memberIdH " + memberIdH);
    String memberRegId = request.getParameter("memberRegId") == null ? "" : request.getParameter("memberRegId").trim();
    String memberVerifyId = request.getParameter("memberVerifyId") == null ? "" : request.getParameter("memberVerifyId").trim();

    //check varification code
    Query varifySQL = dbsession.createSQLQuery("select * from member_temp WHERE id='" + memberVerifyId + "' AND member_temp_id ='" + memberRegId + "'");
    if (!varifySQL.list().isEmpty()) {
        //success

        //add Address information
        String memberMailingAddressCountry = request.getParameter("memberMailingAddressCountry") == null ? "" : request.getParameter("memberMailingAddressCountry").trim();
        String memberMailingAddressDistrict = request.getParameter("memberMailingAddressDistrict") == null ? "" : request.getParameter("memberMailingAddressDistrict").trim();
        String memberMailingAddressThana = request.getParameter("memberMailingAddressThana") == null ? "" : request.getParameter("memberMailingAddressThana").trim();
        String memberMailingAddressLine1 = request.getParameter("memberMailingAddressLine1") == null ? "" : request.getParameter("memberMailingAddressLine1").trim();
        String memberMailingAddressLine2 = request.getParameter("memberMailingAddressLine2") == null ? "" : request.getParameter("memberMailingAddressLine2").trim();
        String memberMailingAddressPostOffice = request.getParameter("memberMailingAddressPostOffice") == null ? "" : request.getParameter("memberMailingAddressPostOffice").trim();
        String memberMailingAddressPostCode = request.getParameter("memberMailingAddressPostCode") == null ? "" : request.getParameter("memberMailingAddressPostCode").trim();

        String addressCheck = request.getParameter("addressCheck") == null ? "false" : request.getParameter("addressCheck").trim();

        System.out.println("addressCheck: " + addressCheck);

        String memberPermanentAddressCountry = request.getParameter("memberPermanentAddressCountry") == null ? "" : request.getParameter("memberPermanentAddressCountry").trim();
        String memberPermanentAddressDistrict = request.getParameter("memberPermanentAddressDistrict") == null ? "" : request.getParameter("memberPermanentAddressDistrict").trim();
        String memberPermanentAddressThana = request.getParameter("memberPermanentAddressThana") == null ? "" : request.getParameter("memberPermanentAddressThana").trim();
        String memberPermanentAddressLine1 = request.getParameter("memberPermanentAddressLine1") == null ? "" : request.getParameter("memberPermanentAddressLine1").trim();
        String memberPermanentAddressLine2 = request.getParameter("memberPermanentAddressLine2") == null ? "" : request.getParameter("memberPermanentAddressLine2").trim();
        String memberPermanentAddressPostOffice = request.getParameter("memberPermanentAddressPostOffice") == null ? "" : request.getParameter("memberPermanentAddressPostOffice").trim();
        String memberPermanentAddressPostCode = request.getParameter("memberPermanentAddressPostCode") == null ? "" : request.getParameter("memberPermanentAddressPostCode").trim();

        //insert mailing address
        //address_book
        //member_address_temp M
        String idMA = getId.getID(27);
        int regMemberMailingAddressId = Integer.parseInt(idMA);

        Query mailingAddressSQL = dbsession.createSQLQuery("INSERT INTO address_book("
                + "ID,"
                + "ADDRESS_1,"
                + "ADDRESS_2,"
                + "THANA_ID,"
                + "MOD_USER,"
                + "ZIPCODE,"
                + "COUNTRY_CODE,"
                + "ADD_USER,"
                + "ADD_DATE,"
                + "MOD_DATE"
                + ") VALUES("
                + "'" + regMemberMailingAddressId + "',"
                + "'" + memberMailingAddressLine1 + "',"
                + "'" + memberMailingAddressLine2 + "',"
                + "'" + memberMailingAddressThana + "',"
                + "'" + memberMailingAddressPostOffice + "',"
                + "'" + memberMailingAddressPostCode + "',"
                + "'" + memberMailingAddressCountry + "',"
                + "'" + adduser + "',"
                + "'" + adddate + "',"
                + "'" + adddate + "'"
                + "  ) ");

        mailingAddressSQL.executeUpdate();

        Query memberMailingAddressSQL = dbsession.createSQLQuery("INSERT INTO member_address_temp("
                + "id,"
                + "member_id,"
                + "address_id,"
                + "address_type,"
                + "ed,"
                + "td"
                + ") VALUES("
                + "'" + regMemberMailingAddressId + "',"
                + "'" + memberVerifyId + "',"
                + "'" + regMemberMailingAddressId + "',"
                + "'M',"
                + "'" + ed + "',"
                + "'" + td + "'"
                + "  ) ");

        memberMailingAddressSQL.executeUpdate();

        //member_address_temp P
        //insert parmanent address
        //address_book
        String idPA = getId.getID(27);
        int regMemberPermanentAddressId = Integer.parseInt(idPA);
        int regPermanentAddressId = regMemberPermanentAddressId;

        if (addressCheck.equals("on")) {
            regMemberPermanentAddressId = regMemberMailingAddressId;
            
        } else {

            Query permanentAddressSQL = dbsession.createSQLQuery("INSERT INTO address_book("
                    + "ID,"
                    + "ADDRESS_1,"
                    + "ADDRESS_2,"
                    + "THANA_ID,"
                    + "MOD_USER,"
                    + "ZIPCODE,"
                    + "COUNTRY_CODE,"
                    + "ADD_USER,"
                    + "ADD_DATE,"
                    + "MOD_DATE"
                    + ") VALUES("
                    + "'" + regMemberPermanentAddressId + "',"
                    + "'" + memberPermanentAddressLine1 + "',"
                    + "'" + memberPermanentAddressLine2 + "',"
                    + "'" + memberPermanentAddressThana + "',"
                    + "'" + memberPermanentAddressPostOffice + "',"
                    + "'" + memberPermanentAddressPostCode + "',"
                    + "'" + memberPermanentAddressCountry + "',"
                    + "'" + adduser + "',"
                    + "'" + adddate + "',"
                    + "'" + adddate + "'"
                    + "  ) ");

            permanentAddressSQL.executeUpdate();

        }

        Query memberPermanentAddressSQL = dbsession.createSQLQuery("INSERT INTO member_address_temp("
                + "id,"
                + "member_id,"
                + "address_id,"
                + "address_type,"
                + "ed,"
                + "td"
                + ") VALUES("
                + "'" + regPermanentAddressId + "',"
                + "'" + memberVerifyId + "',"
                + "'" + regMemberPermanentAddressId + "',"
                + "'P',"
                + "'" + ed + "',"
                + "'" + td + "'"
                + "  ) ");

        memberPermanentAddressSQL.executeUpdate();

        dbsession.flush();
        dbtrx.commit();

        if (dbtrx.wasCommitted()) {
            strMsg = "Success!!! Address Info Updated";
            response.sendRedirect("memberAddEducationInfo.jsp?sessionid=" + sessionIdH + "&regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=5&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! Address Info Update.Please check and try again.";
            dbtrx.rollback();
            response.sendRedirect("memberAddAddressInfo.jsp?sessionid=" + sessionIdH + "&regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=4&strMsg=" + strMsg);
        }

    } else {
        //error
        strMsg = "Error!!! Address Info Update.Please check and try again.";
        response.sendRedirect("memberAddAddressInfo.jsp?sessionid=" + sessionIdH + "&regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=4&strMsg=" + strMsg);
    }

    dbsession.flush();
    dbsession.close();


%>
