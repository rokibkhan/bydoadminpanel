<%-- 
    Document   : voterListCenterExport
    Created on : October 11, 2019, 9:28:45 PM
    Author     : Tahajjat
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.text.DateFormat"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="net.sf.jasperreports.engine.JasperPrintManager"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = dbsession.beginTransaction();

    String voterFeeYearId = request.getParameter("voterFeeYearId").toString();
    String memberSearchCenterId = request.getParameter("memberSearchCenterId").trim();
    String memberSearchSubCenterId = request.getParameter("memberSearchSubCenterId").trim();
    String centerName = "";
    String centerNameSQL = "";
    String downloadFileNamePrefix = "";
    String downloadFileNamePostfix = "";

    DateFormat dateFormatX = new SimpleDateFormat("yyyyMMddHHmmss");
    Date dateX = new Date();
    downloadFileNamePostfix = dateFormatX.format(dateX);

//    if (!memberSearchCenterId.equals("") && !memberSearchSubCenterId.equals("")) {
//        centerNameSQL = "SELECT center_name  FROM center where center_id = '" + memberSearchSubCenterId + "'";
//        centerName = dbsession.createSQLQuery(centerNameSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(centerNameSQL).uniqueResult().toString();
//    } else {
//        centerNameSQL = "SELECT center_name  FROM center where center_id = '" + memberSearchCenterId + "'";
//        centerName = dbsession.createSQLQuery(centerNameSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(centerNameSQL).uniqueResult().toString();
//    }
    dbsession.clear();
    dbsession.close();

    Session dbsession1 = HibernateUtil.getSessionFactory().openSession();

    File reportFile = null;
    Map parameters = new HashMap();
//            String endDate = sTd.getDate(endDate1);

    SessionImpl sessionImpl = (SessionImpl) dbsession1;
    Connection con = sessionImpl.connection();

    if (!memberSearchCenterId.equals("") && !memberSearchSubCenterId.equals("")) {

        Statement stmtPay = con.createStatement();
        centerNameSQL = "SELECT center_name  FROM center where center_id = '" + memberSearchCenterId + "'";
        ResultSet rsCenter = stmtPay.executeQuery(centerNameSQL);
        while (rsCenter.next()) {
            centerName = rsCenter.getString(1);
        }
        rsCenter.close();

        reportFile = new File(application.getRealPath("/Reports/VoterList_SubCenter.jasper"));
        parameters.put("centerId", memberSearchCenterId);
        parameters.put("subCenterId", memberSearchSubCenterId);
        parameters.put("paymentYear", voterFeeYearId);
        downloadFileNamePrefix = centerName + "_SubCenter_Voter_List";

    } else {
        reportFile = new File(application.getRealPath("/Reports/VoterList_CenterWithSubCenter.jasper"));
        parameters.put("centerId", memberSearchCenterId);
        parameters.put("paymentYear", voterFeeYearId);
        downloadFileNamePrefix = centerName + "_Center_Voter_List";
    }

    JasperPrint jasperPrint = JasperFillManager.fillReport(reportFile.getPath(), parameters, con);
    byte bytes[] = new byte[10000];
    JRXlsExporter exporter = new JRXlsExporter();

    ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
    exporter.exportReport();

    bytes = xlsReport.toByteArray();
    response.setContentType("application/vnd.ms-excel");
    //  response.setHeader("Content-Disposition", "attachment; filename=RPT_03.xls");
    response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileNamePrefix + "_" + downloadFileNamePostfix + ".xls");

    response.setContentLength(bytes.length);

    xlsReport.close();

    OutputStream ouputStream = response.getOutputStream();

    ouputStream.write(bytes, 0, bytes.length);
    ouputStream.flush();
    ouputStream.close();

    con.close();
    dbsession1.close();

%>


