<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

        int ix = 1;

        // String targetpage = "memberListFellow.jsp";
        String targetpageWithSession = "memberListAssociateMember.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        int limit = 500;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        String memberSearchUniversityId = request.getParameter("memberSearchUniversityId") == null ? "" : request.getParameter("memberSearchUniversityId").trim();
        String memberSearchPassingYear = request.getParameter("memberSearchPassingYear") == null ? "" : request.getParameter("memberSearchPassingYear").trim();
        String memberSearchDegreeTypeId = request.getParameter("memberSearchDegreeTypeId") == null ? "" : request.getParameter("memberSearchDegreeTypeId").trim();

        //    out.println("memberSearchPassingYear :: " + memberSearchPassingYear + "<br/>");
        //   out.println("memberSearchUniversityId :: " + memberSearchUniversityId + "<br/>");
        //    out.println("memberSearchDegreeTypeId :: " + memberSearchDegreeTypeId + "<br/>");
        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();

        Query subCenterTopSQL = null;
        Object[] subCenterTopObj = null;
        String subCenterTopId = "";
        String subCenterTopStr = "";
        String scTopNoORStr = "";
        int scTopNo = 1;

        String cenSQLStr = "";
        String cenpStr = "";
        String divSQLStr = "";
        String typeSQLStr = "";
        //only University
        if (!memberSearchUniversityId.equals("") && memberSearchPassingYear.equals("")) {

            filterSQLStr = " AND mei.board_university_id=" + memberSearchUniversityId + " ";
            filterp = "&memberSearchUniversityId=" + memberSearchUniversityId + "&";
            System.out.println("memberSearchUniversityId filterSQLStr :: " + filterSQLStr);
            System.out.println("memberSearchUniversityId filterp :: " + filterp);

        } //only Passing Year
        else if (memberSearchUniversityId.equals("") && !memberSearchPassingYear.equals("")) {

            filterSQLStr = " AND mei.year_of_passing=" + memberSearchPassingYear + " ";
            filterp = "&memberSearchPassingYear=" + memberSearchPassingYear + "&";
            System.out.println("memberSearchPassingYear filterSQLStr :: " + filterSQLStr);
            System.out.println("memberSearchPassingYear filterp :: " + filterp);
        } //University and Passing Year
        else if (!memberSearchUniversityId.equals("") && !memberSearchPassingYear.equals("")) {

            filterSQLStr = " AND mei.board_university_id=" + memberSearchUniversityId + " AND mei.year_of_passing=" + memberSearchPassingYear + " ";
            filterp = "&memberSearchUniversityId=" + memberSearchUniversityId + "&memberSearchPassingYear=" + memberSearchPassingYear + "&";
            System.out.println("UniversityId PassingYear filterSQLStr :: " + filterSQLStr);
            System.out.println("UniversityId PassingYear filterp :: " + filterp);
        } else {

            filterSQLStr = " ";
            filterp = "&memberSearchUniversityId=" + memberSearchUniversityId + "&memberSearchPassingYear=" + memberSearchPassingYear + "&";
            System.out.println("UniversityPassingYear filterSQLStr :: " + filterSQLStr);
            System.out.println("UniversityPassingYear filterp :: " + filterp);
        }

        System.out.println("filterSQLStr :: " + filterSQLStr);

        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
        int pageNbr = Integer.parseInt(pageNbr1);

        System.out.println("filter :: " + filter);
        System.out.println("pageNbr :: " + pageNbr);

        String memberId = "";
        String memberIEBId = "";
        String memberName = "";
        String memberPictureName = "";
        String memberPictureUrl = "";
        String memberPicture = "";
        String memberMobile = "";
        String memberEmail = "";
        String memberAddress = "";

        String status1 = "";
        String mOthersInfo = "";
        String memberUniversityName = "";
        String memberDivisionName = "";
        String memberCenterName = "";
        String memberUniversityShortName = "";
        String memberUniversityLongName = "";
        String memberYearOfPassing = "";
        String committeeMemberBtn = "";
        String agrX = "";
    %>

    <script type="text/javascript">

        function showMemebrSearchSubCenterInfo(arg1) {


            console.log("showMemebrSearchSubCenterInfo arg1:: " + arg1);
            var arg2 = "";

            $.post("memberCenterWiseSubCenterInfoShow.jsp", {centerId: arg1, addressInfo: arg2}, function (data) {

                if (data[0].responseCode == 1) {
                    $("#showSubCenterInfo").html(data[0].subCenterInfo);
                }

            }, "json");
        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">University Wise Search</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">University Search</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">
                    <form class="form-group"  name="memberListAll" id="memberListAll" method="GET" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAssociateMember.jsp?sessionid=<%out.print(sessionid);%>" >

                        <input type="hidden" id="sessionid" name="sessionid" value="<%out.print(sessionid);%>"> 
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 font-weight-bold text-right">University</label>
                                    <div class="col-md-9">  
                                        <select name="memberSearchUniversityId" id="memberSearchUniversityId" class="form-control">
                                            <option value="">Select University</option>
                                            <%
                                                Object[] universityObj = null;
                                                Query universitySQL = null;
                                                String universityId = "";
                                                String universityShortName = "";
                                                String universityLongName = "";
                                                String universityName = "";
                                                String universityOptions = "";
                                                String universityOptionsSelect = "";
                                                universitySQL = dbsession.createSQLQuery("select * FROM university  WHERE university_old_name = 'ACCREDITED' ORDER BY university_long_name ASC");
                                                for (Iterator universityItr = universitySQL.list().iterator(); universityItr.hasNext();) {
                                                    universityObj = (Object[]) universityItr.next();
                                                    universityId = universityObj[0].toString();
                                                    universityShortName = universityObj[1].toString();
                                                    universityLongName = universityObj[2].toString() == null ? "" : universityObj[2].toString();

                                                    universityName = universityLongName;

                                                    if (!memberSearchUniversityId.equals("")) {

                                                        if (memberSearchUniversityId.equals(universityId)) {
                                                            universityOptionsSelect = " selected";
                                                        } else {
                                                            universityOptionsSelect = "";
                                                        }

                                                    }

                                                    universityOptions = universityOptions + "<option value=\"" + universityId + "\" " + universityOptionsSelect + ">" + universityName + "</option>";

                                                }
                                            %>
                                            <%=universityOptions%>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-5 font-weight-bold text-right">Year of Passing</label>
                                    <div class="col-md-7">  
                                        <select id="memberSearchPassingYear" name="memberSearchPassingYear" class="form-control input-sm customInput-sm">
                                            <option value="">Select Year of Passing</option>
                                            <%
                                                String memberSearchYearOptions = "";

                                                // int yearStart = 1960 ;
                                                //  int memberSearchYearEnd = 2019;//current Year
                                                DateFormat dateFormat = new SimpleDateFormat("YYYY");
                                                Date date = new Date();
                                                String memberSearchYearEnd1 = dateFormat.format(date);

                                                int memberSearchYearEnd = Integer.parseInt(memberSearchYearEnd1);

                                                for (int yearStart = 1960; yearStart <= memberSearchYearEnd; yearStart++) {

                                                    memberSearchYearOptions = memberSearchYearOptions + "<option value=\"" + yearStart + "\">" + yearStart + "</option>";
                                                }
                                            %>
                                            <%=memberSearchYearOptions%>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-4 font-weight-bold text-right">Degree</label>
                                    <div class="col-md-8">  
                                        <select id="memberSearchDegreeTypeId" name="memberSearchDegreeTypeId" class="form-control input-sm customInput-sm">
                                            <option value="3">B.Sc. Engg.</option>                                            
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">

                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group d-flex align-items-center">
                                    <div class="col-md-6 offset-md-5">                                                                                           
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.white-box -->
            </div>

            <div class="col-md-12">
                <div class="white-box">




                    <%
                        String searchCountSQL = "SELECT count(*) FROM member_education_info mei, member m,member_type mt "
                                + "WHERE m.status = 1 "
                                + "AND mei.degree_type_id = '3' "
                                + "AND mei.member_id = m.id "
                                + "AND m.id = mt.member_id "
                                + " " + filterSQLStr + " "
                                + "ORDER BY m.id DESC";

                        String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        if (pageNbr != 0) {
                            start = (pageNbr - 1) * limit;
                        } else {
                            start = 0;
                        }

                        System.out.println("startLLLL :: " + start);
                        System.out.println("limitLLLL :: " + limit);

                        // Initial page num setup
                        if (pageNbr == 0) {
                            pageNbr = 1;
                        }
                        prev = pageNbr - 1;
                        next = pageNbr + 1;
                        lastpage1 = Math.ceil((double) total_pages / limit);
                        //   DecimalFormat df = new DecimalFormat("#.##");
                        //  System.out.print(df.format(lastpage1));

                        // lastpage = (int) Math.ceil(total_pages / limit);
                        lastpage = (int) lastpage1;
                        LastPagem1 = (int) Math.round(lastpage - 1);

                        System.out.println("total_pages :: " + total_pages);
                        System.out.println("limit :: " + limit);
                        System.out.println("lastpage1 :: " + lastpage1);
                        System.out.println("lastpage :: " + lastpage);
                        System.out.println("LastPagem1 :: " + LastPagem1);

                        String paginate = "";
                        if (lastpage > 1) {
                            paginate = paginate + "<ul class='pagination' style=\"margin:0\">";

                            // Previous
                            if (pageNbr > 1) {
                                p = "page=" + prev;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>previous</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
                            }

                            int counter = 1;
                            // Pages
                            if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

                                for (counter = 1; counter <= lastpage; counter++) {
                                    if (counter == pageNbr) {
                                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                    } else {
                                        p = "page=" + counter;
                                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                    }
                                }
                            } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
                                // Beginning only hide later pages
                                if (pageNbr < 1 + (stages * 2)) {
                                    for (counter = 1; counter < 4 + (stages * 2); counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // Middle hide some front and some back
                                else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // End only hide early pages
                                else {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                                    for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                }
                            }

                            // Next
                            if (pageNbr < counter - 1) {
                                p = "page=" + next;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>next</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
                            }

                            paginate = paginate + "</ul>";
                        }

                        int minPaginationIndexLimit = ((pageNbr - 1) * limit);
                        int maxPaginationIndexLimit = (pageNbr * limit);
                        int lastProductIndex = total_pages;
                        int minPaginationIndexLimitPlus = minPaginationIndexLimit + 1;
//                        if(minPaginationIndexLimitPlus < limit){
//                            minPaginationIndexLimitPlus = 1;
//                        }

                        if (maxPaginationIndexLimit >= lastProductIndex) {
                            maxPaginationIndexLimit = lastProductIndex;
                        }

                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
                                + "md.mem_division_name,c.center_name,   "
                                + "u.university_short_name,u.university_long_name,mei.year_of_passing  "
                                + "FROM  member_education_info mei, member m ,member_type mt,member_division md,center c,university u "
                                + "WHERE m.status = 1 "
                                + "AND mei.degree_type_id = '3' "
                                + "AND mei.member_id = m.id "
                                + "AND m.member_division_id = md.mem_division_id "
                                + "AND m.center_id = c.center_id "
                                + "AND m.id = mt.member_id "
                                + "AND mei.board_university_id = u.university_id "
                                + " " + filterSQLStr + " "
                                + "ORDER BY m.id DESC  LIMIT " + start + ", " + limit + "";

                        System.out.println("searchCountSQL :: " + searchCountSQL);
                        System.out.println("searchSQL :: " + searchSQL);


                    %>
                    <div class="row">

                        <div class="col-md-8" style="padding: 20px 0;">
                            <%=paginate%>
                        </div>

                        <div class="col-md-4" style="padding-top: 23px;">
                            <p class="text-right">
                                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">#</th>
                                        <th class="text-center" style="width: 5%;">Picture</th>
                                        <th class="text-center" style="width: 6%;">IEB ID</th>
                                        <th class="text-center" style="width: 20%;">Name</th>
                                        <th class="text-center">Mobile</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center" style="width: 20%;">Others Info</th>
                                        <th class="text-center" style="width: 5%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <%
                                        Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                                        if (!searchSQLQry.list().isEmpty()) {
                                            for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                                                searchObj = (Object[]) searchItr.next();
                                                memberId = searchObj[0].toString();
                                                memberIEBId = searchObj[1] == null ? "" : searchObj[1].toString();
                                                memberName = searchObj[2] == null ? "" : searchObj[2].toString();

                                                memberPictureName = searchObj[3] == null ? "" : searchObj[3].toString();
                                                memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPictureName;
                                                memberPicture = "<img width=\"120\" src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\">";

                                                memberMobile = searchObj[4] == null ? "" : searchObj[4].toString();
                                                memberEmail = searchObj[5] == null ? "" : searchObj[5].toString();

                                                memberDivisionName = searchObj[7] == null ? "" : searchObj[7].toString();
                                                memberCenterName = searchObj[8] == null ? "" : searchObj[8].toString();

                                                memberUniversityShortName = searchObj[9] == null ? "" : searchObj[9].toString();
                                                memberUniversityLongName = searchObj[10] == null ? "" : searchObj[10].toString();

                                                memberYearOfPassing = searchObj[11] == null ? "" : searchObj[11].toString();

                                                memberUniversityName = memberUniversityShortName;

                                                mOthersInfo = "<strong>University:</strong>" + memberUniversityName + "<br>"
                                                        + "<strong>Year Of Passing:</strong>" + memberYearOfPassing + "<br>"
                                                        + "<strong>Division:</strong>" + memberDivisionName + "<br>"
                                                        + "<strong>Center:</strong>" + memberCenterName;
                                                String memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
                                    %>
                                    <tr id="infoBox<%=memberId%>">
                                        <td><% out.print(minPaginationIndexLimitPlus);%></td>
                                        <td><%=memberPicture%></td>
                                        <td><%=memberIEBId%></td>
                                        <td><%=memberName%></td>
                                        <td><%=memberMobile%></td>
                                        <td><%=memberEmail%></td>
                                        <td><%=mOthersInfo%></td>
                                        <td class="text-center">

                                            <a title="Details" href="<%=memberDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></a>


                                        </td>
                                    </tr>
                                    <%
                                            minPaginationIndexLimitPlus++;
                                        }
                                    } else {
                                    %>
                                    <tr>
                                        <td colspan="8" class="text-center">There no data found</td>

                                    </tr>
                                    <%
                                        }
                                    %>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <%
                        //  }
                        dbsession.clear();
                        dbsession.close();
                    %>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <%=paginate%>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>