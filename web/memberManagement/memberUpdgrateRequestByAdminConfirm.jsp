<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    //memberId: arg1, sessionid: arg2, oldType: arg3, fellowMemberType: fellowMemberType
    String memberId = request.getParameter("memberId");
    String sessionidNx = request.getParameter("sessionid");
    String oldType = request.getParameter("oldType");
    String fellowMemberType = request.getParameter("fellowMemberType");

    System.out.println("memberUpdgrateRequestByAdminConfirm :: memberId " + memberId);
    System.out.println("memberUpdgrateRequestByAdminConfirm :: oldType " + oldType);
    System.out.println("memberUpdgrateRequestByAdminConfirm :: fellowMemberType " + fellowMemberType);

    q1 = dbsession.createSQLQuery("SELECT * FROM member_upgrade_request WHERE updgrade_type =" + fellowMemberType + " AND member_id =" + memberId + "");
    System.out.println("memberUpdgrateRequestByAdminConfirm :: q1 " + q1);
    if (!q1.list().isEmpty()) {

        responseCode = "0";
        responseMsg = "Error!!! Already request this upgrade type";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", memberId);
        jsonArr.add(json);
    } else {

        //insert member_upgrade_request table
        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(43);
        int upgradeRequestId = Integer.parseInt(idS);

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);

        Date adddate1 = dateFormatX.parse(adddate);

        System.out.println("adddate   adddate:: " + adddate);

        String fileName = "";
        String fileNameNID = "";

        Query upgradeRequestSQL = dbsession.createSQLQuery("INSERT INTO member_upgrade_request("
                + "id,"
                + "member_id,"
                + "updgrade_type,"
                + "document_id,"
                + "document_org_name,"
                + "document_name,"
                + "status,"
                + "request_date"
                + ") values( "
                + "'" + upgradeRequestId + "',"
                + "'" + memberId + "',"
                + "'" + fellowMemberType + "',"
                + "'9',"
                + "'" + fileName + "',"
                + "'" + fileNameNID + "',"
                + "'0',"
                + "'" + adddate + "'"
                + ")");

        System.out.println("memberUpdgrateRequestByAdminConfirm :: upgradeRequestSQL " + upgradeRequestSQL);

        upgradeRequestSQL.executeUpdate();

        dbtrx.commit();

        if (dbtrx.wasCommitted()) {
            //  strMsg = "Success !!! New Committee added";
            //  response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "1";
            responseMsg = "Success!!! Upgrade request added successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
        //    json.put("centerDivisionTagContainer", centerDivisionTagContainer);
            json.put("requestId", memberId);
            jsonArr.add(json);

        } else {
            dbtrx.rollback();
            //   strMsg = "Error!!! When Committee add";
            //   response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "0";
            responseMsg = "Error!!! When aUpgrade request add";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("requestId", memberId);
            jsonArr.add(json);
        }

    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>