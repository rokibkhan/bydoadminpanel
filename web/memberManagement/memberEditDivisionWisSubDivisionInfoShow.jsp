<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;
    int responseCode = 0;
    String responseMsg = "";
    String subDivisionInfo = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String divisionId = request.getParameter("divisionId") == null ? "" : request.getParameter("divisionId").trim();
    //String addressInfo = request.getParameter("addressInfo") == null ? "" : request.getParameter("addressInfo").trim();

    if (divisionId != null) {

        Query subDivisionSQL = null;
        Object[] object = null;

        String subDivisionId = "";
        String subDivisionShortName = "";
        String subDivisionName = "";
        String subDivisionOption = "";
        subDivisionSQL = dbsession.createSQLQuery("SELECT * FROM sub_division WHERE division_id='" + divisionId + "' ORDER BY full_name ASC");
        for (Iterator itr = subDivisionSQL.list().iterator(); itr.hasNext();) {
            object = (Object[]) itr.next();
            subDivisionId = object[0].toString();
            subDivisionName = object[4].toString();

            subDivisionOption = subDivisionOption + "<option value=\"" + subDivisionId + "\">" + subDivisionName + "</option>";

        }

        subDivisionInfo = "<select  id=\"memberSubDivision\" name=\"memberSubDivision\" class=\"form-control input-sm customInput-sm\" required>"
                + "<option value=\"\">Select Sub-Division</option>"
                + "" + subDivisionOption + ""
                + "</select>";

        responseCode = 1;
        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Success!</strong> Division Info show."
                + "</div>";
        /*
//        System.out.println("ChangePassword userNameH :: " + userNameH);
//        System.out.println("ChangePassword sessionIdH :: " + sessionIdH);
//        System.out.println("ChangePassword userStoreIdH :: " + userStoreIdH);
        if (passwordOptToken.equals(sessionIdH)) {

            System.out.println("Password Token Match:: ");

            String newPassEnc = new Encryption().getEncrypt(newPass);

            String currentUserPass = new getPassWord().getPass(userIdOptX);

            System.out.println("currentUserPass:: " + currentUserPass);

            if (currentUserPass.equals(newPassEnc)) {

                responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                        + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                        + "<strong>Error!</strong> Old password same as new password."
                        + "</div>";

            } else {

                String adduser = userNameH;
                //  String addterm = request.getParameter("entryTerm");
                String addterm = InetAddress.getLocalHost().getHostName().toString();
                //  String addip = request.getParameter("entryIP");
                String addip = InetAddress.getLocalHost().getHostAddress().toString();

                System.out.println("Old Password Match:: ");

                Query q4 = dbsession.createSQLQuery("UPDATE  SY_USER SET USER_PASSWORD='" + newPassEnc + "',MOD_USR_ID='" + adduser + "',MOD_TERM='" + addterm + "',MOD_IP='" + addip + "',LAST_MOD_DATE=now() where user_Id='" + userIdOptX + "'");

                q4.executeUpdate();
                dbsession.flush();
                dbtrx.commit();
                dbsession.close();

                if (dbtrx.wasCommitted()) {
                    //   response.sendRedirect("success.jsp?sessionid=" + sessionid);
                    responseCode = 1;
                    responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<strong>Success!</strong> Password updated."
                            + "</div>";
                } else {
                    dbtrx.rollback();
                    //   response.sendRedirect("fail.jsp?sessionid=" + sessionid);
                    responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<strong>Error!</strong> Please try again</a>."
                            + "</div>";
                }

            }

        } else {

            responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<strong>Error!</strong> Password token not match."
                    + "</div>";
        }
         */

    } else {

        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    json = new JSONObject();
    json.put("subDivisionInfo", subDivisionInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    jsonArr.add(json);
    out.print(jsonArr);

%>