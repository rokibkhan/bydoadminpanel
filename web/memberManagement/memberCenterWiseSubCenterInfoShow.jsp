<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;
    int responseCode = 0;
    String responseMsg = "";
    String subCenterInfo = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String centerId = request.getParameter("centerId") == null ? "" : request.getParameter("centerId").trim();
    String addressInfo = request.getParameter("addressInfo") == null ? "" : request.getParameter("addressInfo").trim();

    if (!centerId.equals("")) {

        Query subCenterSQL = null;
        Object[] subCenterObj = null;

        String subCenterId = "";
        String subCenterName = "";
        String subCenterOption = "";
        subCenterSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_parent_id='" + centerId + "' ORDER BY center_name ASC");
        for (Iterator subCenterItr = subCenterSQL.list().iterator(); subCenterItr.hasNext();) {
            subCenterObj = (Object[]) subCenterItr.next();
            subCenterId = subCenterObj[0].toString();
            subCenterName = subCenterObj[1].toString();

            subCenterOption = subCenterOption + "<option value=\"" + subCenterId + "\">" + subCenterName + "</option>";

        }

        subCenterInfo = "<select  id=\"memberSearchSubCenterId\" name=\"memberSearchSubCenterId\" class=\"form-control input-sm customInput-sm\" onchange=\"showMemebrSearchSubCenterNameInfo(this.value)\">"
                + "<option value=\"\">Select Sub Center</option>"
                + "" + subCenterOption + ""
                + "</select>";

        responseCode = 1;
        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Success!</strong> Sub Center Info show."
                + "</div>";

    } else {

        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";
        
        subCenterInfo = "<select  id=\"memberSearchSubCenterId\" name=\"memberSearchSubCenterId\" class=\"form-control input-sm customInput-sm\">"
                + "<option value=\"\">Select Sub Center</option>"
                + "</select>";

    }

    json = new JSONObject();
    json.put("addressType", addressInfo);
    json.put("subCenterInfo", subCenterInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    jsonArr.add(json);
    out.print(jsonArr);

%>