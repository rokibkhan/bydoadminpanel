
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberUpgradeRequest"%>
<%@page import="java.util.Date"%>
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">



<script>





</script>
<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    java.util.Date datefBirth = new Date();
    String placeOfBirth = "";
    String gender = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureName = "";
    String pictureLink = "";

    int membershipApplyingFor = 0;
    String membershipApplyingForName = "";

    String presentIEBmembershipNumber = "";
    String divisionId = "";
    String subDivisionId = "";
    String memberTempPass = "";

    Object objReqMem[] = null;
    String reqMemId = "";
    String reqMemRegId = "";
    String reqMemName = "";
    String reqMemFatherName = "";
    String reqMemMotherName = "";
    String reqMemEmail = "";
    String reqMemMobile = "";

    int reqUpgradeId = 0;
    int reqUpgradeMemberId = 0;
    String reqUpgradeMemberIEBId = "";
    String reqUpgradeMemberName = "";
    String reqUpgradeType = "";
    String reqUpgradeStatus = "";

    String reqUpgradeStatusText = "";

    String reqUpgradeDocumentName = "";
    String reqUpgradeDocumentOrgName = "";

    String tabProfileActive = "";
    String tabActivityActive = "";
    String tabMessagesActive = "";
    String tabSettingsActive = "";
    String tabChangePassActive = "";
    String selUserId = "";
    String selUserName = "";
    String selUserEmail = "";
    String selUserMoble = "";
    String selUserDeptId = "";
    String selUserDept = "";

    MemberUpgradeRequest mupreq = null;

    MemberTemp member = null;

    String selectedUserId = request.getParameter("reqUpgradeId") == null ? "" : request.getParameter("reqUpgradeId").trim();

    System.out.println("UpgradeRequestId :: " + selectedUserId);

    if (!selectedUserId.equals("")) {

        System.out.println("UpgradeRequestId IN :: " + selectedUserId);

        q1 = dbsession.createQuery("from MemberUpgradeRequest WHERE id=" + selectedUserId + " ");

        for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
            mupreq = (MemberUpgradeRequest) itr.next();

            reqUpgradeId = mupreq.getId();

            reqUpgradeMemberId = mupreq.getMember().getId();
            reqUpgradeMemberIEBId = mupreq.getMember().getMemberId();
            reqUpgradeMemberName = mupreq.getMember().getMemberName().toString();

            reqUpgradeType = mupreq.getMemberTypeInfo().getMemberTypeName();

            reqUpgradeStatus = mupreq.getStatus().toString();

            reqUpgradeDocumentName = mupreq.getDocumentName().toString();

            reqUpgradeDocumentOrgName = mupreq.getDocumentOrgName().toString();

            //  String reqUpgradeDocumentNameUrl = GlobalVariable.baseUrl + "/upload/memberUpgradeRequestDetails.jsp?sessionid=" + session.getId() + "&reqUpgradeId=" + reqUpgradeId + "&selectedTab=profile";
            memberName = mupreq.getMember().getMemberName();
            fatherName = mupreq.getMember().getFatherName() == null ? "" : mupreq.getMember().getFatherName().toString();
            motherName = mupreq.getMember().getMotherName() == null ? "" : mupreq.getMember().getMotherName().toString();
            placeOfBirth = mupreq.getMember().getPlaceOfBirth() == null ? "" : mupreq.getMember().getPlaceOfBirth();
            datefBirth = mupreq.getMember().getDob();
            age = mupreq.getMember().getDob() == null ? "" : mupreq.getMember().getDob().toString();
            //           nationality = mupreq.getMember().getNationality == null ? "" : mupreq.getMember().getNationality().toString();

            gender = mupreq.getMember().getGender().trim();
            if (gender.equals("M")) {
                gender = "Male";
            } else {
                gender = "Female";
            }
            mobileNumber = mupreq.getMember().getMobile();
            phone1 = mupreq.getMember().getPhone1();
            phone2 = mupreq.getMember().getPhone2();
            userEmail = mupreq.getMember().getEmailId();

            pictureName = mupreq.getMember().getPictureName();

            pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

        }

//        dbsession.flush();
//        dbsession.close();
        String selectedTab = request.getParameter("selectedTab") == null ? "" : request.getParameter("selectedTab").trim();
        if (!selectedTab.equals("")) {

            if (selectedTab.equals("profile")) {
                tabProfileActive = "active";
            } else if (selectedTab.equals("activity")) {
                tabActivityActive = "active";
            } else if (selectedTab.equals("messages")) {
                tabMessagesActive = "active";
            } else if (selectedTab.equals("settings")) {
                tabSettingsActive = "active";
            } else if (selectedTab.equals("changePass")) {
                tabChangePassActive = "active";
            }

        } else {
            tabProfileActive = "active";
        }
    } else {

        System.out.println("selectedUserId OUt ::");

        //  response.sendRedirect(GlobalVariable.baseUrl + "/home.jsp");
    }






%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><%out.print(selUserId);%> Upgrade Request Member Details</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">User Profile</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">

                <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <ul class="nav customtab nav-tabs" role="tablist">                            
                            <li role="presentation" class="nav-item"><a href="#profile" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profile</span></a></li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">
                                <h4 class="font-bold1 m-t-30">Request Details Information</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form class="form-horizontal form-material1" id="updateProfileFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Request For</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%out.print(reqUpgradeType);%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Member ID</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=reqUpgradeMemberIEBId%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>



                                        </form> 
                                    </div>
                                    <div class="col-md-6">
                                        <form class="form-horizontal form-material1" id="updateProfileFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Date of Birth</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=datefBirth%>"  class="form-control input-sm" disabled>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Age</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" disabled>                                                    
                                                </div>
                                            </div>

                                        </form> 
                                    </div>
                                </div>

                                <h4 class="font-bold1 m-t-30">Personal Information</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form class="form-horizontal form-material1" id="updateProfileFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Full Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%out.print(memberName);%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Father Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=fatherName%>" placeholder="User Name" class="form-control input-sm" required  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Mother Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%out.print(motherName);%>" placeholder="Full Name" class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Gender</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=gender%>"  class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Email</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=userEmail%>"  class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            
                                        </form> 
                                    </div>
                                    <div class="col-md-6">
                                        <form class="form-horizontal form-material1" id="updateProfileFrm">

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Date of Birth</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=datefBirth%>"  class="form-control input-sm" disabled>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Age</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Place of Birth</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=placeOfBirth%>" class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Nationality</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=nationality%>" class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row custom-bottom-margin-5x">
                                                <label for="inputName4" class="control-label col-sm-3">Mobile</label>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<%=mobileNumber%>" class="form-control input-sm"  disabled>
                                                    <div id="syUserNameErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>


                                            
                                        </form> 
                                    </div>
                                </div>

                                <h4 class="font-bold1 m-t-30">Address Info</h4>
                                <hr>

                                <%
                                    Query madrSQL = null;
                                    Object[] madrObject = null;

                                    Query padrSQL = null;
                                    Object[] padrObject = null;

                                    int mAddressId = 0;
                                    String mCountryId = "";
                                    String mDistrictId = "";
                                    String mThanaId = "";
                                    String mThanaName = "";
                                    String mThanaDistrictName = "";
                                    String mAddressLine1 = "";
                                    String mAddressLine2 = "";
                                    String mZipOffice = "";
                                    String mZipCode = "";

                                    int pAddressId = 0;
                                    String pCountryId = "";
                                    String pDistrictId = "";
                                    String pThanaId = "";
                                    String pThanaName = "";
                                    String pThanaDistrictName = "";
                                    String pAddressLine1 = "";
                                    String pAddressLine2 = "";
                                    String pZipOffice = "";
                                    String pZipCode = "";

                                    String mAddressStr = "";
                                    String pAddressStr = "";

                                    Thana thana = null;

//                                    Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address where member_id= " + reqUpgradeMemberId + " and address_Type='M' ) ");
//                                    for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
//                                        madrObject = (Object[]) itr2.next();
//                                        mAddressLine1 = madrObject[1].toString();
//                                        mAddressLine2 = madrObject[2].toString();
//                                        mThanaId = madrObject[3].toString();
//
//                                        Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
//                                        for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
//                                            thana = (Thana) itrmThana.next();
//
//                                            mThanaName = thana.getThanaName();
//                                            mThanaDistrictName = thana.getDistrict().getDistrictName();
//                                        }
//
//                                        mZipCode = madrObject[4].toString();
//                                        mZipOffice = madrObject[8].toString();
//
//                                        mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;
//
//                                    }
//                                    Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address where member_id= " + reqUpgradeMemberId + " and address_Type='P' ) ");
//                                    for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {
//
//                                        padrObject = (Object[]) itr3.next();
//                                        pAddressLine1 = padrObject[1].toString();
//                                        pAddressLine2 = padrObject[2].toString();
//                                        pThanaId = padrObject[3].toString();
//
//                                        Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
//                                        for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
//                                            thana = (Thana) itrpThana.next();
//
//                                            pThanaName = thana.getThanaName();
//                                            pThanaDistrictName = thana.getDistrict().getDistrictName();
//                                        }
//
//                                        pZipCode = padrObject[4].toString();
//                                        pZipOffice = padrObject[8].toString();
//
//                                        pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;
//
//                                    }

                                %>
                                <div class="row">                                      
                                    <div class="col-md-4">
                                        <p class="m-2 text-center"><strong>Mailing Address</strong></p>
                                        <div class="form-row form-group">                                                
                                            <div class="col-8 offset-4"><%=mAddressStr%></div>
                                        </div>
                                    </div>   
                                    <div class="col-md-4">
                                        <p class="m-2 text-center"><strong>Permanent Address</strong></p>
                                        <div class="form-row form-group">                                                
                                            <div class="col-8 offset-4"><%=pAddressStr%></div>
                                        </div>
                                    </div>   
                                </div>

                                <h4 class="font-bold1 m-t-30">Education Info</h4>
                                <hr>
                                <div class="row">                                      
                                    <div class="col-md-12">
                                        <%
                                            Query eduTempSQL = null;
                                            int degreeId = 0;
                                            String degreeName = "";
                                            String instituteName = "";
                                            String boardUniversityName = "";
                                            String yearOfPassing = "";
                                            String resultTypeName = "";
                                            String result = "";

                                            MemberEducationInfo mei = null;

                                            eduTempSQL = dbsession.createQuery("from MemberEducationInfo where  member_id=" + reqUpgradeMemberId + " ");

                                            String memberSubdivisionName = "";
                                            String memberSubdivisionFullName = "";
                                            int educationfInfoId = 0;

                                        %>


                                        <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Degree</th>
                                                    <th scope="col">Board/University</th>
                                                    <th scope="col">Year of Passing</th>
                                                    <th scope="col">Score/Class</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%                                                    for (Iterator eduTempItr = eduTempSQL.list().iterator(); eduTempItr.hasNext();) {
                                                        mei = (MemberEducationInfo) eduTempItr.next();

                                                        educationfInfoId = mei.getId();
                                                        degreeId = mei.getDegree().getDegreeId();
                                                        degreeName = mei.getDegree().getDegreeName();

                                                        instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                        boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                        yearOfPassing = mei.getYearOfPassing();
                                                        resultTypeName = mei.getResultType().getResultTypeName();
                                                        result = mei.getResult();

                                                        //                                    if (degreeId == 3 || degreeId == 4) {
                                                        //
                                                        //                                        q2 = dbsession.createQuery("from MemberTemp as member WHERE id=" + registerAppId + " ");
                                                        //
                                                        //                                        for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                                        //                                            member = (MemberTemp) itr2.next();
                                                        //
                                                        //                                         //   memberSubdivisionName = member.getSubDivision().getSubDivisionName();
                                                        //                                         //   memberSubdivisionFullName = member.getSubDivision().getFullName();
                                                        //                                        // memberSubdivisionName = member.getS
                                                        //                                        }
                                                        //                                    } else {
                                                        memberSubdivisionFullName = "";
                                                        //                                   }

                                                        degreeName = degreeName + memberSubdivisionFullName;
                                                %>
                                                <tr>
                                                    <td><%=degreeName%></td>
                                                    <td><%=boardUniversityName%></td>
                                                    <td><%=yearOfPassing%></td>
                                                    <td><%=result%></td>

                                                </tr>
                                                <%
                                                    }


                                                %>

                                            </tbody>
                                        </table>


                                    </div>   
                                </div>

                                <h4 class="font-bold1 m-t-30">Professional Info</h4>
                                <hr>
                                <div class="row">                                      
                                    <div class="col-md-12">
                                        <%                                    Query profTempSQL = null;
                                            //    Query q2 = null;
                                            int profTempId = 0;
                                            String organizationName = "";
                                            String designationName = "";
                                            String startDateProTemp = "";
                                            String endDateProTemp = "";

                                            MemberProfessionalInfo profTemp = null;

                                            profTempSQL = dbsession.createQuery("from MemberProfessionalInfo where  member_id=" + reqUpgradeMemberId + " ");


                                        %>
                                        <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Organization</th>
                                                    <th scope="col">Designation</th>
                                                    <th scope="col">Start Date</th>
                                                    <th scope="col">End Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%                                                for (Iterator profTempItr = profTempSQL.list().iterator(); profTempItr.hasNext();) {
                                                        profTemp = (MemberProfessionalInfo) profTempItr.next();

                                                        organizationName = profTemp.getCompanyName().toString();
                                                        designationName = profTemp.getDesignation().toString();
                                                        startDateProTemp = profTemp.getFromDate().toString();
                                                        endDateProTemp = profTemp.getTillDate().toString();


                                                %>
                                                <tr>
                                                    <td><%=organizationName%></td>
                                                    <td><%=designationName%></td>
                                                    <td><%=startDateProTemp%></td>
                                                    <td><%=endDateProTemp%></td>

                                                </tr>
                                                <%
                                                    }

                                                %>

                                            </tbody>
                                        </table>


                                    </div>   
                                </div>

                                <h4 class="font-bold1 m-t-30">Documents Info</h4>
                                <hr>
                                <div class="row">                                      
                                    <div class="col-md-11 offset-1">                                       

                                        <div class="form-row form-group">
                                            <label class="form-control-label col-3 text-right" for="inputName" style="">Required Document : </label>


                                            <div class="col-6">
                                                <a href="<%=reqUpgradeDocumentName%>"><%=reqUpgradeDocumentOrgName%></a>
                                            </div>

                                        </div> 




                                    </div>   
                                </div>










                                <div class="row">
                                    <div class="col-md-6 offset-md-4" style="padding-left: 25px;">

                                        <%
                                            if (reqUpgradeStatus.equals("0")) {
                                        %>

                                        <a href="<% out.print(GlobalVariable.baseUrl);%>/memberManagement/memberUpgradeApproveSubmitData.jsp?sessionid=<%=session.getId()%>&memberId=<%=reqUpgradeMemberId%>&act=approve" class="btn btn-primary waves-effect waves-light m-r-5"> <i class="fa fa-check"></i> Approve</a>
                                        <a href="<% out.print(GlobalVariable.baseUrl);%>/memberManagement/memberUpgradeApproveSubmitData.jsp?sessionid=<%=session.getId()%>&memberId=<%=reqUpgradeMemberId%>&act=decline" class="btn btn-danger waves-effect waves-light m-r-5"> <i class="fa fa-check"></i> Decline</a>


                                        <%
                                        } else {
                                        %>
                                        <a class="btn btn-primary waves-effect waves-light m-r-5"> <i class="fa fa-check"></i> Approved</a>

                                        <%
                                            }
                                        %>


                                        <a href="<% out.print(GlobalVariable.baseUrl);%>/memberManagement/requestMemberProfilePrint.jsp?sessionid=<%=session.getId()%>&memberId=<%=reqUpgradeMemberId%>"  target=_blank class="btn btn-success waves-effect waves-light m-r-5"> <i class="fa fa-print"></i> Print</a>                                


                                        <a href="<% out.print(GlobalVariable.baseUrl);%>/memberManagement/memberRequestList.jsp?sessionid=<%=session.getId()%>" class="btn btn-inverse waves-effect waves-light m-r-10"> <i class="fa fa-long-arrow-left"></i> Back</a>



                                    </div>
                                </div>

                            </div>




                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%

        dbsession.clear();

        dbsession.close();

    %>

    <%@ include file="../footer.jsp" %>