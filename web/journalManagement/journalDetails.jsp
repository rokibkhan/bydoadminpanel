

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">


    <script type="text/javascript">




    </script>

    <%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

        Session dbsessionAlbum = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrxAlbum = null;
        dbtrxAlbum = dbsessionAlbum.beginTransaction();

        String journalIdX = request.getParameter("journalId") == null ? "" : request.getParameter("journalId").trim();
    %>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Journal Details</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Journal Management</a></li>                    
                    <li class="active">Journal Details</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">



            <div class="col-md-12">
                <div class="white-box">

                    <%
                        dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                        dbtrx = dbsession.beginTransaction();
                        DateFormat dateFormatJournal = new SimpleDateFormat("yyyy-MM-dd");
                        DateFormat dateFormatJournalM = new SimpleDateFormat("MMM");
                        DateFormat dateFormatJournalD = new SimpleDateFormat("dd");
                        DateFormat dateFormatJournalY = new SimpleDateFormat("Y");

                        Date dateJournalM = null;
                        Date dateJournalD = null;
                        Date dateJournalY = null;
                        String newDateJournalM = "";
                        String newDateJournalD = "";
                        String newDateJournalY = "";

                        Query journalSQL = null;
                        Object journalObj[] = null;
                        String journalId = "";
                        String journalTitle = "";
                        String journalAuthor = "";
                        String journalDetials = "";
                        String journalDivisionName = "";
                        String journalCoverPage = "";
                        String journalCoverPageUrl = "";
                        String journalCoverPage1 = "";

                        String journalDateTime = "";
                        String journalMonth = "";
                        String journalDay = "";
                        String journalYear = "";

                        String journalDownloadUrl = "";
                        String journalDetailsUrl = "";
                        String journalInfoContainer = "";
                        int ix = 1;

                        journalSQL = dbsession.createSQLQuery("SELECT jn.id_journal,jn.journal_title,jn.journal_author,jn.journal_desc,"
                                + "jn.journal_link,jn.journal_date,md.mem_division_name,jn.cover_page_picture "
                                + "FROM journal_info jn,member_division md "
                                + "WHERE jn.id_journal = '" + journalIdX + "' AND jn.journal_division = md.mem_division_id "
                                + "ORDER BY jn.id_journal DESC");

                        System.out.println("SQL:: " + journalSQL);

                        if (!journalSQL.list().isEmpty()) {
                            for (Iterator itJournal = journalSQL.list().iterator(); itJournal.hasNext();) {

                                journalObj = (Object[]) itJournal.next();
                                journalId = journalObj[0].toString().trim();
                                journalTitle = journalObj[1].toString().trim();

                                journalAuthor = journalObj[2].toString().trim();

                                journalDetials = journalObj[3].toString().trim();

                                journalDivisionName = journalObj[6].toString().trim();

                                journalDateTime = journalObj[5].toString().trim();

                                dateJournalM = dateFormatJournal.parse(journalDateTime);
                                newDateJournalM = dateFormatJournalM.format(dateJournalM);

                                dateJournalD = dateFormatJournal.parse(journalDateTime);
                                newDateJournalD = dateFormatJournalD.format(dateJournalD);

                                dateJournalY = dateFormatJournal.parse(journalDateTime);
                                newDateJournalY = dateFormatJournalY.format(dateJournalY);

                                journalDownloadUrl = journalObj[4].toString().trim();

                                journalDetailsUrl = GlobalVariable.baseUrl + "/journalManagement/journalDetails.jsp?journalIdX=" + journalId;

                                journalCoverPage = journalObj[7].toString().trim();

                                journalCoverPageUrl = GlobalVariable.journalDirLink + journalCoverPage;

                                journalCoverPage1 = "<img width=\"250\" src=\"" + journalCoverPageUrl + "\" alt=\"" + journalTitle + "\">";

                                ix++;
                            }

                        }

                    %>





                    <div class="row">
                        <div class="col-md-12">
                            <div class="news_cont_saz" style="padding: 0;">
                                <div class="row">
                                    <div class="col-md-3 bg-primary event_date" style="border-right: 5px solid #ccc">
                                        <%=journalCoverPage1%>
                                    </div>
                                    <div class="col-md-9" style="position: relative; min-height: 220px;">                    
                                        <p><strong>Title :</strong> <%=journalTitle%></p>
                                        <p><strong>Author :</strong> <%=journalAuthor%></p>
                                        <p><strong>Division :</strong> <%=journalDivisionName%></p>
                                        <div>

                                            <a href="<%=journalDownloadUrl%>" target="_blank" class="btn btn-sm btn-primary text-white"> <i class="fas fa-file"></i>  download</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ./end news_cont_saz-->
                        </div>
                        <!-- ./end col-md-12 -->
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="white-box">



                    <%

                        out.println("<h3 class=\"text-center m-10\">Chapter Information</h3>");

                        Query journalChildSQL = null;
                        Object journalChildObj[] = null;
                        String journalChildId = "";
                        String journalChildTitle = "";
                        String journalChildAuthor = "";
                        String journalChildDownloadUrl = "";
                        String journalChildTitleLink = "";
                        String journalChapterListStr = "";

                        String journalChildCoverPage = "";
                        String journalChildCoverPageUrl = "";
                        String journalChildCoverPage1 = "";

                        journalChildSQL = dbsession.createSQLQuery("SELECT jn.id_journal,jn.journal_title,jn.journal_author,jn.journal_desc,"
                                + "jn.journal_link,jn.journal_date,md.mem_division_name,jn.cover_page_picture "
                                + "FROM journal_info jn,member_division md "
                                + "WHERE jn.parent_id = '" + journalId + "' AND jn.journal_division = md.mem_division_id "
                                + "ORDER BY jn.id_journal DESC");

                        System.out.println("Child SQL:: " + journalChildSQL);

                        if (!journalChildSQL.list().isEmpty()) {

                            int chj = 1;
                            for (Iterator itJournalChild = journalChildSQL.list().iterator(); itJournalChild.hasNext();) {
                                journalChildObj = (Object[]) itJournalChild.next();
                                journalChildId = journalChildObj[0].toString().trim();
                                journalChildTitle = journalChildObj[1].toString().trim();
                                journalChildAuthor = journalObj[2].toString().trim();
                                journalChildDownloadUrl = journalObj[4].toString().trim();

                                journalChildCoverPage = journalObj[7].toString().trim();

                                journalChildCoverPageUrl = GlobalVariable.journalDirLink + journalChildCoverPage;

                                journalChildCoverPage1 = "<img width=\"200\" src=\"" + journalChildCoverPageUrl + "\" alt=\"" + journalChildTitle + "\">";

                                journalChildTitleLink = "<a href=\"" + journalDownloadUrl + "\">" + journalChildTitle + "</a>";

                                journalChapterListStr = journalChapterListStr + " " + chj + "." + journalChildTitleLink + "<br>";
                    %>


                    <div class="row" style="margin-top: 10px; border-bottom: 5px solid #003eff;">
                        <div class="col-md-12">
                            <div class="news_cont_saz" style="padding: 0;">
                                <div class="row">
                                    <div class="col-md-3 bg-primary event_date" style="border-right: 5px solid #ccc">
                                        <%=journalChildCoverPage1%>
                                    </div>
                                    <div class="col-md-9" style="position: relative; min-height: 220px;">                    
                                        <p><strong>Title :</strong> <%=journalChildTitle%></p>
                                        <p><strong>Author :</strong> <%=journalChildAuthor%></p>
                                        <div>

                                            <a href="<%=journalChildDownloadUrl%>" target="_blank" class="btn btn-sm btn-primary text-white"> <i class="fas fa-file"></i>  download</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ./end news_cont_saz-->
                        </div>
                        <!-- ./end col-md-12 -->
                    </div>



                    <%
                                chj++;
                            }
                        } else {
                            journalChapterListStr = "";
                        }
                    %>





                </div>
            </div>
            </div>
            <!-- /.row -->


        </div>
        <!-- /.container-fluid -->

        <%    dbsession.clear();
                dbsession.close();
                %>


        <%@ include file="../footer.jsp" %>