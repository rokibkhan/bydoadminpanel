
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">
<link rel="stylesheet" type="text/css" href="css/nobodder.css">

<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<!--<link rel="stylesheet" type="text/css" href="css/stylesheet.css">-->
<script src="jquery/jquery-1.9.1.js"></script>
<script src="jquery/jquery-ui.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" href="jquery/jquery-ui.css" />

<script src="FusionChartV3/fusioncharts.js"></script>
<script src="FusionChartV3/themes/fusioncharts.theme.fint.js"></script>




<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;

    String qryparam = "";
    String qryparam1 = "";

    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>

<%    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>

<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
        width: 600px;
    }
    .bp_valid {
        color:green;
    }
</style>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp", "_top", "");
            }
           
        </script>

        <TITLE>Dashboard</TITLE>
            <%
                /*You need to include the following JS file, if you intend to embed the chart using JavaScript.
                 Embedding using JavaScripts avoids the "Click to Activate..." issue in Internet Explorer
                 When you make your own charts, make sure that the path to this JS file is correct. Else, you would get JavaScript errors.
                 */
            %>

        <style type="text/css">
            <!--
            body {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 10px;
            }
            .text{
                font-family: Arial, Helvetica, sans-serif;
                font-size: 10px;
            }
            -->
        </style>
    </head>
    <body onLoad="breakOut();
                    isDateDisable();">

        Dashboard >&nbsp;&nbsp;<a href="Dashboard.jsp?sessionid=<%out.println(sessionid);%>">Today Summary</a>&nbsp;&nbsp;|
               <a href="DashboardPeriodic.jsp?sessionid=<%out.println(sessionid);%>">Periodic Current Month</a>&nbsp;&nbsp;|&nbsp;&nbsp;
               <br>
        <br>        
        

        <table style="width: 1000px;">	
            <tr>

                <td style="width: 500px;">	
                    <%                        /*
                         In this example, we show how to connect FusionCharts to a database.
                         For the sake of ease, we've used a database which contains two tables, which are linked to each
                         other. 
                         */
                        //Database Objects - Initialization
                        Query st1, st2;

                        String strQuery = "";

                        //strXML will be used to store the entire XML document generated
                        String strXML = "";

                        //Generate the chart element
//				strXML = "<graph caption='Operator wise Report' subCaption='By Minutes' decimalPrecision='0' showNames='1'  numberSuffix=' Min'  pieSliceDepth='30' formatNumberScale='0'>";
                        strXML = "<chart caption='Monthly Product wise Sales Ratio' showpercentvalues='1' palette='2' animation='1' formatnumberscale='1' decimals='0' numberprefix='$' pieslicedepth='30' startingangle='125' showborder='0' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='MonthlySalesRatio'>";

                                //Construct the query to retrieve data
                        dbsession = HibernateUtil.getSessionFactory().openSession();
                        org.hibernate.Transaction dbtrx = null;
                        dbtrx = dbsession.beginTransaction();

//                                strQuery = "select * from Rt_Partners_profile";
                        st1 = dbsession.createSQLQuery("select pc.product_CODE,sum(it.amount) from  appuldb.cart it,appuldb.product pc,appuldb.product_inventory pi  where  it.txn_dt between DATE_SUB(now(),INTERVAL 1 MONTH) and now() and pi.product_id=pc.product_id and it.product_inventory_id=pi.id group by pc.product_CODE order by pc.product_CODE");

                        String operName = null;
                        String duration = null;
                        String operNameDur = null;

                                //Iterate through each factory	
                        List lst = st1.list();
                        Iterator itr = lst.iterator();
                        Object[] obj = null;
                        while (itr.hasNext()) {
                            obj = (Object[]) itr.next();
                            operName = obj[0] == null ? "" : obj[0].toString();
                            duration = obj[1] == null ? "" : obj[1].toString();
//                            operNameDur = operName + " [" + duration + "]";
                            operNameDur = operName;
//                                    System.out.println("operName"+operName+"duration"+duration);
                            strXML += "<set label='" + operNameDur + "' value='" + duration + "'/>";

                        }
                        strXML += "</chart>";
//				dbsession.close();
                        //Create the chart - Pie 3D Chart with data from strXML	
%> 
                    <script type="text/javascript">
                            var test1 = "<%= strXML%>";
                            FusionCharts.ready(function () {

                                var myChart1 = new FusionCharts({
                                    "type": "pie3d",
                                    "renderAt": "chartContainer",
                                    "width": "500",
                                    "height": "280",
                                    "dataFormat": "xml",
//      "dataSource": "<chart caption='Harrys SuperMart' subcaption='Monthly revenue for last year' xaxisname='Month' yaxisname='Amount' numberprefix='$' palettecolors='#008ee4' bgalpha='0' borderalpha='20' canvasborderalpha='0' theme='fint' useplotgradientcolor='0' plotborderalpha='10' placevaluesinside='1' rotatevalues='1' valuefontcolor='#ffffff' captionpadding='20' showaxislines='1' axislinealpha='25' divlinealpha='10' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='Monthly_Revenue'><set label='Jan' value='420000' /><set label='Feb' value='810000' /><set label='Mar' value='720000' /><set label='Apr' value='555000' /><set label='May' value='910000' /><set label='Jun' value='510000' /><set label='Jul' value='680000' /><set label='Aug' value='620000' /><set label='Sep' value='610000' /><set label='Oct' value='490000' /><set label='Nov' value='900000' /><set label='Dec' value='730000' /></chart>"

                                    "dataSource": test1
                                });

                                myChart1.render();
                            });
                    </script>
                    <div id="chartContainer">FusionCharts XT will load here!</div>
                    <!--<div id="indicatorDiv" class="well">Hover on any of the pie slice to view details.</div>-->


                </td>

                <td style="width: 500px;">	
                    <%
                        /*
                         In this example, we show how to connect FusionCharts to a database.
                         For the sake of ease, we've used a database which contains two tables, which are linked to each
                         other. 
                         */

                        //Database Objects - Initialization
                        Query st11, st21;

                        String strQuery1 = "";

                        //strXML will be used to store the entire XML document generated
                        String strXML1 = "";

                        //Generate the chart element
//				strXML = "<graph caption='Operator wise Report' subCaption='By Minutes' decimalPrecision='0' showNames='1'  numberSuffix=' Min'  pieSliceDepth='30' formatNumberScale='0'>";
                        strXML1 = "<chart caption='Yearly Product wise Sales Ratio' showpercentvalues='1' palette='2' animation='1' formatnumberscale='1' decimals='0' numberprefix='' pieslicedepth='30' startingangle='125' showborder='0' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='YearSalesRatio'>";

                                //Construct the query to retrieve data
//                                strQuery = "select * from Rt_Partners_profile";
                        st11 = dbsession.createSQLQuery("select pc.product_CODE,sum(it.amount) from  appuldb.cart it,appuldb.product pc,appuldb.product_inventory pi  where  it.txn_dt between DATE_SUB(now(),INTERVAL 365 DAY) and now() and pi.product_id=pc.product_id and it.product_inventory_id=pi.id group by pc.product_CODE order by pc.product_CODE");

                        String operName1 = null;
                        String duration1 = null;
                        String operNameDur1 = null;

                                //Iterate through each factory	
                        List lst1 = st11.list();
                        Iterator itr1 = lst1.iterator();
                        Object[] obj1 = null;
                        while (itr1.hasNext()) {
                            obj1 = (Object[]) itr1.next();
                            operName1 = obj1[0] == null ? "" : obj1[0].toString();
                            duration1 = obj1[1] == null ? "" : obj1[1].toString();
//                            operNameDur1 = operName1 + " [" + duration1 + "]";
                            operNameDur1 = operName1;
//                                    System.out.println("operName"+operName1+"duration"+duration1);
                            strXML1 += "<set label='" + operNameDur1 + "' value='" + duration1 + "'/>";

                        }
                        strXML1 += "</chart>";
//				dbsession.close();
                        //Create the chart - Pie 3D Chart with data from strXML	
%> 
                    <script type="text/javascript">
                               var test2 = "<%= strXML1%>";
                               FusionCharts.ready(function () {

                                   var myChart2 = new FusionCharts({
                                       "type": "pie3d",
                                       "renderAt": "chartContainer1",
                                       "width": "500",
                                       "height": "280",
                                       "dataFormat": "xml",
//      "dataSource": "<chart caption='Harrys SuperMart' subcaption='Monthly revenue for last year' xaxisname='Month' yaxisname='Amount' numberprefix='$' palettecolors='#008ee4' bgalpha='0' borderalpha='20' canvasborderalpha='0' theme='fint' useplotgradientcolor='0' plotborderalpha='10' placevaluesinside='1' rotatevalues='1' valuefontcolor='#ffffff' captionpadding='20' showaxislines='1' axislinealpha='25' divlinealpha='10' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='Monthly_Revenue'><set label='Jan' value='420000' /><set label='Feb' value='810000' /><set label='Mar' value='720000' /><set label='Apr' value='555000' /><set label='May' value='910000' /><set label='Jun' value='510000' /><set label='Jul' value='680000' /><set label='Aug' value='620000' /><set label='Sep' value='610000' /><set label='Oct' value='490000' /><set label='Nov' value='900000' /><set label='Dec' value='730000' /></chart>"

                                       "dataSource": test2
                                   });

                                   myChart2.render();
                               });
                    </script>
                    <div id="chartContainer1">FusionCharts XT will load here!</div>
                    <!--<div id="indicatorDiv1" class="well">Hover on any of the pie slice to view details.</div>-->

                </td>
            </tr>   


            <tr>



                <td style="width: 500px;">	
                    <%
                        /*
                         In this example, we show how to connect FusionCharts to a database.
                         For the sake of ease, we've used a database which contains two tables, which are linked to each
                         other. 
                         */

                                //Database Objects - Initialization
                        //strXML will be used to store the entire XML document generated
                        String strXML4 = "";

                        //Generate the chart element
//				strXML = "<graph caption='Operator wise Report' subCaption='By Minutes' decimalPrecision='0' showNames='1'  numberSuffix=' Min'  pieSliceDepth='30' formatNumberScale='0'>";
//                                strXML3 = "<graph caption='Mediation Status'  decimalPrecision='0' showNames='1'  numberSuffix=' Files'  pieSliceDepth='30' formatNumberScale='0'>";
//                                strXML5 += "<graph caption='Day wise Traffic Report' xAxisName='Status' yAxisName=' Files' decimalPrecision='0' formatNumberScale='0'>";
                        strXML4 = "<chart caption='Day wise Sales Analysis' valueFontColor='#000000' palettecolors='#0075c2,#1aaf5d' rotatelabels='1' xaxisname='Day' yaxisname='Amount ($)'  numberprefix='' bgalpha='0' borderalpha='20' canvasborderalpha='0' theme='fint' useplotgradientcolor='0' plotborderalpha='10' placevaluesinside='1' rotatevalues='1' captionpadding='20' showaxislines='1' axislinealpha='25' divlinealpha='10' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='DayWiseSales'>";
                        //Construct the query to retrieve data
                        String strCategories = "<categories>";

                        //Initiate <dataset> elements
                        String strDataCurr = "<dataset seriesName='Amount' >";
           

                        Query qDaily = dbsession.createSQLQuery("select date_format(TXN_DT,'%b %d %y'), sum(total_amount),date_format(TXN_DT,'%Y%m%d') from invoice_main where TXN_DT between DATE_SUB(now(),INTERVAL 1 MONTH) and now() group by date_format(TXN_DT,'%b %d %y') order by date_format(TXN_DT,'%Y%m%d') ");

                        String dd = null;
                        String amount = null;
                  

                        List dailyList = qDaily.list();
                        Iterator itr4 = dailyList.iterator();
                        Object[] objdailyList = null;
                        while (itr4.hasNext()) {
                            objdailyList = (Object[]) itr4.next();
                            dd = objdailyList[0] == null ? "" : objdailyList[0].toString();
                            amount = objdailyList[1] == null ? "" : objdailyList[1].toString();
                            

                            strCategories = strCategories + "<category label='" + dd + "' />";
                            strDataCurr = strDataCurr + "<set value='" + amount + "' />";
                       
//                                        System.out.println(dd+nocalls+duration);

                        }
                        //Close <categories> element
                        strCategories = strCategories + "</categories>";

                        //Close <dataset> elements
                        strDataCurr = strDataCurr + "</dataset>";
                     

                        //Assemble the entire XML now
                        strXML4 = strXML4 + strCategories + strDataCurr + "</chart>";

//				dbsession.close();
                        //Create the chart - Pie 3D Chart with data from strXML	
%> 
                    <script type="text/javascript">
                            var test4 = "<%= strXML4%>";
                            FusionCharts.ready(function () {

                                var myChart4 = new FusionCharts({
                                    "type": "dragcolumn2d",
                                    "renderAt": "chartContainer3",
                                    "width": "500",
                                    "height": "280",
                                    "dataFormat": "xml",
//      "dataSource": "<chart caption='Harrys SuperMart' subcaption='Monthly revenue for last year' xaxisname='Month' yaxisname='Amount' numberprefix='$' palettecolors='#008ee4' bgalpha='0' borderalpha='20' canvasborderalpha='0' theme='fint' useplotgradientcolor='0' plotborderalpha='10' placevaluesinside='1' rotatevalues='1' valuefontcolor='#ffffff' captionpadding='20' showaxislines='1' axislinealpha='25' divlinealpha='10' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='Monthly_Revenue'><set label='Jan' value='420000' /><set label='Feb' value='810000' /><set label='Mar' value='720000' /><set label='Apr' value='555000' /><set label='May' value='910000' /><set label='Jun' value='510000' /><set label='Jul' value='680000' /><set label='Aug' value='620000' /><set label='Sep' value='610000' /><set label='Oct' value='490000' /><set label='Nov' value='900000' /><set label='Dec' value='730000' /></chart>"

                                    "dataSource": test4
                                });

                                myChart4.render();
                            });
                    </script>
                    <div id="chartContainer3">FusionCharts XT will load here!</div>


                </td>


                <td style="width: 500px;">	
                    <%
                        /*
                         In this example, we show how to connect FusionCharts to a database.
                         For the sake of ease, we've used a database which contains two tables, which are linked to each
                         other. 
                         */

                                //Database Objects - Initialization
                        //strXML will be used to store the entire XML document generated
                        String strXML5 = "";

                        //Generate the chart element
//				strXML = "<graph caption='Operator wise Report' subCaption='By Minutes' decimalPrecision='0' showNames='1'  numberSuffix=' Min'  pieSliceDepth='30' formatNumberScale='0'>";
//                                strXML3 = "<graph caption='Mediation Status'  decimalPrecision='0' showNames='1'  numberSuffix=' Files'  pieSliceDepth='30' formatNumberScale='0'>";
//                                strXML5 += "<graph caption='Day wise Traffic Report' xAxisName='Status' yAxisName=' Files' decimalPrecision='0' formatNumberScale='0'>";
                        strXML5 = "<chart caption='Month wise Sales Analysis' valueFontColor='#000000' palettecolors='#1aaf5d' rotatelabels='1' xaxisname='Month' yaxisname='Amount ($)'  numberprefix='' bgalpha='0' borderalpha='20' canvasborderalpha='0' theme='fint' useplotgradientcolor='0' plotborderalpha='10' placevaluesinside='1' rotatevalues='1' captionpadding='20' showaxislines='1' axislinealpha='25' divlinealpha='10' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='YearlySales'>";
             //Construct the query to retrieve data
                        String strCategories1 = "<categories>";

                        //Initiate <dataset> elements
                        String strDataCurr1 = "<dataset seriesName='Amount' >";
           

                        Query qMonthly = dbsession.createSQLQuery("select date_format(TXN_DT,'%b %y'),date_format(TXN_DT,'%Y%m'),sum(total_amount) from invoice_main where TXN_DT between DATE_SUB(now(),INTERVAL 365 DAY) and now() group by date_format(TXN_DT,'%b %y') order by date_format(TXN_DT,'%Y%m')");

                         String dd1 = null;
                         String amount1 = null;
                  

                        List monthlyList = qMonthly.list();
                        Iterator itr5 = monthlyList.iterator();
                        Object[] objMonthlyList = null;
                        while (itr5.hasNext()) {
                            objMonthlyList = (Object[]) itr5.next();
                            dd1 = objMonthlyList[0] == null ? "" : objMonthlyList[0].toString();
                            amount1 = objMonthlyList[2] == null ? "" : objMonthlyList[2].toString();
                            

                            strCategories1 = strCategories1 + "<category label='" + dd1 + "' />";
                            strDataCurr1 = strDataCurr1 + "<set value='" + amount1 + "' />";
                       
//                                        System.out.println(dd1+"  ## |##   "+amount1);

                        }
                        //Close <categories> element
                        strCategories1 = strCategories1 + "</categories>";

                        //Close <dataset> elements
                        strDataCurr1 = strDataCurr1 + "</dataset>";
                     

                        //Assemble the entire XML now
                        strXML5 = strXML5 + strCategories1 + strDataCurr1 + "</chart>";

//				dbsession.close();
                        //Create the chart - Pie 3D Chart with data from strXML	
%> 
                    <script type="text/javascript">
                            var test5 = "<%= strXML5%>";
                            FusionCharts.ready(function () {

                                var myChart5 = new FusionCharts({
                                    "type": "dragcolumn2d",
                                    "renderAt": "chartContainer5",
                                    "width": "500",
                                    "height": "280",
                                    "dataFormat": "xml",
//      "dataSource": "<chart caption='Harrys SuperMart' subcaption='Monthly revenue for last year' xaxisname='Month' yaxisname='Amount' numberprefix='$' palettecolors='#008ee4' bgalpha='0' borderalpha='20' canvasborderalpha='0' theme='fint' useplotgradientcolor='0' plotborderalpha='10' placevaluesinside='1' rotatevalues='1' valuefontcolor='#ffffff' captionpadding='20' showaxislines='1' axislinealpha='25' divlinealpha='10' exportenabled='1' exportformats='PNG=Export as High Quality Image|JPG|PDF=Export as PDF File' exporttargetwindow='_self' exportfilename='Monthly_Revenue'><set label='Jan' value='420000' /><set label='Feb' value='810000' /><set label='Mar' value='720000' /><set label='Apr' value='555000' /><set label='May' value='910000' /><set label='Jun' value='510000' /><set label='Jul' value='680000' /><set label='Aug' value='620000' /><set label='Sep' value='610000' /><set label='Oct' value='490000' /><set label='Nov' value='900000' /><set label='Dec' value='730000' /></chart>"

                                    "dataSource": test5
                                });

                                myChart5.render();
                            });
                    </script>
                    <div id="chartContainer5">FusionCharts XT will load here!</div>


                </td>

            </tr>

        </table> 
        <%
                dbsession.flush();
                dbsession.close();
            
        %>

    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>