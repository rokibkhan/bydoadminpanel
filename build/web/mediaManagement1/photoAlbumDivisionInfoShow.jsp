<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String albumId = request.getParameter("albumId");
    String albumName = request.getParameter("albumName");

    System.out.println("photoAlbumDivisionInfoShow :: albumId " + albumId);
    System.out.println("photoAlbumDivisionInfoShow :: albumName " + albumName);

    q1 = dbsession.createSQLQuery("SELECT * FROM picture_category WHERE id_cat=" + albumId + "");
    if (!q1.list().isEmpty()) {

        centerSQL = dbsession.createSQLQuery("select * FROM member_division WHERE mem_division_id !='30' ORDER BY mem_division_id ASC");

        for (Iterator centerItr = centerSQL.list().iterator(); centerItr.hasNext();) {
            centerObj = (Object[]) centerItr.next();
            centerId = centerObj[0].toString();
            centerName = centerObj[3].toString();

            centerOptionCon = centerOptionCon + "<option value=\"" + centerId + "\">" + centerName + "</option>";
        }

        centerInfoCon = "<select  name=\"centerDivisionTagId\" id=\"centerDivisionTagId\" class=\"form-control input-sm customInput-sm\" required>"
                + "<option value=\"\" >Select Division </option>"
                + "" + centerOptionCon + ""
                + "</select>";

        mainInfoContainer = "<div class=\"row\">"
                + "<div class=\"col-md-11 offset-md-1\" id=\"centerDivisionTagAllErr\"></div>"
                + "<div class=\"col-md-11 offset-md-1\">"
                + "<div class=\"form-group row custom-bottom-margin-5x\">"
                + "<label class=\"control-label col-md-2\">Album:</label>"
                + "<div class=\"col-md-10\">"
                + " " + albumName + " "
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row custom-bottom-margin-5x\">"
                + "<label class=\"control-label col-md-2\">Division</label>"
                + "<div class=\"col-md-6\">"
                + " " + centerInfoCon + " "
                + "<div  id=\"centerDivisionTagIdErr\" class=\"help-block with-errors\"></div>"
                + "</div>"
                + "</div>"
                + "</div>"                
                + "</div>";

        responseCode = "1";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("mainInfoContainer", mainInfoContainer);
        json.put("requestId", albumId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! No photo info found";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> Error!!! No photo info found</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", albumId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>