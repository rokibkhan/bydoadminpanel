<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String albumId = request.getParameter("albumId");
    String albumName = request.getParameter("albumName");
    String centerDivisionTagId = request.getParameter("centerDivisionTagId");

    System.out.println("photoAlbumDivisionInfoShowAddConfirm :: albumId " + albumId);
    System.out.println("photoAlbumDivisionInfoShowAddConfirm :: albumName " + albumName);
    System.out.println("photoAlbumDivisionInfoShowAddConfirm :: centerDivisionTagId " + centerDivisionTagId);

    q1 = dbsession.createSQLQuery("SELECT * FROM album_division WHERE album_id=" + albumId + " AND division_id =" + centerDivisionTagId + "");
    System.out.println("photoAlbumDivisionInfoShowAddConfirm :: q1 " + q1);
    if (!q1.list().isEmpty()) {

        responseCode = "0";
        responseMsg = "Error!!! Already album assign this division";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", albumId);
        jsonArr.add(json);
    } else {

        //insert album id and center id in album_center table
        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(51);
        int albumCenterId = Integer.parseInt(idS);

        Query albumCenterAddSQL = dbsession.createSQLQuery("INSERT INTO album_division("
                + "ID,"
                + "album_id,"
                + "division_id"
                + ") VALUES("
                + "'" + albumCenterId + "',"
                + "'" + albumId + "',"
                + "'" + centerDivisionTagId + "'"
                + "  ) ");

        System.out.println("photoAlbumDivisionInfoShowAddConfirm :: albumCenterAddSQL " + albumCenterAddSQL);

        albumCenterAddSQL.executeUpdate();

        dbtrx.commit();

        if (dbtrx.wasCommitted()) {
            //  strMsg = "Success !!! New Committee added";
            //  response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "1";
            responseMsg = "Success!!! Album add for division successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            // show album Assign center,division,tag
            String centerDivisionTagContainer = "";

            String centerListContainer = "";
            AlbumCenter alc = null;
            String centerListCenterName = "";
            Query centerListSQL = null;

            String divisionListContainer = "";
            AlbumDivision ald = null;
            String divisionListCenterName = "";
            Query divisionListSQL = null;

            String tagListContainer = "";
            AlbumSlug alt = null;
            String tagListCenterName = "";
            Query tagListSQL = null;

            centerListSQL = dbsession.createQuery(" FROM AlbumCenter where pictureCategory = '" + albumId + "'");
            if (!centerListSQL.list().isEmpty()) {
                for (Iterator centerListItr = centerListSQL.list().iterator(); centerListItr.hasNext();) {
                    alc = (AlbumCenter) centerListItr.next();
                    centerListCenterName = alc.getCenter().getCenterName();

                    centerListContainer = centerListContainer + centerListCenterName + ",";
                }
            } else {
                centerListContainer = "";
            }

            divisionListSQL = dbsession.createQuery(" FROM AlbumDivision where pictureCategory = '" + albumId + "'");
            if (!divisionListSQL.list().isEmpty()) {
                for (Iterator divisionListItr = divisionListSQL.list().iterator(); divisionListItr.hasNext();) {
                    ald = (AlbumDivision) divisionListItr.next();
                    divisionListCenterName = ald.getMemberDivision().getMemDivisionName();

                    divisionListContainer = divisionListContainer + divisionListCenterName + ",";
                }
            } else {
                divisionListContainer = "";
            }

//            tagListSQL = dbsession.createQuery(" FROM AlbumSlug where pictureCategory = '" + albumId + "'");
//            if (!tagListSQL.list().isEmpty()) {
//                for (Iterator tagListItr = tagListSQL.list().iterator(); tagListItr.hasNext();) {
//                    alt = (AlbumSlug) tagListItr.next();
//                    tagListCenterName = alt.get
//
//                    tagListContainer = tagListContainer + tagListCenterName + ",";
//                }
//            } else {
//                tagListContainer = "";
//            }
            centerDivisionTagContainer = centerListContainer + divisionListContainer + tagListContainer;

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("centerDivisionTagContainer", centerDivisionTagContainer);
            json.put("requestId", albumId);
            jsonArr.add(json);

        } else {
            dbtrx.rollback();
            //   strMsg = "Error!!! When Committee add";
            //   response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "0";
            responseMsg = "Error!!! When album assign this center";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("requestId", albumId);
            jsonArr.add(json);
        }

    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>