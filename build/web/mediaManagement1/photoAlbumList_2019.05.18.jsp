

<%@page import="com.appul.entity.AlbumSlug"%>
<%@page import="com.appul.entity.AlbumDivision"%>
<%@page import="com.appul.entity.AlbumCenter"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">


        function photoAlbumDeleteInfo(arg1, arg2) {
            console.log("photoAlbumDeleteInfo :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"photoAlbumDeleteInfoConfirmBtn\" onclick=\"photoAlbumDeleteInfoConfirm(" + arg1 + ",'" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');
        }

        function photoAlbumDeleteInfoConfirm(arg1, arg2) {
            console.log("photoAlbumDeleteInfoConfirm :: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');


            $.post("photoAlbumDeleteInfoProcess.jsp", {deleteItemId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");
        }

        function addAlbumForCenter(arg1, arg2, arg3) {
            console.log("addAlbumForCenter :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addAlbumForCenterConfirmBtn\" onclick=\"addAlbumForCenterConfirm(" + arg1 + ",'" + arg2 + "','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Center for Album");

            $.post("photoAlbumCenterInfoShow.jsp", {albumId: arg1, albumName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addAlbumForCenterConfirm(arg1, arg2, arg3) {
            console.log("addAlbumForCenterConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Center is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("photoAlbumCenterInfoAddConfirm.jsp", {albumId: arg1, albumName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }



        function addAlbumForDivision(arg1, arg2, arg3) {
            console.log("addAlbumForDivision :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addAlbumForDivisionConfirmBtn\" onclick=\"addAlbumForDivisionConfirm(" + arg1 + ",'" + arg2 + "','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Division for Album");

            $.post("photoAlbumDivisionInfoShow.jsp", {albumId: arg1, albumName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addAlbumForDivisionConfirm(arg1, arg2, arg3) {
            console.log("addAlbumForDivisionConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Division is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("photoAlbumDivisionInfoAddConfirm.jsp", {albumId: arg1, albumName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Manage Album</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Photo Management</a></li>                    
                    <li class="active">Photo Album</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Album Name</th>
                                    <th class="text-center">Short Name</th>
                                    <th class="text-center">Assign Album</th>
                                    <th class="text-center" style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession(); //openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String photoAlbumId = "";
                                    String photoAlbumName = "";
                                    String photoAlbumShortName = "";
                                    String photoAlbumUrl = "";

                                    String pictureThumb = "";
                                    String showingOrder = "";
                                    String photoAlbum = "";
                                    String photoSrc = "";
                                    String photoLink = "";
                                    String agrX = "";
                                    String agrXcenter = "";
                                    String btnAddCenter = "";
                                    String btnAddSubCenter = "";
                                    String btnAddDivision = "";
                                    String btnAddNews = "";
                                    String btnAddEvent = "";
                                    String btnAddTag = "";

                                    String centerListContainer = "";
                                    AlbumCenter alc = null;
                                    String centerListCenterName = "";
                                    Query centerListSQL = null;

                                    String divisionListContainer = "";
                                    AlbumDivision ald = null;
                                    String divisionListCenterName = "";
                                    Query divisionListSQL = null;

                                    String tagListContainer = "";
                                    AlbumSlug alt = null;
                                    String tagListCenterName = "";
                                    Query tagListSQL = null;

                                    Query usrSQL = dbsession.createSQLQuery("SELECT * FROM picture_category pc "
                                            + "ORDER BY pc.id_cat DESC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            photoAlbumId = obj[0].toString().trim();
                                            photoAlbumName = obj[1].toString().trim();
                                            photoAlbumShortName = obj[2].toString().trim();

                                            //     photoSrc = GlobalVariable.imagePhotoDirLink + pictureName;
                                            //    photoLink = "<img src=\"" + photoSrc + "\" width=\"150\" height=\"100\"/>";
                                            agrX = "'" + photoAlbumId + "','" + session.getId() + "'";
                                            photoAlbumUrl = GlobalVariable.baseUrl + "/photoManagement/photoAlbumEdit.jsp?sessionid=" + session.getId() + "&photoAlbumId=" + photoAlbumId;

                                            agrXcenter = "'" + photoAlbumId + "','" + photoAlbumName + "','" + session.getId() + "'";

                                            btnAddCenter = "<a onClick=\"addAlbumForCenter(" + agrXcenter + ");\" title=\"Add Center  for this Album\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Center</a>";
                                            btnAddSubCenter = "<a title=\"Add SubCenter  for this Album\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Sub Center</a>";

                                            btnAddDivision = "<a onClick=\"addAlbumForDivision(" + agrXcenter + ");\" title=\"Add Division  for this Album\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Division</a>";

                                            btnAddNews = "<a title=\"Add News  for this Album\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add News</a>";
                                            btnAddEvent = "<a title=\"Add Event  for this Album\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Event</a>";
                                            btnAddTag = "<a title=\"Add Tag  for this Album\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Tag</a>";

                                            // show album Assign center,division,tag
                                            centerListSQL = dbsession.createQuery(" FROM AlbumCenter where pictureCategory = '" + photoAlbumId + "'");
                                            if (!centerListSQL.list().isEmpty()) {
                                                for (Iterator centerListItr = centerListSQL.list().iterator(); centerListItr.hasNext();) {
                                                    alc = (AlbumCenter) centerListItr.next();
                                                    centerListCenterName = alc.getCenter().getCenterName();

                                                    centerListContainer = centerListContainer + centerListCenterName + ",";
                                                }
                                            } else {
                                                centerListContainer = "";
                                            }

                                            divisionListSQL = dbsession.createQuery(" FROM AlbumDivision where pictureCategory = '" + photoAlbumId + "'");
                                            if (!divisionListSQL.list().isEmpty()) {
                                                for (Iterator divisionListItr = divisionListSQL.list().iterator(); divisionListItr.hasNext();) {
                                                    ald = (AlbumDivision) divisionListItr.next();
                                                    divisionListCenterName = ald.getMemberDivision().getMemDivisionName();

                                                    divisionListContainer = divisionListContainer + divisionListCenterName + ",";
                                                }
                                            } else {
                                                divisionListContainer = "";
                                            }
                                %>


                                <tr id="infoBox<%=photoAlbumId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(photoAlbumName);%></td>
                                    <td><%=photoAlbumShortName%></td>
                                    <td class="text-center" id="assignBox<%=photoAlbumId%>">
                                        <%=centerListContainer%> <br/>
                                        <%=divisionListContainer%>
                                    </td>
                                    <td class="text-left" style="padding-left: 10px;">
                                        <%=btnAddCenter%>
                                        <%=btnAddDivision%>
                                        <%=btnAddNews%>
                                        <%=btnAddEvent%>
                                        <%=btnAddTag%>
                                        <%=btnAddSubCenter%>
                                        <a href="<%=photoAlbumUrl%>" title="Edit Album" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onClick="photoAlbumDeleteInfo(<%=agrX%>);" title="Delete Photo" class="btn btn-primary btn-sm"><i class="icon-trash"></i></a>
                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->


    <%    dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>