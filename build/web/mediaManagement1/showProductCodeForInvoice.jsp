<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String productId = "";
    String productClassId = "";
    String productName = "";
    String productGrade = "";
    String productCode = "";
    String uom1 = "";
    String uom2 = "";
    String uom1Id = "";
    String uom2Id = "";
    double quantity1 = 0.00d;
    String stockQuantity1 = "0";
//    String uom1ConversionUom2 = "";
    double uom1ConversionUom2 = 0.00000000d;
    String avgBuyingPrice = "";
    String name = request.getParameter("productId");
    String sellingUnit = request.getParameter("sellingUnit").trim();
    System.out.println("sellingUnit: " + sellingUnit);
    String userStoreId = session.getAttribute("storeId").toString();
    String productSizeCm = "";
    String productSizeInch = "";
    String length = "";
    String weidth = "";
    String orderStock = "0";
    String actualConversionCmSft = "0";
    String[] productDimension = new String[2];
    Query q2 = null;

    q1 = dbsession.createSQLQuery("select  pc.id,p.product_id,p.product_code,p.product_name,pd.product_grade ,c.uom2 uom1,round((c.qty1)/qty2,6) conversion,c.uom1 uom2,pu.id uom1Id,c.id uom2Id,pi.avg_buying_price,quantity1,product_size_cm,product_size_inch "
            + "from product p, product_class pc,product_class_defination pd,carton c,product_uom pu,product_inventory pi,product_size ps  where  p.PRODUCT_ID=pc.product_id "
            + "and pc.class_id=pd.id  and p.PRODUCT_UOM2=c.id and p.PRODUCT_UOM1=pu.id and quantity1>0 and p.product_size_id=ps.id and  "
            + "p.STORE_ID=" + userStoreId + " and (upper(p.product_code) like upper('%" + name + "%') "
            + "or upper(p.product_name) like upper('%" + name + "%') ) and pi.product_class_id=pc.id order by p.product_code ");
    if (!q1.list().isEmpty()) {
        for (Iterator it = q1.list().iterator(); it.hasNext();) {

            obj = (Object[]) it.next();
            productClassId = obj[0].toString().trim();
            productId = obj[1].toString().trim();
            productCode = obj[2].toString();
            productName = obj[3].toString();
            productGrade = obj[4].toString();
            uom1 = obj[5].toString();
            actualConversionCmSft = obj[6].toString();
            uom2 = obj[7].toString();
            uom1Id = obj[8].toString();
            uom2Id = obj[9].toString();
            avgBuyingPrice = obj[10].toString();
            q2 = dbsession.createSQLQuery("select IFNULL(sum(it.quantity1),0) from invoice_items_buffer it where  it.product_class_id='" + productClassId + "' ");
            orderStock = q2.uniqueResult().toString();
            stockQuantity1 = obj[11].toString();
            quantity1 = (Double.parseDouble(stockQuantity1) - Double.parseDouble(orderStock));
            orderStock = "0";
            productSizeCm = obj[12].toString();
            productSizeInch = obj[13].toString();
            if (sellingUnit.equalsIgnoreCase("0")) {
                productDimension = productSizeInch.split("x");
                length = productDimension[0];
                weidth = productDimension[1];
                uom1ConversionUom2 = Double.parseDouble(length) * Double.parseDouble(weidth) / 144;
                System.out.println("uom1ConversionUom2: " + length + "x" + weidth + ": " + uom1ConversionUom2);
            } else {
                uom1ConversionUom2 = Double.parseDouble(actualConversionCmSft);
                System.out.println("uom1ConversionUom2: " + length + "x" + weidth + ": " + uom1ConversionUom2);
            }

            json = new JSONObject();
//            json.put("name", productClassId + " | " + productCode + " | " + productName + " | " + productGrade + " | " + uom1 + " | " + uom1ConversionUom2 + " | " + uom2+" | "+avgBuyingPrice+" | "+quantity1+ " " + uom1);
//            json.put("value", productClassId + " | " + productCode + " | " + productGrade + " | " + uom1Id+" | "+uom1 + " | " + uom1ConversionUom2 + " | " + uom2Id+" | "+uom2+" | "+avgBuyingPrice+" | "+quantity1+ " " + uom1);
            json.put("name", productClassId + " | " + productCode + " | " + productName + " | " + productGrade);
            json.put("value", productClassId + " | " + productCode + " | " + productName + " | " + productGrade);

            json.put("stock", quantity1);
            json.put("uom1Id", uom1Id);
            json.put("uom1", uom1);
            json.put("uom1ConversionUom2", uom1ConversionUom2);
            json.put("actualConversionCmSft", actualConversionCmSft);
            json.put("uom2Id", uom2Id);
            json.put("uom2", uom2);
            json.put("avgBuyingPrice", avgBuyingPrice);
            jsonArr.add(json);
            productSizeCm = "";
            productSizeInch = "";
            length = "";
            weidth = "";

        }
    } else {
        json = new JSONObject();
        json.put("name", "No data found");
        json.put("value", "");
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>