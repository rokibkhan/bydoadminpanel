<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Query q2 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String deleteItemId = request.getParameter("deleteItemId");
    System.out.println("deleteItemId: " + deleteItemId);

    q1 = dbsession.createSQLQuery("SELECT * FROM picture_gallery_info WHERE picture_category=" + deleteItemId + "");
    if (q1.list().isEmpty()) {

        q2 = dbsession.createSQLQuery("DELETE FROM picture_category WHERE id_cat=" + deleteItemId + "");

        q2.executeUpdate();
        dbtrx.commit();
        if (dbtrx.wasCommitted()) {
            responseCode = "1";
            responseMsg = "Success!!! Photo Album info delete successflly";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

        } else {
            responseCode = "0";
            responseMsg = "Error!!! When photo album delete from system";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";
        }

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", deleteItemId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! Please delete photo this album first.Then delete album";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", deleteItemId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>