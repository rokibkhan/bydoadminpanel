

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        

        function photoDeleteInfo(arg1, arg2) {
            console.log("photoDeleteInfo :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"photoDeleteInfoConfirmBtn\" onclick=\"photoDeleteInfoConfirm(" + arg1 + ",'" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');
        }

        function photoDeleteInfoConfirm(arg1, arg2) {
            console.log("photoDeleteInfoConfirm :: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');


            $.post("photoInfoDeleteProcess.jsp", {deleteItemId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");
        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Manage Photos</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Photo Management</a></li>                    
                    <li class="active">Photos</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Caption</th>
                                    <th class="text-center">Album</th>
                                    <th class="text-center">Picture</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession(); //openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String pictureId = "";
                                    String pictureTitle = "";
                                    String pictureCaption = "";
                                    String pictureOrginalName = "";
                                    String pictureName = "";
                                    String pictureThumb = "";
                                    String showingOrder = "";
                                    String photoAlbum = "";
                                    String photoSrc = "";
                                    String photoLink = "";
                                    String agrX = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT "
                                            + "pg.id_picture,pg.picture_title,pg.picture_caption ,pg.picture_orginal_name,pg.picture_name,pg.picture_thumb ,pg.showing_order, "
                                            + "pc.category_name "
                                            + "FROM  picture_gallery_info pg,picture_category pc "
                                            + "WHERE pc.id_cat = pg.picture_category "
                                            + "ORDER BY pg.id_picture DESC");

                                    //+ "LEFT JOIN picture_category pc ON pc.id_cat = pg.picture_category "
                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            pictureId = obj[0].toString().trim();
                                            pictureTitle = obj[1].toString().trim();
                                            pictureCaption = obj[2].toString().trim();
                                            pictureOrginalName = obj[3].toString().trim();
                                            pictureName = obj[4].toString().trim();
                                            pictureThumb = obj[5].toString().trim();
                                            showingOrder = obj[6].toString().trim();
                                            photoAlbum = obj[7].toString().trim();

                                            photoSrc = GlobalVariable.imagePhotoDirLink + pictureName;
                                            photoLink = "<img src=\"" + photoSrc + "\" width=\"150\" height=\"100\"/>";

                                            agrX = "'" + pictureId + "','" + session.getId() + "'";
                                %>


                                <tr id="infoBox<%=pictureId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(photoAlbum);%></td>
                                    <td><% out.print(pictureCaption);%></td>
                                    <td><%=photoLink%></td>
                                    <td class="text-center">
                                        <a href="<% //out.print(updateProfileUrl); %>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onClick="photoDeleteInfo(<%=agrX%>);" title="Delete Photo" class="btn btn-primary btn-sm"><i class="icon-trash"></i></a>
                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->




    <%@ include file="../footer.jsp" %>