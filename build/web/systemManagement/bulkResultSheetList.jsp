

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">
    <%    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
        dbtrx = dbsession.beginTransaction();

        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
            //        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

    %>

    <script type="text/javascript">
        //     $(function(){
        function bulkResultSheetDeleteInfo(arg1, arg2) {
            console.log("bulkResultSheetDeleteInfo :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"bulkResultSheetDeleteInfoConfirmBtn\" onclick=\"bulkResultSheetDeleteInfoConfirm(" + arg1 + ",'" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');
        }

        function bulkResultSheetDeleteInfoConfirm(arg1, arg2) {
            console.log("bulkResultSheetDeleteInfoConfirm :: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');


            $.post("bulkResultSheetDeleteInfoProcess.jsp", {deleteItemId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");
        }
        

        

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Bulk Result Sheet List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">System Management</a></li>                    
                    <li class="active">Result Sheet</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center">Result Sheet Name</th>
                                    <th class="text-center">Records</th>
                                    <th class="text-center">University</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center" style="width: 16%">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%

                                    int ix = 1;
                                    Query resultFileSQL = null;
                                    Object resultFileObj[] = null;
                                    String resultFileId = "";
                                    String resultFileName = "";
                                    String resultFileRecCount = "";
                                    String resultFileStatus = "";
                                    String resultFileStatusText = "";
                                    String resultFileUniversityName = "";

                                    String resultFileDetailsUrl = "";
                                    String resultFileDeleteUrl = "";
                                    String argX = "";

                                    resultFileSQL = dbsession.createSQLQuery("SELECT uf.id,uf.file_name,uf.rec_count,uf.status,u.university_long_name "
                                            + "FROM university_result_file uf,university u "
                                            + "WHERE uf.university_id = u.university_id "
                                            + "ORDER BY uf.id DESC");

                                    System.out.println("resultFileSQL::" + resultFileSQL);

                                    if (!resultFileSQL.list().isEmpty()) {
                                        for (Iterator it1 = resultFileSQL.list().iterator(); it1.hasNext();) {

                                            resultFileObj = (Object[]) it1.next();
                                            resultFileId = resultFileObj[0].toString().trim();
                                            resultFileName = resultFileObj[1].toString().trim();
                                            resultFileRecCount = resultFileObj[2].toString().trim();
                                            resultFileStatus = resultFileObj[3].toString().trim();

                                            if (resultFileStatus.equals("1")) {
                                                resultFileStatusText = "<span class=\"label label-rounded label-success\">Already Processed</span>";
                                            } else {
                                                resultFileStatusText = "<span class=\"label label-rounded label-info\">Waiting for Process</span>";
                                            }

                                            resultFileUniversityName = resultFileObj[4].toString().trim();

                                            argX = "'" + resultFileId + "','" + session.getId() + "'";

                                            resultFileDetailsUrl = GlobalVariable.baseUrl + "/systemManagement/bulkResultSheetDetails.jsp?sessionid=" + session.getId() + "&resultFileIdX=" + resultFileId;
                                            resultFileDeleteUrl = GlobalVariable.baseUrl + "/systemManagement/accereditedUniversityListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&resultFileIdX=" + resultFileId;


                                %>


                                <tr id="infoBox<%=resultFileId%>">
                                    <td><%=ix%></td>
                                    <td><%=resultFileName%></td>
                                    <td class="text-center"><%=resultFileRecCount%></td>
                                    <td><%=resultFileUniversityName%></td>
                                    <td class="text-center"><%=resultFileStatusText%></td>

                                    <td class="text-center">


                                        <a href="<% out.print(resultFileDetailsUrl);%>" title="Details result file" class="btn btn-primary btn-sm"><i class="icon-info"></i> Details</a>

                                        <a onclick="bulkResultSheetDeleteInfo(<%=argX%>)" title="Delete result file" class="btn btn-danger btn-sm"><i class="icon-trash"></i> Delete</a>

                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                    %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>