<%-- 
    Document   : Sysytem User dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();
    String idS = getId.getID(56);
    int universityIds = Integer.parseInt(idS);

    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        String universityNameOld = request.getParameter("universityNameOld").trim();

        String universityName = request.getParameter("universityName").trim();

        //    System.out.println("userPass:: " + userPass + " encpass :: " + encpass);
        String universityShortName = request.getParameter("universityShortName").trim();
        String universityWebsite = request.getParameter("universityWebsite").trim();
        String universityCountryName = request.getParameter("universityCountryName").trim();

        String universityResultCheck = request.getParameter("universityResultCheck") == null ? "0" : request.getParameter("universityResultCheck").trim();

        System.out.println("universityShortName:: " + universityShortName + " universityName :: " + universityName + " universityCountryName :: " + universityCountryName);

        String userstat = request.getParameter("userstat").trim();
        int intStatus = Integer.parseInt(userstat);

        System.out.println("userdept:: " + userstat);

        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();

        String act = request.getParameter("act");
        //check university name already exits

        Query checkUniSQL = dbsession.createSQLQuery("SELECT * FROM university  WHERE university_long_name='" + universityName + "' AND university_old_name ='ACCREDITED'");
        System.out.println("checkUniSQL " + checkUniSQL);
        if (!checkUniSQL.list().isEmpty()) {
            strMsg = "Error!!! University :: " + universityName + " :: already extis";
            //  response.sendRedirect("syUserAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/accereditedUniversityAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        } else {

            String universityType = "0";

            Query accUniAddSQL = dbsession.createSQLQuery("INSERT INTO university("
                    + "university_id,"
                    + "university_short_name,"
                    + "university_long_name,"
                    + "university_old_name,"
                    + "website,"
                    + "type,"
                    + "check_roll,"
                    + "fax"
                    + ") VALUES("
                    + "'" + universityIds + "',"
                    + "'" + universityShortName + "',"
                    + "'" + universityName + "',"
                    + "'ACCREDITED',"
                    + "'" + universityWebsite + "',"
                    + "'" + universityType + "',"
                    + "'" + universityResultCheck + "',"
                    + "'" + universityCountryName + "'"
                    + "  ) ");

            System.out.println("accUniAddSQL " + accUniAddSQL);

            accUniAddSQL.executeUpdate();

            dbtrx.commit();
            dbsession.flush();
            dbsession.close();

            if (dbtrx.wasCommitted()) {
                // response.sendRedirect("success.jsp?sessionid=" + sessionid);
                strMsg = "New Accredited University Added Successfully";
                //  response.sendRedirect("accereditedUniversityAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

                response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/accereditedUniversityAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            } else {
                dbtrx.rollback();
                // response.sendRedirect("fail.jsp?sessionid=" + sessionid);

                strMsg = "Error!!! When information add";
                // response.sendRedirect("accereditedUniversityAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/accereditedUniversityAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

            }
        }

    } else {
        //if session not found message here      
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }
%>
