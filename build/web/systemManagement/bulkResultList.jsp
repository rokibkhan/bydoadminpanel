<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>


<script type="text/javascript">
    //     $(function(){
    function studentResultDeleteInfo(arg1, arg2) {
        console.log("studentResultDeleteInfo :: " + arg1);
        var rupantorLGModal, btnConfirmInfo;

        rupantorLGModal = $('#rupantorLGModal');
        btnConfirmInfo = "<a id=\"studentResultDeleteInfoConfirmBtn\" onclick=\"studentResultDeleteInfoConfirm(" + arg1 + ",'" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

        rupantorLGModal.find("#rupantorLGModalTitle").text("Confirm result information delete");
//        rupantorLGModal.find("#rupantorLGModalBody").html('');
//        rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);
//
//        rupantorLGModal.modal('show');

        $.post("studentResultDeleteInfoShow.jsp", {deleteItemId: arg1, sessionid: arg2}, function (data) {


            console.log(data);
            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirm result information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html(data[0].resultInfo);
            rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInfo);
            // rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);
            rupantorLGModal.modal('show');

        }, "json");



    }

    function studentResultDeleteInfoConfirm(arg1, arg2) {
        console.log("studentResultDeleteInfoConfirm :: " + arg1);
        var rupantorLGModal;
        rupantorLGModal = $('#rupantorLGModal');


        $.post("studentResultDeleteInfoProcess.jsp", {deleteItemId: arg1, sessionid: arg2}, function (data) {


            console.log(data);

            if (data[0].responseCode == 1) {
                //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                rupantorLGModal.modal('hide');
            } else {
                rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
            }

        }, "json");
    }


</script>
<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
        dbtrx = dbsession.beginTransaction();

        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
            //        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

        // String targetpage = "memberListFellow.jsp";
        String targetpageWithSession = "bulkResultList.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        int limit = 500;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();
        String memberSearchUniversityId = request.getParameter("memberSearchUniversityId") == null ? "" : request.getParameter("memberSearchUniversityId").trim();
        String memberSearchPassingYear = request.getParameter("memberSearchPassingYear") == null ? "" : request.getParameter("memberSearchPassingYear").trim();

        //    out.println("memberSearchPassingYear :: " + memberSearchPassingYear + "<br/>");
        //   out.println("memberSearchUniversityId :: " + memberSearchUniversityId + "<br/>");
    %>


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Result List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">System Management</a></li>                    
                    <li class="active">Result</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->


            


            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-12">


                            <div class="table-responsive">
                                <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="text-center">Roll</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">DOB</th>
                                            <th class="text-center">Passing Year</th>
                                            <th class="text-center">Result</th>
                                            <th class="text-center">University</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%
                                            int ix = 1;
                                            Query resultSQL = null;
                                            Object resultObj[] = null;
                                            String resultId = "";
                                            String studentId = "";
                                            String studentName = "";
                                            String studentDOB = "";
                                            String studentPassingYear = "";
                                            String studentResult = "";
                                            String studentUniversityName = "";
                                            String studentResultDeleteBtn = "";
                                            String argX = "";

                                            resultSQL = dbsession.createSQLQuery("SELECT ur.result_id,ur.student_id,ur.student_name,ur.student_birth_date,"
                                                    + "ur.passing_year ,ur.student_result ,u.university_long_name "
                                                    + "FROM university_result ur,university u "
                                                    + "WHERE ur.university_id = u.university_id "
                                                    + "ORDER BY ur.university_id ASC");
                                            
                                                 //   + "ORDER BY ur.passing_year, ur.result_id, ur.university_id ASC");

                                            System.out.println("resultSQL::" + resultSQL);

                                            if (!resultSQL.list().isEmpty()) {
                                                for (Iterator it1 = resultSQL.list().iterator(); it1.hasNext();) {

                                                    resultObj = (Object[]) it1.next();
                                                    resultId = resultObj[0].toString().trim();
                                                    studentId = resultObj[1].toString().trim();
                                                    studentName = resultObj[2].toString().trim();
                                                    studentDOB = resultObj[3].toString().trim();
                                                    studentPassingYear = resultObj[4].toString().trim();
                                                    studentResult = resultObj[5].toString().trim();
                                                    studentUniversityName = resultObj[6].toString().trim();

                                                    argX = "'" + resultId + "','" + session.getId() + "'";
                                                    studentResultDeleteBtn = "<a onclick=\"studentResultDeleteInfo(" + argX + ")\" title=\"Delete result\" class=\"btn btn-danger btn-sm\"><i class=\"icon-trash\"></i> Delete</a>";


                                        %>


                                        <tr id="infoBox<%=resultId%>">
                                            <td><%=ix%></td>
                                            <td><%=studentId%></td>
                                            <td><%=studentName%></td>
                                            <td><%=studentDOB%></td>     
                                            <td class="text-center"><%=studentPassingYear%></td>   
                                            <td><%=studentResult%></td>     
                                            <td><%=studentUniversityName%></td>     
                                            <td><%=studentResultDeleteBtn%></td>                                      
                                        </tr>

                                        <%
                                                    ix++;
                                                }

                                            }
                                            dbsession.clear();
                                            dbsession.close();
                                            %>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>