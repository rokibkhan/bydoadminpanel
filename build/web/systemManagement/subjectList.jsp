

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showUniversitySubjectInfo(arg1, arg2, arg3) {
            console.log("showUniversitySubjectInfo :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Subject Information");

            $.post("accereditedUniversitySubjectInfoShow.jsp", {universityId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].selectUniSubjectDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }


        function universitySubjectAddOption(arg1, arg2, arg3) {
            console.log("universitySubjectAddOption :: " + arg1);

            $("#universitySubjectAddOptionBtn").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Add New Subject Information");

            $.post("accereditedUniversitySubjectAddInfoShow.jsp", {universityId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].selectUniSubjectDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }




        function universitySubjectAddOptionConfirm(arg1, arg2, arg3) {
            console.log("universitySubjectAddOptionConfirm :: " + arg1);



            var rupantorLGModal, subjectName, subjectShortName, subjectDivisionId;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Add New Subject Information");



            subjectName = document.getElementById("subjectName").value;
            subjectShortName = document.getElementById("subjectShortName").value;
            subjectDivisionId = document.getElementById("subjectDivisionId").value;

            //    invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (subjectName == '') {
                //  $("#btnInvoicePaymentSubmit").button("reset");
                $("#subjectNameErr").html("Subject Name is required").css({"color": "red", "font-size": "10px;"});
                $("#subjectName").focus();
                return false;
            } else {
                document.getElementById("subjectNameErr").innerHTML = "";
            }
            if (subjectShortName == '') {
                // $("#btnInvoicePaymentSubmit").button("reset");
                $("#subjectShortNameErr").html("Subject Short Name is required").css({"color": "red", "font-size": "10px;"});
                $("#subjectShortName").focus();
                return false;
            } else {
                document.getElementById("subjectShortNameErr").innerHTML = "";
            }

            if (subjectDivisionId == '') {
                //   $("#btnInvoicePaymentSubmit").button("reset");
                $("#subjectDivisionIdErr").html("Subject Division is required").css({"color": "red", "font-size": "10px;"});
                $("#subjectDivisionId").focus();
                return false;
            } else {
                document.getElementById("subjectDivisionIdErr").innerHTML = "";
            }

            $("#universitySubjectAddOptionConfirmBtn").button("loading");

            $.post("accereditedUniversitySubjectAddInfoConfirm.jsp", {universityId: arg1, sessionid: arg2, subjectShortName: subjectShortName, subjectName: subjectName, subjectDivisionId: subjectDivisionId}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].selectUniSubjectDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }


    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Subject List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">System Management</a></li>                    
                    <li class="active">Subject</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center">Subject Name</th>
                                    <th class="text-center">University</th>
                                    <th class="text-center">IEB Division</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String subjectId = "";
                                    String subjectName = "";
                                    String subjectDepartment = "";
                                    String subjectUniversity = "";
                                    String subjectDivision = "";
                                    String subject_division_id = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT sb.subject_id,sb.subject_long_name,u.university_long_name,md.full_name "
                                            + "FROM university_subject sb,university u,member_division md "
                                            + "WHERE  sb.university_id = u.university_id AND sb.ieb_division_id = md.mem_division_id");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            subjectId = obj[0].toString().trim();
                                            subjectName = obj[1].toString().trim();
                                            subjectUniversity = obj[2].toString().trim();
                                            subjectDivision = obj[3].toString().trim();

                                            String updateProfileUrl = GlobalVariable.baseUrl + "/systemManagement/subjectListUpdateSubjectInfo.jsp?sessionid=" + session.getId() + "&subjectIdX=" + subjectId + "&selectedTab=profile";


                                %>


                                <tr>
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(subjectName);%></td>
                                    <td><% out.println(subjectUniversity); %></td>
                                    <td><% out.print(subjectDivision); %></td>

                                    <td class="text-center">


                                        <a href="<% out.print(updateProfileUrl); %>" title="Edit Subject" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>

                                        <a id="deleteUniversitySubjectInfoBtn<% out.print(subjectId);%>" onclick="deleteUniversitySubjectInfo('<% out.print(subjectId);%>', '<%out.print(subjectUniversity);%>', '<% out.print(session.getId());%>')" title="Delete Subject Info" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                    %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>