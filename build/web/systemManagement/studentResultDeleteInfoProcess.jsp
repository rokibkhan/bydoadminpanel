<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query resultFileCheckSQL = null;
    Query resultFileSQL = null;
    Query studentResultSQL = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String deleteItemId = request.getParameter("deleteItemId");
    System.out.println("deleteItemId: " + deleteItemId);

    resultFileCheckSQL = dbsession.createSQLQuery("SELECT * FROM university_result WHERE result_id=" + deleteItemId + "");
    if (!resultFileCheckSQL.list().isEmpty()) {
        //delete student result
        studentResultSQL = dbsession.createSQLQuery("DELETE FROM university_result WHERE result_id=" + deleteItemId + "");
        studentResultSQL.executeUpdate();

        dbtrx.commit();
        if (dbtrx.wasCommitted()) {
            responseCode = "1";
            responseMsg = "Success!!! Result Info delete successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

        } else {
            responseCode = "0";
            responseMsg = "Error!!! When Result Info delete from system";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";
        }

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", deleteItemId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! Already delete Result Sheet";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", deleteItemId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>