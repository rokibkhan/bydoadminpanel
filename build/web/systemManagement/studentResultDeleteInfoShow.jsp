<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query resultSQL = null;
    Object resultObj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String resultId = "";
    String studentId = "";
    String studentName = "";
    String studentDOB = "";
    String studentPassingYear = "";
    String studentResult = "";
    String studentUniversityName = "";
    String resultInfo = "";
    String btnInfo = "";

    String deleteItemId = request.getParameter("deleteItemId");
    System.out.println("deleteItemId: " + deleteItemId);

    resultSQL = dbsession.createSQLQuery("SELECT ur.result_id,ur.student_id,ur.student_name,ur.student_birth_date,"
            + "ur.passing_year ,ur.student_result ,u.university_long_name "
            + "FROM university_result ur,university u "
            + "WHERE ur.result_id = " + deleteItemId + " AND ur.university_id = u.university_id "
            + "ORDER BY ur.passing_year, ur.result_id, ur.university_id ASC");

    if (!resultSQL.list().isEmpty()) {

        for (Iterator it1 = resultSQL.list().iterator(); it1.hasNext();) {

            resultObj = (Object[]) it1.next();
            resultId = resultObj[0].toString().trim();
            studentId = resultObj[1].toString().trim();
            studentName = resultObj[2].toString().trim();
            studentDOB = resultObj[3].toString().trim();
            studentPassingYear = resultObj[4].toString().trim();
            studentResult = resultObj[5].toString().trim();
            studentUniversityName = resultObj[6].toString().trim();
        }
//        resultInfo = "<div class=\"row\">"
//                + "<div class=\"col-md-4\">"
//                + "<strong>Student Roll :</strong>" + studentId + " <br/> "
//                + "<strong>Student Name :</strong>" + studentName + " <br/> "
//                + "<strong>Date of Birth :</strong>" + studentDOB + " <br/> "
//                + "<strong>Passing Year :</strong>" + studentPassingYear + " <br/> "
//                + "<strong>Result :</strong>" + studentResult + " <br/> "
//                + "<strong>University :</strong>" + studentUniversityName + ""
//                + "</div>"
//                + "<div class=\"col-md-6\">"
//                + "<strong>Student Roll :</strong>" + studentId + " <br/> "
//                + "<strong>Student Name :</strong>" + studentName + " <br/> "
//                + "<strong>Date of Birth :</strong>" + studentDOB + " <br/> "
//                + "<strong>Passing Year :</strong>" + studentPassingYear + " <br/> "
//                + "<strong>Result :</strong>" + studentResult + " <br/> "
//                + "<strong>University :</strong>" + studentUniversityName + ""
//                + "</div>"
//                + "</div>";

        resultInfo = "<div class=\"row\">"
                + "<div class=\"col-md-2 text-right\"><strong>Student Roll :</strong></div>"
                + "<div class=\"col-md-6 text-left\">" + studentId + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-2 text-right\"><strong>Student Name :</strong></div>"
                + "<div class=\"col-md-6 text-left\">" + studentName + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-2 text-right\"><strong>Date of Birth :</strong></div>"
                + "<div class=\"col-md-6 text-left\">" + studentDOB + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-2 text-right\"><strong>Passing Year :</strong></div>"
                + "<div class=\"col-md-6 text-left\">" + studentPassingYear + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-2 text-right\"><strong>Result :</strong></div>"
                + "<div class=\"col-md-6 text-left\">" + studentResult + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-2 text-right\"><strong>University :</strong></div>"
                + "<div class=\"col-md-6 text-left\">" + studentUniversityName + "</div>"
                + "</div>";

        btnInfo = "<a id=\"studentResultDeleteInfoConfirmBtn\" onclick=\"studentResultDeleteInfoConfirm('" + resultId + "','" + session.getId() + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

        responseCode = "1";
        responseMsg = "Success!!! Result info found";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", deleteItemId);
        json.put("resultInfo", resultInfo);
        json.put("btnInfo", btnInfo);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! Result Info not found";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", deleteItemId);
        json.put("resultInfo", resultInfo);
        json.put("btnInfo", btnInfo);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>