
<%@page import="com.appul.entity.ProductSize"%>
<%@page import="com.appul.entity.GlCode"%>
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>




<script src="../js/jquery.autocomplete.js"></script>
<link rel="stylesheet" type="text/css" href="../css/jquery.autocomplete.css" />
<script src="../jquery/jquery-ui.js"></script>
<link rel="stylesheet" href="../jquery/jquery-ui.css" />


<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">




<script>
    function ProductCodeAutocomplete() {
        //  var form = document.getElementById("stockAdd");

        //  alert("OK");
        var name;

        name = 'productId';
        $('#productId').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "productCodeAutocomplete.jsp",
                    type: "POST",
                    dataType: "json",
                    data: {name: request.term},
                    success: function (data) {

                        console.log(data);
                        response($.map(data, function (item) {
                            return {
                                label: item.name,
                                value: item.value,
                            }
                        }));
                    },
                    error: function (error) {
                        alert('error: ' + error);
                    }
                });
            },
            minLength: 2
        });
//                }
    }


    function selectedProductInfoForUOM(productCode) {
        var sproduct, res;
        sproduct = productCode.split('|');
//        alert(productCode);
        if (sproduct != productCode) {

            $("#quantity1UomId").val(sproduct[3]);
            $("#quantity1-addon").html(sproduct[4]);


            $("#quantity2UomId").val(sproduct[6]);
            $("#quantity2-addon").html(sproduct[7]);
            $("#buying-price-addon").html(sproduct[7]);
            $("#exchangeUom").val(sproduct[5]);

        } else {
            $("#quantity1-addon").html('');
            $("#quantity2-addon").html('');
            $("#buying-price-addon").html('');
//            $("#exchangeUom").val('');
        }
    }

    function getQuantity2(arg1) {

        var form = document.getElementById("stockAdd");
        var quantity1, exchangeUom;

        quantity1 = form.quantity1.value;
        exchangeUom = form.exchangeUom.value;
        if (arg1 != "") {

            form.quantity2.value = parseFloat((quantity1 * exchangeUom)).toFixed(2);


        }


    }

</script>
<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
    String tabProfileActive = "";
    String tabActivityActive = "";
    String tabMessagesActive = "";
    String tabSettingsActive = "";
    String tabChangePassActive = "";
    String selectedTab = request.getParameter("selectedTab") == null ? "" : request.getParameter("selectedTab").trim();
    if (!selectedTab.equals("")) {

        if (selectedTab.equals("profile")) {
            tabProfileActive = "active";
        } else if (selectedTab.equals("activity")) {
            tabActivityActive = "active";
        } else if (selectedTab.equals("messages")) {
            tabMessagesActive = "active";
        } else if (selectedTab.equals("settings")) {
            tabSettingsActive = "active";
        } else if (selectedTab.equals("changePass")) {
            tabChangePassActive = "active";
        }

    } else {
        tabProfileActive = "active";
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Profile</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Profile</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="col-md-4 col-xs-12">
                    <div class="white-box">
                        <div class="user-bg"> <img width="100%" alt="user" src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/large/img1.jpg">
                            <div class="overlay-box">
                                <div class="user-content">
                                    <a href="javascript:void(0)"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/no_image.jpg" class="thumb-lg img-circle" alt="img"></a>
                                    <h4 class="text-white"><% out.print(userNameH);%></h4>
                                    <h5 class="text-white">info@myadmin.com</h5> </div>
                            </div>
                        </div>
                        <div class="user-btm-box">
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-purple"><i class="ti-facebook"></i></p>
                                <h1>258</h1> </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-blue"><i class="ti-twitter"></i></p>
                                <h1>125</h1> </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-danger"><i class="ti-dribbble"></i></p>
                                <h1>556</h1> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="white-box">
                        <ul class="nav customtab nav-tabs" role="tablist">                            
                            <li role="presentation" class="nav-item"><a href="#profile" class="nav-link <% out.print(tabProfileActive); %>" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profile</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#activity" class="nav-link <% out.print(tabActivityActive); %>" aria-controls="activity" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Activity</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#messages" class="nav-link <% out.print(tabMessagesActive); %>" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Message</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#settings" class="nav-link <% out.print(tabSettingsActive); %>" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Setting</span></a></li>
                            <li role="presentation" class="nav-item"><a href="#changePass" class="nav-link <% out.print(tabChangePassActive); %>" aria-controls="changePass" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Change Password</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <% out.print(tabProfileActive); %>" id="profile">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                        <br>
                                        <p class="text-muted">Johnathan Deo</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                        <br>
                                        <p class="text-muted">(123) 456 7890</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted">johnathan@admin.com</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                                        <br>
                                        <p class="text-muted">London</p>
                                    </div>
                                </div>
                                <hr>
                                <p class="m-t-30">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries </p>
                                <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h4 class="font-bold m-t-30">Skill Set</h4>
                                <hr>
                                <h5>Wordpress <span class="pull-right">80%</span></h5>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%;"> <span class="sr-only">50% Complete</span> </div>
                                </div>
                                <h5>HTML 5 <span class="pull-right">90%</span></h5>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-custom" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%;"> <span class="sr-only">50% Complete</span> </div>
                                </div>
                                <h5>jQuery <span class="pull-right">50%</span></h5>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%;"> <span class="sr-only">50% Complete</span> </div>
                                </div>
                                <h5>Photoshop <span class="pull-right">70%</span></h5>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%;"> <span class="sr-only">50% Complete</span> </div>
                                </div>
                            </div>
                            <!-- /#profile -->
                            <div class="tab-pane <% out.print(tabActivityActive); %>" id="activity">
                                <div class="steamline">
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/genu.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p>assign a new task <a href="#"> Design weblayout</a></p>
                                                <div class="m-t-20 row"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img1.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="../plugins/images/img2.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="../plugins/images/img3.jpg" alt="user" class="col-md-3 col-xs-12" /></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/sonu.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"> <a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <div class="m-t-20 row">
                                                    <div class="col-md-2 col-xs-12"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img1.jpg" alt="user" class="img-responsive" /></div>
                                                    <div class="col-md-9 col-xs-12">
                                                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa</p> <a href="#" class="btn btn-success"> Design weblayout</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/ritesh.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/govinda.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p>assign a new task <a href="#"> Design weblayout</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /#profile -->
                            <div class="tab-pane <% out.print(tabMessagesActive); %>" id="messages">
                                <div class="steamline">
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/genu.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"> <a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <div class="m-t-20 row">
                                                    <div class="col-md-2 col-xs-12"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img1.jpg" alt="user" class="img-responsive" /></div>
                                                    <div class="col-md-9 col-xs-12">
                                                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa</p> <a href="#" class="btn btn-success"> Design weblayout</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/sonu.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p>assign a new task <a href="#"> Design weblayout</a></p>
                                                <div class="m-t-20 row"><img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img1.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img2.jpg" alt="user" class="col-md-3 col-xs-12" /> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/img3.jpg" alt="user" class="col-md-3 col-xs-12" /></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/ritesh.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/govinda.jpg" alt="user" class="img-circle" /> </div>
                                        <div class="sl-right">
                                            <div class="m-l-40"><a href="#" class="text-info">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                <p>assign a new task <a href="#"> Design weblayout</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /#messages-->
                            <div class="tab-pane <% out.print(tabSettingsActive); %>" id="settings">
                                <form class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12">Full Name</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Password</label>
                                        <div class="col-md-12">
                                            <input type="password" value="password" class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Phone No</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="123 456 7890" class="form-control form-control-line"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Message</label>
                                        <div class="col-md-12">
                                            <textarea rows="5" class="form-control form-control-line"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Select Country</label>
                                        <div class="col-sm-12">
                                            <select class="form-control form-control-line">
                                                <option>London</option>
                                                <option>India</option>
                                                <option>Usa</option>
                                                <option>Canada</option>
                                                <option>Thailand</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">Update Profile</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /#settings -->           
                            <div class="tab-pane <% out.print(tabChangePassActive); %>" id="changePass">
                                <script>
                                    $(function () {

                                        //hang on event of form with id=myform
                                        $("#changePassFrm").submit(function (e) {

                                            //prevent Default functionality
                                            e.preventDefault();

                                            var oldPassword, newPassword, newPassword1, passwordOptToken;

                                            oldPassword = $("#oldPassword").val();
                                            newPassword = $("#newPassword").val();
                                            newPassword1 = $("#newPassword1").val();

                                            if (oldPassword == '') {
                                                $("#oldPassword").focus();
                                                $("#oldPassword").css({"background-color": "#fff", "border": "1px solid red"});
                                                // $("#oldPasswordErr").html("New password is required.");
                                                return false;
                                            }
                                            if (newPassword == '') {
                                                $("#newPassword").focus();
                                                $("#newPassword").css({"background-color": "#fff", "border": "1px solid red"});
                                                // $("#newPasswordErr").html("New password is required.");
                                                return false;
                                            }

                                            if (newPassword1 == '') {
                                                $("#newPassword1").focus();
                                                $("#newPassword1").css({"background-color": "#fff", "border": "1px solid red"});
                                                // $("#newPassword1Err").html("Retype password is required.");
                                                return false;
                                            } else {
                                                $("#newPassword1rr").html("Passwords do not match.");
                                            }

                                            if (newPassword != newPassword1) {
                                                //  alert("Passwords do not match.");                                              
                                                $("#newPassword1").focus();
                                                $("#newPassword1Err").html("Passwords do not match.");
                                                return false;
                                            } else {
                                                $("#newPassword1Err").html("");
                                            }

                                            passwordOptToken = '<%out.print(sessionid);%>';

                                            $("#btnChangePassFrm").button('loading');

                                            $.post("syUserMyPasswordChangeUpdateProcess.jsp", {
                                                oldPass: oldPassword, newPass: newPassword, passwordOptToken: passwordOptToken
                                            }, function (data) {


//                                                console.log(data);
//                                                console.log("responseCode :: " + data[0].responseCode);
//                                                console.log("responseMsg :: " + data[0].responseMsg);
                                                if (data[0].responseCode == 1) {
                                                    $("#oldPassword").val('');
                                                    $("#newPassword").val('');
                                                    $("#newPassword1").val('');
                                                    $("#btnChangePassFrm").button('reset');
                                                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");

                                                } else {
                                                    $("#btnChangePassFrm").button('reset');
                                                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");

                                                }

//                                                if (data.dataError == "NO") {
//
//                                                    //	reqShareMailModal.modal('hide');
//                                                    //	$("#globalAlertInfoBoxCon").show();
//                                                    $("#globalAlertInfoBoxCon").html(data.msgText1).show().delay(3000).fadeOut("slow");
//                                                    $("#btnUnsubscribe").html("Unsubscribtion Complete");
//                                                    $("#btnUnsubscribe").addclass('disabled');
//                                                }
//                                                if (data.dataError == "YES") {
//                                                    //reqShareMailModal.modal('hide');
//                                                    $("#globalAlertInfoBoxCon").html(data.msgText1).show().delay(5000).fadeOut("slow");
//                                                    $("#btnUnsubscribe").button('reset');
//                                                }

                                            }, "json");

                                            //	});

                                        });


                                    });

                                </script>

                                <form class="form-horizontal form-material1" id="changePassFrm">
                                    <div class="form-group row custom-bottom-margin-5x">
                                        <label for="inputName4" class="control-label col-sm-4">Old Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control input-sm" id="oldPassword" name="oldPassword" placeholder="Old password" required onkeypress="ProductCodeAutocomplete()" onfocusout="selectedProductInfoForUOM(this.value)">
                                            <div id="oldPasswordErr" class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="form-group row custom-bottom-margin-5x">
                                        <label for="inputName4" class="control-label col-sm-4">New Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control input-sm" id="newPassword" name="newPassword" placeholder="New password" required>
                                            <div id="newPasswordErr" class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row custom-bottom-margin-5x">
                                        <label for="inputName4" class="control-label col-sm-4">Retype New Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control input-sm" id="newPassword1" name="newPassword1" placeholder="Retype new password" required>
                                            <div id="newPassword1Err" class="help-block with-errors"></div>
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <label class="control-label col-sm-4"></label>
                                        <div class="col-sm-offset-3 col-sm-8">
                                            <button id="btnChangePassFrm" type="submit" class="btn btn-info">Update Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /#changePass -->

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

<%
dbsession.clear();
dbsession.close();
%>


    <%@ include file="../footer.jsp" %>