<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Logger logger = Logger.getLogger("ticketAddSubmitData_jsp.class");

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getRegistryID getId = new getRegistryID();
    String idS = getId.getID(55);
    int subjectIds = Integer.parseInt(idS);

    Object uniObj[] = null;
    Object obj[] = null;

    String sessionIdH = "";
    String userNameH = "";
    String subjectMessage = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    //  if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
    }

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String selectUniversityId = request.getParameter("universityId");
    String selectSubjectShortName = request.getParameter("subjectShortName");
    String selectSubjectName = request.getParameter("subjectName");
    String selectSubjectDivisionId = request.getParameter("subjectDivisionId");
    String sessionid = request.getParameter("sessionid");
    String department = "";
    String status = "1";

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String adddate = dateFormatX.format(dateX);
    String adduser = userNameH;
    String addterm = InetAddress.getLocalHost().getHostName().toString();
    String addip = InetAddress.getLocalHost().getHostAddress().toString();

    //insert subject info
    Query q4 = dbsession.createSQLQuery("INSERT INTO university_subject("
            + "subject_id,"
            + "subject_short_name,"
            + "subject_long_name,"
            + "department,"
            + "university_id,"
            + "ieb_division_id,"
            + "status,"
            + "add_date,"
            + "add_user,"
            + "add_term,"
            + "add_ip) values( "
            + "'" + subjectIds + "',"
            + "'" + selectSubjectShortName + "',"
            + "'" + selectSubjectName + "',"
            + "'" + department + "',"
            + "'" + selectUniversityId + "',"
            + "'" + selectSubjectDivisionId + "',"
            + "'" + status + "',"
            + "'" + adddate + "',"
            + "'" + adduser + "',"
            + "'" + addterm + "',"
            + "'" + addip + "')");

    q4.executeUpdate();
    dbtrx.commit();
    if (dbtrx.wasCommitted()) {
        subjectMessage = "Success!!! Subject added successfully";
    } else {
        subjectMessage = "Error!!!Problem when subject add";
    }

    String selectUniversityName = "";

    Query uniSQL = dbsession.createSQLQuery("SELECT * FROM university WHERE university_id = '" + selectUniversityId + "'");

    if (!uniSQL.list().isEmpty()) {
        for (Iterator uniIt1 = uniSQL.list().iterator(); uniIt1.hasNext();) {

            uniObj = (Object[]) uniIt1.next();
            selectUniversityName = uniObj[2].toString().trim();
        }
    }

    String subjectId = "";
    String subjectName = "";
    String subjectDepartment = "";
    String subjectUniversity = "";
    String subjectDivision = "";
    String subject_division_id = "";

    String selectRoleDetailsTopSection = "";
    String selectRoleDetailsMidSection = "";
    String selectRoleDetailsBottomSection = "";
    String selectSubjectFormSection = "";

    String btnColorClass = "";
    String levelStatusColorClass = "";
    String btnInvInfo = "";
    String selectSubjectDetails = "";

    String allActivityConStr = "";

    String checkActRoleActivity = "";

    String multiSelectCSS = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/multiselect/css/multi-select.css";
    String multiSelectJs = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/multiselect/js/jquery.multi-select.js";

    selectRoleDetailsTopSection = "<div class=\"white-box1 printableArea1\">"
            + "<h4 class=\"text-center\">" + selectUniversityName + "</h4></div>";

    selectRoleDetailsMidSection = "<div class=\"col-md-12\">"
            + "<div class=\"table-responsive\" style=\"\">"
            + "<table class=\"table table-bordered table-hover color-table1 primary-table1 table-custom-padding-5x\">"
            + "<thead>"
            + "<tr>"
            + "<th class=\"text-center\" width=\"5%\">#</th>"
            + "<th class=\"text-center\">Subject</th>"
            + "<th class=\"text-center\">IEB Division</th>"
            + "</tr>"
            + "</thead>"
            + "<tbody>";

    Query usrSQL = dbsession.createSQLQuery("SELECT sb.subject_id,sb.subject_long_name,u.university_long_name,md.full_name "
            + "FROM university_subject sb,university u,member_division md "
            + "WHERE  sb.university_id = u.university_id AND sb.ieb_division_id = md.mem_division_id "
            + "AND sb.university_id ='" + selectUniversityId + "'");
    int ix = 1;
    if (!usrSQL.list().isEmpty()) {
        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            subjectId = obj[0].toString().trim();
            subjectName = obj[1].toString().trim();
            subjectUniversity = obj[2].toString().trim();
            subjectDivision = obj[3].toString().trim();

            selectRoleDetailsMidSection += "<tr>"
                    + "<td class=\"text-center\">" + ix + "</td>"
                    + "<td>" + subjectName + "</td>"
                    + "<td>" + subjectDivision + "</td>"
                    + "</tr>";
            ix++;
        }
    }

    selectRoleDetailsBottomSection = "</tbody>"
            + "</table>"
            + "</div>"
            + "</div>";

    Query divisionSQL = null;
    Object divisionObj[] = null;
    String divisionId = "";
    String divisionName = "";
    String divisionInfoContainer = "";

    divisionSQL = dbsession.createSQLQuery("SELECT * FROM member_division where mem_division_id !='30' ORDER BY full_name ASC");
    if (!divisionSQL.list().isEmpty()) {
        for (Iterator divisionItr = divisionSQL.list().iterator(); divisionItr.hasNext();) {
            divisionObj = (Object[]) divisionItr.next();
            divisionId = divisionObj[0].toString();
            divisionName = divisionObj[3] == null ? "" : divisionObj[3].toString();

            divisionInfoContainer = divisionInfoContainer + "<option value=\"" + divisionId + "\">" + divisionName + "</option>";

        }
    }

    selectSubjectFormSection = "<div class=\"col-md-12\">"
            + "<div class=\"white-box1 printableArea1\">"
            + "<h5 class=\"text-center\">" + subjectMessage + "</h5>"
            + "<form data-toggle=\"validator\" class=\"form-horizontal\">"
            + "<div class=\"form-group row custom-bottom-margin-5x\">"
            + "<label for=\"inputName4\" class=\"control-label col-sm-3\">Subject Name</label>"
            + "<div class=\"col-sm-8\">"
            + "<input type=\"text\" id=\"subjectName\" name=\"subjectName\"  placeholder=\"Subject Name\" class=\"form-control input-sm\" required><div  id=\"subjectNameErr\" class=\"help-block with-errors\"></div>"
            + "</div>"
            + "</div>"
            + "<div class=\"form-group row custom-bottom-margin-5x\">"
            + "<label for=\"inputName4\" class=\"control-label col-sm-3\">Subject Short Name</label>"
            + "<div class=\"col-sm-4\">"
            + "<input type=\"text\" id=\"subjectShortName\" name=\"subjectShortName\"  placeholder=\"Subject Short Name\" class=\"form-control input-sm\" required><div  id=\"subjectShortNameErr\" class=\"help-block with-errors\"></div>"
            + "</div>"
            + "</div>"
            + "<div class=\"form-group row custom-bottom-margin-5x\">"
            + "<label for=\"inputName4\" class=\"control-label col-sm-3\">Subject Division</label>"
            + "<div class=\"col-sm-4\">"
            + "<select  name=\"subjectDivisionId\" id=\"subjectDivisionId\" class=\"form-control input-sm customInput-sm\" required>"
            + "<option value=\"\">Select Division </option>"
            + "" + divisionInfoContainer + ""
            + "</select>"
            + "<div  id=\"subjectDivisionIdErr\" class=\"help-block with-errors\"></div>"
            + "</div>"
            + "</div>"
            + "</form>"
            + "</div>"
            + "</div>";

    selectSubjectDetails = selectRoleDetailsTopSection + selectRoleDetailsMidSection + allActivityConStr + selectRoleDetailsBottomSection + selectSubjectFormSection;

    //     + "<a id=\"btnShowUpdateActivityOption\" onclick=\"showUpdateActivityOption('" + selectRoleId + "','" + selectRoleName + "','" + sessionid + "')\" class=\"btn btn-primary\"> Update Role Activity </a> &nbsp;"
    btnInvInfo = "<div class=\"clearfix\"></div>"
            + "<div class=\"text-right\">"
            + "<a id=\"universitySubjectAddOptionConfirmBtn\" onclick=\"universitySubjectAddOptionConfirm('" + selectUniversityId + "','" + selectUniversityName + "','" + sessionid + "')\"  class=\"btn btn-info waves-effect waves-light\">Add Confirm</a> &nbsp;"
            + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>"
            + "</form></div>"
            + "<br/>";

    json = new JSONObject();
    json.put("selectUniversityId", selectUniversityId);
    json.put("selectUniSubjectDetails", selectSubjectDetails);
    json.put("subjectMessage", subjectMessage);
    json.put("btnInvInfo", btnInvInfo);
    jsonArr.add(json);

    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>