<%-- 
    Document   : logout.jsp
    Created on : January 18, 2018, 11:16:42 AM
    Author     : Akter & Tahajjat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="java.util.*" %>


<%
    Session dbsession = null;
    dbsession = HibernateUtil.getSessionFactory().openSession();

    request.getSession(false);
    if (session.getId() != null) {
        session.setAttribute("username", null);
        session.setAttribute("password", null);
        session.setAttribute("logstat", null);

        session.invalidate();
    }
  
//  String uri = response.encodeRedirectURL("login.jsp");
//        HibernateUtil.closeFactory();

    String sessionIdH = session.getId();
    System.out.println("LogOut sessionIdH :: " + sessionIdH);
//    String userIdH = session.getAttribute("username").toString().toUpperCase();
//    String userStoreIdH = session.getAttribute("storeId").toString();
//

//    System.out.println("LogOut userStoreIdH :: " + userStoreIdH);
//
//    System.out.println("LogOut userIdH :: " + userIdH);

    //    response.sendRedirect("login.jsp");

%>

<%

    response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
%>

<%
dbsession.clear();
dbsession.close();
%>
