



<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.entity.InvoiceMain"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.entity.ProductInventory"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="com.appul.util.GlobalVariable"%>

<!-- Page Content -->
<div id="page-wrapper">

    <%

        String userStoreIdHForHome = session.getAttribute("storeId").toString();
        String pattern = "MMMMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String currentMonthName = simpleDateFormat.format(new Date());

        String patternY = "yyyy";
        SimpleDateFormat simpleDateFormatY = new SimpleDateFormat(patternY);
        String currentYearName = simpleDateFormatY.format(new Date());

        Session dbsessionR = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrxR = dbsessionR.beginTransaction();
        Query sumQueryHome = null;
        Object objHome[] = null;

        String totalSum = "0";
        String dailyTotalSum = "0";
        String dailyTotalSum1 = "";
        String weeklyTotalSum = "0";
        String weeklyTotalSum1 = "";
        String monthlyTotalSum = "0";
        String monthlyTotalSum1 = "";
        String yearlyTotalSum = "0";
        String yearlyTotalSum1 = "";

        DecimalFormat dfR = new DecimalFormat("#,###,###,##0.00");
/*
        //       String sumHql = "Select sum(inv.totalAmount) FROM InvoiceMain AS inv WHERE inv.invType ='I' AND inv.txnDt like concat('%',substring(now(),1,7),'%')";
        String sumHql = "SELECT sum(inv.TOTAL_AMOUNT) AS totalSum,"
                + "(SELECT sum(inv.TOTAL_AMOUNT) FROM invoice_main AS inv WHERE inv.store_id = " + userStoreIdHForHome + " AND inv.TXN_DT like concat('%',substring(now(),1,10),'%')) AS dailyTotal,"
                + "(SELECT sum(inv.TOTAL_AMOUNT) FROM invoice_main AS inv WHERE inv.store_id = " + userStoreIdHForHome + "  AND substr(inv.TXN_DT,1,10) >= substring(DATE_ADD(NOW(), INTERVAL(1-DAYOFWEEK(NOW())) DAY),1,10) AND substr(inv.TXN_DT,1,10) <= substring(DATE_ADD(NOW(), INTERVAL(7-DAYOFWEEK(NOW())) DAY),1,10)) AS weeklyTotal,"
                + "(SELECT sum(inv.TOTAL_AMOUNT) FROM invoice_main AS inv WHERE inv.store_id = " + userStoreIdHForHome + " AND inv.TXN_DT like concat('%',substring(now(),1,7),'%')) AS montlyTotal,"
                + "(SELECT sum(inv.TOTAL_AMOUNT) FROM invoice_main AS inv WHERE inv.store_id = " + userStoreIdHForHome + " AND inv.TXN_DT like concat('%',substring(now(),1,4),'%')) AS yearlyTotal"
                + " FROM invoice_main AS inv WHERE inv.INV_TYPE ='I' AND inv.store_id = " + userStoreIdHForHome;

        System.out.println("Sum SQL :" + sumHql);
        sumQueryHome = dbsessionR.createSQLQuery(sumHql);

        if (!sumQueryHome.list().isEmpty()) {

            System.out.println("Result" + sumQueryHome.list());
            for (Iterator itHome = sumQueryHome.list().iterator(); itHome.hasNext();) {
                objHome = (Object[]) itHome.next();

                totalSum = objHome[0] == null ? "0" : objHome[0].toString().trim();
                dailyTotalSum = objHome[1] == null ? "0" : objHome[1].toString().trim();
                weeklyTotalSum = objHome[2] == null ? "0" : objHome[2].toString().trim();
                monthlyTotalSum = objHome[3] == null ? "0" : objHome[3].toString().trim();
                yearlyTotalSum = objHome[4] == null ? "0" : objHome[4].toString().trim();

                System.out.println("totalSum " + totalSum);
                System.out.println("dailyTotalSum " + dailyTotalSum);
                System.out.println("weeklyTotalSum " + weeklyTotalSum);
                System.out.println("monthlyTotalSum " + monthlyTotalSum);
                System.out.println("yearlyTotalSum " + yearlyTotalSum);

            }
        } else {
            System.out.println("Result null");
        }

        dailyTotalSum1 = dfR.format(Double.parseDouble(dailyTotalSum));
        weeklyTotalSum1 = dfR.format(Double.parseDouble(weeklyTotalSum));
        monthlyTotalSum1 = dfR.format(Double.parseDouble(monthlyTotalSum));
        yearlyTotalSum1 = dfR.format(Double.parseDouble(yearlyTotalSum));
*/
        dbsessionR.clear();
        dbsessionR.close();


    %>
    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("sales/showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("sales/invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showInvoiceDetails(arg1, arg2) {
            console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("sales/showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');


            }, "json");

            //  rupantorLGModal.modal('show');
        }




    </script>

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Demographical</h4> 
            </div>
            <!-- /.page title -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Demographical</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title"><i class="ti-shopping-cart text-success"></i> Order Received</h3>
                    <div class="text-right"> <span class="text-muted">Todays Order</span>
                        <h2><!-- sup><i class="ti-arrow-up text-success"></i></sup -->  TK <% out.println(dailyTotalSum1);%></h2>
                    </div>
                    <span class="text-success">20%</span>
                    <div class="progress m-b-0">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:20%;"> <span class="sr-only">20% Complete</span> </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title"><i class="ti-bolt text-danger"></i>  Weekly Sales</h3>
                    <div class="text-right"> <span class="text-muted">Weekly Income</span>
                        <h2><!-- sup><i class="ti-arrow-down text-danger"></i></sup --> TK <% out.println(weeklyTotalSum1);%></h2>
                    </div>
                    <span class="text-danger">30%</span>
                    <div class="progress m-b-0">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:30%;"> <span class="sr-only">230% Complete</span> </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title"><i class="ti-wallet text-info"></i> Monthly Sales</h3>
                    <div class="text-right"> <span class="text-muted"><% out.println(currentMonthName);%></span>
                        <h2><!-- sup><i class="ti-arrow-up text-info"></i></sup --> TK <% out.println(monthlyTotalSum1);%></h2>
                    </div>
                    <span class="text-info">60%</span>
                    <div class="progress m-b-0">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:60%;"> <span class="sr-only">20% Complete</span> </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title"><i class="ti-stats-up"></i> Yearly Sales</h3>
                    <div class="text-right"> <span class="text-muted">Year <%  out.println(currentYearName); %></span>
                        <h2><!-- sup><i class="ti-arrow-up text-inverse"></i></sup --> TK <% out.println(yearlyTotalSum1);%></h2>
                    </div>
                    <span class="text-inverse">80%</span>
                    <div class="progress m-b-0">
                        <div class="progress-bar progress-bar-inverse" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:80%;"> <span class="sr-only">230% Complete</span> </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.row -->
        <div class="row">                    
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Recent sales</h3>

                    <div class="table-responsive">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>INVOICE</th>
                                    <th>STATUS</th>
                                    <th>DATE</th>
                                    <th>PRICE(TK)</th>
                                    <th>DELIVERY DATE</th>
                                    <th>REMARKS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>



                            <%

                                //   Session dbsession = HibernateUtil.getSessionFactory().openSession();
                                Session dbsessionX = HibernateUtil.getSessionFactory().openSession();
                                //     org.hibernate.Transaction dbtrx = null;
                                //     org.hibernate.Transaction dbtrx = dbsession.beginTransaction();

                                org.hibernate.Transaction dbtrxX = dbsessionX.beginTransaction();

                                int ix = 1;
                                String invNo = "";
                                String invNoX = "";
                                String invDate = "";
                                String invDate1 = "";
                                String invDeliveryDate = "";
                                String invDeliveryDate1 = "";
                                String invRemarks = "";
                                int consumerId = 0;
                                String consumerEmail = "";
                                String invTotalAmount = "";
                                String invTotalAmount1 = "";
                                String invNetAmount = "";
                                String invNetAmount1 = "";
                                String status = "";
                                String deliveryChallanPrintLink = "";
                                String timeZone = "America/Chicago";

                                TimeZoneConversion tz = new TimeZoneConversion();

                                DecimalFormat df = new DecimalFormat("#,###,###,##0.00");

                                StringToDate sTd = new StringToDate();

                                Query q1 = dbsessionX.createQuery("from InvoiceMain WHERE invType ='I' AND store = " + userStoreIdHForHome + " order by invNo desc");
                                q1.setMaxResults(5);

                                if (!q1.list().isEmpty()) {
                                    for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {

                                        InvoiceMain invoiceMain = (InvoiceMain) itr1.next();
                                        invNo = invoiceMain.getInvNo();
                                        invNoX = invNo.trim();
                                        //    invDate = (invoiceMain.getAddDate().toString()).substring(0, 19);
                                        invDate = (invoiceMain.getTxnDt().toString()).substring(0, 19);

                                        invDate1 = sTd.getDate(invDate);

                                        invDeliveryDate = (invoiceMain.getShipmentDate().toString()).substring(0, 19);
                                        invDeliveryDate1 = sTd.getDate(invDeliveryDate);

                                        invRemarks = invoiceMain.getAddTerm() == null ? " " : invoiceMain.getAddTerm().toString();

                                        consumerId = invoiceMain.getConsumer().getId();

                                        invTotalAmount = invoiceMain.getTotalAmount().toString();
                                        invTotalAmount1 = df.format(Double.parseDouble(invTotalAmount));
                                        invNetAmount = invoiceMain.getNetAmount().toString();
                                        invNetAmount1 = df.format(Double.parseDouble(invNetAmount));

                                        status = invoiceMain.getStatus();
                                        //     invDate1 = tz.getTimeZoneConvert(timeZone, invDate);

                                        if (status.equalsIgnoreCase("C")) {
                                            status = "<span class=\"label label-megna label-rounded bg-primary\">Sale</span>";
                                            deliveryChallanPrintLink = "<a title=\"Confirm Delivery Order\" href=\"" + GlobalVariable.baseUrl + "/sales/deliveryOrderPrint.jsp?sessionid=" + sessionid + "&invNo=" + invNo + "\" target=\"_blank\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-truck\"></i></a>";
                                        } else if (status.equalsIgnoreCase("D")) {
                                            status = "<span class=\"label label-megna label-rounded bg-success\">Delivered</span>";
                                            deliveryChallanPrintLink = "<a title=\"Print Delivery Order\" href=\"" + GlobalVariable.baseUrl + "/sales/deliveryOrderPrint.jsp?sessionid=" + sessionid + "&invNo=" + invNo + "\" target=\"_blank\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-truck\"></i></a>";
                                        }

                                        String btnColorClass = "btn-primary";
                                        if (Double.parseDouble(invNetAmount) > 0) {
                                            btnColorClass = "btn-danger";
                                        }

                            %>




                            <tr>
                                <td class="txt-oflo"><% out.print(ix);%></td>
                                <td class="txt-oflo"><% out.print(invNo);%></td>
                                <td><% out.print(status);%></td>
                                <td class="txt-oflo"><% out.print(invDate1); %></td>
                                <td><span class="text-success"><% out.print(invTotalAmount1); %> </span></td>
                                <td><% out.print(invDeliveryDate1); %></td>
                                <td style="width: 35%"><% out.print(invRemarks); %></td>
                                <td class="txt-oflo">


                                    <a id="btnShowInvoiceDetails<% out.print(invNoX);%>" onclick="showInvoiceDetails(<% out.print(invNoX + ",'" + sessionid + "'");%>);" title="Invoice details" class="btn <% out.print(btnColorClass); %> btn-sm"><i class="icon-info"></i></a>
                                    <a title="Print invoice" href="<%out.print(GlobalVariable.baseUrl);%>/sales/invoicePrint.jsp?sessionid=<%=sessionid%>&invNo=<%=invNo%>" target="_blank" class="btn btn-primary btn-sm"><i class="icon-printer"></i></a>
                                        <% out.print(deliveryChallanPrintLink);%>


                                </td>
                            </tr>

                            <%
                                        ix++;
                                    }
                                }
                                dbsessionX.clear();
                                dbsessionX.close();
                            %>


                            </tbody>
                        </table> <a href="<%out.print(GlobalVariable.baseUrl);%>/sales/invoiceList.jsp?sessionid=<% out.println(sessionid); %>">Check all the sales</a> </div>
                </div>
            </div>
        </div>
        <!-- /.row -->


        <%
            Session dbsessionGraph = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrxGraph = dbsessionGraph.beginTransaction();
            Query queryGraph = null;
            Object objGraph[] = null;

            String dailyDateForGraph = "";
            String dailyDateForGraph1 = "";
            String dailyDateForGraph1X = "";
            String dailyTotalSumForGraph = "0";
            Double dailyTotalSumForGraphK;
            String dailyTotalSumForGraph1 = "0";
            String graphDataX = "";

            String sqlForGraph = "SELECT substr(txn_dt,1,10),sum(total_amount) FROM  invoice_main AS inv"
                    + " WHERE inv.INV_TYPE ='I' AND inv.store_id = " + userStoreIdHForHome + " AND substr(txn_dt,1,10)>= substr((CURDATE() - INTERVAL 1 MONTH ),1,10)  AND substr(txn_dt,1,10)<=substring(now(),1,10) "
                    + " group by substr(txn_dt,1,10)"
                    + " order by substr(txn_dt,1,10)";

            System.out.println("Graph SQL :" + sqlForGraph);
            queryGraph = dbsessionGraph.createSQLQuery(sqlForGraph);

            if (!queryGraph.list().isEmpty()) {

                System.out.println("Result Graph" + queryGraph.list());
                for (Iterator itGraph = queryGraph.list().iterator(); itGraph.hasNext();) {
                    objGraph = (Object[]) itGraph.next();

                    dailyDateForGraph = objGraph[0] == null ? "0" : objGraph[0].toString().trim();
                    dailyTotalSumForGraph = objGraph[1] == null ? "0" : objGraph[1].toString().trim();
                    dailyTotalSumForGraph1 = df.format(Double.parseDouble(dailyTotalSumForGraph));

                    dailyDateForGraph1 = sTd.getDate(dailyDateForGraph);

                    dailyDateForGraph1X = dailyDateForGraph1.substring(0, 6);//31-Mar  

                    dailyTotalSumForGraphK = Double.parseDouble(dailyTotalSumForGraph) / 1000;

//                  dailyDateForGraph = objGraph[0].toString().trim();
//                  dailyTotalSumForGraph = objGraph[1].toString().trim();
//                  
                    System.out.println("dailyDateForGraph " + dailyDateForGraph);
                    System.out.println(dailyDateForGraph.substring(8));//he  
                    System.out.println("dailyDateForGraph1 " + dailyDateForGraph1);
                    System.out.println("dailyDateForGraph1X " + dailyDateForGraph1X);
                    System.out.println("dailyTotalSumForGraph " + dailyTotalSumForGraph);
                    System.out.println("dailyTotalSumForGraph1 " + dailyTotalSumForGraph1);
                    System.out.println("dailyTotalSumForGraphK " + Math.round(dailyTotalSumForGraphK));

                    graphDataX = graphDataX + "{year: '" + dailyDateForGraph1X + "', value: " + dailyTotalSumForGraphK + "},";

                }
            }

            System.out.println("graphDataX " + graphDataX);

            //       String sumHql = "Select sum(inv.totalAmount) FROM InvoiceMain AS inv WHERE inv.invType ='I' AND inv.txnDt like concat('%',substring(now(),1,7),'%')";
            dbsessionGraph.clear();
            dbsessionGraph.close();
        %>

        <!--row -->
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Last 30 Days Sales</h3>

                    <div id="myfirstchartMorris" style="height: 340px;"></div>
                </div>
            </div>

        </div>
        <!-- /.row -->

        <script type="text/javascript">
            $(function () {

                console.log("TestX");

                Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'myfirstchartMorris',
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    data: [
            <%out.print(graphDataX);%>
                    ],
                    // The name of the data record attribute that contains x-values.
                    xkey: 'year',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Value'],
                    pointSize: 3,
                    fillOpacity: 0,
                    pointStrokeColors: ['#00bfc7'],
                    behaveLikeLine: true,
                    gridLineColor: '#e0e0e0',
                    lineWidth: 3,
                    hideHover: 'auto',
                    lineColors: ['#00bfc7'],
                    parseTime: false,
                    hoverCallback: function (index, options, content, row) {
                        console.log("content " + content);
                        // console.log("row:: "+row);
                        var dateX = row.year;
                        var saleX = row.value;
                        console.log("dateX:: " + dateX);
                        console.log("saleX:: " + saleX);
                        //  return content+"K";
                        //  return saleX+"K";
                        return dateX + "<br>" + saleX + "K";
                    },
                    resize: true

                });

            });
        </script>


    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->