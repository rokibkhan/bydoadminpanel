

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">




        function journalDeleteInfo(arg1, arg2) {

            console.log("JournalDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Journal Delete confirmation");
            btnInfo = "<a onclick=\"journalDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            rupantorLGModal.find("#rupantorLGModalBody").html('');

            //   rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());

            rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo);


//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }


        function journalDeleteInfoConfirm(arg1, arg2) {
            console.log("JournalDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');



            $.post("journalDeleteProcess.jsp", {journalId: arg1, sessionid: arg2}, function (data) {



                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=\"6\" style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Journal List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Journal Management</a></li>                    
                    <li class="active">Journal</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center">Cover Page</th>
                                    <th class="text-center">Division</th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Author</th>
                                    <th class="text-center">Chapter Info</th>
                                    <th class="text-center">Link</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%

                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();
                                    DateFormat dateFormatJournal = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat dateFormatJournalM = new SimpleDateFormat("MMM");
                                    DateFormat dateFormatJournalD = new SimpleDateFormat("dd");
                                    DateFormat dateFormatJournalY = new SimpleDateFormat("Y");

                                    Date dateJournalM = null;
                                    Date dateJournalD = null;
                                    Date dateJournalY = null;
                                    String newDateJournalM = "";
                                    String newDateJournalD = "";
                                    String newDateJournalY = "";

                                    Query journalSQL = null;
                                    Object journalObj[] = null;
                                    String journalId = "";
                                    String journalTitle = "";
                                    String journalAuthor = "";
                                    String journalDetials = "";
                                    String journalDivisionName = "";
                                    String journalCoverPage = "";
                                    String journalCoverPageUrl = "";
                                    String journalCoverPage1 = "";

                                    String journalDateTime = "";
                                    String journalMonth = "";
                                    String journalDay = "";
                                    String journalYear = "";

                                    String journalDownloadUrl = "";
                                    String journalDetailsUrl = "";
                                    String journalInfoContainer = "";
                                    int ix = 1;

                                    journalSQL = dbsession.createSQLQuery("SELECT jn.id_journal,jn.journal_title,jn.journal_author,jn.journal_desc,"
                                            + "jn.journal_link,jn.journal_date,md.mem_division_name,jn.cover_page_picture "
                                            + "FROM journal_info jn,member_division md "
                                            + "WHERE jn.parent_id = 0 AND jn.journal_division = md.mem_division_id "
                                            + "ORDER BY jn.id_journal DESC");

                                    System.out.println("SQL:: " + journalSQL);

                                    if (!journalSQL.list().isEmpty()) {
                                        for (Iterator itJournal = journalSQL.list().iterator(); itJournal.hasNext();) {

                                            journalObj = (Object[]) itJournal.next();
                                            journalId = journalObj[0].toString().trim();
                                            journalTitle = journalObj[1].toString().trim();

                                            journalAuthor = journalObj[2].toString().trim();

                                            journalDetials = journalObj[3].toString().trim();

                                            journalDivisionName = journalObj[6].toString().trim();

                                            journalDateTime = journalObj[5].toString().trim();

                                            dateJournalM = dateFormatJournal.parse(journalDateTime);
                                            newDateJournalM = dateFormatJournalM.format(dateJournalM);

                                            dateJournalD = dateFormatJournal.parse(journalDateTime);
                                            newDateJournalD = dateFormatJournalD.format(dateJournalD);

                                            dateJournalY = dateFormatJournal.parse(journalDateTime);
                                            newDateJournalY = dateFormatJournalY.format(dateJournalY);

                                            journalDownloadUrl = journalObj[4].toString().trim();

                                            journalDetailsUrl = GlobalVariable.baseUrl + "/journalManagement/journalDetails.jsp?sessionid=" + session.getId() + "&journalId=" + journalId;

                                            journalCoverPage = journalObj[7].toString().trim();

                                            journalCoverPageUrl = GlobalVariable.journalDirLink + journalCoverPage;

                                            journalCoverPage1 = "<img width=\"150\" src=\"" + journalCoverPageUrl + "\" alt=\"" + journalTitle + "\">";

                                            Query journalChildSQL = null;
                                            Object journalChildObj[] = null;
                                            String journalChildId = "";
                                            String journalChildTitle = "";
                                            String journalDownloadChildUrl = "";
                                            String journalChildTitleLink = "";
                                            String journalChapterListStr = "";

                                            journalChildSQL = dbsession.createSQLQuery("SELECT jn.id_journal,jn.journal_title,jn.journal_author,jn.journal_desc,"
                                                    + "jn.journal_link,jn.journal_date,md.mem_division_name,jn.cover_page_picture "
                                                    + "FROM journal_info jn,member_division md "
                                                    + "WHERE jn.parent_id = '" + journalId + "' AND jn.journal_division = md.mem_division_id "
                                                    + "ORDER BY jn.id_journal DESC");

                                            System.out.println("Child SQL:: " + journalChildSQL);

                                            if (!journalChildSQL.list().isEmpty()) {
                                                int chj = 1;
                                                for (Iterator itJournalChild = journalChildSQL.list().iterator(); itJournalChild.hasNext();) {
                                                    journalChildObj = (Object[]) itJournalChild.next();
                                                    journalChildId = journalChildObj[0].toString().trim();
                                                    journalChildTitle = journalChildObj[1].toString().trim();
                                                    journalDownloadChildUrl = journalObj[4].toString().trim();

                                                    journalChildTitleLink = "<a href=\"" + journalDownloadUrl + "\">" + journalChildTitle + "</a>";

                                                    journalChapterListStr = journalChapterListStr + " " + chj + "." + journalChildTitleLink + "<br>";

                                                    chj++;
                                                }
                                            } else {
                                                journalChapterListStr = "";
                                            }

                                %>


                                <tr id="infoBox<%=journalId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><%=journalCoverPage1%></td>
                                    <td><%=journalDivisionName%></td>
                                    <td><% out.print(journalTitle);%></td>
                                    <td><% out.println(journalAuthor);%></td>
                                    <td><% out.println(journalChapterListStr);%></td>
                                    <td><a href="<%=journalDownloadUrl%>">Download</a></td>
                                    <td class="text-center">

                                        <a href="<%=journalDetailsUrl%>" title="Journal details" class="btn btn-primary btn-sm"><i class="icon-info"></i></a>
                                        <a onclick="journalDeleteInfo(<% out.print("'" + journalId + "','" + sessionid + "'");%>)" title="Delete this journal" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%    dbsession.clear();
        dbsession.close();
        %>


    <%@ include file="../footer.jsp" %>