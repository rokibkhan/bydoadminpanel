<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;
    int responseCode = 0;
    String responseMsg = "";
    String thanaInfo = "";
    String userIdOptX = "";
    String newPass = "";
    String passwordOptToken = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";
    String journalInfo = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String divisionId = request.getParameter("divisionId") == null ? "" : request.getParameter("divisionId").trim();

    if (divisionId != null) {

        Query journalSQL = null;
        Object journalObj[] = null;
        String journalId = "";
        String journalTitle = "";
        String journalAuthor = "";
        String journalOption = "";

        journalSQL = dbsession.createSQLQuery("SELECT jn.id_journal,jn.journal_title,jn.journal_author,jn.journal_desc,"
                + "jn.journal_link,jn.journal_date "
                + "FROM journal_info jn "
                + "WHERE jn.journal_division = '" + divisionId + "' AND jn.parent_id = '0' "
                + "ORDER BY jn.id_journal DESC");

        if (!journalSQL.list().isEmpty()) {
            for (Iterator itJournal = journalSQL.list().iterator(); itJournal.hasNext();) {

                journalObj = (Object[]) itJournal.next();
                journalId = journalObj[0].toString().trim();
                journalTitle = journalObj[1].toString().trim();
                journalAuthor = journalObj[2].toString().trim();

                journalOption = journalOption + "<option value=\"" + journalId + "\">" + journalTitle + "</option>";

            }
        }

        journalInfo = "<select  id=\"journalParent\" name=\"journalParent\" class=\"form-control\" required>"
                + "<option value=\"0\">Parent</option>"
                + "" + journalOption + ""
                + "</select>";

        responseCode = 1;
        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Success!</strong> Thana Info show."
                + "</div>";

    } else {

        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    json = new JSONObject();
    json.put("journalInfo", journalInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    jsonArr.add(json);
    out.print(jsonArr);

%>