<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    int responseMsgCode = 0;
    String responseMsg = "";
    String responseMsgHTML = "";
    Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String pageId = request.getParameter("pageId");
    String sessionid = request.getParameter("sessionid").trim();

    Query qq = dbsession.createSQLQuery("SELECT ni.id_page, ni.page_title,  ni.page_short_desc, ni.page_desc, ni.add_date"
                                            + " FROM  page_info ni WHERE ni.id_page=" +  pageId);
    String title = "";
    String description = "";
    String shortDescription = "";
    String pageDateTime = "";
    
    boolean isSticky = false;
    
    if(!qq.list().isEmpty()){
        Iterator iterator  = qq.list().iterator();
        Object[] obj1 = (Object[]) iterator.next();
        pageId = obj1[0].toString().trim();
        title = obj1[1].toString().trim();
        //  pageSlug = obj[2].toString().trim();
        shortDescription = obj1[2].toString().trim();
        description = obj1[3].toString().trim();
        pageDateTime = obj1[4].toString().trim();
      
        
    }
    
    
    responseMsgCode = 1;
    responseMsg = "Page fetching successfully Completed.";
    responseMsgHTML = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
            + "<strong>Delete successfully Completed.</strong>"
            + "</div>";

    json = new JSONObject();
    json.put("selectInvId", pageId);
    json.put("responseMsgCode", responseMsgCode);
    json.put("responseMsg", responseMsg);
    json.put("title", title);
    json.put("description", description);
    json.put("shortDescription", shortDescription);
    json.put("pageDateTime", pageDateTime);
    json.put("isSticky", isSticky);
    jsonArr.add(json);

    out.println(jsonArr);

    dbsession.flush();
    dbtrx.commit();
    dbsession.close();


%>