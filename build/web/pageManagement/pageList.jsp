

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">



        function showSyUserDetails(arg1, arg2) {
            console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');
        }





        function showPageDetails(arg1, arg2) {

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Details for page #" + arg1);



            $.post("pageShowProcess.jsp", {pageId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    rupantorLGModal.find("#rupantorLGModalTitle").html("Page #" + arg1 + "<br/>" + data[0].title);
                    rupantorLGModal.find("#rupantorLGModalBody").html(data[0].description);

//                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
//                    $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h2>"+ data[0].responseMsg + "</h2></td>");
//                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");



            rupantorLGModal.modal('show');
        }



        function pageDeleteInfo(arg1, arg2) {

            console.log("pageDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Page Delete confirmation");
            btnInfo = "<a onclick=\"pageDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";
            rupantorLGModal.find("#rupantorLGModalBody").html('');

            if (modifyFlag === false) {
                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());
                modifyFlag = true;
            }

//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }


        function pageDeleteInfoConfirm(arg1, arg2) {
            console.log("pageDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');

//            $.post(
//                    "pageDeleteProcess.jsp", 
//                    {userId: arg1, sessionid: arg2},
//                    function(data){
//                
//                    }
//          );

            $.post("pageDeleteProcess.jsp", {pageId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Page List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Page Management</a></li>                    
                    <li class="active">Page</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th class="text-center">Short Details</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String pageId = "";
                                    String pageCategory = "";
                                    String pageTitle = "";
                                    String pageShortDetails = "";
                                    String pageDetails = "";
                                    String pagePublished = "";
                                    String pageAddUser = "";
                                    String pageAddDate = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT ni.id_page, ni.page_category, ni.page_title, "
                                            + "ni.page_short_desc, ni.page_desc, ni.published, ni.add_term  "
                                            + " FROM  page_info ni ORDER BY ni.id_page DESC");

                                    if (!usrSQL.list().isEmpty()) {

                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();

                                            pageId = obj[0].toString().trim();

                                            if (obj[1] != null) {
                                                pageCategory = obj[1].toString().trim();
                                            }

                                            pageTitle = obj[2].toString().trim();
                                            pageShortDetails = obj[3].toString().trim();
                                            pageDetails = obj[4].toString().trim();
                                            pagePublished = obj[5].toString().trim();

                                            System.out.println("THIS IS AWE");

                                            String pageEditUrl = GlobalVariable.baseUrl + "/pageManagement/pageEdit.jsp?sessionid=" + session.getId() + "&pageIdX=" + pageId + "&selectedTab=profile";
                                            String pageDeleteUrl = GlobalVariable.baseUrl + "/pageManagement/page.jsp?sessionid=" + session.getId() + "&pageIdX=" + pageId + "&selectedTab=changePass";


                                %>


                                <tr id="infoBox<%=pageId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(pageTitle);%></td>
                                    <td><% out.println(pageShortDetails); %></td>
                                    <td><% out.print(pagePublished); %></td>
                                    <td class="text-center">



                                        <a  href="<%=pageEditUrl%>" title="Edit Page" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onclick="pageDeleteInfo(<% out.print("'" + pageId + "','" + sessionid + "'");%>)" title="Delete this page" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->
    <%    dbsession.clear();
        dbsession.close();
        %>



    <%@ include file="../footer.jsp" %>