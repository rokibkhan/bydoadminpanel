<%-- 
    Document   : designationAdd
    Created on : December 20, 2018, 01:04:45 PM
    Author     : Akter & Tahajjat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<% request.setAttribute("TITLE", "New Invoice Generation");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>


<link rel="stylesheet" href="../jquery/jquery-ui.css" />

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"></script>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">


<%

    String read = "read";
    String write = "write";

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
%>

<!-- Page Content -->
<div id="page-wrapper"> 
    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Salary</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Account</a></li>
                    <li class="active">Close Financial Year</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>

        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-12"> 

                       
                                <form  name="billGenForm" id="billGenForm" action="hotBillGenReport.jsp?sessionid=<%=sessionid%>" action="post">


                                    <%
                                        String status = "";
                                        String userId = session.getAttribute("username").toString();
                                        ClosedFinancialYear closedFinancialYear = new ClosedFinancialYear();
                                        
                                        if (request.getParameter("billReference") != null) {
                                         String   billReference = request.getParameter("billReference").trim();
                                            
                                            // out.println(billReference);
                                            String Result = closedFinancialYear.genTempBill(billReference,userId,userStoreId);
                                            out.println("<font color='green' size='3'>" + Result + "</font>");

                                            if (Result.equalsIgnoreCase("Successfully Generated")) {
                                            }
                                        }

                                    %>



                                </form>



                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>

        </div>
        <!-- /.row -->
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);
            %>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>   