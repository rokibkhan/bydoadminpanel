
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">



<script type="text/javascript">

    function memberPaymentByAdmin(arg1, arg2, arg3) {
        console.log("memberPaymentByAdmin :: " + arg1);
        var rupantorLGModal;

        $("#btnShowInvoicePaymentOption").button("loading");


        rupantorLGModal = $('#rupantorLGModal');
        rupantorLGModal.find("#rupantorLGModalTitle").text("Payment Transaction Information");

        $.post("memberPaymentOptionShow.jsp", {memberId: arg1, txnId: arg2, sessionid: arg3}, function (data) {

            //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
            //showInvoicePaymentOptionDetails
            rupantorLGModal.find("#rupantorLGModalBody").html(data[0].mainInfoContainer);
            rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
            rupantorLGModal.modal('show');

        }, "json");
    }



    function memberPaymentByAdminConfirm(arg1, arg2, arg3) {
        console.log("memberPaymentByAdminConfirm :: " + arg1);
        console.log("memberPaymentByAdminConfirm2 :: " + arg2);
        console.log("memberPaymentByAdminConfirm3 :: " + arg3);
        var rupantorLGModal, paymentRefNo, paymentRefDesc;

        rupantorLGModal = $('#rupantorLGModal');
        rupantorLGModal.find("#rupantorLGModalTitle").text("Payment Transaction Information");

        //  paymentComment = document.getElementById("paymentComment").value;
        paymentRefNo = $("#paymentRefNo").val();
        paymentRefDesc = $("#paymentRefDesc").val();
        //  alert("paymentRefNo:: " + paymentRefNo + " :: paymentRefDesc:: " + paymentRefDesc);

        if (!!paymentRefNo) {
            $("#paymentRefNoErr").html("");
        } else {

            $("#paymentRefNo").focus();
            $("#paymentRefNo").css({"background-color": "#fff", "border": "1px solid red"});
            $("#paymentRefNoErr").html("Payment ref is required.");
            return false;
        }

        if (!!paymentRefDesc) {
            $("#paymentRefDescErr").html("");
        } else {

            $("#paymentRefDesc").focus();
            $("#paymentRefDesc").css({"background-color": "#fff", "border": "1px solid red"});
            $("#paymentRefDescErr").html("Ref desc is required.");
            return false;
        }

//            if (paymentComment == "") {
//                $("#paymentComment").focus();
//                $("#paymentComment").css({"background-color": "#fff", "border": "1px solid red"});
//                $("#paymentCommentErr").html("Payment description is required.");
//                return false;
//            } else {
//                $("#paymentCommentErr").html("");
//            }

        $.post("memberPaymentOptionConfirm.jsp", {memberId: arg1, txnId: arg2, sessionid: arg3, paymentRefNo: paymentRefNo, paymentRefDesc: paymentRefDesc}, function (data) {

            console.log(data);

            if (data[0].responseCode == 1) {
                // rupantorLGModal.find("#rupantorLGModalBody").html(data.memberDetails);
                //  rupantorLGModal.find("#rupantorLGModalFooter").html(data.btnInvInfo);
                $("#mPayTrans" + data[0].requestId).html(data[0].mainInfoContainer);
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show();

                rupantorLGModal.modal('hide');
            } else {
                rupantorLGModal.find("#rupantorLGModalBody").html('<p>' + data[0].responseMsg + '</p>');
                rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                rupantorLGModal.modal('show');
            }

        }, "json");
    }


    function printInvoiceInfo(arg1) {
        var divToPrint = document.getElementById("divToPrint" + arg1);
        var popupWin = window.open('', '_blank', 'width=500,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }

</script>
<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    Object memberObj[] = null;
    Query memberSQL = null;
    String memberId = "";
    String memberIEBId = "";
    String memberName = "";
    String memberFatherName = "";
    String memberMotherName = "";
    String memberBirthPlace = "";
    String memberBirthDate = "";
    String memberBloodGroup = "";
    String memberCountryCode = "";
    String memberGender = "";
    String memberGender1 = "";
    String memberApplyFor = "";
    String memberApplyFor1 = "";
    String memberOldNumber = "";
    String memberEmail = "";
    String memberMobile = "";

    String tabProfileActive = "";
    String tabActivityActive = "";
    String tabMessagesActive = "";
    String tabSettingsActive = "";
    String tabRecomendationActive = "";
    String tabDocumentActive = "";
    String tabChangePassActive = "";

    String memberNewPaymentInfoUrl = "";
    String pictureName = "";
    String pictureLink = "";
    String selectedUserId = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

    System.out.println("selectedUserId :: " + selectedUserId);

    String totalPaymentDue = "0";

    String dueSumSQL = "select sum(amount) from member_fee  WHERE  member_id=" + selectedUserId + " AND status ='0'";

    totalPaymentDue = dbsession.createSQLQuery(dueSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(dueSumSQL).uniqueResult().toString();
    //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

    System.out.println("Member Deatils :: totalPaymentDue :: " + totalPaymentDue);

    if (!selectedUserId.equals("")) {

        System.out.println("selectedUserId IN :: " + selectedUserId);

        memberSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id,  "
                + " m.member_name, m.father_name, m.mother_name, "
                + "m.place_of_birth,m.dob, m.picture_name,m.blood_group, "
                + "m.country_code,m.gender,m.apply_for,m.old_member_id,m.mobile,m.email_id  "
                + "FROM  member m "
                + "WHERE m.id = '" + selectedUserId + "'"
                + "ORDER BY m.id DESC");

        if (!memberSQL.list().isEmpty()) {
            for (Iterator it1 = memberSQL.list().iterator(); it1.hasNext();) {

                memberObj = (Object[]) it1.next();
                memberId = memberObj[0].toString().trim();
                memberIEBId = memberObj[1] == null ? "" : memberObj[1].toString().trim();
                memberName = memberObj[2] == null ? "" : memberObj[2].toString().trim();
                memberFatherName = memberObj[3] == null ? "" : memberObj[3].toString().trim();
                memberMotherName = memberObj[4] == null ? "" : memberObj[4].toString().trim();
                memberBirthPlace = memberObj[5] == null ? "" : memberObj[5].toString().trim();
                memberBirthDate = memberObj[6] == null ? "" : memberObj[6].toString().trim();
                memberBloodGroup = memberObj[8] == null ? "" : memberObj[8].toString().trim();
                memberCountryCode = memberObj[9] == null ? "" : memberObj[9].toString().trim();
                memberGender = memberObj[10] == null ? "" : memberObj[10].toString().trim();

                if (memberGender.equals("M")) {
                    memberGender1 = "Male";
                }
                if (memberGender.equals("F")) {
                    memberGender1 = "Female";
                }
                if (memberGender.equals("")) {
                    memberGender1 = "";
                }

                memberApplyFor = memberObj[11] == null ? "" : memberObj[11].toString().trim();
                if (memberApplyFor.equals("1")) {
                    memberApplyFor1 = "Fellow";
                }
                if (memberApplyFor.equals("2")) {
                    memberApplyFor1 = "Member";
                }
                if (memberApplyFor.equals("3")) {
                    memberApplyFor1 = "Associate Member";
                }

                memberOldNumber = memberObj[11] == null ? "" : memberObj[11].toString().trim();

                memberMobile = memberObj[13] == null ? "" : memberObj[13].toString().trim();
                memberEmail = memberObj[14] == null ? "" : memberObj[14].toString().trim();

                pictureName = memberObj[7] == null ? "" : memberObj[7].toString().trim();

                pictureLink = "<img width=\"90\" src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

                memberNewPaymentInfoUrl = GlobalVariable.baseUrl + "/account/memberNewPaymentInfoAdd.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";

            }
        }

    } else {

        System.out.println("selectedUserId OUt ::");

        //  response.sendRedirect(GlobalVariable.baseUrl + "/home.jsp");
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Member Payment</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Finance & Account</a></li>  
                    <li class="active">Member Payment</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">

                <div class="col-md-12 col-xs-12">
                    <div class="white-box">

                        <div class="row">
                            <div class="col-md-5">

                                <div class="row">
                                    <div class="col-md-3"><strong>Member ID</strong></div>
                                    <div class="col-md-8"><%out.print(memberIEBId);%></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"><strong>Name</strong></div>
                                    <div class="col-md-8"><%out.print(memberName);%></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"><strong>Father Name</strong></div>
                                    <div class="col-md-8"><%=memberFatherName%></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3"><strong>Mother Name</strong></div>
                                    <div class="col-md-8"><%=memberMotherName%></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"><strong>Gender</strong></div>
                                    <div class="col-md-8"><%=memberGender1%></div>
                                </div>                                
                            </div>
                            <div class="col-md-5">

                                <div class="row">
                                    <div class="col-md-3"><strong>Date of Birth</strong></div>
                                    <div class="col-md-8"><%=memberBirthDate%></div>
                                </div>                                 
                                <div class="row">
                                    <div class="col-md-3"><strong>Blood Group</strong></div>
                                    <div class="col-md-8"><%=memberBloodGroup%></div>
                                </div>                                
                                <div class="row">
                                    <div class="col-md-3"><strong>Place of Birth</strong></div>
                                    <div class="col-md-8"><%=memberBirthPlace%></div>
                                </div>                                

                            </div>
                            <div class="col-md-2">
                                <%=pictureLink%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <a class="btn btn-primary p-1" href="<%=memberNewPaymentInfoUrl%>"><i class="fa fa-credit-card" style="font-size:14px;"></i> Add New Payment</a>
                            </div>
                        </div>






                        <h4 class="font-bold1 m-t-30">Total Due Information : <strong><%=totalPaymentDue%></strong> TK</h4>


                        <h4 class="font-bold1 m-t-30">Payment History</h4>
                        <hr>
                        <%
                            //   MemberFeeTemp mpi = null;

                            Query feeSQL = null;
                            Object[] feeObject = null;

                            String memberShipFeeTransId = "";
                            String memberShipFeeMemberId = "";
                            String memberShipFeeRefNo = "";
                            String memberShipFeeRefDesc = "";

                            String memberShipFeePaidDate = "";
                            String memberShipFeeYear = "";
                            String memberShipFeeAmount = "";
                            String memberShipFeeStatus = "";
                            String memberShipFeeStatus1 = "";
                            String memberShipFeeBillType = "";
                            String memberShipFeeBillType1 = "";
                            String memberShipFeePayOption = "";
                            String memberShipFeePayOptionText = "";
                            String memberShipFeeCurrency = "BDT";
                            String memberPaymentDetailsInfo = "";
                            String memberPaymentBtn = "";

                            // feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
                            feeSQL = dbsession.createSQLQuery("SELECT mf.txn_id,mf.member_id,mf.ref_no,mf.ref_description, "
                                    + "mf.txn_date,mf.paid_date,mf.fee_year,mf.amount,mf.status,mf.due_date,mf.bill_type,"
                                    + "mfy.member_fees_year_name,mf.pay_option  "
                                    + "FROM member_fee mf,member_fees_year mfy WHERE mf.fee_year = mfy.member_fees_year_id AND mf.member_id= '" + selectedUserId + "' ORDER BY mf.txn_id DESC");


                        %>
                        <div class="container">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Txn No.</th>
                                            <th scope="col">Payment Date</th>
                                            <th scope="col">Bill Type</th>
                                            <th scope="col">Fee Year</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Status</th> 
                                            <th scope="col">Ref No</th> 
                                            <th scope="col">Payment Info</th>
                                            <th scope="col">Pay</th>
                                            <th scope="col">View Bill</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%                                            if (!feeSQL.list().isEmpty()) {
                                                int ip = 1;
                                                String divToPrint = "";
                                                for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                                                    feeObject = (Object[]) feeItr.next();
                                                    memberShipFeeTransId = feeObject[0].toString();
                                                    memberShipFeeMemberId = feeObject[1].toString();

                                                    memberShipFeeRefNo = feeObject[10] == null ? "" : feeObject[2].toString().trim();
                                                    memberShipFeeRefNo = feeObject[3] == null ? "" : feeObject[3].toString().trim();

                                                    memberShipFeePaidDate = feeObject[5] == null ? "" : feeObject[5].toString().trim();

                                                    memberShipFeeAmount = feeObject[7].toString();

                                                    memberShipFeeYear = feeObject[11] == null ? "" : feeObject[11].toString().trim();
                                                    memberShipFeeStatus = feeObject[8] == null ? "" : feeObject[8].toString().trim();

                                                    //   System.out.println("memberShipFeeStatus :: " + memberShipFeeStatus);
                                                    //    System.out.println("memberPaymentDetailsInfo :: " + memberShipFeeRefDesc);
                                                    String arg = "'" + memberShipFeeMemberId + "','" + memberShipFeeTransId + "','" + sessionid + "'";

                                                    if (memberShipFeeStatus.equals("1")) {
                                                        memberShipFeeStatus1 = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
                                                        memberShipFeeRefDesc = "";
                                                    }
                                                    if (memberShipFeeStatus.equals("0")) {
                                                        memberShipFeeStatus1 = "<span class=\"btn btn-danger btn-sm waves-effect\">Due</span>";
                                                        // payOnline = "<a title=\"Pay Online\" href=\"" + GlobalVariable.baseUrl + "/member/memberPaymentProcessing.jsp?sessionid=" + session.getId() + "&act=add&&memberFeeId=" + memberFeeId + "\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-credit-card\"></i></a>";                                                    

                                                        memberPaymentBtn = "<a class=\"btn btn-primary p-1\" title=\"Cash Payment\" onclick=\"memberPaymentByAdmin(" + arg + ")\"><i class='fa fa-credit-card' style='font-size:14px;'></i> Add Payment</a> ";
                                                        memberShipFeeRefDesc = memberPaymentBtn;
                                                    }
                                                    if (memberShipFeeStatus.equals("2")) {
                                                        memberShipFeeStatus1 = "<span class=\"btn btn-warning btn-sm waves-effect\">Fail</span>";
                                                        memberShipFeeRefDesc = "";
                                                    }

                                                    if (memberShipFeeStatus.equals("")) {
                                                        memberShipFeeStatus1 = "<span class=\"btn btn-warning btn-sm waves-effect\">Null</span>";
                                                        memberShipFeeRefDesc = "";
                                                    }

                                                    memberShipFeeBillType = feeObject[10] == null ? "" : feeObject[10].toString().trim();
                                                    //  1=Renewal,2=Registration ,3=Life, 4=Above65,5=Unknown
                                                    if (memberShipFeeBillType.equals("1")) {
                                                        memberShipFeeBillType1 = "Renewal";
                                                    }
                                                    if (memberShipFeeBillType.equals("2")) {
                                                        memberShipFeeBillType1 = "Registration";
                                                    }
                                                    if (memberShipFeeBillType.equals("3")) {
                                                        memberShipFeeBillType1 = "Life";
                                                    }
                                                    if (memberShipFeeBillType.equals("4")) {
                                                        memberShipFeeBillType1 = "Above65";
                                                    }
                                                    if (memberShipFeeBillType.equals("5")) {
                                                        memberShipFeeBillType1 = "Unknown";
                                                    }

                                                    memberShipFeePayOption = feeObject[12] == null ? "" : feeObject[12].toString().trim();

                                                    if (memberShipFeePayOption.equals("1") && memberShipFeeStatus.equals("1")) {
                                                        memberShipFeePayOptionText = "Online";
                                                    } else if (memberShipFeePayOption.equals("0") && memberShipFeeStatus.equals("1")) {
                                                        memberShipFeePayOptionText = "Cash";
                                                    } else {
                                                        memberShipFeePayOptionText = "";
                                                    }

                                                    divToPrint = "<div id=\"divToPrint" + memberShipFeeTransId + "\" style=\"display:none;\"><!-- Start:  Print Container  -->"
                                                            + "<html>"
                                                            + "<head><title>Bill Invoice Print</title></head>"
                                                            + "<body>"
                                                            + "<div style=\"width: 790px; margin: 0px auto; margin-top: 1.5cm;\">"
                                                            + "<table style=\"width: 95%; margin-bottom: 5px;\">"
                                                            + "<tr>"
                                                            + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                            + "<td style=\"padding: 5px; text-align: right;\">"
                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">Office Copy</span>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">"
                                                            + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\">"
                                                            + "</td>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                            + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                            + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                            + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">&nbsp;</td>"
                                                            + "<td style=\"text-align: center;\">"
                                                            + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span>"
                                                            + "</td>"
                                                            + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td style=\"text-align: center;\">"
                                                            + "<table>"
                                                            + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + memberName + "</td></tr>"
                                                            + "<tr><td style=\"text-align: left;\"><strong>Member ID </strong></td><td>:" + memberIEBId + "</td></tr>"
                                                            + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + memberMobile + "</td></tr>"
                                                            + "</table>"
                                                            + "</td>"
                                                            + "<td style=\"text-align: center;\">"
                                                            + "<table>"
                                                            + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeePaidDate + "</td></tr>"
                                                            + "<tr><td><strong>Invoice No</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                                                            + "<tr><td><strong>Status</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                                                            + "</table>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<thead>"
                                                            + "<tr>"
                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                            + "</tr>"
                                                            + "</thead>"
                                                            + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                            + "<tr>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeYear + "</td>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeBillType + "</td>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                            + "</tr>"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                            + "</tr>"
                                                            + "</tbody>"
                                                            + "</table> <!--End : invoice product list details-->"
                                                            + "<table style=\"width: 95%; border-top:1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">This is computer generated invoice no signature required.</span>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; border-top:1px dotted #ccc; border-collapse: collapse; margin-top: 30px;;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; margin-bottom: 5px;\">"
                                                            + "<tr>"
                                                            + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                            + "<td style=\"padding: 5px; text-align: right;\">"
                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">Member Copy</span>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">"
                                                            + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\">"
                                                            + "</td>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                            + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                            + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                            + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">&nbsp;</td>"
                                                            + "<td style=\"text-align: center;\">"
                                                            + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span>"
                                                            + "</td>"
                                                            + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td style=\"text-align: center;\">"
                                                            + "<table>"
                                                            + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + memberName + "</td></tr>"
                                                            + "<tr><td style=\"text-align: left;\"><strong>Member ID </strong></td><td>:" + memberIEBId + "</td></tr>"
                                                            + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + memberMobile + "</td></tr>"
                                                            + "</table>"
                                                            + "</td>"
                                                            + "<td style=\"text-align: center;\">"
                                                            + "<table>"
                                                            + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeePaidDate + "</td></tr>"
                                                            + "<tr><td><strong>Invoice No</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                                                            + "<tr><td><strong>Status</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                                                            + "</table>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<thead>"
                                                            + "<tr>"
                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                            + "</tr>"
                                                            + "</thead>"
                                                            + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                            + "<tr>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeYear + "</td>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeBillType + "</td>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                            + "</tr>"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                            + "</tr>"
                                                            + "</tbody>"
                                                            + "</table> <!--End : invoice product list details-->"
                                                            + "<table style=\"width: 95%; border-top:1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                            + "<tr>"
                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">This is computer generated invoice no signature required.</span>"
                                                            + "</td>"
                                                            + "</tr>"
                                                            + "</table>"
                                                            + "</div>"
                                                            + "</body>"
                                                            + "</html>"
                                                            + "</div><!-- End:  Print Container  -->";


                                        %>
                                        <tr id="mPayTrans<%=memberShipFeeTransId%>">
                                            <td><%=ip%></td>
                                            <td><%=memberShipFeeTransId%></td>
                                            <td><%=memberShipFeePaidDate%></td>
                                            <td><%=memberShipFeeBillType1%></td>
                                            <td><%=memberShipFeeYear%></td>
                                            <td><%=memberShipFeeAmount%></td> 
                                            <td><%=memberShipFeeStatus1%></td> 
                                            <td><%=memberShipFeeRefNo%></td> 
                                            <td><%=memberShipFeeRefDesc%></td> 
                                            <td><%=memberShipFeePayOptionText%></td> 
                                            <td>
                                                <a onclick="printInvoiceInfo('<%=memberShipFeeTransId%>');"  class="btn btn-primary btn-sm">&nbsp; <i class="fa fa-file"></i>&nbsp; </a>

                                                <%=divToPrint%>
                                            </td> 
                                        </tr>

                                        <%
                                                    ip++;
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>

                            </div>    

                        </div>   
                        <!-- .Payment Information-->






                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>