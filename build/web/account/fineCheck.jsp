<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

        int ix = 1;

        // String targetpage = "memberListFellow.jsp";
        String targetpageWithSession = "fineCheck.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        int limit = 100;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();

        //  System.out.println("filterSQLStr :: " + filterSQLStr);
        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
        int pageNbr = Integer.parseInt(pageNbr1);

        //  System.out.println("filter :: " + filter);
        //  System.out.println("pageNbr :: " + pageNbr);
        String memberId = "";
        String memberIEBId = "";
        String memberName = "";
        String memberPictureName = "";
        String memberPictureUrl = "";
        String memberPicture = "";
        String memberMobile = "";
        String memberEmail = "";
        String memberType = "";

        String mOthersInfo = "";
        String memberUniversityName = "";
        String memberDivisionName = "";
        String memberCenterName = "";
        String agrX = "";

        String memberDetailsUrl = "";
        String memberPaymentInfoUrl = "";

        String txn_id = "";
        String ref_no = "";
        String ref_description = "";
        String txn_date = "";

        String paid_date = "";
        String fee_year = "";
        String feeYearText = "";
        String amount = "";
        String transactionStatus = "";
        String transactionStatusText = "";
        String due_date = "";
        String bill_type = "";
        String billTypeText = "";
        String pay_option = "";

        Query feeSQL = null;
        Object[] feeObject = null;

        String memberShipFeeTransId = "";
        String memberShipFeeTxnDate = "";
        String memberShipFeeAmount = "";
        String memberShipFeeCurrency = "BDT";
        String memberShipFeeBillType = "";
        String memberShipFeeBillYear = "";
        String transactionContainer = "";
        String transactionContainerFull = "";

        double totalDueAmount = 0.0d;
        String fineAmount = "";
        double totalDueWithFineAmount = 0.0d;
    %>

    <script type="text/javascript">

        function showMemebrSearchSubCenterInfo(arg1) {


            console.log("showMemebrSearchSubCenterInfo arg1:: " + arg1);
            var arg2 = "";

            $.post("memberCenterWiseSubCenterInfoShow.jsp", {centerId: arg1, addressInfo: arg2}, function (data) {

                if (data[0].responseCode == 1) {
                    $("#showSubCenterInfo").html(data[0].subCenterInfo);
                }

            }, "json");
        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Fine Eligible Member List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Account</a></li>                    
                    <li class="active">Fine</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">
                    <%
                        String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();
                        String searchQuery = "";
                        /*
                        if (!searchString.equals("")) {
                            // searchQuery = "WHERE member_name like '%" + searchString + "%' OR email_id like '%" + searchString + "%' OR member_id like '%" + searchString + "%' or m.mobile like '%" + searchString + "%'";

                            //  searchQuery = "WHERE mf.txn_id like '%" + searchString + "%' OR m.member_id like '%" + searchString + "%' OR m.member_name like '%" + searchString + "%' or m.mobile like '%" + searchString + "%' OR m.email_id like '%" + searchString + "%'";
                            searchQuery = "WHERE mf.status in (0,1) AND mf.txn_id like '%" + searchString + "%'";

                        } else {
                            searchQuery = "WHERE mf.status in (0,1) ";
                        }
                         */
                        searchQuery = "WHERE  m.member_id !=\"\" ";
                    %>

                    <form class="form-group"  name="memberPaymentAdd" id="memberPaymentAdd" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/account/memberFineCheck.jsp?sessionid=<%out.print(sessionid);%>" >
                        <div class="input-group">
                            <input type="text" id="searchString" name="searchString" value="<%=searchString%>" class="form-control" placeholder="Member ID" disabled> 
                            <span class="input-group-btn">
                                <!--                                <button type="submit" name="btnSearch" class="btn waves-effect waves-light btn-info disabled">
                                                                    <i class="fa fa-search"></i> Search
                                                                </button>-->
                            </span> 
                        </div>
                    </form>
                </div>
                <!-- /.white-box -->
            </div>

            <div class="col-md-12">
                <div class="white-box">




                    <%
                        //String searchCountSQL = "SELECT count(*) FROM  member m,member_type mt "
                        //        + "WHERE m.status = 1 "
                        //        + "AND m.id = mt.member_id "
                        //       + " " + filterSQLStr + " "
                        //       + "ORDER BY m.id DESC";

//                        String searchCountSQL = "SELECT count(*) FROM  member_fee  mf "
//                                + "LEFT JOIN member m ON m.id = mf.member_id  "
//                                + "" + searchQuery + " "
//                                + "ORDER BY mf.txn_id DESC";
                        String searchCountSQL = "SELECT count(*) FROM  member_fine_check1  fc "
                                + "LEFT JOIN member m ON m.id = fc.member_id  "
                                + "LEFT JOIN member_type_info ti ON ti.member_type_id = fc.member_type  "
                                + "" + searchQuery + " "
                                + "ORDER BY fc.txn_id DESC";

                        String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        if (pageNbr != 0) {
                            start = (pageNbr - 1) * limit;
                        } else {
                            start = 0;
                        }

                        //   System.out.println("startLLLL :: " + start);
                        //    System.out.println("limitLLLL :: " + limit);
                        // Initial page num setup
                        if (pageNbr == 0) {
                            pageNbr = 1;
                        }
                        prev = pageNbr - 1;
                        next = pageNbr + 1;
                        lastpage1 = Math.ceil((double) total_pages / limit);
                        //   DecimalFormat df = new DecimalFormat("#.##");
                        //  System.out.print(df.format(lastpage1));

                        // lastpage = (int) Math.ceil(total_pages / limit);
                        lastpage = (int) lastpage1;
                        LastPagem1 = (int) Math.round(lastpage - 1);

                        //  System.out.println("total_pages :: " + total_pages);
                        //   System.out.println("limit :: " + limit);
                        //   System.out.println("lastpage1 :: " + lastpage1);
                        //   System.out.println("lastpage :: " + lastpage);
                        //   System.out.println("LastPagem1 :: " + LastPagem1);
                        String paginate = "";
                        if (lastpage > 1) {
                            paginate = paginate + "<ul class='pagination' style=\"margin:0\">";

                            // Previous
                            if (pageNbr > 1) {
                                p = "page=" + prev;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>previous</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
                            }

                            int counter = 1;
                            // Pages
                            if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

                                for (counter = 1; counter <= lastpage; counter++) {
                                    if (counter == pageNbr) {
                                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                    } else {
                                        p = "page=" + counter;
                                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                    }
                                }
                            } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
                                // Beginning only hide later pages
                                if (pageNbr < 1 + (stages * 2)) {
                                    for (counter = 1; counter < 4 + (stages * 2); counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // Middle hide some front and some back
                                else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // End only hide early pages
                                else {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                                    for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                }
                            }

                            // Next
                            if (pageNbr < counter - 1) {
                                p = "page=" + next;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>next</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
                            }

                            paginate = paginate + "</ul>";
                        }

                        int minPaginationIndexLimit = ((pageNbr - 1) * limit);
                        int maxPaginationIndexLimit = (pageNbr * limit);
                        int lastProductIndex = total_pages;
                        int minPaginationIndexLimitPlus = minPaginationIndexLimit + 1;
//                        if(minPaginationIndexLimitPlus < limit){
//                            minPaginationIndexLimitPlus = 1;
//                        }

                        if (maxPaginationIndexLimit >= lastProductIndex) {
                            maxPaginationIndexLimit = lastProductIndex;
                        }

//                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "md.mem_division_name,c.center_name   "
//                                + "FROM  member m ,member_type mt,member_division md,center c "
//                                + "WHERE m.status = 1 "
//                                + "AND m.member_division_id = md.mem_division_id "
//                                + "AND m.center_id = c.center_id "
//                                + "AND m.id = mt.member_id "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY m.id DESC  LIMIT " + start + ", " + limit + "";
//                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "mf.txn_id,mf.ref_no,mf.ref_description,mf.txn_date, "
//                                + "mf.paid_date,mf.fee_year,mf.amount,mf.status transactionStatus,  "
//                                + "mf.due_date,mf.bill_type,mf.pay_option,fy.member_fees_year_name   "
//                                + "FROM  member_fee mf "
//                                + "LEFT JOIN member m ON m.id = mf.member_id  "
//                                + "LEFT JOIN member_fees_year fy ON fy.member_fees_year_id = mf.fee_year  "
//                                + "" + searchQuery + " "
//                                + "ORDER BY mf.txn_id DESC  LIMIT " + start + ", " + limit + "";
                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
                                + "m.picture_name,m.mobile,m.email_id,fc.member_type,ti.member_type_name  "
                                + "FROM  member_fine_check1  fc "
                                + "LEFT JOIN member m ON m.id = fc.member_id  "
                                + "LEFT JOIN member_type_info ti ON ti.member_type_id = fc.member_type  "
                                + "" + searchQuery + " "
                                + "ORDER BY fc.txn_id DESC  LIMIT " + start + ", " + limit + "";

                        //    System.out.println("searchCountSQL :: " + searchCountSQL);
                        //    System.out.println("searchSQL :: " + searchSQL);

                    %>
                    <div class="row">

                        <div class="col-md-8" style="padding: 20px 0;">
                            <%=paginate%>
                        </div>

                        <div class="col-md-4" style="padding-top: 23px;">
                            <p class="text-right">
                                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">#</th>
                                        <th class="text-center" style="width: 5%;">Picture</th>
                                        <th class="text-center" style="width: 5%;">ID</th>
                                        <th class="text-center" style="width: 5%;">Fine</th>
                                        <th class="text-center" style="width: 7%;">FineWithTotalDue</th>
                                        <th class="text-center" >Transaction & Total Due Amount</th>
                                        <th class="text-center" style="width: 20%;">Member Info</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <%
                                        Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                                        transactionContainer = "";

                                        if (!searchSQLQry.list().isEmpty()) {
                                            for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                                                searchObj = (Object[]) searchItr.next();
                                                memberId = searchObj[0].toString();
                                                memberIEBId = searchObj[1] == null ? "" : searchObj[1].toString();
                                                memberName = searchObj[2] == null ? "" : searchObj[2].toString();

                                                memberPictureName = searchObj[3] == null ? "" : searchObj[3].toString();
                                                memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPictureName;
                                                memberPicture = "<img width=\"120\" src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\">";

                                                memberMobile = searchObj[4] == null ? "" : searchObj[4].toString();
                                                memberEmail = searchObj[5] == null ? "" : searchObj[5].toString();
                                                memberType = searchObj[6] == null ? "" : searchObj[6].toString();

                                                mOthersInfo = "<strong>MemberID:</strong>" + memberIEBId + "<br>"
                                                        + "<strong>Name:</strong>" + memberName + "<br>"
                                                        + "<strong>Mobiile:</strong>" + memberMobile + "<br>"
                                                        + "<strong>Mobiile:</strong>" + memberEmail + "<br>"
                                                        + "<strong>Division:</strong>" + memberDivisionName + "<br>"
                                                        + "<strong>Center:</strong>" + memberCenterName + "<br>"
                                                        + "<strong>University:</strong>" + memberUniversityName;

                                                feeSQL = dbsession.createSQLQuery("select f.txn_id,f.txn_date,f.amount,"
                                                        + "f.bill_type,y.member_fees_year_name  "
                                                        + "FROM member_fee f,member_fees_year y "
                                                        + "WHERE f.fee_year=y.member_fees_year_id AND "
                                                        + "f.member_id= '" + memberId + "' AND f.status = 0 "
                                                        + "ORDER BY f.txn_id DESC");
                                                int inmf = 1;
                                                for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                                                    feeObject = (Object[]) feeItr.next();
                                                    memberShipFeeTransId = feeObject[0].toString();

                                                    memberShipFeeTxnDate = feeObject[1] == null ? "" : feeObject[1].toString();
                                                    memberShipFeeAmount = feeObject[2].toString();
                                                    memberShipFeeBillType = feeObject[3] == null ? "" : feeObject[3].toString();
                                                    memberShipFeeBillYear = feeObject[4] == null ? "" : feeObject[4].toString();

                                                    transactionContainer = transactionContainer + "<tr>"
                                                            + "<td>" + inmf + "</td>"
                                                            + "<td>" + memberShipFeeTransId + "</td>"
                                                            + "<td>" + memberShipFeeTxnDate + "</td>"
                                                            + "<td>" + memberShipFeeBillYear + "</td>"
                                                            + "<td>" + memberShipFeeAmount + "</td>"
                                                            + "</tr>";

                                                    //   totalCostAmount = Double.valueOf(perSMSCost) * Double.valueOf(messageBodyCount) * Double.valueOf(totalPhoneNumberCount);
                                                    totalDueAmount = totalDueAmount + Double.valueOf(memberShipFeeAmount);

                                                    inmf++;
                                                }

                                                transactionContainerFull = "<div class=\"row\" style=\"width: 95%; padding-left:20px\">"
                                                        + "<div class=\"table-responsive\">"
                                                        + "<table class=\"table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x\">"
                                                        + "<thead>"
                                                        + "<tr>"
                                                        + "<th style=\"width: 5%;\">#</th>"
                                                        + "<th class=\"text-center\" style=\"width:15%;\">TransID</th>"
                                                        + "<th class=\"text-center\" style=\"width:40%;\">Date</th>"
                                                        + "<th class=\"text-center\" style =\"width:15%;\">FeeYear</th>"
                                                        + "<th class=\"text-center\" style=\"width:20%;\">Amount</th>"
                                                        + "</tr>"
                                                        + "</thead>"
                                                        + " " + transactionContainer + " "
                                                        + "<tr>"
                                                        + "<td colspan=\"4\"><strong>Total Due Amount</strong></td>"
                                                        + "<td><strong>" + totalDueAmount + "</strong></td>"
                                                        + "</tr>"
                                                        + "<tbody>"
                                                        + "</tbody>"
                                                        + "</table>"
                                                        + "</div>";

                                                if (memberType.equals("1")) {
                                                    fineAmount = "1500";
                                                    totalDueWithFineAmount = totalDueAmount + Double.valueOf(fineAmount);
                                                }
                                                if (memberType.equals("2")) {
                                                    fineAmount = "1000";
                                                    totalDueWithFineAmount = totalDueAmount + Double.valueOf(fineAmount);
                                                }
                                                if (memberType.equals("3")) {
                                                    fineAmount = "500";
                                                    totalDueWithFineAmount = totalDueAmount + Double.valueOf(fineAmount);
                                                }

                                                memberPaymentInfoUrl = GlobalVariable.baseUrl + "/account/memberPaymentHistoryInfo.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
                                                memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";


                                    %>
                                    <tr id="infoBox<%=memberId%>">
                                        <td><% out.print(minPaginationIndexLimitPlus);%></td>
                                        <td><%=memberPicture%></td>
                                        <td><%=memberIEBId%></td>
                                        <td><%=fineAmount%></td>
                                        <td><%=totalDueWithFineAmount%></td>         
                                        <td style="padding: 0,10px;"><%=transactionContainerFull%></td>                                        
                                        <td>
                                            <%=mOthersInfo%><br/>

                                            <a title="<%=memberName%>  Payment History" href="<%=memberPaymentInfoUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-paypal"></i> Payment</a>
                                            <a title="<%=memberName%> Details" href="<%=memberDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i> details</a>


                                        </td>
                                    </tr>
                                    <%
                                            minPaginationIndexLimitPlus++;
                                            transactionContainer = "";
                                            transactionContainerFull = "";
                                            totalDueAmount = 0;
                                        }

                                    } else {
                                    %>
                                    <tr>
                                        <td colspan="6" class="text-center">There no data found</td>

                                    </tr>
                                    <%
                                        }
                                    %>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <%
                        //  }
                        dbsession.clear();

                        dbsession.close();
                    %>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <%=paginate%>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>