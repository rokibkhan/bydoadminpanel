<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    Query feeSQL = null;
    Object[] feeObject = null;

    String memberShipFeeTransId = "";
    String memberShipFeeMemberId = "";
    String memberShipFeePaidDate = "";
    String memberShipFeeYear = "";
    String memberShipFeeAmount = "";
    String memberShipFeeRefNo = "";
    String memberShipFeeRefDesc = "";
    String memberShipFeeTransDate = "";
    String memberShipFeeStatus = "";
    String memberShipFeeStatus1 = "";
    String memberShipFeeCurrency = "BDT";
    String memberPaymentDetailsInfo = "";
    String memberPaymentBtn = "";

    Object memberObj[] = null;
    Query memberSQL = null;
    String memberId = "";
    String memberIEBId = "";
    String memberName = "";
    String memberFatherName = "";
    String memberMotherName = "";
    String memberBirthPlace = "";
    String memberBirthDate = "";
    String memberBloodGroup = "";
    String memberCountryCode = "";
    String memberGender = "";
    String memberGender1 = "";
    String memberApplyFor = "";
    String memberApplyFor1 = "";
    String memberOldNumber = "";
    String memberEmail = "";
    String memberMobile = "";
    String pictureName = "";
    String pictureLink = "";

    String transationInfoCon = "";
    String mainInfoContainer = "";
    String btnInvInfo = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    String newsName = "";
//memberId: arg1, txnId: arg2, sessionid: arg3
    String memberSelId = request.getParameter("memberId");
    String txnId = request.getParameter("txnId");
    String sessionSelId = request.getParameter("sessionid");

  //  System.out.println("memberPaymentOptionShow :: memberId " + memberSelId);
 //   System.out.println("memberPaymentOptionShow :: txnId " + txnId);
 //   System.out.println("memberPaymentOptionShow :: sessionid " + sessionSelId);

    
    feeSQL = dbsession.createSQLQuery("select * from member_fee WHERE txn_id= '" + txnId + "'");

    
    if (!feeSQL.list().isEmpty()) {

        for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
            feeObject = (Object[]) feeItr.next();
            memberShipFeeTransId = feeObject[0].toString();
            memberShipFeeMemberId = feeObject[1].toString();
            memberShipFeeTransDate = feeObject[4].toString();
            memberShipFeePaidDate = feeObject[5] == null ? "" : feeObject[5].toString().trim();
            memberShipFeeYear = feeObject[6] == null ? "" : feeObject[6].toString().trim();
            memberShipFeeAmount = feeObject[7].toString();
            memberShipFeeRefNo = feeObject[2] == null ? "" : feeObject[2].toString().trim();
            memberShipFeeRefDesc = feeObject[3] == null ? "" : feeObject[3].toString().trim();
            memberShipFeeStatus = feeObject[8] == null ? "" : feeObject[8].toString().trim();

            if (memberShipFeeStatus.equals("1")) {
                memberShipFeeStatus1 = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
            } else if (memberShipFeeStatus.equals("0")) {

                memberShipFeeStatus1 = "<span class=\"btn btn-danger btn-sm waves-effect\">Due</span>";

            } else {
                memberShipFeeStatus1 = "<span class=\"btn btn-warning btn-sm waves-effect\">Fail</span>";
            }
        }

        transationInfoCon = "<div class=\"row\">"
                + "<div class=\"col-md-3\">TransactionID</div><div class=\"col-md-8\">" + memberShipFeeTransId + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Fee Year</div><div class=\"col-md-8\">" + memberShipFeeYear + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Date</div><div class=\"col-md-8\">" + memberShipFeeTransDate + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Bill Amount</div><div class=\"col-md-8\">" + memberShipFeeAmount + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Status</div><div class=\"col-md-8\">" + memberShipFeeStatus1 + "</div>"
                + "</div>";

        memberSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id,  "
                + " m.member_name, m.father_name, m.mother_name, "
                + "m.place_of_birth,m.dob, m.picture_name,m.blood_group, "
                + "m.country_code,m.gender,m.apply_for,m.old_member_id,m.mobile,m.email_id  "
                + "FROM  member m "
                + "WHERE m.id = '" + memberSelId + "'"
                + "ORDER BY m.id DESC");

        if (!memberSQL.list().isEmpty()) {
            for (Iterator it1 = memberSQL.list().iterator(); it1.hasNext();) {

                memberObj = (Object[]) it1.next();
                memberId = memberObj[0].toString().trim();
                memberIEBId = memberObj[1] == null ? "" : memberObj[1].toString().trim();
                memberName = memberObj[2] == null ? "" : memberObj[2].toString().trim();
                memberFatherName = memberObj[3] == null ? "" : memberObj[3].toString().trim();
                memberMotherName = memberObj[4] == null ? "" : memberObj[4].toString().trim();
                memberBirthPlace = memberObj[5] == null ? "" : memberObj[5].toString().trim();
                memberBirthDate = memberObj[6] == null ? "" : memberObj[6].toString().trim();
                memberBloodGroup = memberObj[8] == null ? "" : memberObj[8].toString().trim();
                memberCountryCode = memberObj[9] == null ? "" : memberObj[9].toString().trim();
                memberGender = memberObj[10] == null ? "" : memberObj[10].toString().trim();

                if (memberGender.equals("M")) {
                    memberGender1 = "Male";
                } else {
                    memberGender1 = "Female";
                }

                memberApplyFor = memberObj[11] == null ? "" : memberObj[11].toString().trim();
                if (memberApplyFor.equals("1")) {
                    memberApplyFor1 = "Fellow";
                }
                if (memberApplyFor.equals("2")) {
                    memberApplyFor1 = "Member";
                }
                if (memberApplyFor.equals("3")) {
                    memberApplyFor1 = "Associate Member";
                }

                memberOldNumber = memberObj[11] == null ? "" : memberObj[11].toString().trim();

                memberMobile = memberObj[13] == null ? "" : memberObj[13].toString().trim();
                memberEmail = memberObj[14] == null ? "" : memberObj[14].toString().trim();

                pictureName = memberObj[7] == null ? "" : memberObj[7].toString().trim();

                pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

            }
        }

        mainInfoContainer = "<div class=\"white-box printableArea\">"
                + "<div class=\"row\">"
                + "<div class=\"col-md-6\"><p class=\"text-center\"><strong>Member Info</strong></p></div>"
                + "<div class=\"col-md-6\"><p class=\"text-center\"><strong>Transaction Info</strong></p></div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-6\">"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Member Type</div><div class=\"col-md-8\">" + memberApplyFor1 + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">MemberID</div><div class=\"col-md-8\">" + memberIEBId + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Name</div><div class=\"col-md-8\">" + memberName + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Mobile</div><div class=\"col-md-8\">" + memberMobile + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\">Email</div><div class=\"col-md-8\">" + memberEmail + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"col-md-6\">" + transationInfoCon + "</div>"
                + "</div>"
                + "<form class=\"form-horizontal\">"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\"><p class=\"text-center\"><strong>Ref No</strong></p></div>"
                + "<div class=\"col-md-6\">"
                + "<div class=\"form-group row\">"
                + "<input type=\"text\" id=\"paymentRefNo\" name=\"paymentRefNo\" class=\"form-control\" placeholder=\"Payment ref no\" required/>"
                + "<div id=\"paymentRefNoErr\"></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"row\">"
                + "<div class=\"col-md-3\"><p class=\"text-center\"><strong>Ref Desc</strong></p></div>"
                + "<div class=\"col-md-6\">"
                + "<div class=\"form-group row\">"
                + "<input type=\"text\" id=\"paymentRefDesc\" name=\"paymentRefDesc\" class=\"form-control\" placeholder=\"Payment ref description\" required/>"
                + "<div id=\"paymentRefDescErr\"></div>"
                + "</div>"
                + "</div>"
                + "</form>";

        String argM = "'" + memberId + "','" + txnId + "','" + sessionSelId + "'";
        String btnApproveInfo = "<a id=\"memberPaymentByAdminConfirmBtn\" onclick=\"memberPaymentByAdminConfirm(" + argM + ")\" class=\"btn btn-primary\">Payment Confirm</a>";

        btnInvInfo = "<div class=\"clearfix\"></div>"
                + "<div class=\"text-right\">"
                + " " + btnApproveInfo + " "
                + "&nbsp;<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>"
                + "</div>"
                + "<br/>";

        responseCode = "1";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("mainInfoContainer", mainInfoContainer);
        json.put("btnInvInfo", btnInvInfo);
        json.put("requestId", txnId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! No Transaction info found";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("mainInfoContainer", mainInfoContainer);
        json.put("btnInvInfo", btnInvInfo);
        json.put("requestId", txnId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>