<%-- 
    Document   : Sysytem User dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(23);
        int feeTransID = Integer.parseInt(idS);

        String memberNID = request.getParameter("memberNID");
        String paymentDate = request.getParameter("paymentDate");
        String paymentFeeYear = request.getParameter("paymentFeeYear");
        String memberRenewalTypeId = request.getParameter("memberRenewalTypeId") == null ? "" : request.getParameter("memberRenewalTypeId").trim();

        String memberTypeID = request.getParameter("memberTypeID") == null ? "" : request.getParameter("memberTypeID").trim();

        String memberTypeAnnaralSubscriptionFee = request.getParameter("memberTypeAnnaralSubscriptionFee") == null ? "" : request.getParameter("memberTypeAnnaralSubscriptionFee").trim();
        String paymentRefNo = request.getParameter("paymentRefNo") == null ? "" : request.getParameter("paymentRefNo").trim();
        String paymentRefDescription = request.getParameter("paymentRefDescription") == null ? "" : request.getParameter("paymentRefDescription").trim();

        //  double value = Double.valueOf(str);
        double feeAmount = Double.valueOf(memberRenewalTypeId) * Double.valueOf(memberTypeAnnaralSubscriptionFee);

        System.out.println("feeAmount ::" + feeAmount);

        System.out.println("memberNID ::" + memberNID);

        System.out.println("paymentDate :: " + paymentDate);
        System.out.println("paymentFeeYear ::" + paymentFeeYear);
        System.out.println("memberRenewalTypeId ::" + memberRenewalTypeId);
        System.out.println("memberTypeID ::" + memberTypeID);
        System.out.println("memberTypeAnnaralSubscriptionFee ::" + memberTypeAnnaralSubscriptionFee);
        System.out.println("paymentRefNo ::" + paymentRefNo);
        System.out.println("paymentRefDescription ::" + paymentRefDescription);

        Object obj[] = null;
        Query feeCheckSQL = null;

        feeCheckSQL = dbsession.createSQLQuery("SELECT * FROM member_fee WHERE member_id = '" + memberNID + "' AND fee_year = '" + paymentFeeYear + "' AND status = 1");

        System.out.println("feeCheckSQL ::" + feeCheckSQL);

        if (feeCheckSQL.list().isEmpty()) {

            //    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
            //    Date date1 = formatter1.parse(deadline);
            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateX = new Date();
            String adddate = dateFormatX.format(dateX);

            System.out.println("adddate   adddate:: " + adddate);

            String adduser = userNameH;
            //   String addterm = InetAddress.getLocalHost().getHostName().toString();
            //    String addip = InetAddress.getLocalHost().getHostAddress().toString();

            String txn_date = paymentDate;

          //  String billType = "Renewal Fee";
            String billType = "1"; //  1=Renewal,5=Unknown, 3=Life, 4=Above65,2=Registration 

            Query memberFeeAddSQL = dbsession.createSQLQuery("INSERT INTO member_fee("
                    + "txn_id,"
                    + "member_id,"
                    + "ref_no,"
                    + "ref_description,"
                    + "txn_date,"
                    + "paid_date,"
                    + "fee_year,"
                    + "amount,"
                    + "status,"
                    + "due_date,"
                    + "bill_type,"
                    + "add_user,"
                    + "add_date,"
                    + "mod_user,"
                    + "mod_date"
                    + ") VALUES("
                    + "'" + feeTransID + "',"
                    + "'" + memberNID + "',"
                    + "'" + paymentRefNo + "',"
                    + "'" + paymentRefDescription + "',"
                    + "'" + txn_date + "',"
                    + "'" + paymentDate + "',"
                    + "'" + paymentFeeYear + "',"
                    + "'" + feeAmount + "',"
                    + "'1',"
                    + "'" + paymentDate + "',"
                    + "'" + billType + "',"
                    + "'" + adduser + "',"
                    + "'" + adddate + "',"
                    + "'" + adduser + "',"
                    + "'" + adddate + "'"
                    + "  ) ");

            memberFeeAddSQL.executeUpdate();

            dbsession.flush();
            dbtrx.commit();

            if (dbtrx.wasCommitted()) {
                strMsg = "Success !!! Payment Trnasaction info added";
                response.sendRedirect(GlobalVariable.baseUrl + "/account/memberPaymentHistoryInfo.jsp?sessionid=" + sessionIdH + "&memberId=" + memberNID + "&strMsg=" + strMsg + "");
            } else {
                dbtrx.rollback();
                strMsg = "Error!!! When Trnasaction info add";
                response.sendRedirect(GlobalVariable.baseUrl + "/account/memberNewPaymentInfoAdd.jsp?sessionid=" + sessionIdH + "&memberId=" + memberNID + "&strMsg=" + strMsg + "");
            }

        } else {
            strMsg = "Error!!! Trnasaction info already exits";
            response.sendRedirect(GlobalVariable.baseUrl + "/account/memberNewPaymentInfoAdd.jsp?sessionid=" + sessionIdH + "&memberId=" + memberNID + "&strMsg=" + strMsg + "");
        }

    } else {
        strMsg = "Error!!! When adding Trnasaction Info.Please try again";
        response.sendRedirect(GlobalVariable.baseUrl + "/account/memberPaymentAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
    }

    dbsession.clear();
    dbsession.close();
%>
