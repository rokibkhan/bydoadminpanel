<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

    %>



    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Fine Remove Data</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Account</a></li>                    
                    <li class="active">Fine</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">                    


                </div>
                <!-- /.white-box -->
            </div>

            <div class="col-md-12">
                <div class="white-box">

                    <ul>
                        <li><a href="fineCheckListRemovPreviousYearData.jsp">Remove Previous Year Fine Data</a></li>
                        <li><a href="#">Insert Current Year Fine Data</a></li>
                        <li><a href="#">Checking Data</a></li>
                        <li><a href="#">Show Fine Data</a></li>
                        <li><a href="#">Process Fine Transaction</a></li>
                    </ul>



                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
    %>


    <%@ include file="../footer.jsp" %>