
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.MemberType"%>
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">



<script type="text/javascript">

    


    function printInvoiceInfo(arg1) {
        var divToPrint = document.getElementById("divToPrint" + arg1);
        var popupWin = window.open('', '_blank', 'width=500,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }



    function showTotalMemberShipRenewalFee(arg1, arg2, arg3) {
        var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll, renewalType, renewalTypeId, memberTypeAnnaralSubscriptionFee, renewalTotalAmount;
        console.log("showTotalMemberShipRenewalFee -> arg1 :: " + arg1 + " arg2:: " + arg2);


        renewalTypeId = document.getElementById("memberRenewalTypeId").value;
        memberTypeAnnaralSubscriptionFee = document.getElementById("memberTypeAnnaralSubscriptionFee").value;

        renewalType = "Annual";

        if (renewalTypeId != '') {


            renewalTotalAmount = renewalTypeId * memberTypeAnnaralSubscriptionFee;
            document.getElementById("memberTypeTotalAmount").value = renewalTotalAmount;

            url = '<%=GlobalVariable.baseUrl%>' + '/member/memberRenewalTotalFeeShow.jsp';

//            $.post(url, {sessionId: arg1, memberId: arg2, memberTypeId: arg3, renewalType:renewalType,renewalTypeId: renewalTypeId}, function (data) {
////
//                console.log(data);
//
//                if (data[0].responseCode == 1) {
//
//                    rupantorLGModal.modal('hide');
//                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
//                    $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);
//
//                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
//                }
//                if (data[0].responseCode == 0) {
//                    rupantorLGModal.modal('hide');
//                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
//                    $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);
//                    $("#globalAlertInfoBoxConTT").show().css({"display": "block", "margin-top": "5px"});
//                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
//
//                }
//
//
//
//            }, "json");

        } else {

            document.getElementById("memberTypeTotalAmount").value = '';
        }
    }

</script>



<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    MemberType mtype = new MemberType();

    Member member = null;

    Object[] object5 = null;

    Object memberObj[] = null;
    Query memberSQL = null;
    String memberId = "";
    String memberIEBId = "";
    String memberName = "";
    String memberFatherName = "";
    String memberMotherName = "";
    String memberBirthPlace = "";
    String memberBirthDate = "";
    String memberBloodGroup = "";
    String memberCountryCode = "";
    String memberGender = "";
    String memberGender1 = "";
    String memberApplyFor = "";
    String memberApplyFor1 = "";
    String memberOldNumber = "";
    String memberEmail = "";
    String memberMobile = "";
    String pictureName = "";
    String pictureLink = "";

    int memberTypeID = 0;
    String memberTypeName = "";
    String memberTypeAnnaralSubscriptionFee = "";

    String selectedUserId = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

    System.out.println("selectedUserId :: " + selectedUserId);

    if (!selectedUserId.equals("")) {

        System.out.println("selectedUserId IN :: " + selectedUserId);

        memberSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id,  "
                + " m.member_name, m.father_name, m.mother_name, "
                + "m.place_of_birth,m.dob, m.picture_name,m.blood_group, "
                + "m.country_code,m.gender,m.apply_for,m.old_member_id,m.mobile,m.email_id  "
                + "FROM  member m "
                + "WHERE m.id = '" + selectedUserId + "'"
                + "ORDER BY m.id DESC");

        if (!memberSQL.list().isEmpty()) {
            for (Iterator it1 = memberSQL.list().iterator(); it1.hasNext();) {

                memberObj = (Object[]) it1.next();
                memberId = memberObj[0].toString().trim();
                memberIEBId = memberObj[1] == null ? "" : memberObj[1].toString().trim();
                memberName = memberObj[2] == null ? "" : memberObj[2].toString().trim();
                memberFatherName = memberObj[3] == null ? "" : memberObj[3].toString().trim();
                memberMotherName = memberObj[4] == null ? "" : memberObj[4].toString().trim();
                memberBirthPlace = memberObj[5] == null ? "" : memberObj[5].toString().trim();
                memberBirthDate = memberObj[6] == null ? "" : memberObj[6].toString().trim();
                memberBloodGroup = memberObj[8] == null ? "" : memberObj[8].toString().trim();
                memberCountryCode = memberObj[9] == null ? "" : memberObj[9].toString().trim();
                memberGender = memberObj[10] == null ? "" : memberObj[10].toString().trim();

                if (memberGender.equals("M")) {
                    memberGender1 = "Male";
                } else {
                    memberGender1 = "Female";
                }

                memberApplyFor = memberObj[11] == null ? "" : memberObj[11].toString().trim();
                if (memberApplyFor.equals("1")) {
                    memberApplyFor1 = "Fellow";
                }
                if (memberApplyFor.equals("2")) {
                    memberApplyFor1 = "Member";
                }
                if (memberApplyFor.equals("3")) {
                    memberApplyFor1 = "Associate Member";
                }

                memberOldNumber = memberObj[11] == null ? "" : memberObj[11].toString().trim();

                memberMobile = memberObj[13] == null ? "" : memberObj[13].toString().trim();
                memberEmail = memberObj[14] == null ? "" : memberObj[14].toString().trim();

                pictureName = memberObj[7] == null ? "" : memberObj[7].toString().trim();

                pictureLink = "<img width=\"90\" src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

            }

        }

        Query memberTypeSQL = dbsession.createQuery("from MemberType WHERE  member_id=" + selectedUserId + " ");

        for (Iterator memberTypeItr = memberTypeSQL.list().iterator(); memberTypeItr.hasNext();) {
            mtype = (MemberType) memberTypeItr.next();

            memberTypeID = mtype.getId();
            memberTypeName = mtype.getMemberTypeInfo().getMemberTypeName();
            memberTypeAnnaralSubscriptionFee = mtype.getMemberTypeInfo().getAnnaralSubscriptionFee().toString();

        }

    } else {

        System.out.println("selectedUserId OUt ::");

        //  response.sendRedirect(GlobalVariable.baseUrl + "/home.jsp");
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Payment</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Finance & Account</a></li>   
                    <li class="active">Add New Payment</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">

                <div class="col-md-12 col-xs-12">
                    <div class="white-box">

                        <div class="row">
                            <div class="col-md-6">

                                <div class="row">
                                    <div class="col-md-3"><strong>Name</strong></div>
                                    <div class="col-md-8"><%out.print(memberName);%></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"><strong>Father Name</strong></div>
                                    <div class="col-md-8"><%=memberFatherName%></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3"><strong>Mother Name</strong></div>
                                    <div class="col-md-8"><%=memberMotherName%></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"><strong>Gender</strong></div>
                                    <div class="col-md-8"><%=memberGender1%></div>
                                </div>                                
                            </div>
                            <div class="col-md-6">

                                <div class="row">
                                    <div class="col-md-3"><strong>Date of Birth</strong></div>
                                    <div class="col-md-8"><%=memberBirthDate%></div>
                                </div>                                 
                                <div class="row">
                                    <div class="col-md-3"><strong>Blood Group</strong></div>
                                    <div class="col-md-8"><%=memberBloodGroup%></div>
                                </div>                                
                                <div class="row">
                                    <div class="col-md-3"><strong>Place of Birth</strong></div>
                                    <div class="col-md-8"><%=memberBirthPlace%></div>
                                </div>                                
                                <div class="row">
                                    <div class="col-md-3"><strong>Country</strong></div>
                                    <div class="col-md-8"><%=memberCountryCode%></div>
                                </div>
                            </div>

                        </div>




                        <h4 class="font-bold1 m-t-10">Payment Form</h4>
                        <hr>
                        

                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">

                                    <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="memberNewPaymentInfoAddSubmitData.jsp?sessionid=<%=sessionid%>&act=add" onSubmit="return fromDataSubmitValidation()">


                                        <div class="form-group row custom-bottom-margin-5x">
                                            <!-- Date picker plugins css -->
                                            <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                                            <label for="eventTitle" class="control-label col-md-3">Date</label>
                                            <div class="col-md-6">
                                                <input type="text" id="paymentDate" name="paymentDate"  placeholder="Payment Date" class="form-control input-sm" required>
                                                <div  id="paymentDateErr" class="help-block with-errors"></div>
                                            </div>


                                            <!-- Date Picker Plugin JavaScript -->
                                            <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                                            <script type="text/javascript">
                                            $(function () {
                                                jQuery('#paymentDate').datepicker({
                                                    autoclose: true,
                                                    todayHighlight: true,
                                                    format: 'yyyy-mm-dd'
                                                });
                                            });
                                            </script>  
                                        </div>

                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label for="eventTitle" class="control-label col-md-3">Fee Year</label>
                                            <div class="col-md-6">

                                                <select name="paymentFeeYear" id="paymentFeeYear" class="form-control" required="" style="height: 35px;">
                                                    <option value="">Select Fee Year</option>
                                                    <%

                                                        String memberFeeYearId = "";
                                                        String memberFeeYearName = "";
                                                        String memberFeeYearOrder = "";

                                                        Object memberFeeYearObj[] = null;
                                                        Query memberFeeYearSQL = null;

                                                        String memberFeeYearEditUrl = "";
                                                        String agrX = "";

                                                        memberFeeYearSQL = dbsession.createSQLQuery("SELECT * FROM  member_fees_year ORDER BY ordering DESC");

                                                        if (!memberFeeYearSQL.list().isEmpty()) {
                                                            for (Iterator it1 = memberFeeYearSQL.list().iterator(); it1.hasNext();) {

                                                                memberFeeYearObj = (Object[]) it1.next();
                                                                memberFeeYearId = memberFeeYearObj[0].toString().trim();
                                                                memberFeeYearName = memberFeeYearObj[1] == null ? "" : memberFeeYearObj[1].toString().trim();
                                                                memberFeeYearOrder = memberFeeYearObj[2] == null ? "" : memberFeeYearObj[2].toString().trim();
                                                    %>

                                                    <option value="<%=memberFeeYearId%>"><%=memberFeeYearName%></option>

                                                    <%
                                                            }

                                                        }
                                                    %>

                                                </select>


                                                <div  id="paymentFeeYearErr" class="help-block with-errors"></div>
                                            </div>
                                        </div>

                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label for="memberRenewalTypeId" class="control-label col-md-3">Type</label>
                                            <div class="col-md-6">

                                                <select name="memberRenewalTypeId" id="memberRenewalTypeId" class="form-control" required="" onchange="showTotalMemberShipRenewalFee('<%=sessionIdH%>', '<%=memberId%>', '<%=memberTypeID%>')" style="height: 35px;">
                                                    <option value="">Select Renewal Type</option>
                                                    <option value="1">1 Year</option>
                                                    <option value="2">2 Year</option>
                                                    <option value="3">3 Year</option>
                                                    <option value="5">5 Year</option>
                                                    <option value="10">Life Time</option>

                                                </select>


                                                <div  id="eventTitleErr" class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label for="eventTitle" class="control-label col-md-3">Total Amount</label>
                                            <div class="col-md-6">
                                                <input name="memberTypeID" id="memberTypeID" value="<%=memberTypeID%>"  type="hidden">
                                                <input name="memberTypeAnnaralSubscriptionFee" id="memberTypeAnnaralSubscriptionFee" value="<%=memberTypeAnnaralSubscriptionFee%>"  type="hidden">
                                                <input name="memberNID" id="memberNID" value="<%=memberId%>"  type="hidden">


                                                <div class="col-8">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">TK</span>
                                                        </div>
                                                        <input name="memberTypeTotalAmount" id="memberTypeTotalAmount" type="text" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;" disabled>  
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">.00</span>
                                                        </div>
                                                    </div>                                


                                                    <div  id="memberTypeTotalAmountErr" class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label for="eventTitle" class="control-label col-md-3">Ref No.</label>
                                            <div class="col-md-6">
                                                <input type="text" id="paymentRefNo" name="paymentRefNo"  placeholder="Payment reference no" class="form-control input-sm" required>
                                                <div  id="paymentRefNoErr" class="help-block with-errors"></div>
                                            </div>
                                        </div>

                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label for="eventTitle" class="control-label col-md-3">Ref Description</label>
                                            <div class="col-md-9">
                                                <input type="text" id="paymentRefDescription" name="paymentRefDescription"  placeholder="Payment reference description" class="form-control input-sm" required>
                                                <div  id="paymentRefDescriptionErr" class="help-block with-errors"></div>
                                            </div>
                                        </div>

                                        <div class="form-group row custom-bottom-margin-5x">
                                            <label class="control-label col-sm-3"></label>
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                                <input type="hidden" id="userstat" name="userstat" value="1">
                                                <button type="submit" class="btn btn-info" id="eventSubmitBtn">Submit</button>
                                            </div>
                                        </div>
                                    </form> 
                                </div>
                            </div>

                        </div>

                        
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>