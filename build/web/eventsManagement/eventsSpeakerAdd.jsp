
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>







<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var speakerName = $("#speakerName").val();
        var speakerDesignation = $("#speakerDesignation").val();
        /*
         var syUserPassword = $("#syUserPassword").val();
         var syUserPasswordConfirm = $("#syUserPasswordConfirm").val();
         var syUserFullName = $("#syUserFullName").val();
         var syUserMobile = $("#syUserMobile").val();
         var syUserEmail = $("#syUserEmail").val();
         var syUserDept = $("#syUserDept").val();
         */
        if (speakerName == null || speakerName == "") {
            $("#speakerName").focus();
            $("#speakerNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#speakerNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (speakerDesignation == null || speakerDesignation == "") {
            $("#speakerDesignation").focus();
            $("#speakerDesignationErr").addClass("help-block with-errors").html("Designation is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#speakerDesignationErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        /*
         if (syUserPassword == null || syUserPassword == "") {
         $("#syUserPassword").focus();
         $("#syUserPasswordErr").addClass("help-block with-errors").html("Password is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserPasswordConfirm == null || syUserPasswordConfirm == "") {
         $("#syUserPasswordConfirm").focus();
         $("#syUserPasswordConfirmErr").addClass("help-block with-errors").html("Password confirm is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordConfirmErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserFullName == null || syUserFullName == "") {
         $("#syUserFullName").focus();
         $("#syUserFullNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserFullNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserMobile == null || syUserMobile == "") {
         $("#syUserMobile").focus();
         $("#syUserMobileErr").addClass("help-block with-errors").html("Mobile number is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserMobileErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserEmail == null || syUserEmail == "") {
         $("#syUserEmail").focus();
         $("#syUserEmailErr").addClass("help-block with-errors").html("Email is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserEmailErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         
         if (syUserDept == null || syUserDept == "") {
         $("#syUserDept").focus();
         $("#syUserDeptErr").addClass("help-block with-errors").html("Department is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserDeptErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         */

        return true;
    }



</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>

<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    Query eventSQL = null;
    Object eventObj[] = null;

// System.out.println(msgDispalyConT);
    String eventId = request.getParameter("eventIdX") == null ? "" : request.getParameter("eventIdX").trim();
    String eventTitle = "";
    String eventCategory = "";

    if (!eventId.equals("")) {
        eventId = eventId;

        eventSQL = dbsession.createSQLQuery("select * FROM event_info where id_event =" + eventId + "");
        for (Iterator eventItr = eventSQL.list().iterator(); eventItr.hasNext();) {
            eventObj = (Object[]) eventItr.next();

            eventCategory = eventObj[1].toString();
            eventTitle = eventObj[2].toString();
        }

    } else {
        //redirect 

        response.sendRedirect(GlobalVariable.baseUrl + "/eventsManagement/eventsList.jsp?sessionid=" + sessionIdH + "");
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Events Speaker</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Events Management</a></li>
                    <li class="active">Add Events Speaker</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <form  class="form-horizontal"  name="addrole" id="addrole" method="post" action="<%out.print(GlobalVariable.baseUrl);%>/EventsSpeaker" onSubmit="return fromDataSubmitValidation()" enctype="multipart/form-data">


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="eventTitle" class="control-label col-md-2">Event</label>
                            <div class="col-md-10">
                                <p class="form-control" style="border: none;"> <%=eventTitle%></p>
                                <input type="hidden" id="eventId" name="eventId" value="<%=eventId%>">
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="eventTitle" class="control-label col-md-2">Speaker Name</label>
                            <div class="col-md-10">
                                <input type="text" id="speakerName" name="speakerName"  placeholder="Speaker Name" class="form-control input-sm" required>
                                <div  id="speakerNameErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="eventTitle" class="control-label col-md-2">Designation</label>
                            <div class="col-md-10">
                                <input type="text" id="speakerDesignation" name="speakerDesignation"  placeholder="Speaker Designation" class="form-control input-sm" required>
                                <div  id="speakerNameErr" class="help-block with-errors"></div>
                            </div>
                        </div>


                        <link href="<%=cssLinkPmtWysihtml5%>" rel="stylesheet" type="text/css"/>  
                        <div class="form-group row">
                            <label for="eventContent" class="control-label col-md-2">Professional Bio</label>
                            <div class="col-md-10">
                                <textarea id="speakerBio" name="speakerBio" placeholder="Professional Biography " class="form-control" cols="10" rows="15" ></textarea>
                                <div id="speakerBioErr" class="help-block with-errors"></div>

                                <script src="<%=jsLinkPmtWysihtml51%>"></script>
                                <script src="<%=jsLinkPmtWysihtml52%>"></script>
                                <script type="text/javascript">
                        $(function () {
                            $("#speakerBio").wysihtml5();

                        });
                                </script> 
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="speakerShortBio" class="control-label col-md-2">Short Professional Bio</label>
                            <div class="col-md-10">
                                <textarea onkeyup="countShortDescriptionChars(this, '300', 'speakerShortBio', 'speakerShortBioErr');" id="speakerShortBio" name="speakerShortBio" placeholder="Short Professional Biography Maximum 300 character" class="form-control" cols="4" rows="4"></textarea>
                                <div id="speakerShortBioErr" class="help-block with-errors"></div> 
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="eventTitle" class="control-label col-md-2">Speaker Picture</label>
                            <div class="col-md-10">
                                <div class="input-file-container">  
                                    <input onchange="uplaodFileTypeJPGPNGAndSizeCheck('speakerPicture', '2', 'speakerPictureErr', 'speakerSubmitBtn');" name="speakerPicture" id="speakerPicture" accept="image/*" type="file" class="input-file" >
                                    <label tabindex="0" for="my-file" class="input-file-trigger">Select jpg or png file and file size Maximum 2M</label>
                                </div>
                                <div id="speakerPictureErr" class="help-block with-errors" style="color: red;"></div> 
                            </div>


                        </div>

                        <div class="form-group">
                            <label for="eventTitle" class="control-label col-md-2">Presentation</label>
                            <div class="col-md-10">
                                <div class="input-file-container">  
                                    <input onchange="uplaodFileTypePPTXAndSizeCheck('file', '50', 'fileErr', 'speakerSubmitBtn');" name="file" id="file" type="file" class="input-file" >
                                    <label tabindex="0" for="my-file" class="input-file-trigger">Select pptx file and file size Maximum 50M</label>
                                </div>
                                <div id="fileErr" class="help-block with-errors" style="color: red;"></div>
                            </div>


                        </div>





                        <div class="form-group row">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" class="btn btn-info" id="speakerSubmitBtn">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->


    <%
        dbsession.flush();
        dbsession.close();
    %>


    <%@ include file="../footer.jsp" %>