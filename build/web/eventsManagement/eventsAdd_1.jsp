
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>







<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var eventTitle = $("#eventTitle").val();
        var eventCategoryId = $("#eventCategoryId").val();
        /*
         var syUserPassword = $("#syUserPassword").val();
         var syUserPasswordConfirm = $("#syUserPasswordConfirm").val();
         var syUserFullName = $("#syUserFullName").val();
         var syUserMobile = $("#syUserMobile").val();
         var syUserEmail = $("#syUserEmail").val();
         var syUserDept = $("#syUserDept").val();
         */
        if (eventTitle == null || eventTitle == "") {
            $("#eventTitle").focus();
            $("#eventTitleErr").addClass("help-block with-errors").html("Event title is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#eventTitleErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (eventCategoryId == null || eventCategoryId == "") {
            $("#eventCategoryId").focus();
            $("#eventCategoryIdErr").addClass("help-block with-errors").html("Event Category is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#eventCategoryIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        /*
         if (syUserPassword == null || syUserPassword == "") {
         $("#syUserPassword").focus();
         $("#syUserPasswordErr").addClass("help-block with-errors").html("Password is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserPasswordConfirm == null || syUserPasswordConfirm == "") {
         $("#syUserPasswordConfirm").focus();
         $("#syUserPasswordConfirmErr").addClass("help-block with-errors").html("Password confirm is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordConfirmErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserFullName == null || syUserFullName == "") {
         $("#syUserFullName").focus();
         $("#syUserFullNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserFullNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserMobile == null || syUserMobile == "") {
         $("#syUserMobile").focus();
         $("#syUserMobileErr").addClass("help-block with-errors").html("Mobile number is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserMobileErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserEmail == null || syUserEmail == "") {
         $("#syUserEmail").focus();
         $("#syUserEmailErr").addClass("help-block with-errors").html("Email is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserEmailErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         
         if (syUserDept == null || syUserDept == "") {
         $("#syUserDept").focus();
         $("#syUserDeptErr").addClass("help-block with-errors").html("Department is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserDeptErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         */

        return true;
    }



</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>

<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query eventCategorySQL = null;
    Object eventCategoryObj[] = null;

    String eventCategoryId = "";
    String eventCategoryName = "";
    String eventCategoryOptionCon = "";

    eventCategorySQL = dbsession.createSQLQuery("select * FROM event_category ORDER BY category_name ASC");

    for (Iterator eventCategoryItr = eventCategorySQL.list().iterator(); eventCategoryItr.hasNext();) {
        eventCategoryObj = (Object[]) eventCategoryItr.next();
        eventCategoryId = eventCategoryObj[0].toString();
        eventCategoryName = eventCategoryObj[1].toString();

        eventCategoryOptionCon = eventCategoryOptionCon + "<option value=\"" + eventCategoryId + "\">" + eventCategoryName + "</option>";
    }


%>


<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Events</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Events Management</a></li>
                    <li class="active">Add Events</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <form  class="form-horizontal"  name="addrole" id="addrole" method="post" action="<%out.print(GlobalVariable.baseUrl);%>/Events" onSubmit="return fromDataSubmitValidation()" enctype="multipart/form-data">


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="eventTitle" class="control-label col-md-2">Title</label>
                            <div class="col-md-10">
                                <input type="text" id="eventTitle" name="eventTitle"  placeholder="Events Title" class="form-control input-sm" required>
                                <div  id="eventTitleErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="eventTitle" class="control-label col-md-2">Duration</label>
                            <div class="col-md-10">
                                <input type="text" id="eventDuration" name="eventDuration"  placeholder="Events Duration" class="form-control input-sm" required>
                                <div  id="eventTitleErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="eventTitle" class="control-label col-md-2">Venue</label>
                            <div class="col-md-10">
                                <input type="text" id="eventVenue" name="eventVenue"  placeholder="Events Venue" class="form-control input-sm" required>
                                <div  id="eventTitleErr" class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row custom-bottom-margin-5x">
                            <!-- Date picker plugins css -->
                            <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                            <label for="eventTitle" class="control-label col-md-2">Date</label>
                            <div class="col-md-4">
                                <input type="text" id="eventDate" name="eventDate"  placeholder="Events Date" class="form-control input-sm" required>
                                <div  id="eventTitleErr" class="help-block with-errors"></div>
                            </div>


                            <!-- Date Picker Plugin JavaScript -->
                            <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                            <script type="text/javascript">
                        $(function () {
                            jQuery('#eventDate').datepicker({
                                autoclose: true,
                                todayHighlight: true,
                                format: 'yyyy-mm-dd'
                            });
                        });
                            </script>   

                            <label for="eventTitle" class="control-label col-md-2">Categoty</label>
                            <div class="col-md-4">                               

                                <select  name="eventCategoryId" id="eventCategoryId" class="form-control input-sm customInput-sm" required>
                                    <option value="">Select Event Categoty </option>
                                    <%=eventCategoryOptionCon%>
                                </select>
                                <div  id="eventCategoryIdErr" class="help-block with-errors"></div>
                            </div>
                        </div>




                        <link href="<%=cssLinkPmtWysihtml5%>" rel="stylesheet" type="text/css"/>  
                        <div class="form-group row">
                            <label for="eventContent" class="control-label col-md-2">Description</label>
                            <div class="col-md-10">
                                <textarea id="eventDesc" name="eventDesc" placeholder="Events Content" class="form-control" cols="10" rows="15" ></textarea>
                                <div id="eventDescErr" class="help-block with-errors"></div>

                                <script src="<%=jsLinkPmtWysihtml51%>"></script>
                                <script src="<%=jsLinkPmtWysihtml52%>"></script>
                                <script type="text/javascript">
                        $(function () {
                            $("#eventDesc").wysihtml5();

                        });
                                </script> 
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="eventShortDesc" class="control-label col-md-2">Short Description</label>
                            <div class="col-md-10">
                                <textarea onkeyup="countShortDescriptionChars(this, '300', 'eventShortDesc', 'eventShortDescErr');" id="eventShortDesc" name="eventShortDesc" placeholder="Short Description Maximum 300 character" class="form-control" cols="4" rows="4"></textarea>
                                <div id="eventShortDescErr" class="help-block with-errors"></div> 
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="eventShortDesc" class="control-label col-md-2"></label>
                            <div class="col-md-10">
                                <div class="input-file-container">  
                                    <input name="file" id="file" accept="image/*" type="file" class="input-file" onchange="uplaodFileTypeJPGPNGAndSizeCheck('file', '5', 'fileErr', 'eventSubmitBtn');" >
                                    <label tabindex="0" for="my-file" class="input-file-trigger">Select a photo...</label>
                                </div>
                                <div id="fileErr" class="help-block with-errors" style="color: red;"></div> 
                            </div>

                        </div>






                        <div class="form-group row">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" class="btn btn-info" id="eventSubmitBtn">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->


    <%
        dbsession.flush();
        dbsession.close();
        %>


    <%@ include file="../footer.jsp" %>