

<%@page import="com.appul.entity.EventCenter"%>
<%@page import="com.appul.entity.EventDivision"%>
<%@page import="com.appul.entity.AlbumEvent"%>
<%@page import="com.appul.entity.EventSlug"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){




        var modifyFlag = false;


        function eventDeleteInfo(arg1, arg2) {

            console.log("eventDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("News Delete confirmation");
            btnInfo = "<a onclick=\"eventDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            if (modifyFlag === false) {
                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());
                modifyFlag = true;
            }

//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }

        function eventEditInfo(arg1, arg2) {
            console.log("eventEdit: " + arg1 + "-" + arg2);
        }



        function eventDeleteInfoConfirm(arg1, arg2) {
            console.log("eventDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');

//            $.post(
//                    "eventDeleteProcess.jsp", 
//                    {userId: arg1, sessionid: arg2},
//                    function(data){
//                
//                    }
//          );

            $.post("eventDeleteProcess.jsp", {eventId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=\"7\" style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }
        
        
        
        
        
        function changeEventCategoryInfo(arg1, arg2, arg3) {
            console.log("changeEventCategoryInfo :: " + arg1);
            var taskLGModal, btnConfirmInfo, url;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"changeEventCategoryInfoConfirmBtn\" onclick=\"changeEventCategoryInfoConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Change Category for Event");

            url = '<%=GlobalVariable.baseUrl%>' + "/eventsManagement/eventCategoryInfoShow.jsp";

            console.log("url :: " + url);

            $.post(url, {eventId: arg1, eventName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function changeEventCategoryInfoConfirm(arg1, arg2, arg3) {
            console.log("changeEventCategoryInfoConfirm :: " + arg1);
            var taskLGModal, url, eventCategoryId;

            taskLGModal = $('#taskLGModal');

            eventCategoryId = $("#eventCategoryId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (eventCategoryId == null || eventCategoryId == "") {
                $("#eventCategoryIdId").focus();
                $("#eventCategoryIdErr").addClass("help-block with-errors").html("Category is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#eventCategoryIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            url = '<%=GlobalVariable.baseUrl%>' + "/eventsManagement/eventCategoryInfoChangeConfirm.jsp";

            console.log("url :: " + url);


            $.post(url, {eventId: arg1, eventName: arg2, sessionid: arg3, eventCategoryId: eventCategoryId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#eventCatBox" + data[0].requestId).html(data[0].eventCategoryInfoContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#eventCategoryIdErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }



        function addCenterForEvent(arg1, arg2, arg3) {
            console.log("addCenterForEvent :: " + arg1);
            var taskLGModal, btnConfirmInfo, url;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addCenterForEventConfirmBtn\" onclick=\"addCenterForEventConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Center for Event");

            url = '<%=GlobalVariable.baseUrl%>' + "/eventsManagement/eventCenterInfoShow.jsp";

            console.log("url :: " + url);

            $.post(url, {eventId: arg1, eventName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addCenterForEventConfirm(arg1, arg2, arg3) {
            console.log("addCenterForEventConfirm :: " + arg1);
            var taskLGModal, url, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Center is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            url = '<%=GlobalVariable.baseUrl%>' + "/eventsManagement/eventCenterInfoAddConfirm.jsp";

            console.log("url :: " + url);


            $.post(url, {eventId: arg1, eventName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }



        function addDivisionForEvent(arg1, arg2, arg3) {
            console.log("addDivisionForEvent :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addDivisionForEventConfirmBtn\" onclick=\"addDivisionForEventConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Division for Event");

            $.post("eventDivisionInfoShow.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addDivisionForEventConfirm(arg1, arg2, arg3) {
            console.log("addDivisionForEventConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Division is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("eventDivisionInfoAddConfirm.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }


        function addAlbumForEvent(arg1, arg2, arg3) {
            console.log("addAlbumForEvent :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addAlbumForEventConfirmBtn\" onclick=\"addAlbumForEventConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Album for Event");

            $.post("eventAlbumInfoShow.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addAlbumForEventConfirm(arg1, arg2, arg3) {
            console.log("addAlbumForEventConfirm :: " + arg1);
            var taskLGModal, centerAlbumTagId;

            taskLGModal = $('#taskLGModal');

            centerAlbumTagId = $("#centerAlbumTagId").val();
            //   alert("###" + centerAlbumTagId);
            //   document.getElementById(arg1)

            if (centerAlbumTagId == null || centerAlbumTagId == "") {
                $("#centerAlbumTagId").focus();
                $("#centerAlbumTagIdErr").addClass("help-block with-errors").html("Album is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerAlbumTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("eventAlbumInfoAddConfirm.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3, centerAlbumTagId: centerAlbumTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerAlbumTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }


        function addNewsForEvent(arg1, arg2, arg3) {
            console.log("addNewsForEvent :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"addNewsForEventConfirmBtn\" onclick=\"addNewsForEventConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            rupantorLGModal.find("#rupantorLGModalTitle").text("Add News for Event");

            $.post("eventNewsInfoShow.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   rupantorLGModal.modal('hide');


                    rupantorLGModal.find("#rupantorLGModalBody").html(data[0].mainInfoContainer);
                    rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

                    rupantorLGModal.modal('show');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addNewsForEventConfirm(arg1, arg2, arg3) {
            console.log("addNewsForEventConfirm :: " + arg1);
            var rupantorLGModal, centerDivisionTagId;

            rupantorLGModal = $('#rupantorLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("News is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("eventNewsInfoAddConfirm.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    rupantorLGModal.modal('hide');
                } else {
                    // rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }




        function addVideoForEvent(arg1, arg2, arg3) {
            console.log("addVideoForEvent :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addVideoForEventConfirmBtn\" onclick=\"addVideoForEventConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Video for Event");

            $.post("eventVideoInfoShow.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addVideoForEventConfirm(arg1, arg2, arg3) {
            console.log("addVideoForEventConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Video is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("eventVideoInfoAddConfirm.jsp", {eventId: arg1, eventName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }


    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Events List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Events Management</a></li>                    
                    <li class="active">Events</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 3%;">#</th>
                                    <th class="text-center" style="width: 30%;">Title</th>
                                    <th class="text-center" style="width: 10%;">Category</th>
                                    <th class="text-center" style="width: 15%;">Date & Duration  & Vanue</th>
                                    <th class="text-center" style="width: 15%;">Featured Images</th>
                                    <th class="text-center">Assign Event</th>
                                    <th class="text-center" style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String eventId = "";
                                    String eventTitle = "";
                                    String eventCategoryId = "";
                                    String eventCategoryName = "";
                                    String btnKeyNotesSpeaker = "";
                                    String eventSlug = "";
                                    String eventShortDetails = "";
                                    String eventDetails = "";
                                    String eventDateTime = "";
                                    String eventDuration = "";
                                    String eventVenue = "";
                                    String featureImage = "";
                                    String featureImage1 = "";
                                    String featureImageUrl = "";
                                    String eventDurationVanue = "";
                                    String btnColorClass = "";
                                    String levelStatusColorClass = "";

                                    String updateProfileUrl = "";
                                    String updateChangePassUrl = "";

                                    String agrX = "";
                                    String agrXcenter = "";
                                    String btnAddCenter = "";
                                    String btnAddSubCenter = "";
                                    String btnAddDivision = "";
                                    String btnAddAlbum = "";
                                    String btnAddNews = "";
                                    String btnAddTag = "";
                                    String btnAddVideo = "";

                                    String centerDivisionTagContainer = "";

                                    String centerListContainer = "";
                                    EventCenter alc = null;
                                    String centerListCenterName = "";
                                    Query centerListSQL = null;

                                    String divisionListContainer = "";
                                    EventDivision ald = null;
                                    String divisionListCenterName = "";
                                    Query divisionListSQL = null;

                                    String albumListContainer = "";
                                    AlbumEvent ale = null;
                                    String albumListCenterName = "";
                                    Query albumListSQL = null;

                                    String tagListContainer = "";
                                    EventSlug alt = null;
                                    String tagListCenterName = "";
                                    Query tagListSQL = null;

                                    Query usrSQL = dbsession.createSQLQuery("SELECT ni.id_event, ni.event_title, ni.event_short_desc, ni.event_desc, "
                                            + "ni.event_date, ni.event_duration ,ni.event_venue, "
                                            + "ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4,ni.event_category,cat.category_name "
                                            + "FROM  event_info ni,event_category cat "
                                            + "WHERE ni.event_category = cat.id_cat "
                                            + "ORDER BY ni.id_event DESC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            eventId = obj[0].toString().trim();
                                            eventTitle = obj[1].toString().trim();
                                            eventShortDetails = obj[2].toString().trim();
                                            eventDetails = obj[3].toString().trim();

                                            eventDateTime = obj[4] == null ? "" : obj[4].toString().trim();

                                            eventDuration = obj[5].toString().trim();
                                            eventVenue = obj[6].toString().trim();

                                            eventDurationVanue = eventDateTime + " <br> " + eventDuration + " <br> " + eventVenue;

                                            featureImage = obj[7] == null ? "" : obj[7].toString().trim();
                                            featureImageUrl = GlobalVariable.imageNewsDirLink + featureImage;

                                            featureImage1 = "<img width=\"200\" src=\"" + featureImageUrl + "\" alt=\"" + eventTitle + "\">";

                                            eventCategoryId = obj[12].toString().trim();
                                            eventCategoryName = obj[13].toString().trim();

                                            //       String updateProfileUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=profile";
                                            //      String updateChangePassUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=changePass";
                                            
                                            
                                          
                                            agrX = "'" + eventId + "','" + session.getId() + "'";

                                            //    agrXcenter = "'" + eventId + "','" + eventTitle + "','" + session.getId() + "'";
                                            agrXcenter = "'" + eventId + "','','" + session.getId() + "'";

                                            if (eventCategoryId.equals("1")) {
                                                btnKeyNotesSpeaker = "<br/><a href=\"" + GlobalVariable.baseUrl + "/eventsManagement/eventsSpeakerAdd.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "\" title=\"Add Speaker Info for this Event\"  class=\"btn btn-primary btn-xs  m-b-5\"><i class=\"fa fa-plus\"></i> Add Speaker</a><a href=\"" + GlobalVariable.baseUrl + "/eventsManagement/eventsSpeakerList.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "\" title=\"Speaker Info for this Event\"  class=\"btn btn-warning btn-xs  m-b-5\"><i class=\"icon-info\"></i> Speaker Info</a>"
                                                        + "<br><a onClick=\"changeEventCategoryInfo(" + agrXcenter + ");\" title=\"Change Category for this Event\"  class=\"btn btn-danger btn-xs\">Change Category</a>";
                                            } else {
                                                btnKeyNotesSpeaker = "<a onClick=\"changeEventCategoryInfo(" + agrXcenter + ");\" title=\"Change Category for this Event\"  class=\"btn btn-danger btn-xs  m-b-5\">Change Category</a>";
                                            }

                                            btnAddCenter = "<a onClick=\"addCenterForEvent(" + agrXcenter + ");\" title=\"Add Center  for this Event\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Center</a>";
                                            //     btnAddSubCenter = "<a onClick=\"addSubCenterForEvent(" + agrXcenter + ");\" title=\"Add SubCenter  for this Event\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Sub Center</a>";

                                            btnAddDivision = "<a onClick=\"addDivisionForEvent(" + agrXcenter + ");\" title=\"Add Division  for this Event\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Division</a>";

                                            btnAddAlbum = "<a onClick=\"addAlbumForEvent(" + agrXcenter + ");\" title=\"Add Album  for this Event\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Album</a>";

                                            btnAddNews = "<a onClick=\"addNewsForEvent(" + agrXcenter + ");\" title=\"Add News  for this Event\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add News</a>";

                                            btnAddVideo = "<a onClick=\"addVideoForEvent(" + agrXcenter + ");\" title=\"Add Videos  for this Event\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Videos</a>";

                                            // show event Assign center,division,tag
                                            centerListContainer = "";
                                            centerListSQL = dbsession.createQuery(" FROM EventCenter where eventInfo = '" + eventId + "'");
                                            if (!centerListSQL.list().isEmpty()) {
                                                for (Iterator centerListItr = centerListSQL.list().iterator(); centerListItr.hasNext();) {
                                                    alc = (EventCenter) centerListItr.next();
                                                    centerListCenterName = alc.getCenter().getCenterName();

                                                    centerListContainer = centerListContainer + centerListCenterName + ",";
                                                }
                                            } else {
                                                centerListContainer = "";
                                            }

                                            divisionListContainer = "";

                                            divisionListSQL = dbsession.createQuery(" FROM EventDivision where eventInfo = '" + eventId + "'");
                                            if (!divisionListSQL.list().isEmpty()) {
                                                for (Iterator divisionListItr = divisionListSQL.list().iterator(); divisionListItr.hasNext();) {
                                                    ald = (EventDivision) divisionListItr.next();
                                                    divisionListCenterName = ald.getMemberDivision().getMemDivisionName();

                                                    divisionListContainer = divisionListContainer + divisionListCenterName + ",";
                                                }
                                            } else {
                                                divisionListContainer = "";
                                            }

                                            albumListContainer = "";

                                            albumListSQL = dbsession.createQuery(" FROM AlbumEvent where eventInfo = '" + eventId + "'");
                                            if (!albumListSQL.list().isEmpty()) {
                                                for (Iterator albumListItr = albumListSQL.list().iterator(); albumListItr.hasNext();) {
                                                    ale = (AlbumEvent) albumListItr.next();
                                                    albumListCenterName = ale.getPictureCategory().getCategoryName();

                                                    albumListContainer = albumListContainer + albumListCenterName + ",";
                                                }
                                            } else {
                                                albumListContainer = "";
                                            }

//            tagListSQL = dbsession.createQuery(" FROM AlbumSlug where pictureCategory = '" + eventId + "'");
//            if (!tagListSQL.list().isEmpty()) {
//                for (Iterator tagListItr = tagListSQL.list().iterator(); tagListItr.hasNext();) {
//                    alt = (AlbumSlug) tagListItr.next();
//                    tagListCenterName = alt.get
//
//                    tagListContainer = tagListContainer + tagListCenterName + ",";
//                }
//            } else {
//                tagListContainer = "";
//            }
                                            centerDivisionTagContainer = centerListContainer + divisionListContainer + albumListContainer;

                                %>


                                <tr id="infoBox<%=eventId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(eventTitle);%></td>
                                    <td class="text-center" id="eventCatBox<%=eventId%>"><strong><%=eventCategoryName%></strong><br/><br/><%=btnKeyNotesSpeaker%></td>
                                    <td><% out.println(eventDurationVanue); %></td>
                                    <td><% out.println(featureImage1);%></td>
                                    <td class="text-center" id="assignBox<%=eventId%>">

                                        <%=centerDivisionTagContainer%>

                                    </td>
                                    <td class="text-left" style="padding-left: 10px;">
                                        <%=btnAddCenter%>
                                        <%=btnAddSubCenter%>
                                        <%=btnAddDivision%>
                                        <%=btnAddAlbum%>
                                        <%=btnAddNews%>
                                        <%=btnAddVideo%>
                                        <a href="eventDetails.jsp?sessionid=<%=sessionid%>&eventid=<%=eventId%>" title="Event details" class="btn btn-primary btn-sm"><i class="icon-info"></i></a>
                                        <a onclick="eventEditInfo(<% out.print("'" + eventId + "','" + sessionid + "'");%>)" href="eventEdit.jsp?sessionid=<%=sessionid%>&eventid=<%=eventId%>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onclick="eventDeleteInfo(<%=agrX%>)" title="Delete this event" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>