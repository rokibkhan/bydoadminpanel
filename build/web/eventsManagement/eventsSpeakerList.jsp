
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>







<script>

    function eventsSpeakerDeleteInfo(arg1, arg2) {

        console.log("JournalDeleteInfo::: " + arg1);

        var rupantorLGModal, btnInfo, arg;

        arg = "'" + arg1 + "','" + arg2 + "'";
        rupantorLGModal = $('#rupantorLGModal');
        rupantorLGModal.find("#rupantorLGModalTitle").text("Speaker Delete confirmation");
        btnInfo = "<a onclick=\"eventsSpeakerDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary\">Confirm</a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
        rupantorLGModal.find("#rupantorLGModalBody").html('');

        //   rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());

        rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo);


//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
        rupantorLGModal.modal('show');

    }


    function eventsSpeakerDeleteInfoConfirm(arg1, arg2) {
        console.log("JournalDeleteInfoConfirm:: " + arg1);
        var rupantorLGModal;
        rupantorLGModal = $('#rupantorLGModal');



        $.post("eventsSpeakerDeleteProcess.jsp", {eventsSpeakerId: arg1, sessionid: arg2}, function (data) {



            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=\"6\" style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                rupantorLGModal.modal('hide');
            } else {

            }


        }, "json");

    }



</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>

<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

%>


<%    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    Query eventSQL = null;
    Object eventObj[] = null;

// System.out.println(msgDispalyConT);
    String eventId = request.getParameter("eventIdX") == null ? "" : request.getParameter("eventIdX").trim();
    String eventTitle = "";
    String eventDate = "";
    String eventDuration = "";
    String eventVenue = "";
    String eventCategory = "";
    String eventSpeakerBtn = "";

    if (!eventId.equals("")) {
        eventId = eventId;

        eventSQL = dbsession.createSQLQuery("select * FROM event_info where id_event =" + eventId + "");
        for (Iterator eventItr = eventSQL.list().iterator(); eventItr.hasNext();) {
            eventObj = (Object[]) eventItr.next();

            eventTitle = eventObj[2].toString();
            eventDate = eventObj[5].toString();
            eventVenue = eventObj[9].toString();
            eventDuration = eventObj[8].toString();
            eventCategory = eventObj[1].toString();

            if (eventCategory.equals("1")) {
                eventSpeakerBtn = "<a title=\"Add this events speaker\"  class=\"btn btn-primary\" href=\"" + GlobalVariable.baseUrl + "/eventsManagement/eventsSpeakerAdd.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "\"><i class=\"fa fa-plus\"></i> Add Event Speaker</a>";
            }
        }

    } else {
        //redirect 

        response.sendRedirect(GlobalVariable.baseUrl + "/eventsManagement/eventsList.jsp?sessionid=" + sessionIdH + "");
    }
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Events Speaker Info</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Events Management</a></li>
                    <li class="active">Events Speaker Info</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title1 m-b-0">Event: <%=eventTitle%></h3>
                    <h3 class="box-title1 m-b-0">Date: <%=eventDate%></h3>
                    <h3 class="box-title1 m-b-0">Duration: <%=eventDuration%></h3>
                    <h3 class="box-title1 m-b-0">Venue: <%=eventVenue%></h3>
                    <h3><%=eventSpeakerBtn%></h3>
                </div>
            </div>
        </div>
        <!-- .row -->
        <div class="row">

            <%
                Object speakerObj[] = null;
                String speakerId = "";
                String speakerName = "";
                String speakerDesignation = "";
                String speakerBio = "";
                String speakerShortBio = "";
                String speakerPicture = "";
                String speakerPictureUrl = "";
                String speakerPresentation = "";
                String speakerPresentationUrl = "";
                String speakerEditBrn = "";
                String speakerDeleteBrn = "";
                String speakerPresentationDownloadBrn = "";
                String argXn = "";

                Query speakerSQL = dbsession.createSQLQuery("SELECT * FROM  speaker_info s WHERE s.id_event = '" + eventId + "' ORDER BY s.id_speaker ASC");

                if (!speakerSQL.list().isEmpty()) {
                    for (Iterator speakerItr = speakerSQL.list().iterator(); speakerItr.hasNext();) {

                        speakerObj = (Object[]) speakerItr.next();
                        speakerId = speakerObj[0].toString().trim();
                        speakerName = speakerObj[2].toString().trim();
                        speakerDesignation = speakerObj[3].toString().trim();

                        speakerShortBio = speakerObj[4] == null ? "" : speakerObj[4].toString().trim();
                        speakerBio = speakerObj[5].toString().trim();

                        speakerPicture = speakerObj[6] == null ? "" : speakerObj[6].toString().trim();
                        speakerPictureUrl = GlobalVariable.seminarPPTXDirLink + speakerPicture;

                        speakerPresentation = speakerObj[7] == null ? "" : speakerObj[7].toString().trim();
                        speakerPresentationUrl = GlobalVariable.seminarPPTXDirLink + speakerPresentation;

                        speakerPresentationDownloadBrn = "<a title=\"Download presentation\"  class=\"btn btn-primary\" href=\"" + speakerPresentationUrl + "\"><i class=\"icon-info\"></i> presentation download</a>";

                        argXn = "'" + speakerId + "','" + session.getId() + "'";

                       // speakerEditBrn = "<a title=\"Edit\" href=\"" + GlobalVariable.baseUrl + "/eventsManagement/eventsSpeakerUpdate.php?sessionid=" + session.getId() + "&speakerIdX=" + speakerId + "\" class=\"btn btn-warning\"><i class=\"fa fa-pencil-square-o\"></i> Edit</a>";
                        speakerDeleteBrn = "<a onClick=\"eventsSpeakerDeleteInfo(" + argXn + ");\" title=\"Delete speaker for this event\"  class=\"btn btn-danger\"><i class=\"fa fa-trash-o\"></i> Delete</a>";


            %>

            <!-- .col -->
            <div class="col-md-6 col-sm-6">
                <div class="white-box" id="infoBox<%=speakerId%>" style="min-height: 260px; padding: 5px;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                            <a href="#"><img src="<%=speakerPictureUrl%>" alt="<%=speakerName%>" class="img-circle img-responsive"></a>
                            <h3 class="box-title m-b-0"><%=speakerName%></h3> 
                            <h5 class="box-title1 m-b-0" style="line-height1: 20px;"><%=speakerDesignation%></h5> 




                        </div>
                        <div class="col-md-8 col-sm-8">
                            <p><strong>Short Bio:</strong></p>
                            <p><%=speakerShortBio%></p>
                            <p><strong>Details Bio:</strong></p>
                            <p><%=speakerBio%></p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center">
                            <%=speakerPresentationDownloadBrn%>
                            <%=speakerEditBrn%>
                            <%=speakerDeleteBrn%>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->

            <%
                    }
                }

            %>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->


    <%        dbsession.flush();
        dbsession.close();
    %>


    <%@ include file="../footer.jsp" %>