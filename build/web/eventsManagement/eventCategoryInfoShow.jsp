<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;

    Query eventCategorySQL = null;
    Object eventCategoryObj[] = null;

    String eventCategoryId = "";
    String eventCategoryName = "";
    String eventCategoryOptionCon = "";
    String eventCategoryInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    String eventName = "";
    String categoryName = "";
    String categoryId = "";

    String eventId = request.getParameter("eventId");

    System.out.println("eventCategoryInfoShow :: eventId " + eventId);

    q1 = dbsession.createSQLQuery("SELECT ei.id_event, ei.event_title,ei.event_category,cat.category_name FROM event_info ei,event_category cat WHERE ei.event_category = cat.id_cat AND ei.id_event =" + eventId + "");
    
    System.out.println("eventCategoryInfoShow :: q1 " + q1);
    
    if (!q1.list().isEmpty()) {

        for (Iterator nameItr = q1.list().iterator(); nameItr.hasNext();) {
            obj = (Object[]) nameItr.next();
            eventName = obj[1].toString();
            categoryId = obj[2].toString();
            categoryName = obj[3].toString();
        }

        eventCategorySQL = dbsession.createSQLQuery("select * FROM event_category WHERE id_cat !='" + categoryId + "' ORDER BY category_name ASC");

        for (Iterator eventCategoryItr = eventCategorySQL.list().iterator(); eventCategoryItr.hasNext();) {
            eventCategoryObj = (Object[]) eventCategoryItr.next();
            eventCategoryId = eventCategoryObj[0].toString();
            eventCategoryName = eventCategoryObj[1].toString();

            eventCategoryOptionCon = eventCategoryOptionCon + "<option value=\"" + eventCategoryId + "\">" + eventCategoryName + "</option>";
        }

        eventCategoryInfoCon = "<select  name=\"eventCategoryId\" id=\"eventCategoryId\" class=\"form-control input-sm customInput-sm\" required>"
                + "<option value=\"\" >Select Event Category </option>"
                + "" + eventCategoryOptionCon + ""
                + "</select>";

        mainInfoContainer = "<div class=\"row\">"
                + "<div class=\"col-md-11 offset-md-1\" id=\"centerDivisionTagAllErr\"></div>"
                + "<div class=\"col-md-11 offset-md-1\">"
                + "<div class=\"form-group row custom-bottom-margin-5x\">"
                + "<label class=\"control-label col-md-3\">Event:</label>"
                + "<div class=\"col-md-9\">"
                + " " + eventName + " "
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row custom-bottom-margin-5x\">"
                + "<label class=\"control-label col-md-3\">Old Cetegory:</label>"
                + "<div class=\"col-md-9\">"
                + " " + categoryName + " "
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row custom-bottom-margin-5x\">"
                + "<label class=\"control-label col-md-3\">New Category</label>"
                + "<div class=\"col-md-6\">"
                + " " + eventCategoryInfoCon + " "
                + "<div  id=\"eventCategoryIdErr\" class=\"help-block with-errors\"></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "</div>";

        responseCode = "1";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("mainInfoContainer", mainInfoContainer);
        json.put("requestId", eventId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! No event info found";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", eventId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>