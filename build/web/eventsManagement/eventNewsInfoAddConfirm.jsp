<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String eventId = request.getParameter("eventId");
    String eventName = request.getParameter("eventName");
    String centerDivisionTagId = request.getParameter("centerDivisionTagId");

    System.out.println("eventNewsInfoAddConfirm :: eventId " + eventId);
    System.out.println("eventNewsInfoAddConfirm :: eventName " + eventName);
    System.out.println("eventNewsInfoAddConfirm :: centerDivisionTagId " + centerDivisionTagId);

    q1 = dbsession.createSQLQuery("SELECT * FROM event_news WHERE EVENT_ID=" + eventId + " AND NEW_ID =" + centerDivisionTagId + "");
    System.out.println("eventNewsInfoAddConfirm :: q1 " + q1);
    if (!q1.list().isEmpty()) {

        responseCode = "0";
        responseMsg = "Error!!! Already event assign this news";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", eventId);
        jsonArr.add(json);
    } else {

        //insert event id and center id in event_center table
        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(51);
        int eventCenterId = Integer.parseInt(idS);

        Query eventCenterAddSQL = dbsession.createSQLQuery("INSERT INTO event_news("
                + "ID,"
                + "EVENT_ID,"
                + "NEW_ID"
                + ") VALUES("
                + "'" + eventCenterId + "',"
                + "'" + eventId + "',"
                + "'" + centerDivisionTagId + "'"
                + "  ) ");

        System.out.println("eventNewsInfoAddConfirm :: eventCenterAddSQL " + eventCenterAddSQL);

        eventCenterAddSQL.executeUpdate();

        dbtrx.commit();

        if (dbtrx.wasCommitted()) {
            //  strMsg = "Success !!! New Committee added";
            //  response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "1";
            responseMsg = "Success!!! News add for event successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            // show event Assign center,division,tag
            String centerDivisionTagContainer = "";

            String centerListContainer = "";
            EventCenter alc = null;
            String centerListCenterName = "";
            Query centerListSQL = null;

            String divisionListContainer = "";
            EventDivision ald = null;
            String divisionListCenterName = "";
            Query divisionListSQL = null;

            String albumListContainer = "";
            AlbumEvent ale = null;
            String albumListCenterName = "";
            Query albumListSQL = null;

            String tagListContainer = "";
            EventSlug alt = null;
            String tagListCenterName = "";
            Query tagListSQL = null;

            centerListSQL = dbsession.createQuery(" FROM EventCenter where eventInfo = '" + eventId + "'");
            if (!centerListSQL.list().isEmpty()) {
                for (Iterator centerListItr = centerListSQL.list().iterator(); centerListItr.hasNext();) {
                    alc = (EventCenter) centerListItr.next();
                    centerListCenterName = alc.getCenter().getCenterName();

                    centerListContainer = centerListContainer + centerListCenterName + ",";
                }
            } else {
                centerListContainer = "";
            }

            divisionListSQL = dbsession.createQuery(" FROM EventDivision where eventInfo = '" + eventId + "'");
            if (!divisionListSQL.list().isEmpty()) {
                for (Iterator divisionListItr = divisionListSQL.list().iterator(); divisionListItr.hasNext();) {
                    ald = (EventDivision) divisionListItr.next();
                    divisionListCenterName = ald.getMemberDivision().getMemDivisionName();

                    divisionListContainer = divisionListContainer + divisionListCenterName + ",";
                }
            } else {
                divisionListContainer = "";
            }

            albumListSQL = dbsession.createQuery(" FROM AlbumEvent where eventInfo = '" + eventId + "'");
            if (!albumListSQL.list().isEmpty()) {
                for (Iterator albumListItr = albumListSQL.list().iterator(); albumListItr.hasNext();) {
                    ale = (AlbumEvent) albumListItr.next();
                    albumListCenterName = ale.getPictureCategory().getCategoryName();

                    albumListContainer = albumListContainer + albumListCenterName + ",";
                }
            } else {
                albumListContainer = "";
            }

//            tagListSQL = dbsession.createQuery(" FROM AlbumSlug where pictureCategory = '" + eventId + "'");
//            if (!tagListSQL.list().isEmpty()) {
//                for (Iterator tagListItr = tagListSQL.list().iterator(); tagListItr.hasNext();) {
//                    alt = (AlbumSlug) tagListItr.next();
//                    tagListCenterName = alt.get
//
//                    tagListContainer = tagListContainer + tagListCenterName + ",";
//                }
//            } else {
//                tagListContainer = "";
//            }
            centerDivisionTagContainer = centerListContainer + divisionListContainer + albumListContainer;

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("centerDivisionTagContainer", centerDivisionTagContainer);
            json.put("requestId", eventId);
            jsonArr.add(json);

        } else {
            dbtrx.rollback();
            //   strMsg = "Error!!! When Committee add";
            //   response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "0";
            responseMsg = "Error!!! When event assign this news";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("requestId", eventId);
            jsonArr.add(json);
        }

    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>