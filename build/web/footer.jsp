







<footer class="footer text-center"> 2020 &copy; Uposorgo Ltd.</footer>

<!-- sample modal content -->
<div id="rupantorLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h4 class="modal-title" id="rupantorLGModalTitle">Large modal</h4> 
            </div>
            <div class="modal-body" id="rupantorLGModalBody">
                <h4>Overflowing text to show scroll behavior</h4>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
            </div>
            <div class="modal-footer" id="rupantorLGModalFooter">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- sample modal content -->
<div id="taskLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="taskLGModalTitle" style="text-align: left !important;"></h4> 
            </div>
            <div class="modal-body" id="taskLGModalBody">
            </div>
            <div class="modal-footer" id="taskLGModalFooter">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->    


<!-- sample modal content -->
<div id="memberLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">           

            <div class="modal-header">
                <h5 class="modal-title" id="memberLGModalTitle" style="text-align: left !important;"></h5> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            </div>

            <div class="modal-body" id="memberLGModalBody">
            </div>
            <div class="modal-footer" id="memberLGModalFooter">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->    

<%@page import="com.appul.util.GlobalVariable"%>


</div>
<!-- /#wrapper -->
<!-- Bootstrap Core JavaScript -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/bootstrap/dist/js/tether.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/waves.js"></script>
<!--Counter js -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/raphael/raphael-min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/morrisjs/morris.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/custom.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/dashboard1.js"></script>

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/js/customJS.js"></script>

<!-- Sparkline chart JavaScript -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!--    <script type="text/javascript">
    $(document).ready(function() {
        $.toast({
            heading: 'Welcome to Rupantor admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'info',
            hideAfter: 3500,
            stack: 6
        })
    });
    </script>-->
<!--Style Switcher -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
