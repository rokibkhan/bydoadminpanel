<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    int responseMsgCode = 0;
    String responseMsg = "";
    String responseMsgHTML = "";
    Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String newsId = request.getParameter("newsId");
    String sessionid = request.getParameter("sessionid").trim();

    Query qq = dbsession.createSQLQuery("SELECT ni.id_news, ni.news_title, ni.news_slug, ni.news_short_desc, ni.news_desc, ni.news_date_time, is_sticky, ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
                                            + "FROM  news_info ni WHERE ni.id_news=" +  newsId);
    String title = "";
    String description = "";
    String shortDescription = "";
    String newsDateTime = "";
    
    boolean isSticky = false;
    
    if(!qq.list().isEmpty()){
        Iterator iterator  = qq.list().iterator();
        Object[] obj1 = (Object[]) iterator.next();
        newsId = obj1[0].toString().trim();
        title = obj1[1].toString().trim();
        //  newsSlug = obj[2].toString().trim();
        shortDescription = obj1[3].toString().trim();
        description = obj1[4].toString().trim();
        newsDateTime = obj1[5].toString().trim();
        String temp =  obj1[6].toString().trim();
        if(temp.equals("0")){
            isSticky = false;
        }else{
            isSticky = true;
        }
        
        
    }
    
    
    responseMsgCode = 1;
    responseMsg = "News fetching successfully Completed.";
    responseMsgHTML = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
            + "<strong>Delete successfully Completed.</strong>"
            + "</div>";

    json = new JSONObject();
    json.put("selectInvId", newsId);
    json.put("responseMsgCode", responseMsgCode);
    json.put("responseMsg", responseMsg);
    json.put("title", title);
    json.put("description", description);
    json.put("shortDescription", shortDescription);
    json.put("newsDateTime", newsDateTime);
    json.put("isSticky", isSticky);
    jsonArr.add(json);

    out.println(jsonArr);

    dbsession.flush();
    dbtrx.commit();
    dbsession.close();


%>