
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>




<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;
    int responseCode = 0;
    String responseMsg = "";
    String responseMsgText = "";
    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        userStoreIdH = session.getAttribute("storeId").toString();

    } else {
        System.out.println("Header LogIn Required :: ");

//        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
//        return;
    }

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    String adduser = userNameH;
    String addterm = hostname;
    String addip = ipAddress;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String selectRoleId = request.getParameter("roleID");
    String selectRoleName = request.getParameter("roleName");
    String sessionid = request.getParameter("sessionid");

    String actid[] = request.getParameterValues("updatedActivityList[]");

    System.out.println("Acctivity ID ListArray " + Arrays.toString(actid));
    System.out.println("Acctivity ID List0  " + actid[0]);

    //   String updatedActivityList[] = request.getParameterValues("updatedActivityList[]");
//    System.out.println("Acctivity ID ListArray " + Arrays.toString(updatedActivityList));
//
//    System.out.println("Acctivity ID List0  " + updatedActivityList[0]);
    //  String selectRoleId = "1083";
    String roleId = "";
    String roleActId = "";
    String roleActDesc = "";
    String roleActScreen = "";
    String userMsisdn = "";
    String syStatus = "";
    String selectRoleDetailsTopSection = "";
    String selectRoleDetailsMidSection = "";
    String selectRoleDetailsBottomSection = "";
    String btnColorClass = "";
    String levelStatusColorClass = "";
    String btnInvInfo = "";
    String selectRoleDetails = "";

    String allActivityConStr = "";

    String checkActRoleActivity = "";

    getDate date = new getDate();
    SyRoles syrole = null;

    Query q2 = dbsession.createQuery("from SyRoleAct roleact where roleact.id.roleId ='" + selectRoleId + "'");
    SyRoleActId roleactid = null;

    for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
        SyRoleAct syroleact = (SyRoleAct) itr2.next();
        //roleactid=syroleact.getId();
        dbsession.delete(syroleact);
    }

    int i = 0;
//    int j = 0;
    for (i = 0; i < actid.length; i++) {

        SyRoleAct syroleact = new SyRoleAct();
        roleactid = new SyRoleActId(selectRoleId.trim(), actid[i].trim());

        syroleact.setId(roleactid);
        syroleact.setActRead("Y");
        syroleact.setActWrite("Y");
        syroleact.setAddDate(date.serverDate());
        syroleact.setAddUsr(adduser.trim());
        syroleact.setAddTerm(addterm.trim());
        syroleact.setAddIp(addip.trim());
        syroleact.setModUsrId(adduser.trim());
        syroleact.setModTerm(addterm.trim());
        syroleact.setModIp(addip.trim());
        syroleact.setLastModDate(date.serverDate());
        dbsession.save(syroleact);
    }

    SyAuditTrail audit = new SyAuditTrail();
    getRegistryID regId = new getRegistryID();
    getDate gdate = new getDate();
    String eventid = "";
    eventid = regId.getID(16);
    BigDecimal eventId = new BigDecimal(eventid);

    audit.setEvtId(eventId);
    audit.setEvtDatetime(gdate.serverDate());
    audit.setEvtEmpId(userNameH);
//    if (update = !true) {
//        audit.setEvtCode("ROLE-ADD");
//        audit.setEvtDetail("Audit for ROLE CREATION");
//    } else {
    audit.setEvtCode("ROLE-EDIT");
    audit.setEvtDetail("Audit for ROLE-edit");

    //   }
    audit.setEvtRef01("");
    audit.setEvtRef02("");
    audit.setEvtRef03("");
    audit.setEvtRef04("");
    audit.setEvtRef05(hostname + "," + ipAddress);
    dbsession.save(audit);

    dbtrx.commit();

    if (dbtrx.wasCommitted()) {

        selectRoleDetailsTopSection = "<div class=\"white-box1 printableArea1\">"
                + "<p><strong>Role Id : " + selectRoleId + "</strong></p>"
                + "<p><strong>Role Name : " + selectRoleName + "</strong></p></div>";

        selectRoleDetailsMidSection = "<div class=\"col-md-12\">"
                + "<div class=\"table-responsive\" style=\"clear: both;overflow: auto; height:400px;\">"
                + "<table class=\"table table-hover color-table1 primary-table1 table-custom-padding-5x\">"
                + "<thead>"
                + "<tr>"
                + "<th class=\"text-center\" width=\"5%\">#</th>"
                + "<th>Activity ID</th>"
                + "<th>Activity Name</th>"
                + "<th>Activity Screen</th>"
                + "</tr>"
                + "</thead>"
                + "<tbody>";

//    Query usrSQL = dbsession.createSQLQuery("SELECT u.USER_ID,u.USER_NAME,d.DEPT_NAME,st.STATUS_DESC,r.ROLE_DESC "
//            + "FROM sy_dept d ,sy_status st,sy_user u "
//            + "LEFT OUTER JOIN sy_user_role ur ON u.USER_ID = ur.USER_ID "
//            + "LEFT OUTER JOIN sy_roles r ON  r.ROLE_ID = ur.ROLE_ID "
//            + "WHERE u.USER_ID = AND (u.USER_DEPT = d.DEPT_ID AND u.USER_STATUS = st.STATUS_CODE)");
        Query usrSQL = dbsession.createSQLQuery("SELECT ra.ROLE_ID,ra.ACT_ID,act.ACT_DESC,act.ACT_SCREEN "
                + "FROM sy_role_act ra "
                + "LEFT JOIN sy_activity act ON ra.ACT_ID = act.ACT_ID "
                + "WHERE ra.ROLE_ID ='" + selectRoleId + "'");
        int ix = 1;
        if (!usrSQL.list().isEmpty()) {
            for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                obj = (Object[]) it1.next();
                roleId = obj[0].toString().trim();
                roleActId = obj[1].toString().trim();
                roleActDesc = obj[2].toString().trim();
                roleActScreen = obj[3].toString().trim();

                selectRoleDetailsMidSection += "<tr>"
                        + "<td class=\"text-center\">" + ix + "</td>"
                        + "<td>" + roleActId + "</td>"
                        + "<td>" + roleActDesc + "</td>"
                        + "<td>" + roleActScreen + "</td>"
                        + "</tr>";
                ix++;
            }
        }

        selectRoleDetailsBottomSection = "</tbody>"
                + "</table>"
                + "</div>"
                + "</div>";

        selectRoleDetails = selectRoleDetailsTopSection + selectRoleDetailsMidSection + selectRoleDetailsBottomSection;

        //     + "<a id=\"btnShowUpdateActivityOption\" onclick=\"showUpdateActivityOption('" + selectRoleId + "','" + selectRoleName + "','" + sessionid + "')\" class=\"btn btn-primary\"> Update Role Activity </a> &nbsp;"
        btnInvInfo = "<div class=\"clearfix\"></div>"
                + "<div class=\"col-md-12 text-right\">"                
                + "<span class=\"text-left\" id=\"showMsgActvitityOpt\"></span><br>"
                + "<a id=\"btnShowUpdateActivityOption\" onclick=\"showUpdateActivityOption('" + selectRoleId + "','" + selectRoleName + "','" + sessionid + "')\" class=\"btn btn-primary\"> Add/Remove Role Activity </a> &nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>"
                + "</div>"
                + "<br/>";

        responseCode = 1;
        responseMsgText = "Success! Activity Information Updated.";
        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Success!</strong> Activity Information Updated."
                + "</div>";

    } else {
        dbtrx.rollback();
        responseMsgText = "Error! When Update Activity Information.";
        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> When Update Activity Information.."
                + "</div>";
    }

    json = new JSONObject();
    json.put("selectRoleId", selectRoleId);
    json.put("selectRoleDetails", selectRoleDetails);
    json.put("btnInvInfo", btnInvInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    json.put("responseMsgText", responseMsgText);
    jsonArr.add(json);

    out.println(jsonArr);

    dbsession.flush();
    dbsession.close();

%>