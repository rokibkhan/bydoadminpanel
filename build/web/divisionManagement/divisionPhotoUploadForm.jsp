
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>







<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var divisionContent = $("#divisionId").val();
        /*
         var syUserPassword = $("#syUserPassword").val();
         var syUserPasswordConfirm = $("#syUserPasswordConfirm").val();
         var syUserFullName = $("#syUserFullName").val();
         var syUserMobile = $("#syUserMobile").val();
         var syUserEmail = $("#syUserEmail").val();
         var syUserDept = $("#syUserDept").val();
         */
        if (divisionContent == null || divisionContent == "") {
            $("#divisionId").focus();
            $("#divisionIdErr").addClass("help-block with-errors").html("Division ID is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#divisionIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        return true;
    }


</script>

<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";


%>


<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
    dbsession = HibernateUtil.getSessionFactory().openSession();
    //   org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String divisionIdX = request.getParameter("divisionId") == null ? "" : request.getParameter("divisionId").trim();

    Query divisionSQL = null;
    Object divisionObj[] = null;
    String divisionId = "";
    String divisionName = "";
    String divisionShortName = "";
    String divisionSlug = "";
    String divisionShortDetails = "";
    String divisionShortDetails1 = "";
    String divisionShortDetailsX = "";
    String divisionDetails = "";
    String divisionDateTime = "";
    String divisionFeatureImage = "";
    String divisionFeatureImage1 = "";
    String divisionFeatureImageUrl = "";
    String divisionInfoFirstContainer = "";
    String divisionInfoContainer = "";
    String divisionDetailsUrl = "";

    divisionSQL = dbsession.createSQLQuery("select * FROM member_division WHERE mem_division_id = '" + divisionIdX + "'");

    for (Iterator divisionItr = divisionSQL.list().iterator(); divisionItr.hasNext();) {
        divisionObj = (Object[]) divisionItr.next();
        divisionId = divisionObj[0].toString();
        divisionShortName = divisionObj[1].toString();
        divisionName = divisionObj[3].toString();
        divisionDetails = divisionObj[5].toString();
        divisionFeatureImage = divisionObj[6].toString();

        divisionFeatureImageUrl = GlobalVariable.imageDirLink + divisionFeatureImage;

        //  divisionFeatureImageUrl = "<img width=\"200\" src=\"" + featureImageUrl + "\" alt=\"" + newsTitle + "\">";
        divisionFeatureImage1 = "<img width=\"200\" src=\"" + divisionFeatureImageUrl + "\" alt=\"" + divisionName + "\">";

    }

%>
<%    dbsession.clear();
    dbsession.close();
%>
<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Division Photo Change Form</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Division Management</a></li>
                    <li class="active">Division Photo Change</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row">
                        <div class="col-sm-8">

                            <form name="addrole" id="addrole" method="post" action="<%out.print(GlobalVariable.baseUrl);%>/DivisionPhotoUpload"  class="form-horizontal" enctype="multipart/form-data">



                                <input type="hidden" name="divisionId" value="<%=divisionIdX%>">  


                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="newsTitle" class="control-label col-md-3">Division</label>
                                    <div class="col-md-6">
                                        <input type="text" id="divisionName" name="divisionName" class="form-control input-sm" required value="<%=divisionName%>" disabled>
                                        <div  id="centerNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="newsTitle" class="control-label col-md-3">Photo</label>
                                    <div class="col-md-9">
                                        <div class="input-file-container">  
                                            <input onchange="uplaodFileTypeJPGPNGAndSizeCheck('file', '5', 'fileErr', 'divisionPhotoSubmitBtn');" name="file" id="file" accept="image/*" type="file" class="input-file" >
                                            <label tabindex="0" for="my-file" class="input-file-trigger">Select a photo...</label>
                                        </div>
                                        <div id="fileErr" class="help-block with-errors" style="color: red;"></div> 
                                    </div>


                                </div>





                                <div class="form-group row">
                                    <div class="offset-md-3 col-sm-9">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info" id="divisionPhotoSubmitBtn">Update</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <div class="col-sm-4">
                            <%=divisionFeatureImage1%>
                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->




    <%@ include file="../footer.jsp" %>