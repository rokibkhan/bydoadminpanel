<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);


    %>



    <script type="text/javascript">

        $(function () {
            var focus = 0;
            $("#memberShipId").focusout(
                    //  $("#memberShipId").onchange(
                            function () {
                                focus++;
                                console.log("checkUserNameAvaliablity:: " + focus);
                                var memberShipId = $("#memberShipId").val();

                                console.log("memberShipId " + memberShipId);

                                $("#memberShipIdErr").html('<i class="fa fa-spin fa-spinner"></i> Member info checking...');

                                //   showMemberDetailsInformation



                                showMemberDetailsInformation('<%=session.getId()%>');


                            });

                });


        function showMemberDetailsInformation(arg1) {
            console.log("showMemberDetailsInformation ");
            var committeeId, memberShipId, btnInvInfo, url, sessionid, cccll;
            committeeId = $("#committeeId").val();


            memberShipId = $("#memberShipId").val();

            console.log("committeeId::  " + committeeId);
            console.log("memberShipId ::  " + memberShipId);

            if (committeeId != '' && memberShipId != '') {

                url = '<%=GlobalVariable.baseUrl%>' + '/committeeManagement/committeeMemberDetailsShowProcess.jsp';

                $.post(url, {sessionId: arg1, committeeId: committeeId, memberShipId: memberShipId}, function (data) {

                    console.log(data);

                    if (data[0].responseCode == 1) {

                        $("#showMemberDetailsInformationBox").html(data[0].memberInfoCon);
                        $("#memberStatusInCommittee").html(data[0].memberStatusInCommittee);


                        //   $("#acttionBox" + data[0].requestId).html("<a class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approved</a>");
                        //	    	 	$("#globalAlertInfoBoxCon").show();
                        //$("#btnSubscribeEmailOpt").button('reset');
                        //		$("#btnSubscribeEmailOpt").button('complete') // button text will be "finished!"
                        // $("#btnSubscribeEmailOpt").addClass("disabled");
                        //  $("#msgSubscribeEmailOpt").html(data[0].responseMsg);
                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                    }
                    if (data[0].responseCode == 0) {

                        $("#showMemberDetailsInformationBox").html(data[0].memberInfoCon);

                        //  $("#btnSubscribeEmailOpt").button('reset');
                        // $("#errMsgShow").html(data[0].responseMsg);
                        //   rupantorLGModal.find("#rupantorLGModalBody").html(data[0].responseMsg);
                        // $("#globalAlertInfoBoxCon").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                        $("#globalAlertInfoBoxConTT").show().css({"display": "block", "margin-top": "5px"});
                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");

                    }

                }, "json");



            } else {
                $("#showMemberDetailsInformationBox").html("");
            }

            //   $("#showMemberDetailsInformationBox").html(committeeId + " :: " + memberShipId);

        }


        function deleteCommitteeMemberInfo(arg1, arg2) {
            console.log("deleteCommitteeMemberInfo :: " + arg1);
            var rupantorLGModal, btnConfirmInfo;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"deleteCommitteeMemberInfoConfirmBtn\" onclick=\"deleteCommitteeMemberInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');
        }

        function deleteCommitteeMemberInfoConfirm(arg1, arg2) {
            console.log("deleteCommitteeMemberInfoConfirm :: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');


            $.post("committeeMemberInfoDeleteProcess.php", {committeeId: arg1, committeeMemberId: arg2}, function (data) {


                console.log(data);

                if (data.responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data.responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data.requestId).html('<h5>' + data.responseMsg + '</h5>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data.responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");
        }



        function showDivisonCenterInfo(arg1) {
            var committeeType, formX, btnInvInfo, url, sessionid, cccll;
            console.log("showDivisonCenterInfo -> arg1 :: " + arg1 + " arg2:: ");

            //  ("#rupantorLGModalFooter").html(btnInvInfo);
            //  document.getElementById("committeeTypeSearch").disabled = true;

            committeeType = $("#committeeTypeId").val();

            url = '<%=GlobalVariable.baseUrl%>' + '/committeeManagement/committeeDivisionCenterShow.jsp';

            sessionarg1 = "";

            $.post(url, {sessionId: sessionarg1, committeeTypeId: committeeType}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {


                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    $("#divisionCenterCon").html(data[0].responseData);

                    //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    document.getElementById("divisionCenterIdSearch").disabled = true;

                    $("#divisionCenterCon").html("");

                }



            }, "json");


        }

    </script>

    <%
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrxCommitteeAdd = dbsession.beginTransaction();


    %>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Committee Add</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Committee Management</a></li>                    
                    <li class="active">Committee Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->


        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row">
                        <div class="col-md-6"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="committeeAddSubmitData.jsp?sessionid=<%=sessionid%>&act=add" onSubmit="return fromDataSubmitValidation()">


                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeName" class="control-label col-sm-4">Committee Type</label>
                                    <div class="col-sm-8">
                                        <select name="committeeTypeId" id="committeeTypeId"  onchange="showDivisonCenterInfo(this.value)" class="form-control input-sm" required>
                                            <option value="">Select Committee Type</option>
                                            <%

                                                Query committeeTypeSQL = null;
                                                Object committeeTypeObj[] = null;
                                                String committeeTypeId = "";
                                                String committeeTypeName = "";
                                                String committeeTypeShortName = "";
                                                String committeeTypeIdSelected = "";
                                                String committeeTypeConInfo = "";
                                                committeeTypeSQL = dbsession.createSQLQuery("SELECT * FROM committee_category ORDER BY id_cat ASC");

                                                if (!committeeTypeSQL.list().isEmpty()) {
                                                    for (Iterator committeeTypeItr = committeeTypeSQL.list().iterator(); committeeTypeItr.hasNext();) {

                                                        committeeTypeObj = (Object[]) committeeTypeItr.next();
                                                        committeeTypeId = committeeTypeObj[0].toString().trim();
                                                        committeeTypeName = committeeTypeObj[1].toString().trim();
                                                        committeeTypeShortName = committeeTypeObj[2].toString().trim();

                                                        committeeTypeConInfo = committeeTypeConInfo + "<option value=\"" + committeeTypeId + "\" " + committeeTypeIdSelected + ">" + committeeTypeName + "</option>";
                                                    }
                                                }

                                            %>


                                            <%=committeeTypeConInfo%>

                                        </select>

                                        <div  id="committeeTypeIdErr" class="help-block with-errors"></div>


                                    </div>
                                </div>

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeForId" class="control-label col-sm-4">Committee For</label>
                                    <div class="col-sm-8" id="divisionCenterCon">
                                        <select name="divisionCenterIdSearch" id="divisionCenterIdSearch" class="form-control input-sm" required>
                                            <option value="">Select Committee For</option>
                                            

                                        </select>

                                        <div  id="divisionCenterIdSearchErr" class="help-block with-errors"></div>


                                    </div>
                                </div>           

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeName" class="control-label col-sm-4">Committee Name</label>
                                    <div class="col-sm-8">
                                        <input name="committeeName" id="committeeName" type="text" class="form-control" placeholder="Committee Name"  required>
                                        <div  id="committeeNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeName" class="control-label col-sm-4">Committee Duration</label>
                                    <div class="col-sm-8">
                                        <input name="committeeDuration" id="committeeDuration" type="text" class="form-control" placeholder="Committee Duration" required>
                                        <div  id="committeeDurationErr" class="help-block with-errors"></div>
                                    </div>
                                </div>






                                <div class="form-group row">
                                    <label class="control-label col-sm-3"></label>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);                        ?>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);                        ?>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);                        ?>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6" id="showMemberDetailsInformationBox"> </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    <%    dbsession.clear();
        dbsession.close();
        %>


    <%@ include file="../footer.jsp" %>