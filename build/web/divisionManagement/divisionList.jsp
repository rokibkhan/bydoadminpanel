<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        
    
        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

// System.out.println(msgDispalyConT);
String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>


    <script type="text/javascript">
        //     $(function(){




    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Division List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Division Management</a></li>                    
                    <li class="active">Division List</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">



                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center" style="width: 18%;">Name</th>
                                    <th class="text-center"style="width: 60%;">Details</th>
                                    <th class="text-center"style="width: 10%;">Feature Picture</th>
                                    <th class="text-center" style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();
                                    dbtrx = dbsession.beginTransaction();

                                    Query divisionSQL = null;
                                    Object divisionObj[] = null;
                                    String divisionId = "";
                                    String divisionName = "";
                                    String divisionShortName = "";
                                    String divisionSlug = "";
                                    String divisionShortDetails = "";
                                    String divisionShortDetails1 = "";
                                    String divisionShortDetailsX = "";
                                    String divisionDetails = "";
                                    String divisionDateTime = "";
                                    String divisionFeatureImage = "";
                                    String divisionFeatureImage1 = "";
                                    String divisionFeatureImageUrl = "";
                                    String divisionInfoFirstContainer = "";
                                    String divisionInfoContainer = "";
                                    String divisionDetailsUrl = "";
                                    int ix = 1;

                                    divisionSQL = dbsession.createSQLQuery("SELECT * FROM member_division where mem_division_id !='30' ORDER BY full_name ASC");
                                    if (!divisionSQL.list().isEmpty()) {
                                        for (Iterator divisionItr = divisionSQL.list().iterator(); divisionItr.hasNext();) {
                                            divisionObj = (Object[]) divisionItr.next();
                                            divisionId = divisionObj[0].toString();
                                            divisionShortName = divisionObj[1] == null ? "" : divisionObj[1].toString();
                                            divisionName = divisionObj[3] == null ? "" : divisionObj[3].toString();
                                            divisionDetails = divisionObj[5] == null ? "" : divisionObj[5].toString();

                                            divisionFeatureImage = divisionObj[6] == null ? "" : divisionObj[6].toString();

                                            divisionFeatureImageUrl = GlobalVariable.imageDirLink + divisionFeatureImage;

                                            divisionFeatureImage1 = "<img width=\"200\" src=\"" + divisionFeatureImageUrl + "\" alt=\"" + divisionName + "\">";

                                            String divisionEditUrl = GlobalVariable.baseUrl + "/divisionManagement/divisionEdit.jsp?sessionid=" + session.getId() + "&divisionId=" + divisionId + "&selectedTab=profile";

                                            String divisionPhotoEditUrl = GlobalVariable.baseUrl + "/divisionManagement/divisionPhotoUploadForm.jsp?sessionid=" + session.getId() + "&divisionId=" + divisionId + "&selectedTab=profile";

                                %>


                                <tr id="infoBox<%=divisionId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(divisionName);%></td>
                                    <td><% out.print(divisionDetails);%></td>
                                    <td class="text-center">
                                        <% out.print(divisionFeatureImage1);%>
                                        <a style="margin-top: 10px; margin-bottom: 10px;" title="Change <%=divisionName%> Division Photo" href="<%=divisionPhotoEditUrl%>" class="btn btn-primary btn-sm"><i class="icon-picture"></i> Change Photo</a>
                                    </td>
                                    <td class="text-center">

                                        <a title="Edit <%=divisionName%> Division" href="<%=divisionEditUrl%>" class="btn btn-primary btn-sm"><i class="icon-note"></i> Edit</a>

                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>



                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->
    <%    dbsession.clear();
        dbsession.close();
        %>



    <%@ include file="../footer.jsp" %>