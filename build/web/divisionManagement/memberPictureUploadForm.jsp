<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    String memberId = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

    if (!memberId.equals("")) {

        memberIdH = memberId;

    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }
    String memberName = "";
    String pictureLink = "";

    q1 = dbsession.createSQLQuery("SELECT * FROM member WHERE id=" + memberIdH + " ");

    Member member = null;
    Object[] obj = null;

    if (!q1.list().isEmpty()) {
        for (Iterator it1 = q1.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            memberName = obj[2].toString().trim();
            pictureLink = obj[21].toString().trim();
        }
    }

    dbsession.flush();
    dbsession.close();


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"/>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">


<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Update Personal Picture Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->






                    <!-- ============ End Education information Table ================== -->

                    <div class="row">
                        <div class="col-sm-8">
                            <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/MemberPictureUpload" onSubmit="return fromDataSubmitValidation()" enctype="multipart/form-data">

                                <br/>
                                <br/>


                                <div class="form-group row">
                                    <label for="eventShortDesc" class="col-sm-3 col-form-label text-right">Picture</label>
                                    <div class="col-sm-9">  
                                        <input name="file" id="file" accept="image/*" type="file" class="form-control input-sm" >                                        
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <div class="col-sm-9 offset-md-3">
                                        <button type="submit" class="btn btn-primary mr-3 btn-sm">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-4">

                        </div>
                    </div>
                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>

<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);
%>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>



<%@ include file="../footer.jsp" %>
