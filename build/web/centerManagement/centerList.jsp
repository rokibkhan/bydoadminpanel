<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        
    
        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

    
    String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
      


    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Centre List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Centre Management</a></li>                    
                    <li class="active">Centre List</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->


            <div class="col-md-12">
                <div class="white-box">



                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center" style="width: 18%;">Name</th>
                                    <th class="text-center"style="width: 60%;">Details</th>
                                    <th class="text-center"style="width: 10%;">Feature Picture</th>
                                    <th class="text-center" style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();
                                    dbtrx = dbsession.beginTransaction();

                                    Query centerSQL = null;
                                    Object centerObj[] = null;
                                    String centerId = "";
                                    String centerName = "";
                                    String centerDetails = "";
                                    String centerIdSelected = "";
                                    String centerConInfo = "";
                                    String centreFeatureImage = "";
                                    String centreFeatureImage1 = "";
                                    String centreFeatureImageUrl = "";

                                    int ix = 1;

                                    centerSQL = dbsession.createSQLQuery("select * FROM center WHERE center_type_id = '3' ORDER BY center_name ASC");

                                    if (!centerSQL.list().isEmpty()) {
                                        for (Iterator centerItr = centerSQL.list().iterator(); centerItr.hasNext();) {

                                            centerObj = (Object[]) centerItr.next();
                                            centerId = centerObj[0].toString().trim();
                                            centerName = centerObj[1].toString().trim();
                                            centerDetails = centerObj[7].toString().trim();
                                            centreFeatureImage = centerObj[8].toString();

                                            centreFeatureImageUrl = GlobalVariable.imageDirLink + centreFeatureImage;

                                            centreFeatureImage1 = "<img width=\"200\" src=\"" + centreFeatureImageUrl + "\" alt=\"" + centerName + "\">";

                                            String updateProfileUrl = GlobalVariable.baseUrl + "/centerManagement/centerEdit.jsp?sessionid=" + session.getId() + "&centerId=" + centerId + "&selectedTab=profile";

                                            String centerPhotoEditUrlUrl = GlobalVariable.baseUrl + "/centerManagement/centerPhotoUploadForm.jsp?sessionid=" + session.getId() + "&centerId=" + centerId + "&selectedTab=profile";

                                %>


                                <tr id="infoBox<%=centerId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(centerName);%></td>
                                    <td><% out.print(centerDetails);%></td>
                                    <td class="text-center">
                                        <% out.print(centreFeatureImage1);%>
                                        <a style="margin-top: 10px; margin-bottom: 10px;" title="Change <%=centerName%> Center Photo" href="<%=centerPhotoEditUrlUrl%>" class="btn btn-primary btn-sm"><i class="icon-picture"></i> Change Photo</a>
                                    </td>
                                    <td class="text-center">

                                        <a title="Edit <%=centerName%> Centre" href="<%=updateProfileUrl%>" class="btn btn-primary btn-sm"><i class="icon-note"></i> Edit</a>
                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>



                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->
    <%    dbsession.clear();
        dbsession.close();
    %>



    <%@ include file="../footer.jsp" %>