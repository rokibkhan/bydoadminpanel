<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">




        $(function () {
            console.log("ready!");


            $('#searchWaiverItem').click(function () {
                $('#waiverReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/waiverManagement/yearWiseSearchSubmit.jsp?sessionid=<%out.print(sessionid);%>');

                            $("#waiverReportFrm").submit();
                        });


                        $('#exportWaiverItem').click(function () {
                            $('#waiverReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/waiverManagement/yearWiseSearchExportSubmit.jsp?sessionid=<%out.print(sessionid);%>');
                                        //    target="_blank"
                                        $('#waiverReportFrm').attr('target', '_blank');
                                        $("#waiverReportFrm").submit();
                                    });





                                    $("#waiverReportFrm").keypress(function (event) {
                                        if (event.which == 13) {
                                            event.preventDefault();
                                            $('#waiverReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/waiverManagement/yearWiseSearch.jsp?sessionid=<%out.print(sessionid);%>');
                                                            $("#waiverReportFrm").submit();
                                                        }
                                                    });



                                                });



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Year Wise Waiver</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Waiver Management</a></li>                    
                    <li class="active">Year</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box p-t-10">

                    <div class="row">
                        <div class="col-md-12"> 

                            <form class="form-horizontal" name="waiverReportFrm" id="waiverReportFrm" method="POST">

                                <div class="form-group row custom-bottom-margin-5x">


                                    <label for="inputEmail4" class="control-label col-sm-3"> Year</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <input type="text" id="startDate" name="startDate"  class="form-control" placeholder="Year">
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                </div>





                                <div class="form-group row custom-bottom-margin-15x">
                                    <div class="col-md-4 offset-4">      
                                        <input type="button"  id="searchWaiverItem" name="searchWaiverItem" class="btn btn-info" value="Submit">
                                        <input type="button"  id="exportWaiverItem" name="exportWaiverItem" class="btn btn-info" value="Export">


                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Member ID</th>
                                            <th>Name</th>
                                            <th class="text-center">Total Due</th>
                                            <th class="text-center">Less Amount</th>
                                            <th class="text-center">Payment Amount</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>M112233</td>
                                            <td>Engg. Mr. X</td>
                                            <td align="right">5000.00</td>
                                            <td align="right">3000.00</td>
                                            <td align="right">2000.00</td>
                                            <td align="right"></td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>M445566</td>
                                            <td>Engg. Mr. Y</td>
                                            <td align="right">3000.00</td>
                                            <td align="right">0.00</td>
                                            <td align="right">3000.00</td>
                                            <td align="right"></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>                            
                    </div>


                </div>
                <!-- /.white-box -->
            </div>


        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>