<%-- 
    Document   : syUserDetail
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<%

    String username = null;
    String logstat = null;
    String sessionid = null;
    sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {

        return;
    }

    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    //                   out.println(username);
    //                   out.println(logstat);

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }


%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self ==top)
                    window.open("logout.jsp","_top","");
            }
        </script>

        <title>User Detail: <% request.getParameter("fullname");%></title>
        <%


            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }

            //                   out.println(username);
            //                   out.println(logstat);
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }


            String read = request.getParameter("read");
            String write = request.getParameter("write");
  
        %>
    </head>
    <body onLoad="breakOut()">

        <%

            if (request.getParameter("userid") != null) {
                qryparam = request.getParameter("userid");
                String fullname = request.getParameter("fullname").toString();
        %>
        <h1>User Detail: <% out.println(fullname);%></h1>
        <br>
        <table class="tab1">
            <thead>
                <tr >
                    <th align="left" width="150"><b>Property</b></th>
                    <th align="left" width="20"><b>Value</b></th>
                </tr>
            </thead>
                <%
                    dbsession = HibernateUtil.getSessionFactory().openSession();
                    org.hibernate.Transaction dbtrx = null;
                    dbtrx = dbsession.beginTransaction();

                    Query q1 = null;
                    String userId = "";
                    String fullName = "";
                    String userBranch = "";
                    String userDept = "";
                    String userStat = "";
                    String mobileNo = "";
                    String userEmail = "";
                    Date addDate = null;
                    String addUser = "";
                    String addTerm = "";
                    String addIP = "";
                    Date modDate = null;
                    String modUser = "";
                    String modTerm = "";
                    String modIP = "";
                    Date userExpDate = null;

                    int rownum = 0;
                    String oddeven = null;
                    SyUser syuser = null;
                    SyStatus systat = null;
                    SyDept codept = null;

                    if (qryparam != "" && qryparam != null) {
                        q1 = dbsession.createQuery("from SyUser as user inner join user.syStatus as stat inner join user.syDept as dept where upper(user.userId) like upper('%" + qryparam + "%') order by user.userId asc");
                    }

                    Object[] object = null;
                    getUser getuser = new getUser();

                    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                        object = (Object[]) itr.next();
                        //out.println(user.length);
                        //out.println(user[0].getClass());
                        //out.println(user[1].getClass());

                        syuser = (SyUser) object[0];
                        systat = (SyStatus) object[1];
                        codept = (SyDept) object[2];

                        userId = syuser.getUserId();
                        fullName = syuser.getUserName();
                        userDept = codept.getDeptName();
                        userExpDate = syuser.getUserExpDate();
                        userStat = systat.getStatusDesc();
                        mobileNo = syuser.getUserMsisdn();
                        userEmail = syuser.getUserEmail();
                        addDate = syuser.getAddDate();
                        addUser = getuser.getUserName(syuser.getAddUsr());
                        addTerm = syuser.getAddTerm();
                        addIP = syuser.getAddIp();
                        modDate = syuser.getAddDate();
                        modUser = getuser.getUserName(syuser.getModUsrId());
                        modTerm = syuser.getModTerm();
                        modIP = syuser.getModIp();

                   
                    }
                    dbsession.close();
                %>
                <tr >
                    <td align="left" width="150">User ID</td>
                    <td align="left" width="250"><% out.println(userId);%></td>
                </tr>
                <tr >
                    <td align="left" width="20">Full Name</td>
                    <td align="left" width="250"><% out.println(fullName);%></td>
                </tr>
                <tr >
                    <td align="left" width="20">Department</td>
                    <td align="left" width="250"><% out.println(userDept);%></td>
                </tr>
        
                <tr >
                    <td align="left" width="150">Status</td>
                    <td align="left" width="250"><% out.println(userStat);%></td>
                </tr>
                <tr >
                    <td align="left" width="50">Mobile Number</td>
                    <td align="left" width="250"><% out.println(mobileNo);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Email</td>
                    <td align="left" width="250"><% out.println(userEmail);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Effective Date</td>
                    <td align="left" width="250"><% out.println(addDate);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Expiry Date</td>
                    <td align="left" width="250"><% out.println(userExpDate);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Added By</td>
                    <td align="left" width="250"><% out.println(addUser);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Added On</td>
                    <td align="left" width="250"><% out.println(addDate);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Computer (Add)</td>
                    <td align="left" width="250"><% out.println(addTerm);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">IP (Add)</td>
                    <td align="left" width="250"><% out.println(addIP);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Last Update By</td>
                    <td align="left" width="250"><% out.println(modUser);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Last Update Date</td>
                    <td align="left" width="250"><% out.println(modDate);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">Computer (last update)</td>
                    <td align="left" width="250"><% out.println(modTerm);%></td>
                </tr>
                <tr >
                    <td align="left" width="150">IP (last update)</td>
                    <td align="left" width="250"><% out.println(modIP);%></td>
                </tr>
                <tr >
                    <td colspan="2" align="right"><input type="button" name="back" value="Back" onclick="history.go(-1);"></td>
                </tr>
            
        </table>        
    </body>
    <%                        

        }%>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
