<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    //Query q1 = null;
    Object obj[] = null;
    int responseCode = 0;
    String responseMsg = "";
    String responseMsgText = "";
    String userIdP = "";
    String sessionidP = "";
    String uNewRole = "";
    String uNewRoleText = "";
    String uOldRole = "";
    String checkUserIDInRoleTaable = "";
    String allRoleInfo = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    getDate date = new getDate();

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        userIdP = request.getParameter("userId").trim();
        sessionidP = request.getParameter("sessionid").trim();

        uNewRole = request.getParameter("uNewRole").trim().toUpperCase();
        uNewRoleText = request.getParameter("uNewRoleText").trim();
        uOldRole = request.getParameter("uOldRole").trim().toUpperCase();

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        userStoreIdH = session.getAttribute("storeId").toString();

        if (sessionidP.equals(sessionIdH)) {

            System.out.println("Password Token Match:: ");

            if (!uOldRole.equals(uNewRole)) {

                System.out.println("old user role not  Match:: ");

                System.out.println("userIdP :: " + userIdP);
                System.out.println("uNewRole :: " + uNewRole);

                checkUserIDInRoleTaable = "1";
                //check user userID
                if (checkUserIDInRoleTaable != "0") {
                    //  update into role user table

                    System.out.println("User role Exits :: ");

                    SyUserRole syUsrRole = null;
                    SyUserRoleId syUsrRoleId = null;

                    syUsrRole = new SyUserRole();
//
//                    //   syUsrRole.getSyUser(userIdP);
//                    SyUser syuser = null;
//                    syuser = new SyUser();

//                    Query q4 = dbsession.createQuery("from SyUserRole where syUser='" + userIdP + "'");
//                    Iterator itr4 = q4.list().iterator();
//                    if (itr4.hasNext()) {
//                        syUsrRole = (SyUserRole) itr4.next();                       
//                        
//                    }
//                    syUsrRoleId = new SyUserRoleId(userIdP, uNewRole);
//                    syUsrRole.setId(syUsrRoleId);
//                    
//                    dbsession.update(syUsrRole);
                    Query q2 = dbsession.createSQLQuery("update sy_user_role set ROLE_ID='" + uNewRole + "' where USER_ID = '" + userIdP + "'");
                    q2.executeUpdate();

                    dbtrx.commit();

                    if (dbtrx.wasCommitted()) {
                        //   response.sendRedirect("success.jsp?sessionid=" + sessionid);

                        responseCode = 1;
                        responseMsgText = "Success! User role Updated.";
                        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                                + "<strong>Success!</strong> User role Updated."
                                + "</div>";
                    } else {
                        dbtrx.rollback();
                        //   response.sendRedirect("fail.jsp?sessionid=" + sessionid);
                        responseMsgText = "Error! Please try again.";
                        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                                + "<strong>Error!</strong> Please try again</a>."
                                + "</div>";
                    }

                } else {
                    //insert into role user table
                    String adduser = userNameH;
                    String addterm = InetAddress.getLocalHost().getHostName().toString();
                    String addip = InetAddress.getLocalHost().getHostAddress().toString();

                    SyUser syuser = null;
                    SyRoles syrole = null;
                    SyUserRole syUsrRole = null;
                    SyUserRoleId syUsrRoleId = null;

                    syuser = new SyUser();
                    syrole = new SyRoles();

                    syUsrRole = new SyUserRole();
//                
//                syuser.setUserId(userIdP);
//                syUsrRole.setSyUser(syuser)

                    syUsrRoleId = new SyUserRoleId(userIdP, uNewRole);
                    syUsrRole.setId(syUsrRoleId);
                    syUsrRole.setAddDate(date.serverDate());
                    syUsrRole.setAddUsr(adduser.trim());
                    syUsrRole.setAddTerm(addterm.trim());
                    syUsrRole.setAddIp(addip.trim());

                    syUsrRole.setModUsrId(adduser.trim());
                    syUsrRole.setModTerm(addterm.trim());
                    syUsrRole.setModIp(addip.trim());
                    syUsrRole.setLastModDate(date.serverDate());

                    dbsession.save(syUsrRole);

                    dbtrx.commit();

                    if (dbtrx.wasCommitted()) {
                        //   response.sendRedirect("success.jsp?sessionid=" + sessionid);

                        responseCode = 1;
                        responseMsgText = "Success! New role added.";
                        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                                + "<strong>Success!</strong> New role added."
                                + "</div>";
                    } else {
                        dbtrx.rollback();
                        //   response.sendRedirect("fail.jsp?sessionid=" + sessionid);
                        responseMsgText = "Error! Please try again.";
                        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                                + "<strong>Error!</strong> Please try again</a>."
                                + "</div>";
                    }
                }
            } else {
                responseMsgText = "Error! User role same as existing role.";
                responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                        + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                        + "<strong>Error!</strong> User role same as existing role."
                        + "</div>";
            }
        } else {
            responseMsgText = "Error! Token not match.";
            responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<strong>Error!</strong> Token not match."
                    + "</div>";
        }

    } else {

        responseMsgText = "Error! Please login and try again.";
        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    String uRoleListStr = "";
    
    System.out.println("uRoleListStrFirst :: " + uRoleListStr);
    if (responseCode == 1) {

        uRoleListStr = "<select class=\"form-control input-sm customInput-sm\" style=\"height: 30px;\" id=\"syUserRole\" name=\"syUserRole\">"
                + "<option value=\"\">SELECT ROLE</option>";
        String roleIDXc = "";
        String roleDescXc = "";
        Query qr = dbsession.createQuery("from SyRoles WHERE roleId !='" + uNewRole + "' order by roleId asc");
        for (Iterator itr1 = qr.list().iterator(); itr1.hasNext();) {
            SyRoles role = (SyRoles) itr1.next();
            roleIDXc = role.getRoleId().trim();
            roleDescXc = role.getRoleDesc().trim();

            uRoleListStr +="<option  value=" + roleIDXc + ">" + roleDescXc + "</option>";

        }
      

        uRoleListStr += "</select>";
        
        

    }

    System.out.println("uRoleListStrLast :: " + uRoleListStr);
    
    json = new JSONObject();
    json.put("roleIDText", uNewRoleText);
    json.put("roleID", uNewRole);
    json.put("roleListStr", uRoleListStr);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    json.put("responseMsgText", responseMsgText);
    jsonArr.add(json);
    out.print(jsonArr);

    dbsession.flush();
    dbsession.close();


%>   