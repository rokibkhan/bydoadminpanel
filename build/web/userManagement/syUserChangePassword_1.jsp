<%-- 
    Document   : scSupplierList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">
<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    //response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>
<%
    Session dbsession = null;
    String qryparam = "";

    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }
%>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>

<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
    }
    .bp_valid {
        color:green;
    }
</style>

<%
    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
//                        out.println("("+sessionid+")");
//                        out.println("("+session.getId()+")");
//                response.sendRedirect("logout.jsp");
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    //                   out.println(username);
    //                   out.println(logstat);

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>
<script type="text/javascript">
    function ajaxUserName(){
        var userid=document.getElementById(userId);
        var mdiv = document.getElementById("userName");
        
        var targetname="";
        mdiv.innerHTML = targetname;
    }

</script>

<script type="text/javascript">
    function AJAXUserName(url, callback) {

        var req = init();
        req.onreadystatechange = processRequest;

        function init() {
            if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        function processRequest () {
            // readyState of 4 signifies request is complete
            if (req.readyState == 4) {
                // status of 200 signifies sucessful HTTP call
                if (req.status == 200) {
                    if (callback) callback(req.responseXML);
                }
            }
        }

        this.doGet = function() {
            // make a HTTP GET request to the URL asynchronously
            req.open("GET", url, true);
            req.send(null);
        }
    }
    function setUserName() {
        //alert "setUserName";
        var target = document.getElementById("userId");
        var url = "getvalue?id=" + encodeURIComponent(target.value)+"&key=userid";
        var ajax = new AJAXUserName(url, validateCallback);
        ajax.doGet();

        //var udiv=document.getElementById("yourpass");
        //udiv.innerHTML="test";
    }

    function validateCallback(responseXML) {
        // see "The Callback Function" below for more details
        var mdiv = document.getElementById("userName");
        var retvalue=responseXML.getElementsByTagName("retvalue")[0].firstChild.nodeValue;
        mdiv.innerHTML = retvalue.toString();
        document.getElementById("yourpass").focus();
        //alert "callback";
    }
</script>
<script type="text/javascript">
    function dataValidate(){
        var form = document.getElementById("chgpass");

        if(form.userId.value.toString()==null || form.userId.value.toString()==""){
            alert("Missing required data - User ID");
            return false;
        }

        if(form.yourPass.value.toString()==null || form.yourPass.value.toString()==""){
            alert("Missing required data - Your Password");
            return false;
        }
        if(form.newPass.value.toString()==null || form.newPass.value.toString()==""){
            alert("Missing required data - New password");
            return false;
        }
        if(form.renewPass.value.toString()==null || form.renewPass.value.toString()==""){
            alert("Missing required data - Re-enter Password");
            return false;
        }
        if(form.newPass.value.toString() != form.renewPass.value.toString()){
            alert("Password does not Match");
            return false;
        }
        return true;
    }
    function password(){
         var form = document.getElementById("chgpass");
         if(form.newPass.value.toString() != form.renewPass.value.toString()){
            alert("Password does not Match");
            return false;
        }
        return true;
    }
    
</script>
<script type="text/javascript">
    function dataLength(){
        var form = document.getElementById("chgpass");
        if(form.newPass.value.length>30){
            alert("Password Must Be 30 Character");
            form.newPass.value="";
            return false;
        }
        if(form.renewPass.value.length>30){
            alert("Password Must Be 30 Character");
            form.renewPass.value="";
            return false;
        }
        return true;
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp","_top","");
            }
        </script>

        <title>Change User Password</title>
        <%
            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }
            //                   out.println(username);
            //                   out.println(logstat);
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }
            String read = request.getParameter("read");
            String write = request.getParameter("write");
            /*
            out.println(username);
            out.println(read);
            out.println(write);
             */
            String userid = request.getParameter("userid");
            String fullname = request.getParameter("fullname");

        %>

    </head>
    <body onLoad="breakOut()"> <%-- onLoad="init()" --%>
        
        <form name="chgpass" id="chgpass" method="post" action="syUserChangePasswordUpdate.jsp?sessionid=<%out.println(sessionid);%>" onSubmit="return dataValidate()">
              
        <fieldset style="border: 1px solid silver;  width: 600px;">
                <legend style="font-size: 25px;" >Change User's Password</legend>
                
                <table  class="tab">
              <tr >
                    <td align="left" width="100">User <font color="red">*</font></td>
                    <td align="left" width="100"><input class="textbox" style="width:98%;"type="text" id="userId" name="userId" size="30" onkeyup="setUserName()" onmousemove="setUserName()" onblur="setUserName()">
                        <script>
                            $("#userId").autocomplete("getUsersID.jsp");
                        </script>                    
                    </td>                    
                </tr>
                <tr >
                    <td>Full Name</td>
                    <td><div id="userName"></div></td>
                </tr>
                <tr >
                    <td align="left">Your Password <font color="red">*</font></td>
                    <td align="left"><input class="textbox" style="width:98%;" type="password" id="yourPass" name="yourPass" size="30"></td>
                </tr>
                <tr >
                    <td align="left">New Password <font color="red">*</font></td>
                    <td align="left"><input class="textbox" style="width:98%;"type="password" id="newPass" name="newPass" size="30" onkeyup="dataLength()"></td>
                </tr>
                <tr >
                    <td align="left">Re-enter New Password <font color="red">*</font></td>
                    <td align="left"><input class="textbox" style="width:98%;" type="password" id="renewPass" name="renewPass" size="30" onkeyup="dataLength()"></td>
                </tr>
                <tr>
                    <td class="label" align="right"></td>
                    <td class="button" align="right"><input type="reset" value="Reset" name="reset"><input type="submit" value="Submit" name="submit"></td>
                </tr>
                </tbody>
            </table>
        </fieldset>
            <input type="hidden" size="50" id="entryUserID" name="entryUserID" value="<% out.println(username);%>">
            <input type="hidden" size="50" id="entryTerm" name="entryTerm" value="<%out.println(InetAddress.getLocalHost().getHostName().toString());%>">
            <input type="hidden" size="50" id="entryIP" name="entryIP" value="<%out.println(InetAddress.getLocalHost().getHostAddress().toString());%>">            
        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
