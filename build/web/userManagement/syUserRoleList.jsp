<%-- 
    Document   : syRoleList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%    
    Session dbsession = null;
    String qryparam = "";
    /*            out.println("(" + session.getId() + ")"); */
   
    String pagename = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().indexOf("_"));

%>

<!--<link rel="stylesheet" type="text/css" href="css/stylesheet.css">-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>User Roles</title>
        <%            
            String username = null;
            String logstat = null;
            String sessionid = null;
            sessionid = request.getParameter("sessionid").trim();

           

            String read = request.getParameter("read");
            String write = request.getParameter("write");
        %>
    </head>
    <body>

        <div style="clear:both;">&nbsp;</div>
        <div class="container">
            <div class="body-content">
                <h4> User Role List::    </h4>
                   

                <hr />



                <table id="tblContact" class="table table-striped table-bordered" cellspacing="0" style="width:100%;">

                    <thead>
                        <tr >
                            <th align="left" width="20%"><b>User ID</b></th>
                            <th align="left" width=40%"><b>User Name</b></th>
                            <th align="left"><b></b></th>
                            <th align="left"><b></b></th>
                        </tr>
                    </thead>
                    <%
                        dbsession = HibernateUtil.getSessionFactory().openSession();
                        org.hibernate.Transaction dbtrx = null;
                        dbtrx = dbsession.beginTransaction();

                        Query q1 = null;
                        Query q2 = null;
                        String userId = "";
                        String userName = "";
                        boolean isnew = true;

                        SyUser user = null;
                        int rownum = 0;
                        String oddeven = null;

                        if (qryparam != "" && qryparam != null) {
                            //q1 = dbsession.createQuery("from SyUser");
                            q1 = dbsession.createQuery("from SyUser where upper(id.userName) like upper('%" + qryparam + "%') or upper(id.userId) like upper('%" + qryparam + "%') or userId in(select id.userId from SyUserRole where upper(id.roleId) like upper('%" + qryparam + "%') or upper(id.roleId) in( select roleId from SyRoles where upper(roleDesc) like upper('%" + qryparam + "%'))) order by userName asc");
                        } else {
                            q1 = dbsession.createQuery("from SyUser");
                        }

                        for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                            user = (SyUser) itr1.next();
                            userId = user.getUserId();
                            userName = user.getUserName();
                            rownum += 1;

                            q2 = dbsession.createQuery("from SyUserRole where id.userId='" + userId + "'");
                            Iterator itr2 = q2.list().iterator();
                            if (itr2.hasNext()) {
                                isnew = false;
                            } else {
                                isnew = true;
                            }

                            if ((rownum % 2) == 0) {
                                oddeven = "even";
                            } else {
                                oddeven = "odd";
                            }
                            // dbtrx.commit();
                            //dbsession.close();
                    %>

                    <tr >
                        <td> <% out.println(userId);%></td>
                        <td> <% out.println(userName);%></td>
                        <% if (read.trim().equals("Y") && (isnew == false)) {%>
                        <td width="20%"><a href="syUserRoleDetail.jsp?userid=<%out.println(userId);%>&fullname=<%out.println(userName);%>&sessionid=<%out.println(sessionid);%>">Detail</a></td>
                        <%} else {%><td></td><%}
                        if (write.trim().equals("Y")) {%>
                        <td width="20%"><a href="syUserRoleAdd.jsp?userid=<%out.println(userId);%>&fullname=<%out.println(userName);%>&sessionid=<%out.println(sessionid);%>">Manage</a></td>
                        <%} else {%><td></td><%}
                        %>

                    </tr>
                    <%
                        }
                        dbsession.close();
                    %>

                </table>
            </div>
            <!--<hr/>-->
        </div>
        
    </body>
   
</html>
