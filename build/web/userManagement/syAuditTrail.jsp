<%-- 
    Document   : syUserBranchList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">

<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/datepicker.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>


<%

    //out.println(request.getSession(false));
    request.getSession(false);
//out.println(request.getSession(false));
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;

%>

<%    Session dbsession = null;
    String qryparam = "";
    String qryparam1 = "";
    /*            out.println("(" + session.getId() + ")"); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }
    String pagename = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().indexOf("_"));

%>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
<script type="text/javascript">
    function isValidDate() {
        var form = document.getElementById("searchfrm");
        var StrDate = form.param.value;
        var endDate = form.param1.value;

        if ((StrDate.substr(6, 4)) > (endDate.substr(6, 4)))
        {
            alert("End Date is Smaller Then Start Date");
            form.param.value = "";
            form.param1.value = "";
            $('#param1').datepicker("disable");
            return false;
        }
        return true;
    }
    function check() {
        var form = document.getElementById("searchfrm");
        var StrDate = form.param.value;
        var endDate = form.param1.value;
        if (StrDate == "" && endDate != "") {
            alert("Enter Start Date");
            StrDate = "";
            return false;

        }
        if (StrDate != "" && endDate == "") {
            alert("Enter End Date");
            endDate = "";
            return false;
        }
        if (StrDate == "" && endDate == "") {
            alert("Enter Start Date");
            return false;
        }
        return true;
    }

</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp", "_top", "");
            }
            function isDateDisable()
            {


                if ($('#param').val().length == 0) {

                    $('#param1').datepicker("disable");
                } else {
                    $('#param1').datepicker("enable");
                }

            }
        </script>

        <title>User's Audit </title>
        <%            String username = null;
            String logstat = null;
            String sessionid = null;
            sessionid = request.getParameter("sessionid").trim();

//                    out.println(session.getId());
//                    out.println(sessionid);
//out.println(request.getSession(false));
            if (sessionid == null) {
                response.sendRedirect("login.jsp");
                return;
            }

            if (request.getSession(false) == null) {
                response.sendRedirect("login.jsp");
                return;
            }

            if (session.getId() == null) {
                response.sendRedirect("login.jsp");
                return;
            }

            if (sessionid.equals(session.getId()) == false) {
                return;
            }

            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }

            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                return;
            }

            String read = request.getParameter("read");
            String write = request.getParameter("write");
        %>
    </head>
    <body onLoad="breakOut();
            isDateDisable();">

        <h1>User's Audit Trail</h1>


        <form name="searchfrm" id="searchfrm" action="syAuditTrail.jsp" action="post" onsubmit="return check();">
            <table class="">
                <thead>
                    <tr>
                        <th align="left"  width="50%">
                            <!--                        <font color="black">User Id </font><input type="text" name="param2" id="param2"/>-->
                            <font color="black">Start Date</font><input class="textbox"type="text" name="param" id="param" onchange="isDateDisable()" onblur="isDateDisable()"/>
                            <script type="text/javascript">
                                $(function () {
                                    $('#param').datepicker({
                                        //showSecond: true,
                                        dateFormat: 'yy-mm-dd'

                                    });
                                });
                            </script>


                            <font color="black">End Date </font><input  class="textbox" type="text" name="param1" id="param1"/>
                            <script type="text/javascript">
                                $(function () {
                                    $('#param1').datepicker({
                                        //showSecond: true,
                                        dateFormat: 'yy-mm-dd'

                                    });
                                });
                            </script>
                            <input type="hidden" name="sessionid" value="<%out.println(sessionid);%>" />
                            <input type="hidden" name="read" value="<%out.println(read);%>"/>
                            <input type="hidden" name="write" value="<%out.println(write);%>" />
                            <input type="submit" value="Search" name="search">
                        </th>
                        <!--                    <td align="left"  width="10%">
                                            </td>
                                            <td align="left"  width="10%">
                                            </td>                    -->
                    </tr>
            </table>
        </form>

        <br>
        <%
            String end = "";
            if (request.getParameter("param") != null && request.getParameter("param1") != null) {
                qryparam = request.getParameter("param").trim();

                qryparam1 = request.getParameter("param1").trim();
      
                if (qryparam1 != "") {
                    SimpleDateFormat da = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = da.parse(qryparam1);
                    //String endDateQuery=d.toString();
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(d);
                    calendar.add(Calendar.DATE, 1);
                    end = da.format(calendar.getTime());
                    //out.println(qryparam);
                } else {
                    qryparam1 = request.getParameter("param1").trim();
                    //String end=qryparam1;
                }
        %>

        <table class="tab1">

            <thead>
                <tr >
                    <th align="left" width=5%"><b> ID</b></th>
                    <th align="left" width=20%"><b>Date Time</b></th>
                    <th align="left" width=10%"><b>USER ID</b></th>
                    <th align="left" width=10%"><b>Event Code</b></th>
                    <th align="left" width=25%"><b>Event Detail</b></th>
                    <th align="left" width=10%"><b></b>User Terminal</th>
                    <th align="left" width=10%"><b>IP Address</b></th>
                </tr>
            </thead>
            <%
                dbsession = HibernateUtil.getSessionFactory().openSession();
                org.hibernate.Transaction dbtrx = null;
                dbtrx = dbsession.beginTransaction();

                Query q1 = null;
                Query q2 = null;
                BigDecimal eventId = null;
                Date eventDateTime = null;
                String eventEmpId = "";
                String eventCode = "";
                String eventDetail = "";
                String ref05 = "";
                String subString = "";
                String eventIp = "";
                String eventUser = "";
                String endPoint = "";
                boolean isnew = true;

                SyAuditTrail auditTrail = null;

                if (qryparam != "" && qryparam != null && end != "" && end != null) {
                 
                    q1 = dbsession.createQuery("from SyAuditTrail a where  a.evtDatetime >='" + qryparam + "' and  a.evtDatetime  <='" + end + "' order by evtId desc");
                } else {
                    q1 = dbsession.createQuery("from SyAuditTrail a");
                }

                for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                    auditTrail = (SyAuditTrail) itr1.next();
                    eventId = auditTrail.getEvtId();
                    eventDateTime = auditTrail.getEvtDatetime();
                    eventEmpId = auditTrail.getEvtEmpId();
                    eventCode = auditTrail.getEvtCode();
                    eventDetail = auditTrail.getEvtDetail();
                    ref05 = auditTrail.getEvtRef05();
                    //endPoint=ref05.indexOf(",");
                    eventUser = ref05.substring(ref05.indexOf(",") + 1, ref05.length());
                    eventIp = ref05.substring(0, ref05.indexOf(","));
                    isnew = true;

            %>

            <tr >
                <td> <%if (eventId == null) {
                        out.println("");
                    } else {
                        out.println(eventId);
                    }%></td>
                <td> <% out.println(eventDateTime);%></td>
                <td> <% out.println(eventEmpId);%></td>
                <td> <% out.println(eventCode);%></td>
                <td> <% out.println(eventDetail);%></td>
                <td> <% out.println(eventIp);%></td>
                <td> <% out.println(eventUser);%></td>


            </tr>
            <%
                }
                dbsession.close();
            %>

        </table>
        <%}%>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
