
<%@page import="com.appul.entity.SyDept"%>
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>








<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var syUserName = $("#syUserName").val();
        var syUserPassword = $("#syUserPassword").val();
        var syUserPasswordConfirm = $("#syUserPasswordConfirm").val();
        var syUserFullName = $("#syUserFullName").val();
        var syUserMobile = $("#syUserMobile").val();
        var syUserEmail = $("#syUserEmail").val();
        var syUserDept = $("#syUserDept").val();

        if (syUserName == null || syUserName == "") {
            $("#syUserName").focus();
            $("#syUserNameErr").addClass("help-block with-errors").html("User name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#syUserNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (syUserPassword == null || syUserPassword == "") {
            $("#syUserPassword").focus();
            $("#syUserPasswordErr").addClass("help-block with-errors").html("Password is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#syUserPasswordErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (syUserPasswordConfirm == null || syUserPasswordConfirm == "") {
            $("#syUserPasswordConfirm").focus();
            $("#syUserPasswordConfirmErr").addClass("help-block with-errors").html("Password confirm is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#syUserPasswordConfirmErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        if (syUserFullName == null || syUserFullName == "") {
            $("#syUserFullName").focus();
            $("#syUserFullNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#syUserFullNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }

        if (syUserMobile == null || syUserMobile == "") {
            $("#syUserMobile").focus();
            $("#syUserMobileErr").addClass("help-block with-errors").html("Mobile number is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#syUserMobileErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }

        if (syUserEmail == null || syUserEmail == "") {
            $("#syUserEmail").focus();
            $("#syUserEmailErr").addClass("help-block with-errors").html("Email is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#syUserEmailErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        if (syUserDept == null || syUserDept == "") {
            $("#syUserDept").focus();
            $("#syUserDeptErr").addClass("help-block with-errors").html("Department is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#syUserDeptErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }


        return true;
    }


</script>
<%    dbsession = HibernateUtil.getSessionFactory().openSession();

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add System User</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">User Management</a></li>
                    <li class="active">Add User</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-6"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="syUserAddSubmitData.jsp?sessionid=<%out.println(sessionid);%>&act=add" onSubmit="return fromDataSubmitValidation()">

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">User Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="syUserName" name="syUserName"  placeholder="User Name" class="form-control input-sm" required>
                                        <div  id="syUserNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>



                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Password</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="syUserPassword" name="syUserPassword" placeholder="Password" class="form-control input-sm" required>
                                        <div id="syUserPasswordErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Confirm Password</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="syUserPasswordConfirm" name="syUserPasswordConfirm" placeholder="Confirm Password" required>
                                        <div id="syUserPasswordConfirmErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="syUserFullName" name="syUserFullName" placeholder="Full name" required>
                                        <div id="syUserFullNameErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Mobile</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="syUserMobile" name="syUserMobile" placeholder="Mobile number" required>
                                        <div id="syUserMobileErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-3">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="syUserEmail" name="syUserEmail" placeholder="Email address" required>
                                        <div id="syUserEmailErr" class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-15x">
                                    <label class="control-label col-md-3">Department</label>
                                    <div class="col-md-9">
                                        <select class="form-control input-sm customInput-sm" style="height: 30px;" id="syUserDept" name="syUserDept">
                                            <option value="">SELECT</option>
                                            <%
                                                Query q1 = dbsession.createQuery("from SyDept dept order by deptName asc");
                                                for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                                                    SyDept dept = (SyDept) itr1.next();
                                            %> 
                                            <option value=" <% out.print(dept.getDeptId().trim());%> "> <% out.println(dept.getDeptName());%> </option> 
                                            <%
                                                }
                                                dbsession.close();
                                            %>

                                        </select>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="control-label col-sm-3"></label>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<% //out.print(username);%>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<% //out.print(hostname);%>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<% //out.print(ipAddress);%>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->

        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
        <!--Chosen JavaScript -->
        <script src="<% out.print(GlobalVariable.baseUrl);
            %>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>