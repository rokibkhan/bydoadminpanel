<%-- 
    Document   : scSupplierList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">




<%
    Session dbsession = null;
    String qryparam = "";
   

%>

<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<%
    String username = null;
    String logstat = null;
    String sessionid = null;
    sessionid = request.getParameter("sessionid").trim();




%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        

        <title>Role Detail: <% request.getParameter("rolename");%></title>
        <%




            String read = request.getParameter("read");
            String write = request.getParameter("write");
            /*
            out.println(username);
            out.println(read);
            out.println(write);
             */
        %>
    </head>
    <body>

        <%

            if (request.getParameter("roleid") != null) {
                qryparam = request.getParameter("roleid");
                String rolename = request.getParameter("rolename").toString();
        %>
        <h1>Role Permissions: <% out.println(rolename);%></h1>
        <br>
        <table class="tab1">
            <thead>
                <tr >
                    <th align="left" width="200"><b>Activity</b></th>
                    <!--                    <td align="left" width="20"><b>Allow Read?</b></td>
                                        <td align="left" width="20"><b>Allow Write?</b></td>-->
                    <th align="left" width="50"><b>Added By</b></th>
                    <th align="left" width="200"><b>Effective Date</b></th>
                    <th align="left" width="50"><b>Last Modified By</b></th>
                    <th align="left" width="200"><b>Last Modification Date</b></th>
                </tr>
            </thead>
                

                <%
                    dbsession = HibernateUtil.getSessionFactory().openSession();
                    org.hibernate.Transaction dbtrx = null;
                    dbtrx = dbsession.beginTransaction();

                    Query q1 = null;
                    String roleid = "";

                    String actid = "";
                    String actdesc = "";
                    String actread;
                    String actwrite;
                    String adduser = "";
                    Date adddate;
                    String moduser = "";
                    Date moddate;

                    int rownum = 0;
                    String oddeven = null;
                    getAbbr abbr = new getAbbr();

                    if (qryparam != "" && qryparam != null) {
                        //q1=dbsession.createQuery("select ra.id.actId,act.actDesc,ra.actRead,ra.actWrite,ra.addUsr,ra.addDate,ra.modUsrId,ra.lastModDate from SyRoleAct ra, SyActivity act where ra.id.actId=act.actId and ra.id.roleId='"+qryparam+"'");
                        q1 = dbsession.createQuery("from SyRoleAct ra, SyActivity act where ra.id.actId=act.actId and ra.id.roleId='" + qryparam + "'");
                    }

                    //List<SyRoleAct> resultList=q1.list();
                    //Iterator itr = resultList.iterator();

                    Iterator itr = q1.iterate();
                    while (itr.hasNext()) {
                        Object[] pair = (Object[]) itr.next();
                        SyRoleAct roleact = (SyRoleAct) pair[0];
                        SyActivity act = (SyActivity) pair[1];
                        //for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {

                        actid = roleact.getId().getActId();
                        actdesc = act.getActDesc();
                        rolename = request.getParameter("rolename").toString();
                        actread = roleact.getActRead();
                        actwrite = roleact.getActWrite();
                        adduser = roleact.getAddUsr();
                        adddate = roleact.getAddDate();
                        moduser = roleact.getModUsrId();
                        moddate = roleact.getLastModDate();
                        rownum += 1;

                %>
                
                <tr >
       
                    <td><% out.println(actdesc);%></td>
<!--                    <td align="center"> <% out.println(abbr.getLong(actread));%></td>
                    <td align="center"> <% out.println(abbr.getLong(actwrite));%></td>-->
                    <td> <% out.println(adduser);%></td>
                    <td> <% out.println(adddate);%></td>
                    <td> <% out.println(moduser);%></td>
                    <td> <% out.println(moddate);%></td>
                </tr>
                <%}
                    dbsession.close();
                %>

                  <tr >
                    <td colspan="5" align="right"><input type="button" name="back" value="Back" onclick="history.go(-1);"></td>
                </tr>
            
         </table>        
    </body>
    <%}%>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
