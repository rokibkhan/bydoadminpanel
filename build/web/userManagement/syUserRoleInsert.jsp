<%-- 
    Document   : dataInsert
    Created on : Nov 20, 2010, 4:53:05 AM
    Author     : Akter
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="java.net.InetAddress"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>

<%    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Role Insert</title>
    </head>
    <body>
        <h1>updating database, please wait ...</h1>
    </body>
</html>
<%
    String userid = request.getParameter("userid").trim().toUpperCase();
    String fullname = request.getParameter("fullname");
    String adduser = request.getParameter("adduser");
    String addterm = request.getParameter("addterm");
    String addip = request.getParameter("addip");

    String roleid[] = request.getParameterValues("roleid");
    String granted[] = request.getParameterValues("granted");

    boolean exists = false;

    //          out.println(request.getParameter("roleid"));
    getDate date = new getDate();

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q2 = dbsession.createQuery("from SyUserRole where id.userId ='" + userid + "'");
    SyUserRole userrole = null;
    SyUserRoleId userroleid = null;
    for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
        exists = true;
        userrole = (SyUserRole) itr2.next();
        dbsession.delete(userrole);
    }

    int i = 0;
    int j = 0;
//    dbtrx = dbsession.beginTransaction();
    for (i = 0; i < roleid.length; i++) {
        //if(granted[i].equals("Y")){
        userrole = new SyUserRole();
        userroleid = new SyUserRoleId(userid.trim(), roleid[i].trim());
        userrole.setId(userroleid);
        userrole.setAddDate(date.serverDate());
        userrole.setAddUsr(adduser.trim());
        userrole.setAddTerm(addterm.trim());
        userrole.setAddIp(addip.trim());
        userrole.setModUsrId(adduser.trim());
        userrole.setModTerm(addterm.trim());
        userrole.setModIp(addip.trim());
        userrole.setLastModDate(date.serverDate());
        dbsession.save(userrole);
        //}
    }

    SyAuditTrail audit = new SyAuditTrail();
    getRegistryID regId = new getRegistryID();
    getDate gdate = new getDate();
    String eventid = "";
    eventid = regId.getID(16);
    BigDecimal eventId = new BigDecimal(eventid);

    audit.setEvtId(eventId);

    audit.setEvtDatetime(gdate.serverDate());
    audit.setEvtEmpId(username);
    if (exists = !true) {
        audit.setEvtCode("ROLE-ADD");
        audit.setEvtDetail("Audit for ROLE CREATION");
    } else {
        audit.setEvtCode("ROLE-EDIT");
        audit.setEvtDetail("Audit for ROLE-edit");

    }
    audit.setEvtRef01("");
    audit.setEvtRef02("");
    audit.setEvtRef03("");
    audit.setEvtRef04("");
    audit.setEvtRef05(addterm + "," + addip);
    dbsession.save(audit);

    dbsession.flush();
    dbtrx.commit();
    dbsession.close();

    if (dbtrx.wasCommitted()) {
        response.sendRedirect("success.jsp?sessionid=" + sessionid);
    } else {
        dbtrx.rollback();
        response.sendRedirect("fail.jsp?sessionid=" + sessionid);
    }

%>
