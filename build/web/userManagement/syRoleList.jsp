

<%@page import="com.appul.entity.SyRoles"%>
<%@page import="java.util.Date"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">



        //     $(function(){
        function showRoleActivityInfo(arg1, arg2, arg3) {
            console.log("showRoleActivityInfo :: " + arg1);

            $("#btnShowRoleActivityInfo_" + arg1).button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Role Activity List");

            $.post("syRoleActivityDetails.jsp", {roleID: arg1, roleName: arg2, sessionid: arg3}, function (data) {

                console.log(data);
                // rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.find(".modal-dialog").css("margin-top", "180px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].selectRoleDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   $("#btnShowRoleActivityInfo_"+data[0].selectRoleId).button("reset");
                $("#btnShowRoleActivityInfo_" + arg1).button("reset");
                rupantorLGModal.modal('show');
//
            }, "json");
        }

        function showUpdateActivityOption(arg1, arg2, arg3) {
            console.log("showUpdateActivityOption :: " + arg1);

            $("#btnShowUpdateActivityOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Update Activity Info");

            $.post("syRoleActivityUpdateOptionShow.jsp", {roleID: arg1, roleName: arg2, sessionid: arg3}, function (data) {

                console.log(data);
                // rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.find(".modal-dialog").css("margin-top", "180px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].selectRoleDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //     rupantorLGModal.find("#rupantorLGModalFooter").html('');
                //   rupantorLGModal.find(".modal-footer").css({"padding": "0", "border-top": "none"});
                rupantorLGModal.modal('show');
                $("#btnShowUpdateActivityOption").button("reset");
//
            }, "json");


        }


        function updateActivityOptionSubmit(arg1, arg2, arg3) {
            console.log("showRoleActivityInfo :: " + arg1);

            var roleID, roleName, updatedActivityList;


            roleID = $("#roleId").val();
            roleName = $("#roleName").val();
            updatedActivityList = $("#public-methods").val();


            console.log("roleID:: " + roleID);
            console.log("roleName:: " + roleName);

            console.log(" updatedActivityList:: " + updatedActivityList);

            $("#btnUpdateActivityOptionSubmit").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Role Activity List");

            $.post("syRoleActivityUpdatedOption.jsp", {'roleID': arg1, 'roleName': arg2, 'sessionid': arg3, 'updatedActivityList[]': updatedActivityList}, function (data) {

                console.log(data);
                // rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.find(".modal-dialog").css("margin-top", "180px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].selectRoleDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find("#showMsgActvitityOpt").html(data[0].responseMsg);
                rupantorLGModal.find(".alert").css("margin-bottom", "0");

                //      rupantorLGModal.find("#globalAlertInfoBoxConTT").html(data[0].responseMsg).show().delay(3000).fadeOut("slow");
                rupantorLGModal.modal('show');
                $("#btnShowRoleActivityInfo").button("reset");
//
            }, "json");
        }





    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Roles List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">User Management</a></li>                    
                    <li class="active">Roles</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Role ID</th>
                                    <th>Role Name</th>
                                    <th class="text-center">Created By</th>
                                    <th class="text-center">Created On</th>
                                    <th class="text-center">Last Modified By</th>
                                    <th class="text-center">Last Modified On</th>

                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>


                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();
                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    Query q1 = null;
                                    String roleId = "";
                                    String roleName = "";
                                    String adduser = "";
                                    Date adddate;
                                    String moduser = "";
                                    Date moddate;

                                    SyRoles role = null;
                                    int rownum = 0;
                                    String oddeven = null;

                                    q1 = dbsession.createQuery("from SyRoles as syroles order by roleDesc ASC");

                                    for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                                        role = (SyRoles) itr1.next();
                                        roleId = role.getRoleId();
                                        roleName = role.getRoleDesc();
                                        adddate = role.getAddDate();
                                        adduser = role.getAddUsr();
                                        moddate = role.getLastModDate();
                                        moduser = role.getModUsrId();
                                        rownum += 1;

                                        String updateActivityUrl = GlobalVariable.baseUrl + "/userManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&roleIdX=" + roleId;
                                        // dbtrx.commit();
                                        //dbsession.close();
                                %>

                                <tr >
                                    <td><% out.print(ix);%></td>
                                    <td> <% out.print(roleId);%></td>
                                    <td> <% out.print(roleName);%></td>
                                    <td> <% out.print(adduser);%></td>
                                    <td> <% out.print(adddate);%></td>
                                    <td> <% out.print(moduser);%></td>
                                    <td> <% out.print(moddate);%></td>

                                    <td class="text-center">
                                        <a id="btnShowInvoiceDetails<% //out.print(userId);%>" onclick="showInvoiceDetails(<% //out.print(userId + ",'" + sessionid + "'");%>);" title="Invoice details" class="btn btn-primary btn-sm"><i class="icon-info"></i></a>
                                        <a onclick="changeInvoiceStatus('2');" title="Change invoice status" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a id="btnShowRoleActivityInfo_<% out.print(roleId);%>" onclick="showRoleActivityInfo('<% out.print(roleId);%>', '<%out.print(roleName);%>', '<% out.print(session.getId());%>')" title="Add/Remove Activity Info" class="btn btn-primary btn-sm"><i class="fa fa-fort-awesome"></i></a>
                                    </td>
                                </tr>

                                <%
                                        ix++;

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                    %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>