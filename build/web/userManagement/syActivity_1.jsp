<%-- 
    Document   : syRoleCheck
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>


<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="jquery/1.4/jquery.min.js"></script>
<script src="jquery/jqueryui/jquery-ui.min.js"></script>
<script src="jquery/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery.autocomplete.js"></script>


<link rel="stylesheet" type="text/css" href="css/stylesheet.css">


<script type="text/javascript">
    
    
    <%
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        // Query q1=dbsession.createQuery("insert into SyRoles(roleId,roleDesc,addDate,addUsr,addTerm,lastModDate) values('test')");

    %>


</script>


<%
    String username = null;
    String logstat = null;
    String sessionid = null;
    sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }

    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }
    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }


%>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self ==top)
                    window.open("logout.jsp","_top","");
            }

            if (window.history) {
                window.history.forward(1);
            }
        </script>


        <title>Activities</title>
        <%


            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }

            //                   out.println(username);
            //                   out.println(logstat);
            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                // return;
            }
        %>
    </head>
    <body onLoad="breakOut()">


        <h1>Activities</h1>
        <form name="frmRoleAct" id="frmRoleAct" method="post" action="syRoleInsert.jsp?sessionid=<%out.println(sessionid);%>"> <%-- onSubmit="return dataInsert()" --%>
      

            <table class="tab1">
                <thead>  
                <tr class="d0">
                    <th width="200"><b>Activity ID</b></th>
                    <th><b>Activity Description</b></th>
                    <!--                    <th><b>Read?</b></th>
                                        <th><b>Write?</b></th>-->
                </tr>
                </thead>
                <%
                    int i = 1;
                    int j = 0;

                    String fieldname = null;
                    String readfield = null;
                    String writefield = null;
                    String titlefield = null;
                    String readval = null;
                    String writeval = null;
                    SyActivity syActivity = null;
                    String actid = null;
                    String actdesc = null;
                    int rownum = 0;
                    // String actdescString[] = request.getParameterValues("SecondList");
                    // String actId[] = request.getParameterValues("SecondList");
                    // int actdesc = actdescString.length;
                    // out.println(actdescString);
                    // for (int i = 0; i < actId.length; i++) {
                    Query q = dbsession.createQuery("from SyActivity as a order by a.actId asc");
                    for (Iterator itr = q.list().iterator(); itr.hasNext();) {
                        rownum += 1;
                        syActivity = (SyActivity) itr.next();
                        //   actid = syActivity.getActId();
                        actid = syActivity.getActId();
                        actdesc = syActivity.getActDesc();


                %>

                <tr >
                    <td><input type="hidden" id="actid" name="actid" value="<% out.println(actid);%>"><% out.println(actid);%></td>
                    <td><input type="hidden" id="actdesc" name="actdesc" value="<% out.println(actdesc);%>"><% out.println(actdesc);%></td>
                </tr>
                <%
                    }
                    dbsession.close();

                %>
            </table>
        </form>
    </body>
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="NO-STORE">
</html>
