<%-- 
    Document   : syRoleList
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%

    //out.println(request.getSession(false));
    request.getSession(false);
//out.println(request.getSession(false));
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;

%>

<%    Session dbsession = null;
    String qryparam = "";
    /*            out.println("(" + session.getId() + ")"); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }
    String pagename = this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().indexOf("_"));

%>

<!--<link rel="stylesheet" type="text/css" href="css/stylesheet.css">-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/Style.css">
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp", "_top", "");
            }
        </script>

        <title>Roles</title>
        <%            String username = null;
            String logstat = null;
            String sessionid = null;
            sessionid = request.getParameter("sessionid").trim();

//                    out.println(session.getId());
//                    out.println(sessionid);
//out.println(request.getSession(false));
            if (sessionid == null) {
                response.sendRedirect("login.jsp");
                return;
            }

            if (request.getSession(false) == null) {
                response.sendRedirect("login.jsp");
                return;
            }

            if (session.getId() == null) {
                response.sendRedirect("login.jsp");
                return;
            }

            if (sessionid.equals(session.getId()) == false) {
                return;
            }

            if (session.getAttribute("username") != null) {
                username = session.getAttribute("username").toString().toUpperCase();
            }
            if (session.getAttribute("logstat") != null) {
                logstat = session.getAttribute("logstat").toString();
            }

            if (username == null || logstat.equals("Y") == false) {
                response.sendRedirect("logout.jsp");
                return;
            }

            String read = request.getParameter("read");
            String write = request.getParameter("write");
        %>
    </head>
    <body onLoad="breakOut()">
        <div style="clear:both;">&nbsp;</div>
        <div class="container">
            <div class="body-content">
                <h4> Role List ::
                    <% if (read.trim().equals("Y")) {%>
                    <a href="syRoleAdd.jsp?sessionid=<%out.println(sessionid);%>"><img src="image/add_icon.png" alt="Add New Company" border="0" style=" padding: 0px 6px 0px 20px !important;line-height: 0; height:20px;"></a></h4>
                        <%                                    }
                        %>

                <hr />



                <table id="tblContact" class="table table-striped table-bordered" cellspacing="0" style="width:100%;">

            <thead>
                <tr >
                    <th align="left"><b>Role ID</b></th>
                    <th align="left"><b>Role Name</b></th>
                    <th align="left"><b>Created By</b></th>
                    <th align="left"><b>Created On</b></th>                    
                    <th align="left"><b>Last Modified By</b></th>
                    <th align="left"><b>Last Modified On</b></th>                    
                    <th align="left"><b></b></th>
                </tr>
            </thead>
            <%
                dbsession = HibernateUtil.getSessionFactory().openSession();
                org.hibernate.Transaction dbtrx = null;
                dbtrx = dbsession.beginTransaction();

                Query q1 = null;
                String roleId = "";
                String roleName = "";
                String adduser = "";
                Date adddate;
                String moduser = "";
                Date moddate;

                SyRoles role = null;
                int rownum = 0;
                String oddeven = null;

                if (qryparam != "" && qryparam != null) {
                    q1 = dbsession.createQuery("from SyRoles as syroles where upper(roleDesc) like upper('%" + qryparam + "%') or upper(roleId) like upper('%" + qryparam + "%') order by roleDesc asc");
                } else {
                    q1 = dbsession.createQuery("from SyRoles as syroles order by roleDesc asc");
                }

                for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                    role = (SyRoles) itr1.next();
                    roleId = role.getRoleId();
                    roleName = role.getRoleDesc();
                    adddate = role.getAddDate();
                    adduser = role.getAddUsr();
                    moddate = role.getLastModDate();
                    moduser = role.getModUsrId();
                    rownum += 1;

                    if ((rownum % 2) == 0) {
                        oddeven = "even";
                    } else {
                        oddeven = "odd";
                    }
                    // dbtrx.commit();
                    //dbsession.close();
            %>

            <tr >
                <td> <% out.println(roleId);%></td>
                <td> <% out.println(roleName);%></td>
                <td> <% out.println(adduser);%></td>
                <td> <% out.println(adddate);%></td>
                <td> <% out.println(moduser);%></td>
                <td> <% out.println(moddate);%></td>

                <td width="100">
        

                    <% if (read.trim().equals("Y")) {%>
                    <a href="syRoleDetail.jsp?roleid=<%out.println(roleId);%>&rolename=<%out.println(roleName);%>&sessionid=<%out.println(sessionid);%>">Detail</a>
                    <%}
                        if (write.trim().equals("Y")) {%>
                    <a href="syRoleEdit.jsp?roleid=<%out.println(roleId);%>&rolename=<%out.println(roleName);%>&sessionid=<%out.println(sessionid);%>">Edit</a>

                    <%                                                        }
                    %>

                </td>
            </tr>
            <%
                }
                dbsession.flush();
                dbsession.close();
            %>

        </table>
    </div>
    <!--<hr/>-->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tblContact').dataTable({
            "iDisplayLength": 10,
            "lengthMenu": [5, 10, 25, 50, 100]
        });
    });
</script>
</body>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<META HTTP-EQUIV="NO-STORE">
</html>
