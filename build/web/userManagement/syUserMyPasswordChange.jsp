
<%@page import="com.appul.entity.ProductSize"%>
<%@page import="com.appul.entity.GlCode"%>
<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>




<script src="../js/jquery.autocomplete.js"></script>
<link rel="stylesheet" type="text/css" href="../css/jquery.autocomplete.css" />
<script src="../jquery/jquery-ui.js"></script>
<link rel="stylesheet" href="../jquery/jquery-ui.css" />


<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">




<script>
    function ProductCodeAutocomplete() {
        //  var form = document.getElementById("stockAdd");

        //  alert("OK");
        var name;

        name = 'productId';
        $('#productId').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "productCodeAutocomplete.jsp",
                    type: "POST",
                    dataType: "json",
                    data: {name: request.term},
                    success: function (data) {

                        console.log(data);
                        response($.map(data, function (item) {
                            return {
                                label: item.name,
                                value: item.value,
                            }
                        }));
                    },
                    error: function (error) {
                        alert('error: ' + error);
                    }
                });
            },
            minLength: 2
        });
//                }
    }


    function selectedProductInfoForUOM(productCode) {
        var sproduct, res;
        sproduct = productCode.split('|');
//        alert(productCode);
        if (sproduct != productCode) {

            $("#quantity1UomId").val(sproduct[3]);
            $("#quantity1-addon").html(sproduct[4]);


            $("#quantity2UomId").val(sproduct[6]);
            $("#quantity2-addon").html(sproduct[7]);
            $("#buying-price-addon").html(sproduct[7]);
            $("#exchangeUom").val(sproduct[5]);

        } else {
            $("#quantity1-addon").html('');
            $("#quantity2-addon").html('');
            $("#buying-price-addon").html('');
//            $("#exchangeUom").val('');
        }
    }

    function getQuantity2(arg1) {

        var form = document.getElementById("stockAdd");
        var quantity1, exchangeUom;

        quantity1 = form.quantity1.value;
        exchangeUom = form.exchangeUom.value;
        if (arg1 != "") {

            form.quantity2.value = parseFloat((quantity1 * exchangeUom)).toFixed(2);


        }


    }

</script>
<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT,msgInfoText,sLinkOpt;
    if(!strMsg.equals("")){
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>"+strMsg+"</strong> ";
        
    }else{
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);
%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Change Password</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">User Management</a></li>        
                    <li class="active">Change Password</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
             <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
<!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                        <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-md-6"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="changePassword" id="changePassword" method="POST" action="syUserMyPasswordChangeUpdate.jsp?sessionid=<%out.println(sessionid);%>&act=add" onSubmit="return dataValidate()">

                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-4">Old Password</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-sm" id="oldPassword" name="oldPassword" placeholder="Old password" required onkeypress="ProductCodeAutocomplete()" onfocusout="selectedProductInfoForUOM(this.value)">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>



                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-4">New Password</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-sm" id="newPassword" name="newPassword" placeholder="New password" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="inputName4" class="control-label col-sm-4">Retype New Password</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control input-sm" id="newPassword1" name="newPassword1" placeholder="Retype new password" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                               

                                <div class="form-group row">
                                    <label class="control-label col-sm-4"></label>
                                    <div class="col-sm-offset-3 col-sm-8">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->

     

    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>