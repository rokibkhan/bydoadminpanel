<%@page import="java.util.Date"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.*"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    dbsession = HibernateUtil.getSessionFactory().openSession();
    dbtrx = dbsession.beginTransaction();

    //  sessionIdH = session.getId();
    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();

    String showTabOption;
    if (regStep.equals("121")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String gender1 = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureName = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    String membershipApplyingFor = "";
    String membershipApplyingFor1 = "";
    String presentIEBmembershipNumber = "";
    String divisionId = "";
    String subDivisionId = "";
    String memberTempId = "";
    String memberTempPass = "";

    q1 = dbsession.createSQLQuery("select * from member_temp WHERE id= " + regMemId + "");
    for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        memberName = object[4].toString();
        fatherName = object[5] == null ? "" : object[5].toString();
        motherName = object[6] == null ? "" : object[6].toString();
        placeOfBirth = object[7].toString();
        datefBirth = object[8].toString();
        age = object[9] == null ? "" : object[9].toString();
        nationality = object[10] == null ? "" : object[10].toString();
        gender1 = object[11].toString();
        if (gender1.equals("M")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        membershipApplyingFor1 = object[13] == null ? "" : object[13].toString();
        if (membershipApplyingFor1.equals("1")) {
            membershipApplyingFor = " Fellow ";
        } else if (membershipApplyingFor1.equals("2")) {
            membershipApplyingFor = "Member";
        } else {
            membershipApplyingFor = "Associate Member";
        }
        presentIEBmembershipNumber = object[14] == null ? "" : object[14].toString();

        phone1 = object[15] == null ? "" : object[15].toString();
        phone2 = object[16] == null ? "" : object[16].toString();
        mobileNumber = object[17] == null ? "" : object[17].toString();
        userEmail = object[18].toString();

        divisionId = object[24].toString() == null ? "" : object[24].toString();
        // subDivisionId = object[28].toString() == null ? "" : object[28].toString();

        memberTempId = object[2].toString() == null ? "" : object[2].toString();
        memberTempPass = object[3].toString() == null ? "" : object[3].toString();

    }



%>


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about1.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>





<!-- Page Content -->
<div id="page-wrapper">

    <script type="text/javascript">

        function printInvoiceInfo(arg1) {
            var divToPrint = document.getElementById("divToPrint" + arg1);
            var popupWin = window.open('', '_blank', 'width=500,height=600');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
            popupWin.document.close();
        }
    </script>
    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Temp Member Info</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Temp Member Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                     
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">


                    <div class="row">
                        <div class="col-md-12">
                            <div id="accordion">
                                <div class="card">


                                    <p style="padding-top: 10px; padding-left: 10px;">Temp Member application complete. Please check <a target="_blank" href="<%=GlobalVariable.baseUrl%>/memberManagement/tempMemberList.jsp?sessionid=<%=session.getId()%>"><strong>Temp Member List</strong></a> and Check Add Others Info button for others info.</p>

                                    <p style="padding-top: 10px; padding-left: 10px;">Temp Member application ID: <strong><%=memberTempId%></strong></p>
                                    <p style="padding-left: 10px;">Temp Member Password: <strong><%=memberTempPass%></strong></p>






                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 1: Personal Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOne" class="collapse in" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="#">
                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-6">
                                                        <label for="inputName">Name:</label>
                                                        <%=memberName%>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="inputName">Father Name:</label>
                                                        <%=fatherName%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-6">
                                                        <label for="inputName">Mother Name:</label>
                                                        <%=motherName%>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="inputName">Gender:</label>
                                                        <%=gender%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-6">
                                                        <label for="inputDOB">Date of Birth:</label>
                                                        <%=datefBirth%>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="inputAge">Age:</label>
                                                        <%=age%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-6">
                                                        <label for="inputPOB">Place of Birth:</label>
                                                        <%=placeOfBirth%>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="inputAge">Nationality:</label>
                                                        <%=nationality%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-6">
                                                        <label for="inputPOB">Office Phone:</label>
                                                        <%=phone1%>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="inputAge">Residence Phone:</label>
                                                        <%=phone2%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-6">
                                                        <label for="inputPOB">Mobile:</label>
                                                        <%=mobileNumber%>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="inputAge">Email:</label>
                                                        <%=userEmail%>
                                                    </div>
                                                </div>







                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-12">
                                                        <label for="inputName">Membership Applying For:</label>
                                                        <%=membershipApplyingFor%>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputName">Present IEB Membership Number:</span></label>
                                                        <%=presentIEBmembershipNumber%>
                                                    </div>
                                                </div>




                                            </form>
                                        </div>
                                    </div>
                                </div>




                                <div class="card">
                                    <div class="card-header" id="headingFourRegFee">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRegFee" aria-expanded="false" aria-controls="collapseFourRegFee">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 2: Registration Fee Payment Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRegFee" class="collapse  in" aria-labelledby="headingFourRecom" data-parent="#accordion">
                                        <div class="card-body">


                                            <%
                                                //   MemberFeeTemp mpi = null;

                                                Query feeSQL = null;
                                                Object[] feeObject = null;

                                                String memberShipFeeTransId = "";
                                                String memberShipFeeYear = "";
                                                String memberShipFeeBillType = "";
                                                String memberShipFeeMemberId = "";
                                                String memberShipFeePaidDate = "";
                                                String memberShipFeeAmount = "";
                                                String memberShipFeeRefNo = "";
                                                String memberShipFeeRefDesc = "";
                                                String memberShipFeeStatus = "";
                                                String memberShipFeeStatus1 = "";
                                                String memberShipFeeCurrency = "BDT";

                                                // feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
                                                //  feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE member_id= '" + regMemId + "'");
                                                feeSQL = dbsession.createSQLQuery("SELECT mf.txn_id,mf.member_id,mf.ref_no,mf.ref_description, "
                                                        + "mf.txn_date,mf.paid_date,mf.fee_year,mf.amount,mf.status,mf.due_date,mf.bill_type,"
                                                        + "mfy.member_fees_year_name  "
                                                        + "FROM member_fee_temp mf,member_fees_year mfy WHERE mf.fee_year = mfy.member_fees_year_id AND mf.member_id= '" + regMemId + "'");


                                            %>
                                            <div class="row">
                                                <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Txn No.</th>
                                                            <th scope="col">Payment Date</th>
                                                            <th scope="col">Amount</th>
                                                            <th scope="col">Status</th> 
                                                            <th scope="col">Payment Info</th>
                                                            <th scope="col">View Bill</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <%                                                                            if (!feeSQL.list().isEmpty()) {
                                                                int ip = 1;
                                                                String divToPrint = "";
                                                                for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                                                                    feeObject = (Object[]) feeItr.next();
                                                                    memberShipFeeTransId = feeObject[0].toString();
                                                                    memberShipFeeMemberId = feeObject[1].toString();
                                                                    memberShipFeeRefNo = feeObject[2] == null ? "" : feeObject[2].toString();
                                                                    memberShipFeeRefDesc = feeObject[3] == null ? "" : feeObject[3].toString();
                                                                    memberShipFeePaidDate = feeObject[5] == null ? "" : feeObject[5].toString();
                                                                    memberShipFeeAmount = feeObject[7] == null ? "" : feeObject[7].toString();
                                                                    memberShipFeeStatus = feeObject[8] == null ? "" : feeObject[8].toString();

                                                                    if (memberShipFeeStatus.equals("1")) {
                                                                        memberShipFeeStatus1 = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
                                                                    } else {
                                                                        memberShipFeeStatus1 = "<span class=\"btn btn-danger btn-sm waves-effect\">Fail</span>";

                                                                    }

                                                                    memberShipFeeBillType = feeObject[10] == null ? "" : feeObject[10].toString().trim();
                                                                    memberShipFeeYear = feeObject[11].toString();

                                                                    divToPrint = "<div id=\"divToPrint" + memberShipFeeTransId + "\" style=\"display:none;\"><!-- Start:  Print Container  -->"
                                                                            + "<html>"
                                                                            + "<head><title>Bill Invoice Print</title></head>"
                                                                            + "<body>"
                                                                            + "<div style=\"width: 790px; margin: 0px auto; margin-top: 1.5cm;\">"
                                                                            + "<table style=\"width: 95%; margin-bottom: 5px;\">"
                                                                            + "<tr>"
                                                                            + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                                            + "<td style=\"padding: 5px; text-align: right;\">"
                                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">Office Copy</span>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">"
                                                                            + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\">"
                                                                            + "</td>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                                            + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                                            + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                                            + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">&nbsp;</td>"
                                                                            + "<td style=\"text-align: center;\">"
                                                                            + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span>"
                                                                            + "</td>"
                                                                            + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td style=\"text-align: center;\">"
                                                                            + "<table>"
                                                                            + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + memberName + "</td></tr>"
                                                                            + "<tr><td style=\"text-align: left;\"><strong>Member ID </strong></td><td>:" + memberTempId + "</td></tr>"
                                                                            + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + mobileNumber + "</td></tr>"
                                                                            + "</table>"
                                                                            + "</td>"
                                                                            + "<td style=\"text-align: center;\">"
                                                                            + "<table>"
                                                                            + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeePaidDate + "</td></tr>"
                                                                            + "<tr><td><strong>Invoice No</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                                                                            + "<tr><td><strong>Status</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                                                                            + "</table>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<thead>"
                                                                            + "<tr>"
                                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                                            + "</tr>"
                                                                            + "</thead>"
                                                                            + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                                            + "<tr>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeYear + "</td>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeBillType + "</td>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                                            + "</tr>"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                                            + "</tr>"
                                                                            + "</tbody>"
                                                                            + "</table> <!--End : invoice product list details-->"
                                                                            + "<table style=\"width: 95%; border-top:1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">This is computer generated invoice no signature required.</span>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; border-top:1px dotted #ccc; border-collapse: collapse; margin-top: 30px;;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; margin-bottom: 5px;\">"
                                                                            + "<tr>"
                                                                            + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                                            + "<td style=\"padding: 5px; text-align: right;\">"
                                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">Member Copy</span>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">"
                                                                            + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\">"
                                                                            + "</td>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                                            + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                                            + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                                            + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">&nbsp;</td>"
                                                                            + "<td style=\"text-align: center;\">"
                                                                            + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span>"
                                                                            + "</td>"
                                                                            + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td style=\"text-align: center;\">"
                                                                            + "<table>"
                                                                            + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + memberName + "</td></tr>"
                                                                            + "<tr><td style=\"text-align: left;\"><strong>Member ID </strong></td><td>:" + memberTempId + "</td></tr>"
                                                                            + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + mobileNumber + "</td></tr>"
                                                                            + "</table>"
                                                                            + "</td>"
                                                                            + "<td style=\"text-align: center;\">"
                                                                            + "<table>"
                                                                            + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeePaidDate + "</td></tr>"
                                                                            + "<tr><td><strong>Invoice No</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                                                                            + "<tr><td><strong>Status</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                                                                            + "</table>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<thead>"
                                                                            + "<tr>"
                                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                                            + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                                            + "</tr>"
                                                                            + "</thead>"
                                                                            + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                                            + "<tr>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeYear + "</td>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeBillType + "</td>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                                            + "</tr>"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                                            + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                                            + "</tr>"
                                                                            + "</tbody>"
                                                                            + "</table> <!--End : invoice product list details-->"
                                                                            + "<table style=\"width: 95%; border-top:1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                                            + "<tr>"
                                                                            + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                                            + "<span style=\"font-size: 12px; font-weight: bold;\">This is computer generated invoice no signature required.</span>"
                                                                            + "</td>"
                                                                            + "</tr>"
                                                                            + "</table>"
                                                                            + "</div>"
                                                                            + "</body>"
                                                                            + "</html>"
                                                                            + "</div><!-- End:  Print Container  -->";


                                                        %>
                                                        <tr>
                                                            <td><%=ip%></td>
                                                            <td><%=memberShipFeeTransId%></td>
                                                            <td><%=memberShipFeePaidDate%></td>
                                                            <td><%=memberShipFeeAmount%></td> 
                                                            <td><%=memberShipFeeStatus1%></td> 
                                                            <td><%=memberShipFeeRefDesc%></td> 
                                                            <td>
                                                                <a onclick="printInvoiceInfo('<%=memberShipFeeTransId%>');"  class="btn btn-primary btn-sm">&nbsp; <i class="fa fa-file"></i>&nbsp; </a>

                                                                <%=divToPrint%>

                                                            </td> 

                                                        </tr>

                                                        <%
                                                                    ip++;
                                                                }
                                                            }
                                                        %>
                                                    </tbody>
                                                </table>

                                            </div>  






                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>





                </div>
                <!-- /.white-box -->
            </div>

        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
    %>

    <%@ include file="../footer.jsp" %>                               