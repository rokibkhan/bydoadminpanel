<%@page import="java.util.Date"%>
<%@page import="com.appul.entity.AddressBook"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>






<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();

    String showTabOption;
    if (regStep.equals("5")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    System.out.println("Header sessionIdH :: " + sessionIdH);
    System.out.println("Header userNameH :: " + userNameH);

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";

    String membershipApplyingFor = "";
    String membershipApplyingFor1 = "";
    String presentIEBmembershipNumber = "";
    String pictureName = "";
    String pictureLink = "";

    q1 = dbsession.createSQLQuery("select * from member_temp WHERE id= " + regMemId + "");
    for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        memberName = object[4].toString();
        fatherName = object[5] == null ? "" : object[5].toString();
        motherName = object[6] == null ? "" : object[6].toString();
        placeOfBirth = object[7].toString();
        datefBirth = object[8].toString();
        age = object[9] == null ? "" : object[9].toString();
        nationality = object[10] == null ? "" : object[10].toString();
        gender = object[11].toString();

        membershipApplyingFor1 = object[13] == null ? "" : object[13].toString();
        if (membershipApplyingFor1.equals("1")) {
            membershipApplyingFor = " Fellow ";
        } else if (membershipApplyingFor1.equals("2")) {
            membershipApplyingFor = "Member";
        } else {
            membershipApplyingFor = "Associate Member";
        }

        presentIEBmembershipNumber = object[13] == null ? "" : object[13].toString();

        phone1 = object[14] == null ? "" : object[14].toString();
        phone2 = object[15] == null ? "" : object[15].toString();
        mobileNumber = object[17] == null ? "" : object[17].toString();
        userEmail = object[18].toString();
        pictureName = object[25].toString();

        pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

    }


%>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about1.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>


<script type="text/javascript">

    
    function registrationEducationInfoSubjectShow(arg1) {

        console.log("registrationEducationInfoSubjectShow:: " + arg1);

        var memberBSCBoardUniversity = document.getElementById("memberBSCBoardUniversity").value;

        if (memberBSCBoardUniversity != '') {

            $.post("memberAddEducationInfoUniversitySubjectShow.jsp", {universityId: memberBSCBoardUniversity}, function (data) {

                if (data[0].responseCode == 1) {
                    //  $("#showThanaInfo" + data[0].addressType).html(data[0].thanaInfo);
                    $("#subjectCon").html(data[0].subjectInfo);
                }

            }, "json");

        }
    }


</script>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Member Info</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Member Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <div class="row">
                        <div class="col-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-check-circle fa-lg"></i> <span class="h4">Section 1: Personal Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal" method="POST" autocomplete="off" name="frmLogin" action="#">
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputName">Name:</label>
                                                        <%=memberName%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputName">Father Name:</label>
                                                        <%=fatherName%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputName">Mother Name:</label>
                                                        <%=motherName%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputName">Gender:</label>
                                                        <%=gender%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputDOB">Date of Birth:</label>
                                                        <%=datefBirth%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Age:</label>
                                                        <%=age%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Place of Birth:</label>
                                                        <%=placeOfBirth%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Nationality:</label>
                                                        <%=nationality%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Office Phone:</label>
                                                        <%=phone1%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Residence Phone:</label>
                                                        <%=phone2%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Mobile:</label>
                                                        <%=mobileNumber%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Email:</label>
                                                        <%=userEmail%>
                                                    </div>
                                                </div>







                                                <div class="form-row form-group m-0">
                                                    <div class="col-12">
                                                        <label for="inputName">Membership Applying For:</label>
                                                        <%=membershipApplyingFor%>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-12">
                                                        <label for="inputName">Present IEB Membership Number:</span></label>
                                                        <%=presentIEBmembershipNumber%>
                                                    </div>
                                                </div>




                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFourMyPicture">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseMyPicture" aria-expanded="false" aria-controls="collapseMyPicture">
                                                <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 2: My Picture Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseMyPicture" class="collapse" aria-labelledby="headingMyPicture" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationMyPictureUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-10 offset-1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">My Picture</label>


                                                            <div class="col-4">
                                                                <%=pictureLink%>
                                                            </div>
                                                            <div class="col-4">
                                                                &nbsp;
                                                            </div>
                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .JPGE and .PNG files allow and file not more then 5MB.</p>
                                                    </div>
                                                </div>



                                            </form>

                                        </div>
                                    </div>
                                </div>  
                                <div class="card">
                                    <div class="card-header" id="headingOneAddressInfo">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneAddressInfo" aria-expanded="true" aria-controls="collapseOneAddressInfo">
                                                <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 3: Address Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOneAddressInfo" class="collapse" aria-labelledby="headingOneAddressInfo" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationAddressInfoSubmitData.jsp">

                                                <div class="row">


                                                    <%
                                                        Query madrSQL = null;
                                                        Object[] madrObject = null;

                                                        Query padrSQL = null;
                                                        Object[] padrObject = null;

                                                        int mAddressId = 0;
                                                        String mCountryId = "";
                                                        String mDistrictId = "";
                                                        String mThanaId = "";
                                                        String mThanaName = "";
                                                        String mThanaDistrictName = "";
                                                        String mAddressLine1 = "";
                                                        String mAddressLine2 = "";
                                                        String mZipOffice = "";
                                                        String mZipCode = "";

                                                        int pAddressId = 0;
                                                        String pCountryId = "";
                                                        String pDistrictId = "";
                                                        String pThanaId = "";
                                                        String pThanaName = "";
                                                        String pThanaDistrictName = "";
                                                        String pAddressLine1 = "";
                                                        String pAddressLine2 = "";
                                                        String pZipOffice = "";
                                                        String pZipCode = "";

                                                        String mAddressStr = "";
                                                        String pAddressStr = "";

                                                        Thana thana = null;

                                                        Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='M' ) ");
                                                        for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
                                                            madrObject = (Object[]) itr2.next();
                                                            mAddressLine1 = madrObject[1].toString();
                                                            mAddressLine2 = madrObject[2].toString();
                                                            mThanaId = madrObject[3].toString();

                                                            Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                                            for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
                                                                thana = (Thana) itrmThana.next();

                                                                mThanaName = thana.getThanaName();
                                                                mThanaDistrictName = thana.getDistrict().getDistrictName();
                                                            }

                                                            mZipCode = madrObject[4].toString();
                                                            mZipOffice = madrObject[8].toString();

                                                            mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

                                                        }
                                                        Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='P' ) ");
                                                        for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

                                                            padrObject = (Object[]) itr3.next();
                                                            pAddressLine1 = padrObject[1].toString();
                                                            pAddressLine2 = padrObject[2].toString();
                                                            pThanaId = padrObject[3].toString();

                                                            Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                                            for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
                                                                thana = (Thana) itrpThana.next();

                                                                pThanaName = thana.getThanaName();
                                                                pThanaDistrictName = thana.getDistrict().getDistrictName();
                                                            }

                                                            pZipCode = padrObject[4].toString();
                                                            pZipOffice = padrObject[8].toString();

                                                            pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

                                                        }

                                                    %>


                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Mailing Address</p>
                                                        <address class="m-2 text-center">
                                                            <%=mAddressStr%>
                                                        </address>

                                                    </div>
                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Permanent Address</p>

                                                        <address class="m-2 text-center">
                                                            <%=pAddressStr%>
                                                        </address>

                                                    </div>
                                                </div>




                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <span class="h4">Section 4: Education Information</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse <%=showTabOption%>" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">

                                            <%

                                                Object[] educationBoardObj = null;
                                                String educationBoardId = "";
                                                String educationBoardShortName = "";
                                                String educationBoardLongName = "";
                                                String educationBoardName = "";
                                                // String degreeNameX1 = "";
                                                String educationBoardOptions = "";
                                                //     Query educationBoardSQL = dbsession.createSQLQuery("SELECT DISTINCT university_long_name  FROM university WHERE university_long_name LIKE '%Education Board%' ORDER BY university_short_name ASC");
                                                Query educationBoardSQL = dbsession.createSQLQuery("SELECT * FROM university WHERE university_old_name = 'BOARD' ORDER BY university_short_name ASC");
                                                for (Iterator educationBoardItr = educationBoardSQL.list().iterator(); educationBoardItr.hasNext();) {
                                                    educationBoardObj = (Object[]) educationBoardItr.next();
                                                    educationBoardId = educationBoardObj[0].toString();
                                                    educationBoardShortName = educationBoardObj[1].toString();
                                                    educationBoardLongName = educationBoardObj[2].toString() == null ? "" : educationBoardObj[2].toString();

                                                    if (educationBoardLongName.equals("")) {
                                                        educationBoardName = educationBoardShortName;
                                                    } else {
                                                        educationBoardName = educationBoardShortName;
                                                    }
                                                    educationBoardOptions = educationBoardOptions + "<option value=\"" + educationBoardId + "\">" + educationBoardName + "</option>";

                                                }

                                                Object[] object6 = null;
                                                String universityId = "";
                                                String universityShortName = "";
                                                String universityLongName = "";
                                                String universityName = "";
                                                String degreeNameX1 = "";
                                                String universityOptions = "";
                                                Query q6 = dbsession.createSQLQuery("select * FROM university  WHERE university_old_name = 'ACCREDITED' ORDER BY university_long_name ASC");
                                                for (Iterator itr6 = q6.list().iterator(); itr6.hasNext();) {
                                                    object6 = (Object[]) itr6.next();
                                                    universityId = object6[0].toString();
                                                    universityShortName = object6[1].toString();
                                                    universityLongName = object6[2].toString() == null ? "" : object6[2].toString();

                                                    universityName = universityLongName;

                                                    universityOptions = universityOptions + "<option value=\"" + universityId + "\">" + universityName + "</option>";

                                                }

                                                Object[] objectMsc = null;
                                                String universityMscId = "";
                                                String universityMscShortName = "";
                                                String universityMscLongName = "";
                                                String universityMscName = "";
                                                String degreeMscNameX1 = "";
                                                String universityMscOptions = "";
                                                Query uniMscSQL = dbsession.createSQLQuery("select * FROM university  WHERE university_old_name != 'ACCREDITED' AND university_old_name != 'BOARD' ORDER BY university_long_name ASC");
                                                for (Iterator uniMscItr = uniMscSQL.list().iterator(); uniMscItr.hasNext();) {
                                                    objectMsc = (Object[]) uniMscItr.next();
                                                    universityId = objectMsc[0].toString();
                                                    universityShortName = objectMsc[1].toString();
                                                    universityLongName = objectMsc[2].toString() == null ? "" : objectMsc[2].toString();

                                                    if (universityLongName.equals("")) {
                                                        universityName = universityShortName;
                                                    } else {
                                                        universityName = universityLongName;
                                                    }

                                                    universityMscOptions = universityMscOptions + "<option value=\"" + universityId + "\">" + universityName + "</option>";

                                                }

                                                Object[] object7 = null;
                                                String resultTypeIdX = "";
                                                String resultTypeNameX = "";
                                                String resultTypeOptions = "";
                                                Query q7 = dbsession.createSQLQuery("select * FROM result_type ORDER BY result_type_id ASC");
                                                for (Iterator itr7 = q7.list().iterator(); itr7.hasNext();) {
                                                    object7 = (Object[]) itr7.next();
                                                    resultTypeIdX = object7[0].toString();
                                                    resultTypeNameX = object7[1].toString();
                                                    resultTypeOptions = resultTypeOptions + "<option value=\"" + resultTypeIdX + "\">" + resultTypeNameX + "</option>";

                                                }

                                                String yearOptions = "";
                                                // int yearStart = 1960 ;
                                                int yearEnd = 2019;//current Year
                                                // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                                //  Date date = new Date();
                                                //   String entryDate = dateFormat.format(date);
                                                for (int yearStart = 1960; yearStart <= yearEnd; yearStart++) {

                                                    yearOptions = yearOptions + "<option value=\"" + yearStart + "\">" + yearStart + "</option>";
                                                }

                                            %>

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberAddEducationSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5 class="m-t-20 text-center text-dark">SSC/Equivalent</h5>
                                                        <input name="memberSSCDegreeTypeId" id="memberSSCDegreeTypeId" type="hidden" value="1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Institution:</label>
                                                            <div class="col-md-8">
                                                                <input name="memberSSCInstitution" id="memberSSCInstitution" type="text" class="form-control" placeholder="Institution" required>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Board/University:</label>
                                                            <div class="col-md-8">
                                                                <select name="memberSSCBoardUniversity" id="memberSSCBoardUniversity" class="form-control chosen" required>
                                                                    <option value="">Select Education Board</option>                                                       
                                                                    <%=educationBoardOptions%>

                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberSSCPassingYear" name="memberSSCPassingYear"   class="custom-select" required>
                                                                    <option value="">Select Year</option>
                                                                    <%=yearOptions%>									
                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right  m-t-5" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberSSCResultTypeId" id="memberSSCResultTypeId" class="form-control" required>
                                                                    <option value="">Result Type </option>
                                                                    <%=resultTypeOptions%>

                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberSSCScoreClass" id="memberSSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required>
                                                            </div> 
                                                        </div> 
                                                    </div>  

                                                    <div class="col-md-6">
                                                        <h5 class="m-t-20 text-center text-dark">HSC/Equivalent</h5>

                                                        <input name="memberHSCDegreeTypeId" id="memberHSCDegreeTypeId" type="hidden" value="2">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Institution:</label>
                                                            <div class="col-md-8">
                                                                <input name="memberHSCInstitution" id="memberHSCInstitution" type="text" class="form-control" placeholder="Institution" required>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Board/University:</label>
                                                            <div class="col-md-8">
                                                                <select name="memberHSCBoardUniversity" id="memberHSCBoardUniversity" class="form-control chosen" required>
                                                                    <option value="">Select Education Board</option>
                                                                    <%=educationBoardOptions%>

                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberHSCPassingYear" name="memberHSCPassingYear"   class="custom-select" required>
                                                                    <option value="">Select Year</option>
                                                                    <%=yearOptions%>									
                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberHSCResultTypeId" id="memberHSCResultTypeId" class="form-control" required>
                                                                    <option value="">Result Type </option>
                                                                    <%=resultTypeOptions%>

                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberHSCScoreClass" id="memberHSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required>
                                                            </div> 
                                                        </div>                                                
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5 class="m-2 text-center text-dark">B.Sc. Engg./Equivalent </h5>
                                                        <input name="memberBSCDegreeTypeId" id="memberBSCDegreeTypeId" type="hidden" value="3">

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">University:</label>
                                                            <div class="col-md-8">
                                                                <select onchange="registrationEducationInfoSubjectShow(this.value)" name="memberBSCBoardUniversity" id="memberBSCBoardUniversity" class="form-control chosen" required>
                                                                    <option value="">Select University</option>                                                       
                                                                    <%=universityOptions%>

                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Subject:</label>
                                                            <div class="col-md-8" id="subjectCon">
                                                                <select name="memberBSCUniversitySubject" id="memberBSCUniversitySubject" class="form-control chosen" required disabled="">
                                                                    <option value="">Select Subject</option>                                                       

                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberBSCPassingYear" name="memberBSCPassingYear"   class="custom-select" required>
                                                                    <option value="">Select Year</option>
                                                                    <%=yearOptions%>									
                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberBSCResultTypeId" id="memberBSCResultTypeId" class="form-control" required>
                                                                    <option value="">Result Type </option>
                                                                    <%=resultTypeOptions%>

                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberBSCScoreClass" id="memberBSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required>
                                                            </div> 
                                                        </div> 
                                                    </div>  

                                                    <div class="col-md-6">
                                                        <h5 class="m-2 text-center text-dark">M.Sc./Ph.D/Equivalent </h5>

                                                        <input name="memberMSCDegreeTypeId" id="memberMSCDegreeTypeId" type="hidden" value="4">

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">University:</label>
                                                            <div class="col-md-8">
                                                                <select name="memberMSCBoardUniversity" id="memberMSCBoardUniversity" class="form-control chosen">
                                                                    <option value="">Select University</option>
                                                                    <%=universityMscOptions%>

                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberMSCPassingYear" name="memberMSCPassingYear"   class="custom-select">
                                                                    <option value="">Select Year</option>
                                                                    <%=yearOptions%>									
                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right m-t-5" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberMSCResultTypeId" id="memberMSCResultTypeId" class="form-control">
                                                                    <option value="">Result Type </option>
                                                                    <%=resultTypeOptions%>

                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberMSCScoreClass" id="memberMSCScoreClass"  type="text" class="form-control" placeholder="Score/Class">
                                                            </div> 
                                                        </div>                                                
                                                    </div>
                                                </div>                   
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-md-6 offset-md-3">
                                                                <button type="submit" class="btn btn-block btn-primary">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                   

                                            </form>                                
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                <span class="h4">Section 5: Professional Record</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationProfessionalInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                                <ul class="list-inline" id="professionRecordList">
                                                    <li id="proRecList1">

                                                        <div class="form-row">
                                                            <div class="form-group col-md-3">
                                                                <label for="inputNationality">Organization *</label>
                                                                <input name="memProfOrgName1" id="memProfOrgName1" type="text" class="form-control" placeholder="Oranization name" required disabled>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputPOB">Designation *</label>
                                                                <input name="memProfDesignation1" id="memProfDesignation1" type="text" class="form-control" placeholder="Designation name" required disabled>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputPOB">Start Date *</label>
                                                                <input type="text" class="form-control" id="startDate1" name="startDate1" placeholder="yyyy-mm-dd" required="" disabled> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputPOB">End Date *</label>
                                                                <input type="text" class="form-control" id="endDate1" name="endDate1"  placeholder="yyyy-mm-dd" required="" disabled> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                            </div>
                                                        </div>
                                                        <script type="text/javascript">

                                                            jQuery('#startDate1').datepicker({
                                                                autoclose: true,
                                                                todayHighlight: true,
                                                                format: 'yyyy-mm-dd'
                                                            });
                                                            jQuery('#endDate1').datepicker({
                                                                autoclose: true,
                                                                todayHighlight: true,
                                                                format: 'yyyy-mm-dd'
                                                            });

                                                        </script>  


                                                    </li>
                                                </ul>
                                                <div class="form-row"> 
                                                    <div class="form-group col-md-6">
                                                        <a onclick="addMoreProfessionInfo();" class="btn btn-success text-white disabled"><i class="fas fa-plus-circle"></i> Add record</a>

                                                        &nbsp;&nbsp;
                                                        <a id="deleteMoreProfessionInfoBtn" onclick="deleteMoreProfessionInfo();" class="btn btn-danger text-white disabled"><i class="fas fa-minus-circle"></i> Delete record</a>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </form>

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFourDocuments">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourDocuments" aria-expanded="false" aria-controls="collapseFourDocuments">
                                                <span class="h4">Section 6: Related Documents Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourDocuments" class="collapse" aria-labelledby="headingFourDocuments" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationRelatedDocumentsUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="1<%//=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-10 offset-1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">Required Document</label>

                                                            <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                                            <div class="col-4">
                                                                <input name="fileNID" id="fileNID" onchange="relatedDocumentUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required disabled="">  
                                                                <div id="fileNIDErr"></div>
                                                            </div>
                                                            <div class="col-4">
                                                                &nbsp;
                                                            </div>
                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .zip files allow. Please add below documents when create zip file.  
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   i. Certificates: SSC, HSC/Diploma, B.Sc. Engineering copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  ii. Transcripts: HSC/Diploma, B.Sc. Engineering copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Certificates of the others professional bodies (if any) copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Photocopy of NID/Smart card copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  iv. Recognized training record of last two (2) years copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   v. Details records of 150 words (member) and 200 words (fellow) on experience demonstrating competencies & abilities. copy
                                                        </p>
                                                    </div>
                                                </div>



                                            </form>

                                        </div>
                                    </div>
                                </div>                


                                <div class="card">
                                    <div class="card-header" id="headingFourRecom">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRecom" aria-expanded="false" aria-controls="collapseFourRecom">
                                                <span class="h4">Section 7: Recommendation Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRecom" class="collapse" aria-labelledby="headingFourRecom" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationRecomendationInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                                <div class="row">
                                                    <div class="col-4">
                                                        <p class="m-2 text-center text-dark">Proposer</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">

                                                                <input name="proposerMemberId" id="proposerMemberId" type="text" class="form-control" placeholder="Membership No" required disabled>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required disabled>
                                                            </div>                                                    
                                                        </div>    

                                                    </div>
                                                    <div class="col-4">
                                                        <p class="m-2 text-center text-dark">Seconder |</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="seconderMemberId" id="seconderMemberId" type="text" class="form-control" placeholder="Membership No" required<input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required disabled>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="seconderMemberName" id="seconderMemberName" type="text" class="form-control" placeholder="Member name" required disabled>

                                                            </div>                                                    
                                                        </div>    

                                                    </div>
                                                    <div class="col-4">
                                                        <p class="m-2 text-center text-dark">Seconder ||</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="thirdMemberId" id="thirdMemberId" type="text" class="form-control" placeholder="Membership No"<input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required disabled>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <div class="col-8 offset-2">
                                                                <input name="thirdMemberName" id="thirdMemberName" type="text" class="form-control" placeholder="Member name" disabled>

                                                            </div>                                                    
                                                        </div>    

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </form>
                                            <div class="row">
                                                <div class="col-12">
                                                    <p class="text-dark" style="font-size: 0.8rem;">Note |: Proposer and Seconder must be at least 
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.Two Fellows and one Member for Fellowship
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii.One Fellow and two Members for Membership
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii.Two Members for Associate Membership
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                                <div class="card" id="section9">
                                    <div class="card-header" id="headingFourRegFee">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRegFee" aria-expanded="false" aria-controls="collapseFourRegFee">
                                                <span class="h4">Section 8: Registration Fee Payment Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRegFee" class="collapse" aria-labelledby="collapseFourRegFee" data-parent="#accordion">
                                        <div class="card-body">


                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationFeePaymentOptionSubmitData.jsp">



                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                                <thead>

                                                                <th scope="col" class="bg-primary text-white"><strong>Category</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Entrance Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Annual Subscription Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Diploma Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>ID Card Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Total Amount (Tk.)</strong></th>

                                                                </thead>
                                                                <tbody>
                                                                    <%

                                                                        Object[] memTypeObject = null;
                                                                        Query memTypeSQL = null;

                                                                        String membershipApplyingType = "";
                                                                        String membershipEntranceFee = "";
                                                                        String membershipAnnualSubscriptionFee = "";
                                                                        String membershipDiplomaFee = "";
                                                                        String membershipIDCardFee = "";
                                                                        double membershipTotalPayableFee = 0.00;

                                                                        double membershipEntranceFee1 = 0.00;
                                                                        double membershipAnnualSubscriptionFee1 = 0.00;
                                                                        double membershipDiplomaFee1 = 0.00;
                                                                        double membershipIDCardFee1 = 0.00;

                                                                        //SELECT * FROM `member_type_info` 
                                                                        memTypeSQL = dbsession.createSQLQuery("SELECT * FROM member_type_info WHERE member_type_id = '" + membershipApplyingFor1 + "'");

                                                                        if (!memTypeSQL.equals("")) {
                                                                            for (Iterator memTypeItr = memTypeSQL.list().iterator(); memTypeItr.hasNext();) {
                                                                                memTypeObject = (Object[]) memTypeItr.next();

                                                                                membershipApplyingType = memTypeObject[1].toString();
                                                                                membershipEntranceFee = memTypeObject[4].toString();
                                                                                membershipAnnualSubscriptionFee = memTypeObject[6].toString();
                                                                                membershipDiplomaFee = memTypeObject[7].toString();
                                                                                membershipIDCardFee = memTypeObject[3].toString();

                                                                                membershipEntranceFee1 = Double.parseDouble(membershipEntranceFee);
                                                                                System.out.println("Double Entry Fee:: " + membershipEntranceFee1);
                                                                                membershipAnnualSubscriptionFee1 = Double.parseDouble(membershipAnnualSubscriptionFee);
                                                                                membershipDiplomaFee1 = Double.parseDouble(membershipDiplomaFee);
                                                                                membershipIDCardFee1 = Double.parseDouble(membershipIDCardFee);

                                                                                //  membershipTotalFee = Integer.sum(membershipEntranceFee1,membershipAnnualSubscriptionFee1,membershipDiplomaFee1,membershipIDCardFee1);
                                                                                membershipTotalPayableFee = membershipEntranceFee1 + membershipAnnualSubscriptionFee1 + membershipDiplomaFee1 + membershipIDCardFee1;

                                                                    %>
                                                                    <tr>
                                                                        <td><strong><%=membershipApplyingType%></strong></td>
                                                                        <td><%=membershipEntranceFee%></td>
                                                                        <td><%=membershipAnnualSubscriptionFee%></td>
                                                                        <td><%=membershipDiplomaFee%></td>
                                                                        <td><%=membershipIDCardFee%></td>
                                                                        <td><strong><%=membershipTotalPayableFee%></strong></td>
                                                                    </tr>
                                                                    <% }
                                                                        }%>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div> 
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     


                                            </form>





                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>



                </div>
                <!-- /.white-box -->
            </div>

        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>