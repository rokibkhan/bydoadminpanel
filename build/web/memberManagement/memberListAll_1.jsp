<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showSyUserDetails(arg1, arg2) {
            console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');
        }


        function syUserInfoUpdateOpt(arg1, arg2) {
            console.log("User :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Update User Details");

            $.post("syUserListUserDetails.jsp", {userId: arg1, sessionid: arg2}, function (data) {
//
//
//                console.log(data);
//
//                console.log("invoiceDetails :: " + data[0].invoiceDetails);
//
//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');

        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">All Type Member List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Member</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th style="width: 3%;">#</th>
                                    <th class="text-center" style="width: 8%;">Picture</th>
                                    <th class="text-center" style="width: 6%;">IEB ID</th>
                                    <th class="text-center" style="width: 30%;">Name</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center" style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String memberId = "";
                                    String memberIEBId = "";
                                    String memberName = "";
                                    String reqMemStatusText = "";
                                    String memberPictureName = "";
                                    String memberPictureUrl = "";
                                    String memberPicture = "";
                                    String committeeDuration = "";
                                    String committeeDesc = "";
                                    String committeeStatus = "";

                                    Object memberObj[] = null;
                                    Query memberSQL = null;

                                    String status1 = "";
                                    String reqMemStatus = "";
                                    String btnActivePoll = "";
                                    String btnCompletePoll = "";
                                    String committeeMemberBtn = "";
                                    String agrX = "";

                                    memberSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id, "
                                            + " m.member_name, m.father_name, m.mother_name, "
                                            + "m.place_of_birth,m.dob ,m.status,m.picture_name  "
                                            + "FROM  member m ORDER BY m.id DESC LIMIT 0,100");

                                    if (!memberSQL.list().isEmpty()) {
                                        for (Iterator it1 = memberSQL.list().iterator(); it1.hasNext();) {

                                            memberObj = (Object[]) it1.next();
                                            memberId = memberObj[0].toString().trim();
                                            memberIEBId = memberObj[1] == null ? "" : memberObj[1].toString().trim();
                                            memberName = memberObj[2] == null ? "" : memberObj[2].toString().trim();
                                            reqMemStatus = memberObj[7] == null ? "" : memberObj[7].toString().trim();

                                            if (reqMemStatus.equals("0")) {
                                                reqMemStatusText = "<a class=\"btn btn-info waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-undo\"></i> Pending</a>";
                                            }

                                            if (reqMemStatus.equals("1")) {
                                                reqMemStatusText = "<a class=\"btn btn-success waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-check\"></i> Approved</a>";
                                            }
                                            if (reqMemStatus.equals("2")) {
                                                reqMemStatusText = "<a class=\"btn btn-danger waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-times-circle\"></i> Declined</a>";
                                            }

                                            memberPictureName = memberObj[8] == null ? "" : memberObj[8].toString().trim();
                                            memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPictureName;
                                            memberPicture = "<img width=\"120\" src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\">";

                                            String memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";


                                %>


                                <tr id="infoBox<%=memberId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><%=memberPicture%></td>
                                    <td><%=memberIEBId%></td>
                                    <td><%=memberName%></td>
                                    <td class="text-center"><% //out.print(reqMemStatusText);%></td>
                                    <td class="text-center">

                                        <a title="Details" href="<%=memberDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                    %>



                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>