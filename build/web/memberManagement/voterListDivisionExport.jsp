<%-- 
    Document   : voterListCenterExport
    Created on : October 11, 2019, 9:28:45 PM
    Author     : Tahajjat
--%>

<%@page import="java.text.DateFormat"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="net.sf.jasperreports.engine.JasperPrintManager"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();

    String voterFeeYearId = request.getParameter("voterFeeYearId").toString();
    String memberSearchDivisionId = request.getParameter("memberSearchDivisionId").trim();
    String memberSearchDivisionName = request.getParameter("memberSearchDivisionName").trim();

    String memberSearchDivisionName1 = memberSearchDivisionName.replaceAll(" ", "_");

    System.out.println("memberSearchDivisionName::" + memberSearchDivisionName);
    System.out.println("memberSearchDivisionName1::" + memberSearchDivisionName1);

    String downloadFileNamePrefix = "";
    String downloadFileNamePostfix = "";

    DateFormat dateFormatX = new SimpleDateFormat("yyyyMMddHHmmss");
    Date dateX = new Date();
    downloadFileNamePostfix = dateFormatX.format(dateX);

    downloadFileNamePrefix = memberSearchDivisionName1 + "_Voter_List";

    File reportFile = null;
//            String endDate = sTd.getDate(endDate1);

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    reportFile = new File(application.getRealPath("/Reports/VoterList_Division.jasper"));

    Map parameters = new HashMap();
    parameters.put("divisionId", memberSearchDivisionId);
    parameters.put("divisionName", memberSearchDivisionName);
    parameters.put("paymentYear", voterFeeYearId);

    JasperPrint jasperPrint = JasperFillManager.fillReport(reportFile.getPath(), parameters, con);
    byte bytes[] = new byte[10000];
    JRXlsExporter exporter = new JRXlsExporter();

    ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
    exporter.exportReport();

    bytes = xlsReport.toByteArray();
    response.setContentType("application/vnd.ms-excel");
    // response.setHeader("Content-Disposition", "attachment; filename=RPT_03.xls");
    response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileNamePrefix + "_" + downloadFileNamePostfix + ".xls");

    response.setContentLength(bytes.length);

    xlsReport.close();

    OutputStream ouputStream = response.getOutputStream();

    ouputStream.write(bytes, 0, bytes.length);
    ouputStream.flush();
    ouputStream.close();

    con.close();
    dbsession.close();

%>


