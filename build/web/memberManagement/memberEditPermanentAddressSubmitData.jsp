<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

//    if (session.isNew()) {
//        response.sendRedirect("logout.jsp");
//    }
    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        // memberIdH = session.getAttribute("memberId").toString();
        memberIdH = request.getParameter("memberEditId").toString().trim();

        System.out.println("memberIdH " + memberIdH);

        //   String memberName = request.getParameter("memberName").trim().toUpperCase();
        String pAddressCountry = request.getParameter("pAddressCountry") == null ? "" : request.getParameter("pAddressCountry").trim();
        String pAddressDistrict = request.getParameter("pAddressDistrict") == null ? "" : request.getParameter("pAddressDistrict").trim();
        String pAddressThana = request.getParameter("pAddressThana") == null ? "" : request.getParameter("pAddressThana").trim();

        System.out.println("pAddressCountry " + pAddressCountry);
        System.out.println("pAddressDistrict " + pAddressDistrict);
        System.out.println("pAddressThana " + pAddressThana);

        String memberAddressLine1 = request.getParameter("pAddressLine1") == null ? "" : request.getParameter("pAddressLine1").trim();
        String memberAddressLine2 = request.getParameter("pAddressLine2") == null ? "" : request.getParameter("pAddressLine2").trim();
        String pAddressPostOffice = request.getParameter("pAddressPostOffice") == null ? "" : request.getParameter("pAddressPostOffice").trim();
        String pAddressPostCode = request.getParameter("pAddressPostCode") == null ? "" : request.getParameter("pAddressPostCode").trim();
        String memberAddressId = request.getParameter("pAddressId").trim();

        // String addterm = request.getParameter("entryTerm");
        //  String addip = request.getParameter("entryIP");
        //   String addterm = InetAddress.getLocalHost().getHostName().toString();
        //   String addip = InetAddress.getLocalHost().getHostAddress().toString();
        AddressBook addressbook = null;

        Country country = new Country();
        Thana thana = new Thana();

        Query q1 = dbsession.createQuery("from AddressBook WHERE id=" + memberAddressId + " ");

        Iterator itr1 = q1.list().iterator();

        if (itr1.hasNext()) {
            addressbook = (AddressBook) itr1.next();
        }

        country.setCountryCode(pAddressCountry);
        addressbook.setCountry(country);

        thana.setId(Integer.parseInt(pAddressThana));
        addressbook.setThana(thana);

        addressbook.setAddress1(memberAddressLine1);
        addressbook.setAddress2(memberAddressLine2);
        addressbook.setModUser(pAddressPostOffice);
        addressbook.setZipcode(pAddressPostCode);
        dbsession.update(addressbook);

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            strMsg = "Success!!! Permanent Address Information Updated";
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=address&strMsg=" + strMsg);
        } else {
            dbtrx.rollback();
            strMsg = "Error!!! When Permanent Address Information Update";
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=address&strMsg=" + strMsg);
        }
    } else {
        System.out.println("DDDD LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
        return;
    }

%>
