<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
    %>

    <script type="text/javascript">



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Fellow Member List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Member</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <%
                String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();
                String searchQuery = "";
                if (!searchString.equals("")) {
                    searchQuery = "WHERE m.memberName like '%" + searchString + "%' OR m.emailId like '%" + searchString + "%' OR m.memberId like '%" + searchString + "%' or m.mobile like '%" + searchString + "%'";

                } else {
                    searchQuery = "";
                }
            %>

            <div class="col-md-12">
                <div class="white-box">
                    <form class="form-group"  name="memberListAll" id="memberListAll" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%out.print(sessionid);%>" >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 font-weight-bold text-right">Center</label>
                                    <div class="col-md-6">  
                                        <select  id="memberEditCenterId" name="memberEditCenterId" class="form-control input-sm customInput-sm" required>
                                            <option value="">Select Center</option>
                                            <%
                                                Query centerEditSQL = null;
                                                Object[] centerEditObject = null;
                                                String centerEditId = "";
                                                String centerEditName = "";
                                                String centerEditSelected = "";
                                                centerEditSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_type_id ='3' ORDER BY center_id ASC");
                                                for (Iterator centerEditItr = centerEditSQL.list().iterator(); centerEditItr.hasNext();) {
                                                    centerEditObject = (Object[]) centerEditItr.next();
                                                    centerEditId = centerEditObject[0].toString();
                                                    centerEditName = centerEditObject[1].toString();

//                                                    if (centerEditId.equals(Integer.toString(centerId))) {
//                                                        centerEditSelected = " selected";
//                                                    } else {
//                                                        centerEditSelected = "";
//                                                    }

                                            %> 
                                            <option value="<%=centerEditId%>" <%=centerEditSelected%>> <%=centerEditName%> </option> 
                                            <%
                                                }

                                            %>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-2 font-weight-bold text-right">Division</label>
                                    <div class="col-md-9">  
                                        <select name="memberDivisionId" id="memberDivisionId" class="form-control" required>
                                            <option value="">Select Division</option>
                                            <%                                                            Object[] objectEditDiv = null;
                                                String divisionEditId = "";
                                                String divisionEditName = "";
                                                String divisionEditOptions = "";
                                                Query divisionEditSQL = dbsession.createSQLQuery("select * FROM member_division ORDER BY mem_division_name ASC");
                                                for (Iterator divisionEditItr = divisionEditSQL.list().iterator(); divisionEditItr.hasNext();) {
                                                    objectEditDiv = (Object[]) divisionEditItr.next();
                                                    divisionEditId = objectEditDiv[0].toString();
                                                    // divisionEditName = objectEditDiv[1].toString();
                                                    divisionEditName = objectEditDiv[3].toString();
                                                    divisionEditOptions = divisionEditOptions + "<option value=\"" + divisionEditId + "\">" + divisionEditName + "</option>";

                                                }
                                            %>
                                            <%=divisionEditOptions%>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 font-weight-bold text-right">Member Type</label>
                                    <div class="col-md-6">  
                                        <select id="memberTypeId" name="memberTypeId" class="form-control input-sm customInput-sm" required>
                                            <option value="">Select Member Type</option>
                                            <option value="1">Fellow</option>
                                            <option value="2">Member</option>
                                            <option value="3">Associate Member</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-2 font-weight-bold text-right">District</label>
                                    <div class="col-md-9">  
                                        <select name="memberDistrictId" id="memberDistrictId" class="form-control" required>
                                            <option value="">Select District</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group d-flex align-items-center">
                                    <div class="col-md-6 offset-md-5">                                                                                           
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.white-box -->
            </div>

            <div class="col-md-12">
                <div class="white-box">



                    <form class="form-group"  name="memberListAll" id="memberListAll" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberListAll.jsp?sessionid=<%out.print(sessionid);%>" >
                        <div class="input-group">
                            <input type="text" id="searchString" name="searchString" value="<%=searchString%>" class="form-control" placeholder="Membership ID Or Name Or Mobile Number"> 
                            <span class="input-group-btn">
                                <button type="submit" name="btnSearch" class="btn waves-effect waves-light btn-info">
                                    <i class="fa fa-search"></i> Search Member
                                </button>
                            </span> 
                        </div>
                    </form>

                    <%

                        String searchQuery1 = "";

                        int ix = 1;
                        String memberId = "";
                        Object searchObj[] = null;

                        // String targetpage = "memberListFellow.jsp";
                        String targetpageWithSession = "memberListFellow.jsp?sessionid=" + session.getId() + "&";
                        String filterp = "";
                        int limit = 1000;
                        int total_pages = 0;
                        int stages = 3;
                        int start = 0;
                        String p = "";
                        String p1 = "";
                        String p2 = "";

                        int prev = 0;
                        int next = 0;
                        //  double lastpage = 0;
                        //  double LastPagem1 = 0;

                        int lastpage = 0;
                        int LastPagem1 = 0;
                        System.out.println("lastpage :: " + lastpage);
                        System.out.println("LastPagem1 :: " + LastPagem1);

                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();

                        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
                        int pageNbr = Integer.parseInt(pageNbr1);

                        System.out.println("filter :: " + filter);
                        System.out.println("pageNbr :: " + pageNbr);

                        String memberIEBId = "";
                        String memberName = "";
                        String memberPictureName = "";
                        String memberPictureUrl = "";
                        String memberPicture = "";
                        String memberMobile = "";
                        String memberEmail = "";
                        String memberAddress = "";

                        Object memberObj[] = null;
                        Query memberSQL = null;

                        String status1 = "";
                        String reqMemStatus = "";
                        String btnActivePoll = "";
                        String btnCompletePoll = "";
                        String committeeMemberBtn = "";
                        String agrX = "";

                        String searchCountSQL = "SELECT count(*) FROM  member m "
                                + " " + searchQuery1 + " "
                                + "ORDER BY m.id DESC";

                        String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        if (pageNbr != 0) {
                            start = (pageNbr - 1) * limit;
                        } else {
                            start = 0;
                        }

                        System.out.println("startLLLL :: " + start);
                        System.out.println("limitLLLL :: " + limit);

                        // Initial page num setup
                        if (pageNbr == 0) {
                            pageNbr = 1;
                        }
                        prev = pageNbr - 1;
                        next = pageNbr + 1;
                        // lastpage = Math.ceil(total_pages / limit);

                        //lastpage = (int)Math.round(Math.ceil(total_pages / limit));
                        //  LastPagem1 = (int)Math.round(lastpage - 1);
                        lastpage = (int) Math.round(Math.ceil(total_pages / limit));
                        LastPagem1 = (int) Math.round(lastpage - 1);

                        System.out.println("prev Xc:: " + prev);
                        System.out.println("next Xc:: " + next);
                        System.out.println("lastpage Xc:: " + lastpage);
                        System.out.println("LastPagem1 Xc:: " + LastPagem1);

                        String paginate = "";
                        if (lastpage > 1) {
                            paginate = paginate + "<ul class='pagination' style=\"float:right\">";

                            // Previous
                            if (pageNbr > 1) {
                                p = "page=" + prev;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>previous</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
                            }

                            int counter = 1;
                            // Pages
                            if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

                                for (counter = 1; counter <= lastpage; counter++) {
                                    if (counter == pageNbr) {
                                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                    } else {
                                        p = "page=" + counter;
                                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                    }
                                }
                            } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
                                // Beginning only hide later pages
                                if (pageNbr < 1 + (stages * 2)) {
                                    for (counter = 1; counter < 4 + (stages * 2); counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // Middle hide some front and some back
                                else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // End only hide early pages
                                else {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                                    for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                }
                            }

                            // Next
                            if (pageNbr < counter - 1) {
                                p = "page=" + next;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>next</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
                            }

                            paginate = paginate + "</ul>";
                        }

                        out.print(paginate);

                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
                                + "m.picture_name,m.mobile,m.email_id,m.status   "
                                + "FROM  member m "
                                + " " + searchQuery1 + " "
                                + "ORDER BY m.id DESC  LIMIT " + start + ", " + limit + "";


                    %>
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th style="width: 3%;">#</th>
                                    <th class="text-center" style="width: 5%;">Picture</th>
                                    <th class="text-center" style="width: 6%;">IEB ID</th>
                                    <th class="text-center" style="width: 20%;">Name</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%                                   
                                    
                                    Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                                    if (!searchSQLQry.list().isEmpty()) {
                                        for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                                            searchObj = (Object[]) searchItr.next();
                                            memberId = searchObj[0].toString();
                                            memberIEBId = searchObj[1] == null ? "" : searchObj[1].toString();
                                            memberName = searchObj[2] == null ? "" : searchObj[2].toString();

                                            String memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
                                %>
                                <tr id="infoBox<%=memberId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><%=memberPicture%></td>
                                    <td><%=memberIEBId%></td>
                                    <td><%=memberName%></td>
                                    <td class="text-center"><% //out.print(reqMemStatusText);%></td>
                                    <td class="text-center"><% //out.print(reqMemStatusText);%></td>
                                    <td class="text-center"><% //out.print(reqMemStatusText);%></td>
                                    <td class="text-center">

                                        <a title="Details" href="<%=memberDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></a>


                                    </td>
                                </tr>
                                <%
                                        ix++;
                                    }

                                } else {
                                %>
                                <tr>
                                    <td colspan="6">There no data found</td>

                                </tr>
                                <%
                                    }
                                %>

                            </tbody>
                        </table>
                    </div>
                    <%
                        //  }
                        dbsession.clear();
                        dbsession.close();
                    %>

                    <div class="row">
                        <div class="col-md-9 align-right">
                            <%=paginate%>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>