<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.ssl.commerz.Utility.Util"%>
<%@page import="com.appul.entity.MemberDivision"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.GlobalVariable"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfoTemp"%>
<%@page import="com.appul.entity.MemberProfessionalInfoTemp"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>
<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
    Session dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Object objReqMem[] = null;
    String reqMemId = "";
    String reqMemRegId = "";
    String reqMemName = "";
    String maritalStatus = "";
    String nid = "";
    String reqMemFatherName = "";
    String reqMemMotherName = "";
    String reqMemBirthPlace = "";
    String reqMemBirthDate = "";
    String reqMemAge = "";
    String reqMemNationality = "";
    String reqMemGender = "";
    String reqMemGender1 = "";
    String reqMemApplyFor = "";
    String reqMemApplyFor1 = "";
    String reqMemOldNumber = "";
    String reqMemBloodGroup = "";
    String reqMemOfficePhone = "";
    String reqMemResidancePhone = "";
    String reqMemMobilePhone = "";
    String reqMemEmail = "";
    String reqMemSubdivision_id = "";

    String selUserId = "";
    String selUserName = "";
    String selUserEmail = "";
    String selUserMoble = "";
    String selUserDeptId = "";
    String selUserDept = "";
    String pictureName = "";
    String pictureLink = "";
    String selectedUserId = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

    //out.println("selectedUserId :: " + selectedUserId);
    if (!selectedUserId.equals("")) {

        System.out.println("selectedUserId IN :: " + selectedUserId);

        Query usrSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id, m.member_temp_id, "
                + "m.member_temp_pass, m.member_name, m.father_name, m.mother_name, "
                + "m.place_of_birth,m.dob, m.picture_name,age,nationality,gender,apply_for,old_member_id,"
                + "blood_group,phone1,phone2,mobile,email_id,member_subdivision_id "
                + "FROM  member_temp m "
                + "WHERE m.id = '" + selectedUserId + "'"
                + "ORDER BY m.id DESC");
        
        
        List nidList = dbsession.createSQLQuery("select * from member_nid where member_id="+selectedUserId)
                .list();
        Iterator it = nidList.iterator();
        Object[] nidObjs = (Object[]) it.next();
        
        pageContext.setAttribute("nidObjs", nidObjs);
        
        if (!usrSQL.list().isEmpty()) {
            for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                objReqMem = (Object[]) it1.next();
                reqMemId = objReqMem[0].toString().trim();
                reqMemRegId = objReqMem[2] == null ? "" : objReqMem[2].toString().trim();
                reqMemName = objReqMem[4] == null ? "" : objReqMem[4].toString().trim();
                reqMemFatherName = objReqMem[5] == null ? "" : objReqMem[5].toString().trim();
                reqMemMotherName = objReqMem[6] == null ? "" : objReqMem[6].toString().trim();
                reqMemBirthPlace = objReqMem[7] == null ? "" : objReqMem[7].toString().trim();
                reqMemBirthDate = objReqMem[8] == null ? "" : objReqMem[8].toString().trim();
                reqMemAge = objReqMem[10] == null ? "" : objReqMem[10].toString().trim();
                reqMemNationality = objReqMem[11] == null ? "" : objReqMem[11].toString().trim();
                reqMemGender = objReqMem[12] == null ? "" : objReqMem[12].toString().trim();

                if (reqMemGender.equals("M")) {
                    reqMemGender1 = "MALE";
                } else {
                    reqMemGender1 = "FEMALE";
                }

                reqMemApplyFor = objReqMem[13] == null ? "" : objReqMem[13].toString().trim();
                if (reqMemApplyFor.equals("1")) {
                    reqMemApplyFor1 = "Fellow";
                }
                if (reqMemApplyFor.equals("2")) {
                    reqMemApplyFor1 = "Member";
                }
                if (reqMemApplyFor.equals("3")) {
                    reqMemApplyFor1 = "Associate Member";
                }

                reqMemOldNumber = objReqMem[14] == null ? "" : objReqMem[14].toString().trim();

                reqMemBloodGroup = objReqMem[15] == null ? "" : objReqMem[15].toString().trim();

                reqMemOfficePhone = objReqMem[16] == null ? "" : objReqMem[16].toString().trim();
                reqMemResidancePhone = objReqMem[17] == null ? "" : objReqMem[17].toString().trim();
                reqMemMobilePhone = objReqMem[18] == null ? "" : objReqMem[18].toString().trim();
                reqMemEmail = objReqMem[19] == null ? "" : objReqMem[19].toString().trim();

                reqMemSubdivision_id = objReqMem[20] == null ? "" : objReqMem[20].toString().trim();

                pictureName = objReqMem[9] == null ? "" : objReqMem[9].toString().trim();

                pictureLink = "<img width=\"100\" height=\"123\" src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + reqMemName + "\" class=\"img-fluid img_1\">";

            }
        }

    } else {

        System.out.println("selectedUserId OUt ::");

        //  response.sendRedirect(GlobalVariable.baseUrl + "/home.jsp");
    }

    Query madrSQL = null;
    Object[] madrObject = null;

    Query padrSQL = null;
    Object[] padrObject = null;

    int mAddressId = 0;
    String mCountryId = "";
    String mDistrictId = "";
    String mThanaId = "";
    String mThanaName = "";
    String mThanaDistrictName = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String mZipOffice = "";
    String mZipCode = "";

    int pAddressId = 0;
    String pCountryId = "";
    String pDistrictId = "";
    String pThanaId = "";
    String pThanaName = "";
    String pThanaDistrictName = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    String pZipOffice = "";
    String pZipCode = "";

    String mAddressStr = "";
    String pAddressStr = "";

    Thana thana = null;

    Query mailAddrSQL = dbsession.createSQLQuery("select * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + selectedUserId + " and address_Type='M' ) ");
    for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
        madrObject = (Object[]) itr2.next();
        mAddressLine1 = madrObject[1].toString();
        mAddressLine2 = madrObject[2].toString();
        mThanaId = madrObject[3].toString();

        Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
        for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
            thana = (Thana) itrmThana.next();

            mThanaName = thana.getThanaName();
            mThanaDistrictName = thana.getDistrict().getDistrictName();
        }

        mZipCode = madrObject[4].toString();
        mZipOffice = madrObject[8].toString();

        mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

    }
    Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + selectedUserId + " and address_Type='P' ) ");
    for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

        padrObject = (Object[]) itr3.next();
        pAddressLine1 = padrObject[1].toString();
        pAddressLine2 = padrObject[2].toString();
        pThanaId = padrObject[3].toString();

        Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
        for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
            thana = (Thana) itrpThana.next();
            pThanaName = thana.getThanaName();
            pThanaDistrictName = thana.getDistrict().getDistrictName();
        }

        pZipCode = padrObject[4].toString();
        pZipOffice = padrObject[8].toString();

        pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

    }

    Object[] divisionObj = null;
    Object[] subDivisionObj = null;

    String divisionId = "";
    String divisionName = "";
    String subDivisionName = "";
    Query subDivisionSQL = dbsession.createSQLQuery("SELECT * FROM sub_division WHERE sub_division_id='" + reqMemSubdivision_id + "'");
    for (Iterator subDivisionItr = subDivisionSQL.list().iterator(); subDivisionItr.hasNext();) {
        subDivisionObj = (Object[]) subDivisionItr.next();
        divisionId = subDivisionObj[2].toString();
        subDivisionName = subDivisionObj[4].toString();
    }

    Query divisionQuery = dbsession.createQuery("FROM MemberDivision");
    List<MemberDivision> memberDivisions = divisionQuery.list();
    pageContext.setAttribute("memberDivisions", memberDivisions);

    Query divisionSQL = dbsession.createSQLQuery("SELECT * FROM member_division WHERE mem_division_id ='" + divisionId + "'");
    for (Iterator DivisionItr = divisionSQL.list().iterator(); DivisionItr.hasNext();) {
        divisionObj = (Object[]) DivisionItr.next();
        divisionName = divisionObj[1].toString();
    }

    List<String> subDivisionList = Util.subDivisionList(memberDivisions, subDivisionName, divisionName);
    pageContext.setAttribute("subDivisionList", subDivisionList);

    Query eduTempSQL = null;
    eduTempSQL = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + selectedUserId + " ");
    List<MemberEducationInfoTemp> meiList = eduTempSQL.list();
    //request.setAttribute("meiList", meiList);
    pageContext.setAttribute("meiList", meiList);

    Query mpitQuery = dbsession.createQuery("from MemberProfessionalInfoTemp where  member_id=" + selectedUserId + " ");
    List<MemberProfessionalInfoTemp> mpitList = mpitQuery.list();
    pageContext.setAttribute("mpitList", mpitList);
%>

<!DOCTYPE html >


<head>	
    <title>Member Type Membership Form</title>


    <link rel="stylesheet" type="text/css" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/MFORM/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/MFORM/css/print.css" media="print" />
</head>

<body>

    <div id="page-wrap">
        <div class="top">
            <div class="logo">
                <img src="img/IEB-logo.png">
            </div>
            <div class="title"> 
                <ul>
                    <li ><h1 style="margin:0; ">THE INSTITUTION OF ENGINEERS, BANGLADESH</h1></li>
                    <li ><h1 style="margin:0; ">HEADQUARTERS: RAMNA, DHAKA-1000</h1></li>
                    <li ><p style="margin:0; ">Founded in 1948, Registered Under Act XXI of 1860<br>
                            Recognized by the Goverment of the People's Republic of Bangladesh <br>
                            Phone: 9566336, 9559485, 5110330, 55110331, 5110332 <br>
                            Fax: 88-02-9562447; E-mail: info.iebhq@gmail.com; www.iebbd.org </p></li>

                </ul>

            </div>

            <div class="logo">
                <%=pictureLink%>
            </div>


        </div>
        <div class="title-block" align="center">
            <h1 style="margin:0; ">Appication Form For Membership (MEMBER)</h1>
            <p style="margin-top: 0;">(ALL Relevent space must be filled in)</p>

        </div>
        <div align="left" style="display: -webkit-box;" >
            <table class="tbl1" style="margin-left: 20px;">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th>1.0 PERSONAL INFORMATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.1   Name: <%=reqMemName%></td>
                    </tr>
                    <tr>
                        <td>1.2 FATHER'S NAME  : <%=reqMemFatherName%></td>
                    </tr>
                    <tr>
                        <td>1.3 MOTHER'S NAME  : <%=reqMemMotherName%></td>
                    </tr>
                    <tr>
                        <td>1.4 SPOUSE NAME  : ${empty nidObjs[3] ? "N/A" : nidObjs[4]} </td>
                    </tr>
                    <tr>
                        <td>1.5 DATE OF BIRTH  : <%=reqMemBirthDate%></td>
                    </tr>
                    <tr>
                        <td>1.6 AGE (on next birth date) Years : <%=reqMemAge%></td>
                    </tr>
                    <tr>
                        <td>1.6.1  PLACE OF BIRTH : <%=reqMemBirthPlace%></td>
                    </tr>
                    <tr>
                        <td>1.7 BLOOD GROUP : <%=reqMemBloodGroup.equals("") ? "N/A" : reqMemBloodGroup%></td>
                    </tr>
                    <tr>
                        <td>1.8 NID (Number) : ${nidObjs[2]}</td>
                    </tr>
                    <tr>
                        <td>1.9 NATIONALITY : <%=reqMemNationality%></td>
                    </tr>
                    <tr>
                        <td>1.9.1 GENDER : <%=reqMemGender1%>  </td>
                    </tr>
                    <tr>
                        <td>1.9.2 MAILING ADDRESS (With postal code) : <%=mAddressStr%></td>
                    </tr>
                    <tr>
                        <td>1.9.3 PERMANENT ADDRESS (With postal code) : <%=pAddressStr%></td>
                    </tr>
                    <tr>
                        <td>1.9.4 PHONE/CONTACT:  <br>
                            <P>OFFICE : <%=reqMemOfficePhone%> | MOBILE: <%=reqMemMobilePhone%> <br>
                                RESIDENCE : <%=reqMemResidancePhone%> | E-MAIL : <%=reqMemEmail%>
                        </td>
                    </tr>
                    <tr>
                        <td>1.9.5 PRESENT IEB MEMBERSHIP NUMBER (If any): <%=reqMemOldNumber.equals("") ? "N/A" : reqMemOldNumber%>
                        </td>
                    </tr>

                </tbody>
            </table>
            <table class="tbl2" style="margin-left: 10px; ">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th>FOR OFFICIAL USE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Scroll No :</td>
                    </tr>
                    <tr>
                        <td>Date of Receive :</td>
                    </tr>
                    <tr>
                        <td>Acknowledgement :</td>
                    </tr>
                    <tr>
                        <td>Copies of Certificates :</td>
                    </tr>
                    <tr>
                        <td>Copies of Transcripts :</td>
                    </tr>
                    <tr>
                        <td>Photo Enclosed : Yes/No </td>
                    </tr>
                    <tr>
                        <td>Professional Record Enclosed :</td>
                    </tr>
                    <tr>
                        <td>Recommendation : Yes/No </td>
                    </tr>
                    <tr>
                        <td> Name & Signature: </td>
                    </tr>
                    <tr>
                        <td style="padding: 14px;">  </td>
                    </tr>
                    <tr style="background-color: #00aeef; color: white;" >
                        <td>EVALUATED BY MEMBERSHIP SECTION </td>
                    </tr>
                    <tr>
                        <td>Age :</td>
                    </tr>
                    <tr>
                        <td>Education :</td>
                    </tr>
                    <tr>
                        <td>Experience : </td>
                    </tr>
                    <tr>
                        <td>Recommendation : </td>
                    </tr>
                    <tr>
                        <td>Name & Signature : </td>
                    </tr>
                    <tr>
                        <td style="padding: 14px;"> </td>
                    </tr>
                    <tr style="background-color: #00aeef; color: white;" >
                        <td>MEMBERSHIP ASSESSMENT TEAM </td>
                    </tr>
                    <tr>
                        <td>Trainning Record For Two Years : Yes/No </td>
                    </tr>
                    <tr>
                        <td>Detailed Report of 1500 words on exprerience Demonstrating Competencies and Abilities : Yes/No</td>
                    </tr>
                    <tr>
                        <td>Name and Signeture : </td>
                    </tr>
                    <tr>
                        <td>F/M </td>
                    </tr>
                    <tr style="background-color: #00aeef; color: white;" >
                        <td>CHAIRMAN/MEMBER SECRETARY/MEMBER, MEMBERSHIP COMMITTEE</td>
                    </tr>
                    <tr>
                        <td>Accepted/Rejected</td>
                    </tr>
                    <tr>
                        <td>Name & Signature :</td>
                    </tr>
                    <tr>
                        <td>F/M </td>
                    </tr>

                </tbody>
            </table>
        </div>


        <div align="left" style="  margin-top: 10px;">
            <table class="tbl3" style="margin-left: 20px;">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th> 2.0.EDUCATION (enclose attested photocopies of certificates)</th>
                    </tr>
                </thead>
            </table>

            <table class="tbl3" style="margin-left: 20px;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td>EQUIVALENT LEVEL</td>
                        <td>INSTITUTE</td>
                        <td>BOARD/UNIVERSITY/LOCATION</td>
                        <td>YEAR OF PASSING</td>
                        <td>DIV/CLASS/ GPA/CGPA</td>
                    </tr>
                    <c:forEach items="${meiList}" var="mei">
                        <tr>
                            <td>${mei.degree.degreeName}</td>
                            <td>${empty mei.instituteName ? mei.university.universityLongName : mei.instituteName}</td>
                            <td>${mei.university.universityLongName}</td>
                            <td>${mei.yearOfPassing}</td>
                            <td>${mei.result}</td>
                        </tr>
                    </c:forEach>


                </tbody>
            </table>
        </div>

        <div align="left" style="  margin-top: 10px;">
            <table class="tbl3" style="margin-left: 20px;">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th> 3.0. FIELD OF ENGINEERING : Please Tick in Division. Write Sub-Division (if any) </th>
                    </tr>
                </thead>
            </table>

            <table class="tbl3" style="margin-left: 20px;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td style="text-align: center;" >Division</td>
                        <c:forEach items="${memberDivisions}" var="md">
                            <td style="text-align: center;" >${md.memDivisionName}</td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <td style="text-align: center;">Sub-Division</td>
                        <c:forEach items="${subDivisionList}" var="sdl">
                            <td>${sdl}</td>
                        </c:forEach>
                    </tr>

                </tbody>
            </table>




        </div>
        <div align="left" style="  margin-top: 50px;">
            <table class="tbl3" style="margin-left: 20px;">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th> 4.0.PROFESSIONAL RECORD (if necessary enclose separate page) </th>
                    </tr>
                </thead>
            </table>

            <table class="tbl3" style="margin-left: 20px;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td style="text-align: center;" rowspan="2">SL. No.</td>
                        <td style="text-align: center;" colspan="2">PERIOD (DATE)</td>
                        <td style="text-align: center;" rowspan="2">DESIGNATION</td>
                        <td style="text-align: center;" rowspan="2">EMPLOYER</td>
                        <td style="text-align: center;" rowspan="2">BRIEF JOB DESCRIPTION</td>

                    </tr>
                    <tr> 
                        <td style="text-align: center;">FROM</td>
                        <td style="text-align: center;">TO</td>
                    </tr>
                    <c:set var="sl" value="1"/>
                    <c:forEach items="${mpitList}" var="mpi">
                        <tr> 
                            <td>${sl}</td>
                            <td>${mpi.fromDate} </td>
                            <td>${mpi.tillDate} </td>
                            <td>${mpi.designation} </td>
                            <td>${mpi.companyName} </td>
                            <td> </td>
                        </tr>
                        <c:set var="sl" value="${sl+1}"/>
                    </c:forEach>
                    

                </tbody>
            </table>
        </div> 	


        <div align="left" style="  margin-top: 10px;">
            <table class="tbl3" style="margin-left: 20px;">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th> 5.0 Please enclose attested copies of certificates of other Professional bodies including Membership no. (if any) </th>
                    </tr>
                </thead>
            </table>

            <table class="tbl3" style="margin-left: 20px;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td>1. </td>
                    </tr>
                    <tr> 
                        <td>2. </td>

                    </tr>
                    <tr> 
                        <td> <p style=" margin: 0; font-weight: 600; "> 6.0. DECLARATION:</p> <p style=" margin: 0;  font-style: italic;" > I declare that the information I have appended herewith and the documents enclosed are complete and correct. If enrolled, I shall conform to the constitution, bye-laws, rules and regulations of the Institution and to the Code of Ethics. </p></td>

                    </tr>

                </tbody>
            </table>
            <div align="left" style="  margin-top: 10px;     display: -webkit-box;">
                <table class="tbl3" style="margin-left: 20px; width: 50%;">

                    <tbody>
                        <tr style="font-weight: 600;">
                            <td> 0</td>
                            <td> 2</td>
                            <td> 0</td>
                            <td> 5</td>
                            <td> 2</td>
                            <td> 0</td>
                            <td> 1</td>
                            <td> 9</td>

                        </tr>
                        <tr> 
                            <td>D</td>
                            <td>D</td>
                            <td>M</td>
                            <td>M</td>
                            <td>Y</td>
                            <td>Y</td>
                            <td>Y</td>
                            <td>Y</td>


                        </tr>


                    </tbody>

                </table>
                <table class="tbl3" style="margin-left: 10px; width: 43.5%;">

                    <tbody>
                        <tr style="font-weight: 600;">
                            <td style="padding: 10px;">  </td>


                        </tr>
                        <tr> 
                            <td>Signature of the applicant (as of NID)</td>



                        </tr>


                    </tbody>

                </table>
            </div>





        </div> 	

        <div align="left" style="  margin-top: 10px;">
            <table class="tbl3" style="margin-left: 20px;">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th> 7.0. RECOMMENDATION : I recommend him for the class of membership applied for : </th>
                    </tr>
                </thead>
            </table>

            <%                Object[] objProposer = null;
                String reqMemProposerId = "";
                String reqMemProposerMemberId = "";
                String reqMemProposerName = "";
                String reqMemProposerApproveStatus = "";
                String reqMemProposerApproveStatusText = "";
                String reqMemProposerRequestDate = "";
                String reqMemProposerAppRejDate = "";
                int pr = 1;
                String proposerContainerBox = "";
                String proposerXnNm = "";

                Query proposerSQL = dbsession.createSQLQuery("SELECT mp.id,m.member_id, m.member_name, "
                        + "mp.status, mp.request_date, mp.app_rej_date  "
                        + "FROM  member_proposer_info_temp mp,member_temp mt,member m  "
                        + "WHERE mp.member_id = '" + selectedUserId + "' AND mp.member_id = mt.id AND mp.proposer_id=m.id "
                        + "ORDER BY mp.id DESC");


            %>

            <table class="tbl3" style="margin-left: 20px;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td> </td>
                        <td style="text-align: center;">NAME (IN BLOCK LETTERS)</td>
                        <td style="text-align: center;">MEMBERSHIP NO.</td>
                        <td style="text-align: center;">SIGNATURE</td>
                        <td style="text-align: center;">DATE</td>
                    </tr>
                    <%                        if (!proposerSQL.list().isEmpty()) {
                            for (Iterator itPro = proposerSQL.list().iterator(); itPro.hasNext();) {

                                objProposer = (Object[]) itPro.next();
                                reqMemProposerId = objProposer[0].toString().trim();
                                reqMemProposerMemberId = objProposer[1].toString().trim();
                                reqMemProposerName = objProposer[2].toString().trim();
                                reqMemProposerApproveStatus = objProposer[3].toString().trim();
                                if (reqMemProposerApproveStatus.equals("0")) {
                                    // reqMemProposerApproveStatusText = "Wating for Approval";
                                    reqMemProposerApproveStatusText = "<button class=\"btn btn-warning waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-link\"></i></span>Wating for Approval</button>";
                                }
                                if (reqMemProposerApproveStatus.equals("1")) {
                                    //reqMemProposerApproveStatusText = "Approved";
                                    reqMemProposerApproveStatusText = "<button class=\"btn btn-success waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-check\"></i></span>Approved</button>";
                                }
                                if (reqMemProposerApproveStatus.equals("2")) {
                                    // reqMemProposerApproveStatusText = "Decline";
                                    reqMemProposerApproveStatusText = "<button class=\"btn btn-success waves-effect waves-light\" type=\"button\"><span class=\"btn-label\"><i class=\"fa fa-times\"></i></span>Decline</button>";
                                }
                                reqMemProposerRequestDate = objProposer[4].toString().trim();
                                reqMemProposerAppRejDate = objProposer[5] == null ? "" : objProposer[5].toString().trim();

                                proposerContainerBox = proposerContainerBox + ""
                                        + "<div class=\"col-md-4\">"
                                        + "<h5 class=\"font-bold1 text-center\">Proposer 1</h5>"
                                        + "<form class=\"form-horizontal form-material1\">"
                                        + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                        + "<div class=\"col-sm-12\">"
                                        + "<input type=\"text\" value=\"" + reqMemProposerName + "\"  class=\"form-control input-sm\" disabled>"
                                        + "</div>"
                                        + "</div>"
                                        + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                        + "<div class=\"col-sm-12\">"
                                        + "<input type=\"text\" value=\"" + reqMemProposerMemberId + "\"  class=\"form-control input-sm\" disabled>"
                                        + "</div>"
                                        + "</div>"
                                        + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                        + "<div class=\"col-sm-12\">"
                                        + "" + reqMemProposerApproveStatusText + ""
                                        + "</div>"
                                        + "</div>"
                                        + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                        + "<div class=\"col-sm-12\">"
                                        + "<input type=\"text\" value=\"" + reqMemProposerRequestDate + "\"  class=\"form-control input-sm\" disabled>"
                                        + "</div>"
                                        + "</div>"
                                        + "<div class=\"form-group row custom-bottom-margin-5x\">"
                                        + "<div class=\"col-sm-12\">"
                                        + "<input type=\"text\" value=\"" + reqMemProposerAppRejDate + "\"  class=\"form-control input-sm\" disabled>"
                                        + "</div>"
                                        + "</div>"
                                        + "</form>"
                                        + "</div>";

                                if (pr == 1) {
                                    proposerXnNm = "Proposer";
                                }
                                if (pr == 2) {
                                    proposerXnNm = "Seconder I";
                                }
                                if (pr == 3) {
                                    proposerXnNm = "Seconder II";
                                }
                                if (pr == 4) {
                                    proposerXnNm = "Seconder III";
                                }

                                if (pr == 5) {
                                    proposerXnNm = "Seconder IV";
                                }

                                if (pr == 6) {
                                    proposerXnNm = "Seconder V";
                                }


                    %>
                    <tr>
                        <td><%=proposerXnNm%></td>
                        <td><%=reqMemProposerName%></td>
                        <td><%=reqMemProposerMemberId%></td>
                        <td><%= reqMemProposerApproveStatus.equals("1") ? "Approved" : "Not Approved"%></td>
                        <td><%=reqMemProposerAppRejDate%></td>
                    </tr>
                    <%      pr++;
                            }
                        }

                    %>
                    


                </tbody>
            </table>
            <table class="tbl3" style="margin-left: 20px;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td>Note -1 : At least One Fellow and Two Members for Membership.</td>
                    </tr>
                    <tr style="font-weight: 600;">
                        <td>Note -2 : All necessary documents and photos must be attested by Proposer or Seconder.</td>

                    </tr>



                </tbody>
            </table>

        </div>


        <div align="left" style="  margin-top: 10px;     display: -webkit-box;">
            <table class="tbl3" style="margin-left: 20px; width: 50%;">

                <tbody>
                    <tr >
                        <td style="padding: 15px;     border: none;">Approved in the..............th Central Council Meeting</td>


                    </tr>
                    <tr> 
                        <td style="padding: 15px;     border: none;" >held on ............................................</td>



                    </tr>


                </tbody>

            </table>
            <table class="tbl3" style="margin-left: 10px; width: 43.5%;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td colspan="3" style="padding: 5px; text-align: center; border: none;">Countersigned by</td>


                    </tr>
                    <tr style="font-weight: 600;">
                        <td colspan="3" style="padding: 25px; text-align: center;     border: none;"> </td>


                    </tr>
                    <tr> 
                        <td style="text-align: center;     border: none;">Chairman</td>
                        <td style="text-align: center;     border: none;">Membership Committee</td>
                        <td style="text-align: center;     border: none;">Member Secretary</td>



                    </tr>


                </tbody>

            </table>
        </div>


        <div align="left" style="  margin-top: 10px;">
            <table class="tbl3" style="margin-left: 20px;">
                <thead>
                    <tr style="background-color: #00aeef; color: white;">
                        <th> 8.0. INFORMATION FOR APPLYING FOR MEMBERSHIP OF THE IEB </th>
                    </tr>
                </thead>
            </table>

            <table class="tbl3" style="margin-left: 20px;">

                <tbody>
                    <tr style="font-weight: 600;">
                        <td style="text-align: center;"  rowspan="2">Category</td>
                        <td style="text-align: center;" colspan="6">Subscription Rate</td>
                        <td style="text-align: center;" >Others</td>


                    </tr>
                    <tr style="font-weight: 600;"> 
                        <td style="text-align: center;">Entrance Fee (TK.)</td>
                        <td style="text-align: center;">Annual Subscription (TK.)</td>
                        <td style="text-align: center;" >Diploma Fee (TK.)</td>
                        <td style="text-align: center;">ID Card Fee (TK.)</td>
                        <td style="text-align: center;">Total (TK.)</td>
                        <td style="text-align: center;" >Age in Years(At least)</td>
                        <td style="text-align: center;">Experience (Minimum)</td>
                    </tr>
                    <tr> 
                        <td style="text-align: center;" >Member</td>
                        <td style="text-align: center;" >1000.00</td>
                        <td style="text-align: center;" >600.00</td>
                        <td style="text-align: center;" >200.00</td>
                        <td style="text-align: center;" >100.00</td>
                        <td style="text-align: center;" >1,900.00</td>
                        <td style="text-align: center;" >27</td>
                        <td style="text-align: center;">3 years(if Non Member) <br> 2 years(if Non Member)</td>
                    </tr> 
                </tbody>

            </table>

            <div align="left" style="  margin-top: 10px; margin-left: 20px;" class="tbl3" >
                <p style="font-weight: 600;"> Please Enclose attested copies :
                <ul style="list-style: none;">
                    <li>1. Recent Photo P.P. Size (2 copies).</li>
                    <li>2. Certificates: SSC/Equivalent, HSC/Equivalent, B.Sc.Engg./Equivalent. </li>
                    <li>3. Transcripts: HSC/Equivalent, B.Sc. Engg./Equivalent. </li>
                    <li>4. Certificates of other Professional Bodies (if any). </li>
                    <li>5. NID (Photocopy).</li>
                    <li>6. Recognized Training record for last two years.</li>
                    <li>7. 1500 words on experience demonstrating competencies and abilities.</li>

                </ul>
                </p>
            </div>

        </div> 
        <table class="tbl3">
            <thead>
                <tr style="background-color: #00aeef; color: white;">
                    <th style="text-align: center;"> new.iebbd.org </th>
                </tr>
            </thead>
        </table>
    </div>

    <%    dbsession.clear();
        dbsession.close();
    %>
</body>

</html>