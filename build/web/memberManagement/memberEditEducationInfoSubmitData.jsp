
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();


    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
     if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        memberIdH = request.getParameter("memberEditId").trim();

        System.out.println("memberIdH " + memberIdH);

        getRegistryID regId = new getRegistryID();

        MemberEducationInfo memberEducationInfo = null;
        String act = request.getParameter("act").trim();

        if (act.equals("edit")) {
            String educationfInfoId = request.getParameter("educationfInfoId").trim();
            Query q4 = dbsession.createQuery("from MemberEducationInfo where id='" + educationfInfoId + "' ");
            Iterator itr4 = q4.list().iterator();
            if (itr4.hasNext()) {
                memberEducationInfo = (MemberEducationInfo) itr4.next();
            }

            String memberDegreeTypeId = request.getParameter("memberDegreeTypeId").trim();
            String memberBoardUniversity = request.getParameter("memberBoardUniversity").trim();
            String institutionName = request.getParameter("institutionName").trim();
            String memberPassingYear = request.getParameter("memberPassingYear").trim();
            String memberResultTypeId = request.getParameter("memberResultTypeId").trim();
            String memberScoreClass = request.getParameter("memberScoreClass").trim();
  
            Member member = new Member();
            University university = new University();
            Degree degree = new Degree();
            ResultType resultType = new ResultType();
            int memberId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);

            member.setId(memberId1); 
            memberEducationInfo.setMember(member);
            memberEducationInfo.setInstituteName(institutionName);
            university.setUniversityId(Integer.parseInt(memberBoardUniversity));
            memberEducationInfo.setUniversity(university);

            degree.setDegreeId(Integer.parseInt(memberDegreeTypeId));
            memberEducationInfo.setDegree(degree);

            resultType.setResultTypeId(Integer.parseInt(memberResultTypeId));
            memberEducationInfo.setResultType(resultType);
            memberEducationInfo.setYearOfPassing(memberPassingYear);
            memberEducationInfo.setResult(memberScoreClass);
            dbsession.update(memberEducationInfo);
            strMsg = "Information updated successfully.";
        } else if (act.equals("add")) {

            String memberDegreeTypeId = request.getParameter("memberDegreeTypeId").trim();
            String memberBoardUniversity = request.getParameter("memberBoardUniversity").trim();
            String institutionName = request.getParameter("institutionName").trim();
            String memberPassingYear = request.getParameter("memberPassingYear").trim();
            String memberResultTypeId = request.getParameter("memberResultTypeId").trim();
            String memberScoreClass = request.getParameter("memberScoreClass").trim();

            String educationfInfoId = regId.getID(42);
            memberEducationInfo = new MemberEducationInfo();
            Member member = new Member();
            University university = new University();
            Degree degree = new Degree();
            ResultType resultType = new ResultType();
            
            int memberId1 = 0;
            int educationfInfoId1 = 0;
            String memberId = request.getParameter("memberEditId").trim();
            memberId1 = Integer.parseInt(memberId);
            educationfInfoId1 = Integer.parseInt(educationfInfoId);

            member.setId(memberId1);
            memberEducationInfo.setId(educationfInfoId1);
            memberEducationInfo.setMember(member);
            memberEducationInfo.setInstituteName(institutionName);
            university.setUniversityId(Integer.parseInt(memberBoardUniversity));
            memberEducationInfo.setUniversity(university);

            degree.setDegreeId(Integer.parseInt(memberDegreeTypeId));
            memberEducationInfo.setDegree(degree);

            resultType.setResultTypeId(Integer.parseInt(memberResultTypeId));
            memberEducationInfo.setResultType(resultType);
            memberEducationInfo.setYearOfPassing(memberPassingYear);
            memberEducationInfo.setResult(memberScoreClass);
            dbsession.save(memberEducationInfo);
            strMsg = "Information saved successfully.";

        } else if (act.equals("del")) {
            String educationfInfoId = request.getParameter("educationfInfoId").trim();
            Query q4 = dbsession.createQuery("delete from MemberEducationInfo where id='" + educationfInfoId + "' ");
            q4.executeUpdate();
            strMsg = "Information removed successfully.";
        }

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            strMsg = "Success!!! Education Information added ";
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=education&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! When Education Information added ";
            dbtrx.rollback();
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=education&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
        return;
    }

%>
