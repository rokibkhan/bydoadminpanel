<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

        int ix = 1;

        // String targetpage = "memberListFellow.jsp";
        String targetpageWithSession = "voterListDivisionSubmit.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        int limit = 100;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        Query subCenterTopSQL = null;
        Object[] subCenterTopObj = null;
        String subCenterTopId = "";
        String subCenterTopStr = "";
        String scTopNoORStr = "";
        int scTopNo = 1;

        String cenSQLStr = "";
        String cenpStr = "";
        String divSQLStr = "";
        String typeSQLStr = "";
        String uniSQLStr = "";
        String passYrSQLStr = "";

        String voterFeeYearId = request.getParameter("voterFeeYearId") == null ? "" : request.getParameter("voterFeeYearId").trim();

        String memberSearchDivisionId = request.getParameter("memberSearchDivisionId") == null ? "" : request.getParameter("memberSearchDivisionId").trim();
        String memberSearchDivisionName = request.getParameter("memberSearchDivisionName") == null ? "" : request.getParameter("memberSearchDivisionName").trim();
        
        
        //only Fee Year
        if (!voterFeeYearId.equals("") && memberSearchDivisionId.equals("")) {
            //  y.member_fees_year_name='2017-2018'"
            //   filterSQLStr = " AND y.member_fees_year_name='" + voterFeeYearId + "' ";
            filterSQLStr = "AND m.id in ( "
                    + "SELECT distinct member_id from member_fee f,member_fees_year y where y.member_fees_year_id=f.fee_year "
                    + "AND y.member_fees_year_name='" + voterFeeYearId + "' union all "
                    + "SELECT distinct member_id from member_fee f,member_fees_year y where y.member_fees_year_id=f.fee_year and y.member_fees_year_name='LIFE'"
                    + ") ";

            filterp = "&voterFeeYearId=" + voterFeeYearId + "&";

        } //only Division
        else if (!memberSearchDivisionId.equals("") && voterFeeYearId.equals("")) {
            // filterSQLStr = " AND m.member_division_id=" + memberSearchDivisionId + " ";

            filterSQLStr = "AND m.member_division_id=" + memberSearchDivisionId + " AND m.id in ( "
                    + "SELECT distinct member_id from member_fee f,member_fees_year y where y.member_fees_year_id=f.fee_year and y.member_fees_year_name='LIFE'"
                    + ") ";
            filterp = "&memberSearchDivisionId=" + memberSearchDivisionId + "&";
        } //Fee Year and Division
        else if (!memberSearchDivisionId.equals("") && !voterFeeYearId.equals("")) {
            // filterSQLStr = " AND m.member_division_id=" + memberSearchDivisionId + " ";

            filterSQLStr = "AND m.member_division_id=" + memberSearchDivisionId + " AND m.id in ( "
                    + "SELECT distinct member_id from member_fee f,member_fees_year y where y.member_fees_year_id=f.fee_year "
                    + "AND y.member_fees_year_name='" + voterFeeYearId + "' UNION ALL "
                    + "SELECT distinct member_id from member_fee f,member_fees_year y where y.member_fees_year_id=f.fee_year and y.member_fees_year_name='LIFE'"
                    + ") ";
            filterp = "&voterFeeYearId=" + voterFeeYearId + "&memberSearchDivisionId=" + memberSearchDivisionId + "&";
        } //all Blank
        else if (voterFeeYearId.equals("") && memberSearchDivisionId.equals("")) {
            filterSQLStr = "AND m.id in ( "
                    + "SELECT distinct member_id from member_fee f,member_fees_year y where y.member_fees_year_id=f.fee_year and y.member_fees_year_name='LIFE'"
                    + ") ";
            filterp = "&";

        }
//        else {
//            filterSQLStr = "AND m.id in ( "
//                    + "SELECT distinct member_id from member_fee f,member_fees_year y where y.member_fees_year_id=f.fee_year and y.member_fees_year_name='LIFE'"
//                    + ") ";
//            filterp = "&";
//        }

        System.out.println("filterp :: " + filterp);

        System.out.println("filterSQLStr :: " + filterSQLStr);

        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
        int pageNbr = Integer.parseInt(pageNbr1);

        System.out.println("pageNbr :: " + pageNbr);

        String memberId = "";
        String memberIEBId = "";
        String memberName = "";
        String memberPictureName = "";
        String memberPictureUrl = "";
        String memberPicture = "";
        String memberMobile = "";
        String memberEmail = "";
        String memberAddress = "";

        String status1 = "";
        String mOthersInfo = "";
        String memberUniversityName = "";
        String memberDivisionName = "";
        String memberCenterName = "";
        String btnCompletePoll = "";
        String committeeMemberBtn = "";
        String agrX = "";
    %>

    <script type="text/javascript">

        function showMemebrSearchSubCenterInfo(arg1) {


            console.log("showMemebrSearchSubCenterInfo arg1:: " + arg1);
            var arg2 = "";

            $.post("memberCenterWiseSubCenterInfoShow.jsp", {centerId: arg1, addressInfo: arg2}, function (data) {

                console.log(data);
//                if (data[0].responseCode == 1) {
//                    $("#showSubCenterInfo").html(data[0].subCenterInfo);
//                }

                $("#showSubCenterInfo").html(data[0].subCenterInfo);


            }, "json");
        }

        function showMemebrSearchDivisionNameInfo(arg1) {

            var selectedDivisionId, selectedDivisionText, selectedDivisionName;
            console.log("showMemebrSearchDivisionNameInfo arg1:: " + arg1);
            var arg2 = "";

            selectedDivisionId = $("#memberSearchDivisionId").val();

            if (selectedDivisionId != "") {
                selectedDivisionText = $("#memberSearchDivisionId option:selected").text();
                document.getElementById("memberSearchDivisionName").value = selectedDivisionText
            } else {
                selectedDivisionText = "";
                document.getElementById("memberSearchDivisionName").value = "";
            }

            selectedDivisionName = $("#memberSearchDivisionName").val();

            console.log("selectedDivisionId::" + selectedDivisionId);
            console.log("selectedDivisionText::" + selectedDivisionText);
            console.log("selectedDivisionName::" + selectedDivisionName);

        }

        $(function () {
            console.log("ready!");


            $('#searchStockItem').click(function () {
                $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListDivisionSubmit.jsp?sessionid=<%out.print(sessionid);%>');

                            $("#stockReportFrm").submit();
                        });


                        $('#exportStockItem').click(function () {
                            $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListDivisionExport.jsp?sessionid=<%out.print(sessionid);%>');
                                        //    target="_blank"
                                        $('#stockReportFrm').attr('target', '_blank');
                                        $("#stockReportFrm").submit();
                                    });


//        $('input').keydown(function (e) {
//            //      $("input").keypress(function (event) {
//            if (e.which == 13) {
//                e.preventDefault();
//                $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/stockReport.jsp?sessionid=<%out.print(sessionid);%>');
//                $("#stockReportFrm").submit();
//            }
//        });




                                    $("#stockReportFrm").keypress(function (event) {
                                        if (event.which == 13) {
                                            event.preventDefault();
                                            $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListDivisionSubmit.jsp?sessionid=<%out.print(sessionid);%>');
                                                            $("#stockReportFrm").submit();
                                                        }
                                                    });



                                                });

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Division Voter List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Division Voter</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">
                    <form class="form-horizontal" name="stockReportFrm" id="stockReportFrm" method="GET">

                        <input type="hidden" id="sessionid" name="sessionid" value="<%out.print(sessionid);%>"> 
                        <input type="hidden" id="memberSearchDivisionName" name="memberSearchDivisionName" value="<%=memberSearchDivisionName%>">

                        <div class="row">
                            <div class="col-md-3 offset-2">

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-4 font-weight-bold text-right">Fee Year</label>
                                    <div class="col-md-8">  
                                        <select name="voterFeeYearId" id="voterFeeYearId" class="form-control" required>
                                            <option value="">Select Fee Year</option>
                                            <%                                                            
                                                Object[] feeYearObj = null;
                                                Query feeYearSQL = null;

                                                String feeYearId = "";
                                                String feeYearName = "";
                                                String feeYearOptions = "";
                                                String feeYearOptionsSelect = "";
                                                int length = 0;
                                                feeYearSQL = dbsession.createSQLQuery("select * FROM member_fees_year ORDER BY member_fees_year_id ASC");
                                                System.out.println("feeYearSQL :: " + feeYearSQL);
                                                for (Iterator feeYearItr = feeYearSQL.list().iterator(); feeYearItr.hasNext();) {
                                                    feeYearObj = (Object[]) feeYearItr.next();
                                                    feeYearId = feeYearObj[0].toString();
                                                    // divisionEditName = objectEditDiv[1].toString();
                                                    feeYearName = feeYearObj[1].toString();

                                                    length = feeYearName.length(); // length == 8

                                                    if (!voterFeeYearId.equals("")) {

                                                        if (voterFeeYearId.equals(feeYearName)) {
                                                            feeYearOptionsSelect = " selected";
                                                        } else {
                                                            feeYearOptionsSelect = "";
                                                        }

                                                    }
                                                    if (length == 9) {

                                                        feeYearOptions = feeYearOptions + "<option value=\"" + feeYearName + "\" " + feeYearOptionsSelect + ">" + feeYearName + "</option>";

                                                    }

                                                }
                                            %>
                                            <%=feeYearOptions%>
                                        </select>
                                    </div>
                                </div>                                
                            </div>

                            <div class="col-md-5">

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 font-weight-bold text-right">Division</label>
                                    <div class="col-md-8">  
                                        <select name="memberSearchDivisionId" id="memberSearchDivisionId" class="form-control" onchange="showMemebrSearchDivisionNameInfo(this.value)" required>
                                            <option value="">Select Division</option>
                                            <%
                                                Object[] objectEditDiv = null;
                                                String divisionEditId = "";
                                                String divisionEditName = "";
                                                String divisionEditOptions = "";
                                                String divisionEditOptionsSelect = "";
                                                Query divisionEditSQL = dbsession.createSQLQuery("select * FROM member_division ORDER BY mem_division_name ASC");
                                                for (Iterator divisionEditItr = divisionEditSQL.list().iterator(); divisionEditItr.hasNext();) {
                                                    objectEditDiv = (Object[]) divisionEditItr.next();
                                                    divisionEditId = objectEditDiv[0].toString();
                                                    // divisionEditName = objectEditDiv[1].toString();
                                                    divisionEditName = objectEditDiv[3].toString();

                                                    if (!memberSearchDivisionId.equals("")) {

                                                        if (memberSearchDivisionId.equals(divisionEditId)) {
                                                            divisionEditOptionsSelect = " selected";
                                                        } else {
                                                            divisionEditOptionsSelect = "";
                                                        }

                                                    }

                                                    divisionEditOptions = divisionEditOptions + "<option value=\"" + divisionEditId + "\" " + divisionEditOptionsSelect + ">" + divisionEditName + "</option>";

                                                }
                                            %>
                                            <%=divisionEditOptions%>
                                        </select>
                                    </div>
                                </div>                                
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-4 offset-5">      
                                <input type="button"  id="searchStockItem" name="searchStockItem" class="btn btn-info" value="Submit">
                                <input type="button"  id="exportStockItem" name="exportStockItem" class="btn btn-info" value="Export">
                            </div>
                        </div>

                    </form>
                </div>
                <!-- /.white-box -->
            </div>

            <div class="col-md-12">
                <div class="white-box">




                    <%
                        String searchCountSQL = "SELECT count(*) "
                                + "FROM member m,member_type mt "
                                + "WHERE m.status = 1 "
                                + "AND m.id = mt.member_id "
                                + "" + filterSQLStr + " "
                                + "ORDER BY m.id DESC";

                        String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        if (pageNbr != 0) {
                            start = (pageNbr - 1) * limit;
                        } else {
                            start = 0;
                        }

                        System.out.println("startLLLL :: " + start);
                        System.out.println("limitLLLL :: " + limit);

                        // Initial page num setup
                        if (pageNbr == 0) {
                            pageNbr = 1;
                        }
                        prev = pageNbr - 1;
                        next = pageNbr + 1;
                        lastpage1 = Math.ceil((double) total_pages / limit);
                        //   DecimalFormat df = new DecimalFormat("#.##");
                        //  System.out.print(df.format(lastpage1));

                        // lastpage = (int) Math.ceil(total_pages / limit);
                        lastpage = (int) lastpage1;
                        LastPagem1 = (int) Math.round(lastpage - 1);

                        System.out.println("total_pages :: " + total_pages);
                        System.out.println("limit :: " + limit);
                        System.out.println("lastpage1 :: " + lastpage1);
                        System.out.println("lastpage :: " + lastpage);
                        System.out.println("LastPagem1 :: " + LastPagem1);

                        String paginate = "";
                        if (lastpage > 1) {
                            paginate = paginate + "<ul class='pagination' style=\"margin:0\">";

                            // Previous
                            if (pageNbr > 1) {
                                p = "page=" + prev;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>previous</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
                            }

                            int counter = 1;
                            // Pages
                            if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

                                for (counter = 1; counter <= lastpage; counter++) {
                                    if (counter == pageNbr) {
                                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                    } else {
                                        p = "page=" + counter;
                                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                    }
                                }
                            } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
                                // Beginning only hide later pages
                                if (pageNbr < 1 + (stages * 2)) {
                                    for (counter = 1; counter < 4 + (stages * 2); counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // Middle hide some front and some back
                                else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // End only hide early pages
                                else {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                                    for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                }
                            }

                            // Next
                            if (pageNbr < counter - 1) {
                                p = "page=" + next;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>next</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
                            }

                            paginate = paginate + "</ul>";
                        }

                        int minPaginationIndexLimit = ((pageNbr - 1) * limit);
                        int maxPaginationIndexLimit = (pageNbr * limit);
                        int lastProductIndex = total_pages;
                        int minPaginationIndexLimitPlus = minPaginationIndexLimit + 1;
//                        if(minPaginationIndexLimitPlus < limit){
//                            minPaginationIndexLimitPlus = 1;
//                        }

                        if (maxPaginationIndexLimit >= lastProductIndex) {
                            maxPaginationIndexLimit = lastProductIndex;
                        }

                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
                                + "md.mem_division_name,c.center_name   "
                                + "FROM  member m ,member_type mt,member_division md,center c "
                                + "WHERE m.status = 1 "
                                + "AND m.member_division_id = md.mem_division_id "
                                + "AND m.center_id = c.center_id "
                                + "AND m.id = mt.member_id "
                                + "" + filterSQLStr + " "
                                + "ORDER BY m.id DESC  LIMIT " + start + ", " + limit + "";

                        System.out.println("searchCountSQL :: " + searchCountSQL);
                        System.out.println("searchSQL :: " + searchSQL);

                        // out.println("searchCountSQL :: " + searchCountSQL + "<br/><br/>");
                    //    out.println("searchSQL :: " + searchSQL);


                    %>
                    <div class="row">

                        <div class="col-md-8" style="padding: 20px 0;">
                            <%=paginate%>
                        </div>

                        <div class="col-md-4" style="padding-top: 23px;">
                            <p class="text-right">
                                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">#</th>
                                        <th class="text-center" style="width: 5%;">Picture</th>
                                        <th class="text-center" style="width: 6%;">IEB ID</th>
                                        <th class="text-center" style="width: 20%;">Name</th>
                                        <th class="text-center">Mobile</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center" style="width: 20%;">Others Info</th>
                                        <th class="text-center" style="width: 5%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <%
                                        Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                                        if (!searchSQLQry.list().isEmpty()) {
                                            for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                                                searchObj = (Object[]) searchItr.next();
                                                memberId = searchObj[0].toString();
                                                memberIEBId = searchObj[1] == null ? "" : searchObj[1].toString();
                                                memberName = searchObj[2] == null ? "" : searchObj[2].toString();

                                                memberPictureName = searchObj[3] == null ? "" : searchObj[3].toString();
                                                memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPictureName;
                                                memberPicture = "<img width=\"120\" src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\">";

                                                memberMobile = searchObj[4] == null ? "" : searchObj[4].toString();
                                                memberEmail = searchObj[5] == null ? "" : searchObj[5].toString();

                                                memberDivisionName = searchObj[7] == null ? "" : searchObj[7].toString();
                                                memberCenterName = searchObj[8] == null ? "" : searchObj[8].toString();

                                                mOthersInfo = "<strong>Division:</strong>" + memberDivisionName + "<br>"
                                                        + "<strong>Center:</strong>" + memberCenterName;
                                                String memberDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/memberDetails.jsp?sessionid=" + session.getId() + "&memberId=" + memberId + "&selectedTab=profile";
                                    %>
                                    <tr id="infoBox<%=memberId%>">
                                        <td><% out.print(minPaginationIndexLimitPlus);%></td>
                                        <td><%=memberPicture%></td>
                                        <td><%=memberIEBId%></td>
                                        <td><%=memberName%></td>
                                        <td><%=memberMobile%></td>
                                        <td><%=memberEmail%></td>
                                        <td><%=mOthersInfo%></td>
                                        <td class="text-center">

                                            <a title="Details" href="<%=memberDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></a>


                                        </td>
                                    </tr>
                                    <%
                                            minPaginationIndexLimitPlus++;
                                        }
                                    } else {
                                    %>
                                    <tr>
                                        <td colspan="8" class="text-center">There no data found</td>

                                    </tr>
                                    <%
                                        }
                                    %>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <%
                        //  }
                        dbsession.clear();
                        dbsession.close();
                    %>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <%=paginate%>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>