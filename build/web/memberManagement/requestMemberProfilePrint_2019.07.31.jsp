<%-- 
    Document   : trialBalanceReportPrint
    Created on : Sep 15, 2010, 9:28:45 PM
    Author     : Akter
--%>

<%@page import="net.sf.jasperreports.engine.*"%>  
<%@page import="org.hibernate.internal.SessionImpl"%> 
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<meta http-equiv="no-cache">
<meta http-equiv="no-store">


<%

    String memberId = request.getParameter("memberId").trim();
    String pictureName = request.getParameter("pictureName").trim();
//    String signatureImageName = request.getParameter("signatureImageName").trim();
    String signatureImageName = "";
    String imgUrl = GlobalVariable.imageMemberDirLink + "/" + pictureName;
    String signatureImageUrl = GlobalVariable.imageMemberDirLink + signatureImageName;

    File reportFile = null;
    Session dbsession = HibernateUtil.getSessionFactory().openSession();

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    File imageFile = new File(imgUrl);
    if (!(imageFile.exists() && imageFile.isFile())) {
        imgUrl = GlobalVariable.imageMemberDirLink + "/no_image.jpg";
    }
    File imageFile1 = new File(signatureImageUrl);
    if (!(imageFile1.exists() && imageFile1.isFile())) {
        signatureImageUrl = GlobalVariable.imageMemberDirLink + "no_image.png";
    }

    if (memberId != null) {
        reportFile = new File(application.getRealPath("/Reports/TempMemberProfile.jasper"));

        Map parameters = new HashMap();
        parameters.put("memberId", memberId);
        parameters.put("imgUrl", imgUrl);
        parameters.put("signatureImageUrl", signatureImageUrl);
        byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameters, con);
        response.setContentType("application.pdf");

        response.setContentLength(bytes.length);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(bytes, 0, bytes.length);
        outputStream.flush();
        outputStream.close();
        response.flushBuffer();

    }
    con.close();
    dbsession.close();

%>


