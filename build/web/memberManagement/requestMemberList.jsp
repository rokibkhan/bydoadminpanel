<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        // DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormatEventM = new SimpleDateFormat("MMM");
        DateFormat dateFormatEventD = new SimpleDateFormat("dd");
        DateFormat dateFormatEventY = new SimpleDateFormat("Y");

        Date dateNewsM = null;
        Date dateNewsD = null;
        Date dateNewsY = null;
        String newDateNewsM = "";
        String newDateNewsD = "";
        String newDateNewsY = "";

        //   dateNewsM = dateFormatEvent.parse(applicationDate);
        //   newDateNewsM = dateFormatEventM.format(dateNewsM);
        //   dateNewsD = dateFormatEvent.parse(applicationDate);
        //   newDateNewsD = dateFormatEventD.format(dateNewsD);
        //   dateNewsY = dateFormatEvent.parse(applicationDate);
        //   newDateNewsY = dateFormatEventY.format(dateNewsY);
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        out.print("<h2 align=\"center\">" + ft.format(dNow) + "</h2>");

        String searchQuery1 = "";
        String filterSQLStr = "";
        Object searchObj[] = null;

        int ix = 1;

        String targetpageWithSession = "requestMemberList.jsp?sessionid=" + session.getId() + "&";
        String filterp = "";
        int limit = 20;
        int total_pages = 0;
        int stages = 3;
        int start = 0;
        String p = "";
        String p1 = "";
        String p2 = "";

        int prev = 0;
        int next = 0;
        double lastpage1 = 0;
        int lastpage = 0;
        int LastPagem1 = 0;

        String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();

        if (!searchString.equals("")) {
            filterSQLStr = " AND (m.member_name like '%" + searchString + "%' OR m.email_id like '%" + searchString + "%' OR m.member_id like '%" + searchString + "%' OR m.mobile like '%" + searchString + "%' OR m.place_of_birth like '%" + searchString + "%' )";
            filterp = "&searchString=" + searchString + "&";

        } else {

            filterSQLStr = " ";
            filterp = "&searchString=" + searchString + "&";
        }

        String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
        int pageNbr = Integer.parseInt(pageNbr1);

        String reqMemId = "";
        String reqMemRegId = "";
        String reqMemName = "";
        String reqMemStatusText = "";
        String applicationDate = "";
        String committeeDuration = "";
        String committeeDesc = "";
        String committeeStatus = "";

        String status1 = "";
        String reqMemStatus = "";
        String btnActivePoll = "";
        String btnCompletePoll = "";
        String committeeMemberBtn = "";
        String agrX = "";

        Object memberPaymentObj[] = null;

        String reqMemPaymentAmount = "";
        String reqMemPaymentStatus = "";
        String reqMemPaymentStatusText = "";

        String reqMemDetailsUrl = "";
    %>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">MemberShip Request List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">MemberShip</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group"  name="memberListAll" id="memberListAll" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/requestMemberList.jsp?sessionid=<%out.print(sessionid);%>" >
                        <input type="hidden" id="sessionid" name="sessionid" value="<%out.print(sessionid);%>"> 
                        <div class="input-group">
                            <input type="text" id="searchString" name="searchString" value="<%=searchString%>" class="form-control" placeholder="Membership ID Or Name Or Mobile Number"> 
                            <span class="input-group-btn">
                                <button type="submit" name="btnSearch" class="btn waves-effect waves-light btn-info">
                                    <i class="fa fa-search"></i> Search Member
                                </button>
                            </span> 
                        </div>
                    </form>


                    <%
                        //  String searchCountSQL = "SELECT count(*) FROM  member m,member_type mt "
                        //          + "WHERE m.status = 1 "
                        //          + "AND m.id = mt.member_id "
                        //          + " " + filterSQLStr + " "
                        //         + "ORDER BY m.id DESC";

                        String searchCountSQL = "SELECT  count(*) FROM  member_temp m,member_fee_temp mf  "
                                + "WHERE m.id=mf.member_id  "
                                + "AND mf.status = '1'  "
                                + " " + filterSQLStr + " "
                                + "ORDER BY m.id DESC";

                        String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        if (pageNbr != 0) {
                            start = (pageNbr - 1) * limit;
                        } else {
                            start = 0;
                        }

                        // Initial page num setup
                        if (pageNbr == 0) {
                            pageNbr = 1;
                        }
                        prev = pageNbr - 1;
                        next = pageNbr + 1;
                        lastpage1 = Math.ceil((double) total_pages / limit);
                        //   DecimalFormat df = new DecimalFormat("#.##");
                        //  System.out.print(df.format(lastpage1));

                        // lastpage = (int) Math.ceil(total_pages / limit);
                        lastpage = (int) lastpage1;
                        LastPagem1 = (int) Math.round(lastpage - 1);

                        String paginate = "";
                        if (lastpage > 1) {
                            paginate = paginate + "<ul class='pagination' style=\"margin:0\">";

                            // Previous
                            if (pageNbr > 1) {
                                p = "page=" + prev;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>previous</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
                            }

                            int counter = 1;
                            // Pages
                            if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

                                for (counter = 1; counter <= lastpage; counter++) {
                                    if (counter == pageNbr) {
                                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                    } else {
                                        p = "page=" + counter;
                                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                    }
                                }
                            } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
                                // Beginning only hide later pages
                                if (pageNbr < 1 + (stages * 2)) {
                                    for (counter = 1; counter < 4 + (stages * 2); counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // Middle hide some front and some back
                                else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                    paginate = paginate + "<li><a>...</a></li>";
                                    p1 = "page=" + LastPagem1;
                                    p2 = "page=" + lastpage;
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
                                } // End only hide early pages
                                else {
                                    p1 = "page=1";
                                    p2 = "page=2";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                                    //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                                    paginate = paginate + "<li><a>...</a></li>";
                                    //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                                    for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                                        if (counter == pageNbr) {
                                            paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                                        } else {
                                            p = "page=" + counter;
                                            //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                                            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                                        }
                                    }
                                }
                            }

                            // Next
                            if (pageNbr < counter - 1) {
                                p = "page=" + next;
                                // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
                                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
                            } else {
                                //	$paginate.= "<span class='disabled'>next</span>";
                                paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
                            }

                            paginate = paginate + "</ul>";
                        }

                        int minPaginationIndexLimit = ((pageNbr - 1) * limit);
                        int maxPaginationIndexLimit = (pageNbr * limit);
                        int lastProductIndex = total_pages;
                        int minPaginationIndexLimitPlus = minPaginationIndexLimit + 1;
//                        if(minPaginationIndexLimitPlus < limit){
//                            minPaginationIndexLimitPlus = 1;
//                        }

                        if (maxPaginationIndexLimit >= lastProductIndex) {
                            maxPaginationIndexLimit = lastProductIndex;
                        }

//                        String searchSQL = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "md.mem_division_name,c.center_name,   "
//                                + "mt.member_type_id "
//                                + "FROM  member m ,member_type mt,member_division md,center c "
//                                + "WHERE m.status = 1 "
//                                + "AND m.member_division_id = md.mem_division_id "
//                                + "AND m.center_id = c.center_id "
//                                + "AND m.id = mt.member_id "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY m.id DESC  "
//                                + "LIMIT " + start + ", " + limit + "";
                        String searchSQL = "SELECT m.id, m.member_id, m.member_temp_id, "
                                + "m.member_temp_pass, m.member_name, m.father_name, m.mother_name, "
                                + "m.place_of_birth,m.dob ,m.status,m.add_date,mf.amount,mf.paid_date  "
                                + "FROM  member_temp m,member_fee_temp mf  "
                                + "WHERE m.id=mf.member_id  "
                                + "AND mf.status = '1'  "
                                + " " + filterSQLStr + " "
                                + "ORDER BY m.id DESC  "
                                + "LIMIT " + start + ", " + limit + "";
                    %>

                    <div class="row">

                        <div class="col-md-8" style="padding: 20px 0;">
                            <%=paginate%>
                        </div>

                        <div class="col-md-4" style="padding-top: 23px;">
                            <p class="text-right">
                                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
                            </p>

                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center" style="width: 30%;">Name</th>
                                    <th class="text-center">Registration ID</th>
                                    <th class="text-center">Payment</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%

                                    Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                                    if (!searchSQLQry.list().isEmpty()) {
                                        for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                                            searchObj = (Object[]) searchItr.next();
                                            reqMemId = searchObj[0].toString().trim();
                                            reqMemRegId = searchObj[2] == null ? "" : searchObj[2].toString().trim();
                                            reqMemName = searchObj[4] == null ? "" : searchObj[4].toString().trim();
                                            reqMemStatus = searchObj[9] == null ? "" : searchObj[9].toString().trim();

                                            if (reqMemStatus.equals("0")) {
                                                reqMemStatusText = "<a class=\"btn btn-info waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-undo\"></i> Pending</a>";
                                            }

                                            if (reqMemStatus.equals("1")) {
                                                reqMemStatusText = "<a class=\"btn btn-success waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-check\"></i> Approved</a>";
                                            }
                                            if (reqMemStatus.equals("2")) {
                                                reqMemStatusText = "<a class=\"btn btn-danger waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-times-circle\"></i> Declined</a>";
                                            }

                                            reqMemPaymentAmount = searchObj[11] == null ? "" : searchObj[11].toString().trim();

                                            //  applicationDate = searchObj[10] == null ? "" : searchObj[10].toString().trim();
                                            applicationDate = searchObj[12] == null ? "" : searchObj[12].toString();

                                            dateNewsM = dateFormatEvent.parse(applicationDate);
                                            newDateNewsM = dateFormatEventM.format(dateNewsM);

                                            dateNewsD = dateFormatEvent.parse(applicationDate);
                                            newDateNewsD = dateFormatEventD.format(dateNewsD);

                                            dateNewsY = dateFormatEvent.parse(applicationDate);
                                            newDateNewsY = dateFormatEventY.format(dateNewsY);

                                            reqMemDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/requestMemberDetails.jsp?sessionid=" + session.getId() + "&reqMemId=" + reqMemId + "&selectedTab=profile";


                                %>


                                <tr id="infoBox<%=reqMemId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(reqMemName);%></td>
                                    <td><% out.print(reqMemRegId);%></td>
                                    <td class="text-center"><% out.print(reqMemPaymentAmount);%> <br/><%=reqMemPaymentStatusText%></td>
                                    <td class="text-center"><% out.print(reqMemStatusText);%></td>
                                    <td><% out.print(dateNewsD);%><% //out.print(newDateNewsM);%> <% //out.print(newDateNewsY);%></td>
                                    <td class="text-center">

                                        <a title="Details" href="<%=reqMemDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                %>



                            </tbody>
                        </table>
                    </div>



                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>