<%@page import="java.lang.Math.*"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();

        String voterFeeYearId = request.getParameter("voterFeeYearId") == null ? "" : request.getParameter("voterFeeYearId").trim();

        String memberSearchDivisionId = request.getParameter("memberSearchDivisionId") == null ? "" : request.getParameter("memberSearchDivisionId").trim();
     String memberSearchDivisionName = request.getParameter("memberSearchDivisionName") == null ? "" : request.getParameter("memberSearchDivisionName").trim();

    %>

    <script type="text/javascript">

        function showMemebrSearchSubDivisionInfo(arg1) {


            console.log("showMemebrSearchSubDivisionInfo arg1:: " + arg1);
            var arg2 = "";

            $.post("memberDivisionWiseSubDivisionInfoShow.jsp", {centerId: arg1, addressInfo: arg2}, function (data) {

                console.log(data);
//                if (data[0].responseCode == 1) {
//                    $("#showSubDivisionInfo").html(data[0].subDivisionInfo);
//                }

                $("#showSubDivisionInfo").html(data[0].subDivisionInfo);


            }, "json");
        }

        function showMemebrSearchDivisionNameInfo(arg1) {

            var selectedDivisionId, selectedDivisionText, selectedDivisionName;
            console.log("showMemebrSearchDivisionNameInfo arg1:: " + arg1);
            var arg2 = "";

            selectedDivisionId = $("#memberSearchDivisionId").val();

            if (selectedDivisionId != "") {
                selectedDivisionText = $("#memberSearchDivisionId option:selected").text();
                document.getElementById("memberSearchDivisionName").value = selectedDivisionText
            } else {
                selectedDivisionText = "";
                document.getElementById("memberSearchDivisionName").value = "";
            }

            selectedDivisionName = $("#memberSearchDivisionName").val();

            console.log("selectedDivisionId::" + selectedDivisionId);
            console.log("selectedDivisionText::" + selectedDivisionText);
            console.log("selectedDivisionName::" + selectedDivisionName);

        }


        $(function () {
            console.log("ready!");


            $('#searchStockItem').click(function () {
                $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListDivisionSubmit.jsp?sessionid=<%out.print(sessionid);%>');

                            $("#stockReportFrm").submit();
                        });


                        $('#exportStockItem').click(function () {
                            $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListDivisionExport.jsp?sessionid=<%out.print(sessionid);%>');
                                        //    target="_blank"
                                        $('#stockReportFrm').attr('target', '_blank');
                                        $("#stockReportFrm").submit();
                                    });


//        $('input').keydown(function (e) {
//            //      $("input").keypress(function (event) {
//            if (e.which == 13) {
//                e.preventDefault();
//                $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/stockReport.jsp?sessionid=<%out.print(sessionid);%>');
//                $("#stockReportFrm").submit();
//            }
//        });




                                    $("#stockReportFrm").keypress(function (event) {
                                        if (event.which == 13) {
                                            event.preventDefault();
                                            $('#stockReportFrm').attr('action', '<%out.print(GlobalVariable.baseUrl);%>/memberManagement/voterListDivisionSubmit.jsp?sessionid=<%out.print(sessionid);%>');
                                                            $("#stockReportFrm").submit();
                                                        }
                                                    });



                                                });

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4 class="page-title">Division Voter List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Division Voter</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->



            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-horizontal" name="stockReportFrm" id="stockReportFrm" method="GET">

                        <input type="hidden" id="sessionid" name="sessionid" value="<%out.print(sessionid);%>"> 
                        <input type="hidden" id="memberSearchDivisionName" name="memberSearchDivisionName" value="<%=memberSearchDivisionName%>"> 
                        <div class="row">
                            <div class="col-md-3 offset-2">

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-4 font-weight-bold text-right">Fee Year</label>
                                    <div class="col-md-8">  
                                        <select name="voterFeeYearId" id="voterFeeYearId" class="form-control" required>
                                            <option value="">Select Fee Year</option>
                                            <%                                                            
                                                Object[] feeYearObj = null;
                                                Query feeYearSQL = null;

                                                String feeYearId = "";
                                                String feeYearName = "";
                                                String feeYearOptions = "";
                                                String feeYearOptionsSelect = "";
                                                int length = 0;
                                                feeYearSQL = dbsession.createSQLQuery("select * FROM member_fees_year ORDER BY member_fees_year_id ASC");
                                                System.out.println("feeYearSQL :: " + feeYearSQL);
                                                for (Iterator feeYearItr = feeYearSQL.list().iterator(); feeYearItr.hasNext();) {
                                                    feeYearObj = (Object[]) feeYearItr.next();
                                                    feeYearId = feeYearObj[0].toString();
                                                    // divisionEditName = objectEditDiv[1].toString();
                                                    feeYearName = feeYearObj[1].toString();

                                                    length = feeYearName.length(); // length == 8

                                                    if (!voterFeeYearId.equals("")) {

                                                        if (voterFeeYearId.equals(feeYearName)) {
                                                            feeYearOptionsSelect = " selected";
                                                        } else {
                                                            feeYearOptionsSelect = "";
                                                        }

                                                    }
                                                    if (length == 9) {

                                                        feeYearOptions = feeYearOptions + "<option value=\"" + feeYearName + "\" " + feeYearOptionsSelect + ">" + feeYearName + "</option>";

                                                    }

                                                }
                                            %>
                                            <%=feeYearOptions%>
                                        </select>
                                    </div>
                                </div>                                
                            </div>

                            <div class="col-md-5">

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 font-weight-bold text-right">Division</label>
                                    <div class="col-md-8">  
                                        <select name="memberSearchDivisionId" id="memberSearchDivisionId" class="form-control" onchange="showMemebrSearchDivisionNameInfo(this.value)" required>
                                            <option value="">Select Division</option>
                                            <%
                                                Object[] objectEditDiv = null;
                                                String divisionEditId = "";
                                                String divisionEditName = "";
                                                String divisionEditOptions = "";
                                                String divisionEditOptionsSelect = "";
                                                Query divisionEditSQL = dbsession.createSQLQuery("select * FROM member_division ORDER BY mem_division_name ASC");
                                                for (Iterator divisionEditItr = divisionEditSQL.list().iterator(); divisionEditItr.hasNext();) {
                                                    objectEditDiv = (Object[]) divisionEditItr.next();
                                                    divisionEditId = objectEditDiv[0].toString();
                                                    // divisionEditName = objectEditDiv[1].toString();
                                                    divisionEditName = objectEditDiv[3].toString();

                                                    if (!memberSearchDivisionId.equals("")) {

                                                        if (memberSearchDivisionId.equals(divisionEditId)) {
                                                            divisionEditOptionsSelect = " selected";
                                                        } else {
                                                            divisionEditOptionsSelect = "";
                                                        }

                                                    }

                                                    divisionEditOptions = divisionEditOptions + "<option value=\"" + divisionEditId + "\" " + divisionEditOptionsSelect + ">" + divisionEditName + "</option>";

                                                }
                                            %>
                                            <%=divisionEditOptions%>
                                        </select>
                                    </div>
                                </div>                                
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-4 offset-5">      
                                <input type="button"  id="searchStockItem" name="searchStockItem" class="btn btn-info" value="Submit">
                                <input type="button"  id="exportStockItem" name="exportStockItem" class="btn btn-info" value="Export">
                            </div>
                        </div>

                    </form>
                </div>
                <!-- /.white-box -->
            </div>






            <%
                //  }
                dbsession.clear();
                dbsession.close();
            %>


        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>