<%@page import="java.util.Date"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.*"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    dbsession = HibernateUtil.getSessionFactory().openSession();
    dbtrx = dbsession.beginTransaction();

    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();

    String showTabOption;
    if (regStep.equals("9")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureName = "";
    String pictureLink = "";

    String membershipApplyingFor = "";
    String membershipApplyingFor1 = "";
    String presentIEBmembershipNumber = "";

    q1 = dbsession.createSQLQuery("select * from member_temp WHERE id= " + regMemId + "");
    for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        memberName = object[4].toString();
        fatherName = object[5] == null ? "" : object[5].toString();
        motherName = object[6] == null ? "" : object[6].toString();
        placeOfBirth = object[7].toString();
        datefBirth = object[8].toString();
        age = object[9] == null ? "" : object[9].toString();
        nationality = object[10] == null ? "" : object[10].toString();
        gender = object[11].toString();
        membershipApplyingFor1 = object[13] == null ? "" : object[13].toString();
        if (membershipApplyingFor1.equals("1")) {
            membershipApplyingFor = " Fellow ";
        } else if (membershipApplyingFor1.equals("2")) {
            membershipApplyingFor = "Member";
        } else {
            membershipApplyingFor = "Associate Member";
        }

        presentIEBmembershipNumber = object[14] == null ? "" : object[14].toString();

        phone1 = object[15] == null ? "" : object[15].toString();
        phone2 = object[16] == null ? "" : object[16].toString();
        mobileNumber = object[17] == null ? "" : object[17].toString();
        userEmail = object[18].toString();
        pictureName = object[25].toString();

        pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

    }

%>


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css">

<script type="text/javascript">

    function recomendationCheckingForMembership() {
        console.log('recomendationCheckingForMembership :: ');
        var membershipApplyingFor, memberAge, associateMemberAgeLimit, memberAgeLimit, fellowMemberAgeLimit,
                proposerMemberId, seconderMemberId, thirdMemberId,
                proposerFirstCharAt, seconderFirstCharAt, thirdFirstCharAt,
                fellowCount, memberCount,
                totalCount;

        fellowMemberAgeLimit = '37';
        memberAgeLimit = '27';
        associateMemberAgeLimit = '15';

        membershipApplyingFor = document.getElementById('membershipApplyingFor').value;

        proposerMemberId = document.getElementById('proposerMemberId').value;
        seconderMemberId = document.getElementById('seconderMemberId').value;
        thirdMemberId = document.getElementById('thirdMemberId').value;

        proposerFirstCharAt = proposerMemberId.charAt(0);
        seconderFirstCharAt = seconderMemberId.charAt(0);
        thirdFirstCharAt = thirdMemberId.charAt(0);

        totalCount = proposerFirstCharAt + seconderFirstCharAt + thirdFirstCharAt;



        if (membershipApplyingFor != '') {
            //fellow
            if (membershipApplyingFor == 1) {
                //two fellow
                //1 member

                if (totalCount == "FFF" || totalCount == "FFM" || totalCount == "MFF" || totalCount == "FMF") {
                    console.log("totalCount true ::" + totalCount);
                    return true;
                } else {
                    console.log("totalCount false ::" + totalCount);
                    var msg = '<div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">'
                            + '<a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>'
                            + 'Two Fellows and One Member for Fellowship'
                            + '</div>';
                    $("#globalAlertInfoBoxConTT").html(msg).show();
                    return false;
                }


            }
            // Member 
            if (membershipApplyingFor == 2) {
                //one Fellow
                //Two member

                if (totalCount == "FFF" || totalCount == "FFM" || totalCount == "FMM" || totalCount == "MMF" || totalCount == "MFM") {
                    console.log("totalCount true ::" + totalCount);
                    return true;
                } else {
                    console.log("totalCount false ::" + totalCount);
                    var msg = '<div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">'
                            + '<a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>'
                            + 'One Fellow and Two Members for Membership '
                            + '</div>';
                    $("#globalAlertInfoBoxConTT").html(msg).show();
                    return false;
                }


            }
            //  Associate Member  
            if (membershipApplyingFor == 3) {
                //two member

                if (totalCount == "FFF" || totalCount == "FFM" || totalCount == "FMM" || totalCount == "MMF" || totalCount == "MFM" || totalCount == "MMM") {
                    console.log("totalCount true ::" + totalCount);
                    return true;
                } else {
                    console.log("totalCount false ::" + totalCount);
                    var msg = '<div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">'
                            + '<a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>'
                            + 'Two Members for Associate Membership '
                            + '</div>';
                    $("#globalAlertInfoBoxConTT").html(msg).show();
                    return false;
                }

            }
        }
//        else {
//            //disable submit button
//            document.getElementById("btnRegStep1").disabled = true;
//        }
    }

</script>





<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Member Info</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Member Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                     
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">



                    <div class="row">
                        <div class="col-md-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 1: Personal Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="#">
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputName">Name:</label>
                                                        <%=memberName%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputName">Father Name:</label>
                                                        <%=fatherName%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputName">Mother Name:</label>
                                                        <%=motherName%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputName">Gender:</label>
                                                        <%=gender%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputDOB">Date of Birth:</label>
                                                        <%=datefBirth%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Age:</label>
                                                        <%=age%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Place of Birth:</label>
                                                        <%=placeOfBirth%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Nationality:</label>
                                                        <%=nationality%>
                                                    </div>
                                                </div>

                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Office Phone:</label>
                                                        <%=phone1%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Residence Phone:</label>
                                                        <%=phone2%>
                                                    </div>
                                                </div>
                                                <div class="form-row form-group m-0">
                                                    <div class="col-6">
                                                        <label for="inputPOB">Mobile:</label>
                                                        <%=mobileNumber%>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="inputAge">Email:</label>
                                                        <%=userEmail%>
                                                    </div>
                                                </div>







                                                <div class="form-row form-group m-0">
                                                    <div class="col-md-12">
                                                        <label for="inputName">Membership Applying For:</label>
                                                        <%=membershipApplyingFor%>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputName">Present IEB Membership Number:</label>
                                                        <%=presentIEBmembershipNumber%>
                                                    </div>
                                                </div>




                                            </form>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="card">
                                    <div class="card-header" id="headingFourMyPicture">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseMyPicture" aria-expanded="false" aria-controls="collapseMyPicture">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 2: My Picture Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseMyPicture" class="collapse" aria-labelledby="headingMyPicture" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationMyPictureUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-md-10 offset-1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-2 text-right" for="inputName" style="margin-top: 0.25rem;">My Picture</label>


                                                            <div class="col-md-4">
                                                                <%=pictureLink%>
                                                            </div>
                                                            <div class="col-md-4">
                                                                &nbsp;
                                                            </div>
                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-md-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .JPGE and .PNG files allow and file not more then 5MB.</p>
                                                    </div>
                                                </div>



                                            </form>

                                        </div>
                                    </div>
                                </div>                   

                                <div class="card">
                                    <div class="card-header" id="headingOneAddressInfo">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneAddressInfo" aria-expanded="true" aria-controls="collapseOneAddressInfo">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 3: Address Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOneAddressInfo" class="collapse" aria-labelledby="headingOneAddressInfo" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationAddressInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">




                                                    <%
                                                        Query madrSQL = null;
                                                        Object[] madrObject = null;

                                                        Query padrSQL = null;
                                                        Object[] padrObject = null;

                                                        int mAddressId = 0;
                                                        String mCountryId = "";
                                                        String mDistrictId = "";
                                                        String mThanaId = "";
                                                        String mThanaName = "";
                                                        String mThanaDistrictName = "";
                                                        String mAddressLine1 = "";
                                                        String mAddressLine2 = "";
                                                        String mZipOffice = "";
                                                        String mZipCode = "";

                                                        int pAddressId = 0;
                                                        String pCountryId = "";
                                                        String pDistrictId = "";
                                                        String pThanaId = "";
                                                        String pThanaName = "";
                                                        String pThanaDistrictName = "";
                                                        String pAddressLine1 = "";
                                                        String pAddressLine2 = "";
                                                        String pZipOffice = "";
                                                        String pZipCode = "";

                                                        String mAddressStr = "";
                                                        String pAddressStr = "";

                                                        Thana thana = null;

                                                        Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='M' ) ");
                                                        for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
                                                            madrObject = (Object[]) itr2.next();
                                                            mAddressLine1 = madrObject[1].toString();
                                                            mAddressLine2 = madrObject[2].toString();
                                                            mThanaId = madrObject[3].toString();

                                                            Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                                            for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
                                                                thana = (Thana) itrmThana.next();

                                                                mThanaName = thana.getThanaName();
                                                                mThanaDistrictName = thana.getDistrict().getDistrictName();
                                                            }

                                                            mZipCode = madrObject[4].toString();
                                                            mZipOffice = madrObject[8].toString();

                                                            mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

                                                        }
                                                        Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='P' ) ");
                                                        for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

                                                            padrObject = (Object[]) itr3.next();
                                                            pAddressLine1 = padrObject[1].toString();
                                                            pAddressLine2 = padrObject[2].toString();
                                                            pThanaId = padrObject[3].toString();

                                                            Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                                            for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
                                                                thana = (Thana) itrpThana.next();

                                                                pThanaName = thana.getThanaName();
                                                                pThanaDistrictName = thana.getDistrict().getDistrictName();
                                                            }

                                                            pZipCode = padrObject[4].toString();
                                                            pZipOffice = padrObject[8].toString();

                                                            pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

                                                        }

                                                    %>


                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Mailing Address</p>
                                                        <address class="m-2 text-center">
                                                            <%=mAddressStr%>
                                                        </address>

                                                    </div>
                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Permanent Address</p>

                                                        <address class="m-2 text-center">
                                                            <%=pAddressStr%>
                                                        </address>

                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 4: Education Information</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">



                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationEducationSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <%
                                                        Query educationSQL = null;
                                                        int degreeId = 0;
                                                        String degreeName = "";
                                                        String instituteName = "";
                                                        String boardUniversityName = "";
                                                        //java.util.Date dob = new Date();
                                                        String yearOfPassing = "";
                                                        String resultTypeName = "";
                                                        String result = "";

                                                        MemberEducationInfoTemp mei = null;

                                                        educationSQL = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + regMemId + " ");

                                                        MemberTemp membertemp = null;
                                                        String memberSubdivisionName = "";
                                                        String memberSubdivisionFullName = "";
                                                        int educationfInfoId = 0;
                                                    %>


                                                    <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Degree</th>
                                                                <th scope="col">Board/University</th>
                                                                <th scope="col">Year of Passing</th>
                                                                <th scope="col">Score/Class</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%
                                                                for (Iterator itr = educationSQL.list().iterator(); itr.hasNext();) {
                                                                    mei = (MemberEducationInfoTemp) itr.next();

                                                                    educationfInfoId = mei.getId();
                                                                    degreeId = mei.getDegree().getDegreeId();
                                                                    degreeName = mei.getDegree().getDegreeName();

                                                                    instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                                    boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                                    yearOfPassing = mei.getYearOfPassing();
                                                                    resultTypeName = mei.getResultType().getResultTypeName();
                                                                    result = mei.getResult();

                                                                    degreeName = degreeName + memberSubdivisionFullName;
                                                            %>
                                                            <tr>
                                                                <td><%=degreeName%></td>
                                                                <td><%=boardUniversityName%></td>
                                                                <td><%=yearOfPassing%></td>
                                                                <td><%=result%></td>


                                                            </tr>
                                                            <%
                                                                }

                                                            %>

                                                        </tbody>
                                                    </table>

                                                </div>

                                                <div class="row">
                                                    <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                        <thead>
                                                            <tr>                                                
                                                                <th scope="col">Subject</th>
                                                                <th scope="col">University</th>
                                                            </tr>
                                                            <%                                                Object[] subjectObject = null;
                                                                String subjectName = "";
                                                                String subjectUniversityName = "";

                                                                Query subjectSQL = dbsession.createSQLQuery("SELECT usb.subject_long_name,u.university_long_name FROM "
                                                                        + "member_subject_info_temp msb,university_subject usb,university u "
                                                                        + "WHERE msb.subject_id = usb.subject_id "
                                                                        + "AND usb.university_id = u.university_id "
                                                                        + "AND msb.member_id='" + regMemId + "'");
                                                                if (!subjectSQL.list().isEmpty()) {
                                                                    for (Iterator subjectItr = subjectSQL.list().iterator(); subjectItr.hasNext();) {
                                                                        subjectObject = (Object[]) subjectItr.next();
                                                                        subjectName = subjectObject[0].toString();
                                                                        subjectUniversityName = subjectObject[1].toString();
                                                            %>

                                                            <tr>
                                                                <td><%=subjectName%></td>
                                                                <td><%=subjectUniversityName%></td>
                                                            </tr>

                                                            <%
                                                                    }
                                                                }
                                                            %>

                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>        

                                            </form>


                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 5: Professional Record</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationProfessionalInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >


                                                <%
                                                    Query profTempSQL = null;
                                                    //    Query q2 = null;
                                                    int profTempId = 0;
                                                    String organizationName = "";
                                                    String designationName = "";
                                                    String startDateProTemp = "";
                                                    String endDateProTemp = "";

                                                    MemberProfessionalInfoTemp profTemp = null;

                                                    profTempSQL = dbsession.createQuery("from MemberProfessionalInfoTemp where  member_id=" + regMemId + " ");


                                                %>
                                                <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Organization</th>
                                                            <th scope="col">Designation</th>
                                                            <th scope="col">Start Date</th>
                                                            <th scope="col">End Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <%                                                for (Iterator profTempItr = profTempSQL.list().iterator(); profTempItr.hasNext();) {
                                                                profTemp = (MemberProfessionalInfoTemp) profTempItr.next();

                                                                organizationName = profTemp.getCompanyName().toString();
                                                                designationName = profTemp.getDesignation().toString();
                                                                startDateProTemp = profTemp.getFromDate().toString();
                                                                endDateProTemp = profTemp.getTillDate().toString();


                                                        %>
                                                        <tr>
                                                            <td><%=organizationName%></td>
                                                            <td><%=designationName%></td>
                                                            <td><%=startDateProTemp%></td>
                                                            <td><%=endDateProTemp%></td>

                                                        </tr>
                                                        <%
                                                            }

                                                        %>

                                                    </tbody>
                                                </table>


                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <div class="card" id="section8">
                                    <div class="card-header" id="headingFourDocuments">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourDocuments" aria-expanded="false" aria-controls="collapseFourDocuments">
                                                <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 6: Related Documents Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourDocuments" class="collapse" aria-labelledby="headingFourDocuments" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationRelatedDocumentsUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="1<%//=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <%
                                                        Query relDocSQL = null;
                                                        Object[] relDocbject = null;

                                                        String documentId = "";
                                                        String documentName = "";

                                                        relDocSQL = dbsession.createSQLQuery("SELECT * FROM member_document_info_temp WHERE member_id = '" + regMemId + "'");
                                                        for (Iterator relDocItr = relDocSQL.list().iterator();
                                                                relDocItr.hasNext();) {
                                                            relDocbject = (Object[]) relDocItr.next();

                                                            documentId = relDocbject[2].toString();
                                                            documentName = relDocbject[3].toString();
                                                        }

                                                    %>
                                                    <div class="col-md-10 offset-md-1 m-t-20">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName" style="">Required Document : </label>

                                                            <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                                            <div class="col-md-8">
                                                                <%=documentName%>
                                                            </div>

                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-md-2 offset-md-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 

                                            </form>

                                        </div>
                                    </div>
                                </div>                


                                <div class="card" id="section9">
                                    <div class="card-header" id="headingFourRecom">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRecom" aria-expanded="false" aria-controls="collapseFourRecom">
                                                <span class="h4">Section 7: Recommendation Info</span>
                                            </button>
                                        </h5>
                                    </div>


                                    <div id="collapseFourRecom" class="collapse <%=showTabOption%>" aria-labelledby="headingFourRecom" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberAddRecomendationInfoSubmitData.jsp" onsubmit="return recomendationCheckingForMembership()">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                                <input type="hidden" name="membershipApplyingFor" id="membershipApplyingFor" value="<%=membershipApplyingFor%>" >

                                                <div class="row m-t-20">
                                                    <div class="col-md-4">
                                                        <p class="m-2 text-center text-dark">Proposer</p>

                                                        <div class="form-row form-group">
                                                            <div class="col-md-8 offset-2">
                                                                <input oninput="checkRegistrationProposerInfo('1')" name="proposerMemberId" id="proposerMemberId" type="text" class="form-control" placeholder="Member ID (Ex. F01234)" required>
                                                                <input name="proposerId" id="proposerId" type="hidden" value="">
                                                                <div id="proposerErr"></div>
                                                            </div>                                                    
                                                        </div>                                                         

                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="m-2 text-center text-dark">Seconder |</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-md-8 offset-2">
                                                                <input oninput="checkRegistrationProposerInfo('2')" name="seconderMemberId" id="seconderMemberId" type="text" class="form-control" placeholder="Member ID (Ex. F01234)" required>
                                                                <input name="seconderId" id="seconderId" type="hidden" value="">
                                                                <div id="seconderErr"></div>
                                                            </div>                                                    
                                                        </div>    
                                                        
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="m-2 text-center text-dark">Seconder ||</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-md-8 offset-2">
                                                                <input  oninput="checkRegistrationProposerInfo('3')" name="thirdMemberId" id="thirdMemberId" type="text" class="form-control" placeholder="Member ID (Ex. M12345)">
                                                                <input name="thirdId" id="thirdId" type="hidden" value="">
                                                                <div id="thirdErr"></div>

                                                            </div>                                                    
                                                        </div>                                                         

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-md-2 offset-5">
                                                                <button type="submit" class="btn btn-block btn-primary" id="recomendationSubmitBtn" >Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </form>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-dark" style="font-size: 0.8rem;">Note |: Proposer and Seconder must be at least 
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.Two Fellows and One Member for Fellowship
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii.One Fellow and Two Members for Membership
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii.Two Members for Associate Membership
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="card" id="section9">
                                    <div class="card-header" id="headingFourRegFee">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRegFee" aria-expanded="false" aria-controls="collapseFourRegFee">
                                                <span class="h4">Section 8: Registration Fee Payment Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRegFee" class="collapse" aria-labelledby="collapseFourRegFee" data-parent="#accordion">
                                        <div class="card-body">


                                            <form id="loginform" class="form-horizontal form-material123" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationFeePaymentOptionSubmitData.jsp">



                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                                <thead>

                                                                <th scope="col" class="bg-primary text-white"><strong>Category</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Entrance Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Annual Subscription Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Diploma Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>ID Card Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Total Amount (Tk.)</strong></th>

                                                                </thead>
                                                                <tbody>
                                                                    <%

                                                                        Object[] memTypeObject = null;
                                                                        Query memTypeSQL = null;

                                                                        String membershipApplyingType = "";
                                                                        String membershipEntranceFee = "";
                                                                        String membershipAnnualSubscriptionFee = "";
                                                                        String membershipDiplomaFee = "";
                                                                        String membershipIDCardFee = "";
                                                                        double membershipTotalPayableFee = 0.00;

                                                                        double membershipEntranceFee1 = 0.00;
                                                                        double membershipAnnualSubscriptionFee1 = 0.00;
                                                                        double membershipDiplomaFee1 = 0.00;
                                                                        double membershipIDCardFee1 = 0.00;

                                                                        //SELECT * FROM `member_type_info` 
                                                                        memTypeSQL = dbsession.createSQLQuery("SELECT * FROM member_type_info WHERE member_type_id = '" + membershipApplyingFor1 + "'");

                                                                        for (Iterator memTypeItr = memTypeSQL.list().iterator(); memTypeItr.hasNext();) {
                                                                            memTypeObject = (Object[]) memTypeItr.next();

                                                                            membershipApplyingType = memTypeObject[1].toString();
                                                                            membershipEntranceFee = memTypeObject[4].toString();
                                                                            membershipAnnualSubscriptionFee = memTypeObject[6].toString();
                                                                            membershipDiplomaFee = memTypeObject[7].toString();
                                                                            membershipIDCardFee = memTypeObject[3].toString();

                                                                            membershipEntranceFee1 = Double.parseDouble(membershipEntranceFee);
                                                                            System.out.println("Double Entry Fee:: " + membershipEntranceFee1);
                                                                            membershipAnnualSubscriptionFee1 = Double.parseDouble(membershipAnnualSubscriptionFee);
                                                                            membershipDiplomaFee1 = Double.parseDouble(membershipDiplomaFee);
                                                                            membershipIDCardFee1 = Double.parseDouble(membershipIDCardFee);

                                                                            //  membershipTotalFee = Integer.sum(membershipEntranceFee1,membershipAnnualSubscriptionFee1,membershipDiplomaFee1,membershipIDCardFee1);
                                                                            membershipTotalPayableFee = membershipEntranceFee1 + membershipAnnualSubscriptionFee1 + membershipDiplomaFee1 + membershipIDCardFee1;

                                                                    %>
                                                                    <tr>
                                                                        <td><strong><%=membershipApplyingType%></strong></td>
                                                                        <td><%=membershipEntranceFee%></td>
                                                                        <td><%=membershipAnnualSubscriptionFee%></td>
                                                                        <td><%=membershipDiplomaFee%></td>
                                                                        <td><%=membershipIDCardFee%></td>
                                                                        <td><strong><%=membershipTotalPayableFee%></strong></td>
                                                                    </tr>
                                                                    <% }%>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div> 
                                                </div> 


                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-md-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     


                                            </form>





                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>







                </div>
                <!-- /.white-box -->
            </div>

        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>              