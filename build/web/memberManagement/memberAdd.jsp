
<%@page import="java.util.Date"%>
<%@page import="com.appul.entity.AddressBook"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.MemberTemp"%>
<%@page import="com.appul.entity.MemberEducationInfo"%>
<%@page import="com.appul.entity.MemberProfessionalInfo"%>
<%@page import="com.appul.entity.Thana"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="com.appul.entity.SyActivity"%>
<%@page import="com.appul.entity.SyUserRole"%>
<%@page import="com.appul.entity.SyRoles"%>
<%@page import="com.appul.entity.SyDept"%>
<%@page import="com.appul.entity.SyUser"%>

<% request.setAttribute("TITLE", "Add new stock item");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about1.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>



<script type="text/javascript">
    function personalInfoValidation() {

        var genderChecked = $(".genderClass").prop('checked');
        if (!genderChecked) {
            // alert("Please select atlease one jobwork");
            $("#memberGenderErr").html("Please select gender").css({"color": "red", "font-size": "10px;"});
            return false;

        }
        ;
    }

    function getMemberAge() {

        //  console.log('dateString :: ' + dateString);
        var memberDoB = document.getElementById('memberDoB').value;
        console.log('memberDoB :: ' + memberDoB);
        if (memberDoB != '') {

            var today = new Date();
            var birthDate = new Date(memberDoB);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            // return age;

            document.getElementById('memberAgeXn').value = age;
            document.getElementById('memberAge').value = age;
        }

    }
    // console.log('age: ' + getAge("1984-10-31"));


    function ageCheckingForMembership(arg) {
        console.log('ageCheckingForMembership :: ' + arg);
        var membershipApplyingFor, memberAge, associateMemberAgeLimit, memberAgeLimit, fellowMemberAgeLimit;

        fellowMemberAgeLimit = '37';
        memberAgeLimit = '27';
        associateMemberAgeLimit = '15';

        membershipApplyingFor = document.getElementById('membershipApplyingFor').value;
        memberAge = document.getElementById('memberAge').value;
        if (membershipApplyingFor != '') {
            //fellow
            if (membershipApplyingFor == 1) {
                if (Number(memberAge) >= Number(fellowMemberAgeLimit)) {
                    document.getElementById("membershipApplyingForErr").innerHTML = "";
                    document.getElementById("btnRegStep1").disabled = false;
                } else {
                    document.getElementById("membershipApplyingFor").focus();
                    //  document.getElementById("membershipApplyingForErr").innerHTML = "Minimum age for Fellow is " + fellowMemberAgeLimit + " year"; 
                    $("#membershipApplyingForErr").html("Minimum age for Fellow is " + fellowMemberAgeLimit + " year.").css({"color": "red", "font-size": "10px;"});
                    document.getElementById("btnRegStep1").disabled = true;
                }
            }
            // Member 
            if (membershipApplyingFor == 2) {
                if (Number(memberAge) >= Number(memberAgeLimit)) {
                    document.getElementById("membershipApplyingForErr").innerHTML = "";
                    document.getElementById("btnRegStep1").disabled = false;
                } else {
                    document.getElementById("membershipApplyingFor").focus();
                    //  document.getElementById("membershipApplyingForErr").innerHTML = "Minimum age for Member is " + memberAgeLimit + " year.";
                    $("#membershipApplyingForErr").html("Minimum age for Member is " + memberAgeLimit + " year.").css({"color": "red", "font-size": "10px;"});
                    document.getElementById("btnRegStep1").disabled = true;
                }
            }
            //  Associate Member  
            if (membershipApplyingFor == 3) {
                if (Number(memberAge) >= Number(associateMemberAgeLimit)) {
                    document.getElementById("membershipApplyingForErr").innerHTML = "";
                    document.getElementById("btnRegStep1").disabled = false;
                } else {
                    document.getElementById("membershipApplyingFor").focus();
                    //  document.getElementById("membershipApplyingForErr").innerHTML = "Minimum age for Associate Member is " + associateMemberAgeLimit + " year.";
                    $("#membershipApplyingForErr").html("Minimum age for Associate Member is " + associateMemberAgeLimit + " year.").css({"color": "red", "font-size": "10px;"});
                    document.getElementById("btnRegStep1").disabled = true;
                }
            }
        } else {
            //disable submit button
            document.getElementById("btnRegStep1").disabled = true;
        }
    }

</script>

<%
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//        sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();
    dbtrx = dbsession.beginTransaction();

    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();

    String showTabOption = "";
    if (regStep.equals("3")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    System.out.println("Header sessionIdH :: " + sessionIdH);
    System.out.println("Header userNameH :: " + userNameH);


%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Member Info</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>
                    <li class="active">Member Add</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">


                    <div class="row">
                        <div class="col-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <span class="h4">Section 1: Personal Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="form-group col-12">
                                                    <label for="inputName">* Required field</label>

                                                </div>
                                            </div>
                                            <form id="loginform" class="form-horizontal" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/memberManagement/memberAddPersonalInfoSubmitData.jsp" onsubmit="return personalInfoValidation()">

                                                <div class="row">
                                                    <div class="col-12">
                                                        <!-- <div class="form-group d-flex align-items-center">
                                                            <label class="form-control-label col-sm-3 ml-3 font-weight-bold">FatherName</label>
                                                            <div class="col-sm-9">
                                                                <input name="fatherName" id="fatherName" value="" placeholder="Father's name" type="text" class="form-control" required="">                                    
                                                            </div>
                                                        </div>-->

                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label for="inputName">Name *</label>
                                                                <input name="memberName" id="memberName" type="text" class="form-control" placeholder="Full Name" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputNationality">Father Name *</label>
                                                                <input name="fatherName" id="fatherName" type="text" class="form-control" placeholder="Father Name" required>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPOB">Mother Name *</label>
                                                                <input name="motherName" id="motherName" type="text" class="form-control" placeholder="Mother Name" required>
                                                            </div>
                                                        </div>


                                                        <div class="form-row">
                                                            <div class="form-group col-md-2">
                                                                <!-- Date picker plugins css -->
                                                                <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                                                                <label for="inputDOB">Date of Birth *</label>
                                                                <input  name="memberDoB" id="memberDoB" type="text" class="form-control" placeholder="YYYY-MM-DD" onchange="getMemberAge()" required>
                                                                <!-- Date Picker Plugin JavaScript -->
                                                                <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                                                                <script type="text/javascript">
                                                                    jQuery('#memberDoB').datepicker({
                                                                        autoclose: true,
                                                                        todayHighlight: true,
                                                                        format: 'yyyy-mm-dd'
                                                                    });
                                                                </script>  
                                                            </div>
                                                            <div class="form-group col-md-1">
                                                                <label for="inputAge">Age *</label>
                                                                <input  name="memberAgeXn" id="memberAgeXn" type="text" class="form-control" disabled="">
                                                                <input  name="memberAge" id="memberAge" type="hidden" value="">
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputPOB">Place of Birth *</label>
                                                                <input  name="memberPlaceOfBirth" id="memberPlaceOfBirth" type="text" class="form-control" placeholder="Place of Birth" required>


                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputAge">Nationality *</label>
                                                                <input name="memberNationality" id="memberNationality" type="text" class="form-control" placeholder="Nationality" required>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputAge">Gender *</label>
                                                                <div class="col-sm-12 form-control">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input genderClass" type="radio" name="memberGender" id="memberGenderMale" value="M" >
                                                                        <label class="form-check-label radio_label1" for="memberGender">Male</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input genderClass" type="radio" name="memberGender" id="memberGenderFemale" value="F" >
                                                                        <label class="form-check-label radio_label1" for="memberGender">Female</label>
                                                                    </div>
                                                                    <span id="memberGenderErr" class=""></span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <h4 class="text-center m-3 font-weight-bold" > Contact </h4>
                                                        <div class="form-row border border-light my-4">

                                                            <div class="form-group col-md-6">
                                                                <label for="inputOffice">Office *</label>
                                                                <input id="memberOfficePhone" name="memberOfficePhone"  type="text" class="form-control" placeholder="Office phone"  required>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputResidence">Residence *</label>
                                                                <input id="memberResidencePhone" name="memberResidencePhone" type="text" class="form-control" placeholder="Residence number" required>
                                                            </div>
                                                            <div class="w-100"></div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputMobile">Mobile *</label>
                                                                <input id="memberMobile" name="memberMobile" type="text" class="form-control" placeholder="Mobile number"  required>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPOB">Email *</label>
                                                                <input id="memberEmail" name="memberEmail" type="email" class="form-control" placeholder="Email address" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="form-group col-md-4">
                                                                <label for="inputName">Membership Applying For *</label>

                                                                <select  id="membershipApplyingFor" name="membershipApplyingFor" onchange="ageCheckingForMembership(this.value)" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select Membership Applying For</option>
                                                                    <%
                                                                        Query memberTypeSQL = null;
                                                                        Object[] memberTypeObject = null;
                                                                        String memberTypeId = "";
                                                                        String memberTypeName = "";
                                                                        String memberTypeSelected = "";
                                                                        memberTypeSQL = dbsession.createSQLQuery("SELECT * FROM member_type_info ORDER BY member_type_id ASC");
                                                                        for (Iterator memberTypeItr = memberTypeSQL.list().iterator(); memberTypeItr.hasNext();) {
                                                                            memberTypeObject = (Object[]) memberTypeItr.next();
                                                                            memberTypeId = memberTypeObject[0].toString();
                                                                            memberTypeName = memberTypeObject[1].toString();


                                                                    %> 
                                                                    <option value=" <%=memberTypeId%> " > <%=memberTypeName%> </option> 
                                                                    <%
                                                                        }

                                                                    %>

                                                                </select>
                                                                <span id="membershipApplyingForErr"></span>

                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputName">Present IEB Membership Number <span class="text-muted">(if any)</span></label>
                                                                <input  id="presentIEBmembershipNumber" name="presentIEBmembershipNumber" type="text" class="form-control" placeholder="Member ID (Ex. F01234, M12345, A12345)">
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputName">Center *</label>

                                                                <select  id="membershipCenter" name="membershipCenter" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select Center</option>
                                                                    <%                                                    Query centerSQL = null;
                                                                        Object[] centerObject = null;
                                                                        String centerId = "";
                                                                        String centerName = "";
                                                                        String centerSelected = "";
                                                                        centerSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_type_id ='3' ORDER BY center_id ASC");
                                                                        for (Iterator centerItr = centerSQL.list().iterator(); centerItr.hasNext();) {
                                                                            centerObject = (Object[]) centerItr.next();
                                                                            centerId = centerObject[0].toString();
                                                                            centerName = centerObject[1].toString();


                                                                    %> 
                                                                    <option value=" <%=centerId%> " > <%=centerName%> </option> 
                                                                    <%
                                                                        }

                                                                    %>

                                                                </select>

                                                            </div>
                                                        </div>


                                                        <div class="form-row">
                                                            <div class="col-md-3 offset-md-4" style="margin-bottom: 20px;">
                                                                <button type="submit" name="btnRegStep1" id="btnRegStep1" class="btn btn-primary btn-lg btn-block">Submit</button>
                                                            </div>
                                                        </div>

                                                        

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                 

                                <div class="card">
                                    <div class="card-header" id="headingFourMyPicture">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseMyPicture" aria-expanded="false" aria-controls="collapseMyPicture">
                                                <span class="h4">Section 2: My Picture Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseMyPicture" class="collapse <%=showTabOption%>" aria-labelledby="headingMyPicture" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal " method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationMyPictureUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-10 offset-1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">My Picture</label>


                                                            <div class="col-md-4">
                                                                <input name="fileMyPic" id="fileMyPic" onchange="myPictureUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required disabled>  
                                                                <div id="fileMyPicErr"></div>
                                                            </div>
                                                            <div class="col-md-4">

                                                            </div>
                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .JPGE and .PNG files allow and file not more then 5MB.</p>
                                                    </div>
                                                </div>



                                            </form>

                                        </div>
                                    </div>
                                </div>                   


                                <div class="card">
                                    <div class="card-header" id="headingOneAddressInfo">
                                        <h3 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneAddressInfo" aria-expanded="true" aria-controls="collapseOneAddressInfo">
                                                <span class="h4">Section 3: Address Information</span>
                                            </button>
                                        </h3>
                                    </div>

                                    <div id="collapseOneAddressInfo" class="collapse" aria-labelledby="headingOneAddressInfo" data-parent="#accordion">
                                        <div class="card-body">
                                            <form id="loginform" class="form-horizontal " method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationAddressInfoSubmitData.jsp">

                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Mailing Address</p>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Country:</label>
                                                            <div class="col-7">

                                                                <select  id="memberMailingAddressCountry" name="memberMailingAddressCountry" class="form-control input-sm customInput-sm" required disabled="">
                                                                    <option value="">Select Country</option>


                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">District:</label>
                                                            <div class="col-7">
                                                                <select  id="memberMailingAddressDistrict" name="memberMailingAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'MA')" class="form-control input-sm customInput-sm" required disabled="">
                                                                    <option value="">Select District</option>


                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Thana:</label>
                                                            <div class="col-7" id="showThanaInfoMA">                                                    
                                                                <select  id="memberMailingAddressThana" name="memberMailingAddressThana" class="form-control input-sm customInput-sm" required disabled="">
                                                                    <option value="">Select Thana</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Address *</label>
                                                            <div class="col-7">
                                                                <textarea rows="2" cols="50" id="memberMailingAddressLine1" name="memberMailingAddressLine1" class="form-control" placeholder="Address" required> </textarea>                                                    
                                                            </div>
                                                        </div>

                                                        <input name="memberMailingAddressLine2" id="memberMailingAddressLine2" type="hidden" value="">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Post Office:</label>
                                                            <div class="col-md-4">
                                                                <input name="memberMailingAddressPostOffice" id="memberMailingAddressPostOffice" type="text" class="form-control" placeholder="Post Office" required disabled="">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input name="memberMailingAddressPostCode" id="memberMailingAddressPostCode" type="text" class="form-control" placeholder="Post Code" required disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <p class="m-2 text-center">Permanent Address</p>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Country:</label>
                                                            <div class="col-7">
                                                                <select  id="memberPermanentAddressCountry" name="memberPermanentAddressCountry" class="form-control input-sm customInput-sm" required disabled="">
                                                                    <option value="">Select Country</option>                                                      

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">District:</label>
                                                            <div class="col-7">
                                                                <select  id="memberPermanentAddressDistrict" name="memberPermanentAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'PA')" class="form-control input-sm customInput-sm" required>
                                                                    <option value="">Select District</option>

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Thana:</label>
                                                            <div class="col-7" id="showThanaInfoPA">                                                    
                                                                <select  id="memberPermanentAddressThana" name="memberPermanentAddressThana" class="form-control input-sm customInput-sm" required disabled="">
                                                                    <option value="">Select Thana</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Address *</label>
                                                            <div class="col-7">
                                                                <textarea rows="2" cols="50" id="memberPermanentAddressLine1" name="memberPermanentAddressLine1" class="form-control" placeholder="Address" required> </textarea>                                                    

                                                            </div>
                                                        </div>
                                                        <input name="memberPermanentAddressLine2" id="memberPermanentAddressLine2" type="hidden" value="">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Post Office:</label>
                                                            <div class="col-md-4">
                                                                <input name="memberPermanentAddressPostOffice" id="memberPermanentAddressPostOffice" type="text" class="form-control" placeholder="Post Office" required disabled="">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input name="memberPermanentAddressPostCode" id="memberPermanentAddressPostCode" type="text" class="form-control" placeholder="Post Code" required disabled="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-6 offset-3">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </form>
                                        </div>
                                    </div>
                                </div>                
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <span class="h4">Section 4: Education Information</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">



                                            <form id="loginform" class="form-horizontal " method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationEducationSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="m-2 text-center text-dark">SSC/Equivalent</p>
                                                        <input name="memberSSCDegreeTypeId" id="memberSSCDegreeTypeId" type="hidden" value="1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Institution:</label>
                                                            <div class="col-md-8">
                                                                <input name="memberSSCInstitution" id="memberSSCInstitution" type="text" class="form-control" placeholder="Institution" required disabled>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Education Board:</label>
                                                            <div class="col-md-8">
                                                                <select name="memberSSCBoardUniversity" id="memberSSCBoardUniversity" class="form-control chosen" disabled>
                                                                    <option value="">Select Education Board</option>                                                       


                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberSSCPassingYear" name="memberSSCPassingYear"   class="custom-select" disabled>
                                                                    <option value="">Select Year</option>

                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberSSCResultTypeId" id="memberSSCResultTypeId" class="form-control" disabled>>
                                                                    <option value="">Result Type </option>


                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberSSCScoreClass" id="memberSSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required disabled>
                                                            </div> 
                                                        </div> 
                                                    </div>  

                                                    <div class="col-6">
                                                        <p class="m-2 text-center text-dark">HSC/Equivalent</p>

                                                        <input name="memberHSCDegreeTypeId" id="memberHSCDegreeTypeId" type="hidden" value="2">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Institution:</label>
                                                            <div class="col-md-8">
                                                                <input name="memberHSCInstitution" id="memberHSCInstitution" type="text" class="form-control" placeholder="Institution" required disabled>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Education Board:</label>
                                                            <div class="col-md-8">
                                                                <select name="memberHSCBoardUniversity" id="memberHSCBoardUniversity" class="form-control chosen" disabled>
                                                                    <option value="">Select Education Board</option>


                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberHSCPassingYear" name="memberHSCPassingYear"   class="custom-select" disabled>
                                                                    <option value="">Select Year</option>

                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberHSCResultTypeId" id="memberHSCResultTypeId" class="form-control" disabled>
                                                                    <option value="">Result Type </option>


                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberHSCScoreClass" id="memberHSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required disabled>
                                                            </div> 
                                                        </div>                                                
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="m-2 text-center text-dark">B.Sc. Engg./Equivalent </p>
                                                        <input name="memberBSCDegreeTypeId" id="memberBSCDegreeTypeId" type="hidden" value="3">

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">University:</label>
                                                            <div class="col-md-8">
                                                                <select name="memberBSCBoardUniversity" id="memberBSCBoardUniversity" class="form-control chosen" disabled>
                                                                    <option value="">Select University</option>                                                       

                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Subject:</label>
                                                            <div class="col-md-8" id="subjectCon">
                                                                <select name="memberBSCUniversitySubject" id="memberBSCUniversitySubject" class="form-control chosen" required disabled="">
                                                                    <option value="">Select Subject</option>                                                       

                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberBSCPassingYear" name="memberBSCPassingYear"   class="custom-select" required disabled>
                                                                    <option value="">Select Year</option>

                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberBSCResultTypeId" id="memberBSCResultTypeId" class="form-control" disabled>
                                                                    <option value="">Result Type </option>


                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberBSCScoreClass" id="memberBSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required disabled>
                                                            </div> 
                                                        </div> 
                                                    </div>  

                                                    <div class="col-6">
                                                        <p class="m-2 text-center text-dark">M.Sc./Ph.D/Equivalent </p>

                                                        <input name="memberMSCDegreeTypeId" id="memberMSCDegreeTypeId" type="hidden" value="4">

                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">University:</label>
                                                            <div class="col-md-8">
                                                                <select name="memberMSCBoardUniversity" id="memberMSCBoardUniversity" class="form-control chosen" disabled>
                                                                    <option value="">Select University</option>


                                                                </select>
                                                            </div>                                                    
                                                        </div>  
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Year of Passing:</label>
                                                            <div class="col-md-8">
                                                                <select id="memberMSCPassingYear" name="memberMSCPassingYear"   class="custom-select" disabled>
                                                                    <option value="">Select Year</option>

                                                                </select>
                                                            </div>                                                    
                                                        </div> 
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-md-3 text-right" for="inputName">Result :</label>
                                                            <div class="col-md-4">
                                                                <select name="memberMSCResultTypeId" id="memberMSCResultTypeId" class="form-control" disabled>
                                                                    <option value="">Result Type </option>


                                                                </select>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <input name="memberMSCScoreClass" id="memberMSCScoreClass"  type="text" class="form-control" placeholder="Score/Class" disabled>
                                                            </div> 
                                                        </div>                                                
                                                    </div>
                                                </div>                   
                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-6 offset-3">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                   

                                            </form>                                
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                <span class="h4">Section 5: Professional Record</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal " method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationProfessionalInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                                <ul class="list-inline" id="professionRecordList">
                                                    <li id="proRecList1">

                                                        <div class="form-row">
                                                            <div class="form-group col-md-3">
                                                                <label for="inputNationality">Organization *</label>
                                                                <input name="memProfOrgName1" id="memProfOrgName1" type="text" class="form-control" placeholder="Oranization name" required disabled>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputPOB">Designation *</label>
                                                                <input name="memProfDesignation1" id="memProfDesignation1" type="text" class="form-control" placeholder="Designation name" required disabled>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputPOB">Start Date *</label>
                                                                <input type="text" class="form-control" id="startDate1" name="startDate1" placeholder="yyyy-mm-dd" required="" disabled> 
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label for="inputPOB">End Date *</label>
                                                                <input type="text" class="form-control" id="endDate1" name="endDate1"  placeholder="yyyy-mm-dd" required="" disabled>
                                                            </div>
                                                        </div>
                                                        <script type="text/javascript">

                                                            jQuery('#startDate1').datepicker({
                                                                autoclose: true,
                                                                todayHighlight: true,
                                                                format: 'yyyy-mm-dd'
                                                            });
                                                            jQuery('#endDate1').datepicker({
                                                                autoclose: true,
                                                                todayHighlight: true,
                                                                format: 'yyyy-mm-dd'
                                                            });

                                                        </script>  


                                                    </li>
                                                </ul>
                                                <div class="form-row"> 
                                                    <div class="form-group col-md-6">
                                                        <a onclick="addMoreProfessionInfo();" class="btn btn-success text-white disabled"><i class="fas fa-plus-circle"></i> Add record</a>

                                                        &nbsp;&nbsp;
                                                        <a id="deleteMoreProfessionInfoBtn" onclick="deleteMoreProfessionInfo();" class="btn btn-danger text-white disabled"><i class="fas fa-minus-circle"></i> Delete record</a>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </form>

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFourDocuments">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourDocuments" aria-expanded="false" aria-controls="collapseFourDocuments">
                                                <span class="h4">Section 6: Related Documents Upload</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourDocuments" class="collapse" aria-labelledby="headingFourDocuments" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal " method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationRelatedDocumentsUpload" enctype="multipart/form-data">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="1<%//=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                                <div class="row">
                                                    <div class="col-10 offset-1">
                                                        <div class="form-row form-group">
                                                            <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">Required Document</label>

                                                            <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                                            <div class="col-md-4">
                                                                <input name="fileNID" id="fileNID" onchange="relatedDocumentUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required disabled>  
                                                                <div id="fileNIDErr"></div>
                                                            </div>
                                                            <div class="col-md-4">

                                                            </div>
                                                        </div> 


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .zip files allow. Please add below documents when create zip file.  
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   i. Certificates: SSC, HSC/Diploma, B.Sc. Engineering copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  ii. Transcripts: HSC/Diploma, B.Sc. Engineering copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Certificates of the others professional bodies (if any) copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Photocopy of NID/Smart card copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  iv. Recognized training record of last two (2) years copy
                                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   v. Details records of 150 words (member) and 200 words (fellow) on experience demonstrating competencies & abilities. copy
                                                        </p>
                                                    </div>
                                                </div>



                                            </form>

                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingFourRecom">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRecom" aria-expanded="false" aria-controls="collapseFourRecom">
                                                <span class="h4">Section 7: Recommendation Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRecom" class="collapse" aria-labelledby="headingFourRecom" data-parent="#accordion">
                                        <div class="card-body">

                                            <form id="loginform" class="form-horizontal " method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationRecomendationInfoSubmitData.jsp">
                                                <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                                <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <p class="m-2 text-center text-dark">Proposer</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-md-8 offset-2">

                                                                <input name="proposerMemberId" id="proposerMemberId" type="text" class="form-control" placeholder="Member ID (Ex. F01234)" required disabled>
                                                            </div>                                                    
                                                        </div> 
                                                         

                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="m-2 text-center text-dark">Seconder |</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-md-8 offset-2">
                                                                <input name="seconderMemberId" id="seconderMemberId" type="text" class="form-control" placeholder="Member ID (Ex. F01234)" required disabled>
                                                            </div>                                                    
                                                        </div> 
                                                           

                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="m-2 text-center text-dark">Seconder ||</p>
                                                        <div class="form-row form-group">
                                                            <div class="col-md-8 offset-2">
                                                                <input name="thirdMemberId" id="thirdMemberId" type="text" class="form-control" placeholder="Member ID (Ex. M12345)" required disabled>
                                                            </div>                                                    
                                                        </div> 
                                                       

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </form>
                                            <div class="row">
                                                <div class="col-12">
                                                    <p class="text-dark" style="font-size: 0.8rem;">Note |: Proposer and Seconder must be at least 
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.Two Fellows and one Member for Fellowship
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii.One Fellow and two Members for Membership
                                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii.Two Members for Associate Membership
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="card" id="section9">
                                    <div class="card-header" id="headingFourRegFee">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRegFee" aria-expanded="false" aria-controls="collapseFourRegFee">
                                                <span class="h4">Section 8: Registration Fee Payment Info</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFourRegFee" class="collapse" aria-labelledby="collapseFourRegFee" data-parent="#accordion">
                                        <div class="card-body">



                                            <form id="loginform" class="form-horizontal " method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationFeePaymentOptionSubmitData.jsp">



                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                                                <thead>

                                                                <th scope="col" class="bg-primary text-white"><strong>Category</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Entrance Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Annual Subscription Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Diploma Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>ID Card Fee (Tk.)</strong></th>
                                                                <th scope="col" class="bg-primary text-white"><strong>Total Amount (Tk.)</strong></th>

                                                                </thead>
                                                                <tbody>
                                                                    <%

                                                                        Object[] memTypeObject = null;
                                                                        Query memTypeSQL = null;

                                                                        String membershipApplyingType = "";
                                                                        String membershipEntranceFee = "";
                                                                        String membershipAnnualSubscriptionFee = "";
                                                                        String membershipDiplomaFee = "";
                                                                        String membershipIDCardFee = "";
                                                                        double membershipTotalPayableFee = 0.00;

                                                                        double membershipEntranceFee1 = 0.00;
                                                                        double membershipAnnualSubscriptionFee1 = 0.00;
                                                                        double membershipDiplomaFee1 = 0.00;
                                                                        double membershipIDCardFee1 = 0.00;

                                                                        //SELECT * FROM `member_type_info` 
                                                                        memTypeSQL = dbsession.createSQLQuery("SELECT * FROM member_type_info ORDER BY member_type_id  ASC");

                                                                        for (Iterator memTypeItr = memTypeSQL.list().iterator(); memTypeItr.hasNext();) {
                                                                            memTypeObject = (Object[]) memTypeItr.next();

                                                                            membershipApplyingType = memTypeObject[1].toString();
                                                                            membershipEntranceFee = memTypeObject[4].toString();
                                                                            membershipAnnualSubscriptionFee = memTypeObject[6].toString();
                                                                            membershipDiplomaFee = memTypeObject[7].toString();
                                                                            membershipIDCardFee = memTypeObject[3].toString();

                                                                            membershipEntranceFee1 = Double.parseDouble(membershipEntranceFee);
                                                                            System.out.println("Double Entry Fee:: " + membershipEntranceFee1);
                                                                            membershipAnnualSubscriptionFee1 = Double.parseDouble(membershipAnnualSubscriptionFee);
                                                                            membershipDiplomaFee1 = Double.parseDouble(membershipDiplomaFee);
                                                                            membershipIDCardFee1 = Double.parseDouble(membershipIDCardFee);

                                                                            //  membershipTotalFee = Integer.sum(membershipEntranceFee1,membershipAnnualSubscriptionFee1,membershipDiplomaFee1,membershipIDCardFee1);
                                                                            membershipTotalPayableFee = membershipEntranceFee1 + membershipAnnualSubscriptionFee1 + membershipDiplomaFee1 + membershipIDCardFee1;

                                                                    %>
                                                                    <tr>
                                                                        <td><strong><%=membershipApplyingType%></strong></td>
                                                                        <td><%=membershipEntranceFee%></td>
                                                                        <td><%=membershipAnnualSubscriptionFee%></td>
                                                                        <td><%=membershipDiplomaFee%></td>
                                                                        <td><%=membershipIDCardFee%></td>
                                                                        <td><strong><%=membershipTotalPayableFee%></strong></td>
                                                                    </tr>
                                                                    <% }%>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div> 
                                                </div> 


                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="form-row form-group">                                                
                                                            <div class="col-2 offset-5">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     


                                            </form>





                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>



                </div>
                <!-- /.white-box -->
            </div>

        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

    <%
        dbsession.clear();
        dbsession.close();
        %>

    <%@ include file="../footer.jsp" %>