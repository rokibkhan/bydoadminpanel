<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showSyUserDetails(arg1, arg2) {
            console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                console.log(data);

                console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');
        }


        function syUserInfoUpdateOpt(arg1, arg2) {
            console.log("User :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Update User Details");

            $.post("syUserListUserDetails.jsp", {userId: arg1, sessionid: arg2}, function (data) {
//
//
//                console.log(data);
//
//                console.log("invoiceDetails :: " + data[0].invoiceDetails);
//
//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');

        }

        function printInvoiceInfo(arg1) {
            var divToPrint = document.getElementById("divToPrint" + arg1);
            var popupWin = window.open('', '_blank', 'width=500,height=600');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
            popupWin.document.close();
        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Temp Member List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">Temp Member</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center" style="width: 30%;">Name</th>
                                    <th class="text-center">Registration ID</th>
                                    <th class="text-center">Password</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">View Bill</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    System.out.println("tempMemberList");

                                    int ix = 1;
                                    String reqMemId = "";
                                    String reqMemRegId = "";
                                    String reqMemRegPass = "";
                                    String reqMemName = "";
                                    String reqMemStatusText = "";

                                    String reqMemMobile = "";
                                    String reqMemEmail = "";
                                    Object objReqMem[] = null;

                                    String status1 = "";
                                    String reqMemStatus = "";
                                    String btnActivePoll = "";
                                    String btnCompletePoll = "";
                                    String committeeMemberBtn = "";
                                    String agrX = "";
                                    String divToPrint = "";

                                    Query feeSQL = null;
                                    Object[] feeObject = null;

                                    String memberShipFeeTransId = "";
                                    String memberShipFeeMemberId = "";
                                    String memberShipFeeRefNo = "";
                                    String memberShipFeeRefDesc = "";

                                    String memberShipFeePaidDate = "";
                                    String memberShipFeeYear = "";
                                    String memberShipFeeAmount = "";
                                    String memberShipFeeStatus = "";
                                    String memberShipFeeStatus1 = "";
                                    String memberShipFeeBillType = "";
                                    String memberShipFeePayOption = "";
                                    String memberShipFeePayOptionText = "";
                                    String memberShipFeeCurrency = "BDT";
                                    String memberPaymentDetailsInfo = "";
                                    String memberPaymentBtn = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id, m.member_temp_id, "
                                            + "m.member_temp_pass, m.member_name, m.father_name, m.mother_name, "
                                            + "m.place_of_birth,m.dob ,m.status, m.mobile,m.email_id  "
                                            + "FROM  member_temp m  "
                                            + "WHERE m.temp_status = '1' "
                                            + "ORDER BY m.id DESC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            objReqMem = (Object[]) it1.next();
                                            reqMemId = objReqMem[0].toString().trim();

                                            reqMemRegId = objReqMem[2] == null ? "" : objReqMem[2].toString().trim();
                                            reqMemRegPass = objReqMem[3] == null ? "" : objReqMem[3].toString().trim();

                                            reqMemName = objReqMem[4] == null ? "" : objReqMem[4].toString().trim();
                                            reqMemStatus = objReqMem[9] == null ? "" : objReqMem[9].toString().trim();

                                            if (reqMemStatus.equals("0")) {
                                                reqMemStatusText = "<a class=\"btn btn-info waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-undo\"></i> Pending</a>";
                                            }

                                            if (reqMemStatus.equals("1")) {
                                                reqMemStatusText = "<a class=\"btn btn-success waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-check\"></i> Approved</a>";
                                            }
                                            if (reqMemStatus.equals("2")) {
                                                reqMemStatusText = "<a class=\"btn btn-danger waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-times-circle\"></i> Declined</a>";
                                            }

                                            reqMemMobile = objReqMem[10] == null ? "" : objReqMem[10].toString().trim();
                                            reqMemEmail = objReqMem[11] == null ? "" : objReqMem[11].toString().trim();

                                            String reqMemAddOthersInfoUrl = GlobalVariable.baseUrl + "/memberManagement/memberAddMyPictureUpload.jsp?sessionid=" + session.getId() + "&regTempId=" + reqMemRegId + "&regMemId=" + reqMemId + "&regStep=3&strMsg=";

                                            String reqMemDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/requestMemberDetails.jsp?sessionid=" + session.getId() + "&reqMemId=" + reqMemId + "&selectedTab=profile";

                                            
                                            feeSQL = dbsession.createSQLQuery("SELECT mf.txn_id,mf.member_id,mf.ref_no,mf.ref_description, "
                                                    + "mf.txn_date,mf.paid_date,mf.fee_year,mf.amount,mf.status,mf.due_date,mf.bill_type,"
                                                    + "mfy.member_fees_year_name  "
                                                    + "FROM member_fee_temp mf,member_fees_year mfy WHERE mf.fee_year = mfy.member_fees_year_id AND mf.member_id= '" + reqMemId + "'");

                                            for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                                                feeObject = (Object[]) feeItr.next();
                                                memberShipFeeTransId = feeObject[0].toString();
                                                memberShipFeeMemberId = feeObject[1].toString();
                                                memberShipFeeRefNo = feeObject[2].toString();
                                                memberShipFeeRefDesc = feeObject[3].toString();

                                                memberShipFeePaidDate = feeObject[5].toString();
                                                memberShipFeeYear = feeObject[11].toString();
                                                memberShipFeeAmount = feeObject[7].toString();
                                                memberShipFeeStatus = feeObject[8].toString();

                                                System.out.println("memberShipFeeStatus :: " + memberShipFeeStatus);

                                                System.out.println("memberPaymentDetailsInfo :: " + memberShipFeeRefDesc);

                                                memberShipFeeStatus1 = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";

                                                memberShipFeeBillType = feeObject[10] == null ? "" : feeObject[10].toString().trim();

                                               
                                               

                                                divToPrint = "<div id=\"divToPrint" + reqMemId + "\" style=\"display:none;\"><!-- Start:  Print Container  -->"
                                                        + "<html>"
                                                        + "<head><title>Bill Invoice Print</title></head>"
                                                        + "<body>"
                                                        + "<div style=\"width: 790px; margin: 0px auto; margin-top: 1.5cm;\">"
                                                        + "<table style=\"width: 95%; margin-bottom: 5px;\">"
                                                        + "<tr>"
                                                        + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                        + "<td style=\"padding: 5px; text-align: right;\">"
                                                        + "<span style=\"font-size: 12px; font-weight: bold;\">Office Copy</span>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">"
                                                        + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\">"
                                                        + "</td>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                        + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                        + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                        + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">&nbsp;</td>"
                                                        + "<td style=\"text-align: center;\">"
                                                        + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span>"
                                                        + "</td>"
                                                        + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td style=\"text-align: center;\">"
                                                        + "<table>"
                                                        + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + reqMemName + "</td></tr>"
                                                        + "<tr><td style=\"text-align: left;\"><strong>Member Temp ID </strong></td><td>:" + reqMemRegId + "</td></tr>"
                                                        + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + reqMemMobile + "</td></tr>"
                                                        + "</table>"
                                                        + "</td>"
                                                        + "<td style=\"text-align: center;\">"
                                                        + "<table>"
                                                        + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeePaidDate + "</td></tr>"
                                                        + "<tr><td><strong>Invoice No</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                                                        + "<tr><td><strong>Status</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                                                        + "</table>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<thead>"
                                                        + "<tr>"
                                                        + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                        + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                        + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                        + "</tr>"
                                                        + "</thead>"
                                                        + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                        + "<tr>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeYear + "</td>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeBillType + "</td>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                        + "</tr>"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                        + "</tr>"
                                                        + "</tbody>"
                                                        + "</table> <!--End : invoice product list details-->"
                                                        + "<table style=\"width: 95%; border-top:1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                        + "<span style=\"font-size: 12px; font-weight: bold;\">This is computer generated invoice no signature required.</span>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; border-top:1px dotted #ccc; border-collapse: collapse; margin-top: 30px;;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; margin-bottom: 5px;\">"
                                                        + "<tr>"
                                                        + "<td style=\"padding: 5px;\">&nbsp;</td>"
                                                        + "<td style=\"padding: 5px; text-align: right;\">"
                                                        + "<span style=\"font-size: 12px; font-weight: bold;\">Member Copy</span>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">"
                                                        + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\">"
                                                        + "</td>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                        + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                        + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                        + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\">&nbsp;</td>"
                                                        + "<td style=\"text-align: center;\">"
                                                        + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span>"
                                                        + "</td>"
                                                        + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td style=\"text-align: center;\">"
                                                        + "<table>"
                                                        + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + reqMemName + "</td></tr>"
                                                        + "<tr><td style=\"text-align: left;\"><strong>Member Temp ID </strong></td><td>:" + reqMemRegId + "</td></tr>"
                                                        + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + reqMemMobile + "</td></tr>"
                                                        + "</table>"
                                                        + "</td>"
                                                        + "<td style=\"text-align: center;\">"
                                                        + "<table>"
                                                        + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeePaidDate + "</td></tr>"
                                                        + "<tr><td><strong>Invoice No</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                                                        + "<tr><td><strong>Status</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                                                        + "</table>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<thead>"
                                                        + "<tr>"
                                                        + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                        + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                        + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                        + "</tr>"
                                                        + "</thead>"
                                                        + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                        + "<tr>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeYear + "</td>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeBillType + "</td>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                        + "</tr>"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                        + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                        + "</tr>"
                                                        + "</tbody>"
                                                        + "</table> <!--End : invoice product list details-->"
                                                        + "<table style=\"width: 95%; border-top:1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                        + "<tr>"
                                                        + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                        + "<span style=\"font-size: 12px; font-weight: bold;\">This is computer generated invoice no signature required.</span>"
                                                        + "</td>"
                                                        + "</tr>"
                                                        + "</table>"
                                                        + "</div>"
                                                        + "</body>"
                                                        + "</html>"
                                                        + "</div><!-- End:  Print Container  -->";

                                            }


                                %>


                                <tr id="infoBox<%=reqMemId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(reqMemName);%></td>
                                    <td><% out.print(reqMemRegId);%></td>
                                    <td><% out.print(reqMemRegPass);%></td>
                                    <td class="text-center"><% out.print(reqMemStatusText);%></td>
                                    <td>
                                        <a onclick="printInvoiceInfo('<%=reqMemId%>');"  class="btn btn-primary btn-sm">&nbsp; <i class="fa fa-file"></i>&nbsp; </a>

                                        <%=divToPrint%>
                                    </td> 
                                    <td class="text-center">

                                        <a title="Add Othrs Info" href="<%=reqMemAddOthersInfoUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"> Add Others Info</i></a>
                                        <a title="Details" href="<%=reqMemDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"> Details</i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                %>



                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>