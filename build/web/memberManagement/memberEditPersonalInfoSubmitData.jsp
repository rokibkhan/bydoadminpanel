<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // Date date1 = new Date();
    //  String entryDate = dateFormat.format(date1);
//    if (session.isNew()) {
//        response.sendRedirect("logout.jsp");
//    }
    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        memberIdH = request.getParameter("memberEditId").trim();

        System.out.println("memberIdH " + memberIdH);

        String memberName = request.getParameter("memberName").trim().toUpperCase();
        String fatherName = request.getParameter("fatherName") == null ? "" : request.getParameter("fatherName").trim();
        String motherName = request.getParameter("motherName").trim();
        String memberEmail = request.getParameter("memberEmail").trim();
        String memberMobile = request.getParameter("memberMobile").trim();
        String memberBloodGroup = request.getParameter("memberBloodGroup").trim();
        String memberGender = request.getParameter("memberGender").trim();
        String memberDOB = request.getParameter("memberDOB") == null ? "" : request.getParameter("memberDOB").trim();

        // String addterm = request.getParameter("entryTerm");
        //  String addip = request.getParameter("entryIP");
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();

        Member member = null;

        Query q1 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

        Iterator itr1 = q1.list().iterator();

        if (itr1.hasNext()) {
            member = (Member) itr1.next();
        }

        member.setMemberName(memberName);
        member.setFatherName(fatherName);
        member.setMotherName(motherName);
        member.setEmailId(memberEmail);
        member.setGender(memberGender);
        member.setBloodGroup(memberBloodGroup);
        member.setMobile(memberMobile);

        if (!memberDOB.equals("")) {

            Date dateSSS = dateFormat.parse(memberDOB);
//           
//           Date dateSSS = dateFormat.format(memberDOB);
//            
//           
            member.setDob(dateSSS);
        }

        dbsession.update(member);
        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
            strMsg = "Success!!! Personal Information Updated";
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=profile&strMsg=" + strMsg);
        } else {
            dbtrx.rollback();
            strMsg = "Error!!! When Personal Information Updated";
            response.sendRedirect("memberEdit.jsp?sessionid=" + sessionIdH + "&memberId=" + memberIdH + "&selectedTab=profile&strMsg=" + strMsg);
        }
    } else {
        System.out.println("DDDD LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
        return;
    }

%>
