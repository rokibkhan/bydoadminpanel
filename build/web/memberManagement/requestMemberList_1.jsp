<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        // System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            //console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }
        function invoicePaymentSubmit(arg1, arg2) {
            var rupantorLGModal, invoiceDuePaymentDate, invoiceDuePaymentMode, invoiceDueAccountHead, invoiceDuePaymentAmount;
            // console.log("invoicePaymentSubmit :: " + arg1);

            $("#btnInvoicePaymentSubmit").button("loading");
            rupantorLGModal = $('#rupantorLGModal');

            invoiceDuePaymentDate = document.getElementById("invoiceDuePaymentDate").value;
            invoiceDuePaymentMode = document.getElementById("invoiceDuePaymentMode").value;
            invoiceDueAccountHead = document.getElementById("invoiceDueAccountHead").value;
            invoiceDuePaymentAmount = document.getElementById("invoiceDuePaymentAmount").value;


            if (invoiceDuePaymentDate == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentDate").focus();
                return false;
            }
            if (invoiceDuePaymentMode == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentMode").focus();
                return false;
            }

            if (invoiceDueAccountHead == '') {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDueAccountHead").focus();
                return false;
            }


            if (invoiceDuePaymentAmount == '' || invoiceDuePaymentAmount == 0) {
                $("#btnInvoicePaymentSubmit").button("reset");
                $("#invoiceDuePaymentAmount").focus();
                return false;
            }

            $.post("invoicePaymentSubmit.jsp", {invoiceId: arg1, sessionid: arg2, invoiceDuePaymentDate: invoiceDuePaymentDate, invoiceDuePaymentMode: invoiceDuePaymentMode, invoiceDueAccountHead: invoiceDueAccountHead, invoiceDuePaymentAmount: invoiceDuePaymentAmount}, function (data) {

                //  console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    rupantorLGModal.modal('hide');
                    var btnInfoX = data[0].selectInvId;
                    if (Number(data[0].invoiceDuePaymentAmount) == Number(data[0].invoiceDueAmount)) {
                        $("#btnShowInvoiceDetails" + btnInfoX).removeClass("btn-danger").addClass("btn-primary");
                    }
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");

                } else {
                    $("#btnInvoicePaymentSubmit").button("reset");
                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgText).show().delay(3000).fadeOut("slow");
                    rupantorLGModal.modal('show');
                }

//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
//                rupantorLGModal.modal('show');

            }, "json");

        }
        function showSyUserDetails(arg1, arg2) {
            // console.log("Invoice :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Details");

            $.post("showInvoiceDetails.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {


                //   console.log(data);

                //   console.log("invoiceDetails :: " + data[0].invoiceDetails);

                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');
        }


        function syUserInfoUpdateOpt(arg1, arg2) {
            // console.log("User :: " + arg1);

//            var invoiceID = arg1.trim();
//            console.log("InvoiceTrim :: " + invoiceID);
            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');


            //   rupantorLGModal.modal('show');

            rupantorLGModal.find("#rupantorLGModalTitle").text("Update User Details");

            $.post("syUserListUserDetails.jsp", {userId: arg1, sessionid: arg2}, function (data) {
//
//
//                console.log(data);
//
//                console.log("invoiceDetails :: " + data[0].invoiceDetails);
//
//                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
//                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                //   rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                rupantorLGModal.modal('show');

//                if (data.dataError == "NO") {
//                    var clntAA = data.client;
//                    nJQry("#clientState" + clntAA).html(data.newClientState);
//                    nJQry("#agentState").html(data.newAgentState);
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//// 							nJQry( "#inlineCheckbox1"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox2"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox3"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox4"+clntAA).prop( "checked", false );
//// 							nJQry( "#inlineCheckbox5"+clntAA).prop( "checked", false );
//                    nJQry("#inlineCheckbox1" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox2" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox3" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox4" + clntAA).attr("checked", false);
//                    nJQry("#inlineCheckbox5" + clntAA).attr("checked", false);
//                    nJQry("#agentPaymentInputBox" + clntAA).val("");
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }
//
//                if (data.dataError == "YES") {
//                    var clntAA = data.client;
//                    nJQry("#updatePMsg" + clntAA).html(data.msg);
//                    //	nJQry('#loadingbig').hide();
//                    nJQry("#btnClientPaymentByAgent" + clntAA).button("reset");
//                }


            }, "json");

            //  rupantorLGModal.modal('show');

        }



    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">MemberShip Request List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Member Management</a></li>                    
                    <li class="active">MemberShip</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center" style="width: 30%;">Name</th>
                                    <th class="text-center">Registration ID</th>
                                    <th class="text-center">Payment</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%

                                    dbsession = HibernateUtil.getSessionFactory().openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    // DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat dateFormatEventM = new SimpleDateFormat("MMM");
                                    DateFormat dateFormatEventD = new SimpleDateFormat("dd");
                                    DateFormat dateFormatEventY = new SimpleDateFormat("Y");

                                    Date dateNewsM = null;
                                    Date dateNewsD = null;
                                    Date dateNewsY = null;
                                    String newDateNewsM = "";
                                    String newDateNewsD = "";
                                    String newDateNewsY = "";

                                    int ix = 1;
                                    String reqMemId = "";
                                    String reqMemRegId = "";
                                    String reqMemName = "";
                                    String reqMemStatusText = "";
                                    String applicationDate = "";
                                    String committeeDuration = "";
                                    String committeeDesc = "";
                                    String committeeStatus = "";
                                    Object objReqMem[] = null;

                                    String status1 = "";
                                    String reqMemStatus = "";
                                    String btnActivePoll = "";
                                    String btnCompletePoll = "";
                                    String committeeMemberBtn = "";
                                    String agrX = "";

                                    Object memberPaymentObj[] = null;

                                    String reqMemPaymentAmount = "";
                                    String reqMemPaymentStatus = "";
                                    String reqMemPaymentStatusText = "";

                                    Query usrSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id, m.member_temp_id, "
                                            + "m.member_temp_pass, m.member_name, m.father_name, m.mother_name, "
                                            + "m.place_of_birth,m.dob ,m.status,m.add_date "
                                            + "FROM  member_temp m ORDER BY m.id DESC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            objReqMem = (Object[]) it1.next();
                                            reqMemId = objReqMem[0].toString().trim();
                                            reqMemRegId = objReqMem[2] == null ? "" : objReqMem[2].toString().trim();
                                            reqMemName = objReqMem[4] == null ? "" : objReqMem[4].toString().trim();
                                            reqMemStatus = objReqMem[9] == null ? "" : objReqMem[9].toString().trim();

                                            if (reqMemStatus.equals("0")) {
                                                reqMemStatusText = "<a class=\"btn btn-info waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-undo\"></i> Pending</a>";
                                            }

                                            if (reqMemStatus.equals("1")) {
                                                reqMemStatusText = "<a class=\"btn btn-success waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-check\"></i> Approved</a>";
                                            }
                                            if (reqMemStatus.equals("2")) {
                                                reqMemStatusText = "<a class=\"btn btn-danger waves-effect waves-light m-r-5 disabled\"> <i class=\"fa fa-times-circle\"></i> Declined</a>";
                                            }

                                            Query memberPaymentSQL = dbsession.createSQLQuery("SELECT * FROM member_fee_temp where member_id='" + reqMemId + "'");

                                            System.out.println("memberRequestList memberPaymentSQL ::" + memberPaymentSQL);

                                            if (!memberPaymentSQL.list().isEmpty()) {
                                                for (Iterator memberPaymentItr = memberPaymentSQL.list().iterator(); memberPaymentItr.hasNext();) {

                                                    memberPaymentObj = (Object[]) memberPaymentItr.next();
                                                    reqMemPaymentAmount = memberPaymentObj[7] == null ? "" : memberPaymentObj[7].toString().trim();
                                                    reqMemPaymentStatus = memberPaymentObj[8] == null ? "" : memberPaymentObj[8].toString().trim();
                                                    //  reqMemRegId = memberPaymentObj[2] == null ? "" : memberPaymentObj[2].toString().trim();

                                                    if (reqMemPaymentStatus.equals("1")) {
                                                        reqMemPaymentStatusText = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
                                                    } else {
                                                        reqMemPaymentStatusText = "<span class=\"btn btn-danger btn-sm waves-effect\">Fail</span>";
                                                        // payOnline = "<a title=\"Pay Online\" href=\"" + GlobalVariable.baseUrl + "/member/memberPaymentProcessing.jsp?sessionid=" + session.getId() + "&act=add&&memberFeeId=" + memberFeeId + "\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-credit-card\"></i></a>";
                                                    }

                                                    applicationDate = memberPaymentObj[4] == null ? "" : memberPaymentObj[4].toString().trim();

                                                    dateNewsM = dateFormatEvent.parse(applicationDate);
                                                    newDateNewsM = dateFormatEventM.format(dateNewsM);

                                                    dateNewsD = dateFormatEvent.parse(applicationDate);
                                                    newDateNewsD = dateFormatEventD.format(dateNewsD);

                                                    dateNewsY = dateFormatEvent.parse(applicationDate);
                                                    newDateNewsY = dateFormatEventY.format(dateNewsY);

                                                }
                                            } else {

                                                reqMemPaymentAmount = "";
                                                reqMemPaymentStatusText = "";
                                            }
                                            String reqMemDetailsUrl = GlobalVariable.baseUrl + "/memberManagement/requestMemberDetails.jsp?sessionid=" + session.getId() + "&reqMemId=" + reqMemId + "&selectedTab=profile";


                                %>


                                <tr id="infoBox<%=reqMemId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(reqMemName);%></td>
                                    <td><% out.print(reqMemRegId);%></td>
                                    <td class="text-center"><% out.print(reqMemPaymentAmount);%> <br/><%=reqMemPaymentStatusText%></td>
                                    <td class="text-center"><% out.print(reqMemStatusText);%></td>
                                    <td><% out.print(dateNewsD);%><% //out.print(newDateNewsM);%> <% //out.print(newDateNewsY);%></td>
                                    <td class="text-center">

                                        <a title="Details" href="<%=reqMemDetailsUrl%>" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                %>



                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>