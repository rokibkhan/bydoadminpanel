<%@page import="com.appul.entity.VideoSlug"%>

<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">


        var modifyFlag = false;


        function videoDeleteInfo(arg1, arg2) {

            console.log("videoDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            rupantorLGModal = $('#rupantorLGModal');
            btnConfirmInfo = "<a id=\"videoDeleteInfoConfirmBtn\" onclick=\"videoDeleteInfoConfirm(" + arg1 + ",'" + arg2 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

            rupantorLGModal.find("#rupantorLGModalTitle").text("Confirmation information delete");
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            rupantorLGModal.find("#rupantorLGModalFooter").html(btnConfirmInfo);

            rupantorLGModal.modal('show');

        }

        function videoEditInfo(arg1, arg2) {
            console.log("videoEdit: " + arg1 + "-" + arg2);
        }



        function videoDeleteInfoConfirm(arg1, arg2) {
            console.log("videoDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');

            $.post("videoInfoDeleteProcess.jsp", {deleteItemId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    rupantorLGModal.modal('hide');
                } else {
                    rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }


            }, "json");

        }


        function addCenterForVideo(arg1, arg2, arg3) {
            console.log("addCenterForVideo :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addCenterForVideoConfirmBtn\" onclick=\"addCenterForVideoConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Center for Video");

            $.post("videoCenterInfoShow.jsp", {videoId: arg1, videoName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addCenterForVideoConfirm(arg1, arg2, arg3) {
            console.log("addCenterForVideoConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Center is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("videoCenterInfoAddConfirm.jsp", {videoId: arg1, videoName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }

        function addDivisionForVideo(arg1, arg2, arg3) {
            console.log("addDivisionForVideo :: " + arg1);
            var taskLGModal, btnConfirmInfo;

            taskLGModal = $('#taskLGModal');
            btnConfirmInfo = "<a id=\"addDivisionForVideoConfirmBtn\" onclick=\"addDivisionForVideoConfirm(" + arg1 + ",'','" + arg3 + "')\" class=\"btn btn-primary\"> Confirm </a> <button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";
            taskLGModal.find("#taskLGModalTitle").text("Add Division for Video");

            $.post("videoDivisionInfoShow.jsp", {videoId: arg1, videoName: arg2, sessionid: arg3}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    //  $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    //   $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');
                    //   taskLGModal.modal('hide');


                    taskLGModal.find("#taskLGModalBody").html(data[0].mainInfoContainer);
                    taskLGModal.find("#taskLGModalFooter").html(btnConfirmInfo);

                    taskLGModal.modal('show');
                } else {
                    taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }

            }, "json");



        }

        function addDivisionForVideoConfirm(arg1, arg2, arg3) {
            console.log("addDivisionForVideoConfirm :: " + arg1);
            var taskLGModal, centerDivisionTagId;

            taskLGModal = $('#taskLGModal');

            centerDivisionTagId = $("#centerDivisionTagId").val();
            //   alert("###" + centerDivisionTagId);
            //   document.getElementById(arg1)

            if (centerDivisionTagId == null || centerDivisionTagId == "") {
                $("#centerDivisionTagId").focus();
                $("#centerDivisionTagIdErr").addClass("help-block with-errors").html("Division is required").css({"background-color": "#fff", "border": "none", "color": "red"});
                return false;
            } else {
                $("#centerDivisionTagIdErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
            }

            $.post("videoDivisionInfoAddConfirm.jsp", {videoId: arg1, videoName: arg2, sessionid: arg3, centerDivisionTagId: centerDivisionTagId}, function (data) {


                console.log(data);

                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                    // $("#infoBox" + data[0].requestId).html('<td align="center" colspan="8"><h5>' + data[0].responseMsg + '</h5></td>');

                    $("#assignBox" + data[0].requestId).html(data[0].centerDivisionTagContainer);
                    taskLGModal.modal('hide');
                } else {
                    // taskLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    taskLGModal.find("#centerDivisionTagAllErr").html(data[0].responseMsgHtml);
                }

            }, "json");
        }




    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .video title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="video-title">Video List</h4>

            </div>
            <!-- /.video title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Video Management</a></li>                    
                    <li class="active">Videos</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Video Title</th>
                                    <th>Chapter Name</th>
                                    <th>Session Name</th>
                                    <th>Subject Name</th>
                                    <th>Course Name</th>
                                    <th class="text-center">Thumbnail</th>
                                    <th class="text-center">Video</th>
                                 <!--   <th class="text-center">Assign Video</th> -->
                                    <th class="text-center" style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();

                                    int ix = 1;
                                    String videoId = "";
                                    String videoTitle = "";
                                    String videoCaptions = "";
                                    String videoOriginalLink = "";
                                    String videoEmbedLink = "";
                                    String videoEmbedLink1 = "";
                                    String videoThumbnail = "";
                                    String videoThumbnailn = "";
                                    String videoCategory = "";
                                    String videoChapterName = "";
                                    String videoChapterSessionName = ""; 
                                    String videoChapterSessionSubjectName = ""; 
                                    String videoChapterSessionSubjectCourseName = ""; 
                                    String agrX = "";
                                    String agrXcenter = "";
                                    String btnAddCenter = "";
                                    String btnAddSubCenter = "";
                                    String btnAddDivision = "";
                                    String btnAddTag = "";

                                    String centerDivisionTagContainer = "";

                                    String centerListContainer = "";
                                    //VideoCenter vdc = null;
                                    String centerListCenterName = "";
                                    Query centerListSQL = null;

                                    String divisionListContainer = "";
                                    //VideoDivision vdd = null;
                                    String divisionListCenterName = "";
                                    Query divisionListSQL = null;

                                    String tagListContainer = "";
                                    VideoSlug vds = null;
                                    String tagListCenterName = "";
                                    Query tagListSQL = null;

                                    Query usrSQL = dbsession.createSQLQuery("SELECT ni.id_video,ni.video_category, ni.video_title, ni.video_caption, ni.video_orginal_link,"
                                            + " ni.video_emded_link,ni.feature_image,ci.chapter_name,"
                                            + " si.session_name,sui.subject_name, coi.course_name "
                                            + "FROM  video_gallery_info ni "
                                            + " join chapter_info ci on ci.id_chapter = ni.chapter_id"
                                            + " join session_info si on ci.session_id = si.id_session"
                                            + " join subject_info sui on  sui.id_subject = si.subject_id"
                                            + " join course_info coi on coi.id_course = sui.course_id"
                                            + " ORDER BY ni.id_video DESC");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                                            obj = (Object[]) it1.next();
                                            videoId = obj[0].toString().trim();
                                            videoCategory = obj[1].toString().trim();
                                            videoTitle = obj[2].toString().trim();
                                            videoCaptions = obj[3].toString().trim();
                                            videoOriginalLink = obj[4].toString().trim();
                                            videoEmbedLink = obj[5] == null ? "" : obj[5].toString().trim();

                                            videoEmbedLink1 = "<iframe width=\"250\" height=\"150\" src=\"" + videoEmbedLink + "\"></iframe>";

                                            videoThumbnail = obj[6] == null ? "" : obj[6].toString().trim();
                                            videoChapterName = obj[7] == null ? "" : obj[7].toString().trim();
                                            videoChapterSessionName = obj[8] == null ? "" : obj[8].toString().trim();
                                            videoChapterSessionSubjectName = obj[9] == null ? "" : obj[9].toString().trim();
                                            videoChapterSessionSubjectCourseName = obj[10] == null ? "" : obj[10].toString().trim();
                                            videoThumbnailn = "<img width=\"250\" height=\"150\" src=\"" + videoThumbnail + "\" alt=\"" + videoTitle + "\">";

                                        //   agrXcenter = "'" + videoId + "','" + videoTitle + "','" + session.getId() + "'";
                                            agrXcenter = "'" + videoId + "','','" + session.getId() + "'";
                                            
                                            btnAddCenter = "<a onClick=\"addCenterForVideo(" + agrXcenter + ");\" title=\"Add Center  for this Video\"  class=\"btn btn-primary btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Center</a>";
                                            //     btnAddSubCenter = "<a onClick=\"addSubCenterForVideo(" + agrXcenter + ");\" title=\"Add SubCenter  for this Video\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Sub Center</a>";

                                            btnAddDivision = "<a onClick=\"addDivisionForVideo(" + agrXcenter + ");\" title=\"Add Division  for this Video\"  class=\"btn btn-success btn-sm  m-b-5\"><i class=\"fa fa-plus\"></i> Add Division</a>";

                                            // show video Assign center,division,tag
                                            centerListContainer = "";
                                            

//            tagListSQL = dbsession.createQuery(" FROM AlbumSlug where pictureCategory = '" + eventId + "'");
//            if (!tagListSQL.list().isEmpty()) {
//                for (Iterator tagListItr = tagListSQL.list().iterator(); tagListItr.hasNext();) {
//                    alt = (AlbumSlug) tagListItr.next();
//                    tagListCenterName = alt.get
//
//                    tagListContainer = tagListContainer + tagListCenterName + ",";
//                }
//            } else {
//                tagListContainer = "";
//            }
                                            centerDivisionTagContainer = centerListContainer + divisionListContainer;


                                %>


                                <tr id="infoBox<%=videoId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(videoTitle);%></td>
                                    <td><% out.print(videoChapterName);%></td>
                                    <td><% out.print(videoChapterSessionName);%></td>
                                    <td><% out.print(videoChapterSessionSubjectName);%></td>
                                    <td><% out.print(videoChapterSessionSubjectCourseName);%></td>
                                    
                                    <td><% out.println(videoThumbnailn); %></td>
                                    <td><% out.print(videoEmbedLink1);%></td>
                                    <td class="text-center">

                                        <a onClick="videoDeleteInfo(<%=videoId%>);" title="Delete Video" class="btn btn-primary btn-sm"><i class="icon-trash"></i></a>
                                    </td>  
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }
                                    dbsession.clear();
                                    dbsession.close();
                                    %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->



    <%@ include file="../footer.jsp" %>