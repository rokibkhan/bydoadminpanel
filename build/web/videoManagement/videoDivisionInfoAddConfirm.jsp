<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    Object centerObj[] = null;

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    String videoId = request.getParameter("videoId");
    String videoName = request.getParameter("videoName");
    String centerDivisionTagId = request.getParameter("centerDivisionTagId");

    System.out.println("videoDivisionInfoShowAddConfirm :: videoId " + videoId);
    System.out.println("videoDivisionInfoShowAddConfirm :: videoName " + videoName);
    System.out.println("videoDivisionInfoShowAddConfirm :: centerDivisionTagId " + centerDivisionTagId);

    q1 = dbsession.createSQLQuery("SELECT * FROM video_division WHERE VIDEO_ID=" + videoId + " AND DIVISION_ID =" + centerDivisionTagId + "");
    System.out.println("videoDivisionInfoShowAddConfirm :: q1 " + q1);
    if (!q1.list().isEmpty()) {

        responseCode = "0";
        responseMsg = "Error!!! Already video assign this division";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", videoId);
        jsonArr.add(json);
    } else {

        //insert video id and center id in video_center table
        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(51);
        int videoCenterId = Integer.parseInt(idS);

        Query videoCenterAddSQL = dbsession.createSQLQuery("INSERT INTO video_division("
                + "ID,"
                + "VIDEO_ID,"
                + "DIVISION_ID"
                + ") VALUES("
                + "'" + videoCenterId + "',"
                + "'" + videoId + "',"
                + "'" + centerDivisionTagId + "'"
                + "  ) ");

        System.out.println("videoDivisionInfoShowAddConfirm :: videoCenterAddSQL " + videoCenterAddSQL);

        videoCenterAddSQL.executeUpdate();

        dbtrx.commit();

        if (dbtrx.wasCommitted()) {
            //  strMsg = "Success !!! New Committee added";
            //  response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "1";
            responseMsg = "Success!!! Division add for video successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            // show video Assign center,division,tag
            String centerDivisionTagContainer = "";

            String centerListContainer = "";
            VideoCenter vdc = null;
            String centerListCenterName = "";
            Query centerListSQL = null;

            String divisionListContainer = "";
            VideoDivision vdd = null;
            String divisionListCenterName = "";
            Query divisionListSQL = null;

            String tagListContainer = "";
            VideoSlug vds = null;
            String tagListCenterName = "";
            Query tagListSQL = null;

            centerListSQL = dbsession.createQuery(" FROM VideoCenter where videoGalleryInfo = '" + videoId + "'");
            if (!centerListSQL.list().isEmpty()) {
                for (Iterator centerListItr = centerListSQL.list().iterator(); centerListItr.hasNext();) {
                    vdc = (VideoCenter) centerListItr.next();
                    centerListCenterName = vdc.getCenter().getCenterName();

                    centerListContainer = centerListContainer + centerListCenterName + ",";
                }
            } else {
                centerListContainer = "";
            }

            divisionListSQL = dbsession.createQuery(" FROM VideoDivision where videoGalleryInfo = '" + videoId + "'");
            if (!divisionListSQL.list().isEmpty()) {
                for (Iterator divisionListItr = divisionListSQL.list().iterator(); divisionListItr.hasNext();) {
                    vdd = (VideoDivision) divisionListItr.next();
                    divisionListCenterName = vdd.getMemberDivision().getMemDivisionName();

                    divisionListContainer = divisionListContainer + divisionListCenterName + ",";
                }
            } else {
                divisionListContainer = "";
            }

//            tagListSQL = dbsession.createQuery(" FROM AlbumSlug where pictureCategory = '" + eventId + "'");
//            if (!tagListSQL.list().isEmpty()) {
//                for (Iterator tagListItr = tagListSQL.list().iterator(); tagListItr.hasNext();) {
//                    alt = (AlbumSlug) tagListItr.next();
//                    tagListCenterName = alt.get
//
//                    tagListContainer = tagListContainer + tagListCenterName + ",";
//                }
//            } else {
//                tagListContainer = "";
//            }
            centerDivisionTagContainer = centerListContainer + divisionListContainer;

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("centerDivisionTagContainer", centerDivisionTagContainer);
            json.put("requestId", videoId);
            jsonArr.add(json);

        } else {
            dbtrx.rollback();
            //   strMsg = "Error!!! When Committee add";
            //   response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

            responseCode = "0";
            responseMsg = "Error!!! When video assign this center";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            json.put("requestId", videoId);
            jsonArr.add(json);
        }

    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>