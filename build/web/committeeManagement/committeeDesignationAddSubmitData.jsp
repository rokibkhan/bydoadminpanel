<%-- 
    Document   : Sysytem User dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(50);
        int commID = Integer.parseInt(idS);

        String committeeId = request.getParameter("committeeId");
        String committeeDesignation = request.getParameter("committeeDesignation");

        System.out.println("committeeId :: " + committeeId);
        System.out.println("committeeDesignation ::" + committeeDesignation);

        Object obj[] = null;
        Query q1 = null;

        q1 = dbsession.createSQLQuery("SELECT * FROM committee_designation WHERE designation_name = '" + committeeDesignation + "' AND committee_id = '" + committeeId + "'");
        if (q1.list().isEmpty()) {

                     

            Query committeeAddSQL = dbsession.createSQLQuery("INSERT INTO committee_designation("
                    + "id,"
                    + "designation_name,"
                    + "short_name,"
                    + "orderby,"
                    + "committee_id"
                    + ") VALUES("
                    + "'" + commID + "',"
                    + "'" + committeeDesignation + "',"
                    + "'" + committeeDesignation + "',"
                    + "'1',"
                    + "'" + committeeId + "'"
                    + "  ) ");

            committeeAddSQL.executeUpdate();

            
            dbsession.flush();
            dbtrx.commit();

            if (dbtrx.wasCommitted()) {
                strMsg = "Success !!! New Designation added";
                response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeDesignationAdd.jsp?sessionid=" + sessionIdH + "&committeeId=" + committeeId + "&strMsg=" + strMsg + "");
            } else {
                dbtrx.rollback();
                strMsg = "Error!!! When Designation add";
                response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeDesignationAdd.jsp?sessionid=" + sessionIdH + "&committeeId=" + committeeId + "&strMsg=" + strMsg + "");
            }

        } else {
            strMsg = "Error!!! Designation already exits";
            response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeDesignationAdd.jsp?sessionid=" + sessionIdH + "&committeeId=" + committeeId + "&strMsg=" + strMsg + "");
        }

    } else {
        strMsg = "Error!!! When adding committee Designation.Please try again";
        response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeDesignationAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
    }

    dbsession.clear();
    dbsession.close();
    %>
