<%@page import="java.net.InetAddress"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Query commMemberCheckSQL = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    getRegistryID regId = new getRegistryID();
    String subscribeId = regId.getID(14);
    //memberId: arg2,jobId:arg3

    String sessionId = request.getParameter("sessionId").trim();
    String proposerMemberId = request.getParameter("memberShipId").trim();
    String committeeId = request.getParameter("committeeId").trim();

    String name = "";
    System.out.println("sessionId " + sessionId);

    String memberId = "";
    String memberName = "";
    String memberMobile = "";
    String memberEmail = "";
    String memberPicture = "";
    String memberPictureUrl = "";
    String memberInfoCon = "";
    String memberStatusInCommittee = "";

    q1 = dbsession.createSQLQuery("SELECT * FROM member WHERE member_id = '" + proposerMemberId + "'");
    if (!q1.list().isEmpty()) {

        for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
            obj = (Object[]) itr2.next();
            memberId = obj[0].toString();
            memberName = obj[2].toString();
            memberMobile = obj[13] == null ? "" : obj[13].toString();
            memberEmail = obj[14] == null ? "" : obj[14].toString();

            memberPicture = obj[21].toString();

           // memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPicture;
       
            memberPictureUrl = "http://joybanglabd.org:8252/IEBWEB/upload/member/" + memberPicture;
            

            memberInfoCon = "<div class=\"col-md-12 col-sm-12\">"
                    + "<div class=\"white-box\" style=\"min-height: 260px; padding: 5px; border:1px solid #ccc;\">"
                    + "<div class=\"row\" id=\"memberStatusInCommittee\"></div>"
                    + "<div class=\"row\">"
                    + "<div class=\"col-md-4 col-sm-4 text-center\">"
                    + "<a href=\"contact-detail.html\">"
                    + "<img src=\"" + memberPictureUrl + "\" alt=\"" + memberName + "\" class=\"img-circle img-responsive\">"
                    + "</a>"
                    + "</div>"
                    + "<div class=\"col-md-8 col-sm-8\">"
                    + "<h3 class=\"box-title m-b-0\">" + memberName + "</h3> "
                    + "<h5 class=\"box-title1 m-b-0\" style=\"line-height1: 20px;\">" + proposerMemberId + "</h5>"
                    + "<small>" + memberEmail + "</small>"
                    + "<address>"
                    + "795 Folsom Ave, Suite 600 San Francisco, CADGE 94107"
                    + "<br/><br/>"
                    + "<abbr title=\"Phone\">Office :</abbr>&nbsp;<br/>"
                    + "<abbr title=\"Phone\">Home :</abbr>&nbsp;<br/>"
                    + "<abbr title=\"Phone\">Mobile :</abbr>" + memberMobile + "<br/>"
                    + "</address>"
                    + "</div>"
                    + "</div>"
                    + "</div>"
                    + "</div>";

        }

        //check alrady in committee
        commMemberCheckSQL = dbsession.createSQLQuery("SELECT * FROM committee_member WHERE id_committtee ='" + committeeId + "' AND member_id = '" + memberId + "'");
        if (commMemberCheckSQL.list().isEmpty()) {
            memberStatusInCommittee = "";
        } else {
            memberStatusInCommittee = "<div class=\"col-md-12 col-sm-12\"><div class=\"alert alert-danger alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> Member Already exits in committee</span>"
                    + "</div></div>";
        }

        responseCode = "1";
        responseMsg = "Success!!! Member found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("proposeMemberShipId", proposerMemberId);
        json.put("proposeMemberId", memberId);
        json.put("proposeMemberName", memberName);
        json.put("proposeMemberEmail", memberEmail);
        json.put("proposeMemberMobile", memberMobile);
        json.put("proposeMemberPicture", memberPicture);
        json.put("proposeMemberPictureUrl", memberPictureUrl);
        json.put("memberInfoCon", memberInfoCon);
        json.put("requestCommitteeId", committeeId);
        json.put("memberStatusInCommittee", memberStatusInCommittee);
        jsonArr.add(json);

    } else {

       

        responseCode = "0";
        responseMsg = "Error!!! Member not found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        memberInfoCon = "<div class=\"col-md-12 col-sm-12\">"
                + "<div class=\"white-box\" style=\"min-height: 70px; padding: 5px; border:1px solid #ccc;\">"
                + "<div class=\"col-md-12 col-sm-12\">"
                + "<div class=\"alert alert-danger alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> Member Not Found.Please try again</span>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("memberInfoCon", memberInfoCon);
        json.put("requestMemberId", proposerMemberId);
        json.put("requestCommitteeId", committeeId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>