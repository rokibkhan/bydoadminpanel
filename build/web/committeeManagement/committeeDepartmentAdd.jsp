<%@page import="com.appul.entity.Member"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>


<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
        String msgDispalyConT, msgInfoText, sLinkOpt;
        if (!strMsg.equals("")) {
            msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
            msgInfoText = "<strong>" + strMsg + "</strong> ";

        } else {
            msgDispalyConT = "style=\"display: none;\"";
            msgInfoText = "";
        }

        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);

        String committeeIdSel = request.getParameter("committeeId") == null ? "" : request.getParameter("committeeId").trim();

        if (committeeIdSel.equals("")) {

            //redirect committee list
            response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeList.jsp");
        }


    %>


    <%        Session dbsessionCommitteeAdd = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrxCommitteeAdd = dbsessionCommitteeAdd.beginTransaction();

        int ix = 1;
        String committeeId = "";
        String committeeType = "";
        String committeeName = "";
        String committeeShortName = "";
        String committeeDuration = "";
        String committeeDesc = "";
        String committeeStatus = "";
        Object objCmmt[] = null;

        String status1 = "";
        String btnActivePoll = "";
        String btnCompletePoll = "";
        String committeeMemberBtn = "";
        String agrX = "";

        String committeeComTypeId = "";
        String committeeTypeText = "";

        Query q1 = null;
        Object objcmType[] = null;

        Query usrSQL = dbsessionCommitteeAdd.createSQLQuery("SELECT c.id_committee, c.committee_type, c.committee_name, "
                + "c.committee_short_name, c.committee_duration, c.committee_desc, c.status, "
                + "c.feature_image,c.com_cat_id  "
                + "FROM  committee c "
                + "WHERE  c.id_committee = '" + committeeIdSel + "'");

        if (!usrSQL.list().isEmpty()) {
            for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                objCmmt = (Object[]) it1.next();
                committeeId = objCmmt[0].toString().trim();
                committeeType = obj[1] == null ? "" : objCmmt[1].toString().trim();
                committeeName = obj[2] == null ? "" : objCmmt[2].toString().trim();
                committeeShortName = objCmmt[3] == null ? "" : objCmmt[3].toString().trim();
                committeeDuration = obj[4] == null ? "" : objCmmt[4].toString().trim();
                //        committeeDesc = obj[5] == null ? "" : objCmmt[5].toString().trim();
                committeeStatus = obj[6] == null ? "" : objCmmt[6].toString().trim();

                agrX = "";

                if (committeeStatus.equals("0")) {
                    status1 = "InActive";
                    btnActivePoll = "";
                    //      btnActivePoll = "&nbsp;<a title=\"Status change to current\" onClick=\"committeeStatusChangeInfo("+agrX+");\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";
                    status1 = status1 + " " + btnActivePoll;
                    committeeMemberBtn = "";
                }

                if (committeeStatus.equals("1")) {
                    status1 = "Active";
                    btnCompletePoll = "&nbsp;<a title=\"Status change to old\" onClick=\"committeeStatusChangeInfo(" + agrX + ");\" class=\"btn btn-primary\"><i class=\"fa fa-check-circle-o fa-lg\"></i> Change</a>";

                    status1 = status1 + btnCompletePoll;
                    committeeMemberBtn = "<a title=\"Add committee member\"  class=\"btn btn-primary\" href=\"" + GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "\"><i class=\"fa fa-plus\"></i> Add Committee Member</a>";
                }

                //    techTeritoryName = obj[5] == null ?"" : obj[5].toString().trim();
                String committeeDetailsUrl = GlobalVariable.baseUrl + "/committeeManagement/committeeDetails.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                String updateProfileUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=profile";
                String updateChangePassUrl = GlobalVariable.baseUrl + "/committeeManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&committeeId=" + committeeId + "&selectedTab=changePass";

                committeeComTypeId = objCmmt[8] == null ? "" : objCmmt[8].toString().trim();

                System.out.println("committeeComTypeId::" + committeeComTypeId);
                System.out.println("committeeType::" + committeeType);

                if (committeeType.equals("HQ")) {
                    committeeTypeText = "Head Quarter";
                } else {

                    //division
                    if (committeeComTypeId.equals("2")) {

                        q1 = dbsessionCommitteeAdd.createSQLQuery("select * FROM member_division WHERE mem_division_id = '" + committeeType + "'");
                        if (!q1.list().isEmpty()) {
                            for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                objcmType = (Object[]) divisionItr.next();
                                committeeTypeText = objcmType[1].toString();
                            }
                        }

                    }
                    //centre
                    if (committeeComTypeId.equals("3")) {
                    }
                    //sub-centre
                    if (committeeComTypeId.equals("4")) {
                    }
                    //Overseas Chapter  Committee
                    if (committeeComTypeId.equals("5")) {
                    }
                    //Student Chapter  Committee
                    if (committeeComTypeId.equals("6")) {
                    }

                }
            }
        }
    %>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Committee Department</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Committee Management</a></li>                    
                    <li class="active">Department</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">&nbsp;&nbsp;<%=committeeName%></h3>
                    <h3 class="box-title m-b-0">&nbsp;&nbsp;<%=committeeTypeText%></h3>
                    <h3 class="box-title m-b-0">&nbsp;&nbsp;<%=committeeDuration%></h3>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row">
                        <div class="col-md-6"> 
                            <!--<form data-toggle="validator" class="form-horizontal">-->
                            <form data-toggle="validator" class="form-horizontal" name="syUserAdd" id="syUserAdd" method="POST" action="committeeDepartmentAddSubmitData.jsp?sessionid=<%=sessionid%>&act=add" onSubmit="return fromDataSubmitValidation()">
                                <input type="hidden" id="committeeId" name="committeeId" value="<%=committeeId%>">
                                <div class="form-group row custom-bottom-margin-5x">
                                    <label for="committeeName" class="control-label col-sm-3">Department Name</label>
                                    <div class="col-sm-9">

                                        <input name="committeeDepartment" id="committeeDepartment" type="text" class="form-control" placeholder="Department Name" required>

                                        <div  id="committeeDepartmentErr" class="help-block with-errors"></div>
                                    </div>
                                </div>








                                <div class="form-group row">
                                    <label class="control-label col-sm-3"></label>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);                        ?>">
                                        <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);                        ?>">
                                        <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);                        ?>">
                                        <input type="hidden" id="userstat" name="userstat" value="1">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6" id="showMemberDetailsInformationBox"> </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Committee Designation List</h3>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <%
                Object[] objDept = null;
                String departmentId = "";
                String departmentName = "";

                Query deptSQL = dbsessionCommitteeAdd.createSQLQuery("select * FROM committee_department WHERE committee_id ='" + committeeIdSel + "'  ORDER BY orderby ASC");
                if (!deptSQL.list().isEmpty()) {
                    for (Iterator itrDesg = deptSQL.list().iterator(); itrDesg.hasNext();) {
                        objDept = (Object[]) itrDesg.next();
                        departmentId = objDept[0].toString();
                        departmentName = objDept[1].toString();

            %>


            <!-- .col -->
            <div class="col-md-4 col-sm-4">
                <div class="white-box" style="min-height: 50px; padding: 5px;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">

                            <h5 class="box-title1 m-b-0" style="line-height1: 20px;"><%=departmentName%></h5> 

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.col -->

            <%
                    }
                }


            %>


        </div>


        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    <%    dbsessionCommitteeAdd.clear();
        dbsessionCommitteeAdd.close();
        %>


    <%@ include file="../footer.jsp" %>