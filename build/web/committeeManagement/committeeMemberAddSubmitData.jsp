<%-- 
    Document   : Sysytem User dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%-- <%@page import="java.sql.*" %> --%>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(50);
        int commID = Integer.parseInt(idS);

        String committeeId = request.getParameter("committeeId");
        String memberShipId = request.getParameter("memberShipId");
        String committeeMemberDesignation = request.getParameter("committeeMemberDesignation") == null ? "" : request.getParameter("committeeMemberDesignation").trim();
        String committeeMemberDepartment = request.getParameter("committeeMemberDepartment") == null ? "" : request.getParameter("committeeMemberDepartment").trim();

        System.out.println("committeeId:: " + committeeId);
        System.out.println("memberShipId::" + memberShipId);
        System.out.println("committeeMemberDesignation ::" + committeeMemberDesignation);
        System.out.println("committeeMemberDepartment:: " + committeeMemberDepartment);

        if ((memberShipId != null && !memberShipId.isEmpty()) && (committeeMemberDesignation != null && !committeeMemberDesignation.isEmpty())) {
            Object obj[] = null;
            Query q1 = null;

            String memberId = "";

            q1 = dbsession.createSQLQuery("SELECT * FROM member WHERE member_id = '" + memberShipId + "'");
            if (!q1.list().isEmpty()) {

                for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
                    obj = (Object[]) itr2.next();
                    memberId = obj[0].toString();
                }

                //    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                //    Date date1 = formatter1.parse(deadline);
                DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dateX = new Date();
                String adddate = dateFormatX.format(dateX);

                System.out.println("adddate   adddate:: " + adddate);

                String adduser = userNameH;
                String addterm = InetAddress.getLocalHost().getHostName().toString();
                String addip = InetAddress.getLocalHost().getHostAddress().toString();

                Query committeeAddSQL = dbsession.createSQLQuery("INSERT INTO committee_member("
                        + "id_committee_member,"
                        + "id_committtee,"
                        + "member_id,"
                        + "committee_designation,"
                        + "committee_department,"
                        + "add_user,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip,"
                        + "mod_user,"
                        + "mod_date,"
                        + "mod_term,"
                        + "mod_ip"
                        + ") VALUES("
                        + "'" + commID + "',"
                        + "'" + committeeId + "',"
                        + "'" + memberId + "',"
                        + "'" + committeeMemberDesignation + "',"
                        + "'" + committeeMemberDepartment + "',"
                        + "'" + adduser + "',"
                        + "'" + adddate + "',"
                        + "'" + addterm + "',"
                        + "'" + addip + "',"
                        + "'" + adduser + "',"
                        + "'" + adddate + "',"
                        + "'" + addterm + "',"
                        + "'" + addip + "'"
                        + "  ) ");

                committeeAddSQL.executeUpdate();

                dbsession.flush();
                dbtrx.commit();

                if (dbtrx.wasCommitted()) {
                    strMsg = "Success !!! New Job added";
                    response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + sessionIdH + "&committeeId=" + committeeId + "&strMsg=" + strMsg + "");
                } else {
                    dbtrx.rollback();
                    strMsg = "Error!!! When Job add";
                    response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + sessionIdH + "&committeeId=" + committeeId + "&strMsg=" + strMsg + "");
                }

            } else {
                strMsg = "Error!!! Member Info Not Found.Please try again";
                response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + sessionIdH + "&committeeId=" + committeeId + "&strMsg=" + strMsg + "");
            }
        } else {
            strMsg = "Error!!! MemberShip ID And Committee Designation is required.";
            response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + sessionIdH + "&committeeId=" + committeeId + "&strMsg=" + strMsg + "");
        }

    } else {
        strMsg = "Error!!! When adding committee member.Please try again";
        response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeMemberAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
    }

    dbsession.clear();
    dbsession.close();
%>
