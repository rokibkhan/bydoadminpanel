<%@page import="java.net.InetAddress"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Query q2 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    getRegistryID regId = new getRegistryID();
    String subscribeId = regId.getID(14);
    //memberId: arg2,jobId:arg3

    String sessionId = request.getParameter("sessionId").trim();
    String proposerMemberId = request.getParameter("proposerMemberId").trim();
    String proId = request.getParameter("proId").trim();

    String name = "";
    System.out.println("sessionId " + sessionId);

    String memberId = "";
    String memberName = "";

    q1 = dbsession.createSQLQuery("SELECT * FROM member WHERE member_id = '" + proposerMemberId + "'");
    if (!q1.list().isEmpty()) {

        for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
            obj = (Object[]) itr2.next();
            memberId = obj[0].toString();
            memberName = obj[2].toString();
        }

        responseCode = "1";
        responseMsg = "Success!!! Member found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestMemberId", proposerMemberId);
        json.put("proposerId", memberId);
        json.put("proposerName", memberName);
        json.put("reqProId", proId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! Member not found";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestMemberId", proposerMemberId);
        json.put("reqProId", proId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>