<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    int responseCode = 0;
    String responseMsg = "";
    String responseMsgHTML = "";
    Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String committeeMemberId = request.getParameter("committeeMemberId");
    // String sessionid = request.getParameter("sessionid").trim();

    q1 = dbsession.createSQLQuery("delete from committee_member where id_committee_member = " + committeeMemberId + "");

    q1.executeUpdate();

    dbsession.flush();
    dbtrx.commit();
    dbsession.close();

    if (dbtrx.wasCommitted()) {
        // strMsg = "Success !!! New Committee added";
        // response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

        responseCode = 1;
        responseMsg = "Delete successfully Completed.";
        responseMsgHTML = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>" + responseMsg + "</strong>"
                + "</div>";
    } else {
        dbtrx.rollback();
//        strMsg = "Error!!! When Committee add";
//        response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");
        responseCode = 0;
        responseMsg = "Error!!! When committee member delete.Please try again";
        responseMsgHTML = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>" + responseMsg + "</strong>"
                + "</div>";
    }

    json = new JSONObject();
    json.put("selectInvId", committeeMemberId);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    json.put("responseMsgHTML", responseMsgHTML);
    jsonArr.add(json);

    out.println(jsonArr);


%>