

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.SyUser"%>
<%@page import="com.appul.util.StringToDate"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.appul.util.TimeZoneConversion"%>
<%@page import="com.appul.util.TimeZoneConversion"%>



<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>

<!-- Page Content -->
<div id="page-wrapper">

    <%        String stockPgSessionid = request.getParameter("sessionid").trim();

        stockPgSessionid = session.getId();
        System.out.println(stockPgSessionid);
    %>

    <script type="text/javascript">
        //     $(function(){
        function showInvoicePaymentOption(arg1, arg2) {
            console.log("showInvoicePaymentOption :: " + arg1);

            $("#btnShowInvoicePaymentOption").button("loading");

            var rupantorLGModal;

            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("Invoice Payment");

            $.post("showInvoicePaymentOption.jsp", {invoiceId: arg1, sessionid: arg2}, function (data) {

                //    rupantorLGModal.find(".modal-dialog").css("margin-top", "336px");
                //showInvoicePaymentOptionDetails
                rupantorLGModal.find("#rupantorLGModalBody").html(data[0].invoiceDetails);
                rupantorLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);
                rupantorLGModal.modal('show');

            }, "json");
        }



        var modifyFlag = false;


        function noticeDeleteInfo(arg1, arg2) {

            console.log("NoticeDeleteInfo::: " + arg1);

            var rupantorLGModal, btnInfo, arg;

            arg = "'" + arg1 + "','" + arg2 + "'";
            rupantorLGModal = $('#rupantorLGModal');
            rupantorLGModal.find("#rupantorLGModalTitle").text("News Delete confirmation");
            btnInfo = "<a onclick=\"noticeDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";
            rupantorLGModal.find("#rupantorLGModalBody").html('');
            if (modifyFlag === false) {
                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo + rupantorLGModal.find("#rupantorLGModalFooter").html());
                modifyFlag = true;
            }

//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
            rupantorLGModal.modal('show');

        }

        function newsEditInfo(arg1, arg2) {
            console.log("newsEdit: " + arg1 + "-" + arg2);
        }



        function noticeDeleteInfoConfirm(arg1, arg2) {
            console.log("NoticeDeleteInfoConfirm:: " + arg1);
            var rupantorLGModal;
            rupantorLGModal = $('#rupantorLGModal');

//            $.post(
//                    "newsDeleteProcess.jsp", 
//                    {userId: arg1, sessionid: arg2},
//                    function(data){
//                
//                    }
//          );

            $.post("noticeDeleteProcess.jsp", {noticeId: arg1, sessionid: arg2}, function (data) {

                console.log(data);

                if (data[0].responseMsgCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    $("#infoBox" + data[0].selectInvId).html("<td colspan=\"6\" style=\"text-align: center;\"><h2>" + data[0].responseMsg + "</h2></td>");
                    rupantorLGModal.modal('hide');
                } else {

                }


            }, "json");

        }

    </script>

    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Notice List</h4>

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Notice Management</a></li>                    
                    <li class="active">Notice</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT">

            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" style="display: none;">
                <!--                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.
                                        <strong><%//=msgInfoText%></strong>
                                </div>-->
            </div>
            <!-- .globalAlertInfoBoxConTT end -->

            <div class="col-md-12">
                <div class="white-box">

                    <form class="form-group" role="search">
                        <div class="input-group">
                            <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search"> <span class="input-group-btn"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span> 
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered color-table inverse-table table-hover table-custom-padding-5x">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th class="text-center">Short Details</th>
                                    <th class="text-center">Download link</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    dbsession = HibernateUtil.getSessionFactory().openSession();//.openSession();

                                    dbtrx = dbsession.beginTransaction();
                                    DateFormat dateFormatNotice = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat dateFormatNoticeM = new SimpleDateFormat("MMM");
                                    DateFormat dateFormatNoticeD = new SimpleDateFormat("dd");
                                    DateFormat dateFormatNoticeY = new SimpleDateFormat("Y");

                                    Date dateNoticeM = null;
                                    Date dateNoticeD = null;
                                    Date dateNoticeY = null;
                                    String newDateNoticeM = "";
                                    String newDateNoticeD = "";
                                    String newDateNoticeY = "";

                                    Query noticeSQL = null;
                                    Object noticeObj[] = null;
                                    String noticeId = "";
                                    String noticeTitle = "";
                                    String noticeDetials = "";

                                    String noticeDateTime = "";
                                    String noticeMonth = "";
                                    String noticeDay = "";
                                    String noticeYear = "";

                                    String noticeDownloadUrl = "";
                                    String noticeDetailsUrl = "";
                                    String noticeInfoContainer = "";
                                    int ix = 1;

                                    noticeSQL = dbsession.createSQLQuery("SELECT * FROM post_notice pn "
                                            + "ORDER BY pn.id_notice_content DESC");

                                    if (!noticeSQL.list().isEmpty()) {
                                        for (Iterator itNotice = noticeSQL.list().iterator(); itNotice.hasNext();) {

                                            noticeObj = (Object[]) itNotice.next();
                                            noticeId = noticeObj[0].toString().trim();
                                            noticeTitle = noticeObj[4].toString().trim();

                                            noticeDetials = noticeObj[6].toString().trim();

                                            noticeDateTime = noticeObj[8].toString().trim();

                                            dateNoticeM = dateFormatNotice.parse(noticeDateTime);
                                            newDateNoticeM = dateFormatNoticeM.format(dateNoticeM);

                                            dateNoticeD = dateFormatNotice.parse(noticeDateTime);
                                            newDateNoticeD = dateFormatNoticeD.format(dateNoticeD);

                                            dateNoticeY = dateFormatNotice.parse(noticeDateTime);
                                            newDateNoticeY = dateFormatNoticeY.format(dateNoticeY);

                                            noticeDownloadUrl = noticeObj[7].toString().trim();

                                            noticeDetailsUrl = GlobalVariable.baseUrl + "/notice/noticeDetails.jsp?noticeIdX=" + noticeId;

                                %>


                                <tr id="infoBox<%=noticeId%>">
                                    <td><% out.print(ix);%></td>
                                    <td><% out.print(noticeTitle);%></td>
                                    <td><% out.println(noticeDetials); %></td>
                                    <td><a href="<%=noticeDownloadUrl%>">Download</a></td>
                                    <td class="text-center">

                                        <a onclick="newsEditInfo(<% out.print("'" + noticeId + "','" + sessionid + "'");%>)" href="newsEdit.jsp?sessionid=<%=sessionid%>&newsid=<%=noticeId%>" title="Edit Profile" class="btn btn-primary btn-sm"><i class="icon-note"></i></a>
                                        <a onclick="noticeDeleteInfo(<% out.print("'" + noticeId + "','" + sessionid + "'");%>)" title="Delete this news" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i></a>


                                    </td>
                                </tr>

                                <%
                                            ix++;
                                        }

                                    }

                                %>


                            </tbody>
                        </table>
                    </div>

                    <div class="align-right">
                        <ul class="pagination pagination-sm m-b-0 justify-content-end">
                            <li class="disabled"> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>

                    </div>


                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

    <%    dbsession.clear();
        dbsession.close();
    %>


    <%@ include file="../footer.jsp" %>