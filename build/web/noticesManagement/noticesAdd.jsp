

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<% request.setAttribute("TITLE", "Add new role");%>

<%@ include file="../header.jsp" %>

<%@ include file="../leftSideBar.jsp" %>



<%    String cssLinkPmtWysihtml5 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css";
    String jsLinkPmtWysihtml51 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js";
    String jsLinkPmtWysihtml52 = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js";

    String cssLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css";
    String jsLinkPmtDate = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js";

    String jsLinkMoment = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/moment/min/moment.min.js";
    String cssLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css";
    String jsLinkPmtDateTime = GlobalVariable.baseUrl + "/commonUtil/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js";
%>




<script>

    $(function () {
        var focus = 0;
        $("#syUserName").focusout(
                function () {
                    focus++;
                    console.log("checkUserNameAvaliablity:: " + focus);
                    var syUserName = $("#syUserName").val();

                    console.log("syUserName:: " + syUserName);

                    $("#syUserNameErr").html('<i class="fa fa-spin fa-spinner"></i> User name checking...');



                });

    });





    function fromDataSubmitValidation() {

//        console.log("DDDDD");
//        alert("KKK");

        // var thanaId = document.getElementById("thanaId").value;
        var noticeTitle = $("#noticeTitle").val();
        /*
         var syUserPassword = $("#syUserPassword").val();
         var syUserPasswordConfirm = $("#syUserPasswordConfirm").val();
         var syUserFullName = $("#syUserFullName").val();
         var syUserMobile = $("#syUserMobile").val();
         var syUserEmail = $("#syUserEmail").val();
         var syUserDept = $("#syUserDept").val();
         */
        if (noticeTitle == null || noticeTitle == "") {
            $("#noticeTitle").focus();
            $("#noticeTitleErr").addClass("help-block with-errors").html("Notice title is required").css({"background-color": "#fff", "border": "none", "color": "red"});
            return false;
        } else {
            $("#noticeTitleErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
        }
        /*
         if (syUserPassword == null || syUserPassword == "") {
         $("#syUserPassword").focus();
         $("#syUserPasswordErr").addClass("help-block with-errors").html("Password is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserPasswordConfirm == null || syUserPasswordConfirm == "") {
         $("#syUserPasswordConfirm").focus();
         $("#syUserPasswordConfirmErr").addClass("help-block with-errors").html("Password confirm is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserPasswordConfirmErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         if (syUserFullName == null || syUserFullName == "") {
         $("#syUserFullName").focus();
         $("#syUserFullNameErr").addClass("help-block with-errors").html("Name is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserFullNameErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserMobile == null || syUserMobile == "") {
         $("#syUserMobile").focus();
         $("#syUserMobileErr").addClass("help-block with-errors").html("Mobile number is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserMobileErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         if (syUserEmail == null || syUserEmail == "") {
         $("#syUserEmail").focus();
         $("#syUserEmailErr").addClass("help-block with-errors").html("Email is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserEmailErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         
         
         if (syUserDept == null || syUserDept == "") {
         $("#syUserDept").focus();
         $("#syUserDeptErr").addClass("help-block with-errors").html("Department is required").css({"background-color": "#fff", "border": "none", "color": "red"});
         return false;
         } else {
         $("#syUserDeptErr").removeClass("help-block with-errors").html("").css({"background-color": "#fff", "border": "none"});
         }
         */

        return true;
    }


</script>


<%
    String read = request.getParameter("read");
    String write = request.getParameter("write");

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String vId = request.getParameter("vId") == null ? "" : request.getParameter("vId").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block;\"";
//      sLinkOpt = GlobalVariable.baseUrl+"/account/voucherReportPrint.jsp?sessionid="+sessionid+"&voucherNo="+vId;
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }

// System.out.println(msgDispalyConT);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String todayX = dateFormatX.format(dateX);

%>

<!-- Page Content -->
<div id="page-wrapper">


    <!-- /.container-fluid -->
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Notice</h4>  

            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                        
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Notice Management</a></li>
                    <li class="active">Add Notice</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row globalAlertInfoBoxConParentTT" >
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-primary alert-dismissable fade in" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a> 
                    <!--                    <strong>Success!</strong> You should <a href="#" class="alert-link" style="color: #fff;">read this message</a>.-->
                    <strong><%=msgInfoText%></strong>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
            <div class="col-md-12">
                <div class="white-box">

                    <form  class="form-horizontal"  name="addrole" id="addrole" method="post" action="<%out.print(GlobalVariable.baseUrl);%>/Notice" onSubmit="return fromDataSubmitValidation()" enctype="multipart/form-data">

                        <input type="hidden" value="<%=todayX%>" id="noticeDate" name="noticeDate">


                        <div class="form-group row custom-bottom-margin-5x">
                            <label for="noticeTitle" class="control-label col-md-2">Title</label>
                            <div class="col-md-10">
                                <input type="text" id="noticeTitle" name="noticeTitle"  placeholder="Notice Title" class="form-control input-sm" required>
                                <div  id="noticeTitleErr" class="help-block with-errors"></div>
                            </div>                                
                        </div>



                        <link href="<%=cssLinkPmtWysihtml5%>" rel="stylesheet" type="text/css"/>  
                        <div class="form-group">
                            <label for="newsContent" class="control-label col-md-2">Description</label>
                            <div class="col-md-10">
                                <textarea id="noticeDesc" name="noticeDesc" placeholder="Notice Content" class="form-control" cols="10" rows="15" ></textarea>
                                <div id="noticeDescErr" class="help-block with-errors"></div>
                            </div>
                            <script src="<%=jsLinkPmtWysihtml51%>"></script>
                            <script src="<%=jsLinkPmtWysihtml52%>"></script>
                            <script type="text/javascript">
                        $(function () {
                            $("#noticeDesc").wysihtml5();

                        });
                            </script> 
                        </div>

                        <div class="form-group">
                            <label for="newsContent" class="control-label col-md-2">Short Description</label>
                            <div class="col-md-10">
                                <textarea onkeyup="countShortDescriptionChars(this, '200', 'noticeShortDesc', 'noticeShortDescErr');" placeholder="Short Description Maximum 200 character"  id="noticeShortDesc" name="noticeShortDesc" class="form-control" cols="5" rows="5"></textarea>
                                <div id="noticeShortDescErr" class="help-block with-errors" style="color: red;"></div>
                            </div>

                        </div>



                        <div class="form-group">
                            <div class="col-md-10 offset-md-2">
                                <div class="input-file-container">  
                                    <input onchange="uplaodFileTypePDFAndSizeCheck('file', '4', 'fileErr', 'noticeSubmitBtn');" name="file" id="file" type="file" class="input-file" >
                                    <label tabindex="0" for="my-file" class="input-file-trigger">Select a pdf...</label>
                                </div>
                                <div id="fileErr" class="help-block with-errors" style="color: red;"></div>
                            </div>


                        </div>






                        <div class="form-group row">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" id="entryUserID" name="entryUserID" value="<?php //out.print(username);      ?>">
                                <input type="hidden" id="entryTerm" name="entryTerm" value="<?php //out.print(hostname);      ?>">
                                <input type="hidden" id="entryIP" name="entryIP" value="<?php //out.print(ipAddress);      ?>">
                                <input type="hidden" id="userstat" name="userstat" value="1">
                                <button type="submit" class="btn btn-info" id="noticeSubmitBtn">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
                <!-- /.white-box -->
            </div>
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->




    <%@ include file="../footer.jsp" %>