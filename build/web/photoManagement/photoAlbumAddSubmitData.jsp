<%-- 
    Document   : Sysytem User dataInsert
    Created on : Jan 22, 2018, 11:22:05 AM
    Author     : Akter && Tahajjat
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>

<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>




<%
    Session dbsession = null;
    String qryparam = "";

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String userStoreIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        userStoreIdH = session.getAttribute("storeId").toString();
        int userStoreIdHi = Integer.parseInt(userStoreIdH);

        String photoAlbumName = request.getParameter("videoTitle");

        String photoAlbumShortName = request.getParameter("videoCaption");

        //    String videoPhoto = request.getParameter("videoPhoto");
//        System.out.println(""+videoPhoto);
//        System.out.println("CheckSticky: " + request.getParameter("checkbox_selection"));
//        String checkboxSelection = request.getParameter("checkbox_selection");
//        int stickyVal = -1;
//        if(checkboxSelection == null){
//            System.out.println("null paisi");
//            stickyVal = 0;
//        }
//        else if(checkboxSelection.equals("1")){
//            stickyVal = 1;
//            System.out.println("1 paisi");
//        }
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        Byte published = 1;
        String feature_image = "no_image.jpg";
        String showingOrder = "1";

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(20);
        int photoAlbumId = Integer.parseInt(idS);

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);
        String adduser = userNameH;
        String addterm = InetAddress.getLocalHost().getHostName().toString();
        String addip = InetAddress.getLocalHost().getHostAddress().toString();
        /*
        Query q = dbsession.createSQLQuery("INSERT INTO video_gallery_info("
                + "video_category,"
                + "video_title,"
                + "video_caption,"
                + "video_orginal_link,"
                + "video_emded_link,"
                + "feature_image,"
                + "image1,"
                + "image2,"
                + "image3,"
                + "image4,"
                + "showing_order,"
                + "ADD_DATE,"
                + "ADD_USER,"
                + "ADD_TERM,"
                + "ADD_IP) values( "
                + "'" + videoCategoryId + "',"
                + "'" + videoTitle + "',"
                + "'" + videoCaption + "',"
                + "'" + videoLink + "',"
                + "'" + videoEmdedLink + "',"
                + "'" + featureImageLink + "',"
                + "'" + image1 + "',"
                + "'" + image2 + "',"
                + "'" + image3 + "',"
                + "'" + image4 + "',"
                + "'" + showingOrder + "',"
                + "'" + adddate + "',"
                + "'" + adduser + "',"
                + "'" + addterm + "',"
                + "'" + addip + "')");

        q.executeUpdate();
         */

        PictureCategory album = new PictureCategory();

        album.setIdCat(photoAlbumId);
        album.setCategoryName(photoAlbumName);
        album.setCategoryShortName(photoAlbumShortName);
        album.setFeatureImage(feature_image);
        album.setImage1(feature_image);
        album.setImage2(feature_image);
        album.setImage3(feature_image);
        album.setImage4(feature_image);
        album.setShowingOrder(showingOrder);
        album.setPublished(published);

        album.setAddDate(adddate);
        album.setAddUser(adduser);
        album.setAddTerm(addterm);
        album.setAddIp(addip);

        album.setModDate(adddate);
        album.setModUser(adduser);
        album.setModTerm(addterm);
        album.setModIp(addip);

        dbsession.save(album);

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {

            strMsg = "Photo Album Added Successfully";
            response.sendRedirect("photoAlbumAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        } else {
            dbtrx.rollback();

            strMsg = "Error!!! When Photo Album add";
            response.sendRedirect("photoAlbumAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        }

    } else {
        //if session not found message here      
        response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    }
%>
