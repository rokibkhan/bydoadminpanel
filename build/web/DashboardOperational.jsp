
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>

<meta http-equiv="no-cache">
<meta http-equiv="no-store">

<link rel="stylesheet" type="text/css" href="css/nobodder.css">

<%
    request.getSession(false);
    response.setHeader("pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.addDateHeader("Expires", 0);
    response.setDateHeader("max-age", 0);
    response.setIntHeader("Expires", -1); //prevents caching at the proxy server
    response.addHeader("cache-Control", "private"); //IE5.x only;
%>

<%
    Session dbsession = null;
    String qryparam = "";
    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    /*            out.println(session.getId()); */
    if (session.isNew()) {
        response.sendRedirect("logout.jsp");
    }

%>

<%    String username = null;
    String logstat = null;
    String sessionid = request.getParameter("sessionid").trim();

    if (request.getSession(false) == null) {
//                        response.sendRedirect("login.jsp");
        return;
    }

    if (session.getId() == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    if (sessionid.equals(session.getId()) == false) {
        return;
    }
    if (session.getAttribute("username") != null) {
        username = session.getAttribute("username").toString().toUpperCase();
    }
    if (session.getAttribute("logstat") != null) {
        logstat = session.getAttribute("logstat").toString();
    }

    if (username == null || logstat.equals("Y") == false) {
        response.sendRedirect("logout.jsp");
        return;
    }
%>

<style type="text/css">
    .bp_invalid {
        color:white;
        background:red;
        width: 600px;
    }
    .bp_valid {
        color:green;
    }
</style>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function breakOut() {
                if (self == top)
                    window.open("logout.jsp", "_top", "");
            }
            function timeRefresh(timeOutPeriod) {
                setTimeout("location.reload(true);", timeOutPeriod);
            }

        </script>

        <TITLE>Dashboard</TITLE>
            <%
                /*You need to include the following JS file, if you intend to embed the chart using JavaScript.
                 Embedding using JavaScripts avoids the "Click to Activate..." issue in Internet Explorer
                 When you make your own charts, make sure that the path to this JS file is correct. Else, you would get JavaScript errors.
                 */
            %>
           
        <script type="text/javascript" src="./FusionChartsV3/fusioncharts.js"></script>
        <script type="text/javascript" src="./FusionChartsV3/themes/fusioncharts.theme.fint.js"></script>
     
        <style type="text/css">
            <!--
            body {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 10px;
            }
            .text{
                font-family: Arial, Helvetica, sans-serif;
                font-size: 10px;
            }
            -->
        </style>
    </HEAD>
    <BODY onLoad="breakOut();
            timeRefresh(120000);">

        Dashboard >&nbsp;&nbsp;<a href="Dashboard.jsp?sessionid=<%out.println(sessionid);%>">Today Summary</a>&nbsp;&nbsp;|
        <a href="DashboardPreviousDay.jsp?sessionid=<%out.println(sessionid);%>">Previous Day Summary</a>&nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="DashboardPeriodic.jsp?sessionid=<%out.println(sessionid);%>">Periodic Current Month</a>&nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="DashboardPeriodicOld.jsp?sessionid=<%out.println(sessionid);%>">Periodic Old</a>&nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="DashboardOperational.jsp?sessionid=<%out.println(sessionid);%>">Operational</a>&nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="DashboardOperatorWise.jsp?sessionid=<%out.println(sessionid);%>">Operator wise</a>&nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="DashboardTopDestination.jsp?sessionid=<%out.println(sessionid);%>">Top 20 Dest</a>&nbsp;&nbsp;|&nbsp;&nbsp;


        <table class="" style="width: 1000px;" >

            <tr>

                <td style="width: 500px;">	
                    <%

                        Query q1 = null;
                        String fileName = "";
                        String colTime = "";
                        String mediationTime = "";
                        java.util.Date lastThreadChangeDate;
                        String mediationStatus = "";
                        String ratingTime = "";
                        String ratingStatus = "";
                        String recCount = "";
                        int waitB4Alerm = 0;

                        int rownum = 0;
                        String oddeven = null;
                        Controller mdnThread = null;

                        //q1 = dbsession.createQuery("from SyUser as syuser where upper(userId) like upper('%" + qryparam + "%') or upper(userName) like upper('%" + qryparam + "%') order by userId asc");
                        q1 = dbsession.createSQLQuery("select file_name,col_time,dc_sttime,decode(dc_status,'N','Pending','Y','Complete','P','Pending','F','Failed') dc_status,rt_sttime,decode(rt_status,'N','Pending','Y','Complete','P','Pending','F','Failed') rt_status,rec_count from cdr_file where col_time=(select max(col_time) from cdr_file)");
                        Object[] object = null;
                        for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                            object = (Object[]) itr.next();
                            fileName = object[0].toString();
                            colTime = object[1].toString();
                            mediationTime = object[2] == null ? "" : object[2].toString();
                            mediationStatus = object[3].toString();
                            ratingTime = object[4] == null ? "" : object[4].toString();
                            ratingStatus = object[5].toString();
                            recCount = object[6] == null ? "" : object[6].toString();
                        }

                    %>
                    <h2>Report Update Time:<%out.println(new java.util.Date());%></h2>
                    <h1>Last File Status</h1>
                    <table  class="tab1">

                        <thead>

                            <tr >
                                <th align="left" width="110">Property</th>
                                <th align="left" width="390">Value</th>
                            </tr>
                        </thead>
                        <tr >
                            <td align="left" >File Name</td>
                            <td><% out.println(fileName);%></td>
                        </tr>
                        <tr >
                            <td align="left" >Collection Time</td>
                            <td><% out.println(colTime);%></td>
                        </tr>                    
                        <tr >
                            <td align="left" >Mediation Time</td>
                            <td><% out.println(mediationTime);%></td>
                        </tr>
                        <tr >
                            <td align="left" >Mediation Status</td>
                            <td><%out.println(mediationStatus);%></td>
                        </tr>
                        <tr >
                            <td align="left" >Rating Time</td>
                            <td><%out.println(ratingTime);%></td>
                        </tr>
                        <tr >
                            <td align="left" >Rating Status</td>
                            <td><%out.println(ratingStatus);%></td>
                        </tr> 
                        <tr >
                            <td align="left" >Record Count</td>
                            <td><%out.println(recCount);%></td>
                        </tr> 

                        </tbody>
                    </table>



                </td>

                <td style="width: 500px; ">	
                    <%
                        /*
                         In this example, we show how to connect FusionCharts to a database.
                         For the sake of ease, we've used a database which contains two tables, which are linked to each
                         other. 
                         */

                        //Database Objects - Initialization
                        //strXML will be used to store the entire XML document generated
                        String strXML4 = "";

                        //Generate the chart element
//				strXML = "<graph caption='Operator wise Report' subCaption='By Minutes' decimalPrecision='0' showNames='1'  numberSuffix=' Min'  pieSliceDepth='30' formatNumberScale='0'>";
//                                strXML3 = "<graph caption='Mediation Status'  decimalPrecision='0' showNames='1'  numberSuffix=' Files'  pieSliceDepth='30' formatNumberScale='0'>";
                        strXML4 += "<chart caption='CDR Reject Status' rotateLabels='0' valueFontColor='#000000' showcolumnshadow='0' divlineisdashed='1' divlinedashlen='1' divlinedashgap='2' xaxisname='Error Code' yaxisname='No. of CDR' theme='fint'>";

                        //Construct the query to retrieve data
//                                strQuery = "select * from Rt_Partners_profile";
                        Query qRej = dbsession.createSQLQuery("select REJECT_CODE,count(*) from APPREF.DISCARD_CDR group by REJECT_CODE order by REJECT_CODE");

                        String errorCode = null;
                        String errRecCount = null;

                        //Iterate through each factory	
                        List rejList = qRej.list();
                        Iterator itr4 = rejList.iterator();
                        Object[] objRejList = null;

                        while (itr4.hasNext()) {
                            objRejList = (Object[]) itr4.next();
                            errorCode = objRejList[0] == null ? "" : objRejList[0].toString();
                            errRecCount = objRejList[1] == null ? "" : objRejList[1].toString();

//                                    System.out.println("operName"+operName1+"duration"+duration1);
                            strXML4 += "<set label='" + errorCode + "' value='" + errRecCount + "' />";

                        }
                        strXML4 += "</chart>";
//				dbsession.close();
                        //Create the chart - Pie 3D Chart with data from strXML	
%> 
                    
                    <script type="text/javascript">
                        var test1 = "<%= strXML4%>";
                        FusionCharts.ready(function() {

                            var myChart1 = new FusionCharts({
                                "type": "column3d",
                                "renderAt": "chartContainer1",
                                "width": "550",
                                "height": "250",
                                "dataFormat": "xml",
                                "dataSource": test1
                            });

                            myChart1.render();
                        });
                    </script>
                    <div id="chartContainer1">FusionCharts XT will load here!</div>
     

                </td>

            </tr>   


            <tr>




                <td style="width: 500px; ">	
                    <%
                        /*
                         In this example, we show how to connect FusionCharts to a database.
                         For the sake of ease, we've used a database which contains two tables, which are linked to each
                         other. 
                         */

                        //Database Objects - Initialization
                        Query st12, st22;

                        String strQuery2 = "";

                        //strXML will be used to store the entire XML document generated
                        String strXML2 = "";

                        //Generate the chart element
//				strXML = "<graph caption='Operator wise Report' subCaption='By Minutes' decimalPrecision='0' showNames='1'  numberSuffix=' Min'  pieSliceDepth='30' formatNumberScale='0'>";
//                                strXML2 = "<graph caption='Rating Status'  decimalPrecision='0' showNames='1'  numberSuffix=' Files'  pieSliceDepth='30' formatNumberScale='0'>";
                        strXML2 += "<chart caption='Rating Status'  rotateLabels='0'  showcolumnshadow='0' divlineisdashed='1' divlinedashlen='1' divlinedashgap='2'  valueFontColor='#000000' xaxisname='Status' yaxisname='No. of Files' theme='fint'>";

                        //Construct the query to retrieve data
//                                strQuery = "select * from Rt_Partners_profile";
                        st12 = dbsession.createSQLQuery("select sum(case when rt_status in ('N','P') then 1 else 0 end) pending, sum(case when rt_status='Y' and c.rt_sttime between to_char(sysdate,'DD-MON-YYYY')||'12.00.00.000000000 AM' and to_char(sysdate,'DD-MON-YYYY')||'11.59.59.999000000 PM' then 1 else 0 end) rated, sum(case when rt_status='F' then 1 else 0 end) failed from appref.cdr_file c");

                        String rtPending = null;
                        String rtComplete = null;
                        String rtFailed = null;

                        //Iterate through each factory	
                        List lst2 = st12.list();
                        Iterator itr2 = lst2.iterator();
                        Object[] obj2 = null;

                        while (itr2.hasNext()) {
                            obj2 = (Object[]) itr2.next();
                            rtPending = obj2[0] == null ? "" : obj2[0].toString();
                            rtComplete = obj2[1] == null ? "" : obj2[1].toString();
                            rtFailed = obj2[2] == null ? "" : obj2[2].toString();

//                                    System.out.println("operName"+operName1+"duration"+duration1);
                        }
                        strXML2 += "<set label='Pending' value='" + rtPending + "' color='8BBA00'/>";
                        strXML2 += "<set label='Complete' value='" + rtComplete + "' color='FF8E46'/>";
                        strXML2 += "<set label='Failed' value='" + rtFailed + "' color='#FF0000'/>";
                        strXML2 += "</chart>";
//				dbsession.close();
                        //Create the chart - Pie 3D Chart with data from strXML	
%> 
                    <script type="text/javascript">
                        var test2 = "<%= strXML2%>";
                        FusionCharts.ready(function() {

                            var myChart2 = new FusionCharts({
                                "type": "column3d",
                                "renderAt": "chartContainer2",
                                "width": "550",
                                "height": "250",
                                "dataFormat": "xml",
                                "dataSource": test2
                            });

                            myChart2.render();
                        });
                    </script>
                    <div id="chartContainer2">FusionCharts XT will load here!</div>
   

                </td>



                <td style="width: 500px; ">	
                    <%
                        /*
                         In this example, we show how to connect FusionCharts to a database.
                         For the sake of ease, we've used a database which contains two tables, which are linked to each
                         other. 
                         */

                        //Database Objects - Initialization
                        Query st13, st23;

                        String strQuery3 = "";

                        //strXML will be used to store the entire XML document generated
                        String strXML3 = "";

                        //Generate the chart element
//				strXML = "<graph caption='Operator wise Report' subCaption='By Minutes' decimalPrecision='0' showNames='1'  numberSuffix=' Min'  pieSliceDepth='30' formatNumberScale='0'>";
//                                strXML3 = "<graph caption='Mediation Status'  decimalPrecision='0' showNames='1'  numberSuffix=' Files'  pieSliceDepth='30' formatNumberScale='0'>";
                        strXML3 += "<chart caption='Mediation Status' rotateLabels='0' valueFontColor='#000000' showcolumnshadow='0' divlineisdashed='1' divlinedashlen='1' divlinedashgap='2' xaxisname='Status' yaxisname='No. of Files' theme='fint'>";

                        //Construct the query to retrieve data
//                                strQuery = "select * from Rt_Partners_profile";
                        st13 = dbsession.createSQLQuery("select sum(case when dc_status in ('N','P') then 1 else 0 end) pending, sum(case when dc_status='Y' and c.dc_sttime between to_char(sysdate,'DD-MON-YYYY')||'12.00.00.000000000 AM' and to_char(sysdate,'DD-MON-YYYY')||'11.59.59.999000000 PM' then 1 else 0 end) mediated, sum(case when dc_status='F' then 1 else 0 end) failed  from appref.cdr_file c ");

                        String mPending = null;
                        String mComplete = null;
                        String mFailed = null;
                        ResultSet rs13;

                        //Iterate through each factory	
                        List lst3 = st13.list();
                        Iterator itr3 = lst3.iterator();
                        Object[] obj3 = null;

                        while (itr3.hasNext()) {
                            obj3 = (Object[]) itr3.next();
                            mPending = obj3[0] == null ? "" : obj3[0].toString();
                            mComplete = obj3[1] == null ? "" : obj3[1].toString();
                            mFailed = obj3[2] == null ? "" : obj3[2].toString();

//                                    System.out.println("operName"+operName1+"duration"+duration1);
                        }
                        strXML3 += "<set label='Pending' value='" + mPending + "' />";
                        strXML3 += "<set label='Complete' value='" + mComplete + "' />";
                        strXML3 += "<set label='Failed' value='" + mFailed + "' />";
                        strXML3 += "</chart>";
//				dbsession.close();
                        //Create the chart - Pie 3D Chart with data from strXML	
%> 
                    <script type="text/javascript">
                        var test4 = "<%= strXML3%>";
                        FusionCharts.ready(function() {

                            var myChart4 = new FusionCharts({
                                "type": "column3d",
                                "renderAt": "chartContainer3",
                                "width": "550",
                                "height": "250",
                                "dataFormat": "xml",
                                "dataSource": test4
                            });

                            myChart4.render();
                        });
                    </script>
                    <div id="chartContainer3">FusionCharts XT will load here!</div>
     

                </td>




            </tr>
            <tr>
                <td colspan="2">


                    <table  class="">
                        <tr>
                            <td style="width: 300px;">

                                <table  class="">

                                    <%
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        //                Query q1 = null;
                                        String neId = "";
                                        java.util.Date collasttime = null;
                                        int interval = 0;
                                        //                int rownum = 0;
                                        getDate gdate = new getDate();
                                        String currentDate = sdf.format(gdate.serverDate());
                                        java.util.Date crrDate = new java.util.Date();
                                        //                String oddeven = null;
                                        String cdate = "";
                                        String ldate = "";
                                        String year = "";
                                        String month = "";
                                        String ddate = "";
                                        String hour = "";
                                        String min = "";
                                        String sec = "";
                                        long colllectionTime = 0;
                                        long currentTime = 0;
                                        long count = 0;

                                        NodeConf neconf = null;
                                        q1 = dbsession.createQuery("from NodeConf as neconf");

                                        for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                            neconf = (NodeConf) itr.next();

                                            neId = neconf.getNeId();
                                            collasttime = neconf.getNeLastColDatetime();
                                            interval = neconf.getNeAlarmInterval();

                                            cdate = currentDate.toString();
                                            if (collasttime != null) {
                                                ldate = sdf.format(collasttime);

                                                year = ldate.substring(0, 4);
                                                month = ldate.substring(5, 7);
                                                ddate = ldate.substring(8, 10);
                                                hour = ldate.substring(11, 13);
                                                min = ldate.substring(14, 16);
                                                sec = ldate.substring(17, ldate.length());
                                                colllectionTime = (Long.parseLong(year) * 365 * 24 * 3600) + (Long.parseLong(month) * 30 * 24 * 3600) + (Long.parseLong(ddate) * 24 * 3600)
                                                        + (Long.parseLong(hour) * 3600) + (Long.parseLong(min) * 60) + (Long.parseLong(sec));

                                            }
                                            year = cdate.substring(0, 4);
                                            month = cdate.substring(5, 7);
                                            ddate = cdate.substring(8, 10);
                                            hour = cdate.substring(11, 13);
                                            min = cdate.substring(14, 16);
                                            sec = cdate.substring(17, cdate.length());
                                            currentTime = (Long.parseLong(year) * 365 * 24 * 3600) + (Long.parseLong(month) * 30 * 24 * 3600) + (Long.parseLong(ddate) * 24 * 3600)
                                                    + (Long.parseLong(hour) * 3600) + (Long.parseLong(min) * 60) + (Long.parseLong(sec));

                                            count = currentTime - colllectionTime;
                                            rownum += 1;

                                            if ((currentTime - colllectionTime) > interval) {
                                                oddeven = "even";
                                            } else {
                                                oddeven = "odd";
                                            }
                                    %>

                                    <tr >
                                        <td> <% if (neId.equalsIgnoreCase("COLL_NSN01")) {out.println("NSN Colection1 Process Status:");}
                                                else if (neId.equalsIgnoreCase("COLL_NSN02")) {out.println("NSN Colection2 Process Status:");}
                                                else if  (neId.equalsIgnoreCase("COLL_GEN01")) {out.println("Genband Collection Process Status:");}
                                                else if (neId.equalsIgnoreCase("COLL_TCB01")) {out.println("TelcoBridge Collection Process Status:");}%></td>
                                        <td align="middle"> <%if (oddeven == "even") {%> <img src="image/red.png" width="30px" height="30px"> <%} else {%><img src="image/green.png" width="30px" height="30px"> <%}%> </td>
                                    </tr>


                                    <%
                                        }
                                    %>

                                </table>
                            </td>
                            <td style="width: 300px;">	

                                <table class="">
                                    <%
                                        Query q2 = null;
                                        String normId = "";
                                        java.util.Date norCollasttime = null;
                                        int norInterval = 0;
                                        int norRownum = 0;
                                        String norCurrentDate = sdf.format(gdate.serverDate());
                                        java.util.Date norcrrDate = new java.util.Date();
                                        String norOddeven = null;
                                        String norCdate = "";
                                        String norLdate = "";
                                        String norYear = "";
                                        String norMonth = "";
                                        String norDdate = "";
                                        String norHour = "";
                                        String norMin = "";
                                        String norSec = "";
                                        long norColllectionTime = 0;
                                        long norCurrentTime = 0;
                                        long norCount = 0;

                                        MediationConf norconf = null;
                                        q2 = dbsession.createQuery("from MediationConf as norconf");

                                        for (Iterator it = q2.list().iterator(); it.hasNext();) {
                                            norconf = (MediationConf) it.next();

                                            normId = norconf.getNormId().trim();
                                            norCollasttime = norconf.getNormLastExecTime();
                                            norInterval = norconf.getNormAlarmInterval();

                                            norCdate = norCurrentDate.toString();
                                            if (norCollasttime != null) {
                                                norLdate = sdf.format(norCollasttime);

                                                norYear = norLdate.substring(0, 4);
                                                norMonth = norLdate.substring(5, 7);
                                                norDdate = norLdate.substring(8, 10);
                                                norHour = norLdate.substring(11, 13);
                                                norMin = norLdate.substring(14, 16);
                                                norSec = norLdate.substring(17, norLdate.length());
                                                norColllectionTime = (Long.parseLong(norYear) * 365 * 24 * 3600) + (Long.parseLong(norMonth) * 30 * 24 * 3600) + (Long.parseLong(norDdate) * 24 * 3600)
                                                        + (Long.parseLong(norHour) * 3600) + (Long.parseLong(norMin) * 60) + (Long.parseLong(norSec));
                                            }

                                            norYear = norCdate.substring(0, 4);
                                            norMonth = norCdate.substring(5, 7);
                                            norDdate = norCdate.substring(8, 10);
                                            norHour = norCdate.substring(11, 13);
                                            norMin = norCdate.substring(14, 16);
                                            norSec = norCdate.substring(17, norCdate.length());
                                            norCurrentTime = (Long.parseLong(norYear) * 365 * 24 * 3600) + (Long.parseLong(norMonth) * 30 * 24 * 3600) + (Long.parseLong(norDdate) * 24 * 3600)
                                                    + (Long.parseLong(norHour) * 3600) + (Long.parseLong(norMin) * 60) + (Long.parseLong(norSec));

                                            norCount = norCurrentTime - norColllectionTime;
                                            rownum += 1;

                                            if ((currentTime - norColllectionTime) > norInterval) {
                                                norOddeven = "even";
                                            } else {
                                                norOddeven = "odd";
                                            }
                                    %>

                                    <tr>
                                        <td> <% if (normId.equalsIgnoreCase("MED_NSN01")) {out.println("NSN Mediation1 Process Status:");}
                                                else if (normId.equalsIgnoreCase("MED_NSN02")) {out.println("NSN Mediation2 Process Status:");}
                                                else if  (normId.equalsIgnoreCase("MED_GEN01")) {out.println("Genband Mediation Process Status:");}
                                                else if (normId.equalsIgnoreCase("MED_TCB01")) {out.println("TelcoBridge Mediation Process Status:");}%></td>
                                        <td align="middle"> <%if (norOddeven == "even") {%> <img src="image/red.png"  width="30px" height="30px"> <%} else {%><img src="image/green.png" width="30px" height="30px"> <%}%> </td>
                                    </tr>


                                    <%
                                        }
                                    %>

                                </table>



                            </td>

                            <td style="width: 300px;">	
                                <table class="">
                                    <%
                                        Query q3 = null;
                                        String ratId = "";
                                        java.util.Date ratCollasttime = null;
                                        int ratInterval = 0;
                                        int ratRownum = 0;
                                        String ratCurrentDate = sdf.format(gdate.serverDate());
                                        java.util.Date ratcrrDate = new java.util.Date();
                                        String ratOddeven = null;
                                        String ratCdate = "";
                                        String ratLdate = "";
                                        String ratYear = "";
                                        String ratMonth = "";
                                        String ratDdate = "";
                                        String ratHour = "";
                                        String ratMin = "";
                                        String ratSec = "";
                                        long ratColllectionTime = 0;
                                        long ratCurrentTime = 0;
                                        long ratCount = 0;

                                        RatingConf ratconf = null;
                                        q3 = dbsession.createQuery("from RatingConf as ratconf where ratThdId='RAT_MAIN'");

                                        for (Iterator it = q3.list().iterator(); it.hasNext();) {
                                            ratconf = (RatingConf) it.next();

                                            ratId = ratconf.getRatThdId();
                                            ratCollasttime = ratconf.getRatLastExecTime();
                                            ratInterval = ratconf.getRatAlarmInterval();

                                            ratCdate = currentDate.toString();
                                            ratLdate = sdf.format(ratCollasttime);

                                            ratYear = ratCdate.substring(0, 4);
                                            ratMonth = ratCdate.substring(5, 7);
                                            ratDdate = ratCdate.substring(8, 10);
                                            ratHour = ratCdate.substring(11, 13);
                                            ratMin = ratCdate.substring(14, 16);
                                            ratSec = ratCdate.substring(17, ldate.length());
                                            ratCurrentTime = (Long.parseLong(ratYear) * 365 * 24 * 3600) + (Long.parseLong(ratMonth) * 30 * 24 * 3600) + (Long.parseLong(ratDdate) * 24 * 3600)
                                                    + (Long.parseLong(ratHour) * 3600) + (Long.parseLong(ratMin) * 60) + (Long.parseLong(ratSec));

                                            ratYear = ratLdate.substring(0, 4);
                                            ratMonth = ratLdate.substring(5, 7);
                                            ratDdate = ratLdate.substring(8, 10);
                                            ratHour = ratLdate.substring(11, 13);
                                            ratMin = ratLdate.substring(14, 16);
                                            ratSec = ratLdate.substring(17, cdate.length());
                                            ratColllectionTime = (Long.parseLong(ratYear) * 365 * 24 * 3600) + (Long.parseLong(ratMonth) * 30 * 24 * 3600) + (Long.parseLong(ratDdate) * 24 * 3600)
                                                    + (Long.parseLong(ratHour) * 3600) + (Long.parseLong(ratMin) * 60) + (Long.parseLong(ratSec));

                                            ratCount = ratCurrentTime - ratColllectionTime;
                                            rownum += 1;

                                            if ((ratCurrentTime - ratColllectionTime) > ratInterval) {
                                                oddeven = "even";
                                            } else {
                                                oddeven = "odd";
                                            }
                                    %>
                                    <tr>    
                                        <td> <% out.println("Rating Process Status:");%></td>
                                        <td align="middle"> <%if (oddeven == "even") {%> <img src="image/red.png"  width="30px" height="30px"> <%} else {%><img src="image/green.png" width="30px" height="30px"> <%}%> </td>
                                    </tr>


                                    <%
                                        }
                                    %>

                                </table>

                            </td>
                        </tr>
                    </table>
                </td>


            </tr>

        </table> 
        <%
                                dbsession.flush();
                                dbsession.close();
        %>


    </BODY>
</HTML>
