<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    int responseMsgCode = 0;
    String responseMsg = "";
    String responseMsgHTML = "";
    Query q1 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String jobId = request.getParameter("jobId");
    String sessionid = request.getParameter("sessionid").trim();

    Query qq = dbsession.createSQLQuery("SELECT * "
            + "FROM  job_post  WHERE ID=" + jobId);

    String jobCategoryId = "";
    String jobDesignation = "";
    String jobDescription = "";
    String jobAge = "";
    String jobGender = "";
    String jobSalaryMin = "";
    String jobSalaryMax = "";
    String jobSalaryNego = "";
    String jobExperience = "";
    String jobType = "";
    String jobCompany = "";
    String jobLocation = "";
    String jobApplicationDeadline = "";
    String syStatus = "";
    String userStatusDesc = "";
    String userRole = "";
    String btnColorClass = "";
    String levelStatusColorClass = "";
    String deadline = "";

    if (!qq.list().isEmpty()) {
        Iterator iterator = qq.list().iterator();
        obj = (Object[]) iterator.next();
        jobId = obj[0] != null ? obj[0].toString().trim() : "";
        jobCategoryId = obj[1] != null ? obj[1].toString().trim() : "";
        jobDesignation = obj[2] != null ? obj[2].toString().trim() : "";
        jobDescription = obj[3] != null ? obj[3].toString().trim() : "";
        jobAge = obj[4] != null ? obj[4].toString().trim() : "";
        jobGender = obj[5] != null ? obj[5].toString().trim() : "";
        jobSalaryMin = obj[6] != null ? obj[6].toString().trim() : "";
        jobSalaryMax = obj[7] != null ? obj[7].toString().trim() : "";
        jobSalaryNego = obj[8] != null ? obj[8].toString().trim() : "";
        if (jobSalaryNego.equals("0")) {
            jobSalaryNego = "No";
        } else {
            jobSalaryNego = "Yes";
        }
        jobExperience = obj[9] != null ? obj[9].toString().trim() : "";
        jobType = obj[10] != null ? obj[10].toString().trim() : "";
        jobCompany = obj[11] != null ? obj[11].toString().trim() : "";
        jobLocation = obj[12] != null ? obj[12].toString().trim() : "";
        jobApplicationDeadline = obj[13] != null ? obj[13].toString().trim() : "";

        System.out.println("UTT " + jobApplicationDeadline);

        deadline = jobApplicationDeadline.split(" ")[0];

        Query us1 = dbsession.createSQLQuery("SELECT * "
                + "FROM  job_post_category  WHERE ID = " + jobCategoryId);

        Iterator i1 = us1.list().iterator();
        Object[] o1 = (Object[]) i1.next();
        String jobCategoryName = o1[1].toString();

        us1 = dbsession.createSQLQuery("SELECT * "
                + "FROM  job_post_type  WHERE ID = " + jobType);
        i1 = us1.list().iterator();
        o1 = (Object[]) i1.next();
        jobType = o1[1].toString();

        us1 = dbsession.createSQLQuery("SELECT * "
                + "FROM  company  WHERE ID = " + jobCompany);
        i1 = us1.list().iterator();
        o1 = (Object[]) i1.next();
        jobCompany = o1[1].toString();

    }

    responseMsgCode = 1;
    responseMsg = "Job fetching successfully Completed.";
    responseMsgHTML = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
            + "<strong>Delete successfully Completed.</strong>"
            + "</div>";

    json = new JSONObject();
    json.put("selectInvId", jobId);
    json.put("responseMsgCode", responseMsgCode);
    json.put("responseMsg", responseMsg);
    json.put("jobDesignation", jobDesignation);
    json.put("jobDescription", jobDescription);
    json.put("jobAge", jobAge);

    json.put("jobGender", jobGender);

    json.put("jobSalaryMin", jobSalaryMin);
    json.put("jobSalaryMax", jobSalaryMax);
    json.put("jobSalaryNego", jobSalaryNego);
    json.put("jobExperience", jobExperience);
    json.put("jobCompany", jobCompany);
    json.put("jobLocation", jobLocation);
    json.put("deadline", deadline);
    jsonArr.add(json);

    out.println(jsonArr);

    dbsession.flush();
    dbtrx.commit();
    dbsession.close();


%>